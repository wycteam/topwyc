<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon ;

class TransferBalance extends Model
{
    use SoftDeletes;
    protected $connection = 'visa';
	protected $table = 'client_transfer_balance';
    protected $fillable = [
		 'id'
        ,'transfer_from'
		,'from_type'
        ,'transfer_to'
		,'to_type'
        ,'amount'
        ,'reason'
		,'log_date'
    ];

    public $timestamps = false;

    // public function user(){
    // 	return $this->belongsTo('App\Models\User','client_id');
    // }

    // public function group(){
    // 	return $this->belongsTo('App\Models\Visa\Group','group_id');
    // }

}
