<?php

namespace App\Models\Visa;

use App\Models\User;
use App\Models\Visa\ByBatch;
use App\Models\Visa\Group;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\Controllers\Cpanel\SynchronizationController;
use App\Http\Controllers\Cpanel\ClientsController;
use Carbon\Carbon;
use DB;

class ClientTransactions extends Model
{
    use SoftDeletes;
    // protected $dates = ['deleted_at'];
    protected $connection = 'visa';
	protected $table = 'client_transactions';
    protected $fillable = [
		 'client_id'
		,'type'
        ,'amount'
        ,'group_id'
		,'service_id'
        ,'reason'
		,'tracking'
		,'log_date'
        ,'authorizer'
        ,'storage_type'
        ,'alipay_reference'
        ,'is_commission'
    ];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            SynchronizationController::insertUpdate($model->client_id, Carbon::now()->toDateTimeString());
        });

        static::updating(function($model)
        {
            SynchronizationController::insertUpdate($model->client_id, Carbon::now()->toDateTimeString());
        });

        static::created(function ($model) {
            static::updateBalance($model->client_id, $model->group_id, $model->log_date, $model->service_id);
        });

        static::updated(function ($model) {
            static::updateBalance($model->client_id, $model->group_id, $model->log_date, $model->service_id);
        });
    }

    private static function updateBalance($clientId, $groupId, $log_date, $serviceId) {
        if($groupId == 0){
            $balance = ClientsController::clientBal($clientId);
            $col = ClientsController::clientCollectable($clientId);
            $temp_balance = ClientsController::clientBalDec1($clientId);
            User::where('id', $clientId)->update([
                'balance' => $balance,'collectable' => $col, 
                // 'temp_balance' => $temp_balance
            ]);
        }
        else{
            $balance = ClientsController::groupBal($groupId);
            $col = ClientsController::groupCollectable($groupId);
            $temp_balance = ClientsController::groupBalDec1($groupId);
            Group::where('id', $groupId)->update([
                'balance' => $balance,'collectable' => $col,
                // 'temp_balance' => $temp_balance
            ]);

            //try
                $id = $groupId;
                $service_date = str_replace(' ', '', substr($log_date,0,10));

                $byBatch = DB::connection('visa')
                        ->table('client_services as a')
                        ->select(DB::raw('id,detail,service_date, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y") as sdate, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%Y%m%d") as tdate'))
                        ->where('active',1)->where('group_id',$id)
                        ->where('service_date',$service_date)
                        ->orderBy('id','DESC')
                        ->groupBy('sdate')
                        ->get();
                $byBatchDate = $byBatch->pluck('tdate');
                //\Log::info($byBatch);
                //\Log::info($service_date);

                $transactions = DB::connection('visa')
                    ->table('client_transactions as a')
                    ->select(DB::raw('
                        a.amount,a.type,a.service_id,a.log_date, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate'))
                        ->where('group_id', $id)
                        ->where(function($query) {
                            $query->where('service_id', null)
                                ->orWhere('service_id', 0);
                        })
                        ->where('log_date','LIKE', '%'.$service_date.'%')
                        ->orderBy('tdate','DESC')    
                        ->get();
                //\Log::info($transactions);

                $transDate = $transactions->pluck('tdate');

                $merged = $byBatchDate->merge($transDate);

                $result = $merged->all();
                $tdates = array_unique($result);
                rsort($tdates);

                $countRecord = count($tdates);
                if($countRecord < 1){
                    $byBatch = DB::connection('visa')
                        ->table('client_services as a')
                        ->select(DB::raw('id,detail,service_date, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y") as sdate, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%Y%m%d") as tdate'))
                        ->where('active',1)->where('group_id',$id)
                        ->where('id',$serviceId)
                        ->groupBy('sdate')
                        ->get();
                    $byBatchDate = $byBatch->pluck('tdate');
                    $merged = $byBatchDate->merge($transDate);

                    $result = $merged->all();
                    $tdates = array_unique($result);
                    rsort($tdates);
                }

                $groupcost = ClientsController::groupCollectable2($groupId);
                $lastcost = 0;
                $tlist = [];
                $datectr = 0;
                $dt = null;
                $dt2 = null;
                $arr = [];
                $arr2 = [];
                foreach($tdates as $d){
                    $y =  substr($d,0,4);
                    $m =  substr($d,4,2);
                    $day =  substr($d,6,2);
                    $nDate = $m.'/'.$day.'/'.$y;

                    $dt =   $d;
                    $dt2 =   Carbon::parse($nDate)->format('F d,Y');

                    $query = DB::connection('visa')
                        ->table('client_services')->where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$nDate)->where('group_id', $id)->where('active', 1)->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('client_id')->get();
                        $ctr2 = 0;
                        //$servTotal = 0;
                        $client = [];
                        $arr = [];
                        $flag = true;
                        $client_ctr = 0;
                        foreach($query as $m){
                            $ss =  ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$nDate)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->with('discount2')->get();
                            if(count($ss) > 0){
                                $client = User::where('id',$m->client_id)->select('first_name','last_name')->first()->full_name;
                                $totalcost = ClientService::where('service_id',$id)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
                                $services = $ss;

                                $arr[$client_ctr]['name'] = $client;

                                $servctr = 0;
                                foreach($services as $serv){
                                    $tcost = $serv->cost + $serv->tip + $serv->charge + $serv->com_client + $serv->com_agent;
                                    if($serv->status != 'complete'){
                                        $tcost = 0;
                                    }

                                    if(strpos($serv->detail, "9A") === false && $serv->status != 'complete'){
                                        $tcost = $serv->cost + $serv->tip + $serv->charge + $serv->com_client + $serv->com_agent;
                                    }

                                    $arr[$client_ctr]['services'][$servctr]['tracking'] = $serv->tracking;
                                    $arr[$client_ctr]['services'][$servctr]['detail'] = $serv->detail;
                                    $arr[$client_ctr]['services'][$servctr]['total_cost'] = $tcost;
                                    $arr[$client_ctr]['services'][$servctr]['service_id'] = $serv->id;

                                    $sp = $serv->status;
                                    if($serv->active==0){
                                        $sp = "CANCELLED";
                                    }
                                    $arr[$client_ctr]['services'][$servctr]['status'] = $sp;

                                    // if($flag= false){
                                    //     $tempTotal = $groupcost - $lastcost;
                                    // }
                                    // else{
                                    //     $tempTotal = $groupcost + $lastcost;
                                    // }

                                    $translated = Service::where('id',$serv->service_id)->first();
                                    $cnserv =$serv->detail;
                                    if($translated){
                                        $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                                    }
                                    $arr[$client_ctr]['services'][$servctr]['detail_cn'] = $cnserv;
                                    //$arr[$client_ctr]['services'][$servctr]['tempTotal'] = $tempTotal;


                                    //$lastcost = $tcost;
                                    //$groupcost = $tempTotal;

                                    if($serv->discount_amount>0){
                                        $servctr++;
                                        //$tempTotal = $groupcost + $lastcost;

                                        $arr[$client_ctr]['services'][$servctr]['tracking'] = '';
                                        $arr[$client_ctr]['services'][$servctr]['detail'] = 'Discount';
                                        $arr[$client_ctr]['services'][$servctr]['detail_cn'] = '折扣';
                                        $arr[$client_ctr]['services'][$servctr]['total_cost'] = $serv->discount_amount;
                                        $arr[$client_ctr]['services'][$servctr]['service_id'] = '';
                                        $arr[$client_ctr]['services'][$servctr]['status'] = '';
                                        //$arr[$client_ctr]['services'][$servctr]['tempTotal'] = $tempTotal;


                                        //$lastcost = -1 *$serv->discount_amount;
                                        //$groupcost = $tempTotal;

                                    }

                                    //$row++;
                                    $flag = false;
                                    $servctr++;
                                }
                            }
                            $client_ctr++;

                            //$row++;
                            $ctr2++;
                        }

                        $flag = false;
                        $trans_ctr = 0;
                        $arr2 = [];
                        foreach($transactions as $t){
                            if($t->tdate == $d){
                                if($t->type != 'Discount' || ($t->type == 'Discount' && ($t->service_id == null || $t->service_id == 0))){
                                    $dtype = '';
                                    // $sign = ' +';
                                    // if($flag= false){
                                    //     $tempTotal = $groupcost - $lastcost;
                                    // }
                                    // else{
                                    //     $tempTotal = $groupcost + $lastcost;
                                    // }


                                    if($t->type == 'Deposit'){
                                        $dtype = '预存款';

                                        //$lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }
                                    if($t->type == 'Discount'){
                                        $dtype = '折扣';
                                        //$lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }                            
                                    if($t->type == 'Payment'){
                                        $dtype = '已付款';
                                        //$lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }                            
                                    if($t->type == 'Refund'){
                                        $dtype = '退款';
                                        //$lastcost = $t->amount;
                                        //$sign = '-';
                                        //$tempTotal = $groupcost + (-1*$lastcost);
                                    }

                                    $arr2[$trans_ctr]['detail'] = $t->type;
                                    $arr2[$trans_ctr]['detail_cn'] = $dtype;
                                    $arr2[$trans_ctr]['total_cost'] = $t->amount;
                                    //$arr2[$trans_ctr]['tempTotal'] = $tempTotal;
                                    }
                                    //$groupcost = $tempTotal;

                                    //$flag = true;
                                    $trans_ctr++;

                            }
                        }
                    $datectr++;

                }
            //\Log::info($arr);
            //\Log::info($arr2);
            //\Log::info(!empty($tlist));
            if($dt != null || $dt2 !=null){
                ByBatch::updateOrCreate(
                   ['group_id' => $groupId, 'date' => $dt],
                   ['services' => json_encode($arr) ,'transactions' => json_encode($arr2), 'date2' => $dt2]
                );
                //ByBatch::where('group_id', $groupId)->where('date',$dt)->updateOrCreate([$tlist]);
            }
        }
    }

    public function user(){
    	return $this->belongsTo('App\Models\User','client_id');
    }

    public function group(){
    	return $this->belongsTo('App\Models\Visa\Group','group_id');
    }

    public function service() {
        return $this->belongsTo('App\Models\Visa\ClientService', 'service_id');
    }

}
