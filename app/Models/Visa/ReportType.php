<?php

namespace App\Models\Visa;

use App\Models\Visa\Service;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class ReportType extends Model
{

    use SoftDeletes;

    protected $connection = 'visa';

	protected $table = 'report_types';

    protected $fillable = ['title', 'title_cn', 'description', 'services_id', 'with_docs'];

    public $timestamps = false;

    protected $dates = ['deleted_at'];

    protected $appends = ['services_name'];

    public function getServicesNameAttribute() {
 		$servicesIdArray = explode(",", $this->services_id);
    	
    	return Service::whereIn('id', $servicesIdArray)->pluck('detail');
    }

}
