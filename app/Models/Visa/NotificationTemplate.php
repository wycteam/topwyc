<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class NotificationTemplate extends Model
{

    use SoftDeletes;

    protected $connection = 'visa';

	protected $table = 'notification_templates';

    protected $fillable = ['name', 'report_type_id', 'docs_id'];

    public $timestamps = false;

    protected $dates = ['deleted_at'];

}
