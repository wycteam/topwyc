<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

use App\Http\Controllers\Cpanel\SynchronizationController;
use Carbon\Carbon;

class Package extends Model
{
    protected $connection = 'visa';
	protected $table = 'packages';
    protected $fillable = [
		 'client_id'
		,'group_id'
		,'log_date'
		,'tracking'
        ,'status'
    ];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            SynchronizationController::insertUpdate($model->client_id, Carbon::now()->toDateTimeString());
        });

        static::updating(function($model)
        {
            SynchronizationController::insertUpdate($model->client_id, Carbon::now()->toDateTimeString());
        });
    }

    public function client() {
        return $this->belongsTo('App\Models\User', 'client_id', 'id');
    }

    public function group() {
        return $this->belongsTo('App\Models\Visa\Group', 'group_id', 'id');
    }

    public function service()
    {
        return $this->hasMany(ClientService::class, 'tracking', 'tracking');
    }

    public function active_services() {
        return $this->service()->where('active', 1);
    }
}
