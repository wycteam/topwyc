<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon ;

class Branch extends Model
{
    protected $connection = 'visa';
	protected $table = 'branches';
    protected $fillable = [
         'id',
		 'name'
    ];

    public $timestamps = false;
}
