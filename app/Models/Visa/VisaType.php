<?php

namespace App\Models\Visa;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class VisaType extends Model
{
  protected $connection = 'mysql';
  protected $table = "user_visa_linker";
  protected $fillable = ['user_id','visa_id', 'exp_date', 'icard_exp_date', 'issue_date', 'first_exp_date', 'arrival_date'];
  public $timestamps = false;

  public $appends = ['three_days'];

  public function user() {
  	return $this->belongsTo('App\Models\User', 'user_id', 'id');
  }

  public function visa() {
  	return $this->belongsTo('App\Models\Visa\UserVisaType', 'visa_id', 'visa_id');
  }

  private function visibilty($date) {
  	$expirationDate = Carbon::parse($date);
	$today = Carbon::today();

	$difference = $today->diffInDays($expirationDate, false);

	if( $difference <=30 && ($difference % 3 == 0 || $difference < 0) ) return true;

	return false;
  }

  public function getThreeDaysAttribute() {
  	$ifShow = false;

  	// For 9g and TRV
	if($this->visa_id == 2 || $this->visa_id == 3) {
		// icard_exp_date
		if($this->icard_exp_date)
	  		if( $this->visibilty($this->icard_exp_date) ) $ifShow = true;

		// exp_date
		if($this->exp_date)
	  		if( $this->visibilty($this->exp_date) ) $ifShow = true;
	}

  	// For CWV
  	if($this->visa_id == 4) {
  		// exp_date
  		if($this->exp_date)
	  		if( $this->visibilty($this->exp_date) ) $ifShow = true;
  	}

  	return $ifShow;
  }

}
