<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\UserOpen;
use App\Models\ShopRate;
use App\Models\Visa\ByBatch;
use App\Models\Visa\Group;
use App\Models\Visa\GroupMember;
use App\Models\Visa\Service;
use App\Models\Visa\Package;
use App\Models\Visa\ClientTransactions;
use App\Models\Visa\CommissionLogs;
use App\Models\Visa\TransactionLogs;
use App\Http\Controllers\Cpanel\SynchronizationController;
use App\Http\Controllers\Cpanel\ClientsController;
use Carbon\Carbon;
use DB;
use App\Classes\Common;


class ClientService extends Model
{
  protected $connection = 'visa';
  protected $table = 'client_services';
  protected $fillable = [
		 'client_id'
        ,'service_id'
        ,'detail'
        ,'cost'
        ,'charge'
        ,'tip'
		,'status'
		,'service_date'
        ,'remarks'
        ,'group_id'
        ,'tracking'
        ,'temp_tracking'
        ,'active'
        ,'extend'
        ,'rcv_docs'
        ,'rcv_docs_released'
        ,'report_docs'
        ,'released'
        ,'commissionable_amt'
        ,'com_middleman'
        ,'com_agent'
        ,'com_client'
        ,'client_com_id'
        ,'agent_com_id'
        ,'completed_at'
        ,'created_at'
        ,'updated_at'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            SynchronizationController::insertUpdate($model->client_id, Carbon::now()->toDateTimeString());
        });

        static::created(function ($model) {
            static::updateTotalServiceCost($model->client_id, $model->group_id);
            static::updateBalance($model->client_id, $model->group_id, $model->id, $model->service_date);
            //static::updateCommissions($model->id, $model->client_id, $model->service_id, $model->status, $model->active);
            $grp = Group::where('id', $model->group_id)->first();
            if($grp){
                $client_com_id = 0;
                $agent_com_id = 0;
                if($grp->client_com_id!='' && $grp->client_com_id>0){
                    $client_com_id = $grp->client_com_id;
                }
                if($grp->agent_com_id!='' && $grp->agent_com_id>0){
                    $agent_com_id = $grp->agent_com_id;
                }
                DB::connection('visa')->table('client_services')->where('id', $model->id)->update([
                    'agent_com_id' => $agent_com_id,
                    'client_com_id' => $client_com_id
                ]);
            }
        });

        static::updating(function($model)
        {
            SynchronizationController::insertUpdate($model->client_id, Carbon::now()->toDateTimeString());
            //static::updateBalance($model->client_id, $model->group_id, $model->id, $model->service_date);
        });

        static::updated(function ($model) {
            $original = $model->getOriginal();
            
            if ($model->group_id != $original['group_id']) {
                static::updateBalance($model->client_id, $original['group_id'], $model->id, $model->service_date);
            }

            //if user change service status from complete to on process or pending disable commission from that service
            if(($original['status'] == 'complete' && $original['status'] != $model->status) || ($original['active'] != $model->active && $model->active == 0)){
               $check =  CommissionLogs::where('service_id', $model->id)->where('client_id',$model->client_com_id)->first();
                if($check){
                    $check->display = 1;
                    $check->save();
                    $commissionClient = ClientTransactions::where('client_id',$model->client_com_id)->where('tracking', $check->id)->where('type','Deposit')->where('is_commission',1)->first();
                    if($commissionClient){
                        // Delete from client_transactions
                        $commissionClient->forceDelete();
                        TransactionLogs::where('transaction_id',$commissionClient->id)->where('amount',$commissionClient->amount)->delete();
                    }
                }

                 $check =  CommissionLogs::where('service_id', $model->id)->where('client_id',$model->agent_com_id)->first();
                if($check){
                    $check->display = 1;
                    $check->save();
                    $commissionAgent = ClientTransactions::where('client_id',$model->agent_com_id)->where('tracking', $check->id)->where('type','Deposit')->where('is_commission',1)->first();
                    if($commissionAgent){
                        // Delete from client_transactions
                        $commissionAgent->forceDelete();
                        TransactionLogs::where('transaction_id',$commissionAgent->id)->where('amount',$commissionAgent->amount)->delete();
                    }
                }
            }

            //if user change service status from on process or pending to complete create commission to client and agent
            if($model->status=='complete' && $original['status'] != $model->status && $model->active == 1 && $model->group_id > 0){
                
                $user = \Auth::user()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
                date_default_timezone_set("Asia/Manila");
                $date = date('m/d/Y h:i:s A');
                
                $detail_cn = DB::connection('visa')->table('services')->select('detail_cn')->where('id',$model->service_id)->first()->detail_cn;
                $group_name = DB::connection('visa')->table('groups')->select('name')->where('id',$model->group_id)->first()->name;
                

                $d = Carbon::now()->format('Ymd');
                $year =  substr($d,0,4);
                $month =  substr($d,4,2);
                $day =  substr($d,6,2);
                if((int)$day >= 1 && (int)$day <= 10){
                    $day = 10;
                }
                else if((int)$day >= 11 && (int)$day <= 20){
                    $day = 20;
                }
                else{
                    $day = 31;
                }
                //Client Commissions
                if($model->client_com_id != '' && $model->client_com_id>0){
                    $client = User::where('id',$model->client_com_id)->select('first_name','last_name')->first()->full_name;
               
                    $log = new CommissionLogs;
                    $log->client_id = $model->client_com_id;
                    $log->group_id = $model->group_id;
                    $log->service_id = $model->id;
                    $log->tracking = $model->tracking;
                    $log->user_id = $user->id;
                    $log->type = "Client Commission";
                    $log->commission = $model->com_client;
                    $log->detail = "Completed Service ".$model->detail." (".$model->service_date."). Commission is ".$model->com_client." for ".$client.".";
                    $log->detail_cn = "完成的服务 ".$detail_cn." (".$model->service_date."). 对于 ".$client." 佣金是 ".$model->com_client.".";
                    $log->detail_client = "Completed Service ".$model->detail." (".$model->service_date.") from ".$group_name.". Commission received is ".$model->com_client.".";
                    $log->detail_client_cn = "已从 ".$group_name." 完成服务 ".$detail_cn." (".$model->service_date.")。 收到的佣金为 ".$model->com_client."。";
                    $log->log_date =  Carbon::now()->format('F j, Y');
                    $log->groupdate =  $year.$month.$day;
                    $log->display = 0;
                    $log->save();

                    $depo = new ClientTransactions;
                    $depo->client_id = $model->client_com_id;
                    $depo->type = 'Deposit';
                    $depo->group_id = 0;
                    $depo->tracking = $log->id;
                    $depo->log_date = $date;
                    $depo->amount = $model->com_client;
                    $depo->is_commission = 1;
                    $depo->save();

                    //save transaction logs
                    $notes = 'Received commission Php'.$model->com_client.' from group '.$group_name.'.';
                    $notes_cn = $notes;
                    $total_balance = ClientsController::clientCollectable($model->client_com_id);
                    $log_data = array(
                        'client_id' => $model->client_com_id,
                        'service_id' => 0,
                        'group_id' => 0,
                        'type' => 'deposit',
                        'amount' => $model->com_client,
                        'transaction_id' => $depo->id,
                        'balance' => $total_balance,
                        'detail'=> $notes,
                        'detail_cn'=> $notes_cn,
                        'commission_ids'=> $log->id,
                    );
                    Common::saveTransactionLogs($log_data);
                    static::combineComLogs($model->client_com_id, $year.$month.$day);
                }

                //Agent Commissions
                if($model->agent_com_id != '' && $model->agent_com_id>0){
                    $agent = User::where('id',$model->agent_com_id)->select('first_name','last_name')->first()->full_name;


                    $log = new CommissionLogs;
                    $log->client_id = $model->agent_com_id;
                    $log->group_id = $model->group_id;
                    $log->service_id = $model->id;
                    $log->tracking = $model->tracking;
                    $log->user_id = $user->id;
                    $log->type = "Agent Commission";
                    $log->commission = $model->com_agent;
                    $log->detail = "Completed Service ".$model->detail." (".$model->service_date."). Commission is ".$model->com_agent." for ".$agent.".";
                    $log->detail_cn =  "完成的服务 ".$detail_cn." (".$model->service_date."). 对于 ".$agent." 佣金是 ".$model->com_agent.".";
                    $log->detail_client = "Completed Service ".$model->detail." (".$model->service_date.") from ".$group_name.". Commission received is ".$model->com_agent.".";
                    $log->detail_client_cn = "已从 ".$group_name." 完成服务 ".$detail_cn." (".$model->service_date.")。 收到的佣金为 ".$model->com_agent."。";
                    $log->log_date =  Carbon::now()->format('F j, Y');
                    $log->groupdate =  $year.$month.$day;
                    $log->display = 0;
                    $log->save();

                    $depo = new ClientTransactions;
                    $depo->client_id = $model->agent_com_id;
                    $depo->type = 'Deposit';
                    $depo->group_id = 0;
                    $depo->tracking = $log->id;
                    $depo->log_date = $date;
                    $depo->amount = $model->com_agent;
                    $depo->is_commission = 1;
                    $depo->save();

                    //save transaction logs
                    $notes = 'Received commission Php'.$model->com_agent.' from group '.$group_name.'.';
                    $notes_cn = $notes;
                    $total_balance = ClientsController::clientCollectable($model->agent_com_id);
                    $log_data = array(
                        'client_id' => $model->agent_com_id,
                        'service_id' => 0,
                        'group_id' => 0,
                        'type' => 'deposit',
                        'amount' => $model->com_agent,
                        'transaction_id' => $depo->id,
                        'balance' => $total_balance,
                        'detail'=> $notes,
                        'detail_cn'=> $notes_cn,
                        'commission_ids'=> $log->id,
                    );
                    Common::saveTransactionLogs($log_data);
                    static::combineComLogs($model->agent_com_id, $year.$month.$day);
                }
            }
            //\Log::info($model->status);
            //\Log::info($original['status']);
            //\Log::info($model->completed_at);
            if($model->status=='complete' && $original['status'] != $model->status && $model->completed_at != null && $model->active == 1 && $model->group_id == 0){

                    $newServiceCost = $model->tip + $model->cost + $model->charge;
                    $user = \Auth::user()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);

                    $log_data = array(
                        'client_id' => $model->client_id,
                        'service_id' => $model->id,
                        'tracking' => $model->tracking,
                        'user_id' => $user->id,
                        'type' => 'service',
                        'amount' => $newServiceCost,
                        //'detail'=> $notes,
                        //'detail_cn'=> $notes_cn,
                    );

                    $log_data['id'] = '';
                    $log_data['detail'] = 'Completed Service with total service charge of <b>Php'.$newServiceCost.'</b>.';
                    $log_data['detail_cn'] = 'Completed Service with total service charge of <b>Php'.$newServiceCost.'</b>.';
                    $log_data['amount'] = '-'.$newServiceCost; 
                    $update_log = TransactionLogs::where('service_id',$model->id)->where('type','service')->first();
                    if($update_log){
                        $log_data['old_detail'] = $update_log->detail.'<br><small>'.$user->first_name.' - '.$update_log->log_date.'</small><br><br>'.$update_log->old_detail;
                        $log_data['old_detail_cn'] = $update_log->detail_cn.'<br><small>'.$user->first_name.' - '.$update_log->log_date.'</small><br><br>'.$update_log->old_detail_cn;
                        $update_log->delete();
                    }
            //\Log::info($log_data);

                    Common::saveTransactionLogs($log_data);
            }

            static::updateTotalServiceCost($model->client_id, $model->group_id);
            static::updateBalance($model->client_id, $model->group_id, $model->id, $model->service_date);
            //static::updateCommissions($model->id, $model->client_id, $model->service_id, $model->status, $model->active, $model->charge, $model->service_date);
        });
    }

    protected $appends = [
        'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id'
    ];

    private static function combineComLogs($clientId, $groupdate) {
        $countClient = CommissionLogs::where('client_id',$clientId)->where('included',0)->where('display',0)
                        //->where('groupdate',$groupdate)
                        ->get();
            //\Log::info('client #:'.$clientId);
            //\Log::info($countClient->count());
            if($countClient->count() == 50){
                $total = 0;
                $ids = '';
                $old_detail = '';
                $old_detail_cn = '';

                // $checkIfGroupedAlready = CommissionLogs::where('client_id',$clientId)->where('included',1)->where('groupdate',$groupdate)->first();
                // if($checkIfGroupedAlready){
                //      $trans = TransactionLogs::where('client_id',$clientId)->where('commission_ids','LIKE','%'.$checkIfGroupedAlready->id.'%')->where('commission_ids','LIKE','%,%')->first();
                //      if($trans){

                //             $total = $trans->amount;
                //             $ids = $trans->commission_ids.',';
                //             $old_detail = $trans->old_detail;
                //             $old_detail_cn = $trans->old_detail_cn;
                //         }
                // }

                foreach($countClient as $cc){
                    $total += $cc->commission;
                    $ids .= $cc->id.',';
                    $checkTrans = TransactionLogs::where('commission_ids',$cc->id)->first();
                    if($checkTrans){
                        $old_detail .= '** '.$checkTrans->detail.'<br>'.$checkTrans->old_detail.'<br>';
                        $old_detail_cn .= '** '.$checkTrans->detail_cn.'<br>'.$checkTrans->old_detail_cn.'<br>';
                        $checkTrans->display = 1;
                        $checkTrans->save();
                    }
                    $comi = CommissionLogs::where('id',$cc->id)->first();
                    $comi->included = 1;
                    $comi->save();
                }

                $collect_ids = rtrim($ids, ', ');
                $notes = 'Total commission received -> Php'.$total.'.';
                $notes_cn = $notes;
                //$total_balance = ClientsController::clientCollectable($model->client_com_id);
                // if($checkIfGroupedAlready){
                //     $trans->amount = $total;
                //     $trans->detail = $notes;
                //     $trans->old_detail = $old_detail;
                //     $trans->detail_cn = $notes_cn;
                //     $trans->old_detail_cn = $old_detail_cn;
                //     $trans->commission_ids = $collect_ids;
                //     $trans->save();
                // }
                // else{                
                    $log_data = array(
                            'client_id' => $clientId,
                            'service_id' => 0,
                            'group_id' => 0,
                            'type' => 'deposit',
                            'amount' => $total,
                            'transaction_id' => null,
                            'balance' => 0,
                            'detail'=> $notes,
                            'old_detail'=> $old_detail,
                            'detail_cn'=> $notes_cn,
                            'old_detail_cn'=> $old_detail_cn,
                            'commission_ids'=> $collect_ids,
                        );
                    Common::saveTransactionLogs($log_data);
                //}

            }
        
    }

    private static function updateTotalServiceCost($clientId, $groupId) {
        $totalServiceCost = \DB::connection('visa')
            ->table('client_services')
            ->where('client_id', $clientId)
            ->where('group_id', $groupId)
            ->where('active', 1)
            ->value(\DB::raw("SUM(cost + charge + tip + com_client + com_agent )"));

        $clientServices = ClientService::has('documentLogs')
            ->with(['documentLogs' => function($query) {
                $query->orderBy('id', 'desc');
            }])
            ->where('active', 1)
            ->where('client_id', $clientId)
            ->get()
            ->makeHidden([
                'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
            ]);

        $documentsOnHand = [];
        $arr = [];

        foreach($clientServices as $clientService) {
            if( $clientService->documentLogs[0]->documents_on_hand  ) {
                $documentsOnHand[] = $clientService->documentLogs[0]->documents_on_hand;
            }
        }

        $docsIdArr = explode(',', implode(',', $documentsOnHand));

        $unqdocs =  DB::connection('visa')->table('service_documents')->where('is_unique', 1)->whereIn('id', $docsIdArr)->pluck('id');

        $usr =  User::where('id',$clientId)->select('id','first_name','last_name')->limit(1)->get()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
            if($usr){
                $arr = $usr[0];
                $arr['packages'] = Package::where('client_id',$clientId)->where('group_id',$groupId)->get();
                $arr['uniqueDocumentsOnHand'] = $unqdocs;
            }

        \DB::connection('visa')
            ->table('group_members')
            ->where('client_id', $clientId)
            ->where('group_id', $groupId)
            ->update(['total_service_cost' => $totalServiceCost , 'client' => json_encode($arr)]);
    }

    private static function updateBalance($clientId, $groupId, $sid, $service_date) {
            //\Log::info($groupId);

        if($groupId == 0){
            $balance = ClientsController::clientBal($clientId);
            $col = ClientsController::clientCollectable($clientId);
            $temp_balance = ClientsController::clientBalDec1($clientId);
            User::where('id', $clientId)->update([
                'balance' => $balance,'collectable' => $col,
                // 'temp_balance' => $temp_balance
            ]);
        }
        else{
            $balance = ClientsController::groupBal($groupId);
            $col = ClientsController::groupCollectable($groupId);
            $temp_balance = ClientsController::groupBalDec1($groupId);
            Group::where('id', $groupId)->update([
                'balance' => $balance,'collectable' => $col,
                // 'temp_balance' => $temp_balance
            ]);


            //try
                $id = $groupId;
                $byBatch = DB::connection('visa')
                        ->table('client_services as a')
                        ->select(DB::raw('id,detail,service_date, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y") as sdate, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%Y%m%d") as tdate'))
                        ->where('active',1)->where('group_id',$id)
                        ->where('service_date',$service_date)
                        ->orderBy('id','DESC')
                        ->groupBy('sdate')
                        ->get();
                $byBatchDate = $byBatch->pluck('tdate');
                //\Log::info($byBatchDate);


                $transactions = DB::connection('visa')
                    ->table('client_transactions as a')
                    ->select(DB::raw('
                        a.amount,a.type,a.service_id, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%M %d,%Y") as log_date'))
                        ->where('group_id', $id)
                        ->where(function($query) {
                            $query->where('service_id', null)
                                ->orWhere('service_id', 0);
                        })
                        ->where('log_date','LIKE', '%'.$service_date.'%')
                        ->orderBy('tdate','DESC')
                        ->get();

                $transDate = $transactions->pluck('tdate');

                $merged = $byBatchDate->merge($transDate);

                $result = $merged->all();
                $tdates = array_unique($result);
                rsort($tdates);
                //\Log::info($tdates);
                $groupcost = ClientsController::groupCollectable2($groupId);
                $lastcost = 0;
                $tlist = [];
                $datectr = 0;
                $dt = null;
                $dt2 = null;
                $arr = [];
                $arr2 = [];
                $countDate = count($tdates);

                if($countDate < 1){
                    DB::connection('visa')->table('by_batch')->where('group_id', $id)->where('date2',Carbon::parse($service_date)->format('F d,Y'))->delete();
                }

                foreach($tdates as $d){
                    $y =  substr($d,0,4);
                    $m =  substr($d,4,2);
                    $day =  substr($d,6,2);
                    $nDate = $m.'/'.$day.'/'.$y;

                    $dt =   $d;
                    $dt2 =   Carbon::parse($nDate)->format('F d,Y');

                    $query = DB::connection('visa')
                        ->table('client_services')->where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$nDate)->where('group_id', $id)->where('active', 1)->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('client_id')->get();
                        $ctr2 = 0;
                        //$servTotal = 0;
                        $client = [];
                        $arr = [];
                        $flag = true;
                        $client_ctr = 0;
                        foreach($query as $m){
                            $ss =  ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$nDate)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->with('discount2')->get();
                            if(count($ss) > 0){
                                $client = User::where('id',$m->client_id)->select('first_name','last_name')->first()->full_name;
                                $totalcost = ClientService::where('service_id',$id)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
                                $services = $ss;

                                $arr[$client_ctr]['name'] = $client;

                                $servctr = 0;
                                foreach($services as $serv){
                                    $tcost = $serv->cost + $serv->tip + $serv->charge + $serv->com_client + $serv->com_agent;
                                    if($serv->status != 'complete'){
                                        $tcost = 0;
                                    }

                                    if(strpos($serv->detail, "9A") === false && $serv->status != 'complete'){
                                        $tcost = $serv->cost + $serv->tip + $serv->charge + $serv->com_client + $serv->com_agent;
                                    }

                                    $arr[$client_ctr]['services'][$servctr]['tracking'] = $serv->tracking;
                                    $arr[$client_ctr]['services'][$servctr]['detail'] = $serv->detail;
                                    $arr[$client_ctr]['services'][$servctr]['total_cost'] = $tcost;
                                    $arr[$client_ctr]['services'][$servctr]['service_id'] = $serv->id;

                                    $sp = $serv->status;
                                    if($serv->active==0){
                                        $sp = "CANCELLED";
                                    }
                                    $arr[$client_ctr]['services'][$servctr]['status'] = $sp;

                                    // if($flag= false){
                                    //     $tempTotal = $groupcost - $lastcost;
                                    // }
                                    // else{
                                    //     $tempTotal = $groupcost + $lastcost;
                                    // }

                                    $translated = Service::where('id',$serv->service_id)->first();
                                    $cnserv =$serv->detail;
                                    if($translated){
                                        $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                                    }
                                    $arr[$client_ctr]['services'][$servctr]['detail_cn'] = $cnserv;
                                    //$arr[$client_ctr]['services'][$servctr]['tempTotal'] = $tempTotal;


                                    //$lastcost = $tcost;
                                    //$groupcost = $tempTotal;

                                    if($serv->discount_amount>0){
                                        $servctr++;
                                        //$tempTotal = $groupcost + $lastcost;

                                        $arr[$client_ctr]['services'][$servctr]['tracking'] = '';
                                        $arr[$client_ctr]['services'][$servctr]['detail'] = 'Discount';
                                        $arr[$client_ctr]['services'][$servctr]['detail_cn'] = '折扣';
                                        $arr[$client_ctr]['services'][$servctr]['total_cost'] = $serv->discount_amount;
                                        $arr[$client_ctr]['services'][$servctr]['service_id'] = '';
                                        $arr[$client_ctr]['services'][$servctr]['status'] = '';
                                        //$arr[$client_ctr]['services'][$servctr]['tempTotal'] = $tempTotal;


                                        //$lastcost = -1 *$serv->discount_amount;
                                        //$groupcost = $tempTotal;

                                    }

                                    //$row++;
                                    $flag = false;
                                    $servctr++;
                                }
                            }
                            $client_ctr++;

                            //$row++;
                            $ctr2++;
                        }

                        $flag = false;
                        $trans_ctr = 0;
                        $arr2 = [];
                        foreach($transactions as $t){
                            if($t->tdate == $d){
                                if($t->type != 'Discount' || ($t->type == 'Discount' && ($t->service_id == null || $t->service_id == 0))){
                                    $dtype = '';
                                    // $sign = ' +';
                                    // if($flag= false){
                                    //     $tempTotal = $groupcost - $lastcost;
                                    // }
                                    // else{
                                    //     $tempTotal = $groupcost + $lastcost;
                                    // }


                                    if($t->type == 'Deposit'){
                                        $dtype = '预存款';

                                        //$lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }
                                    if($t->type == 'Discount'){
                                        $dtype = '折扣';
                                        //$lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }
                                    if($t->type == 'Payment'){
                                        $dtype = '已付款';
                                        //$lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }
                                    if($t->type == 'Refund'){
                                        $dtype = '退款';
                                        //$lastcost = $t->amount;
                                        //$sign = '-';
                                        //$tempTotal = $groupcost + (-1*$lastcost);
                                    }

                                    $arr2[$trans_ctr]['detail'] = $t->type;
                                    $arr2[$trans_ctr]['detail_cn'] = $dtype;
                                    $arr2[$trans_ctr]['total_cost'] = $t->amount;
                                    //$arr2[$trans_ctr]['tempTotal'] = $tempTotal;
                                    }
                                    //$groupcost = $tempTotal;

                                    //$flag = true;
                                    $trans_ctr++;

                            }
                        }
                    $datectr++;

                }
            //\Log::info($arr);
            //\Log::info($arr2);
            //\Log::info(!empty($tlist));
            if($dt != null || $dt2 !=null){
                ByBatch::updateOrCreate(
                   ['group_id' => $groupId, 'date' => $dt],
                   ['services' => json_encode($arr) ,'transactions' => json_encode($arr2), 'date2' => $dt2]
                );
                //ByBatch::where('group_id', $groupId)->where('date',$dt)->updateOrCreate([$tlist]);
            }

    }
}

    private static function updateCommissions($id, $client_id, $service_id, $status,$active, $charge, $service_date) {
        if($status == 'complete'){
            $client = 0.1; $mid = 0; $agent = 0;
            $referred = UserOpen::where('client_id',$client_id)->first();
            if($referred){
                $service = Service::where('id', $service_id)->first();
                $d = explode("/",$service_date);
                $rate = ShopRate::where('client_id',$referred->referral_id)->where('month',$d[0])->where('year',$d[2])->first();
                $ref = UserOpen::where('referral_id',$referred->referral_id)->groupBy('client_id')->pluck('client_id');
                $cs = DB::connection('visa')->table('client_services')->select(DB::raw('id, cost,charge,tip,
                    date_format(STR_TO_DATE(service_date, "%m/%d/%Y"),"%Y%m")  as servdates'))
                        ->whereIn('client_id',$ref)->where('status','complete')->where('detail','!=','Other Payment')->where('active',1)
                        ->orderBy('id','asc')->get();
                //\Log::info($cs);
                $cur = $cs->filter(function ($c) use($rate) {
                    return $c->servdates == $rate->year.$rate->month;
                });
                $curIds = $cur->pluck('id');

                $total = DB::connection('visa')->table('client_services')->select(DB::raw('sum(cost+charge+tip) as cashflow'))
                        ->whereIn('id',$curIds)->get();
                $total_cashflow = $total[0]->cashflow;

                if($total_cashflow <= 1000000){
                    $agent = 0.4;
                    $mid = 0.4;
                }
                else if($total_cashflow > 1000000 && $total_cashflow <= 1500000){
                    $agent = 0.45;
                    $mid = 0.35;
                }
                else if($total_cashflow > 1500000 && $total_cashflow <= 2000000){
                    $agent = 0.5;
                    $mid = 0.3;
                }
                else{
                    $agent = 0.55;
                    $mid = 0.25;
                }

                if($rate->rate < $agent){
                    $rate->rate = $agent;
                    $rate->save();

                    $updateRate = ShopRate::where('client_id',$referred->referral_id)->where('id','>',$rate->id)->get();
                    foreach($updateRate as $r){
                        if($r->rate < $agent){
                            $getRate = ShopRate::find($r->id);
                            $getRate->rate = $agent;
                            $getRate->save();
                        }
                    }

                    DB::connection('visa')->table('client_services')->whereIn('id', $curIds)
                        ->update([
                            'com_agent' => DB::raw("`commissionable_amt`*".($agent)),
                            'com_middleman' => DB::raw("`commissionable_amt`*".($mid)),
                            'com_client' => DB::raw("`commissionable_amt`*".($client))
                        ]);
                }
                else{
                    $prevMaxRate = ShopRate::where('client_id',$referred->referral_id)->where('id','<',$rate->id)->max('rate');
                    if($prevMaxRate <= $agent){
                        $rate->rate = $agent;
                        $rate->save();

                        DB::connection('visa')->table('client_services')->whereIn('id', $curIds)
                        ->update([
                            'com_agent' => DB::raw("`commissionable_amt`*".($agent)),
                            'com_middleman' => DB::raw("`commissionable_amt`*".($mid)),
                            'com_client' => DB::raw("`commissionable_amt`*".($client))
                        ]);
                    }
                }

                if($service && $active == 1){
                    $com_amt = $charge * 0.4;
                    $client = $com_amt * $client;
                    $agent = $com_amt * $agent;
                    $mid = $com_amt * $mid;
                    DB::connection('visa')->table('client_services')->where('id', $id)->update([
                        'commissionable_amt' => $com_amt,
                        'com_agent' => $agent,
                        'com_middleman' => $mid,
                        'com_client' => $client
                    ]);
                }
                else{
                    DB::connection('visa')->table('client_services')->where('id', $id)->update([
                        'com_agent' => null,
                        'com_middleman' => null,
                        'com_client' => null
                    ]);
                }
            }
        }
        else{
            DB::connection('visa')->table('client_services')->where('id', $id)->update([
                        'com_agent' => null,
                        'com_middleman' => null,
                        'com_client' => null
                    ]);
        }
    }

    public function getParentIdAttribute() {
        $service = Service::where('id', $this->service_id)->first();

        if($service) {
            return $service->parent_id;
        }

        return null;
    }

    public function getDetailCnAttribute()
    {
        $getServ =  $this->service_category()->first();
        if($getServ){
            return $getServ->detail_cn;
        }
        return '';
    }

    public function getDocsNeededAttribute()
    {
        $getServ =  $this->service_category()->first();
        if($getServ){
            return $getServ->docs_needed;
        }
        return '';
    }

    public function getDocsOptionalAttribute()
    {
        $getServ =  $this->service_category()->first();
        if($getServ){
            return $getServ->docs_optional;
        }
        return '';
    }

    public function getDiscountAmountAttribute() {
        $discount = ClientTransactions::where('type', 'Discount')->where('service_id', $this->id)->first();

        if($discount) {
            return $discount->amount;
        }

        return 0;
    }

    public function getGroupBindedAttribute()
    {
        $groups =  GroupMember::where('client_id',$this->client_id)->where('group_id',$this->group_id)->pluck('group_id');
        //\Log::info($groups);
        $leaders = Group::whereIn('id',$groups)->get();
        //$leads = [];
        $grps = '';
        //$ctr = 0;
        foreach($leaders as $l){
            $g = GroupMember::where('group_id',$l->id)->where('leader','>',0)->pluck('leader');
            $binded = User::whereIn('id',$g)->where('password','!=',null)->count();
            if($binded > 0){
                $grps =  $l->name.', ';
            }
            // $leads[$ctr]['Group'] = $l->name;
            // $leads[$ctr]['leader_binded'] = $binded;
            // $ctr++;
        }
        if($grps!=''){
            $grps = "Leader Binded - Group : ".$grps;
        }
        return $grps;
    }

    public function user(){
    	return $this->belongsTo('App\Models\User','client_id');
    }

    public function group(){
    	return $this->belongsTo('App\Models\Visa\Group','group_id');
    }

    public function discount() {
        return $this->hasOne('App\Models\Visa\ClientTransactions', 'service_id', 'id')->where('type','Discount');
    }

    public function discount2() {
        return $this->hasMany('App\Models\Visa\ClientTransactions', 'service_id', 'id')->where('type','Discount');
    }

    public function report() {
        return $this->hasMany('App\Models\Visa\Report', 'service_id', 'id');
    }

    public function service_category() {
        return $this->belongsTo('App\Models\Visa\Service', 'service_id');
    }

    public function tasks() {
        return $this->hasMany('App\Models\Visa\Task', 'client_service_id', 'id');
    }

    public function discounts() {
        return $this->hasMany('App\Models\Visa\ClientTransactions', 'service_id', 'id')->where('type', '=', 'Discount');
    }

    public function documentLogs() {
        return $this->hasMany('App\Models\Visa\DocumentLogs', 'client_service_id', 'id');
    }

    public function serviceDocuments() {
        return $this->belongsToMany('App\Models\Visa\ServiceDocuments', 'client_service_document', 'client_service_id', 'document_id');
    }

    // public function package() {
    //     return $this->belongsTo('App\Models\Visa\Package', 'tracking');
    // }
}
