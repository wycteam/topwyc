<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class Doc extends Model
{

    use SoftDeletes;

    protected $connection = 'visa';

	protected $table = 'docs';

    protected $fillable = ['title', 'title_cn', 'parent_id'];

    public $timestamps = false;

    protected $dates = ['deleted_at'];

    protected $appends = ['doc_types'];

    public function getDocTypesAttribute() {
    	return \DB::connection('visa')->table('docs')->where('parent_id', $this->id)->where('deleted_at', NULL)->get();
    }

}
