<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class ServiceBranchCost extends Model
{
    protected $connection = 'visa';
	protected $table = 'service_branch_cost';
    protected $fillable = [
		 'service_id'
		,'branch_id'
		,'cost'
        ,'charge'
        ,'tip'
        ,'com_agent'
        ,'com_client'
    ];

    public $timestamps = false;

    public static function boot() {
        parent::boot();

        static::created(function($serviceBranchCost) {
            $query = \DB::connection('visa')->table('service_branch_cost')->where('service_id', $serviceBranchCost->service_id)->where('branch_id', $serviceBranchCost->branch_id)->get();

            if(count($query) > 1)
                \DB::connection('visa')->table('service_branch_cost')->where('id', $query[1]->id)->delete();
        });
    }

    public function branch() {
        return $this->belongsTo('App\Models\Visa\Branch', 'branch_id', 'id');
    }

}
