<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class ClientServiceDocument extends Model
{

	protected $connection = 'visa';

  	protected $table = 'client_service_document';

  	protected $fillable = ['client_service_id', 'document_id', 'created_at'];

  	public function clientService() {
  		return $this->belongsTo('App\Models\Visa\ClientService', 'client_service_id', 'id');
  	}

  	public function serviceDocument() {
  		return $this->belongsTo('App\Models\Visa\ServiceDocuments', 'document_id', 'id');
  	}

}