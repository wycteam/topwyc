<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class UserVisaType extends Model
{

	protected $connection = 'mysql';

	protected $table = 'user_visa_type';

	public $timestamps = false;

	protected $primaryKey = 'visa_id';
	
}