<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class CommentVote extends Model
{
    protected $connection = 'visa';
    protected $table = "comment_user_vote";
    protected $fillable = ['comment_id','user_id','vote'];

    public $timestamps = false;
}
