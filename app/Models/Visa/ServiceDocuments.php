<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class ServiceDocuments extends Model
{

    protected $connection = 'visa';

	protected $table = 'service_documents';

    protected $fillable = ['title', 'title_cn', 'is_unique'];

    public $timestamps = false;

    public function clientServices() {
        return $this->belongsToMany('App\Models\Visa\ClientService', 'client_service_document', 'document_id', 'client_service_id');
    }
    
}
