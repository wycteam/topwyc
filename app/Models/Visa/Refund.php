<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

use App\Http\Controllers\Cpanel\SynchronizationController;
use Carbon\Carbon;

class Refund extends Model
{
    protected $connection = 'visa';
	protected $table = 'client_refunds';
    protected $fillable = [
		 'client_id'
		,'refund_amount'
		,'group_id'
        ,'reason'
		,'tracking'
		,'log_date'
    ];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            SynchronizationController::insertUpdate($model->client_id, Carbon::now()->toDateTimeString());
        });

        static::updating(function($model)
        {
            SynchronizationController::insertUpdate($model->client_id, Carbon::now()->toDateTimeString());
        });
    }
    
    public function user(){
    	return $this->belongsTo('App\Models\User','client_id');
    }

    public function group(){
    	return $this->belongsTo('App\Models\Visa\Group','group_id');
    }

}
