<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

use App\Models\Visa\ClientService;
use App\Models\Visa\ServiceDocuments;
use App\Models\User;
use Auth, Carbon\Carbon, DB;

class DocumentLogs extends Model
{

    protected $connection = 'visa';

    protected $table = 'documents_logs';

    protected $fillable = [
        'client_service_id', 'documents', 'documents_on_hand', 'processor_id', 'type', 'detail', 'detail_cn', 'log_date', 'created_at', 'updated_at'
    ];

    protected $appends = ['documents_string', 'documents_on_hand_string', 'documents_is_unique'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            $cs = ClientService::find($model->client_service_id);
            static::updateClientField($cs->client_id, $cs->group_id);
        });

        static::created(function ($model) {
            $cs = ClientService::find($model->client_service_id);
            static::updateClientField($cs->client_id, $cs->group_id);
        });

    }

    private static function updateClientField($clientId, $groupId) {
        $clientServices = ClientService::has('documentLogs')
            ->with(['documentLogs' => function($query) {
                $query->orderBy('id', 'desc');
            }])
            ->where('active', 1)
            ->where('client_id', $clientId)
            ->get()
            ->makeHidden([
                'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
            ]);

        $documentsOnHand = [];
        $arr = [];

        foreach($clientServices as $clientService) {
            if( $clientService->documentLogs[0]->documents_on_hand  ) {
                $documentsOnHand[] = $clientService->documentLogs[0]->documents_on_hand;
            }
        }

        $docsIdArr = explode(',', implode(',', $documentsOnHand));

        $unqdocs =  DB::connection('visa')->table('service_documents')->where('is_unique', 1)->whereIn('id', $docsIdArr)->pluck('id');

        $usr =  User::where('id',$clientId)->select('id','first_name','last_name')->limit(1)->get()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
            if($usr){
                $arr = $usr[0];
                $arr['packages'] = Package::where('client_id',$clientId)->where('group_id',$groupId)->get();
                $arr['uniqueDocumentsOnHand'] = $unqdocs;
            }

        \DB::connection('visa')
            ->table('group_members')
            ->where('client_id', $clientId)
            ->where('group_id', $groupId)
            ->update(['client' => json_encode($arr)]);
    }

    public static function autoUpdateDocuments($type, $clientId, $documents, $exceptClientServiceId) {
        if($type == 'Received') {
            $clientServices = ClientService::has('documentLogs')
                ->with(['documentLogs' => function($query) {
                    $query->orderBy('id', 'desc');
                }])
                ->where('active', 1)
                ->where('client_id', $clientId)
                ->where(function($query) {
                    $query->where('status', 'pending')->orWhere('status', 'on process');
                })
                ->get()
                ->makeHidden([
                    'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id'
                ]);
            
            for($i=0; $i<count($clientServices); $i++) {
                $documentsOnHandArray = ($clientServices[$i]->documentLogs[0]->documents_on_hand) 
                    ? explode(',', $clientServices[$i]->documentLogs[0]->documents_on_hand)
                    : [];

                if( count($documentsOnHandArray) > 0 ) {
                    for($j=0; $j<count($clientServices); $j++) {
                        $service = DB::connection('visa')->table('services')->where('id', $clientServices[$j]->service_id)->first();
                        $docsNeededArray = ($service->docs_needed) ? explode(',', $service->docs_needed) : [];
                        $docsOptionalArray = ($service->docs_optional) ? explode(',', $service->docs_optional) : [];

                        $uniqueDocumentsArray = DB::connection('visa')->table('service_documents')
                            ->where('is_unique', 1)
                            ->where(function($query) use($docsNeededArray, $docsOptionalArray) {
                                $query->whereIn('id', $docsNeededArray)->orWhereIn('id', $docsOptionalArray);
                            })
                            ->pluck('id');

                        $clientServiceRcvDocs = explode(',', $clientServices[$j]->rcv_docs);

                        $toBeAddedDocuments = [];
                        foreach($uniqueDocumentsArray as $u) {
                            if( in_array($u, $documentsOnHandArray) && !in_array($u, $clientServiceRcvDocs) ) {
                                $toBeAddedDocuments[] = $u;
                            }
                        }

                        if( count($toBeAddedDocuments)  > 0 ) {
                            $clientService = ClientService::findOrFail($clientServices[$j]->id);

                            // Update client_services
                                $clientService->update([
                                    'rcv_docs' => implode(',', array_unique(array_merge(explode(',', $clientService->rcv_docs), $toBeAddedDocuments)))
                                ]);

                            // Insert documents_logs
                                $documents = implode(',', $toBeAddedDocuments);

                                $documentLog = DocumentLogs::where('client_service_id', $clientService->id)->orderBy('id', 'desc')->first();
                                $documentsOnHand = ($documentLog->documents_on_hand) 
                                    ? explode(',', $documentLog->documents_on_hand)
                                    : [];
                                $documentsOnHand = implode(',', array_unique(array_merge($documentsOnHand, $toBeAddedDocuments)));

                                $type = 'Received';
                                $serviceName = $service->detail;
                                $detail = '<strong>Received document/s for service '.$serviceName.':</strong>';
                                $detailCn = '<strong>Received document/s for service '.$serviceName.':</strong>';

                                DocumentLogs::create([
                                    'client_service_id' => $clientService->id,
                                    'documents' => $documents,
                                    'documents_on_hand' => (strlen($documentsOnHand) > 0) ? $documentsOnHand : null,
                                    'processor_id' => Auth::user()->id,
                                    'type' => $type,
                                    'detail' => $detail,
                                    'detail_cn' => $detailCn,
                                    'log_date' => Carbon::now()->format('Y-m-d')
                                ]);

                            // Update client_services
                                $documentsOnHand = explode(',', $documentsOnHand);
                                $result = array_diff($docsNeededArray, $documentsOnHand);
                                if(count($result) == 0) {
                                    ClientService::findOrFail($clientService->id)->update([
                                        'status' => 'on process'
                                    ]);
                                }
                        }
                    }
                }
            }
        } elseif($type == 'Released') {
            $clientServices = ClientService::has('documentLogs')
                ->with(['documentLogs' => function($query) {
                    $query->orderBy('id', 'desc');
                }])
                ->where('active', 1)
                ->where('client_id', $clientId)
                ->where(function($query) {
                    $query->where('status', 'pending')->orWhere('status', 'on process');
                })
                ->where('id', '<>', $exceptClientServiceId)
                ->get()
                ->makeHidden([
                    'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id'
                ]);

            $toBeSubtractedDocuments = explode(',', $documents);
            foreach($clientServices as $clientService) {
                $service = DB::connection('visa')->table('services')->where('id', $clientService->service_id)->first();
                $docsNeededArray = ($service->docs_needed) ? explode(',', $service->docs_needed) : [];
                $docsOptionalArray = ($service->docs_optional) ? explode(',', $service->docs_optional) : [];

                $uniqueDocumentsArray = DB::connection('visa')->table('service_documents')
                    ->where('is_unique', 1)
                    ->where(function($query) use($docsNeededArray, $docsOptionalArray) {
                        $query->whereIn('id', $docsNeededArray)->orWhereIn('id', $docsOptionalArray);
                    })
                    ->pluck('id');

                $clientServiceRcvDocs = explode(',', $clientService->rcv_docs);

                $tempDocuments = [];
                foreach($uniqueDocumentsArray as $u) {
                    if( in_array($u, $toBeSubtractedDocuments) && in_array($u, $clientServiceRcvDocs) ) {
                        $tempDocuments[] = $u;
                    }
                }

                if( count($tempDocuments) > 0 ) {
                    // Update client_services
                        $clientService->update([
                            'rcv_docs' => implode(',', array_unique(array_diff(explode(',', $clientService->rcv_docs), $toBeSubtractedDocuments)))
                        ]);

                    // Insert documents_logs
                        $documents = implode(',', $tempDocuments);

                        $documentLog = DocumentLogs::where('client_service_id', $clientService->id)->orderBy('id', 'desc')->first();
                        $documentsOnHand = ($documentLog->documents_on_hand) 
                            ? explode(',', $documentLog->documents_on_hand)
                            : [];
                        $documentsOnHand = implode(',', array_unique(array_diff($documentsOnHand, $toBeSubtractedDocuments)));

                        $type = 'Released';
                        $serviceName = $service->detail;
                        $detail = '<strong>Released document/s for service '.$serviceName.':</strong>';
                        $detailCn = '<strong>Released document/s for service '.$serviceName.':</strong>';

                        DocumentLogs::create([
                            'client_service_id' => $clientService->id,
                            'documents' => $documents,
                            'documents_on_hand' => (strlen($documentsOnHand) > 0) ? $documentsOnHand : null,
                            'processor_id' => Auth::user()->id,
                            'type' => $type,
                            'detail' => $detail,
                            'detail_cn' => $detailCn,
                            'log_date' => Carbon::now()->format('Y-m-d')
                        ]);

                    // Update client_services
                        $documentsOnHand = explode(',', $documentsOnHand);
                        $result = array_diff($docsNeededArray, $documentsOnHand);
                        if(count($result) > 0) {
                            ClientService::findOrFail($clientService->id)->update([
                                'status' => 'pending'
                            ]);
                        }
                }
            }
        }
    }

    public function clientService() {
        return $this->belongsTo(ClientService::class, 'client_service_id', 'id');
    }

    public function processor() {
        return $this->belongsTo(User::class, 'processor_id', 'id');
    }

    private function documentIdsToDocumentStrings($documentIds) {
        $documentArray = explode(',', $documentIds);

        $serviceDocuments = ServiceDocuments::whereIn('id', $documentArray)->orderBy('is_unique', 'desc')->get();

        $documentStrings = '';
        foreach($serviceDocuments as $s) {
            if(\Request::cookie('locale') == 'en') {
                $documentStrings .= $s->title . ',';
            } else {
                $documentStrings .= $s->title_cn . ',';
            }
        }

        return substr($documentStrings, 0, -1);
    }

    public function getDocumentsStringAttribute() {
        $documents = null;

        if($this->documents) {
            $documents = $this->documentIdsToDocumentStrings($this->documents);
        }

        return $documents;
    }

    public function getDocumentsIsUniqueAttribute() {
        $isUnique = [];

        if( $this->documents ) {
            $documentArray = explode(',', $this->documents);

            $serviceDocuments = ServiceDocuments::whereIn('id', $documentArray)->orderBy('is_unique', 'desc')->get();
            
            foreach($serviceDocuments as $s) {
                $isUnique[] = $s->is_unique;    
            }
        }

        return $isUnique;
    }

    public function getDocumentsOnHandStringAttribute() {
        $documentsOnHand = null;

        if($this->documents_on_hand) {
            $documentsOnHand = $this->documentIdsToDocumentStrings($this->documents_on_hand);
        }

        return $documentsOnHand;
    }
    
}
