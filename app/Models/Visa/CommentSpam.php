<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class CommentSpam extends Model
{
  protected $connection = 'visa';
  protected $table = "comment_spam";
  protected $fillable = ['comment_id','user_id'];
  public $timestamps = false;
}
