<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class DateLogs extends Model
{
  protected $connection = 'visa';
  protected $table = 'date_logs';
  protected $fillable = [
    'client_id',
    'service_id',
    'exp_date',
    'estimated_date',
  ];
}
