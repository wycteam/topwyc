<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;
use App\Models\Visa\Service;

class ServiceProfiles extends Model
{
    protected $connection = 'visa';
	protected $table = 'service_profiles';
    protected $fillable = [
		 'id'
		,'name'
		,'slug'
		,'is_active'
    ];

    public $timestamps = false;

}
