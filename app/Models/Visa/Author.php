<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
  protected $connection = 'visa';
  protected $table = 'news_author';
  protected $primaryKey = 'author_id';
  protected $fillable = [
    'user_id',
    'name',
    'ip_address'
  ];
}
