<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\UserOpen;
use App\Models\Visa\Group;
use App\Models\Visa\ClientService;

use Carbon\Carbon ;
use DB;

class GroupMember extends Model
{
    protected $connection = 'visa';
	protected $table = 'group_members';
    protected $fillable = ['group_id', 'client_id', 'leader', 'total_service_cost','client'];
    
    public $timestamps = false;


    public static function boot()
    {
        parent::boot();

        static::created(function ($gm) {
            static::updateClientField($gm->client_id, $gm->group_id);
            // $group = Group::findOrFail($gm->group_id);
            // $user = User::findOrFail($gm->client_id);
            // if($user){
            //     $leaderAgent = UserOpen::where('referral_id',$group->leader)->first();
            //     if($leaderAgent || $group->is_shop > 0){
            //         // if($user->contact_number == null || $user->contact_number == ''){
            //         //     $user->contact_number = '09209824652';
            //         // }
            //         // if($user->birth_date == null || $user->birth_date == ''){
            //         //     $user->birth_date = '1992-02-04';
            //         // }
            //         $checkIfReferred = UserOpen::where('client_id',$gm->client_id)->first();
            //         if(!$checkIfReferred){                    
            //             UserOpen::create([
            //                 'client_id' => $gm->client_id,
            //                 'cp_num' => $user->contact_number,
            //                 'first_name' => $user->first_name,
            //                 'last_name' => $user->last_name,
            //                 'bday' => $user->birth_date,
            //                 'gender' => $user->gender,
            //                 'referral_id' => $group->leader,
            //                 'is_verified' => 1,
            //             ]);
            //         }
            //     }
            // }

        });

    }

    private static function updateClientField($clientId, $groupId) {
        $clientServices = ClientService::has('documentLogs')
            ->with(['documentLogs' => function($query) {
                $query->orderBy('id', 'desc');
            }])
            ->where('active', 1)
            ->where('client_id', $clientId)
            ->get()
            ->makeHidden([
                'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
            ]);

        $documentsOnHand = [];
        $arr = [];

        foreach($clientServices as $clientService) {
            if( $clientService->documentLogs[0]->documents_on_hand  ) {
                $documentsOnHand[] = $clientService->documentLogs[0]->documents_on_hand;
            }
        }

        $docsIdArr = explode(',', implode(',', $documentsOnHand));

        $unqdocs =  DB::connection('visa')->table('service_documents')->where('is_unique', 1)->whereIn('id', $docsIdArr)->pluck('id');

        $usr =  User::where('id',$clientId)->select('id','first_name','last_name')->limit(1)->get()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
            if($usr){
                $arr = $usr[0];
                $arr['packages'] = Package::where('client_id',$clientId)->where('group_id',$groupId)->get();
                $arr['uniqueDocumentsOnHand'] = $unqdocs;
            }

        \DB::connection('visa')
            ->table('group_members')
            ->where('client_id', $clientId)
            ->where('group_id', $groupId)
            ->update(['client' => json_encode($arr)]);
    }


    public function client() {
        return $this->belongsTo('App\Models\User', 'client_id', 'id');
    }

    public function services() {
        return $this->hasMany('App\Models\Visa\ClientService', 'client_id', 'client_id');
    }

    public function group_detail(){
        return $this->belongsTo('App\Models\Visa\Group', 'group_id', 'id');
    }
}
