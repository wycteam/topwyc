<?php

namespace App\Models\Visa;

use App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Synchronization extends Model
{
    
    protected $connection = 'visa';

	protected $table = 'synchronizations';

	public $timestamps = false;

	protected $appends = ['page_count'];

	public function getPageCountAttribute() {
		$pageCount = 1;

		if($this->attributes['module'] == 'clients') {
			$pageCount = User::with('packages.service')
				->whereHas('roles', function($query) {
        			$query->where('name', 'visa-client');
        		})->where('is_active', 1)->count();

        	$pageCount = ceil($pageCount / 100);
		}

		return $pageCount;
	}

}