<?php

namespace App\Models\Visa;

use App\Models\User;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class ActionLogs extends Model
{
    use QueryFilterTrait;


    protected $connection = 'visa';
    protected $table = 'action_logs';
    protected $fillable = [
        'client_id', 'details', 'service_id', 'group_id', 'user_id', 'log_date'
    ];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();
    }

    ///////////////////
    // Relationships //
    ///////////////////

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
}
