<?php

namespace App\Models\Visa;

use App\Models\User;
use App\Models\Visa\ClientService;
use App\Models\Visa\CommissionLogs;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use App\Classes\Common;

class TransactionLogs extends Model
{
    use QueryFilterTrait;


    protected $connection = 'visa';
    protected $table = 'transaction_logs';
    protected $fillable = [
        'client_id', 'group_id', 'service_id','tracking' , 'user_id','type', 'amount' ,'balance', 'detail', 'detail_cn', 'log_date', 'old_detail', 'old_detail_cn', 'display','transaction_id','commission_ids','total'
    ];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::updated(function ($model) {
            static::updateParent($model->group_id, $model->service_id, $model->display);
        });

        static::created(function ($model) {
            

            //$countAgent = CommissionLogs::where('client_id',$model->client_id)->where('included',0)->count();
        });
    }


    private static function updateParent($groupId, $serviceId, $display) {
        if($groupId > 0 && $display > 0){
            $trans =  TransactionLogs::
                // ->where(function ($query) use($serviceId){
                //      $query->where('tracking', 'LIKE', '%'.$serviceId.'%')->orwhere('tracking','LIKE', '%,'.$serviceId.'%');
                //    })
                where('tracking', 'LIKE', '%'.$serviceId.'%')
                ->where('display', 0)
                ->where('group_id', $groupId)
                ->first();
                //\Log::info($trans);
            // if($trans){
            //     //\Log::info(json_decode( json_encode($trans), true));
            //     $csid = explode(',', $trans->tracking);
            //     $getcs = ClientService::whereIn('id',$csid)->select('id','detail','service_id','client_id','charge','cost','tip','status','active')->orderBy('service_id')->get();
            //     $serv_list = '<div class="row">';
            //     $current_id = 0;
            //     $old_detail = '';
            //     $old_detail_cn = '';
            //     $complete = 1;
            //     $amount = 0;
            //     foreach($getcs as $gcs){
            //         if($gcs->status != 'complete'){
            //             $ttl = ' - Total Charge : 0';
            //             $complete = 0;
            //         }
            //         else{
            //             $ttl = ' - Total Charge : '.(($gcs->cost + $gcs->charge + $gcs->tip)-($gcs->discount_amount));
            //             $amount += ($gcs->cost + $gcs->charge + $gcs->tip) - ($gcs->discount_amount);
            //         }

            //         if($current_id == 0 || $current_id != $gcs->service_id){
            //             if($current_id != 0 && $serv_list != '<div class="row">'){
            //                 $serv_list .= '</div>';
            //             }
            //             $current_id = $gcs->service_id;
            //             $serv_list .= '<div class="col-xs-6"><br><b>'.$gcs->detail.'</b>';
            //             $serv_list .= '<br><b>Client #'.$gcs->client_id.'</b> - '.$gcs->status.$ttl;
            //         }
            //         else{
            //             $serv_list .= '<br><b>Client #'.$gcs->client_id.'</b> - '.$gcs->status.$ttl;
            //         }

            //         $update_log = \DB::connection('visa')->table('transaction_logs')->where('service_id',$gcs->id)->where('type','service')->first();
            //         if($update_log){
            //             $old_detail .= '** '.$update_log->detail.'<br>'.$update_log->old_detail.'<br>';
            //             $old_detail_cn .= '** '.$update_log->detail_cn.'<br>'.$update_log->old_detail_cn.'<br>';
            //         }
            //     }
            //     $serv_list .= '</div>';

            //     $trans->detail = strstr($trans->detail, '.<small></small>', true);
            //     $trans->detail = $trans->detail.'.<small></small><br>'.$serv_list;
            //     if($complete){
            //         $trans->detail = str_replace("Added a", "Completed ", $trans->detail);
            //     }
            //     date_default_timezone_set('Asia/Manila');
            //     $dates = date('F j, Y, g:i a');
            //     $trans->log_date = $dates;
            //     $trans->amount = '-'.$amount;
            //     $trans->old_detail = $old_detail;
            //     $trans->old_detail_cn = $old_detail_cn;
            //     $trans->save();
            // }


        if($trans){
                //\Log::info(json_decode( json_encode($trans), true));
            $csid = explode(',', $trans->tracking);
            $getcs = ClientService::whereIn('id',$csid)->select('id','detail','service_id','client_id','charge','cost','tip','status','active')->orderBy('service_id')->get();


            $serv_list = '<div>';
            $current_id = 0;
            $amount = 0;
            $total = 0;
            $complete = 1;
            $old_detail = '';
            $old_detail_cn = '';
            $collect_client_service_ids = '';
            $client_service_id = '';
            foreach($getcs as $gcs){
                $client = User::findOrFail($gcs->client_id)->makeHidden([ 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);

                if($gcs->status != 'complete'){
                    //$ttl = ' - Total Charge : 0';
                    $complete = 0;
                }
                else{
                    //$ttl = ' - Total Charge : '.(($gcs->cost + $gcs->charge + $gcs->tip)-($gcs->discount_amount));
                    $amount += ($gcs->cost + $gcs->charge + $gcs->tip + $gcs->com_agent + $gcs->com_client) - ($gcs->discount_amount);
                }

                $total += ($gcs->cost + $gcs->charge + $gcs->tip + $gcs->com_agent + $gcs->com_client) - ($gcs->discount_amount);


                $unq_client = '<b>[' . $client->id . ']</b> ' . $client->full_name;

                $collect_client_service_ids .= $gcs->id.',';
                $client_service_id = $gcs->id;

                // $checkstat = ($gcs->status == 'complete' ? 'complete-status' : $gcs->status == 'on process' ? 'on-process-status' : $gcs->status == 'pending' ? 'pending-status' : '');

                $checkstat = '';

                if($gcs->status == 'complete'){
                    $checkstat = 'complete-status';
                }
                else if($gcs->status == 'on process'){
                    $checkstat = 'on-process-status';
                }
                else if($gcs->status == 'pending'){
                    $checkstat = 'pending-status';
                }

                $stat = '<small class="label '.$checkstat.'">'.strtoupper($gcs->status).'</small>';
                if($current_id == 0 || $current_id != $gcs->service_id){
                    if($current_id != 0 && $serv_list != '<div>'){
                        $serv_list .= '</div>';
                    }
                    $current_id = $gcs->service_id;
                    $serv_list .= '<div>';
                    $serv_list .= ''.$stat.' '.$unq_client;
                }
                else{
                    $serv_list .= '<br>'.$stat.' '.$unq_client;
                }

                $update_log = TransactionLogs::where('service_id',$gcs->id)->where('type','service')->first();
                if($update_log){
                    $serv_list .= '<br><br>&bull; '.$update_log->detail.'<br><br>'.$update_log->old_detail;
                    //$old_detail_cn .= $update_log->detail_cn.'<br><br>'.$update_log->old_detail_cn;
                }
            }
            $serv_list .= '</div>';

            //\Log::info($getcs);

                $trans->total = $total;
                $trans->detail = $serv_list;
                date_default_timezone_set('Asia/Manila');
                $dates = date('F j, Y, g:i a');
                $trans->log_date = $dates;
                $trans->amount = '-'.$amount;
                $trans->old_detail = $old_detail;
                $trans->old_detail_cn = $old_detail_cn;
                $trans->save();
        }

    }
    }



    ///////////////////
    // Relationships //
    ///////////////////

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function clientService() {
        return $this->belongsTo(ClientService::class, 'service_id', 'id');
    }
    
}
