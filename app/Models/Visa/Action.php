<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    
    protected $connection = 'visa';

	public $table = 'actions';

	public function categories() {
		return $this->belongsToMany('App\Models\Visa\Category', 'action_category', 'action_id', 'category_id');
	}

}
