<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;


class ServiceProfileCost extends Model
{
    protected $connection = 'visa';
	protected $table = 'service_profile_cost';
    protected $fillable = [
		 'service_id'
		,'profile_id'
        ,'branch_id'
		,'cost'
        ,'charge'
        ,'tip'
        ,'com_agent'
        ,'com_client'
    ];

    public $timestamps = false;

    public function profile() {
        return $this->belongsTo('App\Models\Visa\ServiceProfiles', 'profile_id', 'id');
    }

    // public function services()
    // {
    //     return $this->belongsToMany(Service::class,'id');
    // }

}
