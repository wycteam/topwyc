<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
  protected $connection = 'visa';
  protected $table = 'news_content';
  protected $primaryKey = 'news_id';
  protected $fillable = [
    'header',
    'content',
    'featured',
    'news_id',
    'author_id',
    'image',
    'featured',
    'created_at',
    'updated_at',
    'allow_comment',
    'only_private',
    'mobile_content'
  ];
}
