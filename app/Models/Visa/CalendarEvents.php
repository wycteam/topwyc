<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class CalendarEvents extends Model
{
    protected $connection = 'mysql';
    protected $table = 'calendar_events';
    public $timestamps = false;
    protected $fillable = [
        'user_id','title','start_date','end_date','type','dept_head_id','ceo_id','hr_id','reason','reason_denied','status','file_date','approval','year_added','rate','time_start','time_end','deductible','_locked_until'
    ];

    public static function boot()
    {
        parent::boot();
    }

    public function users(){
        return $this->belongsTo('App\Models\User','user_id');
    }
}
