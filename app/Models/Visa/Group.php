<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;
use App\Models\Visa\Branch;
use App\Models\User;
use App\Models\UserOpen;
use Carbon\Carbon ;

class Group extends Model
{
    protected $connection = 'visa';
	protected $table = 'groups';
    protected $fillable = [
         'id',
		 'name'
		,'leader'
		,'address'
        ,'tracking'
        ,'cost_level'
        ,'risk'
		,'branch_id'
        ,'balance'
        ,'collectable'
        ,'is_shop'
        ,'temp_balance','temp_collectable'
    ];

    public $timestamps = false;

    protected $appends = [
        'branch',
        'check_shop'
    ];


    //************ Attributes ***************

    
    public function getBranchAttribute()
    {
        return Branch::where('id',$this->branch_id)->first()->name;
    }

    public function getCheckShopAttribute()
    {
        return  User::where('id',$this->leader)->whereHas('roles', function($query)  {
                $query->where('role_id', 17);
            })->count();
    }


    //************ END Attributes ***************

    public function user(){
    	return $this->belongsTo('App\Models\User','leader');
    }

    public function deposit()
    {
        return $this->hasMany('App\Models\Visa\ClientTransactions', 'group_id', 'id')->where('type', '=', 'Deposit');
    }

    public function payment()
    {
        return $this->hasMany('App\Models\Visa\ClientTransactions', 'group_id', 'id')->where('type', '=', 'Payment');
    }

    public function refund()
    {
        return $this->hasMany('App\Models\Visa\ClientTransactions', 'group_id', 'id')->where('type', '=', 'Refund');
    }

    public function discount()
    {
        return $this->hasMany('App\Models\Visa\ClientTransactions', 'group_id', 'id')->where('type', '=', 'Discount');
    }

    public function packages()
    {
        return $this->hasMany(Visa\Package::class);
    }

    public function groupmember()
    {
        return $this->hasMany(GroupMember::class, 'group_id', 'id');
    }

    public function clients() {
        return $this->belongsToMany('App\Models\User', 'visa.group_members', 'group_id', 'client_id')->withPivot('leader');
    }

}
