<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    
    protected $connection = 'visa';

    public $table = 'categories';

    public function actions() {
		return $this->belongsToMany('App\Models\Visa\Action', 'action_category', 'category_id', 'action_id');
	}

}
