<?php

namespace App\Models\Visa;

use App\Models\User;
use App\Models\Visa\ClientService;
use App\Models\Visa\TransactionLogs;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Classes\Common;

class CommissionLogs extends Model
{
    use QueryFilterTrait;


    protected $connection = 'visa';
    protected $table = 'commission_logs';
    protected $fillable = [
        'client_id', 'group_id', 'service_id','tracking' , 'type', 'user_id' ,'commission', 'detail', 'detail_cn','detail_client','detail_client_cn', 'log_date', 'display', 'included', 'groupdate'
    ];

    public $timestamps = false;


    public static function boot()
    {
        parent::boot();

        static::updated(function ($model) {
            if($model->display == 1 && $model->included == 1){
                static::updateLogs($model->id);
            }
        });
    }


    private static function updateLogs($comId) {
        $trans = TransactionLogs::where('commission_ids','LIKE','%'.$comId.'%')->where('commission_ids','LIKE','%,%')->first();
        if($trans){
            $csid = explode(',', $trans->commission_ids);
            $countClient = CommissionLogs::whereIn('id',$csid)->where('id','!=',$comId)->get();

                $total = 0;
                $ids = '';
                $old_detail = '';
                $old_detail_cn = '';
                foreach($countClient as $cc){
                    $total += $cc->commission;
                    $ids .= $cc->id.',';
                    $checkTrans = TransactionLogs::where('commission_ids',$cc->id)->first();
                    if($checkTrans){
                        $old_detail .= '** '.$checkTrans->detail.'<br>'.$checkTrans->old_detail.'<br>';
                        $old_detail_cn .= '** '.$checkTrans->detail_cn.'<br>'.$checkTrans->old_detail_cn.'<br>';
                        $checkTrans->display = 1;
                        $checkTrans->save();
                    }
                    $comi = CommissionLogs::where('id',$cc->id)->first();
                    $comi->included = 1;
                    $comi->save();
                }

                $collect_ids = rtrim($ids, ', ');
                $notes = 'Total commission received -> Php'.$total.'.';
                $notes_cn = $notes;
                //$total_balance = ClientsController::clientCollectable($model->client_com_id);
                $trans->amount = $total;
                $trans->detail = $notes;
                $trans->old_detail = $old_detail;
                $trans->detail_cn = $notes_cn;
                $trans->old_detail_cn = $old_detail_cn;
                $trans->commission_ids = $collect_ids;
                $trans->save();
        }
    }


      ///////////////////
    // Relationships //
    ///////////////////

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
