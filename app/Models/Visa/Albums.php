<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class Albums extends Model
{
  protected $connection = 'visa';
  protected $table = 'image_album';
  protected $fillable = ['title','allow_public'];
  public $timestamps = false;
}
