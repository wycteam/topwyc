<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class BudgetOutput extends Model
{
  protected $connection = 'visa';
  protected $table = 'budget_output';
  protected $fillable = [
   'user_id','client_id','cost','extra_cost','service_id'
  ];
}
