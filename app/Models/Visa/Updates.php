<?php

namespace App\Models\Visa;

use App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Updates extends Model
{
    
    protected $connection = 'visa';

	protected $table = 'updates';

	public $timestamps = false;

	protected $fillable = ['id','client_id', 'date_modified'];

}