<?php

namespace App\Models\Visa;

use App\Models\User;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class PushNotificationsModel extends Model
{
    use QueryFilterTrait;

    protected $connection = 'visa';
    protected $table = 'push_notifications';
    protected $fillable = ['client_id','title','body','extra','type','type_id','main','parent','parent_type','seen_at','read_at'];

    public $timestamps = false;

    public static function boot()
    {
        parent::boot();
    }

    public function user(){
    	return $this->belongsTo('App\Models\User','client_id');
    }
    
}
