<?php

namespace App\Models\Visa;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $connection = 'visa';
    protected $table = 'advertisements';
    protected $fillable = ['title','image', 'url', 'status', 'serialized','sizes','created_at', 'updated_at'];
}
