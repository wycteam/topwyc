<?php

namespace App\Models;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Awb extends Model
{
    use QueryFilterTrait;

    protected $fillable = [
        'id', 'order_id', 'product_id',
        'included', 'total_price', 'wataf_cost', 'twogo_cost'
    ];

    public static function boot()
    {
        parent::boot();
    }

    ///////////////////
    // Relationships //
    ///////////////////


}
