<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 'label', 'description',
    ];

    public function setLabelAttribute($value)
    {
        $this->attributes['name'] = isset($this->attributes['name'])
            ? $this->attributes['name']
            : str_slug($value);

        $this->attributes['label'] = $value;
    }

    public function users()
    {
        return $this->belongsToMany(User::class)
            ->withTimestamps();
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class)
            ->withTimestamps();
    }

    public function assignPermission($permission)
    {
        if (is_string($permission)) {
            $this->permissions()->save(
                Permission::whereName($permission)->firstOrFail()
            );
        } elseif (is_array($permission)) {
            $this->permissions()->attach($permission);
        }
    }
}
