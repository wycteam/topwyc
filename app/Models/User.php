<?php

namespace App\Models;

use App\Acme\Traits\Imageable;
use App\Acme\Traits\Notifiable;
use App\Acme\Traits\Numberable;
use App\Acme\Traits\Addressable;
use App\Models\Shopping\Friends;
use App\Models\Visa\PushNotificationsModel;
use App\Models\Visa\Group;
use App\Models\Visa\Branch;
use App\Models\Visa\GroupMember;
use App\Models\Visa\ClientService;
use App\Models\Visa\ServiceDocuments;
use App\Models\Visa\Deposit;
use App\Models\Visa\Payment;
use App\Models\Visa\Refund;
use App\Models\Visa\Discount;
use App\Models\Visa\ClientTransactions;
use App\Models\User;
use App\Acme\Traits\Documentable;
use Laravel\Passport\HasApiTokens;
use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Activity;
use App\Classes\Common;
use App\Http\Controllers\Cpanel\SynchronizationController;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, HasApiTokens,
        Addressable, Numberable, Imageable, Documentable, QueryFilterTrait;

    protected $connection = 'mysql';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'first_name', 'middle_name', 'last_name',
        'birth_date', 'gender', 'civil_status', 'height', 'weight',
        'passport', 'passport_exp_date','display_name', 'display_image',
        'risk', 'branch_id','nationality','birth_country',
        'address','contact_number','alternate_contact_number',
        'visa_type','icard','exp_date','arrival_date','icar_exp_date','issue_date','first_exp_date',
        'balance','collectable','middleman',
        'temp_balance','temp_collectable'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verification_token', 'access_token', 'images',
    ];

    protected $casts = [
        'is_active' => 'boolean',
        'is_verified' => 'boolean',
        'is_docs_verified' => 'boolean',
        'is_display_name' => 'boolean',
    ];

    protected $appends = [
        'full_name',
        'avatar',
        // 'is_docs_verified',
        // 'address',
        // , 'number', 'contact'
        'permissions',
        'access_control',
        'binded',
        'unread_notif',
        'group_binded',
        'document_receive',
        'is_leader',
        'total_points',
        'total_deposit',
        'total_discount',
        'total_refund',
        'total_payment',
        'total_cost',
        'total_complete_cost',
        'total_balance',
        'collectable',
        'branch',
        'three_days'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            ! $user->is_verified && $user->verification_token = str_random(30);
        });

        //code to reset all user token
        $usrs = User::where('access_token','')->orWhere('access_token',null)->get();
        foreach($usrs as $u){
            if($u->access_token == ''){
                $u->access_token = $u->createToken('Personal')->accessToken;
                $u->save();
            }
        }

        // $usrs = User::find(1)->get();
        // foreach($usrs as $u){
        //     if($u->access_token == ''){
        //         $u->access_token = $u->createToken('Personal')->accessToken;
        //         $u->save();
        //     }
        // }

        static::created(function ($user) {
            $user->access_token = $user->createToken('Personal')->accessToken;
            $user->save();

            if($user->exp_date == '0000-00-00'){    
                $user->exp_date = null;
                $user->save();
            }
            
            SynchronizationController::insertUpdate($user->id, Carbon::now()->toDateTimeString());

        });

        static::updating(function($user)
        {
            $changes = [];

            foreach($user->getDirty() as $key => $value)
            {
                $original = $user->getOriginal($key);

                 if($key != 'access_token' && $key != 'device_id' && $key != 'device_type' && $key != 'token' && $key != 'language'){
                    SynchronizationController::insertUpdate($user->id, Carbon::now()->toDateTimeString());
                 }
            }

        });

        static::updated(function ($user) {
            // if($user->access_token == ''){
            //     $user->access_token = $user->createToken('Personal')->accessToken;
            //     $user->save();
            // }
            
            if($user->exp_date == '0000-00-00'){    
                $user->exp_date = null;
                $user->save();

                SynchronizationController::insertUpdate($user->id, Carbon::now()->toDateTimeString());
            }
        });

        // static::addGlobalScope('avatar', function (Builder $builder) {
        //     $builder->with(['images' => function ($query) {
        //         $query->whereType('avatar');
        //     }]);
        // });
    }

    public function receivesBroadcastNotificationsOn()
    {
        return 'User.'.$this->id;
    }

    public function setPasswordAttribute($value)
    {
        if (\Hash::needsRehash($value)) {
            $this->attributes['password'] = bcrypt($value);
        } else {
            $this->attributes['password'] = $value;
        }
    }

    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = ucfirst($value);
    }

    public function setMiddleNameAttribute($value)
    {
        $this->attributes['middle_name'] = ucfirst($value);
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = ucfirst($value);
    }

    // public function getNameIfSupportAttribute()
    // {
    //     if ($this->hasRole('support')) {
    //         return $this->first_name . ' ' . $this->last_name. ' (support)';
    //     }
    //     return $this->first_name . ' ' . $this->last_name;
    // }

    public function getFullNameAttribute()
    {
        return ucwords($this->first_name) . ' ' . ucwords($this->last_name);
    }

    public function getIsLeaderAttribute()
    {
        $is_leader = GroupMember::where('client_id',$this->id)->where('leader','>',0)->first();
        $leader = false;
        if($is_leader){
            $leader = true;
        }
        return $leader;
    }

    // public function getInitialsAttribute()
    // {
    //     return strtoupper($this->first_name[0] . $this->last_name[0]);
    // }

    // public function getIsDocsVerifiedAttribute()
    // {
    //     $documents = $this->documents->sortByDesc('updated_at');
    //     $documents = $documents->unique('type');
    //     $individual = 0;
    //     $enterprise = 0;
    //     foreach ($documents as $key => $value) {
    //         switch ($value->type) {
    //             case 'nbi':
    //             // case 'birthCert':
    //             case 'govId':
    //                 $individual += $value->status=='Verified'?1:0;
    //                 break;
    //                 # code...
    //             case 'sec' :
    //             case 'bir':
    //             case 'permit':
    //                 $enterprise += $value->status == 'Verified'?1:0;
    //                 break;
    //         }
    //     }

    //     if($individual == 2 || $enterprise == 3)
    //     {
    //         return true;
    //     }else
    //     {
    //         return false;
    //     }

    // }

    // public function getAddressAttribute()
    // {
    //     return $this->addresses()->first();
    // }

    public function getBranchAttribute()
    {
        if($this->branch_id){
            return Branch::where('id',$this->branch_id)->first()->name;
        }
        return null;
    }

    private function visibilty($date) {
        $expirationDate = Carbon::parse($date);
        $today = Carbon::today();

        $difference = $today->diffInDays($expirationDate, false);

        if( $difference <=30 && ($difference % 3 == 0 || $difference < 0) ) return true;

        return false;
    }

    public function getThreeDaysAttribute() {
        $ifShow = false;

        // For 9g and TRV
        if($this->visa_type == '9G' || $this->visa_type == 'TRV') {
            // icard_exp_date
            if($this->icard_exp_date)
                if( $this->visibilty($this->icard_exp_date) ) $ifShow = true;

            // exp_date
            if($this->exp_date)
                if( $this->visibilty($this->exp_date) ) $ifShow = true;
        }

        // For CWV
        if($this->visa_type == 'CWV') {
            // exp_date
            if($this->exp_date)
                if( $this->visibilty($this->exp_date) ) $ifShow = true;
        }

        return $ifShow;
    }

    // public function getNumberAttribute()
    // {
    //     return $this->numbers()->first();
    // }

    // public function getContactAttribute()
    // {
    //     return $this->numbers()->first();
    // }

    public function getPermissionsAttribute()
    {
        return $this->permissions()->get();
    }

    public function getBindedAttribute()
    {
        $binded = 0;
        if($this->password != null){
            $binded = 1;
        }
        return $binded;
    }

    public function getUnreadNotifAttribute()
    {
        return PushNotificationsModel::where('client_id',$this->id)->where('parent_type','>',0)->where('read_at',NULL)->count();
    }

    public function getTotalPointsAttribute()
    {
        return ClientService::where('client_id', $this->id)
            ->where('group_id', 0)
            ->where('active', 1)
            ->where('status', 'complete')
            ->sum('com_client');
    }

    public function getTotalDepositAttribute()
    {
        return ClientTransactions::where('type', 'Deposit')
            ->where('client_id', $this->id)
            ->where('group_id', 0)
            ->sum('amount');
    }

    public function getTotalRefundAttribute()
    {
        return ClientTransactions::where('type', 'Refund')
            ->where('client_id', $this->id)
            ->where('group_id', 0)
            ->sum('amount');
    }

    public function getTotalPaymentAttribute()
    {
        return ClientTransactions::where('type', 'Payment')
            ->where('client_id', $this->id)
            ->where('group_id', 0)
            ->sum('amount');
    }

    public function getTotalDiscountAttribute()
    {
        return ClientTransactions::where('type', 'Discount')
            ->where('client_id', $this->id)
            ->where('group_id', 0)
            ->sum('amount');
    }

    public function getTotalCostAttribute()
    {
        $cost = ClientService::where('client_id',$this->id)->where('group_id',0)->where('active',1)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
        if($cost == null || $cost == ''){
            return 0;
        }
        return $cost;
    }

    public function getTotalCompleteCostAttribute()
    {
        $cost = ClientService::where('client_id',$this->id)->where('status','complete')->where('group_id',0)->where('active',1)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
        if($cost == null || $cost == ''){
            return 0;
        }
        return $cost;
    }

    public function getTotalBalanceAttribute()
    {
        $bal =     ((
                       (float)$this->getTotalDepositAttribute()
                        + (float)$this->getTotalPaymentAttribute()
                        + (float)$this->getTotalDiscountAttribute()
                    )-(
                        (float)$this->getTotalRefundAttribute()
                        + (float)$this->getTotalCostAttribute()
                    ));
        return $bal;
    }

    public function getCollectableAttribute()
    {
        $bal =     ((
                       (float)$this->getTotalDepositAttribute()
                        + (float)$this->getTotalPaymentAttribute()
                        + (float)$this->getTotalDiscountAttribute()
                    )-(
                        (float)$this->getTotalRefundAttribute()
                        + (float)$this->getTotalCompleteCostAttribute()
                    ));
        return $bal;
    }

    public function getGroupBindedAttribute()
    {
        $trans = trans('cpanel/clients-page');
        $groups =  GroupMember::where('client_id',$this->id)->pluck('group_id');
        $leaders = Group::whereIn('id',$groups)->get();
        //$leads = [];
        $grps = '';
        //$ctr = 0;
        foreach($leaders as $l){
            $g = GroupMember::where('group_id',$l->id)->where('leader','>',0)->pluck('leader');
            $binded = User::whereIn('id',$g)->where('password','!=',null)->count();
            if($binded > 0){
                $grps =  $l->name.', ';
            }
            // $leads[$ctr]['Group'] = $l->name;
            // $leads[$ctr]['leader_binded'] = $binded;
            // $ctr++;
        }
        if($grps!=''){
            $grps = $trans['leader-binded']." - " .$trans['group'].": ".$grps;
        }
        return $grps;
    }

    public function getDocumentReceiveAttribute()
    {
        $rcvs =  ClientService::where('client_id',$this->id)->pluck('rcv_docs');
        $unique ='';
        foreach($rcvs as $r){
                $unique .= ','.$r;
        }
        $u = array_unique(explode(',', trim($unique,",")));
        $s = ServiceDocuments::whereIn('id',$u)->pluck('title');
        return $s;
    }

    public function getAvatarAttribute()
    {
        if ($this->images->where('type', 'avatar')->count()) {
            return '/images/avatar/' . $this->id . '.jpg';
        }

        if($this->gender == 'Male')
        {
            return '/images/avatar/boy-avatar.png';
        }else
        {
            return '/images/avatar/girl-avatar.png';
        }
    }

    public function getAccessControlAttribute()
    {
        return AccessControl::all();
    }

    // public function getFeeOnHoldAttribute()
    // {
    //     // $fee = $this->product_orderdetails->where('status','In Transit')->sum('shipping_fee');
    //     // $charge = $this->product_orderdetails->where('status','In Transit')->sum('charge');
    //     // $vat = $this->product_orderdetails->where('status','In Transit')->sum('vat');

    //     // $fee += $this->product_orderdetails->where('status','Returned')->where('returned_received_date','')->sum('shipping_fee');
    //     // $charge += $this->product_orderdetails->where('status','Returned')->where('returned_received_date','')->sum('charge');
    //     // $vat += $this->product_orderdetails->where('status','Returned')->where('returned_received_date','')->sum('vat');

    //     $fee = $this->product_orderdetails->where('status','Pending Request')->sum('disc_fee');
    //     $fee += $this->product_orderdetails->where('status','In Transit')->sum('disc_fee');
    //     $fee += $this->product_orderdetails->where('status','Returned')->where('returned_received_date','')->sum('disc_fee');


    //     return $fee;
    // }

    //function to know if user has transaction to specific products
    // public function getAllOrdersAttribute()
    // {
    //     $orders = $this->getall_orders;
    //     if($orders){
    //         return $orders;
    //     }
    //     return null;
    // }

    // public function getIsOnlineAttribute()
    // {
    //     if(Activity::users()->where('user_id',$this->id)->first()){
    //         return true;
    //     }
    //     return false;
    // }
    ///////////////////
    // Relationships //
    ///////////////////

    public function roles()
    {
        return $this->belongsToMany(Role::class)
            ->withTimestamps();
    }

   public function rolesname()
    {
        return $this->belongsToMany(Role::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class)
            ->withTimestamps();
    }

    public function settings()
    {
        return $this->hasOne(Setting::class);
    }

    /***** EMPLOYEES *****/

    public function attendance()
    {
        return $this->hasMany(Employee\Attendance::class);
    }

    public function department()
    {
        return $this->hasMany(Employee\DepartmentUser::class);
    }

    public function payrollSettings(){
        return $this->hasOne(Employee\PayrollSettings::class);
    }

    public function leaves(){
        return $this->hasMany(Visa\CalendarEvents::class);
    }
    
    /***** EMPLOYEES *****/


    /***** VISA *****/
    public function pushnotifications()
    {
        return $this->belongsToMany(Visa\PushNotificationsModel::class);
    }

    public function group()
    {
        return $this->hasMany(Visa\Group::class);
    }

    public function deposit()
    {
        //return $this->hasMany(Visa\Deposit::class);
        return $this->client_transactions()->where('type', 'Deposit');
    }

    public function payment()
    {
        //return $this->hasMany(Visa\Payment::class);
        return $this->client_transactions()->where('type', 'Payment');
    }

    public function refund()
    {
        //return $this->hasMany(Visa\Refund::class);
        return $this->client_transactions()->where('type', 'Refund');
    }

    public function discount()
    {
        // return $this->hasMany(Visa\Discount::class);
        return $this->client_transactions()->where('type', 'Discount');
    }

    public function client_transactions()
    {
        return $this->hasMany(Visa\ClientTransactions::class);
    }

    public function packages()
    {
        return $this->hasMany(Visa\Package::class, 'client_id', 'id');
    }

    public function services()
    {
        return $this->hasMany(Visa\ClientService::class, 'client_id', 'id')->orderBy('id','desc');
    }

    public function groups() {
        return $this->belongsToMany(Visa\Group::class, 'visa.group_members', 'client_id', 'group_id')->withPivot('leader');
    }

    public function schedule() {
        return $this->hasOne('App\Models\Schedule', 'user_id', 'id');
    }

    /***** VISA *****/

    // public function ewallet()
    // {
    //     return $this->hasOne(Shopping\Ewallet::class);
    // }

    // public function ewallet_transactions()
    // {
    //     return $this->hasMany(Shopping\EwalletTransactions::class,'user_id');
    // }

    // public function chat_rooms()
    // {
    //     return $this->belongsToMany(ChatRoom::class)
    //         ->withTimestamps();
    // }

    // public function messages()
    // {
    //     return $this->hasMany(Message::class);
    // }

    // public function latest_message_notifications()
    // {
    //     return $this->hasMany(Views\LatestMessageNotification::class, 'notifiable_id');
    // }

    // public function friend_request_notifications()
    // {
    //     return $this->hasMany(Views\FriendRequestNotification::class, 'notifiable_id');
    // }

    // public function orders(){
    //     return $this->hasMany(Shopping\Order::class);
    // }

    public function tasks() {
        return $this->hasMany('App\Models\Visa\Task', 'who_is_in_charge', 'id');
    }

    //////////////////////
    // Custom Functions //
    //////////////////////

    public function confirmEmail()
    {
        $this->verification_token = null;
        $this->is_verified = true;

        $this->save();
    }

    public function assignRole($role)
    {
        if (is_string($role)) {
            $this->roles()->save(
                Role::whereName($role)->firstOrFail()
            );
        } elseif (is_array($role)) {
            $this->roles()->attach($role);
        }
    }

    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !! $role->intersect($this->roles)->count();
    }

    public function givePermission($permission)
    {
        if (is_string($permission)) {
            $this->permissions()->save(
                Permission::whereName($permission)->firstOrFail()
            );
        } elseif (is_array($permission)) {
            $this->permissions()
                ->attach($permission);
        }
    }

    public function hasPermission($permission)
    {
        if (is_string($permission)) {
            return $this->permissions->contains('name', $permission);
        }

        return !! $permission->intersect($this->permissions)->count();
    }

    public function isMemberOf($chatRoomId)
    {
        return $this->chat_rooms->contains('id', $chatRoomId);
    }

    public function joinChatRoom($chatRoomId)
    {
        return $this->chat_rooms()->attach($chatRoomId);
    }

    public function getMutualChatRoom($userId)
    {
        $mutualChatGroup = DB::table('user_chat_room')->where('a_user_id', $this->id)
            ->where('b_user_id', $userId)
            ->where('name', null)
            ->first();

        if (! $mutualChatGroup) {
            return null;
        }

        return ChatRoom::find($mutualChatGroup->id);
    }

    /**
     * Get user's friends
     * @return Eloquent-Collection
     */
    // public function friends()
    // {
    //     return Relationship::where(function ($query) {
    //         $query->where('a_user_id', $this->id)
    //             ->orWhere('b_user_id', $this->id);
    //     })
    //     ->where('type', 'Friend')
    //     ->where('status', 'Accepted')
    //     ->get();
    // }

    // public function relationships()
    // {
    //     return Relationship::where(function ($query) {
    //         $query->where('a_user_id', $this->id)
    //             ->orWhere('b_user_id', $this->id);
    //     })->get();
    // }

    /**
     * Check if the user is friends with the given id
     * @param  integer  $id User id
     * @return boolean     [description]
     */
    // public function isFriendsWith($id)
    // {
    //     return !! Relationship::where(function ($query) use ($id) {
    //         $query->where(function ($query) use ($id) {
    //             $query->where('a_user_id', $this->id)
    //                 ->where('b_user_id', $id);
    //         })->orWhere(function ($query) use ($id) {
    //             $query->where('a_user_id', $id)
    //                 ->where('b_user_id', $this->id);
    //         });
    //     })
    //     ->where('type', 'Friend')
    //     ->where('status', 'Accepted')
    //     ->count();
    // }

    // public function friends2($status = 'Friend')
    // {
    //     return DB::select(
    //         DB::raw('
    //             SELECT u.id, u.email, u.first_name, u.last_name, u.nick_name, u.gender, f.status, i.name AS avatar
    //             FROM wyc.users AS u
    //             JOIN shopping.friends AS f
    //             ON u.id = (
    //                 CASE
    //                     WHEN f.user_id = :uid THEN f.user_id2
    //                     WHEN f.user_id2 = :uid THEN f.user_id
    //                 END
    //             )
    //                 AND (f.user_id = :uid OR f.user_id2 = :uid)
    //                 AND f.status = :status
    //             LEFT JOIN wyc.images AS i
    //             ON u.id = i.imageable_id
    //                 AND i.imageable_type = "App\Models\User"
    //                 AND i.type = "avatar"
    //         '),
    //         ['uid' => $this->id, 'status' => $status]
    //     );
    // }
}
