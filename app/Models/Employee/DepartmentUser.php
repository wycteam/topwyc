<?php

namespace App\Models\Employee;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class DepartmentUser extends Model
{
    use QueryFilterTrait;
    protected $connection = 'mysql';
    protected $table = 'department_user';

    protected $fillable = [
        'department_id', 'user_id'
    ];

    public static function boot()
    {
        parent::boot();
    }

    ///////////////////
    // Relationships //
    ///////////////////

    public function user(){
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function departments(){
        return $this->belongsTo('App\Models\Employee\Departments','department_id');
    }
}
