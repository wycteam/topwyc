<?php

namespace App\Models\Employee;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Departments extends Model
{
    use QueryFilterTrait;
    protected $connection = 'mysql';
    protected $table = 'departments';

    protected $fillable = [
        'id', 'name'
    ];

    public static function boot()
    {
        parent::boot();
    }

    ///////////////////
    // Relationships //
    ///////////////////
    public function departmentUser(){
        return $this->hasMany(DepartmentUser::class);
    }

}
