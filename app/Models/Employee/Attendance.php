<?php

namespace App\Models\Employee;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Attendance extends Model
{
    use QueryFilterTrait;
    protected $connection = 'mysql';
    protected $table = 'attendance';

    protected $fillable = [
        'id', 'user_id', 'day',
        'month', 'year', 'time_in', 'in_reason'
        ,'timein_status', 'time_out', 'out_reason'
        , 'duration', 'timout_status', 'approved','event_id'
    ];

    public static function boot()
    {
        parent::boot();
    }

    public function user(){
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function userName(){
        return $this->belongsTo('App\Models\User','user_id')->select(array('id', 'first_name','last_name'));
    }

    ///////////////////
    // Relationships //
    ///////////////////


}
