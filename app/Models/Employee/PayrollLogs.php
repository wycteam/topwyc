<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class PayrollLogs extends Model
{
    protected $connection = 'mysql';
    protected $table = 'payroll_logs';
    protected $fillable = [
      'user_id','_bonus','_total_deductions','_year','_month','_date','_allowance','_absent_count','_late_count','_leave_count','_holiday_count','_total_pay','_total_earnings','_overtime','_total_lates','_loan','_penalty'
    ];
}
