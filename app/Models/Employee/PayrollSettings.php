<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class PayrollSettings extends Model
{
    protected $connection = 'mysql';
    protected $table = 'payroll_settings';
    public $timestamps = false;
    protected $fillable = [
        'user_id', '_basic_pay','_sss','_pag_ibig','_phil_health','_allowance','_sss_es','_pag_ibig_es','_phil_health_es','_day_from','_day_to','_employment_date'
    ];

    public static function boot()
    {
        parent::boot();
    }

    ///////////////////
    // Relationships //
    ///////////////////
    public function users(){
        return $this->belongsTo('App\Models\User','user_id');
    }
}
