<?php

namespace App\Models\Employee;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class AttendanceLogs extends Model
{
    use QueryFilterTrait;
    protected $connection = 'mysql';
    protected $table = 'attendance_logs';
    
    protected $fillable = [
        'atd_id', 'time_in', 'time_out',
    ];

    public static function boot()
    {
        parent::boot();
    }

    ///////////////////
    // Relationships //
    ///////////////////


}
