<?php

namespace App\Models\Employee;

use Illuminate\Database\Eloquent\Model;

class Installments extends Model
{
    protected $connection = 'mysql';
    protected $table = 'payroll_installments';
    protected $fillable = [
      'user_id','full_amount','payment_amount','credit_type','payment_terms','payment_count','counter','status'
    ];
}
