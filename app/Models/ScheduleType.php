<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduleType extends Model
{
    
	protected $table = 'schedule_types';

	public function schedules() {
		return $this->hasMany('App\Models\Schedule', 'schedule_type_id', 'id');
	}

}
