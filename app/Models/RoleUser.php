<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class RoleUser extends Model
{
    use QueryFilterTrait;
	
    protected $connection = 'mysql';

	protected $table = 'role_user';
    protected $fillable = [
		 'user_id'
		,'role_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class,'role_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

}
