<?php

namespace App\Models;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use QueryFilterTrait;

    protected $fillable = [
	   'id', 'province_id', 'name', 'area_code',
	   'port_code', 'delivery_category'
    ];

	public $timestamps = false;
}
