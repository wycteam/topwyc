<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $connection = 'mysql';

    protected $fillable = [
        'user_id',
        'type', 'name',
        'documentable_id', 'documentable_type',
        'status', 'expires_at', 'reason',
    ];

    public function documentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function uploader()
    {
        return $this->belongsTo(User::class, 'documentable_id');
    }
}
