<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DevelopmentLogs extends Model
{
    protected $connection = 'visa';

	protected $table = 'development_logs';
    protected $fillable = [
		 'title'
		,'body'
		,'version'
		,'date'
    ];
}
