<?php

namespace App\Models;

use Illuminate\Support\Facades\File as Filesystem;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $connection = 'mysql';

    protected $fillable = [
        'file', 'type',
        'fileable_id', 'fileable_type',
    ];

    const IMAGE_TYPES = [
        'jpg', 'jpeg', 'png', 'gif', 'svg',
    ];

    const VIDEO_TYPES = [
        'avi', 'mov', 'qt', 'wmv', 'yuv', 'webm', 'mkv', 'flv', 'vob', 'amv', 'mp4',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($file) {
            $file->type = self::getFileType($file);
        });
    }

    ///////////////////
    // Relationships //
    ///////////////////

    public function fileable()
    {
        return $this->morphTo();
    }

    ///////////////////////
    // Private Functions //
    ///////////////////////

    private static function getFileType($file)
    {
        $type = 'Unknown';
        $ext = strtolower(Filesystem::extension($file->file));

        if (in_array($ext, self::IMAGE_TYPES)) {
            $type = 'Image';
        } else if (in_array($ext, self::VIDEO_TYPES)) {
            $type = 'Video';
        }

        return $type;
    }
}
