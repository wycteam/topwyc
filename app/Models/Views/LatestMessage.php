<?php

namespace App\Models\Views;

use Illuminate\Database\Eloquent\Model;

class LatestMessage extends Model
{
    protected $fillable = [
        'user_id', 'chat_group_id',
        'body',
    ];

    ///////////////////
    // Relationships //
    ///////////////////

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function chat_room()
    {
        return $this->belongsTo(\App\Models\ChatRoom::class);
    }
}
