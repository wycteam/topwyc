<?php

namespace App\Models\Views;

use Illuminate\Database\Eloquent\Model;

class FriendRequestNotification extends Model
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array',
        'seen_at' => 'datetime',
        'read_at' => 'datetime',
    ];

    ///////////////////
    // Relationships //
    ///////////////////

    public function from()
    {
        return $this->belongsTo(\App\Models\User::class, 'from_id');
    }
}
