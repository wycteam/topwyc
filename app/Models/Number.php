<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    protected $connection = 'mysql';

    protected $fillable = [
        'numberable_id', 'numberable_type',
        'type',
        'area_code','number', 'number_type', 'country',
        'remarks',
        'is_active',
    ];

    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = ucwords($value);
    }

    public function setNumberTypeAttribute($value)
    {
        $this->attributes['number_type'] = ucwords($value);
    }

    public function setCountryAttribute($value)
    {
        $this->attributes['country'] = ucwords($value);
    }

    ///////////////////
    // Relationships //
    ///////////////////

    public function numberable()
    {
        return $this->morphTo();
    }
}
