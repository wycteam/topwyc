<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = [
        'type',
        'question', 'answer',
        'question_cn', 'answer_cn', 
        'upload_type',
    ];
}
