<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class DocumentLog extends Model
{
    use QueryFilterTrait;
	
    protected $connection = 'mysql';

	protected $table = 'document_logs';
    protected $fillable = [
		 'user_id'
		,'doc_id'
		,'processor_id'
    ];

    public function document()
    {
        return $this->belongsTo(Document::class,'doc_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function processor()
    {
        return $this->belongsTo(User::class,'processor_id');
    }

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('addNeededAttributes', function (Builder $builder) {
            $user = Auth::user();
            if(($user->hasRole('cpanel-admin') || $user->hasRole('master')))
            {
                $builder->with('document'
                            ,'user'
                            ,'processor'
                            );
            }
            $builder->orderBy('updated_at','desc');

        });
    }
}
