<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessControl extends Model
{
    protected $connection = 'mysql';

 	protected $table = 'access_control';

    protected $fillable = [
        'setting'
    ];
}
