<?php

namespace App\Models;

use App\Models\User;
use Laravel\Passport\HasApiTokens;
use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Activity;
use App\Classes\Common;

class ShopRate extends Authenticatable
{
    //use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'shop_rate';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'month','year', 'rate'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];




    public static function boot()
    {
        parent::boot();
    }

    public function user()
    {
        return $this->belongsTo(User::class,'client_id','id');
    }
    
    ///////////////////
    // Relationships //
    ///////////////////

}
