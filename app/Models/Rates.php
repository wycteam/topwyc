<?php

namespace App\Models;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Rates extends Model
{
    use QueryFilterTrait;

    protected $fillable = [
        'rate_category', 'halfkg',
        'fkg', 'excess',
        'fkg3', 'excess2',
    ];

    public static function boot()
    {
        parent::boot();
    }

    ///////////////////
    // Relationships //
    ///////////////////


}
