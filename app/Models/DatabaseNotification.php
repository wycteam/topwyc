<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\DatabaseNotificationCollection;

class DatabaseNotification extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notifications';

    /**
     * The guarded attributes on the model.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array',
        'seen_at' => 'datetime',
        'read_at' => 'datetime',
    ];

    protected $appends = [
        'human_created_at',
        'sender_avatar'
    ];

    /**
     * Get the notifiable entity that the notification belongs to.
     */
    public function notifiable()
    {
        return $this->morphTo();
    }

    public function getHumanCreatedAtAttribute(){
        return $this->created_at->diffForHumans();
    }

    public function getSenderAvatarAttribute() {
        if(User::findOrFail($this->from_id)->images->where('type', 'avatar')->count()) {
            return '/images/avatar/' . $this->from_id . '.jpg';
        }

        if(User::findOrFail($this->from_id)->gender == 'Male')
        {
            return '/images/avatar/boy-avatar.png';
        }else
        {
            return '/images/avatar/girl-avatar.png';
        }
    }

    /**
     * Mark the notification as read.
     *
     * @return void
     */
    public function markAsRead()
    {
        if (is_null($this->read_at)) {
            $this->forceFill(['read_at' => $this->freshTimestamp()])->save();
        }
    }

    /**
     * Determine if a notification has been read.
     *
     * @return bool
     */
    public function read()
    {
        return $this->read_at !== null;
    }

    /**
     * Determine if a notification has not been read.
     *
     * @return bool
     */
    public function unread()
    {
        return $this->read_at === null;
    }

    /**
     * Create a new database notification collection instance.
     *
     * @param  array  $models
     * @return \Illuminate\Notifications\DatabaseNotificationCollection
     */
    public function newCollection(array $models = [])
    {
        return new DatabaseNotificationCollection($models);
    }
}
