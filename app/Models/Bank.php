<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Bank extends Model
{
    protected $fillable = [
        'bank_name', 'bank_account_type', 'bank_account_number',
        'bankable_id', 'bankable_type',
    ];

    public function setBankNameAttribute($value)
    {
        $this->attributes['bank_name'] = Crypt::encryptString($value);
    }

    public function setBankAccountNumber($value)
    {
        $this->attributes['bank_account_number'] = Crypt::encryptString($value);
    }

    public function setBankAccountType($value)
    {
        $this->attributes['bank_account_type'] = Crypt::encryptString($value);
    }

    public function getBankNameAttribute($value)
    {
        return Crypt::decryptString($value);
    }

    public function getBankAccountNumberAttribute($value)
    {
        return $value;
    }

    public function getBankAccountTypeAttribute($value)
    {
        return $value;
    }

    public function bankable()
    {
        return $this->morphTo();
    }
}
