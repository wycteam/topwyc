<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Relationship extends Model
{
    protected $fillable = [
        'a_user_id', 'b_user_id', 'action_user_id',
        'type', 'status',
    ];

    ///////////////////
    // Relationships //
    ///////////////////

    public function a_user()
    {
        return $this->belongsTo(User::class, 'a_user_id');
    }

    public function b_user()
    {
        return $this->belongsTo(User::class, 'b_user_id');
    }

    public function action_user()
    {
        return $this->belongsTo(User::class, 'action_user_id');
    }
}
