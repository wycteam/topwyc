<?php

namespace App\Models;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    use QueryFilterTrait;

    public $fillable = [
	   'id', 'name', 'region',
    ];

	public $timestamps = false;
}
