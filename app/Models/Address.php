<?php

namespace App\Models;

use App\Acme\Traits\Numberable;
use Illuminate\Database\Eloquent\Model;

use App\Http\Controllers\Cpanel\SynchronizationController;
use Carbon\Carbon;

class Address extends Model
{
    use Numberable;

    protected $connection = 'mysql';

    protected $fillable = [
        'addressable_id', 'addressable_type',
        'type', 'label',
        'address', 'barangay', 'city','city_id', 'province', 'province_id', 'zip_code', 'country','landmark',
        'remarks', 'name_of_receiver',
        'contact_number', 'alternate_contact_number', 
        'is_active'
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    public static function boot()
    {
        parent::boot();

        // static::creating(function ($model) {
        //     SynchronizationController::insertUpdate($model->client_id, Carbon::now()->toDateTimeString());
        // });

        static::updating(function($model)
        {
            SynchronizationController::insertUpdate($model->addressable_id, Carbon::now()->toDateTimeString());
        });
    }

    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = ucwords($value);
    }

    public function setAddressAttribute($value)
    {
        $this->attributes['address'] = ucwords($value);
    }

    public function setBarangayAttribute($value)
    {
        $this->attributes['barangay'] = ucwords($value);
    }

    ///////////////////
    // Relationships //
    ///////////////////

    public function addressable()
    {
        return $this->morphTo();
    }
}
