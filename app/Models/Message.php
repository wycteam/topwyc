<?php

namespace App\Models;

use Carbon\Carbon;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes, QueryFilterTrait;

    protected $fillable = [
        'user_id', 'chat_room_id',
        'body',
    ];

    ///////////////////
    // Relationships //
    ///////////////////

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function chat_room()
    {
        return $this->belongsTo(ChatRoom::class);
    }

    // Accessor
    public function getCreatedAtAttribute($value) {
        return (Carbon::parse($value)->isToday()) 
            ? Carbon::parse($value)->format('g:i a') 
            : Carbon::parse($value)->format('F j, Y g:i a');
    }

}
