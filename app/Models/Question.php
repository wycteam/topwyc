<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Question extends Model
{
    protected $fillable = [
        'type', 'question',
    ];

    ///////////////////
    // Relationships //
    ///////////////////

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
