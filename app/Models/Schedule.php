<?php

namespace App\Models;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    
    protected $table = 'schedules';

    protected $primaryKey = 'user_id';

    public $timestamps = false;

    protected $fillable = ['user_id', 'schedule_type_id', 'time_in', 'time_out', 'time_in_from', 'time_in_to'];

    public function user() {
    	return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function scheduleType() {
    	return $this->belongsTo('App\Models\ScheduleType', 'schedule_type_id', 'id');
    }

    // Accessor
    // public function getTimeInAttribute($value) {
    // 	return ($value) ? Carbon::parse($value)->format('h:i A') : null;
    // }

    // public function getTimeOutAttribute($value) {
    // 	return ($value) ? Carbon::parse($value)->format('h:i A') : null;
    // }

    // public function getTimeInFromAttribute($value) {
    // 	return ($value) ? Carbon::parse($value)->format('h:i A') : null;
    // }

    // public function getTimeInToAttribute($value) {
    // 	return ($value) ? Carbon::parse($value)->format('h:i A') : null;
    // }

}
