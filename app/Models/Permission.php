<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
        'label', 'description'
    ];

    protected $appends = [
        'display_name',
    ];

    public function setLabelAttribute($value)
    {
        $this->attributes['name'] = isset($this->attributes['name'])
            ? $this->attributes['name']
            : str_slug($value);

        $this->attributes['label'] = $value;
    }

    public function getDisplayNameAttribute()
    {
        return $this->label . ' ' . $this->type;
    }

    public function users()
    {
        return $this->belongsToMany(User::class)
            ->withTimestamps();
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class)
            ->withTimestamps();
    }
}
