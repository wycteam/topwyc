<?php

namespace App\Models;

use App\Models\User;
use Laravel\Passport\HasApiTokens;
use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Activity;
use App\Classes\Common;

class UserOpen extends Authenticatable
{
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $table = 'users_open';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','client_id', 'password','cp_num',
        'first_name', 'last_name',
        'bday', 'gender',
        'referral_id', 'is_verified', 'verification_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];




    public static function boot()
    {
        parent::boot();
    }

    public function user()
    {
        return $this->belongsTo(User::class,'client_id','id');
    }
    
    ///////////////////
    // Relationships //
    ///////////////////

}
