<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $connection = 'shopping';
	protected $table = 'category_product';
    protected $fillable = [
		 'category_id'
		,'product_id'
    ];

    public $timestamps = true;

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
