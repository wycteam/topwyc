<?php

namespace App\Models\Shopping;
use App\Acme\Traits\QueryFilterTrait;
use App\Acme\Traits\Imageable;
use App\Acme\Traits\Rateable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

use File, Auth, DB;

class ProductReviews extends Model
{
	use QueryFilterTrait, Imageable, Rateable;

	protected $connection = 'shopping';
	protected $table = 'product_reviews';
    protected $fillable = [
		 'user_id'
		,'product_id'
        ,'parent_id'
		,'rating_id'
		,'content'
		,'created_at'
		,'updated_at'
    ];

    protected $appends = ['image', 'image_path', 'star_rating', 'review'];
    protected $path = 'shopping/images/product_review/';

    protected static function boot()
	{
	    parent::boot();
	    static::addGlobalScope('addNeededAttributes', function (Builder $builder) {
    		$builder->with('comments'
                            ,'user'
                            ,'likes'
                            );
            $builder->withCount('likes');
            // $builder->where('approved',1); //this needs to be changed maybe using user roles
            $builder->orderBy('created_at','desc');
        });

        static::created(function ($review) {
            $path = public_path() . '/' . $review->path . $review->id;

            if (! file_exists($path)) {
                mkdir($path, 0777, true);
            }
        });
	}

    public function getImageAttribute()
    {
        $image = $this->path . $this->id . '/review.jpg';
        if (File::exists($image))
        {
            return $image;
        }
        return false;
    }

    public function getImagePathAttribute()
    {
        return $this->path . $this->id . '/';
    }

    public function getStarRatingAttribute()
    {
        $rate = DB::connection('mysql')
            ->table('ratings')
            ->select('rating')
            ->where('id', $this->rating_id)
            ->first();

            //$starRating *=20;
            //$starRating = $starRating->rating;
            if($this->parent_id == 0 && $rate != null){
                return 'width:'.($rate->rating*20).'%';
            }   

        return '';
    }

    public function getReviewAttribute()
    {
        $data = DB::connection('mysql')
            ->table('ratings')
            ->select('review')
            ->where('id', $this->rating_id)
            ->first(); 

        return $data->review;
    }

    //before saving
    public function save(array $options = array())
	{
        if( ! $this->user_id)
        {
	    	$this->user_id = Auth::user()->id;
	    }
	    parent::save($options);

	}

    ///////////////////
    // Relationships //
    ///////////////////

    public function comments(){
    	return $this->hasMany('App\Models\Shopping\ProductReviews','parent_id')->orderBy('created_at','desc');
    }

    public function user(){
    	return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function products(){
        return $this->belongsTo('App\Models\Shopping\Product', 'product_id');
    }

    public function likes(){
        return $this->hasMany('App\Models\Shopping\ProductReviewLikes','review_id');
    }
}
