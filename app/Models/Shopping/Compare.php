<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class Compare extends Model
{
    protected $connection = 'shopping';
	protected $table = 'compare';
    protected $fillable = [
		 'user_id'
		,'product_id'
    ];

    public $timestamps = true;

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}


