<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    protected $connection = 'shopping';
	protected $table = 'product_images';
    protected $fillable = [
		 'product_id'
		,'filename'
		,'created_at'
		,'updated_at'
    ];
}
