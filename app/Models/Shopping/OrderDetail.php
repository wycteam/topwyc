<?php

namespace App\Models\Shopping;

use App\Acme\Traits\Fileable;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    use Fileable;

	protected $connection = 'shopping';

    protected $fillable = [
		'order_id', 'product_id', 'tracking_number',
		'quantity', 'color', 'size', 'price_per_item',
        'sale_price', 'discount', 'disc_fee', 'shipping_fee' , 'subtotal',
		'status', 'weight','length','width','height', 'shipment_type', 'courier', 
        'remarks', 'seller_notes', 'reason', 'case_number',
        'pickup_location', 'shipped_date', 'received_date', 
        'returned_received_date',
        'returned_date', 'created_at'
    ];

    public static function boot()
    {
        parent::boot();

        static::updated(function ($order) {
            if (($order->status === 'In Transit' && $order->tracking_number == null)) {
                $order->tracking_number = 'TNWYC-'.$order->id.strtoupper(str_random(8));
                $order->save();
            }
        });
    }

    protected $dates = ['shipped_date', 'received_date', 'returned_date'];

    protected $appends = ['combined_tracking_number'];

    public function getCombinedTrackingNumberAttribute()
    {
        return empty($this->tracking_number)
            ? $this->order->tracking_number
            : $this->order->tracking_number.'-'.$this->tracking_number;
    }

    ///////////////////
    // Relationships //
    ///////////////////

    public function order()
    {
    	return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
