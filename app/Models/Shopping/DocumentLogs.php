<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class DocumentLogs extends Model
{
    protected $connection = 'shopping';
	protected $table = 'document_logs';
    protected $fillable = [
		 'user_id'
		,'doc_id'
		,'processor_id'
    ];
}
