<?php

namespace App\Models\Shopping;

use App\Models\User;
use App\Acme\Traits\Imageable;
use App\Acme\Traits\Numberable;
use App\Acme\Traits\Addressable;
use App\Acme\Traits\Documentable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Store extends Model
{
    use SoftDeletes,
        Addressable, Numberable, Imageable, Documentable;

    protected $connection = 'shopping';

    protected $fillable = [
        'user_id',
        'slug', 'name', 'cn_name', 'description', 'cn_description', 'email',
        'status', 'reason', 'is_active',
    ];

    protected $appends = ['image_path', 'cover_image', 'profile_image', 'full_address', 'latest_prod','pending_orders', 'orders','store_order_status','address_detail'];

    protected $path = 'shopping/images/store/';

    public static function boot()
    {
        parent::boot();

        static::creating(function ($store) {
            if (Store::whereSlug($store->slug)->count()) {
                $store->slug = $store->slug . '-' . str_random(5);
            }
        });

        static::created(function ($store) {
            $path = public_path() . '/' . $store->path . $store->id;

            if (! file_exists($path)) {
                mkdir($path, 0777, true);
            }
        });
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function setNameAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
        $this->attributes['name'] = $value;
    }

    public function getImagePathAttribute()
    {
        return $this->path . $this->id . '/';
    }

    public function getCoverImageAttribute()
    {
        $coverImage = $this->images->where('type', 'cover')->first();

        if (! is_null($coverImage)) {
            return asset($this->image_path . $coverImage->name);
        }

        return 'https://dummyimage.com/1140x300/b0b0b0/f5f5f5&text=Cover+Photo';
    }

    public function getProfileImageAttribute()
    {
        $profileImage = $this->images->where('type', 'profile')->first();

        if (! is_null($profileImage)) {
            return asset($this->image_path . $profileImage->name);
        }

        return asset('images/avatar/default.jpg');
    }

    public function getAddressDetailAttribute()
    {
        $address = $this->addresses->first();

        if (! is_null($address)) {
            return $address;
        }

        return 'N/A';
    }

    public function getFullAddressAttribute()
    {
        $address = $this->addresses->first();

        if (! is_null($address)) {
            return $address->address . ', ' . $address->city . ', ' . $address->province . ', ' . $address->country;
        }

        return 'N/A';
    }

    public function getLatestProdAttribute()
    {
        $latest = $this->products()->where('is_searchable',1)->orderBy('id','desc')->take(3)->get();

        if (! is_null($latest)) {
            return $latest;
        }

        return '';
    }

    public function getPendingOrdersAttribute()
    {
        return $this->pending_order_count()->count();
    }

    public function getContactNumberAttribute()
    {
        $number = $this->numbers->first();

        if (! is_null($number)) {
            return $number->number;
        }

        return 'N/A';
    }

    public function getStoreOrderStatusAttribute()
    {
        if(! Auth::check() ) {
            return;
        }

        $orders = $this->orders->all();
        $ctr = [];
        foreach($orders as $o){
            $ctr[$o->id][] = $this->order_details->where('order_id',$o->id)->where('status','Pending')->count();
        }
        return $ctr;
    }

    //////////////////////
    // Custom Functions //
    //////////////////////

    public function saveAddress($address)
    {
        return $this->addresses()->create($address);
    }

    public function saveNumber($number)
    {
        return $this->numbers()->create($number);
    }

    public function saveCategories($categoryIds)
    {
        return $this->categories()->attach($categoryIds);
    }


    ///////////////////
    // Relationships //
    ///////////////////

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function city()
    {
        return $this->belongsTo(\App\Models\City::class);
    }

    public function province()
    {
        return $this->belongsTo(\App\Models\Province::class);
    }

    public function order_details()
    {
        return $this->hasManyThrough(OrderDetail::class, Product::class)->orderBy('id', 'desc');
    }

    public function orders()
    {
        return $this->hasManyThrough(Order::class,OrderDetail::class, Product::class)->orderBy('id', 'desc');
    }

    public function company()
    {
        return $this->hasOne(StoreCompany::class);

    }

    public function pending_order_count()
    {
        return $this->order_details()
                  ->where('status','Pending')->orderBy('id', 'desc');
    }

   public function shipped_order_count()
    {
        return $this->order_details()           
            ->where('status', 'Shipped')->orderBy('id', 'desc');
    }

   public function delivered_order_count()
    {
        return $this->order_details()
            ->where('status', 'Delivered')->orWhere('status', 'Received')->orderBy('id', 'desc');
    }

   public function cancelled_order_count()
    {
        return $this->order_details()
            ->where('status', 'Cancelled')->orderBy('id', 'desc');
    }

       public function transit_order_count()
    {
        return $this->order_details()
            ->where('status', 'In Transit')->orderBy('id', 'desc');
    }

    public function zero_product_count(){
        return $this->products()
            ->selectRaw('id, store_id, COUNT(*) AS aggregate')
            ->where('stock', 0)
            ->groupBy('products.id');
    }

    public function getSuspendedProducts(){
        return $this->products()
            ->orderBy('id','desc')
            ->where('is_searchable', 0);
    }

    public function getOrdersAttribute() {
        if(! Auth::check() ) {
            return;
        }

        $orderIdArray = $this->order_details->unique('order_id')->pluck('order_id');

        return \App\Models\Shopping\Order::with('user', 'address', 'details.product')
            ->with(['details' => function($query1) {
                $query1->whereHas('product', function($query2) {
                    $query2->where('user_id', Auth::user()->id);
                });
            }])
            ->whereIn('id', $orderIdArray)
            ->orderBy('id', 'desc')
            ->get();
    }
}