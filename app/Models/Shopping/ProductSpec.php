<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class ProductSpec extends Model
{
    protected $connection = 'shopping';

    protected $fillable = [
        'product_id',
        'label', 'description',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
