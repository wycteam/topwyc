<?php

namespace App\Models\Shopping;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Shopping\Friends;
use Illuminate\Database\Eloquent\Model;
use Auth;

class Friends extends Model
{
	use QueryFilterTrait;

    protected $connection = 'shopping';
	protected $table = 'friends';
	protected $fillable = [
		 'user_id'
		,'user_id2'
		,'status'
    ];

    // Relationships
    public function user(){
    	return $this->belongsTo('App\Models\User','user_id');
    }

    public function user2(){
    	return $this->belongsTo('App\Models\User','user_id2');
    }

    public static function getByStatus($status){

		$friends = Friends::with('user','user2')
			->where('status',$status)
			->where(function($query) {
                 $query->where('user_id', Auth::user()->id);
                 $query->orWhere('user_id2', Auth::user()->id);
             })
			->orderBy('updated_at','asc')
			->get();

		$data = [];

		foreach ($friends as  $item) {
			$temp = [];
			$temp['date'] = $item->updated_at->diffForHumans();

			if($item->user->id == Auth::user()->id)
			{
				$temp['user'] = $item->user2;
				$temp['requestor'] = true;
			}else
			{
				$temp['user'] = $item->user;
				$temp['requestor'] = false;
			}
			array_push($data, $temp);
		}

		return $data;
    }



    protected static function boot()
	{
	    parent::boot();
	  //   static::addGlobalScope('addNeededAttributes', function (Builder $builder) {
   //  		$builder->with(
   //  			 'user'
   //  			,'user2'
			// );
   //      });
	}

 //    public function save(array $options = array())
	// {
	// 	$current_user = Auth::user()->id;

	//     if (! $this->user_id) {
	//     	$user_id = $current_user;
	//         $this->user_id = $user_id;
	//     }

	//     $count = Friends::where(function($query) use ($current_user)  {
 //            $query->where('user_id', $current_user)
 //                ->where('user_id2', $this->user_id2);
 //        })->orWhere(function($query) use ($current_user){
 //         	$query->where('user_id', $this->user_id2)
 //                ->where('user_id2', $current_user);
 //        })->count();

 //        if (!$count) {
 //            parent::save($options);
 //        }
	// }
}
