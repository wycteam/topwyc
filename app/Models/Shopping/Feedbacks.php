<?php

namespace App\Models\Shopping;

use App\Acme\Traits\QueryFilterTrait;
use App\Acme\Traits\Imageable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Shopping\FeedbackLikes;

use Auth,File;

class Feedbacks extends Model
{
	use QueryFilterTrait, Imageable;

    protected $connection = 'shopping';
	protected $table = 'feedbacks';
    protected $fillable = [
		 'user_id'
		,'parent_id'
		,'content'
		,'approved'
		,'processor_id'
    ];


    protected $appends = ['image_path','image'];
    protected $path = 'shopping/images/feedbacks/';

	protected static function boot()
	{
	    parent::boot();
	    static::addGlobalScope('addNeededAttributes', function (Builder $builder) {
    		$builder->with('comments'
    						,'user'
    						,'likes'
							);
            $builder->withCount('likes');
            $user = Auth::user();
            if(($user->hasRole('cpanel-admin') || $user->hasRole('master')))
            {
                $builder->with('processor');
            }else
            {
        		$builder->where('approved',1); //show only approved for normal users
            }
    		$builder->orderBy('updated_at','desc');

        });

        static::created(function ($feedback) {
            $path = public_path() . '/' . $feedback->path . $feedback->id;

            if (! file_exists($path)) {
                mkdir($path, 0777, true);
            }
        });
	}

	public function getImagePathAttribute()
    {
        return $this->path . $this->id . '/';
    }

    public function getImageAttribute()
    {
        $image = $this->path . $this->id . '/feedback.jpg';
        if (File::exists($image))
        {
            return $image;
        }
        return false;
    }

	public function save(array $options = array())
	{
	    if( ! $this->user_id)
	    {
	    	$user_id = Auth::user()->id;
	        $this->user_id = $user_id;
	    }

	    if($this->parent_id)
	    {
	    	$this->approved = true;
	    }
	    parent::save($options);

	}

	///////////////////
    // Relationships //
    ///////////////////

    public function comments(){
    	return $this->hasMany('App\Models\Shopping\Feedbacks','parent_id')->orderBy('created_at','desc');
    }

    public function user(){
    	return $this->belongsTo('App\Models\User', 'user_id');
    }
    public function processor(){
        return $this->belongsTo('App\Models\User', 'processor_id');
    }

    public function likes(){
    	return $this->hasMany('App\Models\Shopping\FeedbackLikes','feedback_id');
    }
	
}
