<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class ProductReport extends Model
{
    protected $connection = 'shopping';
	protected $table = 'product_reports';
    protected $fillable = [
		 'issue_id'
		,'product_id'
    ];

    public $timestamps = true;

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
