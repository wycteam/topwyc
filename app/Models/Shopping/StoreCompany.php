<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class StoreCompany extends Model
{
    protected $fillable = [
        'store_id',
        'name', 'address',
        'facebook', 'twitter', 'instagram',
    ];

    ///////////////////
    // Relationships //
    ///////////////////

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
