<?php

namespace App\Models\Shopping;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use QueryFilterTrait;

    protected $connection = 'shopping';
    protected $table = 'categories';
    protected $fillable = [
		 'name'
        ,'name_cn'
		,'slug'
		,'parent_id'
		,'active'
		,'featured'
		,'tree_parent'
		,'created_at'
		,'updated_at'
    ];

    public function save(array $options = array())
    {
        if( !$this->parent_id)
        {
            $this->parent_id = 0;
        }

        //create tree level
        $tree_parent = '';
        $parent = $this->find($this->parent_id);
        if($parent){
            $grandparent = $this->find($parent->parent_id);
            if($grandparent)
            {
                $tree_parent .= $grandparent->id .'-'. $parent->id;
            }else
            {
                $tree_parent .= $parent->id;
            }
        }

        $this->tree_parent = $tree_parent;
        
        parent::save($options);
    }

    public static function getSidebarCategoryLookups(&$out_tree, &$out_cats)
    {
        $arranged = [];
        $cats = Category::where('active', '=', 1)
            ->where('featured', '=', 1)
            ->get();

        self::arrangeTreetably(0, $cats, $arranged);

        foreach ($arranged as $category) {
            $cat_id = $category->id;

            $out_cats[$cat_id] = [
                'id' => $category->id,
                'slug' => $category->slug,
                'icon' => $category->icon,
                'name' => $category->name,
                'parent' => $category->parent_id,
                'img_home' => $category->img_home,
                'img_primary' => $category->img_primary,
                'tree_parent' => $category->tree_parent,
            ];

            if(!is_null($category->tree_parent)) {
                $tp = explode('-', $category->tree_parent);
                switch (count($tp)) {
                    case 1: $out_tree[$tp[0]][$cat_id] = []; break;
                    case 2: $out_tree[$tp[0]][$tp[1]][$cat_id] = []; break;
                    case 3: $out_tree[$tp[0]][$tp[1]][$tp[2]][$cat_id] = []; break;
                    // current cats is up to 3dimentions only, extend if needed
                    // update: as per boss, we will limit cats at 3rd level deep only
                }
            }
        }
    }

    // -- getSidebarCategoryLookups helper / also used in category selection that needs tree (e.g. cat add page)

    public static function arrangeTreetably($parent_id, &$main_src, &$arranged_categories)
    {
        $new_src = [];
        foreach ($main_src as $k => $src) {

            if($src->parent == $parent_id) {
                $new_src[$k] = $src;
            }
        }
        foreach ($new_src as $k => $src) {
            $arranged_categories[$k] = $src; // push to dest
            unset($main_src[$k]); // pop  from src, less data = fast execution

            // call self, but this is in another mem space
            self::arrangeTreetably($src->id, $main_src, $arranged_categories);
        }
    }

    public function stores()
    {
        return $this->belongsToMany(Store::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

}
