<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class Banks extends Model
{
    protected $connection = 'shopping';
	protected $table = 'banks';
    protected $fillable = [
		 'name'
		,'shortname'
		,'account_number'
		,'active'
    ];

    public $timestamps = false;

    protected $appends = [
        'name_number'
    ];

    public function getNameNumberAttribute() {
        if($this->account_number!=null){
        	return $this->name .' ('.$this->account_number.')';
        }
        return $this->name;
    }
}
