<?php

namespace App\Models\Shopping;
use App\Models\Shopping\Banks;
use App\Models\Shopping\BankAccounts;
use App\Models\Shopping\Ewallet;
use App\Models\Shopping\EwalletLogs;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use App\Notifications\ChangeEwalletTransactionStatus;
use App\Notifications\NewEwalletTransaction;

use App\Acme\Traits\QueryFilterTrait;

class EwalletTransactions extends Model
{
    use QueryFilterTrait;

    protected $connection = 'shopping';
	protected $table = 'ewallet_transactions';
    protected $fillable = [
		 'user_id'
		,'type'
		,'ref_number'
		,'amount'
		,'description'
		,'status'
		,'processor_id'
		,'balance'
		,'deposit_slip'
		,'reason'
		,'rcvr_id'
    ];

    public function user(){
    	return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function receiver(){
    	return $this->belongsTo('App\Models\User', 'rcvr_id');
    }

    public function processor(){
    	return $this->belongsTo('App\Models\User', 'processor_id');
    }

    public function logs(){
    	return $this->hasMany('App\Models\Shopping\EwalletLogs','transaction_id');
    }


    /**
	 * The "booting" method of the model.
	 *
	 * @return void
	 */
	protected static function boot()
	{
	    parent::boot();

	    static::addGlobalScope('current_user', function (Builder $builder) {
	    	$user = \Auth::user();
    		$builder->with('user','receiver','processor');
	    	if (! (
	    			$user->hasRole('cpanel-admin') || 
	    			$user->hasRole('master')  || 
	    			$user->hasRole('vice-master')  || 
	    			$user->hasRole('support') 
	    		)) {
		 		$builder
		 			->where('user_id', $user->id)
		 			->orWhere('rcvr_id', $user->id);
	    	}
        });
	}

	public function save(array $options = array())
	{
		$user_id = \Auth::user()->id;
	    if( ! $this->user_id)
	    {
	        $this->user_id = $user_id;
	     	$this->ref_number = -1*createSN('',rand(8,99999999),8);
	    }

	    if(! $this->balance)
	    {
	     	$this->balance = Ewallet::whereUserId($user_id)->first()->amount;
	    }

		    switch ($this->type) {
		    	case 'Received':

		    	break;
		    	case 'Withdraw':

		    		$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		    		//in cpanel
					if ($this->id) {
					// if (strpos($url,env("WATAF_CPANEL_SUBDOMAIN", "cpanel.wataf.ph")) !== false) {
						
		    			$this->processor_id = $user_id;
		    			EwalletLogs::create([
		    				 'user_id'			=> $this->user_id
							,'processor_id'		=> $this->processor_id
							,'transaction_id'	=> $this->id
							,'action'			=> 'to '. $this->status .' '.$this->reason
		    			]);
			    		
			    		$this->user->notify(new ChangeEwalletTransactionStatus($this, User::findOrFail($this->user->id)));
					} 
					else {
						
				    	$bank_account = BankAccounts::find(request()->bank_id);
				    	$bank = Banks::find($bank_account->bank_id);
				    	$this->description = $bank->shortname.'('.$bank_account->account_name.':'.$bank_account->account_number.')';
					}

		    		break;
		    		
		    	case 'Transfer':
		    		if(!$this->rcvr_id)
		    		{
				    	$user = User::find(request()->bank_id);
				    	$this->rcvr_id = $user->id;
				    	$this->description = 'Receiver: '.$user->email;
		    		}
		    		break;
		    	
		    	default:
			    	$bank = Banks::find(request()->bank_id);
		    		$this->type = 'Deposit';
		    		if(! $this->description)
		    		{
				    	$this->description = $bank->shortname;
				    	
		    		}

		    		if($this->id)
		    		{
		    			$user = User::findOrFail($user_id);
			    		if(  
			    			$user->hasRole('cpanel-admin') || 
			    			$user->hasRole('master')  || 
			    			$user->hasRole('support') 
			    		)
			    		{
			    			$this->processor_id = $user_id;
			    			EwalletLogs::create([
			    				 'user_id'			=> $this->user_id
								,'processor_id'		=> $this->processor_id
								,'transaction_id'	=> $this->id
								,'action'			=> 'to '. $this->status .' '.$this->reason
			    			]);

			    			$this->user->notify(new ChangeEwalletTransactionStatus($this, User::findOrFail($user_id)));
			    		}
		    		}
		    		break;
		    }
	    

	    if(! $this->status)
	    {
	    	$this->status = 'Pending';
	    }
	   	parent::save($options);
	   
	}

	private function returnEwallet($status){

	}

	
}
