<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class ProductVariationSizes extends Model
{
    protected $connection = 'shopping';

    protected $fillable = [
        'product_id','color_id','count_id',
        'label','stock'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
