<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;
use App\Acme\Traits\Addressable;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    use Addressable;

	protected $connection = 'shopping';

    protected $fillable = [
		'user_id', 'address_id',
		'total', 'tax', 'shipping_fee',
        'tracking_number',
		'status', 'name_of_receiver',
		'contact_number', 'alternate_contact_number',
        'balance'
    ];

    protected $appends = ['order_details_status'];

    //////////////////////
    // Custom Functions //
    //////////////////////

    public function saveAddress($address)
    {
        return $this->addresses()->create($address);
    }

    ///////////////////
    // Relationships //
    ///////////////////

    public function address()
    {
        return $this->belongsTo(\App\Models\Address::class);
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function getOrderDetailsStatusAttribute() {
        if(! Auth::check() ) {
            return;
        }

        $user = Auth::user();
        $prods = $user->products->pluck('id');
        return $this->details()->where('status','Pending')->whereIn('product_id',$prods)->count();
    }

}