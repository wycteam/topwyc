<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;
use App\Models\Shopping\EwalletTransactions;
use App\Models\Shopping\Order;

use Carbon\Carbon ;

class Ewallet extends Model
{
    protected $connection = 'shopping';
	protected $table = 'ewallet';
    protected $fillable = [
		 'user_id'
		,'card_number'
		,'amount'
    ];

    protected $appends = [
        'usable_ewallet'
    ];

    public function user(){
    	return $this->belongsTo('App\Models\User','user_id');
    }

    public function transactions(){
        return $this->hasMany('App\Models\Shopping\EwalletTransactions','user_id','user_id');
    }

    public function getUsableEwalletAttribute()
    {
        $user = \Auth::user();

        // $considered_days = Carbon::now()->addDays(10);
        // subtract pending and (shipped that exceeds 10 days)
        // $orders = Order::
        //     with([ 'details' => function($query) use ($considered_days) {
        //         $query->where('status','Pending');
        //         $query->orWhere(function($q) use ($considered_days){
        //             $q->where('status','Shipped');
        //             //less than or equal to 10 days from now === understood as delivered
        //             $q->whereDate( 'updated_at', '<=', $considered_days);
        //         });
        //     }])
        //     ->where('user_id', $this->user->id)
        //     ->get();
            
        $not_usable = 0;
        
        //from orders
        // foreach($orders as $order)
        // {
        //     foreach($order->details as $detail)
        //     {
        //         $not_usable += $detail->subtotal;
        //     }
        // }
        
        //from ewallet transactions == cannot use for transfer and withdraws
        $not_usable += $this->transactions()
            ->where(function($query){
                $query->where('status','Pending');
                $query->orWhere('status','Processing');
                $query->orWhere('status','On Hold');
            })
            ->where('type','<>','Deposit')
            ->get()
            ->pluck('amount')
            ->sum();

        // $test = App\Models\Shopping\Ewallet::find(5)->usable_ewallet
        //return ''.$this->amount .'-'. $not_usable .'-'. $this->user->ewallet_on_hold;
        return $this->amount - ($not_usable + $this->user->fee_on_hold);
    }
 
}
