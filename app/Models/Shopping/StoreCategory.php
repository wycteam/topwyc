<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class StoreCategory extends Model
{
    protected $connection = 'shopping';
	protected $table = 'store_category';
    protected $fillable = [
		 'cat_id'
		,'store_id'
    ];
    public $timestamps = false;
}
