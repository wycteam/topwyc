<?php

namespace App\Models\Shopping;

use App\Models\User;
use App\Acme\Traits\Rateable;
use App\Acme\Traits\Imageable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Acme\Traits\QueryFilterTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
use Auth;

class Product extends Model
{
    use SoftDeletes, Rateable, Imageable,QueryFilterTrait,SearchableTrait;

	protected $connection = 'shopping';
	protected $table = 'products';
    protected $fillable = [
        'brand',
        'user_id', 'store_id',
        'slug', 'name', 'description','cn_name', 'cn_description',
        'stock', 'sold_count',
        'price', 'sale_price', 'discount',
        'shipment_type','boxContent',
        'wPeriod','weight','length','width','height','package_type',
        'shipping_fee','charge', 'vat',
        'shipping_fee_2go','fee_included', 'vat_2go',
        'is_draft', 'is_active', 'is_approved', 'is_searchable',
    ];


    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'products.name' => 20,
            'stores.name'   => 3,
        ],
        'joins' => [
            'stores' => ['products.store_id','stores.id'],
        ],
    ];

	protected $appends = ['image_path', 'image_path_color', 'main_image', 'main_image_thumbnail', 'main_image2', 'main_image3', 'main_image4', 'main_image5', 'main_image6', 'desc_image', 'desc_image2', 'desc_image3', 'desc_image4', 'desc_image5', 'desc_image6', 'desc_image7','desc_image8', 'desc_image9', 'desc_image10', 'desc_image11', 'desc_image12', 'desc_image13','desc_image14', 'desc_image15', 'desc_image16', 'desc_image17', 'desc_image18', 'desc_image19','desc_image20', 'product_rating', 'rating_count', 'product_cats', 'product_color_variations', 'product_size_variations', 'product_size_variations1', 'product_size_variations2', 'product_size_variations3', 'product_size_variations4', 'product_size_variations5', 'product_size_variations6', 'product_size_variations7', 'product_size_variations8', 'product_size_variations9', 'product_size_variations10', 'my_product'];


    protected $path = 'shopping/images/product/';

    public static function boot()
    {
        parent::boot();

        static::creating(function ($product) {
            if (Product::whereSlug($product->slug)->count()) {
                $product->slug = $product->slug . '-' . str_random(5);
            }
        });

        static::created(function ($product) {
            $path = public_path() . '/' . $product->path . $product->id;

            if (! file_exists($path)) {
                mkdir($path, 0777, true);
            }

            $path2 = public_path() . '/' . $product->path . $product->id . '/color';

            if (! file_exists($path2)) {
                mkdir($path2, 0777, true);
            }
        });

        static::updated(function ($product) {
            $path2 = public_path() . '/' . $product->path . $product->id . '/color';

            if (! file_exists($path2)) {
                mkdir($path2, 0777, true);
            }
        });
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    // public function getNameAttribute(){
    //     if( $this->usrlang == 'cn')
    //         return $this->attributes['cn_name']? : $this->attributes['name'];
    //     return $this->attributes['name'];
    // }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function setNameAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
        $this->attributes['name'] = $value;
    }

    public function getImagePathAttribute()
    {
        return $this->path . $this->id . '/';
    }

    public function getImagePathColorAttribute()
    {
        return $this->path . $this->id . '/color/';
    }

    public function getMainImageAttribute()
    {
        $mainImage = $this->images->where('type', 'primary')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/'.$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return asset('images/avatar/default-product.jpg');
    }

    public function getMainImageThumbnailAttribute()
    {
        $mainImage = $this->images->where('type', 'primary-thumbnail')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/'.$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return asset('images/avatar/default-product.jpg');
    }

    public function getMainImage2Attribute()
    {
        $mainImage2 = $this->images->where('type', 'primary2')->first();

        if (! is_null($mainImage2)) {
            return asset('/shopping/images/product/' .$mainImage2->imageable_id.'/'. $mainImage2->name);
        }

        return '';
    }

    public function getMainImage3Attribute()
    {
        $mainImage = $this->images->where('type', 'primary3')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getMainImage4Attribute()
    {
        $mainImage = $this->images->where('type', 'primary4')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getMainImage5Attribute()
    {
        $mainImage = $this->images->where('type', 'primary5')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getMainImage6Attribute()
    {
        $mainImage = $this->images->where('type', 'primary6')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImageAttribute()
    {
        $mainImage = $this->images->where('type', 'descimage1')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage2Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage2')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage3Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage3')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage4Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage4')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage5Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage5')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage6Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage6')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage7Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage7')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage8Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage8')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage9Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage9')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage10Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage10')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage11Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage11')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage12Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage12')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage13Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage13')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage14Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage14')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage15Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage15')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage16Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage16')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage17Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage17')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage18Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage18')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage19Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage19')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getDescImage20Attribute()
    {
        $mainImage = $this->images->where('type', 'descimage20')->first();

        if (! is_null($mainImage)) {
            return asset('/shopping/images/product/' .$mainImage->imageable_id.'/'. $mainImage->name);
        }

        return '';
    }

    public function getProductRatingAttribute()
    {
        $likeCountGood = 0;
        foreach($this->reviews as $r) {
            if($r->review == 'good') {
                $likeCountGood += $r->likes_count;
            }
        }

        $likeCountBad = 0;
        foreach($this->reviews as $r) {
            if($r->review == 'bad') {
                $likeCountBad += $r->likes_count;
            }
        }
        //divisor cannot be zero
        $divisor = (($this->sumRating + ($likeCountGood*0.1))+($this->averageRating + ($likeCountBad*0.1)));
        if($divisor > 0){
            $productRating = ($this->sumRating + ($likeCountGood*0.1))/$divisor;

            if (! is_null($productRating)) {
                return round($productRating * 100);
            }
        }
        return '0';
    }

    public function getRatingCountAttribute()
    {
        $ratingCount = $this->ratings;
        return $ratingCount;

        //return '0';
    }

    public function getProductCatsAttribute()
    {
        $cats = $this->categories()->select('id','name')->get();
        return $cats;
    }

    public function getProductColorVariationsAttribute()
    {
        $vars = $this->color_variations()->get();
        return $vars;
    }

    public function getProductSizeVariationsAttribute()
    {
        $vars = $this->size_variations()->get();
        return $vars;
    }

    public function getProductSizeVariations1Attribute()
    {
        $vars = $this->size_variations()->where('count_id','=','1')->get();
        return $vars;
    }

    public function getProductSizeVariations2Attribute()
    {
        $vars = $this->size_variations()->where('count_id','=','2')->get();
        return $vars;
    }

    public function getProductSizeVariations3Attribute()
    {
        $vars = $this->size_variations()->where('count_id','=','3')->get();
        return $vars;
    }

    public function getProductSizeVariations4Attribute()
    {
        $vars = $this->size_variations()->where('count_id','=','4')->get();
        return $vars;
    }

    public function getProductSizeVariations5Attribute()
    {
        $vars = $this->size_variations()->where('count_id','=','5')->get();
        return $vars;
    }

    public function getProductSizeVariations6Attribute()
    {
        $vars = $this->size_variations()->where('count_id','=','6')->get();
        return $vars;
    }

    public function getProductSizeVariations7Attribute()
    {
        $vars = $this->size_variations()->where('count_id','=','7')->get();
        return $vars;
    }

    public function getProductSizeVariations8Attribute()
    {
        $vars = $this->size_variations()->where('count_id','=','8')->get();
        return $vars;
    }

    public function getProductSizeVariations9Attribute()
    {
        $vars = $this->size_variations()->where('count_id','=','9')->get();
        return $vars;
    }

    public function getProductSizeVariations10Attribute()
    {
        $vars = $this->size_variations()->where('count_id','=','10')->get();
        return $vars;
    }

    public function getMyProductAttribute() {
        if(!Auth::check()) {
            return false;
        }

        return $this->user_id == Auth::user()->id;
    }

    //////////////////////
    // Custom Functions //
    //////////////////////

    public function saveCategories($categoryIds)
    {
        return $this->categories()->attach($categoryIds);
    }

    public function saveSpecs()
    {
        //
    }


    ///////////////////
    // Relationships //
    ///////////////////

    public function user()
    {
    	return $this->belongsTo(\App\Models\User::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class,'store_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function reviews()
    {
        return $this->hasMany(ProductReviews::class);
    }

    public function order_details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function color_variations()
    {
        return $this->hasMany(ProductVariationColors::class);
    }

    public function size_variations()
    {
        return $this->hasMany(ProductVariationSizes::class);
    }
}
