<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class EwalletLogs extends Model
{
    protected $connection = 'shopping';
    protected $table = 'ewallet_logs';
    protected $dates = ['created_at'];
    
    protected $fillable = [
		 'user_id'
		,'processor_id'
		,'transaction_id'
		,'action'
    ];

    public $timestamps = false;

    public function transaction(){
        return $this->belongsTo('App\Models\Shopping\EwalletTransaction','transaction_id');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }
}
