<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class BrandCategory extends Model
{
    protected $connection = 'shopping';
	protected $table = 'brand_category';
    protected $fillable = [
		 'cat_id'
		,'brand_id'
    ];

    public $timestamps = false;
}
