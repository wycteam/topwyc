<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    protected $connection = 'shopping';
	protected $table = 'brands';
    protected $fillable = [
		 'store_id'
		,'name'
		,'slug'
		,'image'
		,'created_at'
		,'updated_at'
    ];
}
