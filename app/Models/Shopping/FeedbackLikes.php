<?php

namespace App\Models\Shopping;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

class FeedbackLikes extends Model
{
    use QueryFilterTrait;

    protected $connection = 'shopping';
	protected $table = 'feedlikes';
    protected $fillable = [
		 'user_id'
		,'feedback_id'
		,'created_at'
    ];

   public function feedback(){
    	return $this->belongsTo('App\Models\Shopping\Feedbacks','feedback_id');
    }
}
