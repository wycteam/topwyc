<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class BrandRequests extends Model
{
	protected $connection = 'shopping';
	protected $table = 'brand_requests';
    protected $fillable = [
		 'product_id'
		,'store_id'
		,'user_id'
		,'name'
		,'status'
		,'reason'
		,'created_at'
		,'updated_at'
    ];
}
