<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class ShopUsers extends Model
{
	protected $connection = 'shopping';
	protected $table = 'shop_users';
    protected $fillable = [
		 'user_id'
		,'sn'
		,'avatar'
		,'created_at'
		,'updated_at'
		,'online'
		,'status'
    ];
}
