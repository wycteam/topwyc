<?php

namespace App\Models\Shopping;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

class ProductReviewLikes extends Model
{
    use QueryFilterTrait;
	
	
    protected $connection = 'shopping';
	protected $table = 'product_review_likes';
    protected $fillable = [
		 'user_id'
		,'review_id'
		,'created_at'
    ];
}
