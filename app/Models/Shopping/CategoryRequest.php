<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class CategoryRequest extends Model
{
	protected $connection = 'shopping';
	protected $table = 'category_request';
    protected $fillable = [
		 'product_id'
		,'store_id'
		,'user_id'
		,'name'
		,'status'
		,'reason'
		,'created_at'
		,'updated_at'
    ];
}
