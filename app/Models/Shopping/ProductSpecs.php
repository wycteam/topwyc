<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class ProductSpecs extends Model
{
    protected $connection = 'shopping';
	protected $table = 'product_specs';
    protected $fillable = [
    	 'product_id'
		,'label'
		,'description'
		,'create_at'
		,'updated_at'
    ];
}
