<?php

namespace App\Models\Shopping;

use Illuminate\Database\Eloquent\Model;

class BankAccounts extends Model
{
    protected $connection = 'shopping';
	protected $table = 'bank_accounts';
    protected $fillable = [
		 'user_id'
		,'bank_id'
		,'account_name'
		,'account_number'
		,'status'
    ];

    public $timestamps = false;

    public function bank(){
    	return $this->belongsTo('App\Models\Shopping\Banks', 'bank_id');
    }
}
