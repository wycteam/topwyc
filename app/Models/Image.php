<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $connection = 'mysql';

    protected $fillable = [
        'name', 'type',
        'imageable_id', 'imageable_type',
        'is_thumbnail',
    ];

    public function imageable()
    {
        return $this->morphTo();
    }
}
