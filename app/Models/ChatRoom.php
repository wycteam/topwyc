<?php

namespace App\Models;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;

class ChatRoom extends Model
{
    use QueryFilterTrait;

    protected $fillable = [
        'name', 'settings', 'is_active',
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    ///////////////////
    // Relationships //
    ///////////////////

    public function users()
    {
        return $this->belongsToMany(User::class)
            ->withTimestamps();
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function latest_message()
    {
        return $this->hasOne(Views\LatestMessage::class);
    }

    //////////////////////
    // Custom Functions //
    //////////////////////

    public function addMember($userId)
    {
        $this->users()->attach($userId);
    }
}
