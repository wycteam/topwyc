<?php

namespace App\Models;

use App\Acme\Traits\QueryFilterTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class Issue extends Model
{
    use SoftDeletes, QueryFilterTrait;

    protected $fillable = [
        'user_id', 'issue_number',
        'subject', 'body',
        'status',
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function ($issue) {
            $issue->issue_number = 'IN-'.$issue->id.strtoupper(str_random(5));
            $issue->save();
        });

        static::updated(function ($issue) {
            if (($issue->status === 'closed') && $issue->chat_room && $issue->chat_room->is_active) {
                $issue->chat_room->is_active = false;
                $issue->chat_room->save();
            } elseif (($issue->status === 'open') && $issue->chat_room && ! $issue->chat_room->is_active) {
                $issue->chat_room->is_active = true;
                $issue->chat_room->save();
            }
        });
    }

    ///////////////////
    // Relationships //
    ///////////////////

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function chat_room()
    {
        return $this->hasOne(ChatRoom::class, 'name', 'issue_number');
    }

    //////////////////////
    // Custom Functions //
    //////////////////////

    public function assignSupportToChatRoom($userIds)
    {
        if ($this->chat_room) {
            $this->chat_room->addMember($userIds);

            return $this->chat_room;
        } else {
            $chatRoom = $this->chat_room()->create(['name' => $this->issue_number]);
            $chatRoom->addMember(is_array($userIds)
                ? array_merge($userIds, [$this->user_id])
                : [$userIds, $this->user_id]
            );

            return $chatRoom;
        }
    }

    public function createChatRoom()
    {
        $chatRoom = $this->chat_room()->create(['name' => $this->issue_number]);
        return $chatRoom;          
    }

    public function getCreatedAtAttribute($value) {
        return Carbon::parse($value)->format('F d, Y g:i A');
    }

}
