<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //Commands\WycInit::class,
        //Commands\CheckExpiredDocuments::class,
        //Commands\UpdateUndeliveredOrders::class,
        //Commands\ReturnUntransitOrders::class,
        //Commands\PendingToInTransitOrderDetailsStatus::class,
        //Commands\InTransitToDeliveredOrderDetailsStatus::class
        Commands\MarkAsAbsent::class,
        Commands\DeletePdf::class,
        // Commands\ExpirationReminder::class,
        // Commands\UpdateTask::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('attendance:mark-as-absent')->timezone('Asia/Manila')
            ->between('22:00', '23:00');
            // ->dailyAt('20:00');

        $schedule->command('delete:pdf')->dailyAt('22:00');

        // $schedule->command('visa-type:expiration-reminder')->weekdays()->timezone('Asia/Manila')
        //         ->between('11:00', '12:00');
            //->between('16:00', '19:00');
            //->everyMinute();

        // $schedule->command('task:update')->daily();

        //$schedule->command('document:invalidate')
            //->everyMinute();

        //$schedule->command('order:mark-as-delivered')
            //->everyMinute();

        //$schedule->command('order:return-orders-not-in-transit')
            //->everyMinute();

        // $schedule->command('order-detail:pending-to-in-transit')
        //     ->everyMinute();

        // $schedule->command('order-detail:in-transit-to-delivered')
        //     ->everyMinute();

        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
