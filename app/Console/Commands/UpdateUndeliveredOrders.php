<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Shopping\OrderDetail;
use App\Models\Shopping\Order;
use Illuminate\Support\Facades\Auth;
use App\Notifications\OrderStatusUpdate;

class UpdateUndeliveredOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:mark-as-delivered';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark undelivered orders as delivered';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $orders = OrderDetail::whereStatus('In Transit')
            ->get();

        if (empty($orders)) {
            return;
        }else
        {
            $this->info(count($orders) .' in Transit');
        }

        $orders->each(function ($orderDetail) use ($now) {
            if ($orderDetail->shipped_date->diffInDays($now) > 7) {
            // if ($orderDetail->shipped_date->diffInMinutes($now) >= 5) {
                $orderDetail->status = 'Delivered';
                $orderDetail->received_date = Carbon::now();

                //add the sold product's price to the seller's ewallet
                $seller_ewallet = $orderDetail
                    ->product()->first()
                    ->user()->first()
                    ->ewallet()->first();

                $seller_ewallet->amount += $orderDetail->subtotal;

                $orderDetail->balance = $seller_ewallet->amount;

                $seller_ewallet->save();
                $orderDetail->save();

                //get order then check if all order details are received or delivered then change the status of the order
                $this->ParentOrderUpdateStatus($orderDetail->order()->first()->id);

                $orderDetail->order->user->notify(new OrderStatusUpdate($orderDetail, Auth::user()));
            }
        });
    }

    private function ParentOrderUpdateStatus($parent_order_id){
        $co_orders = OrderDetail::where('order_id',$parent_order_id)->get();
        $order_details_count = count($co_orders);

        $order = Order::find($parent_order_id);

        $status_complete = 0;
        $status_cancelled = 0;

        foreach ($co_orders as $key => $value) {
            switch ($value->status) {
                case 'Cancelled':
                    $status_cancelled++;
                    break;
                
                case 'Pending':
                case 'In Transit':
                    break;

                case 'Received':
                case 'Delivered':
                case 'Returned':
                case 'On Hold':
                    $status_complete++;
                    break;
                default:
                    # code...
                    break;
            }
        }

        if($status_cancelled == $order_details_count)
        {
            $order->status = 'Cancelled';
            $order->save();
        }else if( ($status_complete + $status_cancelled) == $order_details_count)
        {
            $order->status = 'Complete';
            $order->save();
        }

    }
}
