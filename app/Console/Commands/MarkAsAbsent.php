<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\User;
use App\Models\CalendarEvents;
use App\Models\Employee\Attendance;

use App\Classes\Common;

class MarkAsAbsent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attendance:mark-as-absent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark employees who dont have time_in and time_out as absent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $d = Carbon::now('Asia/Manila');
        $date = $d->toDateString();
        $time = $d->toTimeString();
        $dayname = $d->format('l');

        $dt = explode('-',$date);
        $day = $dt[2];
        $month = $dt[1];
        $year = $dt[0];

        // $query = User::with('roles', 'schedule.scheduleType');
        //     $data = $query->whereHas('roles', function ($query) {
        //                         $query->where('name', 'employee');
        //                     })->whereHas('schedule', function ($query) {
        //                         $query->where('schedule_type_id', '!=','3');
        //                     })->where('is_active',1)->get();
               
        $query = User::with('roles', 'schedule.scheduleType');
            $data = $query->whereHas('roles', function ($query) {
                                $query->where('name', 'employee');
                            })->whereHas('schedule')->where('is_active',1)->get();

        foreach($data as $d){
            $checkAttendance = Attendance::where('day',$day)->where('month',$month)->where('year',$year)->where('user_id',$d->id)->first();
     
            
            if(!$checkAttendance && ($dayname=='Saturday' || $dayname=='Sunday')){
                $timein = new Attendance();
                $timein->user_id = $d->id;
                $timein->day = $day;
                $timein->month = $month;
                $timein->year = $year;
                $timein->timein_status = 'WEEKENDS';
                $timein->timeout_status = 'WEEKENDS';
                $timein->save();
            }
            $cdate = $year.'-'.$month.'-'.$day;
            $checkDay = CalendarEvents::where('user_id',$d->id)->where('start_date','>=',$cdate)->where('end_date','<=',$cdate)->where('approval','Approved')->first();
            if(!$checkDay) $checkDay = CalendarEvents::where('user_id',0)->where('start_date','>=',$cdate)->where('end_date','<=',$cdate)->where('approval','Approved')->first();
            if(!$checkAttendance && ($dayname!='Saturday' && $dayname!='Sunday')){
                
                //\Log::info($checkDay);
                //set free time always present
                //\Log::info($d['schedule']['schedule_type_id']);
                if(!$checkDay){           
                    if($d['schedule']['schedule_type_id']==3){
                        
                        $timein = new Attendance();
                        $timein->user_id = $d->id;
                        $timein->day = $day;
                        $timein->month = $month;
                        $timein->year = $year;
                        $timein->time_in = '08:00:00';
                        $timein->time_out = '17:00:00';
                        $timein->save();

                    }else{
                        $timein = new Attendance();
                        $timein->user_id = $d->id;
                        $timein->day = $day;
                        $timein->month = $month;
                        $timein->year = $year;
                        $timein->timein_status = 'ABSENT';
                        $timein->timeout_status = 'ABSENT';
                        $timein->save();
    
                        $title = 'ABSENT';
                        $title_cn = 'ABSENT';
                        $body = "<b>ABSENT</b>";
                        $body_cn = "<b>ABSENT</b>";
                        $main = "<b>ABSENT</b>";
                        $main_cn = "<b>ABSENT</b>";
                        $parent_type = 1;
                        $parent = $date."-".$d->id;
                        $extra = '#c30000';
                    }   
                 

                    //Common::savePushNotif($d->id, $title, $body,'Timelog',$dayname."--".$d->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type, null , $extra,true);
                }
                else{
                    $type= $checkDay->type;
                   
                    if($type == 'Holiday' || $type == 'Half day' || $type == 'Consume OT' || $type == 'Sick Leave' || $type == 'Annual Leave' || $type == 'Waiver' || $type == 'Paternal Leave' || $type == 'Maternal Leave' || $type == 'Leave with Permission' || $type == 'Leave without Permission'){  
                        $eventType = strtoupper(str_replace(' ', '', $type));
                        $timein = new Attendance();
                        $timein->user_id = $d->id;
                        $timein->day = $day;
                        $timein->month = $month;
                        $timein->year = $year;
                        $timein->timein_status = $eventType;
                        $timein->timeout_status = $eventType;
                        $timein->event_id = $checkDay->id;
                        $timein->save();

                        $title = $eventType;
                        $title_cn = $eventType;
                        $body = "<b>".$eventType."</b>";
                        $body_cn = "<b>".$eventType."</b>";
                        $main = "<b>".$eventType."</b>";
                        $main_cn = "<b>".$eventType."</b>";
                        $parent_type = 1;
                        $parent = $date."-".$d->id;
                        $extra = '#c30000';

                        //Common::savePushNotif($d->id, $title, $body,'Timelog',$dayname."--".$d->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type, null , $extra,true);
                    }
                }
            }else if($checkAttendance && ($dayname!='Saturday' && $dayname!='Sunday')){
                if($checkDay){
                    $type= $checkDay->type;
                    $eventType = strtoupper(str_replace(' ', '', $type));
                    Attendance::where('day',$day)->where('month',$month)->where('year',$year)->where('user_id',$d->id)->update(['timein_status'=>$eventType,'timeout_status'=>$eventType,'event_id'=>$checkDay->id]);
                }
                if($checkAttendance->time_out==NULL && $d['schedule']['schedule_type_id']==3){
                    Attendance::where('day',$day)->where('month',$month)->where('year',$year)->where('user_id',$d->id)->update(['time_out'=>'18:00']);
                }
                
            }
        }
    }

}
