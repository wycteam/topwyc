<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class WycInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'wyc:init {type} {delete?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize app';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('type') === 'visa') {
            $this->visa();
        }

        $this->info('Migrating...');
        $this->call('migrate:refresh', [
            '--force' => true,
        ]);

        $this->info('Installing Laravel Passport...');
        $this->call('passport:install');

        $this->info('Seeding the database...');
        $this->call('db:seed');
    }

    private function visa()
    {
        if ($this->argument('delete')) {
            $this->info('Deleting image folders...');
            File::cleanDirectory(public_path('shopping/images/store', true));
            File::cleanDirectory(public_path('shopping/images/product', true));
            File::cleanDirectory(public_path('shopping/images/product_review', true));
            File::cleanDirectory(public_path('shopping/images/feedbacks', true));
            File::cleanDirectory(public_path('shopping/images/deposit-slip', true));
            File::cleanDirectory(storage_path('app/public/order-details/returned', true));
        }
    }
}
