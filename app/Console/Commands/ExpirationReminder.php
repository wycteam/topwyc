<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\User;
use App\Models\Visa\ClientService;
use App\Models\Visa\VisaType;

use App\Classes\Common;

class ExpirationReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'visa-type:expiration-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notif to processing to remind them (expiration of some services)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $d = Carbon::now('Asia/Manila');
        $date = $d->toDateString();
        $time = $d->toTimeString();
        $dayname = $d->format('l');

        $dt = explode('-',$date);
        $day = $dt[2];
        $month = $dt[1];
        $year = $dt[0];

        $data = User::where(function($query) { // For 9A
                        $query->where('visa_type', '9A')
                            ->where(function($query2) {
                                $query2->whereRaw('exp_date <= DATE_ADD(CURDATE(), INTERVAL 7 DAY)')
                                    ->orWhereRaw('first_exp_date <= DATE_ADD(CURDATE(), INTERVAL 7 DAY)');
                            });
                    })
                    ->orWhere(function($query) { // For 9G, TRV and CWV
                        $query->where('visa_type', '!=', '9A')
                            ->where(function($query2) {
                                $query2->whereRaw('exp_date <= DATE_ADD(CURDATE(), INTERVAL 30 DAY)');
                            });
                    })
                    ->orderBy('id')
                    ->get();

            $s9A = ''; $s9G = ''; $sTRV = ''; $sCWV = '';
            foreach($data as $d){
                $ifShow = false;

                // For 9g and TRV
                if($d->visa_type == '9G' || $d->visa_type == 'TRV') {
                    // icard_exp_date
                    if($d->icard_exp_date)
                        if( $this->visibilty($d->icard_exp_date) ) $ifShow = true;

                    // exp_date
                    if($d->exp_date)
                        if( $this->visibilty($d->exp_date) ) $ifShow = true;
                }

                // For CWV
                if($d->visa_type == 'CWV') {
                    // exp_date
                    if($d->exp_date)
                        if( $this->visibilty($d->exp_date) ) $ifShow = true;
                }
                
                if($d->visa_type == '9A' || (($d->visa_type == '9G' || $d->visa_type == 'TRV' || $d->visa_type == 'CWV')&& $ifShow)){
                    if($d->visa_type == '9A' && $d->exp_date !=null && $d->exp_date !='0000-00-00'){
                        $exp =   (new Carbon($d->exp_date))->toFormattedDateString();
                        $clexp = '<b>['.$d->id.']</b> '.$d->first_name.' '.$d->last_name.' - '.$exp.'</br><br>';
                        $s9A .=$clexp;
                        $title = 'Expiration notice 9A';
                        $title_cn = 'Expiration notice 9A';
                        $body = "<b>Your visa will expire on</b><br><br>".$clexp.'. Please contact us for your visa renewal. ';
                        $body_cn = "<b>Expiration notice for 9A Visa</b><br><br>".$clexp.'. Please contact us for your visa renewal. ';
                        $main = $body;
                        $main_cn = $body_cn;
                        $parent_type = 1;
                        $uid = $d->id;
                        $parent = $date."-".$uid;
                        $extra = '#000';
                        Common::savePushNotif($uid, $title, $body,'Reminder',$dayname."--".$uid,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type, null , $extra,true);
                    }
                }
            }
            
            $admins = User::whereHas('roles', function($query) {
                            $query->where('name','master')->orwhere('name', 'cpanel-admin')->orwhere('name', 'processing-dept-head')->orwhere('name', 'processing-assistant');
                            })->pluck('id');

            if($s9A != ''){
                foreach($admins as $ad){ 
                    $title = 'Expiration notice 9A';
                    $title_cn = 'Expiration notice 9A';
                    $body = "<b>Expiration notice for 9A Visa</b><br><br>".$s9A;
                    $body_cn = "<b>Expiration notice for 9A Visa</b><br><br>".$s9A;
                    $main = $body;
                    $main_cn = $body_cn;
                    $parent_type = 1;
                    $uid = $ad;
                    $parent = $date."-".$uid;
                    $extra = '#000';

                    Common::savePushNotif($uid, $title, $body,'Reminder',$dayname."--".$uid,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type, null , $extra,true);
                }
                
            }


    }

    private function visibilty($date) {
        $expirationDate = Carbon::parse($date);
        $today = Carbon::today();

        $difference = $today->diffInDays($expirationDate, false);

        if( $difference <=30 && ($difference % 3 == 0 || $difference < 0) ) return true;

        return false;
    }

}
