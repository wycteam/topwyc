<?php

namespace App\Console\Commands;

use App\Models\User;

use App\Models\Shopping\OrderDetail;
use App\Models\Shopping\Product;
use App\Models\Awb;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
//use Auth;
use Carbon\Carbon;

use Illuminate\Console\Command;

use App\Notifications\OrderStatusUpdate;

class PendingToInTransitOrderDetailsStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order-detail:pending-to-in-transit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark pending order details as in-transit';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pendingOrderDetails = $this->pendingOrderDetails();
        if(count($pendingOrderDetails)) {
            //\Log::info($pendingOrderDetails);

            $awbNoArray = [];
            $awbNoWithOrderDetailsId = [];
            foreach($pendingOrderDetails as $pendingOrderDetail) {
                $id = $pendingOrderDetail->product_id; 
                $awb = Awb::where('order_id', $pendingOrderDetail->order_id)->whereRaw("find_in_set(".$id.",included)")->get();
                // \Log::info($awb);

                if($awb) {
                    //$awbNo = $this->generateAwbNo($awb[0]->id);
                    foreach($awb as $a){
                        $awbNo = $a->code;
                        //\Log::info($a->code);

                        if(!in_array($awbNo, $awbNoArray)) {
                            $awbNoArray[] = $awbNo;
                        }

                        $awbNoWithOrderDetailsId[$awbNo][] = $pendingOrderDetail->id;
                    }
                }
            }

            $data['d'] = [
                'ID_' => "APIUSER",
                'TOKEN_' => "Xs2SakJ1M",
                'tracking_no' => $awbNoArray
            ];

            try {
                $client = new Client([
                    'Content-Type' => 'application/json'
                ]);

                $r = $client->request('POST', 
                    'https://expressapps.2go.com.ph/2gowebdev/model/api/shipment_update.asmx/track', 
                    [
                        'json' => $data
                    ]
                );

                $response = json_decode($r->getBody());
                // \Log::info($r->getBody());
                // \Log::info($response->d->status);

                // T = True/Success
                if($response->d->status == 'T') {

                    $this->processResults($response->d->results, $awbNoWithOrderDetailsId);
                }
            } catch (\GuzzleHttp\Exception\ClientException $ex) {

            }

        }
    }

    private function pendingOrderDetails() {
        return OrderDetail::where('status', 'Pending Request')->get();
    }

    private function processResults($results, $awbNoWithOrderDetailsId) {

        foreach($results as $result) {
            
            $inTransit = false;
            $ctrUpd = count($result->updates);
            $countOrderDetail = count($awbNoWithOrderDetailsId[$result->tracking_no]);

            foreach($result->updates as $update) {
                if($update->id==1){             
                    // T = In Transit , A = Arrival, C= Acceptance, L = Loaded, D = Out for Delivery
                    if($update->status == 'T' || $update->status == 'A' || $update->status == 'C' || $update->status == 'L' || $update->status == 'D') { 
                        $inTransit = true;
                        $dateTime = $update->datetime;
                    }
                }
            }

            if($inTransit) {
                $ctr = 0;
                $dpr = 0; //total discounted price
                $_orderDetails = [];
                foreach($awbNoWithOrderDetailsId[$result->tracking_no] as $orderDetailId) {
                    $orderDetail = OrderDetail::findOrFail($orderDetailId);
                    $orderDetail->status = "In Transit";
                    $orderDetail->shipped_date = Carbon::parse($dateTime);
                    $orderDetail->save();
                    $_orderDetails[] = OrderDetail::where('id',$orderDetailId)->with('product')->first();
                    //\Log::info(json_decode($_orderDetails));

                    $pr = (floatval($orderDetail->sale_price) > 0 ? floatval($orderDetail->sale_price) : floatval($orderDetail->price_per_item)); //price
                    $pr *= floatval($orderDetail->quantity); // multiply to quantity
                    $dpr += ((floatval($pr) + floatval($orderDetail->disc_fee))); //get total discounted price

                    $ctr++;
                    if($countOrderDetail == $ctr){                      
                        //$orderDetail = OrderDetail::findOrFail($orderDetailId);
                        $seller_id = Product::where('id',$orderDetail->product_id)->select('user_id')->first();
                        $seller = User::findOrFail($seller_id->user_id);

                        //Send Email Notification
                        $orderDetail->order->user->notify(new OrderStatusUpdate($orderDetail, $seller, $_orderDetails , $dpr));
                    }
                }
            }

        }

    }

}
