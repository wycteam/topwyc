<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Shopping\OrderDetail;
use App\Models\User;
use Carbon\Carbon;
use DB;
use App\Notifications\OrderStatusUpdate;

class ReturnUntransitOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:return-orders-not-in-transit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Return not in transit orders given the number of days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now();
        $days_return_ewallet = 3;

        $orders = OrderDetail::whereStatus('Pending')
            ->where( DB::raw('timestampdiff(DAY, created_at, now())'), '>=',$days_return_ewallet)
            ->get();


        if (empty($orders)) {
            $this->info('No Pending orders within '. $days_return_ewallet.' days.');
            return;
        }else
        {
            $this->info(count($orders). ' order/s cancelled.');
        }

        foreach ($orders as  $orderDetail) {
            $order = $orderDetail->order()->first();
            $product = $orderDetail->product()->first();
            
            $buyer = $order->user()->first();
            $buyer_ewallet = $buyer->ewallet()->first();
                
            //get qty
            $qty_ordered = $orderDetail->quantity;
            //return the stock
            $product->stock += $qty_ordered;
            $product->save();

            //get amount
            $return_amount = $orderDetail->subtotal;
            //return it to the buyer
            $buyer_ewallet->amount += $return_amount;
            //update the order detail
            $orderDetail->balance = $buyer_ewallet->amount;
            $buyer_ewallet->save();

            $orderDetail->status = 'Cancelled';
            $orderDetail->reason = 'Status not changed to "In Transit" within '.$days_return_ewallet.' days by the seller.';

            if($orderDetail->save())
            {
                //send notification
                $seller = $product->user()->first();

                //seller from seller?????
                $seller->notify(new OrderStatusUpdate($orderDetail, $seller));
                $buyer->notify(new OrderStatusUpdate($orderDetail, $seller));
            }
        }
       
    }
}
