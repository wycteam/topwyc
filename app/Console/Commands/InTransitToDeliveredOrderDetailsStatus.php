<?php

namespace App\Console\Commands;
use App\Models\User;

use App\Models\Shopping\Order;
use App\Models\Shopping\OrderDetail;
use App\Models\Shopping\Product;
use App\Models\Awb;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Carbon\Carbon;

use Illuminate\Console\Command;

use App\Notifications\OrderStatusUpdate;

class InTransitToDeliveredOrderDetailsStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order-detail:in-transit-to-delivered';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mark in-transit order details as delivered';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $inTransitOrderDetails = $this->inTransitOrderDetails();
        if(count($inTransitOrderDetails)) {
            $awbNoArray = [];
            $awbNoWithOrderDetailsId = [];
            foreach($inTransitOrderDetails as $inTransitOrderDetail) {
                $id = $inTransitOrderDetail->product_id; 
                $awb = Awb::where('order_id', $inTransitOrderDetail->order_id)->whereRaw("find_in_set(".$id.",included)")->get();
                if($awb) {
                    foreach($awb as $a){
                        $awbNo = $a->code;
                        //\Log::info($a->code);

                        if(!in_array($awbNo, $awbNoArray)) {
                            $awbNoArray[] = $awbNo;
                        }

                        $awbNoWithOrderDetailsId[$awbNo][] = $inTransitOrderDetail->id;
                    }
                }
            }

            $data['d'] = [
                'ID_' => "APIUSER",
                'TOKEN_' => "Xs2SakJ1M",
                'tracking_no' => $awbNoArray
            ];

            try {
                $client = new Client([
                    'Content-Type' => 'application/json'
                ]);

                $r = $client->request('POST', 
                    'https://expressapps.2go.com.ph/2gowebdev/model/api/shipment_update.asmx/track', 
                    [
                        'json' => $data
                    ]
                );

                $response = json_decode($r->getBody());

                if($response->d->status == 'T') { // Success
                    $this->processResults($response->d->results, $awbNoWithOrderDetailsId);
                }
            } catch (\GuzzleHttp\Exception\ClientException $ex) {

            }

        }
    }

    private function inTransitOrderDetails() {
        return OrderDetail::where('status', 'In Transit')->get();
    }

    private function processResults($results, $awbNoWithOrderDetailsId) {

        foreach($results as $result) {
            
            $delivered = false;
            $cancelled = false;
            $ctrUpd = count($result->updates);
            $countOrderDetail = count($awbNoWithOrderDetailsId[$result->tracking_no]);

            foreach($result->updates as $update) {
                if($update->id==1){
                    if($update->status == 'DEL') { // DELIVERED
                        $delivered = true;
                        $dateTime = $update->datetime;
                    }
                    //mark order as cancelled if status returned from 2GO API found on status below
                    // RTS = Returned to Sender, RTA - Refused to Accept, RTP = Refused to Pay, BCL = Business Closed, UCN = Consignee doesnt exist, CLD = Cancelled, MRD = Consignee is incorrect, BAD = Bad Address, PNR = Payment not ready
                    if($update->status == 'RTS' || $update->status == 'RTA' || $update->status == 'RTP' || $update->status == 'BCL' || $update->status == 'UCN' || $update->status == 'CLD' || $update->status == 'MRD' || $update->status == 'BAD' || $update->status == 'PNR') { 
                        $cancelled = true;
                        $dateTime = $update->datetime;
                    }
                }
            }

            if($delivered) {
                $ctr = 0;
                $dpr = 0; //total discounted price
                $_orderDetails = [];
                foreach($awbNoWithOrderDetailsId[$result->tracking_no] as $orderDetailId) {
                    $orderDetail = OrderDetail::findOrFail($orderDetailId);
                    $orderDetail->status = "Delivered";
                    $orderDetail->received_date = Carbon::parse($dateTime);
                    $orderDetail->save();
                    $_orderDetails[] = OrderDetail::where('id',$orderDetailId)->with('product')->first();

                    $pr = (floatval($orderDetail->sale_price) > 0 ? floatval($orderDetail->sale_price) : floatval($orderDetail->price_per_item)); //price
                    $pr *= floatval($orderDetail->quantity); // multiply to quantity
                    $dpr += ((floatval($pr) + floatval($orderDetail->disc_fee))); //get total discounted price

                        //add the sold product's price to the seller's ewallet
                            $seller_ewallet = $orderDetail->product()->first()
                                                ->user()->first()
                                                ->ewallet()->first();

                            $seller_ewallet->amount += ((floatval($pr) + floatval($orderDetail->disc_fee)));

                            $orderDetail->balance = $seller_ewallet->amount;
                            $seller_ewallet->save();
                            $orderDetail->save();

                    $ctr++;
                    if($countOrderDetail == $ctr){                      
                        $seller_id = Product::where('id',$orderDetail->product_id)->select('user_id')->first();
                        $seller = User::findOrFail($seller_id->user_id);

                        $this->ParentOrderUpdateStatus($orderDetail->order()->first()->id);

                        //Send Email Notification
                        $orderDetail->order->user->notify(new OrderStatusUpdate($orderDetail, $seller, $_orderDetails , $dpr));

                    }
                }
            }

            if($cancelled) {
                $ctr = 0;
                $dpr = 0; //total discounted price
                $_orderDetails = [];
                foreach($awbNoWithOrderDetailsId[$result->tracking_no] as $orderDetailId) {
                    $orderDetail = OrderDetail::findOrFail($orderDetailId);
                    $orderDetail->status = "Cancelled";
                    //$orderDetail->reason = "Cancelled";
                    $orderDetail->save();
                    $_orderDetails[] = OrderDetail::where('id',$orderDetailId)->with('product')->first();

                    $pr = (floatval($orderDetail->sale_price) > 0 ? floatval($orderDetail->sale_price) : floatval($orderDetail->price_per_item)); //price
                    $pr *= floatval($orderDetail->quantity); // multiply to quantity
                    $dpr += ((floatval($pr) + floatval($orderDetail->disc_fee))); //get total discounted price
                            

                            $buyer_id = Order::where('id',$orderDetail->order_id)->select('user_id')->first();
                            $buyer = User::findOrFail($buyer_id->user_id);
                            //return product's price to the buyer's ewallet
                            $buyer_ewallet = $buyer->ewallet()->first();
                            $buyer_ewallet->amount += ((floatval($pr) + floatval($orderDetail->disc_fee)));

                            $orderDetail->balance = $buyer_ewallet->amount;
                            $buyer_ewallet->save();
                            $orderDetail->save();

                    $ctr++;
                    if($countOrderDetail == $ctr){                      
                        // $buyer_id = Order::where('id',$orderDetail->order_id)->select('user_id')->first();
                        // $buyer = User::findOrFail($buyer_id->user_id);

                        $seller_id = Product::where('id',$orderDetail->product_id)->select('user_id')->first();
                        $seller = User::findOrFail($seller_id->user_id);

                        $this->ParentOrderUpdateStatus($orderDetail->order()->first()->id);

                        //Send Email Notification to buyer and ?seller?
                        $buyer->notify(new OrderStatusUpdate($orderDetail, $seller, $_orderDetails , $dpr));
                        //$seller->notify(new OrderStatusUpdate($orderDetail, $buyer, $_orderDetails , $dpr));

                    }
                }
            }

        }

    }

    private function ParentOrderUpdateStatus($parent_order_id){
        $co_orders = OrderDetail::where('order_id',$parent_order_id)->get();
        $order_details_count = count($co_orders);

        $order = Order::find($parent_order_id);

        $status_complete = 0;
        $status_cancelled = 0;

        foreach ($co_orders as $key => $value) {
            switch ($value->status) {
                case 'Cancelled':
                    $status_cancelled++;
                    break;
                
                case 'Pending':
                case 'Pending Request':
                case 'In Transit':
                    break;

                case 'Received':
                case 'Delivered':
                case 'Returned':
                case 'On Hold':
                    $status_complete++;
                    break;
                default:
                    # code...
                    break;
            }
        }

        if($status_cancelled == $order_details_count)
        {
            $order->status = 'Cancelled';
            $order->save();
        }else if( ($status_complete + $status_cancelled) == $order_details_count)
        {
            $order->status = 'Complete';
            $order->save();
        }

    }

}
