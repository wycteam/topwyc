<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Document;
use App\Notifications\ExpiredDocument;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;


class CheckExpiredDocuments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'document:invalidate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invalidate expired documents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $verifiedDocs = Document::whereStatus('Verified')->get();
        foreach ($verifiedDocs as $key => $value) {
            $expiry_date = Carbon::parse($value->expires_at)->timestamp;
            if(Carbon::now()->timestamp >= $expiry_date)
            {
                $this->info('Expired Document ID: '.$value->id);                
                $this->info('Updating..');
                $doc = Document::find($value->id);
                $doc->status = 'Expired';
                if($doc->save())
                {
                    $doc->documentable->notify(new ExpiredDocument($doc));
                }
            }
        }
    }
}
