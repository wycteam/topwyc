<?php

namespace App\Console\Commands;

use App\Models\Visa\Task;

use Carbon\Carbon;

use Illuminate\Console\Command;

class UpdateTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'task:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        Task::where('status', 'Filed')->whereDate('releasing_date', Carbon::today())->update(['status' => 'For releasing']);

    }
}
