<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Contracts\Validation\Validator;

class ApiController extends Controller
{
    protected $statusCode = 200;
    protected $filters;
    //model to be defined by the child class
    protected $model;

    protected function formatValidationErrors(Validator $validator)
    {
        return [
            'error' => [
                'message' => $validator->errors()->all(),
                'status_code' => 422
            ]
        ];
    }

    /**
     * Gets the value of statusCode.
     *
     * @return mixed
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Sets the value of statusCode.
     *
     * @param mixed $statusCode the status code
     *
     * @return self
     */
    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Gets the value of filters.
     *
     * @return mixed
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Sets the value of filters.
     *
     * @param mixed $filters the filters
     *
     * @return self
     */
    protected function setFilters($filters)
    {
        $this->filters = $filters;

        return $this;
    }

    protected function respondWithErrors($message)
    {
        return $this->respond([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ]);
    }

    protected function respondNotFound($message = 'Record not found')
    {
        return $this->setStatusCode(404)
            ->respondWithErrors($message);
    }

    protected function respondCreated($data, $message = 'Record successfully created')
    {
        $responseData = $this->setStatusCode(201)
            ->createResponseData($data, $message);

        return $this->respond($responseData);
    }

    protected function respondUpdated($data, $message = 'Record successfully updated')
    {
        $responseData = $this->setStatusCode(200)
            ->createResponseData($data, $message);

        return $this->respond($responseData);
    }

    protected function respondDeleted($data, $message = 'Record successfully deleted')
    {
        $responseData = $this->setStatusCode(200)
            ->createResponseData($data, $message);

        return $this->respond($responseData);
    }

    protected function createResponseData($data, $message)
    {
        return [
            'data' => $data,
            'success' => [
                'message' => $message,
                'status_code' => $this->getStatusCode()
            ]
        ];
    }

    protected function respond($data, $headers = [])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }

    //////////////////////////
    // Resourceful methods //
    //////////////////////////

    public function index()
    {
        $query = $this->filters?$this->model->filter($this->filters):$this->model;
        $data = $query->get();

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function store(Request $request)
    {
        app(get_class($this->validation));

        $data = $this->model->create($request->all());

        return $this->respondCreated($data);
    }

    public function show($id)
    {
        $data = $this->model->findOrFail($id);

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function update(Request $request, $id)
    {
        app(get_class($this->validation));

        $data = $this->model->findOrFail($id);
        $data->fill($request->all())
            ->save();

        return $this->respondUpdated($data);
    }

    public function destroy($id)
    {
        $data = $this->model->findOrFail($id);
        $data->delete();

        return $this->respondDeleted($data);
    }

   
}