<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\NewUser;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function showRegistrationForm()
    {
        $data['lang'] = trans('signup');
        return view('auth.register')->with($data);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255|regex:/^[\pL\s]+$/u',
            'last_name' => 'required|string|max:255|regex:/^[\pL\s]+$/u',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|max:255|confirmed',
            'birth_date' => 'required|date',
            'gender' => 'required|string',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'birth_date' => $data['birth_date'],
            'gender' => $data['gender'],
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $shopPermissionIds = Permission::whereIn('type', ['Store', 'Product', 'Category', 'Order'])->get()
            ->pluck('id')
            ->toArray();

        $user->assignRole([6, 7]);
        $user->givePermission($shopPermissionIds);

        $lang = trans('signup');
        session()->flash('status', $lang['email-sent']);

        return back();
    }

    public function confirmEmail($token)
    {
        $user = User::where('verification_token', $token)->firstOrFail();
        $user->notify(new NewUser($user));
        $user->confirmEmail();

        
        $lang = trans('signup');

        session()->flash('message', $lang['email-verified']);

        return redirect('home');
    }
}
