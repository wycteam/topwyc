<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Visa\ServiceValidation;
use App\Http\Requests\Visa\EditServiceValidation;
use App\Http\Requests\Visa\ServiceDocumentValidation;

use App\Models\Visa\Branch;
use App\Models\Visa\ClientService;
use App\Models\Visa\Group;
use App\Models\Visa\Service as service;
use App\Models\Visa\FetchService;
use App\Models\Visa\ServiceProfiles;
use App\Models\Visa\ServiceProfileCost;
use App\Models\Visa\ServiceDocuments as documents;
use App\Models\Visa\ClientDocumentType;
use App\Models\Visa\ClientDocuments;
use App\Models\Visa\ServiceBranchCost;
use App\Models\Visa\DocumentLogs;
use App\Models\Visa\Report;
use App\Models\User;

use Carbon\Carbon, DB;

use App\Http\Controllers\Cpanel\SynchronizationController;

class ServiceController extends Controller
{

    public function getTranslation() {
        return json_encode([
            'translation' => trans('cpanel/documents-page')
        ]);
    }

    public function getDocumentTypeTranslation() {
        return json_encode([
            'translation' => trans('cpanel/document-type-page')
        ]);
    }

    public function getServiceProfileTranslation() {
        return json_encode([
            'translation' => trans('cpanel/service-profile-page')
        ]);
    }

    public function getListOfServicesTranslation() {
        return json_encode([
            'translation' => trans('cpanel/list-of-services-page')
        ]);
    }

    public function index() {
        $trans = trans('cpanel/list-of-services-page');
        $data = [
            'title' => $trans['service-manager']
        ];

        return view('cpanel.visa.services.index', $data);
    }

    public function serviceProfiles() {
        $trans = trans('cpanel/service-profile-page');
		$data = [
			'title' => $trans['service-profiles']
		];

		return view('cpanel.visa.services.service-profile-tab', $data);
	}

    public function saveService(ServiceValidation $request){
    	$data = $request->all();
    	$service = new service();
       
        if(!$data['docs_needed']) { $data['docs_needed'] = null; }
        if(!$data['docs_optional']) { $data['docs_optional'] = null; }
        if(!$data['docs_released']) { $data['docs_released'] = null; }
        if(!$data['docs_released_optional']) { $data['docs_released_optional'] = null; }
        $data['cost'] = $data['feeArray'][0]['cost']; 
        $data['charge'] = $data['feeArray'][0]['charge']; 
        $data['tip'] = $data['feeArray'][0]['tip'];
        $data['com_agent'] = $data['feeArray'][0]['com_agent'];
        $data['com_client'] = $data['feeArray'][0]['com_client']; 
        //\Log::info($data);

    	$service->fill($data)->save();
        $nsID = $service->id;
        for($i=1;$i<count($data['feeArray']);$i++){
            ServiceBranchCost::updateOrCreate(
                ['service_id' =>  $nsID, 'branch_id' => $data['feeArray'][$i]['branch_id']],
                ['cost' => $data['feeArray'][$i]['cost'], 'charge' => $data['feeArray'][$i]['charge'], 'tip' => $data['feeArray'][$i]['tip'], 'com_agent' => $data['feeArray'][$i]['com_agent'], 'com_client' => $data['feeArray'][$i]['com_client']]
            );
        }

        //if((int)$data['parent_id'] > 0) {
            //$sId = service::orderBy('id', 'desc')->first()->id;

            // Service Branch Cost
                // Manila
                    // service::find($sId)->update([
                    //     'cost' => ($request->costArray[0]) ? $request->costArray[0] : 0,
                    //     'charge' => ($request->chargeArray[0]) ? $request->chargeArray[0] : 0,
                    //     'tip' => ($request->tipArray[0]) ? $request->tipArray[0] : 0,
                    //     'com_agent' => ($request->tipArray[0]) ? $request->tipArray[0] : 0,
                    //     'com_client' => ($request->tipArray[0]) ? $request->tipArray[0] : 0
                    // ]);
                // Other branches
                // for($i=0; $i<count($request->costArray); $i++) {
                //     if($i != 0) {
                //         $cost = ($request->costArray[$i]) ? $request->costArray[$i] : 0;
                //         $charge = ($request->chargeArray[$i]) ? $request->chargeArray[$i] : 0;
                //         $tip = ($request->tipArray[$i]) ? $request->tipArray[$i] : 0;
                //         $com_agent = ($request->comAgentArray[$i]) ? $request->comAgentArray[$i] : 0;
                //         $com_client = ($request->comClientArray[$i]) ? $request->comClientArray[$i] : 0;
                        
                //         ServiceBranchCost::updateOrCreate(
                //             ['service_id' =>  $sId, 'branch_id' => $request->costBranchIdArray[$i]],
                //             ['cost' => $cost, 'charge' => $charge, 'tip' => $tip, 'com_agent' => $com_agent, 'com_client' => $com_client]
                //         );
                //     }
                // }
               
                   
        //}

        // Web to Mobile Synchronization
        SynchronizationController::update('services', Carbon::now()->toDateTimeString());

    	return $service;
    }

    public function editService(EditServiceValidation $request){
        $data = $request->all();
        
        $service = service::find($data['id']);

        $data['is_active'] = 1; // Set active
        if(!$data['docs_needed']) { $data['docs_needed'] = null; }
        if(!$data['docs_optional']) { $data['docs_optional'] = null; }
        if(!$data['docs_released']) { $data['docs_released'] = null; }
        if(!$data['docs_released_optional']) { $data['docs_released_optional'] = null; }
        // if(!$data['cost']) { $data['cost'] = 0; }
        // if(!$data['charge']) { $data['charge'] = 0; }
        // if(!$data['tip']) { $data['tip'] = 0; }
        // if(!$data['com_agent']) { $data['com_agent'] = 0; }
        // if(!$data['com_client']) { $data['com_client'] = 0; }
        
            
        
        if($data['profile_id']==0){
            
            $data['cost'] = $data['feeArray'][0]['cost']; 
            $data['charge'] = $data['feeArray'][0]['charge']; 
            $data['tip'] = $data['feeArray'][0]['tip'];
            $data['com_agent'] = $data['feeArray'][0]['com_agent'];
            $data['com_client'] = $data['feeArray'][0]['com_client']; 
           
            $service->fill($data)->save();
            
            $nsID = $service->id;
            for($i=1;$i<count($data['feeArray']);$i++){
             
                ServiceBranchCost::where('service_id', $data['id'])
                                    ->where('branch_id', $data['feeArray'][$i]['branch_id'])
                                    ->update([
                                        'cost' => $data['feeArray'][$i]['cost'],
                                        'charge' => $data['feeArray'][$i]['charge'],
                                        'tip' => $data['feeArray'][$i]['tip'],
                                        'com_agent' => $data['feeArray'][$i]['com_agent'],
                                        'com_client' => $data['feeArray'][$i]['com_client']
                                    ]);
            }
        
        }else{
            // $serv = new service;
            // $docs_needed = (!$data['docs_needed']) ? $data['docs_needed'] : null;
            // $docs_optional = (!$data['docs_optional']) ? $data['docs_optional'] : null;
            // $docs_released = (!$data['docs_released']) ? $data['docs_released'] : null;
            // $docs_released_optional = (!$data['docs_released_optional']) ? $data['docs_released_optional'] : null;
            unset($data['cost']);
            unset($data['charge']);
            unset($data['tip']);
            unset($data['com_agent']);
            unset($data['com_client']);

            $service->fill($data)->save();

            $nsID = $service->id;
            for($i=0;$i<count($data['feeArray']);$i++){
                //\Log::info($data['id'].'-'.$data['feeArray'][$i]['branch_id'].'-'.$data['profile_id']);
               
                ServiceProfileCost::where('service_id', $data['id'])
                                    ->where('branch_id', $data['feeArray'][$i]['branch_id'])
                                    ->where('profile_id',$data['profile_id'])
                                    ->update([
                                        'cost' => $data['feeArray'][$i]['cost'],
                                        'charge' => $data['feeArray'][$i]['charge'],
                                        'tip' => $data['feeArray'][$i]['tip'],
                                        'com_agent' => $data['feeArray'][$i]['com_agent'],
                                        'com_client' => $data['feeArray'][$i]['com_client']
                                    ]);
            }
        }
        //$data['commissionable_amt'] = number_format( $data['commissionable_amt'], 2);

       

        // if((int)$data['parent_id'] > 0) {
        //     // Service Branch Cost
        //         // Manila
        //             service::find($data['id'])->update([
        //                 'cost' => ($request->costArray[0]) ? $request->costArray[0] : 0,
        //                 'charge' => ($request->chargeArray[0]) ? $request->chargeArray[0] : 0,
        //                 'tip' => ($request->tipArray[0]) ? $request->tipArray[0] : 0,
        //                 'com_agent' => ($request->comAgentArray[0]) ? $request->comAgentArray[0] : 0,
        //                 'com_client' => ($request->comClientArray[0]) ? $request->comClientArray[0] : 0
        //             ]);
        //         // Other branches
        //             for($i=0; $i<count($request->costArray); $i++) {
        //                 if($i != 0) {
        //                     $cost = ($request->costArray[$i]) ? $request->costArray[$i] : 0;
        //                     $charge = ($request->chargeArray[$i]) ? $request->chargeArray[$i] : 0;
        //                     $tip = ($request->tipArray[$i]) ? $request->tipArray[$i] : 0;
        //                     $com_agent = ($request->comAgentArray[$i]) ? $request->comAgentArray[$i] : 0;
        //                     $com_client = ($request->comClientArray[$i]) ? $request->comClientArray[$i] : 0;

        //                     ServiceBranchCost::where('service_id', $data['id'])
        //                         ->where('branch_id', $request->costBranchIdArray[$i])
        //                         ->update([
        //                             'cost' => $cost,
        //                             'charge' => $charge,
        //                             'tip' => $tip,
        //                             'com_agent' => $com_agent,
        //                             'com_client' => $com_client
        //                         ]);
        //                 }
        //             }
        // }
        
        // Web to Mobile Synchronization
        SynchronizationController::update('services', Carbon::now()->toDateTimeString());

        return $service;
    }


    public function editProfileCost(Request $request){
        $data = $request->all();

        $service= [];
        if((int)$data['service_id'] > 0 && (int)$data['profile_id'] > 0) {
            // Service Profile Cost
                    for($i=0; $i<count($request->costArray); $i++) {
                        // if($i != 0) {
                            $cost = ($request->costArray[$i]) ? $request->costArray[$i] : 0;
                            $charge = ($request->chargeArray[$i]) ? $request->chargeArray[$i] : 0;
                            $tip = ($request->tipArray[$i]) ? $request->tipArray[$i] : 0;
                            $com_agent = ($request->comAgentArray[$i]) ? $request->comAgentArray[$i] : 0;
                            $com_client = ($request->comClientArray[$i]) ? $request->comClientArray[$i] : 0;
                            //$branch_id = ($request->costBranchIdArray[$i]) ? $request->costBranchIdArray[$i] : 0;

                            $service = ServiceProfileCost::where('service_id', $data['service_id'])
                                ->where('profile_id', $data['profile_id'])
                                ->where('branch_id', $request->costBranchIdArray[$i])
                                ->update([
                                    'cost' => $cost,
                                    'charge' => $charge,
                                    'tip' => $tip,
                                    'com_agent' => $com_agent,
                                    'com_client' => $com_client
                                ]);
                        // }
                    }
        }
    

        return $service;
    }

    public function saveProfile(Request $request){
        $id = $request->id;
        $data = $request->all();
        $data['slug'] = str_slug($request->name, '-');
        if($id == ""){
            $checkName = ServiceProfiles::where('slug',$data['slug'])->first();
            if(!$checkName){            
                $sp = new ServiceProfiles();
                $sp->fill($data)->save();
                
              
                return json_encode([
                    'success' => true,
                    'message' => 'New Profile created!',
                ]);
            }
            else{
                return json_encode([
                    'success' => false,
                    'message' => 'Profile name already exist!',
                ]);
            }
        }
        else{
            $sp = ServiceProfiles::find($data['id']);
            $sp->fill($data)->save();
        }

        // Web to Mobile Synchronization
        SynchronizationController::update('services', Carbon::now()->toDateTimeString());

        return $sp;
    }

    public function showService2($group_id){ // jeff
        $group = Group::where('id',$group_id)->first();
        if($group->cost_level > 0){     
            $profileServiceIds = ServiceProfileCost::where('profile_id',$group->cost_level)->where('branch_id',$group->branch_id)
                                ->where(function($query) {
                                    return $query->orwhere('cost', '>', 0)
                                        ->orWhere('charge', '>', 0)
                                        ->orWhere('tip', '>', 0)
                                        ->orWhere('com_agent', '>', 0)
                                        ->orWhere('com_client', '>', 0);
                                })
                                ->pluck('service_id');
            $profileServices = Service::whereIn('id',$profileServiceIds)->orwhere(function($query) {
                                    return $query->where('cost', 0)
                                        ->where('charge', 0)
                                        ->where('tip', 0)
                                        ->where('com_agent', 0)
                                        ->where('com_client', 0);
                                })->get();
            return $profileServices;
        }
        else{        
            $parents = FetchService::where('parent_id',0)->orderBy('id')->where('is_active', 1)->groupBy('id')->pluck('id');
            $searchVal = array("[", "]"); 
            // Array containing replace string from  search string 
            $replaceVal = array("", ""); 
            // Function to replace string 
            $parents = str_replace($searchVal, $replaceVal, $parents); 
            $parents = explode(",",$parents);

            $merged = collect();
            foreach ($parents as $p) {
                    $g = FetchService::where('id',$p)->get();
                    $merged = $merged->merge($g);
                    $g = FetchService::where('parent_id',$p)->where('parent_id','>',0)->where('is_active', 1)->orderBy('detail')->get();
                    $merged = $merged->merge($g);
                }

            return $merged;
        }
    }

    public function showServiceAdded($group_id, $month = 0, $year = 0){ // jeff
        //$group = Group::where('id',$group_id)->first();
            // $date = explode("/",$date);
            // $month = $date[0];            
            // $year = $date[1];            
            $month2 = $month;
            if($month < 10){
                $month2 = ltrim($month2, "0");
            }
            $groupServices = ClientService::where('group_id',$group_id)
                                ->groupBy('service_id')
                                ->where(function($q) use($month,$year, $month2){
                                    $q->where('service_date','LIKE', $month.'%')->where('service_date','LIKE', '%'.$year);
                                    $q->orWhere('service_date','LIKE', $month2.'%')->where('service_date','LIKE', '%'.$year);
                                })
                                ->pluck('service_id');
            $profileServices = Service::select('id','detail')->whereIn('id',$groupServices)->get();
            return $profileServices;


    }

    public function showDateAdded($group_id, $date = 0){ // jeff
       // $group = Group::where('id',$group_id)->first();
   
            $dates = DB::connection('visa')
            ->table('client_services as a')
            ->select(DB::raw('a.*, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%Y") as sdate'))
            ->where('active',1)->where('group_id',$group_id)
            ->orderBy('id','DESC')
            ->groupBy('sdate')
            ->pluck('sdate');

            //$profileServices = Service::whereIn('id',$groupServices)->get();
            return $dates;

    }


    public function showService(){
        $parents = FetchService::where('parent_id',0)->orderBy('id')->where('is_active', 1)->groupBy('id')->pluck('id');
        $searchVal = array("[", "]"); 
        // Array containing replace string from  search string 
        $replaceVal = array("", ""); 
        // Function to replace string 
        $parents = str_replace($searchVal, $replaceVal, $parents); 
        $parents = explode(",",$parents);

        $merged = collect();
        foreach ($parents as $p) {
                $g = FetchService::where('id',$p)->get();
                $merged = $merged->merge($g);
                $g = FetchService::where('parent_id',$p)->where('parent_id','>',0)->where('is_active', 1)->orderBy('detail')->get();
                $merged = $merged->merge($g);
            }

    	return $merged;
    }

    public function getServiceProfileCost($service_id, $profile_id, $branch_id){
        $service = Service::where('id',$service_id)->first();
        if($branch_id > 1){
            $bcost = ServiceBranchCost::where('service_id',$service_id)->where('branch_id', $branch_id)->first();
            $scost = $bcost->cost;
            $stip = $bcost->tip;
            $scharge =$bcost->charge;
            $com_client = $bcost->com_client;
            $com_agent =$bcost->com_agent;
        }
        else{
            $scost = $service->cost;
            $stip = $service->tip;
            $scharge =$service->charge;
            $com_client = $service->com_client;
            $com_agent =$service->com_agent;
        }

        $serviceProfile = ServiceProfileCost::where('service_id',$service_id)->where('branch_id', $branch_id)->where('profile_id', $profile_id)->first();
        
        if($serviceProfile){
            $scharge = $serviceProfile->charge;
            $scost = $serviceProfile->cost;
            $stip = $serviceProfile->tip;
            $com_client = $serviceProfile->com_client;
            $com_agent =$serviceProfile->com_agent;
        }
   
         $scharge = ($scharge>0 ? $scharge : $service->charge);
         $scost = ($scost>0 ? $scost : $service->cost);
         $stip = ($stip>0 ? $stip : $service->tip);
         $com_client = ($com_client>0 ? $com_client : $service->com_client);
         $com_agent = ($com_agent>0 ? $com_agent : $service->com_agent);
       
        // if($scost == 0 && $scharge == 0 && $stip == 0){
        //     $scharge = $service->charge;
        //     $scost = $service->cost;
        //     $stip = $service->tip;
        // }
        $data = [
            'charge' => $scharge,
            'cost' => $scost,
            'tip' => $stip,
            'com_client' => $com_client,
            'com_agent' => $com_agent,
        ];
        return $data;
    }

    public function updateProfileCost(Request $request){
        $data = ServiceProfileCost::where('service_id',$request->service_id)->where('profile_id',$request->profile_id)->first();
        if($data){
            $data->cost = $request->cost;
            $data->save();
            $g = Group::where('cost_level',$request->profile_id)->pluck('id');
            ClientService::whereIn('group_id', $g)->where('service_id', $request->service_id)
                ->update(['charge' => $request->cost]);
        }

        // Web to Mobile Synchronization
        SynchronizationController::update('services', Carbon::now()->toDateTimeString());

        return $data;
    }

    public function sortedServices(){
        // $parents = service::where('parent_id',0)->where('branch_id', Auth::user()->branch_id)->get();
        $parents = service::where('parent_id',0)->where('is_active','!=',0)->get();
        $ctr = 0;
        $services = [];
        foreach($parents as $p){
            $services[$ctr] = $p;
            $getcost = ServiceProfileCost::where('service_id',$p->id)->where('profile_id','!=','2')
                ->whereHas('profile', function($query) {
                    $query->where('is_active', 1);
                })
                ->orderBy('profile_id','DESC')->get();
            $services[$ctr]['costs'] = $getcost;
            $ctr++;
            $child = service::where('parent_id',$p->id)->where('is_active','!=',0)->orderBy('detail', 'ASC')->get();
            foreach($child as $c){
                $services[$ctr] = $c;
                $getcost = ServiceProfileCost::where('service_id',$c->id)->where('profile_id','!=','2')
                    ->whereHas('profile', function($query) {
                        $query->where('is_active', 1);
                    })
                    ->orderBy('profile_id','DESC')->get();
                $services[$ctr]['costs'] = $getcost;
                $ctr++;
            }
        }
        return $services;
    }
    
    public function getAllProfile(){
        $profiles = [];
        $profiles['profiles'] = ServiceProfiles::orderBy('is_active','desc')->where('is_active',1)->orderBy('id','desc')->get();
        $profiles['profiles2'] = ServiceProfiles::orderBy('is_active','desc')->orderBy('id','desc')->get();
        return $profiles;
    }

    public function servProfiles(){
        $profiles = [];
        $profiles['profiles'] = ServiceProfiles::where('is_active',1)->where('id','!=','2')->orderBy('id','DESC')->get();
        $profiles['count'] = ServiceProfiles::where('is_active',1)->where('id','!=','2')->count();
        return $profiles;
    }
    public function updateProfile(Request $request){
        $msg='';
        if($request->action=='disable'){
            ServiceProfiles::where('id',$request->id)->update(['is_active'=>0]);
            $msg = 'success';
        }else if($request->action=='active'){
            ServiceProfiles::where('id',$request->id)->update(['is_active'=>1]);
            $msg = 'success';
        }else if($request->action=='update'){
            $prof = ServiceProfiles::where('name','like',$request->name.'%')->count();
            if($prof>0){
                $msg = 'failed';
            }else{
                ServiceProfiles::where('id',$request->id)->update(['name'=>$request->name]);
                $msg = 'success';
            }
            
        }
        return $msg;
    }

    public function showAllParentServices(){

    	// $parents = service::where('parent_id', 0)->where('is_active', 1)->orderBy('id', 'ASC')->where('branch_id', Auth::user()->branch_id)->get();
        $parents = FetchService::where('parent_id', 0)->where('is_active', 1)->orderBy('id', 'ASC')->get();

    	//show children as well
  //   	$parents->map(function ($par) {
  //   		$children = service::where('parent_id', $par->id)->where('is_active', 1)->get();
		//     $par['children'] = $children;
		// });
		return $parents;

    }

    public function showAllChildren($id){
    	$children = service::where('parent_id', $id)->where('is_active', 1)->get();
    	return $children;
    }

    
    public function getService($id){

    	$service = FetchService::find($id);
        $need =array();
        $opt =array();
        $rel = array();
        $rel_optional = array();
        if($service->docs_needed != ''){
            $docs_needed = explode(",",$service->docs_needed);
             foreach ($docs_needed as $d) {
                $needed = documents::whereId($d)->get();
                if(empty($need)){
                    $need = array($needed);
                }else{
                    array_push($need, $needed);
                }
            }
        }

        if($service->docs_optional != ''){
            $docs_optional = explode(",",$service->docs_optional);
            foreach ($docs_optional as $d) {
                $optional = documents::whereId($d)->get();
                if(empty($opt)){
                    $opt= array($optional);
                }else{
                    array_push($opt, $optional);
                }
            }
        }

        if($service->docs_released != ''){
            $docs_released = explode(",",$service->docs_released);
            foreach ($docs_released as $d) {
                $released = documents::whereId($d)->get();
                if(empty($rel)){
                    $rel= array($released);
                }else{
                    array_push($rel, $released);
                }
            }
        }

        if($service->docs_released_optional != ''){
            $docs_released_optional = explode(",",$service->docs_released_optional);
            foreach ($docs_released_optional as $d) {
                $released_optional = documents::whereId($d)->get();
                if(empty($rel_optional)){
                    $rel_optional= array($released_optional);
                }else{
                    array_push($rel_optional, $released_optional);
                }
            }
        }

        $service['select_docs_needed'] = $need;
        $service['select_docs_optional'] = $opt;
        $service['select_docs_released'] = $rel;
        $service['select_docs_released_optional'] = $rel_optional;

        $service['parent'] = ($service->parent_id == 0)
            ? null
            : FetchService::findOrFail($service->parent_id);

        // Service Branch Cost
            $serviceBranchCosts = [];
            $serviceBranchCosts[0]['cost'] = $service->cost;
            $serviceBranchCosts[0]['charge'] = $service->charge;
            $serviceBranchCosts[0]['tip'] = $service->tip;
            $serviceBranchCosts[0]['com_agent'] = $service->com_agent;
            $serviceBranchCosts[0]['com_client'] = $service->com_client;
            $serviceBranchCosts[0]['branch'] = 'Manila';
            $serviceBranchCosts[0]['branch_id'] = 1;

            // for($i=0; $i<count($service->serviceBranchCosts); $i++) {
            //     // if($service->serviceBranchCosts[$i]->branch->id != 3){           
            //         $serviceBranchCosts[$i+1]['cost'] = $service->serviceBranchCosts[$i]->cost;
            //         $serviceBranchCosts[$i+1]['charge'] = $service->serviceBranchCosts[$i]->charge;
            //         $serviceBranchCosts[$i+1]['tip'] = $service->serviceBranchCosts[$i]->tip;
            //         $serviceBranchCosts[$i+1]['com_agent'] = $service->serviceBranchCosts[$i]->com_agent;
            //         $serviceBranchCosts[$i+1]['com_client'] = $service->serviceBranchCosts[$i]->com_client;
            //         $serviceBranchCosts[$i+1]['branch'] = $service->serviceBranchCosts[$i]->branch->name;
            //         $serviceBranchCosts[$i+1]['branch_id'] = $service->serviceBranchCosts[$i]->branch->id;
            //     // }
            // }

            //$pcost = ServiceBranchCost::where('branch_id','!=',3)->where('service_id',$id)->orderBy('branch_id')->get();
            $pcost = ServiceBranchCost::where('branch_id','!=',3)->where('service_id',$id)->orderBy('branch_id')->get();
            
                for($i=0; $i<count($pcost); $i++) {
                    $branch = Branch::where('id',$pcost[$i]->branch_id)->first();
                    $serviceBranchCosts[$i+1]['cost'] = $pcost[$i]->cost;
                    $serviceBranchCosts[$i+1]['charge'] = $pcost[$i]->charge;
                    $serviceBranchCosts[$i+1]['tip'] = $pcost[$i]->tip;
                    $serviceBranchCosts[$i+1]['com_agent'] = $pcost[$i]->com_agent;
                    $serviceBranchCosts[$i+1]['com_client'] = $pcost[$i]->com_client;
                    $serviceBranchCosts[$i+1]['branch'] = $branch->name;
                    $serviceBranchCosts[$i+1]['branch_id'] = $pcost[$i]->branch_id;
                
            }
            $service['branch_costs'] = $serviceBranchCosts;

    	return $service;
    }
    public function getProfileCost2($pid, $sid){
        $pcost = ServiceProfileCost::where('profile_id',$pid)->where('branch_id','!=',3)->where('service_id',$sid)->orderBy('branch_id')->get();
        $serviceProfileCosts = [];
        $service = [];
        if(count($pcost)<1){
            $branches = Branch::where('id','!=',3)->get();
            foreach ($branches as $key => $value) {
                $spc = new ServiceProfileCost;
                $spc->profile_id = $pid;
                $spc->service_id = $sid;
                $spc->branch_id = $value->id;
                $spc->save();
                //\Log::info($value);
                $pcost = ServiceProfileCost::where('profile_id',$pid)->where('branch_id','!=',3)->where('service_id',$sid)->orderBy('branch_id')->get();
               
            }
        }

        for($i=0; $i<count($pcost); $i++) {
            //\Log::info($pcost[$i]->branch_id);
            $branch = Branch::where('id',$pcost[$i]->branch_id)->first();
            $serviceProfileCosts[$i]['cost'] = $pcost[$i]->cost;
            $serviceProfileCosts[$i]['charge'] = $pcost[$i]->charge;
            $serviceProfileCosts[$i]['tip'] = $pcost[$i]->tip;
            $serviceProfileCosts[$i]['com_agent'] = $pcost[$i]->com_agent;
            $serviceProfileCosts[$i]['com_client'] = $pcost[$i]->com_client;
            $serviceProfileCosts[$i]['branch'] = $branch->name;
            $serviceProfileCosts[$i]['branch_id'] = $pcost[$i]->branch_id;
        }

        $service['profile_costs'] = $serviceProfileCosts;
        return $service;

          
    }

    public function getProfileCost($pid, $sid){
        $pcost = ServiceProfileCost::where('profile_id',$pid)->where('branch_id','!=',3)->where('service_id',$sid)->orderBy('branch_id')->get();
            $serviceProfileCosts = [];
            $service = [];
        //\Log::info($pcost);
            for($i=0; $i<count($pcost); $i++) {
                $branch = Branch::where('id',$pcost[$i]->branch_id)->first();
                $serviceProfileCosts[$i]['cost'] = $pcost[$i]->cost;
                $serviceProfileCosts[$i]['charge'] = $pcost[$i]->charge;
                $serviceProfileCosts[$i]['tip'] = $pcost[$i]->tip;
                $serviceProfileCosts[$i]['com_agent'] = $pcost[$i]->com_agent;
                $serviceProfileCosts[$i]['com_client'] = $pcost[$i]->com_client;
                $serviceProfileCosts[$i]['branch'] = $branch->name;
                $serviceProfileCosts[$i]['branch_id'] = $pcost[$i]->branch_id;
            }

            $service['profile_costs'] = $serviceProfileCosts;

        return $service;
    }

    public function deleteService($id){
    	// $service = service::find($id)->delete();
        $child = service::where('parent_id', $id)->count();
        if($child < 1){
            $service = service::find($id)->update(['is_active' => 0]);
            return json_encode([
                'success' => true,
                'message' => 'Successfully deleted.',
            ]);
        }
        else{
            return json_encode([
                'success' => false,
                'message' => 'Cannot delete. Services exist under this parent service!',
            ]);

        }

        // Web to Mobile Synchronization
        SynchronizationController::update('services', Carbon::now()->toDateTimeString());

    	return response()->json($service);
    }

    public function viewAll() {
        $services = service::where('parent_id', 0)->orderBy('detail')->get();

        $services->map(function($service) {
            $children = service::where('parent_id', $service->id)->orderBy('detail')->get();

            return $service->setAttribute('children', $children);
        });

        $data = [
            'title' => 'View All Services',
            'services' => $services
        ];

        return view('cpanel.visa.services.view-all', $data);
    }

    public function getAllServiceDocs(){
        $documents = DB::connection('visa')->table('service_documents')->orderBy('title')->get();
        
        return $documents;
    }

    private function uniqueDocumentsOnHand($clientId) {
        $clientServices = ClientService::has('documentLogs')
            ->with(['documentLogs' => function($query) {
                $query->orderBy('id', 'desc');
            }])
            ->where('active', 1)
            ->where('client_id', $clientId)
            ->get()
            ->makeHidden([
                'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
            ]);

        $documentsOnHand = [];

        foreach($clientServices as $clientService) {
            if( $clientService->documentLogs[0]->documents_on_hand  ) {
                $documentsOnHand[] = $clientService->documentLogs[0]->documents_on_hand;
            }
        }

        $docsIdArr = explode(',', implode(',', $documentsOnHand));

        return DB::connection('visa')->table('service_documents')->where('is_unique', 1)->whereIn('id', $docsIdArr)->pluck('id');
    }

    public function getDocs(Request $request) {
        $detailArray = [];
        $requiredDocsArray = [];
        $anyAcrIcardRequiredDocsArray = [];
        $optionalDocsArray = [];
        $anyAcrIcardOptionalDocsArray = [];

        if($request->servicesId && count($request->servicesId) > 0) {
            $services = DB::connection('visa')->table('services')->whereIn('id', $request->servicesId)->select('id', 'detail', 'docs_needed', 'docs_optional')->orderBy('id')->get();

            foreach($services as $service) {
                $detailArray[] = $service->detail;

                $docs = DocumentLogsController::getDocs($service->id);
                $requiredDocsArray[] = $docs['requiredDocsArray'];
                $anyAcrIcardRequiredDocsArray[] = $docs['anyAcrIcardRequiredDocsArray'];
                $optionalDocsArray[] = $docs['optionalDocsArray'];
                $anyAcrIcardOptionalDocsArray[] = $docs['anyAcrIcardOptionalDocsArray'];
            }
        }

        $uniqueDocumentsOnHand = $this->uniqueDocumentsOnHand($request->clientId);

        return json_encode([
            'detailArray' => $detailArray,
            'requiredDocsArray' => $requiredDocsArray,
            'anyAcrIcardRequiredDocsArray' => $anyAcrIcardRequiredDocsArray,
            'optionalDocsArray' => $optionalDocsArray,
            'anyAcrIcardOptionalDocsArray' => $anyAcrIcardOptionalDocsArray,
            'uniqueDocumentsOnHand' => $uniqueDocumentsOnHand
        ]);
    }

    //service documents
    public function listServiceDocs(){
        $trans = trans('cpanel/documents-page');
        $data = [
            'title' => $trans['documents'],
        ];

        return view('cpanel.visa.services.documents-index', $data);
    }

    public function addDocuments(ServiceDocumentValidation $request){
        $data = $request->all();
        $documents = new documents();
        $documents->fill($data)->save();

        // Web to Mobile Synchronization
        SynchronizationController::update('documents', Carbon::now()->toDateTimeString());

        return response()->json($documents);
    }

    public function getDocInfo($id){
        $document = documents::find($id);
        return response()->json($document);
    }


    public function editDocuments(ServiceDocumentValidation $request){
        $data = $request->all();
        $document = documents::find($data['id']);
        $document->fill($data)->save();

        // Web to Mobile Synchronization
        SynchronizationController::update('documents', Carbon::now()->toDateTimeString());

        return response()->json($document);
    }

    public function deleteDocuments($id){
        $document = documents::find($id)->delete();

        // Web to Mobile Synchronization
        SynchronizationController::update('documents', Carbon::now()->toDateTimeString());

        return response()->json($document);
    }

    //client documents
    public function clientDocuments(){
        $trans = trans('cpanel/document-type-page');
        $data = [
            'title' => $trans['client-document-type']
        ];

        return view('cpanel.visa.clients.documents-index', $data);
    }

    public function getAllClientDocType(){
        $documents = ClientDocumentType::orderBy('name')->get();
        return $documents;
    }

    public function clientDocumentsById($id){
        $type = ClientDocumentType::findorFail($id);
        $data = [
            'title' => $type->name,
            'type' => $type,
        ];
        return view('cpanel.visa.clients.documents-view', $data);
    }

    public function uploadFiles(Request $request, $id)
    {
        $clientDoc = ClientDocumentType::findOrFail($id);
        $path = '';

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $f = explode('&&',$filename);

            $path = $file->storeAs(
                        'client-documents/'.$clientDoc->name, $filename ,'public'
                    );

            $checkDuplicate = $clientDoc->clientdocuments()->where('type',$clientDoc->name)
                                ->where('user_id',$f[0])->where('issue_at',$f[1])->where('expires_at',$f[2])
                                ->first();

            $clientDoc->clientdocuments()->create([
                'type' => $clientDoc->name,
                'file_path' => $path,
                'user_id' => $f[0],
                'file_size' => $file->getClientSize(),
                'file_mime' => $file->getMimeType(),
                'issue_at' => $f[1],
                'expires_at' => $f[2],
            ]);

            // $clientDoc->clientdocuments()->updateOrCreate(
            //     ['user_id' => $f[0], 'type' => $clientDoc->name, 'issue_at' => $f[1],'expires_at' => $f[2]],
            //     ['file_size' => $file->getClientSize(),'file_mime' => $file->getMimeType(),'file_path' => $path]
            // );

            if($checkDuplicate){
                $checkDuplicate->delete();
            }
        }

        return json_encode([
            'success' => true,
            'message' => 'Successfully saved.',
            'path' => $path
        ]);
    }

    public function uploadDocuments(Request $request) {
        $clientDoc = ClientDocumentType::findOrFail($request->documentType);
        $path = '';

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $f = explode('&&',$filename);

            $path = $file->storeAs(
                        'client-documents/'.$clientDoc->name, $filename ,'public'
                    );

            $clientDoc->clientdocuments()->create([
                'type' => $clientDoc->name,
                'file_path' => $path,
                'user_id' => $f[0],
                'file_size' => $file->getClientSize(),
                'file_mime' => $file->getMimeType(),
                'issue_at' => $f[1],
                'expires_at' => $f[2],
            ]);
        }

        return json_encode([
            'success' => true,
            'message' => 'Successfully saved.',
            'path' => $path
        ]);
    }

    public function uploadDocumentsV2(Request $request) {
        // \Log::info(count($request->file('attachments')));

        for($i=0; $i<count($request->images); $i++) {
            $clientDoc = ClientDocumentType::findOrFail($request->documentTypes[$i]);

            $file = $request->file('attachments')[$i];
            $expiredAt = $request->expiredAt[$i];

            if($request->expiredAt[$i] == 'null'){
                $filename = $request->userId . '&&' . $request->issuedAt[$i] . '.jpg';
                $expiredAt = null;
            }else{
                $filename = $request->userId . '&&' . $request->issuedAt[$i] . '&&' . $expiredAt . '.jpg';
            }


            $fileSize = $file->getClientSize();
            $fileMime = $file->getMimeType();

            $path = $file->storeAs(
                'client-documents/'.$clientDoc->name, $filename ,'public'
            );

            $checkDuplicate = $clientDoc->clientdocuments()->where('type',$clientDoc->name)
                                ->where('user_id',$request->userId)->where('issue_at',$request->issuedAt[$i])
                                ->where('expires_at',$request->expiredAt[$i])
                                ->first();

            $clientDoc->clientdocuments()->create([
                'type' => $clientDoc->name,
                'file_path' => $path,
                'user_id' => $request->userId,
                'file_size' => $fileSize,
                'file_mime' => $fileMime,
                'issue_at' => $request->issuedAt[$i],
                'expires_at' => $expiredAt,
            ]);

            if($checkDuplicate){
                $checkDuplicate->delete();
            }
        }

        return json_encode([
            'success' => true
        ]);
    }

    public function deleteDocument($id){
        $del = ClientDocuments::findOrFail($id);
        $file_delete = Storage::delete('public/'.$del['file_path']);
        $file_del = $del->delete();
        return json_encode([
            'success' => true,
            'message' => "Document deleted permanently."
        ]);

    }


    public function addDocumentType(Request $request) {
        $this->validate($request, [
            'name' => 'required'
        ]);

        ClientDocumentType::create([
            'name' => $request->name
        ]);

        return json_encode([
            'success' => true,
            'message' => 'New client document type added.'
        ]);
    }

    public function editDocumentType(Request $request) {
        $this->validate($request, [
            'id' => 'required',
            'name' => 'required'
        ]);

        ClientDocumentType::findOrFail($request->id)
            ->update([
                'name' => $request->name
            ]);

        return json_encode([
            'success' => true,
            'message' => 'Client document type updated.'
        ]);
    }

    public function getDocumentType($id) {
        $documentType = ClientDocumentType::findOrFail($id);

        return response()->json($documentType);
    }

    public function getClientDocuments($id) {
        $files = ClientDocuments::where('clientdocumentable_id', $id)
            ->where('clientdocumentable_type', 'App\Models\Visa\ClientDocumentType')
            ->orderBy('id', 'desc')->get();

        return response()->json($files);
    }

    public function getClientDocumentsByClientId($id) {
        $documents = ClientDocuments::where('user_id', $id)->orderBy('id', 'desc')->get();

        return response()->json($documents);
    }

    public function getClientDocumentsReceived($id) {
        $rcvs =  ClientService::where('client_id',$id)->pluck('rcv_docs');
        $unique ='';
        foreach($rcvs as $r){
                $unique .= ','.$r;
        }
        $u = array_unique(explode(',', trim($unique,",")));
        $s = documents::whereIn('id',$u)->pluck('title');

        return $s;
    }

    public function getBranchCost() {
        $branchCost = [];

        $branches = Branch::where('id','!=',3)->get();
        
        foreach($branches as $branch) {
            $branchCost[] = [
                'cost' => 0,
                'charge' => 0,
                'tip' => 0,
                'com_agent' => 0,
                'com_client' => 0,
                'branch' => $branch->name,
                'branch_id' => $branch->id
            ];
        }

        return json_encode([
            'branchCost' => $branchCost
        ]);
    }

    public function outcomeDocuments(Request $request) {
        $outcomeDocuments = [];
        $clientsOutcomeDocuments = [];

        $clientServices = ($request->clientServices) ? $request->clientServices : [];
        $servicesIdArray = ($request->servicesIdArray) ? $request->servicesIdArray : [];

        $services = service::whereIn('id', $servicesIdArray)->get();
        foreach($services as $service) {
            // START: $clientsOutcomeDocuments
                $client = [];
                foreach($clientServices as $clientService) {
                    if($service->id == $clientService['service_id']) {
                        $user = User::findOrFail($clientService['client_id'])->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);

                        $likes = [];

                        // 123=Driver's License 
                        if($clientService['parent_id'] == 123) {
                            $likes[] = 'Released from LTO';
                        }
                        // 133=NSO Certificate                                                                              
                        elseif($clientService['parent_id'] == 133) {
                            if($service->id == 197 ||$service->id == 198) {
                                $likes[] = 'Released from NSO';
                            } elseif($service->id == 321) {
                                $likes[] = 'Released from Cityhall';
                            }
                        }
                        // 158=Department of Foreign Affairs (DFA)
                        elseif($clientService['parent_id'] == 158) {
                            if($service->id == 264 || $service->id ==  370) {
                                $likes[] = 'Released from DFA';
                            } elseif($service->id == 291 || $service->id == 338 || $service->id == 339 || $service->id == 381) {
                                $likes[] = 'Released from Chinese Embassy';
                            }
                        }
                        // 160=Other Services
                        elseif($clientService['parent_id'] == 160) {
                            if($service->id == 116) {
                                $likes[] = 'Released from NBI';
                            } elseif($service->id == 118 || $service->id == 194) {
                                $likes[] = 'Released from DOLE';
                            } elseif($service->id == 120 || $service->id == 169 || $service->id == 215) {
                                $likes[] = 'Released from Immigration';
                            } elseif($service->id == 396) {
                                $likes[] = 'Released from PAL';
                                $likes[] = 'Released from Cebu Pacific';
                                $likes[] = 'Released from China Southern Airlines';
                                $likes[] = 'Released from China East Airlines';
                            }
                        }
                        // 217=Special Retiree Resident Visa (SRRV) Conversion
                        elseif($clientService['parent_id'] == 217) {
                            $likes[] = 'Released from PRA';
                        }
                        // 222=Bureau of Quarantine (BOQ)
                        elseif($clientService['parent_id'] == 222) {
                            $likes[] = 'Released from BOQ';
                        }
                        // 238=Cagayan Economic Zone Authority(CEZA) Working Visa
                        elseif($clientService['parent_id'] == 238) {
                            $likes[] = 'Released from CEZA';
                        }
                        // 259=Department of Justice (DOJ)
                        elseif($clientService['parent_id'] == 259) {
                            $likes[] = 'Released from DOJ';
                        }
                        else {
                            $likes[] = 'Released from Immigration';
                        }

                        $count = Report::has('service.documentLogs')
                            ->where('service_id', $clientService['id'])
                            ->where(function($query) use($likes) {
                                foreach($likes as $index => $l) {
                                    if($index == 0) {
                                        $query->where('detail', 'LIKE', '%'.$l.'%');
                                    } else {
                                        $query->orWhere('detail', 'LIKE', '%'.$l.'%');
                                    }
                                }
                            })
                            ->count();

                        // 228=Company Registration
                        if( $count == 0 && $clientService['parent_id'] != 228 ) {
                            $documentsOnHand = [];
                        } else {
                            $documentLog = DocumentLogs::where('client_service_id', $clientService['id'])->orderBy('id', 'desc')->first()->makeHidden(['documents_string', 'documents_on_hand_string']);
                            if($documentLog) {
                                $documentsOnHand = explode(',', $documentLog->documents_on_hand);
                            } else {
                                $documentsOnHand = [];
                            }
                        }

                        $client[] = [
                            'id' => $clientService['client_id'],
                            'name' => $user->first_name . ' ' . $user->last_name,
                            'documents_on_hand' => $documentsOnHand,
                            'client_service_id' => $clientService['id']
                        ];
                    }
                }
                $clientsOutcomeDocuments[] = $client;
            // END: $clientsOutcomeDocuments

            // START: $outcomeDocuments
                $docsReleased = $service->docs_released;

                if($docsReleased != null && $docsReleased != '') {
                    $docsReleased = explode(',', $docsReleased);

                    $_outcomeDocument = [];
                    foreach($docsReleased as $docId) {
                        $document = documents::findOrFail($docId);

                        $_outcomeDocument[] = [
                            'id' => $document->id,
                            'title' => $document->title
                        ];
                    }

                    // 9A Services
                    if($service->parent_id == 63) {
                        // 7=ACR I-Card Original
                        $_outcomeDocument[] = [
                            'id' => 7,
                            'title' => documents::findorFail(7)->title
                        ];
                    }
                    // Downgrading
                    if($service->parent_id == 114) {
                        // 121=9G Working Visa Downgrading - Complete Documents
                        // 394=9G Working Visa Downgrading - Complete Documents- Rush
                        // 423=9G Working Visa Downgrading - Incomplete Documents- Rush
                        if($service->id == 121 || $service->id == 394 || $service->id == 423) {
                            // 131=Cancellation of ACR I-CARD
                            $_outcomeDocument[] = [
                                'id' => 131,
                                'title' => documents::findorFail(131)->title
                            ];
                        }
                        // 403=Cancellation of Working Visa
                        elseif($service->id == 403) {
                            // unset 122=Cancellation Letter from DOLE
                            $temp = [];
                            foreach($_outcomeDocument as $_oDcmnt) {
                                if( $_oDcmnt['id'] != 122) {
                                    $temp[] = $_oDcmnt;
                                }
                            }
                            $_outcomeDocument = $temp;
                        }
                    }
                    // Blacklist
                    if($service->parent_id == 126) {
                        // 334=Request BLO order
                        if($service->id == 334) {
                            // 199=BLO Order
                            $_outcomeDocument[] = [
                                'id' => 199,
                                'title' => documents::findorFail(199)->title
                            ];
                        }
                    }
                    // 217-SRRV Services
                    if($service->parent_id == 217) {
                        // 386=SRRV Principal
                        if($service->id == 386) {
                            // Affirmation
                            $_outcomeDocument[] = [
                                'id' => 201,
                                'title' => documents::findorFail(201)->title
                            ];

                            // Certificate of Deposit
                            $_outcomeDocument[] = [
                                'id' => 200,
                                'title' => documents::findorFail(200)->title
                            ];
                        }
                    }
                } else {
                    $_outcomeDocument = null;
                }

                $outcomeDocuments[] = $_outcomeDocument;
            // END: $outcomeDocuments
        }

        return json_encode([
            'outcomeDocuments' => $outcomeDocuments,
            'clientsOutcomeDocuments' => $clientsOutcomeDocuments
        ]);
    }

    public function outcomeDocumentsV2(Request $request) {
        $clientsOutcomeDocuments = [];

        $clientServices = ($request->clientServices) ? $request->clientServices : [];
        $servicesIdArray = ($request->servicesIdArray) ? $request->servicesIdArray : [];

        $services = DB::connection('visa')->table('services')->whereIn('id', $servicesIdArray)->get();

        foreach($services as $service) {
            // Clients Outcome Documents
                $_clientsOutcomeDocuments = [];
                foreach($clientServices as $clientService) {
                    $clientService = json_decode($clientService, true);

                    if($service->id == $clientService['service_id']) {
                        $user = DB::table('users')->where('id', $clientService['client_id'])->first();

                        $_____outcomeDocuments = [];
                        $_____outcomeOptionalDocuments = [];
                        $_____additionalOutcomeDocuments = [];
                        $_____additionalOutcomeDocuments2 = [];

                        // $_____outcomeDocuments
                            $docsReleased = $service->docs_released;
                            if($docsReleased != null && $docsReleased != '') {
                                $docsReleased = explode(',', $docsReleased);

                                // ACR I-CARD Renewal
                                // ACR I-CARD Certification
                                if( $service->id == 137 || $service->id == 318 ) {
                                    $docsReleased = array_diff($docsReleased, [7]);

                                    $anyAcrIcard = DocumentLogsController::getDocs($service->id)['anyAcrIcardOutcomeDocsArray'];
                                    $rcvDocs = ($clientService['rcv_docs']) 
                                        ? explode(',', $clientService['rcv_docs']) 
                                        : [];
                                    foreach($anyAcrIcard as $a) {
                                        if( in_array($a->id, $rcvDocs) ) {
                                            $docsReleased[] = $a->id;
                                        }
                                    }
                                }

                                // ACR I-CARD Replacement
                                elseif( $service->id == 229 ) {
                                    $docsReleased = array_diff($docsReleased, [7]);

                                    $anyAcrIcard = DocumentLogsController::getDocs($service->id)['anyAcrIcardOutcomeDocsArray'];
                                    $rcvDocs = ($clientService['rcv_docs']) 
                                        ? explode(',', $clientService['rcv_docs']) 
                                        : [];
                                    $flag = 0;
                                    foreach($anyAcrIcard as $a) {
                                        if( in_array(($a->id+1), $rcvDocs) ) {
                                            $flag = $a->id;
                                        }

                                        $_____additionalOutcomeDocuments[] = [
                                            'id' => $a->id,
                                            'title' => $a->title
                                        ];
                                    }

                                    if( $flag != 0 ) {
                                        $docsReleased[] = $flag;

                                        $_____additionalOutcomeDocuments = [];
                                    }
                                }

                                $serviceDocuments = DB::connection('visa')->table('service_documents')->whereIn('id', $docsReleased)->get();
                                foreach($serviceDocuments as $sd) {
                                    $_____outcomeDocuments[] = [
                                        'id' => $sd->id,
                                        'title' => $sd->title
                                    ];
                                }
                            }

                        // $_____outcomeOptionalDocuments
                            $rcvDocs = ($clientService['rcv_docs']) 
                                ? explode(',', $clientService['rcv_docs'])
                                : [];

                            $docs = DocumentLogsController::getDocs($service->id);
                            $optionalDocs = $docs['optionalDocsArray'];
                            $anyAcrIcardOptionalDocsArray = $docs['anyAcrIcardOptionalDocsArray'];

                            $allOptionalDocs = [];

                            foreach($optionalDocs as $o) {
                                $allOptionalDocs[] = $o->id;
                            }

                            foreach($anyAcrIcardOptionalDocsArray as $a) {
                                $allOptionalDocs[] = $a->id;
                            }
                                
                            $allOptionalDocs = array_unique($allOptionalDocs);
                            $optionalDocsReleased = [];
                            foreach($allOptionalDocs as $a) {
                                if( in_array($a, $rcvDocs) ) {
                                    $optionalDocsReleased[] = $a;
                                }
                            }

                            $optionalServiceDocuments = DB::connection('visa')->table('service_documents')->whereIn('id', $optionalDocsReleased)->get();
                            foreach($optionalServiceDocuments as $osd) {
                                $_____outcomeOptionalDocuments[] = [
                                    'id' => $osd->id,
                                    'title' => $osd->title
                                ];
                            }

                        // $_____additionalOutcomeDocuments / $_____additionalOutcomeDocuments2
                            if( $service->id == 67 || $service->id == 286 || $service->id == 349 || $service->id == 350 || $service->id == 364 || $service->id == 337 || $service->id == 361 || $service->id == 405 || $service->id == 412 || $service->id == 419 || $service->id == 418 || $service->id == 420 || $service->id == 421 ) {
                                $_____additionalOutcomeDocuments[] = [
                                    'id' => '243',
                                    'title' => 'With 9a ACR-ICARD Original' 
                                ];

                                $_____additionalOutcomeDocuments[] = [
                                    'id' => '0',
                                    'title' => 'Without 9a ACR-ICARD Original' 
                                ];

                                $temp = [];
                                foreach($_____outcomeOptionalDocuments as $ood) {
                                    if($ood['id'] != 243) {
                                        $temp[] = $ood;
                                    }
                                }
                                $_____outcomeOptionalDocuments = $temp; 
                            }

                            elseif( $service->id == 121 || $service->id == 394 || $service->id == 423 ) {
                                $_____additionalOutcomeDocuments[] = [
                                    'id' => '131',
                                    'title' => 'With Cancellation of ACR I-CARD' 
                                ];

                                $_____additionalOutcomeDocuments[] = [
                                    'id' => '0',
                                    'title' => 'Without Cancellation of ACR I-CARD' 
                                ];

                                $temp = [];
                                foreach($_____outcomeOptionalDocuments as $ood) {
                                    if($ood['id'] != 131) {
                                        $temp[] = $ood;
                                    }
                                }
                                $_____outcomeOptionalDocuments = $temp; 
                            }

                            elseif( $service->id == 334 ) {
                                $_____additionalOutcomeDocuments[] = [
                                    'id' => '199',
                                    'title' => 'With BLO Order' 
                                ];

                                $_____additionalOutcomeDocuments[] = [
                                    'id' => '0',
                                    'title' => 'Without BLO Order' 
                                ];

                                $temp = [];
                                foreach($_____outcomeOptionalDocuments as $ood) {
                                    if($ood['id'] != 199) {
                                        $temp[] = $ood;
                                    }
                                }
                                $_____outcomeOptionalDocuments = $temp;
                            }

                            elseif( $service->id == 386 ) {
                                $_____additionalOutcomeDocuments[] = [
                                    'id' => '201',
                                    'title' => 'With Affirmation' 
                                ];

                                $_____additionalOutcomeDocuments[] = [
                                    'id' => '0',
                                    'title' => 'Without Affirmation' 
                                ];

                                $_____additionalOutcomeDocuments2[] = [
                                    'id' => '200',
                                    'title' => 'With Certificate of Deposit' 
                                ];

                                $_____additionalOutcomeDocuments2[] = [
                                    'id' => '0',
                                    'title' => 'Without Certificate of Deposit' 
                                ];

                                $temp = [];
                                foreach($_____outcomeOptionalDocuments as $ood) {
                                    if($ood['id'] != 201 && $ood['id'] != 200) {
                                        $temp[] = $ood;
                                    }
                                }
                                $_____outcomeOptionalDocuments = $temp;
                            }

                        $_clientsOutcomeDocuments[] = [
                            'id' => $user->id,
                            'name' => $user->first_name . ' ' . $user->last_name,
                            'documents_on_hand' => ($clientService['report_docs']) 
                                ? explode(',', $clientService['report_docs'])
                                : [],
                            'outcomeDocuments' => $_____outcomeDocuments,
                            'outcomeOptionalDocuments' => $_____outcomeOptionalDocuments,
                            'additionalOutcomeDocuments' => $_____additionalOutcomeDocuments,
                            'additionalOutcomeDocuments2' => $_____additionalOutcomeDocuments2,
                            'client_service_id' => $clientService['id']
                        ];
                    }
                }
                $clientsOutcomeDocuments[] = $_clientsOutcomeDocuments;
        }

        return json_encode([
            'clientsOutcomeDocuments' => $clientsOutcomeDocuments
        ]);
    }

    public function outcomeOptionalDocumentsV2(Request $request) {
        $clientsOutcomeOptionalDocuments = [];

        $clientServices = ($request->clientServices) ? $request->clientServices : [];
        $servicesIdArray = ($request->servicesIdArray) ? $request->servicesIdArray : [];

        $services = DB::connection('visa')->table('services')->whereIn('id', $servicesIdArray)->get();

        foreach($services as $service) {
            // clients Outcome Optional Documents
                $_clientsOutcomeOptionalDocuments = [];
                foreach($clientServices as $clientService) {
                    $clientService = json_decode($clientService, true);

                    if($service->id == $clientService['service_id']) {
                        $user = DB::table('users')->where('id', $clientService['client_id'])->first();

                        $_____outcomeOptionalDocuments = [];

                        if($service->parent_id == 70) {
                            // 9G Working Visa Extension with AEP - 1 Year
                            // 9G Working Visa Extension with AEP - 2 Years
                            // 9G Working Visa Extension with AEP - 3 Years
                            // 9G Working Visa Conversion with AEP - 1 Year
                            // 9G Working Visa Conversion with AEP - 2 Years
                            // 9G Working Visa Conversion with AEP - 3 Years
                            // 9G Working Visa Conversion - 2 Years All in Package
                            // 9G Working Visa Conversion - 1 Year All in Package
                            // 9G Working Visa Conversion - 3 Years All in Package
                            if($service->id == 325 || $service->id == 326 || $service->id == 327 || $service->id == 328 || $service->id == 97 || $service->id == 98 || $service->id == 401 || $service->id == 417 || $service->id == 400) {
                                $_____outcomeOptionalDocuments[] = [
                                    'id' => 10,
                                    'title' => 'Alien Employment Permit (AEP) Card Original'
                                ];
                            }
                        }
                        // Downgrading
                        elseif($service->parent_id == 114) {
                            // CEZA Working Visa Downgrading
                            // CEZA Working Visa Downgrading - Incomplete Documents- Rush
                            // CEZA Working Visa Downgrading - Complete Documents- Rush
                            if($service->id == 307 || $service->id == 415 || $service->id == 416) {
                                $_____outcomeOptionalDocuments[] = [
                                    'id' => 75,
                                    'title' => 'CEZA Cancellation Order Original'
                                ];
                            }

                            // Cancellation of Working Visa
                            elseif($service->id == 403) {
                                $_____outcomeOptionalDocuments[] = [
                                    'id' => 122,
                                    'title' => 'Cancellation Letter from DOLE'
                                ];
                            } 
                        }

                        $_clientsOutcomeOptionalDocuments[] = [
                            'id' => $user->id,
                            'name' => $user->first_name . ' ' . $user->last_name,
                            'documents_on_hand' => ($clientService['report_docs']) 
                                ? explode(',', $clientService['report_docs'])
                                : [],
                            'outcomeOptionalDocuments' => $_____outcomeOptionalDocuments,
                            'client_service_id' => $clientService['id']
                        ];
                    }
                }
                $clientsOutcomeOptionalDocuments[] = $_clientsOutcomeOptionalDocuments;
        }

        return json_encode([
            'clientsOutcomeOptionalDocuments' => $clientsOutcomeOptionalDocuments
        ]);
    }

    // public function addDocuments(ServiceDocumentValidation $request){
    //     $data = $request->all();
    //     $documents = new documents();
    //     $documents->fill($data)->save();
    //     return response()->json($documents);
    // }

    // public function getDocInfo($id){
    //     $document = documents::find($id);
    //     return response()->json($document);
    // }


    // public function editDocuments(ServiceDocumentValidation $request){
    //     $data = $request->all();
    //     $document = documents::find($data['id']);
    //     $document->fill($data)->save();
    //     return response()->json($document);
    // }

    // public function deleteDocuments($id){
    //     $document = documents::find($id)->delete();
    //     return response()->json($document);
    // }
}
