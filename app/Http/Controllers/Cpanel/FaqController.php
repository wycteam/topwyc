<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Faq;

use App\Classes\Common;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cpanel.faq.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       $data = Faq::create([
         'type'     => $request['type']
        ,'question' => $request['question']
        ,'question_cn' => $request['questionCn']
        ,'answer'   => $request['answer']
        ,'answer_cn'   => $request['answerCn']
        ]);

       $act_id = Faq::orderBy('id', 'desc')->first()->id;
       $detail = 'Created new FAQ -> ' . $request['question'];
       Common::saveActionLogs('Faq', $act_id, $detail);

       return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data = Faq::orderBy('id', 'DESC')->get();
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = Faq::find($id);
        $data->type = $request['type'];
        $data->question = $request['question'];
        $data->question_cn = $request['questionCn'];
        $data->answer = $request['answer'];
        $data->answer_cn = $request['answerCn'];

        if($data->isDirty()){
            //getOriginal() -> get original values of model
            //getDirty -> get all fields updated with value
            $changes = $data->getDirty();
            $detail = 'Updated FAQ -> ';
            foreach ($changes as $key => $value) {
                $old = $data->getOriginal($key);
                $detail .= "Change ".$key." from ".$old." to ".$value.". ";
            }
            Common::saveActionLogs('Faq', $id, $detail);
        }

        $data->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $faq = Faq::find($id);
        $question = $faq->question;
        $faq->delete();

        $detail = 'Deleted FAQ -> ' . $question;
        Common::saveActionLogs('Faq', $id, $detail);

        return $faq;
    }

    public function getQuestion($id)
    {
        $data = Faq::whereId($id)->first();
        return $data;
    }
}
