<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Visa\Doc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DocsController extends Controller
{

	public function index() {
		return Doc::where('parent_id', 0)->get();
	}

	public function store(Request $request) {
		$this->validate($request, [
			'title' => 'required',
            'title_cn' => 'required',
            'doc_types.*.title' => 'sometimes|required',
            'doc_types.*.title_cn' => 'sometimes|required'
		]);

		Doc::create([
			'title' => $request->title,
			'title_cn' => $request->title_cn,
			'parent_id' => 0
		]);

		$lastInsertedDocumentId = Doc::orderBy('id', 'desc')->first()->id;

		foreach($request->doc_types as $docType) {
			Doc::create([
				'title' => $docType['title'],
				'title_cn' => $docType['title_cn'],
				'parent_id' => $lastInsertedDocumentId
			]);
		}

		return json_encode([
			'success' => true,
			'message' => 'Document successfully added.'
		]);
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'title' => 'required',
            'title_cn' => 'required',
            'doc_types.*.title' => 'sometimes|required',
            'doc_types.*.title_cn' => 'sometimes|required'
		]);

		Doc::findOrFail($id)
			->update([
				'title' => $request->title,
				'title_cn' => $request->title_cn
			]);

		$currentDocumentIdArray = [];
		foreach($request->doc_types as $docType) {
			if($docType['id'] != null) {
				Doc::where('id', $docType['id'])
					->update([
						'title' => $docType['title'],
						'title_cn' => $docType['title_cn']
					]);

				$currentDocumentIdArray[] = $docType['id'];
			} else {
				Doc::create([
					'title' => $docType['title'],
					'title_cn' => $docType['title_cn'],
					'parent_id' => $id
				]);

				$lastInsertedDocumentId = Doc::orderBy('id', 'desc')->first()->id;
				$currentDocumentIdArray[] = $lastInsertedDocumentId;
			}
		}

		$documents = Doc::where('parent_id', $id)->whereNotIn('id', $currentDocumentIdArray)->get();
		foreach($documents as $document) {
			Doc::findOrFail($document->id)->delete();
		}

		return json_encode([
			'success' => true,
			'message' => 'Document successfully updated.'
		]);
	}

	public function destroy($id) {
		Doc::where('id', $id)->orWhere('parent_id', $id)->delete();

		return json_encode([
			'success' => true,
			'message' => 'Document successfully deleted.'
		]);
	}

}