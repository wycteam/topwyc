<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TrackingController extends Controller
{

	public function index() {
		
		return view('cpanel.visa.tracking.index');
	}
	
}