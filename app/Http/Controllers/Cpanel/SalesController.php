<?php

namespace App\Http\Controllers\Cpanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Rates;
use App\Models\Shopping\Order;

use Excel;

use Carbon\Carbon;

class SalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $rates = Rates::get();
        session()->flash('rates', $rates);

        return view('cpanel/sales/index');
    }

    public function exportDailyOrders() {
        $startDateTime = Carbon::now()->subDay()->hour(16)->minute(0)->second(0);
        $endDateTime = Carbon::now()->hour(16)->minute(0)->second(0);

        $rows = Order::with('user', 'address', 'details.product.store')
            ->with(['details' => function($query) {
                $query->where('status', 'In Transit');
            }])
            ->whereHas('details', function($query) {
                $query->where('status', 'In Transit');
            })
            ->whereBetween('created_at', [$startDateTime, $endDateTime])
            ->get();

        $dateTimeRange = Carbon::today()->subDay()->format('n/j/Y') . ' ' . '4:00PM' .' - '. Carbon::today()->format('n/j/Y') . ' ' . '4:00PM'; 
        $filename = 'WATAF - Daily Orders - ' . $dateTimeRange;
        $sheetName = 'WATAF - Daily Orders';

        return Excel::create($filename, function($excel) use ($rows, $dateTimeRange, $sheetName) {
            $excel->sheet($sheetName, function($sheet) use($rows, $dateTimeRange) {
                $sheet->loadView('cpanel.exports.daily_orders', array('rows' => $rows, 'dateTimeRange' => $dateTimeRange));
            });

        })->download('xlsx');
    }

}
