<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Visa\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function getCategoriesByActionId(Request $request){
    	$actionId = $request->actionId;

    	return Category::orderBy('name')->whereHas('actions', function($query) use($actionId) {
    		$query->where('action_id', $actionId);
    	})->get();
    }
}
