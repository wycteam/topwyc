<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Visa\ClientService;
use App\Models\Visa\Report;
use App\Models\Visa\ReportType;


use App\Classes\Common;
use DB, Carbon\Carbon;

class NotificationManagerController extends Controller
{

	public function index() {
		$this->load_client_list();
		return view('cpanel.visa.notifications.manager.index');
	}

    public function selectServices($id,$rt){
        session()->flash('data', $id);
        session()->flash('message', $rt);

        return view('cpanel.visa.notifications.manager.services');
    }

    public function getClients($id){

        $id = explode(";",$id);
        $user = User::with('services.report')->whereIn('id', $id)->get();
        return $user;
      
    }

    public function getReportType(){
        $reportType = ReportType::orderBy('title', 'asc')->get();
        return $reportType;
    }

    public function getSelectedReportType($id){
        $reportType = ReportType::where('id', $id)->get();
        return $reportType;
    }

    public function saveReport(Request $request){
        $data = $request->all();
        $id = explode(";",$data['id']);
        $user = Auth::user();
        foreach ($id as $service) {
            if($service!=''){
                $sv = ClientService::whereId($service)->first();
                $report = new Report();
                $report->service_id = $sv->id;
                $report->detail = $data['message'];
                $report->user_id = $user->id;
                $report->tracking = $sv->tracking;
                $report->log_date = Carbon::now();
                $report->save();
            }

        }
    }

	public function load_client_list(){
        $myfile = fopen(storage_path('/app/public/data/ClientsReport.txt'), 'w') or die('Unable to open file!');
        $txt = "{\n";
        fwrite($myfile, $txt);
        $txt = '"data":[';
        fwrite($myfile, $txt);
        $txt = "\n";
        fwrite($myfile, $txt);
        $count = 0;

        $records = DB::connection('mysql')
            ->table('users as a')
            ->select(DB::raw('
                a.*,
                total.total_amount as total_costs,
                total.service_date,
                total2.total_deposit,
                total4.total_refund,
                total5.total_discount,
                total6.dates, role.role_id,
                (
                    (case when total.total_amount > 0 then total.total_amount else 0 end)
                    - (
                        (
                            case when total2.total_deposit > 0 then total2.total_deposit else 0 end
                            + case when total3.total_payment > 0 then total3.total_payment else 0 end
                            + case when total5.total_discount > 0 then total5.total_discount else 0 end
                        )
                        - case when total4.total_refund > 0 then total4.total_refund else 0 end
                    )
                ) as total_balance,
                total3.total_payment'))
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(round(sum(b.cost))+round(sum(b.charge))+round(sum(b.tip)),0) as total_amount,
                        COALESCE(sum(b.tip),0) as total_charge,
                        b.client_id, b.service_date
                        from visa.client_services as b
                        where b.active = 1
                        and b.group_id = 0
                        group by b.client_id
                        order by b.service_date desc
                    ) as total'),
                    'total.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(round(sum(c.deposit_amount)),0) as total_deposit,
                        c.client_id
                        from visa.client_deposits as c
                        where c.group_id = 0
                        group by c.client_id
                    ) as total2'),
                    'total2.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(round(sum(d.payment_amount)),0) as total_payment,
                        d.client_id
                        from visa.client_payments as d
                        where d.group_id = 0
                        group by d.client_id
                    ) as total3'),
                    'total3.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(round(sum(e.refund_amount)),0) as total_refund,
                        e.client_id
                        from visa.client_refunds as e
                        where e.group_id = 0
                        group by e.client_id
                    ) as total4'),
                    'total4.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(round(sum(f.discount_amount)),0) as total_discount,
                        f.client_id
                        from visa.client_discounts as f
                        where f.group_id = 0
                        group by f.client_id
                    ) as total5'),
                    'total5.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select *
                        from role_user as r
                        where r.role_id = 9
                    ) as role'),
                    'role.user_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as dates,
                        x.client_id
                        from( SELECT STR_TO_DATE(log_date, "%M %e, %Y, %l:%i %p") as dates,
                            client_id, status
                            FROM visa.packages
                            ORDER BY dates desc
                        ) as x
                        group by x.client_id) as total6'),
                    'total6.client_id', '=', 'a.id')
                ->orderBy(DB::raw('
                    (
                        (case when total.total_amount > 0 then total.total_amount else 0 end)
                        - (
                            (
                                case when total2.total_deposit > 0 then total2.total_deposit else 0 end
                                + case when total3.total_payment > 0 then total3.total_payment else 0 end
                                + case when total5.total_discount > 0 then total5.total_discount else 0 end
                            )
                            - case when total4.total_refund > 0 then total4.total_refund else 0 end
                        )
                    )'), 'desc')
                ->where("role.role_id","9")
                ->get();
        //\Log::info($records);

        foreach ($records as $value) {
	            $count++;
	            $txt = "[";
	            fwrite($myfile, $txt);
	            $txt = '"'.$value->id.'",';
	            fwrite($myfile, $txt);
	            $txt = '"'.ucwords($value->first_name).' '.ucwords($value->last_name).'",';
	            fwrite($myfile, $txt);

	            $txt = '"'.Common::formatMoney($value->total_costs*1).'",';
	            fwrite($myfile, $txt);

	            $txt = '"'.$value->birth_date.'",';
	            fwrite($myfile, $txt);

	            $fullname = ucwords($value->first_name)." ".ucwords($value->last_name);

	            $txt = '"<center><a href=\"javascript:void(0)\" id=\"add'.$value->id.'\" onclick=\"addClient('."'".$value->id."',"."'".$fullname."'".');\"><i class=\"fa fa-plus fa-1x\"></i></a></center>"';
	            fwrite($myfile, $txt);

	            if(count($records)==$count){
	                $txt = "]\n";
	            }else{
	                $txt = "],\n";
	            }
	            fwrite($myfile, $txt);
	        }
	        $txt = "\n]\n}";
	        fwrite($myfile, $txt);
	        fclose($myfile);
	        return true;
	}	

}