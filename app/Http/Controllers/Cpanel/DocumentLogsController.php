<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Models\Visa\ClientService;
use App\Models\Visa\ClientTransactions;
use App\Models\Visa\DocumentLogs;
use App\Models\Visa\Group;
use App\Models\Visa\GroupMember;
use App\Models\Visa\Package;
use App\Models\Visa\Service;
use App\Models\Visa\ServiceDocuments;
use App\Models\User;
use App\Models\Visa\ClientServiceDocument;

use App\Classes\Common;

use Auth, Carbon\Carbon, DB;

use App\Http\Controllers\Controller;

class DocumentLogsController extends Controller
{

	public function clientDocumentLogs($clientId) {
        $clientServices = ClientService::has('documentLogs')
            ->where('client_id', $clientId)
            ->orderBy('id', 'desc')->get()
            ->makeHidden([
                'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                'documentLogs.documents_on_hand_string'
            ]);

        $arraylogs = [];
        $month = null;
        $day = null;
        $year = null;

        foreach($clientServices as $c){
            $cdate = Carbon::parse($c->created_at)->format('M d Y');
            $dt = explode(" ", $cdate);
            $m = $dt[0];
            $d = $dt[1];
            $y = $dt[2];
            if($y == $year){
                $y = null;
                if($m == $month && $d == $day){
                    $m = null;
                    $d = null;
                }
                else{
                    $month = $m;
                    $day = $d;
                }
            }
            else{
                $year = $y;
                $month = $m;
                $day = $d;
            }

            $service = DB::connection('visa')->table('services')->where('id', $c->service_id)->first();

            $details = DB::connection('visa')->table('documents_logs')->where('client_service_id', $c->id)
                ->where('type', null)
                ->orderBy('created_at', 'desc')
                ->distinct('detail')
                ->pluck('detail');
                
            $lists = [];

            foreach($details as $index1 => $detail) {
                $documentLogs = DB::connection('visa')->table('documents_logs')
                    ->where('client_service_id', $c->id)
                    ->where('detail', $detail)
                    ->where('type', null)
                    ->orderBy('created_at', 'desc')
                    ->get();

                $documentsArr = [];
                foreach($documentLogs as $index2 => $documentLog) {
                    $documentIdArr = ($documentLog->documents) ? explode(',', $documentLog->documents) : [];

                    foreach($documentIdArr as $documentId) {
                        $serviceDocument = DB::connection('visa')->table('service_documents')->where('id', $documentId)->first();

                        if( $serviceDocument ) {
                            $processor = DB::table('users')->where('id', $documentLog->processor_id)->first();

                            $documentsArr[] = [
                                'title' => $serviceDocument->title,
                                'is_unique' => $serviceDocument->is_unique,
                                'created_at' => $documentLog->created_at,
                                'latest' => ($index1 == 0 && $index2 == 0) ? true : false,
                                'processor' => $processor->first_name . ' ' . $processor->last_name
                            ];
                        }
                    }
                }

                $lists[] = [
                    'detail' => $detail,
                    'documents' => $documentsArr
                ];
            }

            $arraylogs[] = array(
                'month' => $m,
                'day' => $d,
                'year' => $y,
                'data' => array (
                    'active' => $c->active,
                    'status' => $c->status,
                    'service_name' => $service->detail,
                    'created_at' => Carbon::parse($c->created_at)->format('M d, Y h:i: A'),
                    'lists' => $lists
                )
            );
        }

        return json_encode($arraylogs);
    }

    private function documentIdsToDocumentStrings($documentsIds) {
        $documentStrings = '';

        $documentArray = explode(',', $documentsIds);
        foreach($documentArray as $d) {
            $serviceDocuments = ServiceDocuments::findOrFail($d);

            if(\Request::cookie('locale') == 'en') {
                $documentStrings .= $serviceDocuments->title . ',';
            } else {
                $documentStrings .= $serviceDocuments->title_cn . ',';
            }
        }

        return substr($documentStrings, 0, -1);
    }

    public function clientDocumentLogsDetails($clientId) {
        // $documentLogsDetails = DocumentLogs::whereHas('clientService', function($query) use($clientId) {
        //     $query->where('client_id', $clientId)->where('active', 1);
        // })
        // ->select(array('client_service_id'))
        // ->orderBy('id', 'desc')
        // ->groupBy('client_service_id')
        // ->get()
        // ->makeHidden([
        //     'documents_string', 'documents_on_hand_string'
        // ]);
        
        // $data = [];
        // $counter = 0;
        // foreach($documentLogsDetails as $documentLogsDetail) {
        //     $documentLog = DocumentLogs::where('client_service_id', $documentLogsDetail->client_service_id)
        //         ->orderBy('id', 'desc')
        //         ->first()
        //         ->makeHidden(['documents_string']);

        //     if($documentLog->documents_on_hand) {
        //         $documentsOnHand = ($documentLog->documents_on_hand) ? explode(',', $documentLog->documents_on_hand) : [];
        //         $counter += count($documentsOnHand) + 3;

        //         $clientService = DB::connection('visa')->table('client_services')
        //             ->select(array('service_id', 'status'))
        //             ->where('id', $documentLog->client_service_id)->first();
        //         $service = DB::connection('visa')->table('services')
        //             ->select(array('detail', 'detail_cn'))
        //             ->where('id', $clientService->service_id)->first();

        //         $data['data'][] = [
        //             'service' => (\Request::cookie('locale') == 'en')
        //                 ? $service->detail
        //                 : $service->detail_cn,
        //             'current_status' => $clientService->status,
        //             'current_documents_on_hand' => $documentLog->documents_on_hand_string
        //         ]; 
        //     }
        // }

        // if($counter <= 10) { $className = 'one-column'; }
        // elseif($counter >= 11 && $counter <= 20) { $className = 'two-column'; }
        // elseif($counter >= 21) { $className = 'three-column'; }

        // $data['className'] = $className;

        // return $data;

        $clientServices = ClientService::has('documentLogs')
            ->with(['documentLogs' => function($query) {
                $query->orderBy('id', 'desc');
            }])
            ->where('active', 1)
            ->where('client_id', $clientId)
            ->get()
            ->makeHidden([
                'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
            ]);

        $documentsOnHand = [];

        foreach($clientServices as $clientService) {
            if( $clientService->documentLogs[0]->documents_on_hand  ) {
                $documentsOnHand[] = $clientService->documentLogs[0]->documents_on_hand;
            }
        }

        $docsIdArr = explode(',', implode(',', $documentsOnHand));

        return DB::connection('visa')->table('service_documents')->whereIn('id', $docsIdArr)
            ->select(array('id', 'title'))
            ->get();
    }

    private function clientEditService($id, $oldStatus) {
        $service = ClientService::findOrFail($id);

        $log = '';

        $translated = Service::where('id', $service->service_id)->first();
        $cnserv = $service->detail;
        if($translated) {
            $cnserv = ($translated->detail_cn != '' ? $translated->detail_cn : $translated->detail);
        }

		$log = '&nbsp;<b>Service Status</b> from ' . $oldStatus . ' to ' . $service->status;

        $title = "Status update for Service " . $service->detail;
        $body = 'Service status for ' . $service->detail . ' is now ' . $service->status;
        $title_cn = "服务更新 " . $cnserv;
		$st = '已改为完成';
        $body_cn = " 的服务状态 为" . $cnserv . " " . $st;
        $parent = $service->id;
        $parent_type = 2;

        //include total cost
        $checkDisc = ClientTransactions::where("service_id", $service->id)->first();
        $gTotal = $service->cost + $service->charge + $service->tip;
        if($checkDisc) {
            $gTotal -= $checkDisc->amount;
        }
        $body = $body . " with total cost of Php" . $gTotal;
        $body_cn = $body_cn . " 以及总服务费 Php" . $gTotal;

        $main = $body;
        $main_cn = $body_cn;

        Common::savePushNotif($service->client_id, $title, $body,'Service', $service->id, $main, $main_cn, $title_cn, $body_cn, $parent, $parent_type);

        Common::check_package_updates($service->tracking);
        
        $group = Package::where('tracking', $service->tracking)->first();
        if($log != '') {
            if(substr($service->tracking,0,1)!="G") {
                $new_log = 'Updated Service &nbsp;' . $service->detail . '&nbsp;' . $log . '&nbsp; on Package ' . $service->tracking . '.';
                Common::saveActionLogs($service->id, $service->client_id, $new_log);
            }
        }
    }

    private function groupEditService($id, $groupId) {
        $collection = ClientService::where('id', $id);
        $oldCollect = $collection;
        $clientServices = $collection->get();
        $getGroup = Group::findOrFail($groupId);

        $userId = Auth::user()->id;
        $cdetail = '';
        $colClients = [];
        $cls = '';

        foreach($clientServices as $clientService) {
            $updCollect = '';
            $forleadCollect ='';
            $forleadChinese ='';

            $srv = ClientService::findOrFail($clientService->id);
            $translated = Service::where('id', $srv->service_id)->first();
            $cnserv = $srv->detail;
            if($translated) {
                $cnserv = ($translated->detail_cn != '' ? $translated->detail_cn : $translated->detail);
            }

            $updCollect = '';
            $updChinese = '';

			$status = "<b><h4>" . $srv->status . "</h4></b>";
            $status_cn = "<b><h4> 已完成  </h4></b>";
			$updCollect .= 'Service status is now ' . $status;
			$updChinese .= '服务状态为 ' . $status_cn;

			$new_log = 'Log: Updated Client Service ' . $id . ' - ' . $srv->detail . '</br><b>Service status</b> is now complete';
            Common::saveActionLogs($id, $srv->client_id, $new_log, $groupId);

            $client = User::findOrFail($srv->client_id);

            if($updCollect != ''){
                if( !in_array($srv->client_id, $colClients) ) {
                    $checkIfVice = [];
                    $checkIfVice[] = $srv->client_id;
                    $groupVice = GroupMember::where('group_id', $groupId)
                    	->where('leader', 2)
                    	->whereIn('client_id', $checkIfVice)
                    	->count();
                    if($srv->client_id != $getGroup->leader || $groupVice < 1) {
                        $title = "Update for Service " . $srv->detail;
                        $title_cn = "服务更新 " . $cnserv;
                        $body = "Update for Service " . $srv->detail . ", " . $updCollect;
                        $body_cn = "服务更新 " . $cnserv . ", " . $updChinese;
                        $main = "Update for Service " . $srv->detail . ", " . $updCollect;
                        $main_cn = "服务更新 " . $cnserv . ", " . $updChinese;
                        $parent = $srv->id;
                        $parent_type = 2;
                        Common::savePushNotif($srv->client_id, $title, $body, 'Service', $srv->id, $main, $main_cn, $title_cn, $body_cn, $parent, $parent_type);
                        $cls = $cls . $client->first_name . " " . $client->last_name . ", ";
                    } else {
                        $cls = $cls . $client->first_name . " " . $client->last_name . ", ";
                    }
                    $colClients[] = $srv->client_id;
                }
            }

            // Check package updates
            ClientService::where('group_id', $clientService->group_id)
                ->where('client_id', $clientService->client_id)
                ->where('service_id', $clientService->service_id)
                ->where('tracking', $clientService->tracking)
                ->select('tracking')
                ->get()
                ->map(function($d) {
                    Common::check_package_updates($d->tracking);
                });
        }

        $cls = substr($cls, 0, -2);
        $nms = explode(", ", $cls);
        sort($nms);
        $clientNames = '';
        $crt = 1;
        foreach($nms as $n){
            $clientNames .=$crt.".".$n." ";
            $crt++;
        }

        if($updCollect != '' || $forleadCollect != '') {
            $title = "Update for Service " . $srv->detail;
            $title_cn = "服务更新 " . $cnserv;
            $body = "Update for " . $clientNames . " Service " . $srv->detail;
            $body_cn = "更新了 " . $clientNames . " 服务 " . $cnserv;
            $main = "Update for " . $clientNames . " Service " . $srv->detail . ", " . $updCollect . $forleadCollect;
            $main_cn = "更新了 " . $clientNames . " 服务 " . $cnserv . ", " . $updChinese . $forleadChinese;
            $parent_type = 1;
            $sDate = $srv->service_date;
            $parent = $clientNames;

            Common::savePushNotif($getGroup->leader, $title, $body, 'GService', $sDate . "--" . $getGroup->tracking, $main, $main_cn, $title_cn, $body_cn, $parent, $parent_type, $clientNames);

            $vices = GroupMember::where('group_id', $getGroup->id)->where('leader', 2)->get();
            foreach($vices as $v){
                $par = $clientNames;
                $parent = $v->client_id . "-" . $par;
                Common::savePushNotif($v->client_id, $title, $body, 'GService', $sDate . "--" . $getGroup->tracking, $main, $main_cn, $title_cn, $body_cn, $parent, $parent_type, $clientNames);
            }
        }
    }

    public static function store($clientService, $type, $documents, $detail, $detailCn, $documentsOnHand) {
    	DocumentLogs::create([
            'client_service_id' => $clientService->id,
            'documents' => $documents,
            'documents_on_hand' => $documentsOnHand,
            'processor_id' => Auth::user()->id,
            'type' => $type,
            'detail' => $detail,
            'detail_cn' => $detailCn,
            'log_date' => Carbon::now()->format('Y-m-d')
        ]);

    	$service = Service::findOrFail($clientService->service_id);
    	$outcome = ($service->docs_released != '' && $service->docs_released != null) 
    		? explode(',', $service->docs_released) 
    		: [];
    	$documentsOnHand = explode(',', $documentsOnHand);

    	$result = array_diff($outcome, $documentsOnHand);
    	if(count($result) == 0) {
    		$groupId = $clientService->group_id;
    		$oldStatus = $clientService->status;

    		ClientService::findOrFail($clientService->id)->update([
    			'status' => 'complete',
                'completed_at' => Carbon::now()
    		]);

            // Add optional documents passed by the client to documents_on_hand upon completion
                $docsOptional = ($service->docs_optional != '' && $service->docs_optional != null) 
                    ? explode(',', $service->docs_optional) 
                    : [];
                $cs = ClientService::findOrFail($clientService->id);
                $rcvDocsOptional = ($cs->rcv_docs != '' && $cs->rcv_docs != null)
                    ? explode(',', $cs->rcv_docs)
                    : [];
                $temp = [];
                foreach($rcvDocsOptional as $r) {
                    if( in_array($r, $docsOptional) ) {
                        $temp[] = $r;
                    }
                }
                $documentLog = DocumentLogs::where('client_service_id', $clientService->id)
                    ->orderBy('id', 'desc')->first();
                $documentLog->update([
                    'documents' => implode(',', array_unique(array_merge($temp, explode(',', $documentLog->documents)))),
                    'documents_on_hand' => implode(',', array_unique(array_merge($temp, explode(',', $documentLog->documents_on_hand))))
                ]);

    		if($groupId == 0) {
				(new self)->clientEditService($clientService->id, $oldStatus);
			} else if($groupId > 0) {
				(new self)->groupEditService($clientService->id, $groupId);
			}
    	}

        DocumentLogs::autoUpdateDocuments($type, $clientService->client_id, $documents, $clientService->id);
    }








    private function anyAcrIcard($field, $serviceId) {
        $ids = [];

        $_9a_Original = 243;
        $_9a_Photocopy = 244;

        $_9g_Original = 245;
        $_9g_Photocopy = 246;

        $_prv_Original = 247;
        $_prv_Photocopy = 248;

        $_trv_Original = 249;
        $_trv_Photocopy = 250;

        $_13a_Original = 251;
        $_13a_Photocopy = 252;

        $_sveg_Original = 253;
        $_sveg_Photocopy = 254;

        $_13g_Original = 255;
        $_13g_Photocopy = 256;

        $_quota_visa_Original = 257;
        $_quota_visa_Photocopy = 258;

        // ECC Payment
        if( $serviceId == 374 ) {
            if( $field == 'required_docs' ) {
                $ids = [$_prv_Photocopy, $_trv_Photocopy, $_13a_Photocopy, $_sveg_Photocopy, $_13g_Photocopy, $_quota_visa_Photocopy, $_9g_Photocopy];
            } elseif( $field == 'optional_docs' || $field == 'outcome_optional_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original];
            }
        }

        // ACR I-CARD Renewal
        elseif( $serviceId == 137 ) {
            if( $field == 'required_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original];
            } elseif( $field == 'outcome_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original];
            }
        }

        // ACR I-CARD Replacement
        elseif( $serviceId == 229 ) {
            if( $field == 'optional_docs' || $field == 'outcome_optional_docs' ) {
                $ids = [$_prv_Photocopy, $_trv_Photocopy, $_13a_Photocopy, $_sveg_Photocopy, $_13g_Photocopy, $_quota_visa_Photocopy, $_9g_Photocopy];
            } elseif( $field == 'outcome_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original, $_9g_Original];
            }
        }

        // ACR I-CARD Cancellation
        elseif( $serviceId == 265 ) {
            if( $field == 'required_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original, $_9g_Original];
            }
        }

        // ACR I-CARD Certification
        elseif( $serviceId == 318 ) {
            if( $field == 'required_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original, $_9g_Original];
            } elseif( $field == 'outcome_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original, $_9g_Original];
            }
        }

        // Annual Report Package
        elseif( $serviceId == 168 ) {
            if( $field == 'required_docs' ) {
                $ids = [$_prv_Photocopy, $_trv_Photocopy, $_13a_Photocopy, $_sveg_Photocopy, $_13g_Photocopy, $_quota_visa_Photocopy, $_9g_Photocopy];
            } elseif( $field == 'optional_docs' || $field == 'outcome_optional_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original, $_9g_Original];
            }
        }

        // DFA/ Embassy Authentication
        elseif( $serviceId == 338 ) {
            if( $field == 'optional_docs' || $field == 'outcome_optional_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original, $_9g_Original];
            }
        }

        // New Passport - Package to New Passport 
        elseif( $serviceId == 381 ) {
            if( $field == 'required_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original, $_9g_Original];
            }
        }

        // Lost Passport 
        elseif( $serviceId == 428 ) {
            if( $field == 'required_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original, $_9g_Original];
            }
        }

        // NBI Clearance - Package
        elseif( $serviceId == 116 ) {
            if( $field == 'optional_docs' || $field == 'outcome_optional_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original, $_9g_Original, $_9a_Original, $_prv_Photocopy, $_trv_Photocopy, $_13a_Photocopy, $_sveg_Photocopy, $_13g_Photocopy, $_quota_visa_Photocopy, $_9g_Photocopy, $_9a_Photocopy];
            }
        }

        // Certification
        elseif( $serviceId == 231 ) {
            if( $field == 'optional_docs' || $field == 'outcome_optional_docs' ) {
                $ids = [$_prv_Original, $_trv_Original, $_13a_Original, $_sveg_Original, $_13g_Original, $_quota_visa_Original, $_9g_Original, $_9a_Original, $_prv_Photocopy, $_trv_Photocopy, $_13a_Photocopy, $_sveg_Photocopy, $_13g_Photocopy, $_quota_visa_Photocopy, $_9g_Photocopy, $_9a_Photocopy];
            }
        }

        return DB::connection('visa')->table('service_documents')->whereIn('id', $ids)->get();
    }

    public static function getDocs($serviceId) {
        $service = DB::connection('visa')->table('services')->where('id', $serviceId)->select('id', 'docs_needed', 'docs_optional', 'docs_released', 'docs_released_optional')->first();

        $requiredDocs = explode(',', $service->docs_needed);
        $optionalDocs = explode(',', $service->docs_optional);
        $outcomeDocs = explode(',', $service->docs_released);
        $outcomeOptionalDocs = explode(',', $service->docs_released_optional);

        if( in_array(7, $requiredDocs) ) {
            $requiredDocsArray = DB::connection('visa')->table('service_documents')->whereIn('id', $requiredDocs)->where('id', '<>', 7)->get();
            $anyAcrIcardRequiredDocsArray = (new self)->anyAcrIcard('required_docs', $serviceId);
        } else {
            $requiredDocsArray = DB::connection('visa')->table('service_documents')->whereIn('id', $requiredDocs)->get();
            $anyAcrIcardRequiredDocsArray = [];
        }

        if( in_array(7, $optionalDocs) ) {
            $optionalDocsArray = DB::connection('visa')->table('service_documents')->whereIn('id', $optionalDocs)->where('id', '<>', 7)->get();
            $anyAcrIcardOptionalDocsArray = (new self)->anyAcrIcard('optional_docs', $serviceId);
        } else {
            $optionalDocsArray = DB::connection('visa')->table('service_documents')->whereIn('id', $optionalDocs)->get();
            $anyAcrIcardOptionalDocsArray = [];
        }

        if( in_array(7, $outcomeDocs) ) {
            $outcomeDocsArray = DB::connection('visa')->table('service_documents')->whereIn('id', $outcomeDocs)->where('id', '<>', 7)->get();
            $anyAcrIcardOutcomeDocsArray = (new self)->anyAcrIcard('outcome_docs', $serviceId);
        } else {
            $outcomeDocsArray = DB::connection('visa')->table('service_documents')->whereIn('id', $outcomeDocs)->get();
            $anyAcrIcardOutcomeDocsArray = [];
        }

        if( in_array(7, $outcomeOptionalDocs) ) {
            $outcomeOptionalDocsArray = DB::connection('visa')->table('service_documents')->whereIn('id', $outcomeOptionalDocs)->where('id', '<>', 7)->get();
            $anyAcrIcardOutcomeOptionalDocsArray = (new self)->anyAcrIcard('outcome_optional_docs', $serviceId);
        } else {
            $outcomeOptionalDocsArray = DB::connection('visa')->table('service_documents')->whereIn('id', $outcomeOptionalDocs)->get();
            $anyAcrIcardOutcomeOptionalDocsArray = [];
        }

        return [
            'requiredDocsArray' => $requiredDocsArray,
            'anyAcrIcardRequiredDocsArray' => $anyAcrIcardRequiredDocsArray,
            'optionalDocsArray' => $optionalDocsArray,
            'anyAcrIcardOptionalDocsArray' => $anyAcrIcardOptionalDocsArray,
            'outcomeDocsArray' => $outcomeDocsArray,
            'anyAcrIcardOutcomeDocsArray' => $anyAcrIcardOutcomeDocsArray,
            'outcomeOptionalDocsArray' => $outcomeOptionalDocsArray,
            'anyAcrIcardOutcomeOptionalDocsArray' => $anyAcrIcardOutcomeOptionalDocsArray
        ];
    }

    public static function getClientServiceStatus($serviceId, $docs) {
        $status = 'pending';

        $service = DB::connection('visa')->table('services')->where('id', $serviceId)
            ->select('docs_needed')->first();

        $docsNeeded = explode(',', $service->docs_needed);

        if( in_array(7, $docsNeeded) ) {
            $docs = explode(',', $docs);
            $result = array_unique(array_diff($docsNeeded,$docs));
            if( count($result) == 1 ) {
                $result2 = (new self)->anyAcrIcard('required_docs', $serviceId)->filter(
                    function($d) use($docs) {
                        return in_array($d->id, $docs);
                    }
                );

                if( count($result2) == 1 ) {
                    $status = 'on process';
                }
            }
        } else {
            $docs = explode(',', $docs);
            $result = array_unique(array_diff($docsNeeded, $docs));
            if( count($result) == 0 ) {
                $status = 'on process';
            }
        }

        return $status;
    }

    public static function isComplete($serviceId, $docs) {
        $status = false;

        $service = DB::connection('visa')->table('services')->where('id', $serviceId)
            ->select('docs_released')->first();

        $docsReleased = explode(',', $service->docs_released);

        if( in_array(7, $docsReleased) ) {
            $docs = explode(',', $docs);
            $result = array_unique(array_diff($docsReleased,$docs));
            if( count($result) == 1 ) {
                $result2 = (new self)->anyAcrIcard('outcome_docs', $serviceId)->filter(
                    function($d) use($docs) {
                        return in_array($d->id, $docs);
                    }
                );

                if( count($result2) == 1 ) {
                    $status = true;
                }
            }
        } else {
            $docs = explode(',', $docs);
            $result = array_unique(array_diff($docsReleased, $docs));
            if( count($result) == 0 ) {
                $status = true;
            }
        }

        return $status;
    }

    public static function hasDocumentLogs($clientServiceId) {
        return ClientService::has('documentLogs')->where('id', $clientServiceId)->count() > 0;
    }

    private function getUniqueDocuments($docsIdArr) {
        $docsIdArr = explode(',', implode(',', $docsIdArr));

        return DB::connection('visa')->table('service_documents')->where('is_unique', 1)->whereIn('id', $docsIdArr)->pluck('id');
    }

    private function getClientDocumentsOnHand($clientId) {
        $clientServices = ClientService::has('documentLogs')
            ->with(['documentLogs' => function($query) {
                $query->orderBy('id', 'desc');
            }])
            ->where('active', 1)
            ->where('client_id', $clientId)
            ->get()
            ->makeHidden([
                'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
            ]);

        $documentsOnHand = [];

        foreach($clientServices as $clientService) {
            if( $clientService->documentLogs[0]->documents_on_hand  ) {
                $documentsOnHand[] = $clientService->documentLogs[0]->documents_on_hand;
            }
        }

        return $documentsOnHand;
    }

    private function checkPackage($tracking, $status) {
        $count = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $tracking)
            ->where('active', 1)
            ->where('status', 'like', '%'.$status.'%')
            ->count();

        return $count;
    }

    public static function updatePackage($tracking) {
        $status = 0; // empty

        if( (new self)->checkPackage($tracking, 'complete') > 0 ){
            $status = 4; // complete
        }
        if( (new self)->checkPackage($tracking, 'on process') > 0 ){
            $status = 1; // on process
        }
        if( (new self)->checkPackage($tracking, 'pending') > 0 ){
            $status = 2; // pending
        }

        $data = array('status' => $status);

        DB::connection('visa')
            ->table('packages')
            ->where('tracking', $tracking)
            ->update($data);
    }

    public static function share($clientIdArr, $type = null, $sentence = null) {
        foreach($clientIdArr as $clientId) {
            $uniqueDocuments = (new self)->getUniqueDocuments((new self)->getClientDocumentsOnHand($clientId));
            
            if( count($uniqueDocuments) > 0 ) {
                $clientServices = ClientService::has('documentLogs')
                    ->with(['documentLogs' => function($query) {
                        $query->orderBy('id', 'desc');
                    }])
                    ->where('active', 1)
                    ->where('client_id', $clientId)
                    ->where(function($query) {
                        $query->where('status', 'pending')->orWhere('status', 'on process');
                    })
                    ->get()
                    ->makeHidden([
                        'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                        'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
                    ]);

                foreach($clientServices as $clientService) {
                    $documentsOnHandArray = ($clientService->documentLogs[0]->documents_on_hand) 
                        ? explode(',', $clientService->documentLogs[0]->documents_on_hand)
                        : [];

                    $service = DB::connection('visa')->table('services')->where('id', $clientService->service_id)->first();
                    
                    $docsNeededArray = ($service->docs_needed) ? explode(',', $service->docs_needed) : [];
                    if( in_array(7, $docsNeededArray) ) {
                        $docsNeededArray = array_diff($docsNeededArray, [7]);

                        $flag = true;
                        $anyAcrIcard = [];
                        $temp = (new self)->anyAcrIcard('required_docs', $clientService->service_id);
                        foreach($temp as $t) {
                            $anyAcrIcard[] = $t->id;
                            
                            if( in_array($t->id, $documentsOnHandArray) ) {
                                $flag = false;
                            }
                        }

                        if( $flag ) {
                            $docsNeededArray = array_unique(array_merge($docsNeededArray, $anyAcrIcard));
                        }
                    }

                    $docsOptionalArray = ($service->docs_optional) ? explode(',', $service->docs_optional) : [];
                    if( in_array(7, $docsOptionalArray) ) {
                        $docsOptionalArray = array_diff($docsOptionalArray, [7]);

                        $flag = true;
                        $anyAcrIcard = [];
                        $temp = (new self)->anyAcrIcard('optional_docs', $clientService->service_id);
                        foreach($temp as $t) {
                            $anyAcrIcard[] = $t->id;

                            if( in_array($t->id, $documentsOnHandArray) ) {
                                $flag = false;
                            }
                        }

                        if( $flag ) {
                            $docsOptionalArray = array_unique(array_merge($docsOptionalArray, $anyAcrIcard));
                        }
                    }

                    $toBeAdded = [];

                    foreach($uniqueDocuments as $uniqueDocument) {
                        if( (in_array($uniqueDocument, $docsNeededArray) || in_array($uniqueDocument, $docsOptionalArray)) && !in_array($uniqueDocument, $documentsOnHandArray) ) {
                            $toBeAdded[] = $uniqueDocument;
                        }
                    }

                    if( count($toBeAdded) > 0 ) {
                        $rcvDocs = implode(',', array_unique(
                            array_merge(
                                ($clientService->rcv_docs) ? explode(',', $clientService->rcv_docs) : [], 
                                $toBeAdded
                            )
                        ));
                        $status = (new self)::getClientServiceStatus($clientService->service_id, $rcvDocs);

                        $clientService->update([
                            'rcv_docs' => $rcvDocs,
                            'status' => $status
                        ]);

                        (new self)::updatePackage($clientService->tracking);

                        $originsArray = (new self)::getOrigins($toBeAdded, $clientId, $clientService->id);

                        (new self)::saveOrigins($originsArray, $clientService->id, $type, $sentence);
                    }
                }
            }
        }
    }

    public static function cancelClientServices($cancelledClientServicesArr) {
        $processorId = Auth::user()->id;

        foreach($cancelledClientServicesArr as $cancelledClientService) {
            $documentLog = DB::connection('visa')->table('documents_logs')->where('client_service_id', $cancelledClientService->id)->orderBy('id', 'desc')->first();

            if( $documentLog ) {
                $documentsOnHand = ($documentLog->documents_on_hand) 
                    ? explode(',', $documentLog->documents_on_hand)
                    : [];

                $clientDocumentsOnHand = (new self)->getClientDocumentsOnHand($cancelledClientService->client_id);
                $clientDocumentsOnHand = explode(',', implode(',', $clientDocumentsOnHand));

                $toBeReleased = array_unique(array_diff($documentsOnHand, $clientDocumentsOnHand));

                ClientServiceDocument::where('client_service_id', $cancelledClientService->id)->delete();

                if( count($toBeReleased) > 0 ) {
                    $dl_detail = 'Released document/s to client for cancelled service:';

                    DocumentLogs::create([
                        'client_service_id' => $cancelledClientService->id,
                        'documents' => implode(',', $toBeReleased),
                        'documents_on_hand' => null,
                        'processor_id' => $processorId,
                        'detail' => $dl_detail,
                        'detail_cn' => $dl_detail,
                        'log_date' => Carbon::now()->format('Y-m-d')
                    ]);
                }

                $remaining = array_unique(array_diff($documentsOnHand, $toBeReleased));
                if( count($remaining) > 0 ) {
                    $dl_detail = 'Removed document/s for cancelled service:';

                    DocumentLogs::create([
                        'client_service_id' => $cancelledClientService->id,
                        'documents' => implode(',', $remaining),
                        'documents_on_hand' => null,
                        'processor_id' => $processorId,
                        'detail' => $dl_detail,
                        'detail_cn' => $dl_detail,
                        'log_date' => Carbon::now()->format('Y-m-d')
                    ]);
                }
            }
        }
    }

    public static function removeDocuments($clientId, $docsIdArr, $exceptClientServiceId = 0, $type = null, $sentence = null) {
        $uniqueDocuments = (new self)->getUniqueDocuments($docsIdArr)->toArray();

        if( count($uniqueDocuments) > 0 ) {
            $processorId = Auth::user()->id;

            $clientServices = ClientService::has('documentLogs')
                ->with(['documentLogs' => function($query) {
                    $query->orderBy('id', 'desc');
                }])
                ->where('active', 1)
                ->where('client_id', $clientId)
                ->where('id', '<>', $exceptClientServiceId)
                ->where(function($query) {
                    $query->where('status', 'pending')->orWhere('status', 'on process');
                })
                ->get()
                ->makeHidden([
                    'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                    'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
                ]);

            foreach($clientServices as $clientService) {
                $rcvDocs = ($clientService->rcv_docs) ? explode(',', $clientService->rcv_docs) : [];
                $rcvDocs = array_unique(array_diff($rcvDocs, $uniqueDocuments));

                if( $clientService->rcv_docs != implode(',', $rcvDocs) ) {
                    $status = (new self)::getClientServiceStatus($clientService->service_id, implode(',', $rcvDocs));

                    $clientService->update([
                        'rcv_docs' => count($rcvDocs) > 0 ? implode(',', $rcvDocs) : null,
                        'status' => $status
                    ]);

                    (new self)::updatePackage($clientService->tracking);

                    $exceptClientService = DB::connection('visa')->table('client_services')->where('id', $exceptClientServiceId)->first();
                    $service = DB::connection('visa')->table('services')->where('id', $exceptClientService->service_id)->first();
                    if( $type == 'complete' ) {
                        $dl_detail = 'Released document/s to client for completed service <strong style="color:orange;">'.$service->detail.'</strong>:';
                    } elseif( $type == 'report' ) {
                        $dl_detail = $sentence;
                    } else {
                        $dl_detail = 'Removed document/s from client for service <strong style="color:orange;">'.$service->detail.'</strong>:';
                    }

                    DocumentLogs::create([
                        'client_service_id' => $clientService->id,
                        'documents' => implode(',', $uniqueDocuments),
                        'documents_on_hand' => count($rcvDocs) > 0 ? implode(',', $rcvDocs) : null,
                        'processor_id' => $processorId,
                        'type' => ( $type == 'report' ) ? 'Submitted' : null,
                        'detail' => $dl_detail,
                        'detail_cn' => $dl_detail,
                        'log_date' => Carbon::now()->format('Y-m-d')
                    ]);
                }
            }
        }
    }

    public static function releaseDocuments($clientId, $docsIdArr, $exceptClientServiceId = 0) {
        $uniqueDocuments = (new self)->getUniqueDocuments($docsIdArr)->toArray();

        if( count($uniqueDocuments) > 0 ) {
            $clientServices = ClientService::has('documentLogs')
                ->with(['documentLogs' => function($query) {
                    $query->orderBy('id', 'desc');
                }])
                ->where('active', 1)
                ->where('client_id', $clientId)
                ->where('id', '<>', $exceptClientServiceId)
                ->where('status', 'complete')
                ->get()
                ->makeHidden([
                    'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                    'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
                ]);

            foreach($clientServices as $clientService) {
                $documentsOnHandArray = ($clientService->documentLogs[0]->documents_on_hand) 
                    ? explode(',', $clientService->documentLogs[0]->documents_on_hand)
                    : [];

                $toBeReleaseDocuments = [];
                foreach($documentsOnHandArray as $d) {
                    if( in_array($d, $uniqueDocuments) ) {
                        $toBeReleaseDocuments[] = $d;
                    }
                }

                if( count($toBeReleaseDocuments) > 0 ) {
                    $rcvDocsReleased = ($clientService->rcv_docs_released) ? explode(',', $clientService->rcv_docs_released) : [];

                    $clientService->update([
                        'rcv_docs_released' => implode(',', array_unique(array_merge($rcvDocsReleased, $toBeReleaseDocuments)))
                    ]);

                    $documentsOnHand = array_unique(array_diff($documentsOnHandArray,  $toBeReleaseDocuments));

                    $_exceptClientService = DB::connection('visa')->table('client_services')->where('id', $exceptClientServiceId)->first();
                    $_service = DB::connection('visa')->table('services')->where('id', $_exceptClientService->service_id)->first();
                    $dl_detail = 'Released document/s to client for completed service <strong style="color:orange;">'.$_service->detail.'</strong>:';

                    DocumentLogs::create([
                        'client_service_id' => $clientService->id,
                        'documents' => (count($toBeReleaseDocuments) > 0) 
                            ? implode(',', $toBeReleaseDocuments)
                            : null,
                        'documents_on_hand' => (count($documentsOnHand) > 0)
                            ? implode(',', $documentsOnHand)
                            : null,
                        'processor_id' => Auth::user()->id,
                        'detail' => $dl_detail,
                        'detail_cn' => $dl_detail,
                        'log_date' => Carbon::now()->format('Y-m-d')
                    ]);
                }
            }
        }
    }

    public function quickRelease(Request $request) {
        $clientId = $request->clientId;
        $documents = $request->documents;

        $clientServices = ClientService::has('documentLogs')
            ->with(['documentLogs' => function($query) {
                $query->orderBy('id', 'desc');
            }])
            ->where('active', 1)
            ->where('client_id', $clientId)
            ->get()
            ->makeHidden([
                'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
            ]);

        foreach($clientServices as $clientService) {
            $documentsOnHandArray = ($clientService->documentLogs[0]->documents_on_hand) 
                ? explode(',', $clientService->documentLogs[0]->documents_on_hand)
                : [];

            $toBeReleaseDocuments = array_intersect($documentsOnHandArray, $documents);

            if( count($toBeReleaseDocuments) > 0 ) {
                $_clientServiceId = $clientService->id;
                $_documents = implode(',', $toBeReleaseDocuments);
                $_documentsOnHand = array_unique(array_diff($documentsOnHandArray,  $toBeReleaseDocuments));
                $_processorId = Auth::user()->id;
                $_service = Service::where('id', $clientService->service_id)->first();
                $_dlDetail = 'Released document/s to client via quick release:';
                $_logDate = Carbon::now()->format('Y-m-d');

                if($clientService->status == 'pending') {
                    $rcvDocs = null;

                    if($clientService->rcv_docs) {
                        $remaining = array_unique(
                            array_diff(
                                explode(',', $clientService->rcv_docs),
                                $toBeReleaseDocuments
                            )
                        );

                        if(count($remaining) != 0) {
                            $rcvDocs = implode(',', $remaining);
                        }
                    }

                    $clientService->update([
                        'rcv_docs' => $rcvDocs
                    ]);
                } elseif($clientService->status == 'on process') {
                    if( $clientService->report_docs ) {
                        $clientService->update([
                            'rcv_docs_released' => ($clientService->rcv_docs_released)
                                ? implode(',', array_unique(
                                    array_merge(
                                        explode(',', $clientService->rcv_docs_released), 
                                        $toBeReleaseDocuments
                                    )
                                ))
                                : implode(',', array_unique($toBeReleaseDocuments))
                        ]);
                    } else {
                        $rcvDocs = null;

                        if($clientService->rcv_docs) {
                            $remaining = array_unique(
                                array_diff(
                                    explode(',', $clientService->rcv_docs),
                                    $toBeReleaseDocuments
                                )
                            );

                            if(count($remaining) != 0) {
                                $rcvDocs = implode(',', $remaining);
                            }
                        }
                    
                        $clientService->update([
                            'rcv_docs' => $rcvDocs,
                            'status' => (new self)::getClientServiceStatus(
                                $clientService->service_id,
                                (count($_documentsOnHand) > 0)
                                    ? implode(',', $_documentsOnHand)
                                    : null
                            )
                        ]);
                    }
                } elseif($clientService->status == 'complete') {
                    $clientService->update([
                        'rcv_docs_released' => ($clientService->rcv_docs_released)
                            ? implode(',', array_unique(
                                array_merge(
                                    explode(',', $clientService->rcv_docs_released), 
                                    $toBeReleaseDocuments
                                )
                            ))
                            : implode(',', array_unique($toBeReleaseDocuments))
                    ]);
                }

                (new self)::updatePackage($clientService->tracking);

                ClientServiceDocument::whereHas('clientService', function($query) use($clientId) {
                    $query->where('client_id', $clientId);
                })
                ->whereIn('document_id', $toBeReleaseDocuments)
                ->delete();

                DocumentLogs::create([
                    'client_service_id' => $_clientServiceId,
                    'documents' => $_documents,
                    'documents_on_hand' => (count($_documentsOnHand) > 0)
                        ? implode(',', $_documentsOnHand)
                        : null,
                    'processor_id' => $_processorId,
                    'detail' => $_dlDetail,
                    'detail_cn' => $_dlDetail,
                    'log_date' => $_logDate
                ]);
            }
        }

        return json_encode([
            'success' => true
        ]);
    }

    public static function getOrigins($docsArray, $clientId, $clientServiceId) {
        $origins = [];

        foreach($docsArray as $d) {
            $clientServiceDocument = ClientServiceDocument::whereHas('serviceDocument', function($query) {
                    $query->where('is_unique', 1);
                })
                ->whereHas('clientService', function($query) use($clientId) {
                    $query->where('client_id', $clientId)->where('active', 1);
                })
                ->where('document_id', $d)
                ->first();

            if( $clientServiceDocument ) {
                $origins[$clientServiceDocument->client_service_id]['documents'][] = $d;
                $origins[$clientServiceDocument->client_service_id]['created_at'] = $clientServiceDocument->created_at;
            } else {
                $createdAt =  Carbon::now();

                $origins[$clientServiceId]['documents'][] = $d;
                $origins[$clientServiceId]['created_at'] = $createdAt;

                ClientServiceDocument::firstOrCreate([
                    'client_service_id' => $clientServiceId,
                    'document_id' => $d,
                    'created_at' => $createdAt
                ]);
            }
        }

        return $origins;
    }

    public static function saveOrigins($originsArray, $clientServiceId, $type = null, $sentence = null) {
        $processorId = Auth::user()->id;

        foreach($originsArray as $key => $o) {
            $documentLog = DocumentLogs::where('client_service_id', $clientServiceId)->orderBy('id', 'desc')->first();
            if( $documentLog ) {
                $documentsOnHand = ($documentLog->documents_on_hand)
                    ? explode(',', $documentLog->documents_on_hand)
                    : [];

                $documentsOnHand = implode(',', array_unique(array_merge($documentsOnHand, $o['documents'])));
            } else {
                $documentsOnHand = implode(',', $o['documents']);
            }

            if( $type == 'report' ) {
                $detail = $sentence;
                $detailCn = $sentence;
            } else {
                if( $key == $clientServiceId ) {
                    $detail = 'Received document/s from client:';
                    $detailCn = 'Received document/s from client:';
                } else {
                    $clientServiceOrigin = DB::connection('visa')->table('client_services')->where('id', $key)->first();
                    $serviceOrigin = Service::findOrFail($clientServiceOrigin->service_id);

                    $detail = 'Received document/s from client for service <strong style="color:orange;">'.$serviceOrigin->detail.'</strong>:';
                    $detailCn = 'Received document/s from client for service <strong style="color:orange;">'.$serviceOrigin->detail.'</strong>:';
                }
            }

            DocumentLogs::create([
                'client_service_id' => $clientServiceId,
                'documents' => implode(',', $o['documents']),
                'documents_on_hand' => $documentsOnHand,
                'processor_id' => $processorId,
                'detail' => $detail,
                'detail_cn' => $detailCn,
                'log_date' => Carbon::now()->format('Y-m-d'),
                'created_at' => $o['created_at']
            ]);
        }
    }

}