<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Visa\Branch;
use App\Models\User;

class BranchesController extends Controller
{

    public function translation() {
        return json_encode([
            'translation' => trans('cpanel/branch-manager-page')
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $lang = trans('cpanel/branch-manager-page');
        $data = [
            'title' => $lang['branch-manager'],
            'enterBranchName' => $lang['branch-name']
        ];
         return view('cpanel.visa.branches.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Branch::create($request->all());
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Branch::whereId($id)->first();
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data = Branch::whereId($id)
            ->update([
                'name' => $request->name,
            ]);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getBranches()
    {
        $branches = Branch::all();
        foreach ($branches as $key=>$branch) {
            $clients = $this->getClients($branch->id);
            $branches[$key]['count_clients'] = Count($clients);
        }
        return $branches;
    }

    public function getClients($branch)
    {   
        $clients = User::where(function ($query) use($branch){
                            $query->where('branch_id', $branch)
                                  ->orwhere('branch_id', '3');
                        })->get();

        return $clients;
    }

    public function delete($id){
        $data = Branch::whereId($id);
        $data->delete();
        return response()->json($data);
    }

    public function _getBranches() {
        return Branch::all();
    }

}
