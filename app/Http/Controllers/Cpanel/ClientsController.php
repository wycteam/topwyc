<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ClientsValidation;
use App\Http\Requests\Clients2Validation;
use App\Http\Requests\CpanelDocumentsValidation;
use App\Models\Visa\Finance as Finance;
use App\Models\Visa\BudgetOutput as BudgetOutput;

use App\Models\Document;
use App\Models\Role;
use App\Models\User;
use App\Models\UserOpen;
use App\Models\ShopRate;
use App\Models\Permission;
use App\Models\Visa\Group;
use App\Models\Visa\Branch;
use App\Models\Visa\GroupMember;
use App\Models\Visa\ClientTransactions;
use App\Models\Visa\Deposit;
use App\Models\Visa\Refund;
use App\Models\Visa\Payment;
use App\Models\Visa\Discount;
use App\Models\Visa\TransferBalance;
use App\Models\Visa\ClientService;
use App\Models\Visa\Package;
use App\Models\Visa\Service;
use App\Models\Visa\ServiceBranchCost;
use App\Models\Visa\ServiceDocuments;
use App\Models\Visa\ActionLogs;
use App\Models\Visa\TransactionLogs;
use App\Models\Visa\CommissionLogs;
use App\Models\Visa\Report;
use App\Models\Visa\Task;
use App\Models\Visa\Updates;
use App\Models\Address;
use App\Models\Visa\DocumentLogs;
use App\Models\Visa\ClientServiceDocument;

use App\Acme\Filters\UserFilters;

use App\Http\Controllers\Cpanel\TasksController;
use App\Http\Controllers\Cpanel\SynchronizationController;
use App\Http\Controllers\Cpanel\DocumentLogsController;

use App\Classes\Common;
use Image, DB, Carbon\Carbon;
use Hash;
use File;
use PDF;
use Storage;


class ClientsController extends Controller
{

    public function getTranslation() {
        return json_encode([
            'translation' => trans('cpanel/clients-page')
        ]);
    }

    public function addTemporaryClientTranslation() {
        return json_encode([
            'translation' => trans('cpanel/add-temporary-client-component')
        ]);
    }

    public function transactionLogsTranslation() {
        return json_encode([
            'translation' => trans('cpanel/transaction-logs-component')
        ]);
    }

    public function clientDocumentsTranslation() {
        return json_encode([
            'translation' => trans('cpanel/client-documents-component')
        ]);
    }

    public function packageTranslation() {
        return json_encode([
            'translation' => trans('cpanel/package-component')
        ]);
    }

    public function viewReportTranslation() {
        return json_encode([
            'translation' => trans('cpanel/view-report-component')
        ]);
    }

    public function addServiceTranslation() {
        return json_encode([
            'translation' => trans('cpanel/add-service-component')
        ]);
    }

    public function editServiceTranslation() {
        return json_encode([
            'translation' => trans('cpanel/edit-service-component')
        ]);
    }


    public function index() {
        $lang = trans('cpanel/clients-page');
        $ajax = $this->load_client_list();
        $col = User::where('collectable','<',0)->sum('collectable');
        $data = array(
            'title' => $lang['clients'],
            // 'collect' => 0,
            'collect' => Common::formatMoney($col*1),
        );
       
        return view('cpanel.visa.clients.index', $data);
    }

    public function trackingList() {
        $data = [
            'title' => 'Clients'
        ];
        $id = Auth::user()->id;
        $selectedUser = User::where('id',$id)->first();
        $assignedRoles = $selectedUser->roles;
        $assignedPermissions = $selectedUser->permissions;

        $roles = Role::all();
        $permissions = Permission::all();

        session()->flash('data', $selectedUser);
        return view('cpanel.visa.clients.list-of-tracking', $data);
    }


    public function create()
    {
        $lang = trans('cpanel/clients-page');
        $trans = trans('cpanel/client-edit-create-page');
        $data = [
            'title' => $lang['create-client'],
            'lang'  => $trans
        ];
        return view('cpanel.visa.clients.create', $data);
    }

    public function store(ClientsValidation $request)
    {
        $trans = trans('cpanel/client-edit-create-page');
        $binded = true;
        if($request->group_id != 0) {
            $groupLeaderId = Group::findOrFail($request->group_id)->leader;
            $groupLeader = User::findOrfail($groupLeaderId);

            if(
                // (is_null($groupLeader->password) || $groupLeader->password == '')
                // &&
                // $groupLeader->last_name == 'N/A'
                //&&
                empty($request->contact_number)
            ) {
                $binded = false;
                $contactNumber = $groupLeader->contact_number;
            }
        }

        $branch_id = $request->get('branch_id');
        if($request->get('branch_id') == null){
            $branch_id = Auth::user()->branch_id;
        }

        $user = new User($request->all());
        $user->address = $request->address['address'];
        $user->contact_number = $request->address['contact_number'];
        $user->alternate_contact_number = $request->address['alternate_contact_number'];
        $user->date_register = Carbon::now()->format('m/d/Y');
        $user->group_id = $request->get('group_id');
        $user->branch_id = $branch_id;
        $user->is_verified = true;

        if(!empty($request->address['contact_number'])){
            $contact_number = $request->address['contact_number'];
            preg_match_all('!\d+!', $contact_number, $matches);
            $contact_number = implode("", $matches[0]);
            $contact_number = ltrim($contact_number,"0");
            $contact_number = ltrim($contact_number,'+');
            $contact_number = ltrim($contact_number,'63');
            $user->password = bcrypt("0".$contact_number);
        }

        if($request->group_id == 0){
            if(($request->address['contact_number']==NULL || $request->address['contact_number']=='') && ($request->address['alternate_contact_number']!=NULL || $request->address['alternate_contact_number']!='')){
                $user->contact_number = $request->address['alternate_contact_number'];
                $user->alternate_contact_number = NULL;
            }
        }
        if($request->visa_type=='9G' || $request->visa_type=='TRV'){
          $user->visa_type = $request->visa_type;
          $user->exp_date =  (($request->service_exp_date!='') ? Carbon::parse($request->service_exp_date)->format('Y-m-d') : NULL);
          $user->issue_date = (($request->issue_date!='') ? Carbon::parse($request->issue_date)->format('Y-m-d') : NULL);
          $user->icard_exp_date = (($request->icard_exp_date!='') ? Carbon::parse($request->icard_exp_date)->format('Y-m-d') : NULL);
        }else if($request->visa_type=='CWV'){
          $user->visa_type = $request->visa_type;
          $user->exp_date = (($request->service_exp_date!='') ? Carbon::parse($request->service_exp_date)->format('Y-m-d') : NULL);
        }else if ($request->visa_type=='9A'){
          $user->visa_type = $request->visa_type;
          $user->arrival_date = (($request->arrival_date!='') ? Carbon::parse($request->arrival_date)->format('Y-m-d') : NULL);
          $user->first_exp_date = (($request->first_exp_date!='') ? Carbon::parse($request->first_exp_date)->format('Y-m-d') : NULL);
          $user->exp_date = (($request->service_exp_date!='') ? Carbon::parse($request->service_exp_date)->format('Y-m-d') : NULL);
        }else{
          $user->visa_type=null;
        }
        $user->save();


        SynchronizationController::update('clients', Carbon::now()->toDateTimeString());
        //SynchronizationController::insertUpdate($user->id, Carbon::now()->toDateTimeString());

        if($user->group_id > 0 ){
            $group = new GroupMember();
            $group->group_id = $user->group_id;
            $group->client_id = $user->id;
            $group->leader = 0;
            $group->save();
            SynchronizationController::update('groups', Carbon::now()->toDateTimeString());
        }


        $this->load_client_list();

        // $addressesArray = $request->get('address');
        // if(!$binded) {
        //     $addressesArray['contact_number'] = $contactNumber;
        // }
        // $user->addresses()->create($addressesArray);

        $user->assignRole(['0' => 9]);

        session()->flash('message', $trans['msg-client-create-success']);

        $act_id = $user->id;
        $branchName = Branch::findOrFail($branch_id)->name;
        $detail = "Created new client -> ".$user->first_name.' '.$user->middle_name.' '.$user->last_name . ' [' . $branchName . ']';
        Common::saveActionLogs(0,$act_id,$detail);

        $selectedUser = User::where('id',$act_id)->first();
        $assignedRoles = $selectedUser->roles;
        $assignedPermissions = $selectedUser->permissions;

        $roles = Role::all();
        $permissions = Permission::all();

        session()->flash('data', $selectedUser);

        // return view('cpanel/visa/clients/profile', compact('selectedUser', 'assignedRoles', 'assignedPermissions', 'roles', 'permissions'));

        return redirect()->to('/visa/client/'.$act_id)->with([
            'selectedUser' => $selectedUser,
            'assignedRoles' => $assignedRoles,
            'assignedPermissions' => $assignedPermissions,
            'roles' => $roles,
            'permissions' => $permissions
        ]);
    }

    public function store2(Clients2Validation $request)
    {
        $trans = trans('cpanel/client-edit-create-page');
        $binded = true;
        if($request->group_id != 0) {
            $groupLeaderId = Group::findOrFail($request->group_id)->leader;
            $groupLeader = User::findOrfail($groupLeaderId);

            if( 
                // (is_null($groupLeader->password) || $groupLeader->password == '')
                // &&
                // $groupLeader->last_name == 'N/A'
                // &&
                empty($request->contact_number)
            ) {
                $binded = false;
                $contactNumber = $groupLeader->contact_number;
            }
        }

        $branch_id = $request->get('branch_id');
        if($request->get('branch_id') == null){
            $branch_id = Auth::user()->branch_id;
        }

        $user = new User($request->all());
        $user->address = $request->address['address'];
        $user->contact_number = $request->address['contact_number'];
        $user->alternate_contact_number = $request->address['alternate_contact_number'];
        $user->date_register = Carbon::now()->format('m/d/Y');
        $user->group_id = $request->get('group_id');
        $user->branch_id = $branch_id;

        if(!empty($request->address['contact_number'])){
            $contact_number = $request->address['contact_number'];
            preg_match_all('!\d+!', $contact_number, $matches);
            $contact_number = implode("", $matches[0]);
            $contact_number = ltrim($contact_number,"0");
            $contact_number = ltrim($contact_number,'+');
            $contact_number = ltrim($contact_number,'63');
            $user->password = bcrypt("0".$contact_number);
        }

        if($request->group_id == 0){
            if(($request->address['contact_number']==NULL || $request->address['contact_number']=='') && ($request->address['alternate_contact_number']!=NULL || $request->address['alternate_contact_number']!='')){
                $user->contact_number = $request->address['alternate_contact_number'];
                $user->alternate_contact_number = NULL;
            }
        }
        if($request->visa_type=='9G' || $request->visa_type=='TRV'){
          $user->visa_type = $request->visa_type;
          $user->exp_date =  (($request->service_exp_date!='') ? Carbon::parse($request->service_exp_date)->format('Y-m-d') : NULL);
          $user->issue_date = (($request->issue_date!='') ? Carbon::parse($request->issue_date)->format('Y-m-d') : NULL);
          $user->icard_exp_date =  (($request->icard_exp_date!='') ? Carbon::parse($request->icard_exp_date)->format('Y-m-d') : NULL);
        }else if($request->visa_type=='CWV'){
          $user->visa_type = $request->visa_type;
          $user->exp_date = (($request->service_exp_date!='') ? Carbon::parse($request->service_exp_date)->format('Y-m-d') : NULL);
        }else if ($request->visa_type=='9A'){
          $user->visa_type = $request->visa_type;
          $user->arrival_date = (($request->arrival_date!='') ? Carbon::parse($request->arrival_date)->format('Y-m-d') : NULL);
          $user->first_exp_date = (($request->first_exp_date!='') ? Carbon::parse($request->first_exp_date)->format('Y-m-d') : NULL);
          $user->exp_date = (($request->service_exp_date!='') ? Carbon::parse($request->service_exp_date)->format('Y-m-d') : NULL);
        }else{
          $user->visa_type=null;
        }
      //\Log::info($user);
        $user->save();

        SynchronizationController::update('clients', Carbon::now()->toDateTimeString());
        //SynchronizationController::insertUpdate($user->id, Carbon::now()->toDateTimeString());

        if($user->group_id > 0 ){
            $group = new GroupMember();
            $group->group_id = $user->group_id;
            $group->client_id = $user->id;
            $group->leader = 0;
            $group->save();
            SynchronizationController::update('groups', Carbon::now()->toDateTimeString());
        }


        $this->load_client_list();

        // $addressesArray = $request->get('address');
        // if(!$binded) {
        //     $addressesArray['contact_number'] = $contactNumber;
        // }
        // $user->addresses()->create($addressesArray);

        $user->assignRole(['0' => 9]);

        session()->flash('message', $trans['msg-client-create-success']);

        $act_id = $user->id;
        $branchName = Branch::findOrFail($request->get('branch_id'))->name;
        $detail = "Created new client -> ".$user->first_name.' '.$user->middle_name.' '.$user->last_name . ' [' . $branchName . ']';
        Common::saveActionLogs(0,$act_id,$detail);

        $selectedUser = User::where('id',$act_id)->first();
        $assignedRoles = $selectedUser->roles;
        $assignedPermissions = $selectedUser->permissions;

        $roles = Role::all();
        $permissions = Permission::all();

        session()->flash('data', $selectedUser);

        // return view('cpanel/visa/clients/profile', compact('selectedUser', 'assignedRoles', 'assignedPermissions', 'roles', 'permissions'));

        return redirect()->to('/visa/client/'.$act_id)->with([
            'selectedUser' => $selectedUser,
            'assignedRoles' => $assignedRoles,
            'assignedPermissions' => $assignedPermissions,
            'roles' => $roles,
            'permissions' => $permissions
        ]);
    }

    public function edit($id)
    {   
        $lang = trans('cpanel/clients-page');
        $trans = trans('cpanel/client-edit-create-page');
        $data = [
            'title' => $lang['edit-client'],
            'lang'  => $trans
        ];

        //if(VisaType::where('user_id', $id)->count()>0){
            //$visa =  User::select('user_visa_linker.*','users.id')->leftJoin('user_visa_linker', 'users.id', '=', 'user_visa_linker.user_id')->where('users.id',$id)->first();
        //}

        $client =  User::with('roles','permissions')->whereId($id)->get();
        $validemail = filter_var( $client[0]->email, FILTER_VALIDATE_EMAIL );
        if(!$validemail){
            $client[0]->email = null;
        }
        session()->flash('data', array('data'=>$client[0]));

        return view('cpanel.visa.clients.edit', compact('client'), $data);
    }

    public function update(ClientsValidation $request, $id)
    {
        $user = User::find($id);
        $old_contact_number = $user->contact_number;
        $contact_number = $request->get('contact_number');
        $user->fill($request->all());
        $user->group_id = $request->get('group_id');
        if(!empty($contact_number)){
            preg_match_all('!\d+!', $contact_number, $matches);
            $contact_number = implode("", $matches[0]);
            $contact_number = ltrim($contact_number,"0");
            $contact_number = ltrim($contact_number,'+');
            $contact_number = ltrim($contact_number,'63');
            if("0".$contact_number != $old_contact_number){
                $user->password = bcrypt("0".$contact_number);
            }
        }
        $user->save();
       
        $binded = true;
        if($request->group_id != 0) {
            $groupLeaderId = Group::findOrFail($request->group_id)->leader;
            $groupLeader = User::findOrfail($groupLeaderId);

            if( (is_null($groupLeader->password) || $groupLeader->password == '')
                &&
                $groupLeader->last_name == 'N/A'
                &&
                empty($request->get('contact_number'))
            ) {
                $binded = false;
                $contactNumber = $groupLeader->contact_number;
            }
            else if($user->contact_number == ($request->get('contact_number'))){
                $binded = false;
                $contactNumber = $request->get('contact_number');
            }
        }else{
            if(($request->get('contact_number')==NULL || $request->get('contact_number')=='') && ($request->get('alternate_contact_number')!=NULL || $request->get('alternate_contact_number')!='')){
                $user->contact_number = $request->get('alternate_contact_number');
                $user->alternate_contact_number = NULL;
            }
        }


        if($user->isDirty()){
            //getOriginal() -> get original values of model
            //getDirty -> get all fields updated with value
            $changes = $user->getDirty();
            $detail = "Updated client Account.";
            foreach ($changes as $key => $value) {
                $old = $user->getOriginal($key);
                $act_id = $user->id;
                $detail .= "Change ".$key." from ".$old." to ".$value.". ";
            }
            Common::saveActionLogs(0,$act_id,$detail);
            $user->save();
            SynchronizationController::insertUpdate($user->id, Carbon::now()->toDateTimeString());
        }

        // $addressesArray = $request->input('address');
        // if(!$binded) {
        //     $addressesArray['contact_number'] = $contactNumber;
        // }
        // $user->addresses()->updateOrCreate(['id' => $request->input('address.id')], $addressesArray);

        $data = [
            'title' => 'Edit Client'
        ];


          if($request->get('visa_type')=='9G' || $request->get('visa_type')=='TRV'){
            User::where('id', $user->id)
            ->update(['visa_type' => $request->get('visa_type'),'icard'=>$request->get('icard'),'exp_date'=>(($request->get('service_exp_date')!='') ? Carbon::parse($request->get('service_exp_date'))->format('Y-m-d') : NULL),'issue_date'=>(($request->get('issue_date')!='') ? Carbon::parse($request->get('issue_date'))->format('Y-m-d') : NULL),'icard_exp_date'=>(($request->get('icard_exp_date')!='') ? Carbon::parse($request->get('icard_exp_date'))->format('Y-m-d') : NULL),'arrival_date'=> NULL,'first_exp_date'=>NULL]);
          }else if($request->get('visa_type')=='CWV'){
            User::where('id', $user->id)
            ->update(['visa_type' => $request->get('visa_type'),'exp_date'=>(($request->get('service_exp_date')!='') ? Carbon::parse($request->get('service_exp_date'))->format('Y-m-d') : NULL),'arrival_date'=> NULL,'first_exp_date'=>NULL,'issue_date'=>NULL,'icard_exp_date'=>NULL]);
          }else{
            User::where('id', $user->id)
            ->update(['visa_type' => $request->get('visa_type'),'exp_date'=>(($request->get('service_exp_date')!='') ? Carbon::parse($request->get('service_exp_date'))->format('Y-m-d') : NULL),'arrival_date'=>(($request->get('arrival_date')!='') ? Carbon::parse($request->get('arrival_date'))->format('Y-m-d') : NULL),'first_exp_date'=>(($request->get('first_exp_date')!='') ? Carbon::parse($request->get('first_exp_date'))->format('Y-m-d') : NULL),'issue_date'=>NULL,'icard_exp_date'=>NULL]);
          }

        $client =  User::with('roles','permissions')->whereId($id)->get();

        session()->flash('data', array('data'=>$client[0]));
        session()->flash('message', 'Client has been updated successfully.');
        return redirect()->route('cpanel.visa.client_profile', ['id' => $id]);
    }

    public function updatewithemail(Clients2Validation $request, $id)
    {
        $user = User::find($id);
        $old_contact_number = $user->contact_number;
        $contact_number = $request->get('contact_number');
        $user->fill($request->all());
        $user->group_id = $request->get('group_id');
        if(!empty($contact_number)){
            preg_match_all('!\d+!', $contact_number, $matches);
            $contact_number = implode("", $matches[0]);
            $contact_number = ltrim($contact_number,"0");
            $contact_number = ltrim($contact_number,'+');
            $contact_number = ltrim($contact_number,'63');
            if("0".$contact_number != $old_contact_number){
                $user->password = bcrypt("0".$contact_number);
            }
        }
        $user->save();
         //\Log::info($request->all());
        // \Log::info($request->get('address')['contact_number']);
        $binded = true;
        if($request->group_id != 0) {
            $groupLeaderId = Group::findOrFail($request->group_id)->leader;
            $groupLeader = User::findOrfail($groupLeaderId);

            if( (is_null($groupLeader->password) || $groupLeader->password == '')
                &&
                $groupLeader->last_name == 'N/A'
                &&
                empty($request->contact_number)
            ) {
                $binded = false;
                $contactNumber = $groupLeader->contact_number;
            }
            else if($user->contact_number == ($request->contact_number)) {
                $binded = false;
                $contactNumber = $request->contact_number;
            }
        }else{
            if(($request->get('contact_number')==NULL || $request->get('contact_number')=='') && ($request->get('alternate_contact_number')!=NULL || $request->get('alternate_contact_number')!='')){
                $user->contact_number = $request->get('alternate_contact_number');
                $user->alternate_contact_number = NULL;
            }
        }


        // if($user->isDirty()){
        //     //getOriginal() -> get original values of model
        //     //getDirty -> get all fields updated with value
        //     $changes = $user->getDirty();
        //     $detail = "Updated client Account.";
        //     foreach ($changes as $key => $value) {
        //         $old = $user->getOriginal($key);
        //         $act_id = $user->id;
        //         $detail .= "Change ".$key." from ".$old." to ".$value.". ";
        //     }
        //     Common::saveActionLogs(0,$act_id,$detail);
        //     $user->save();
        //     //SynchronizationController::insertUpdate($user->id, Carbon::now()->toDateTimeString());
        //
        // }

        // $addressesArray = $request->input('address');
        // if(!$binded) {
        //     $addressesArray['contact_number'] = $contactNumber;
        // }
        // $user->addresses()->updateOrCreate(['id' => $request->input('address.id')], $addressesArray);

        $data = [
            'title' => 'Edit Client'
        ];
        //update visa type

          if($request->get('visa_type')=='9G'  || $request->get('visa_type')=='TRV'){
            User::where('id', $user->id)
            ->update(['visa_type' => $request->get('visa_type'),'icard'=>$request->get('icard'),'exp_date'=>(($request->get('service_exp_date')!='') ? Carbon::parse($request->get('service_exp_date'))->format('Y-m-d') : NULL),'issue_date'=>(($request->get('issue_date')!='') ? Carbon::parse($request->get('issue_date'))->format('Y-m-d') : NULL),'icard_exp_date'=>(($request->get('icard_exp_date')!='') ? Carbon::parse($request->get('icard_exp_date'))->format('Y-m-d') : NULL),'arrival_date'=> NULL,'first_exp_date'=>NULL]);
          }else if($request->get('visa_type')=='CWV'){
            User::where('id', $user->id)
            ->update(['visa_type' => $request->get('visa_type'),'exp_date'=>(($request->get('service_exp_date')!='') ? Carbon::parse($request->get('service_exp_date'))->format('Y-m-d') : NULL),'arrival_date'=> NULL,'first_exp_date'=>NULL,'issue_date'=>NULL,'icard_exp_date'=>NULL]);
          }else{
            User::where('id', $user->id)
            ->update(['visa_type' => $request->get('visa_type'),'exp_date'=>(($request->get('service_exp_date')!='') ? Carbon::parse($request->get('service_exp_date'))->format('Y-m-d') : NULL),'arrival_date'=>(($request->get('arrival_date')!='') ? Carbon::parse($request->get('arrival_date'))->format('Y-m-d') : NULL),'first_exp_date'=>(($request->get('first_exp_date')!='') ? Carbon::parse($request->get('first_exp_date'))->format('Y-m-d') : NULL),'issue_date'=>NULL,'icard_exp_date'=>NULL]);
          }

        $client =  User::with('roles','permissions')->whereId($id)->get();
        session()->flash('data', array('data'=>$client[0]));
        session()->flash('message', 'Client has been updated successfully.');

        return redirect()->route('cpanel.visa.client_profile', ['id' => $id]);
    }

    public function updateAvatar(Request $request, $id)
    {
        $user = User::find($id);

        $width = $request->get('width');
        $height = $request->get('height');
        $image = $request->get('image');

        $img = Image::make($image)->resize($width, null, function ($constraint) {
          $constraint->aspectRatio();
        })
        ->resizeCanvas($width, $height, 'center')
        ->encode('data-url');

        $img2 = clone $img;

        Image::make($img)
          ->encode('jpg', 100)
          ->save(public_path().'/images/avatar/'.$user->id.'.jpg');

        $user->images()->create([
          'name' => $user->id.'.jpg',
          'type' => 'avatar',
        ]);

        return $img2;
    }

    public function uploadImage(Request $request, $id)
    {
        $user = User::find($id);

        $width = $request->get('width');
        $height = $request->get('height');
        $image = $request->get('image');
        $type = $request->get('type');
        $img = Image::make($image)->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        })
        ->encode('data-url');

        $img2 = clone $img;

        if (!file_exists(public_path().'/images/documents/'.$user->id)) {
            mkdir(public_path().'/images/documents/'.$user->id, 0777, true);
        }
        $unique_value = str_random('5');

        Image::make($img)
          ->encode('jpg', 100)
          ->save(public_path().'/images/documents/'.$user->id.'/'.$type.$unique_value.'.jpg');


        if($type=='9gICard'){
            $docs9gICard = $user->documents()->where('type','=','9gICard')->get();

            if(count($docs9gICard) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }

        if($type=='9gOrder'){
            $docs9gOrder = $user->documents()->where('type','=','9gOrder')->get();

            if(count($docs9gOrder) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }

        if($type=='prvCard'){
            $docsPrvCard = $user->documents()->where('type','=','prvCard')->get();

            if(count($docsPrvCard) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }

        if($type=='prvOrder'){
            $docsPrvCard = $user->documents()->where('type','=','prvOrder')->get();

            if(count($docsPrvCard) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }


        if($type=='srrvCard'){
            $docsPrvCard = $user->documents()->where('type','=','srrvCard')->get();

            if(count($docsPrvCard) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }

        if($type=='srrvVisa'){
            $docsPrvCard = $user->documents()->where('type','=','srrvVisa')->get();

            if(count($docsPrvCard) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }

        if($type=='marriageCert'){
            $docsPrvCard = $user->documents()->where('type','=','marriageCert')->get();

            if(count($docsPrvCard) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }

        if($type=='aep'){
            $docsNbiId = $user->documents()->where('type','=','aep')->get();

            if(count($docsNbiId) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }

        if($type=='nbi'){
            $docsNbiId = $user->documents()->where('type','=','nbi')->get();

            if(count($docsNbiId) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }

        if($type=='govId'){
            $docsNbiId = $user->documents()->where('type','=','govId')->get();

            if(count($docsNbiId) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }

        if($type=='birthCert'){
            $docsNbiId = $user->documents()->where('type','=','birthCert')->get();

            if(count($docsNbiId) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }

        if($type=='sec'){
            $docsNbiId = $user->documents()->where('type','=','sec')->get();

            if(count($docsNbiId) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }

        if($type=='bir'){
            $docsNbiId = $user->documents()->where('type','=','bir')->get();

            if(count($docsNbiId) == 0){

              $document = $user->documents()->create([
                'user_id' => Auth::user()->id,
                'type' => $type,
                'name' => $type.$unique_value.'.jpg',
                'status' => '',
                'expires_at' => null
              ]);

            } else {
                $document = $user->documents()->where('type','=',$type)->update([
                    'user_id' => Auth::user()->id,
                    'name' => $type.$unique_value.'.jpg',
                    'expires_at' => null
                ]);
            }
        }


      return $img2;
    }

    public function verify(CpanelDocumentsValidation $request)
    {
        //$authUser = Auth::user();
        $data = Document::findOrFail($request->get('id'));
        $data->fill([
            'user_id' => Auth::user()->id,
            'expires_at' => $request->get('expires_at'),
        ]);

        if($data->isDirty()) {
            //getOriginal() -> get original values of model
            //getDirty -> get all fields updated with value
            $changes = $data->getDirty();
            $detail = 'Updated Document -> ';
            foreach ($changes as $key => $value) {
                $old = (is_null($data->getOriginal($key))) ? 'NULL' : $data->getOriginal($key);

                if($key == 'user_id') {
                    $key = 'user';

                    if($old != 'NULL') {
                        $user = User::find($old)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
                        $old = $user->first_name . ' ' . $user->last_name;
                    }

                    $user = User::find($value)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
                    $value = $user->first_name . ' ' . $user->last_name;
                }

                $detail .= "Change ".$key." from ".$old." to ".$value.". ";
            }
            Common::saveActionLogs(0, $data->documentable_id, $detail);
        }

        $data->save();

        return $data;
    }

    public function showGroup($branch_id = null){
        $groups = Group::orderBy('name', 'asc')->get();
        if($branch_id > 0){
            $groups = Group::where('branch_id',$branch_id)->orderBy('name', 'asc')->get();
        }
        return $groups;
    }

    public function showBranch(){
        $br = Branch::orderBy('id', 'asc')->get();
        return $br;
    }

    public function client_profile($id) {
        $data = [
            'title' => 'Client Profile'
        ];
        $selectedUser = User::where('id',$id)->first();
        //$visa = User::select('users.*')->where('users.id',$id)->first();
       

        //session()->flash('visa', $visa);
    
         //dd($selectedUser);
        // return view('cpanel/visa/clients/profile', compact('selectedUser', 'assignedRoles', 'assignedPermissions', 'roles', 'permissions', 'groups'));

        if($selectedUser != null){
            $assignedRoles = $selectedUser->roles;
            $assignedPermissions = $selectedUser->permissions;
            $selectedUser['total_client_com'] = ClientService::select(DB::raw("SUM(com_client) as total"))->where('client_com_id',$id)->where('status','complete')->first()->total;
            $selectedUser['total_agent_com'] = ClientService::select(DB::raw("SUM(com_agent) as total"))->where('agent_com_id',$id)->where('status','complete')->first()->total;
            $roles = Role::all();
            $permissions = Permission::all();
            $groups = GroupMember::where('client_id', $id)
                    ->where('group_id', '>', 0)
                    ->with('group_detail', 'group_detail.user')
                    ->get();
            //\Log::info($selectedUser);
            session()->flash('data', array('data'=>$selectedUser));
            return view('cpanel/visa/clients/profile', compact('selectedUser', 'assignedRoles', 'assignedPermissions', 'roles', 'permissions', 'groups'));
        }else{
            abort(404);
        }
    }

    public function clientDeposit($client_id){
        return $this->computeTransactions($client_id, 'Deposit');
        // return Deposit::where('client_id',$client_id)
        //             ->where('group_id',0)
        //             ->sum('deposit_amount');
    }

    public function clientDiscount($client_id){
        return $this->computeTransactions($client_id, 'Discount');
        // return Discount::where('client_id',$client_id)
        //             ->where('group_id',0)
        //             ->sum('discount_amount');
    }

    public function clientPayment($client_id){
        return $this->computeTransactions($client_id, 'Payment');
        // return Payment::where('client_id',$client_id)
        //             ->where('group_id',0)
        //             ->sum('payment_amount');
    }

    public function clientRefund($client_id){
        return $this->computeTransactions($client_id, 'Refund');
        // return Refund::where('client_id',$client_id)
        //             ->where('group_id',0)
        //             ->sum('refund_amount');
    }

    public function computeTransactions($client_id = 0, $type, $group_id = 0){
        if($client_id != 0){
            return ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',$group_id)->where('type',$type)
                    ->sum('amount');
        }
        else{
            return ClientTransactions::where('group_id',$group_id)->where('type',$type)
                    ->sum('amount');
        }
    }

    public function clientTransfer($client_id){

        $minus = TransferBalance::where('transfer_from',$client_id)
                    ->sum('amount');
        $minus *= -1;
        $add = TransferBalance::where('transfer_to',$client_id)
                    ->sum('amount');

        return $minus + $add;
    }

    public function clientPointsEarned($client_id){

        $getAgent = User::where('middleman',$client_id)->select('id')->get()->makeHidden(['permissions','document_receive']);
        $ptsAsAgent = 0;
        $ptsAsMid = 0;
        foreach($getAgent as $agent){
            $referredClientIds = UserOpen::where('referral_id', $agent->id)->where('is_verified', 1)->pluck('client_id');

            $ptsAsMid += ClientService::whereIn('client_id', $referredClientIds)
                ->where('status', 'Complete')
                ->where('active', 1)
                ->sum('com_middleman');
            }

        $referredClientIds = UserOpen::where('referral_id', $client_id)->where('is_verified', 1)->pluck('client_id');

            $ptsAsAgent += ClientService::whereIn('client_id', $referredClientIds)
                ->where('status', 'Complete')
                ->where('active', 1)
                ->sum('com_agent');
            

        $ptsAsReferral = ClientService::where('client_id',$client_id)->where('active',1)->where('status','complete')
                    ->sum('com_client');

        return $ptsAsReferral + $ptsAsAgent + $ptsAsMid;
    }

    public function changeStatus($client_id,$stat){

        $usr = User::where('id',$client_id)->first();
        if($usr){
            $usr->risk = $stat;
            $usr->save();
            //SynchronizationController::insertUpdate($usr->id, Carbon::now()->toDateTimeString());
        }

        return $usr;
    }

    public function clientServiceCost($client_id){
        $cost = ClientService::where('client_id',$client_id)
            ->where('group_id', 0)
            ->where('active', 1)
            ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
        if($cost){
            return $cost;
        }
        return 0;
    }

    public function GetCompleteCost($client_id){
        $cost = ClientService::select(DB::raw('sum(cost+charge+tip+ com_client + com_agent) as total_cost'))
            ->where('client_id', '=', $client_id)
            ->where('group_id', 0)
            ->where('active', 1)
            ->where('status', '=', 'complete')
            ->first();
        if($cost->total_cost){
            return $cost->total_cost;
        }
        return 0;
    }

    public function clientBalance($client_id){
        $balance = ((
                        Common::toInt($this->clientDeposit($client_id))
                        + Common::toInt($this->clientPayment($client_id))
                        + Common::toInt($this->clientDiscount($client_id))
                    )-(
                        Common::toInt($this->clientRefund($client_id))
                        + Common::toInt($this->clientServiceCost($client_id))
                    ));

        return $balance;
    }
    public function clientCommission($client_id){
        $selectedUser['total_client_com'] = ClientService::select(DB::raw("SUM(com_client) as total"))->where('client_id',$client_id)->where('status','complete')->first()->total;
        $selectedUser['total_agent_com'] = ClientService::select(DB::raw("SUM(com_agent) as total"))->where('client_id',$client_id)->where('status','complete')->first()->total;
        
        return array('total_client_com' => $selectedUser['total_client_com'] , 'total_agent_com' => $selectedUser['total_agent_com']);
    }

    public function clientCompleteBalance($client_id){
        $balance = ((
                        Common::toInt($this->clientDeposit($client_id))
                        + Common::toInt($this->clientPayment($client_id))
                        + Common::toInt($this->clientDiscount($client_id))
                    )-(
                        Common::toInt($this->clientRefund($client_id))
                        + Common::toInt($this->GetCompleteCost($client_id))
                    ));
        // if($balance > 0){
        //     return 0;
        // }
        return $balance;
    }

    public function groupDeposit($group_id){
        return $this->computeTransactions(0, 'Deposit', $group_id);
        // return Deposit::where('group_id', $group_id)
        //             ->sum('deposit_amount');
    }

    public function groupDiscount($group_id){
        return $this->computeTransactions(0, 'Discount', $group_id);
        // return Discount::where('group_id', $group_id)
        //             ->sum('discount_amount');
    }

    public function groupPayment($group_id){
        return $this->computeTransactions(0, 'Payment', $group_id);
        // return Payment::where('group_id', $group_id)
        //             ->sum('payment_amount');
    }

    public function groupRefund($group_id){
        return $this->computeTransactions(0, 'Refund', $group_id);
        // return Refund::where('group_id', $group_id)
        //             ->sum('refund_amount');
    }

    public function groupServiceCost($group_id){
        $cost = ClientService::where('group_id', $group_id)
            ->where('active', 1)
            ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));

        return $cost;
    }

    public function groupBalance($group_id){
        $balance = ((
                        $this->groupDeposit($group_id)
                        + $this->groupPayment($group_id)
                        +$this->groupDiscount($group_id)
                    )-(
                        $this->groupRefund($group_id)
                        + $this->groupServiceCost($group_id)
                    ));

        return $balance;
    }

    public function package_deposit($tracking_id){
        $package_deposit = ClientTransactions::where('tracking', $tracking_id)
            ->where('group_id', 0)->where('type','Deposit')
            ->sum('amount');

        return $package_deposit;
    }

    public function package_payment($tracking_id){
        $package_payment = ClientTransactions::
        where('tracking', $tracking_id)
            ->where('group_id', 0)->where('type','Payment')
            ->sum('amount');

        return $package_payment;
    }
    public function package_discount($tracking_id) {
        $package_discount = ClientTransactions::
        where('tracking', $tracking_id)
            ->where('group_id', 0)->where('type','Discount')
            ->sum('amount');

        return $package_discount;
    }
    public function package_refund($tracking_id){
        $package_refund = ClientTransactions::
        where('tracking', $tracking_id)->where('type','Refund')
            ->sum('amount');

        return $package_refund;
    }

    public function clientPackages($client_id,$tracking = null){
        if ($tracking =='0') :
            $packs = Package::where('client_id', $client_id)
                ->orderBy('id', 'desc')
                ->get();
        else :
            $packs = Package::where('client_id', $client_id)
                ->where('tracking', 'like', $tracking.'%')
                ->orderBy('id', 'desc')
                ->get();
        endif;
        return $packs;
    }

    public function clientServices($client_id,$tracking = null){
        $checktrack = ($tracking == 0 ? false : true);

        if ($checktrack) :
            $services = DB::connection('visa')
                ->table('client_services as a')
                ->select(DB::raw('a.*, b.name, c.reason,c.amount,c.type, c.id as disc_id, d.parent_id as parent_id, d.months_required as months_required, d.docs_needed as docs_needed, d.docs_optional as docs_optional, a.rcv_docs as rcv, d.docs_released as docs_released, d.docs_released_optional as docs_released_optional, e.documents as _9a_docs_released_optional, f.documents as _9g_docs_released_optional, g.documents as _blacklist_docs_released_optional, h.documents as _downgrading_1_docs_released_optional, i.documents as _downgrading_2_docs_released_optional, j.documents as _srrv_1_docs_released_optional, k.documents as _srrv_2_docs_released_optional'))
                ->leftjoin(DB::raw('(select * from groups) as b'),'b.id','=','a.group_id')
                // ->leftjoin(DB::raw('(select * from client_transactions) as c'),'c.service_id','=','a.id')
                ->leftJoin('client_transactions AS c', function($join){
                            $join->on('c.service_id', '=', 'a.id')
                            ->where('c.type', '=', 'Discount');
                    })
                ->leftjoin(DB::raw('(select * from services) as d'), 'd.id', '=', 'a.service_id')
                // 9A
                ->leftJoin('documents_logs AS e', function($join){
                    $join->on('e.client_service_id', '=', 'a.id')
                        ->where('e.type', '=', 'Received')
                        ->whereRaw("find_in_set('7', e.documents)");
                })
                // 9G
                ->leftJoin('documents_logs AS f', function($join){
                    $join->on('f.client_service_id', '=', 'a.id')
                        ->where('f.type', '=', 'Received')
                        ->whereRaw("find_in_set('10', f.documents)");
                })
                // Blacklist
                ->leftJoin('documents_logs AS g', function($join){
                    $join->on('g.client_service_id', '=', 'a.id')
                        ->where('a.service_id', 334)
                        ->where('g.type', '=', 'Received')
                        ->whereRaw("find_in_set('199', g.documents)");
                })
                // Downgrading 1
                ->leftJoin('documents_logs AS h', function($join){
                    $join->on('h.client_service_id', '=', 'a.id')
                        ->where(function($query) {
                            $query->where('a.service_id', 121)
                                ->orWhere('a.service_id', 394)
                                ->orWhere('a.service_id', 423);
                        })
                        ->where('h.type', '=', 'Received')
                        ->whereRaw("find_in_set('131', h.documents)");
                })
                // Downgrading 2
                ->leftJoin('documents_logs AS i', function($join){
                    $join->on('i.client_service_id', '=', 'a.id')
                        ->where(function($query) {
                            $query->where('a.service_id', 307)
                                ->orWhere('a.service_id', 415)
                                ->orWhere('a.service_id', 416);
                        })
                        ->where('i.type', '=', 'Received')
                        ->whereRaw("find_in_set('75', i.documents)");
                })
                // Special Retiree Resident Visa (SRRV) Conversion 1
                ->leftJoin('documents_logs AS j', function($join){
                    $join->on('j.client_service_id', '=', 'a.id')
                        ->where('a.service_id', 386)
                        ->where('j.type', '=', 'Received')
                        ->whereRaw("find_in_set('201', j.documents)");
                })
                // Special Retiree Resident Visa (SRRV) Conversion 2
                ->leftJoin('documents_logs AS k', function($join){
                    $join->on('k.client_service_id', '=', 'a.id')
                        ->where('a.service_id', 386)
                        ->where('k.type', '=', 'Received')
                        ->whereRaw("find_in_set('200', k.documents)");
                })
                ->where('a.tracking', $tracking)
                // ->where('c.type', 'Discount')
                ->orderBy('a.id', 'desc')
                ->get();

            $servicestotal = DB::connection('visa')
                ->table('client_services')
                ->select(DB::raw('
                    sum(IFNULL(cost,0)+IFNULL(charge,0)+IFNULL(tip,0)+IFNULL(com_client,0)+IFNULL(com_agent,0)) as total,
                    service_date
                '))
                ->where('tracking', $tracking)
                ->where('active', 1)
                ->orderBy('id', 'desc')
                ->first();

            //get cost per individual package
            $CostPerPackage = DB::connection('visa')
                ->table('client_services')
                ->select(DB::raw('sum(IFNULL(cost,0)+IFNULL(charge,0)+IFNULL(tip,0)+IFNULL(com_client,0)+IFNULL(com_agent,0)) as total, tracking'))
                ->where('client_id', $client_id)
                ->where('group_id', 0)
                ->where('active', 1)
                ->groupBy('tracking')
                ->orderBy('id', 'asc')
                ->get();

            //computation of balance per individual package
            $personal_payment = $this->clientPayment($client_id);
            $personal_cost = $this->GetCompleteCost($client_id);
            $personal_discount = $this->clientDiscount($client_id);
            $personal_refund = $this->clientRefund($client_id);
            $personal_deposit = $this->clientDeposit($client_id);
            $clientMoney = ($personal_payment + $personal_deposit + $personal_discount)-$personal_refund;
            $bal = 0;
            foreach($CostPerPackage as $c){
              if($clientMoney<0){
                  $clientMoney = 0;
              }
              $clientMoney -= $c->total;
              if($c->tracking == $tracking){
                  if($clientMoney>0){
                    $bal = 0;
                  }
                  else{
                    $bal = $clientMoney;
                  }
              }
            }
            //\Log::info($bal);
            //end

            // //get client group id
            // $groupID = DB::connection('visa')
            //     ->table('group_members')
            //     ->select(DB::raw('group_id'))
            //     ->where('client_id', $client_id)
            //     ->first();

            // //get group money details
            // if($groupID!=null){
            // $Gpayment = $this->groupPayment($groupID->group_id);
            // $Gcost = $this->groupServiceCost($groupID->group_id);
            // $Gdiscount = $this->groupDiscount($groupID->group_id);
            // $Grefund = $this->groupRefund($groupID->group_id);
            // $Gdeposit = $this->groupDeposit($groupID->group_id);
            // $gMoney = ($Gpayment + $Gdeposit + $Gdiscount)-$Grefund;
            // //$GpackageCost = $this->GetGroupCost($groupID->group_id);

            // $CostGroupPackage = DB::connection('visa')
            //     ->table('client_services')
            //     ->select(DB::raw('sum(IFNULL(cost,0)+IFNULL(charge,0)+IFNULL(tip,0)) as total, tracking'))
            //     ->where('group_id', $groupID->group_id)
            //     ->groupBy('tracking')
            //     ->orderBy('id', 'asc')
            //     ->get();

            //     foreach($CostGroupPackage as $g){
            //       if($gMoney<0){
            //           $gMoney = 0;
            //       }
            //       $gMoney -= $g->total;
            //       if($g->tracking == $tracking){
            //           if($gMoney>0){
            //             $bal = 0;
            //           }
            //           else{
            //             $bal = $gMoney;
            //           }
            //       }
            //     }
            // }//end if

            $packages = Package::where('tracking', $tracking)->first();

            $services_total = $servicestotal->total;

            $package_deposit = $this->package_deposit($tracking);
            $package_payment = $this->package_payment($tracking);
            $package_refund = $this->package_refund($tracking);
            $package_discount = $this->package_discount($tracking);

            $initialdeposit = (
                (
                    Common::toInt($package_deposit)
                    + Common::toInt($package_payment)
                    + Common::toInt($package_discount)
                )
            );

            $packagebalance = (
                (Common::toInt(Common::formatMoney($package_deposit))
                  + Common::toInt(Common::formatMoney($package_payment))
                  + Common::toInt(Common::formatMoney($package_discount)))
                  -( $services_total)
              );

              if($packagebalance>0){
                 $packagebalance -= Common::toInt(Common::formatMoney($package_refund));
              }
              else{
                 $packagebalance += Common::toInt(Common::formatMoney($package_refund));
              }

              $current = strtotime('2017-01-01');
              $cpackage_date = strtotime(date('Y-m-d', strtotime($packages->log_date)));
              if($current < $cpackage_date){
                  $bal = $packagebalance;
              }

            $data = array(
                'services' => $services,
                'status' => $packages->status,
                'package_cost' => $services_total,
                'package_date' => $packages->log_date,
                'package_deposit' => $package_deposit,
                'package_payment' => $package_payment,
                'package_refund' => $package_refund,
                'package_discount' => $package_discount,
                'package_balance' => $bal
            );

        else :
            if(substr($tracking,0,1)=="G"):
                $services = DB::connection('visa')
                    ->table('client_services as a')
                    ->select(DB::raw('a.*, b.name, c.reason,c.amount,c.type, c.id as disc_id, d.parent_id as parent_id, d.months_required as months_required, d.docs_needed as docs_needed, d.docs_optional as docs_optional, a.rcv_docs as rcv, d.docs_released as docs_released, d.docs_released_optional as docs_released_optional, e.documents as _9a_docs_released_optional, f.documents as _9g_docs_released_optional, g.documents as _blacklist_docs_released_optional, h.documents as _downgrading_1_docs_released_optional, i.documents as _downgrading_2_docs_released_optional, j.documents as _srrv_1_docs_released_optional, k.documents as _srrv_2_docs_released_optional'))
                    ->leftjoin(DB::raw('(select * from groups) as b'),'b.id','=','a.group_id')
                    ->leftJoin('client_transactions AS c', function($join){
                            $join->on('c.service_id', '=', 'a.id')
                            ->where('c.type', '=', 'Discount');
                    })
                    // ->leftjoin(DB::raw('(select * from client_transactions) as c'),'c.service_id','=','a.id')
                    ->leftjoin(DB::raw('(select * from services) as d'), 'd.id', '=', 'a.service_id')
                    // 9A
                    ->leftJoin('documents_logs AS e', function($join){
                        $join->on('e.client_service_id', '=', 'a.id')
                            ->where('e.type', '=', 'Received')
                            ->whereRaw("find_in_set('7', e.documents)");
                    })
                    // 9G
                    ->leftJoin('documents_logs AS f', function($join){
                        $join->on('f.client_service_id', '=', 'a.id')
                            ->where('f.type', '=', 'Received')
                            ->whereRaw("find_in_set('10', f.documents)");
                    })
                    // Blacklist
                    ->leftJoin('documents_logs AS g', function($join){
                        $join->on('g.client_service_id', '=', 'a.id')
                            ->where('a.service_id', 334)
                            ->where('g.type', '=', 'Received')
                            ->whereRaw("find_in_set('199', g.documents)");
                    })
                    // Downgrading 1
                    ->leftJoin('documents_logs AS h', function($join){
                        $join->on('h.client_service_id', '=', 'a.id')
                            ->where(function($query) {
                                $query->where('a.service_id', 121)
                                    ->orWhere('a.service_id', 394)
                                    ->orWhere('a.service_id', 423);
                            })
                            ->where('h.type', '=', 'Received')
                            ->whereRaw("find_in_set('131', h.documents)");
                    })
                    // Downgrading 2
                    ->leftJoin('documents_logs AS i', function($join){
                        $join->on('i.client_service_id', '=', 'a.id')
                            ->where(function($query) {
                                $query->where('a.service_id', 307)
                                    ->orWhere('a.service_id', 415)
                                    ->orWhere('a.service_id', 416);
                            })
                            ->where('i.type', '=', 'Received')
                            ->whereRaw("find_in_set('75', i.documents)");
                    })
                    // Special Retiree Resident Visa (SRRV) Conversion 1
                    ->leftJoin('documents_logs AS j', function($join){
                        $join->on('j.client_service_id', '=', 'a.id')
                            ->where('a.service_id', 386)
                            ->where('j.type', '=', 'Received')
                            ->whereRaw("find_in_set('201', j.documents)");
                    })
                    // Special Retiree Resident Visa (SRRV) Conversion 2
                    ->leftJoin('documents_logs AS k', function($join){
                        $join->on('k.client_service_id', '=', 'a.id')
                            ->where('a.service_id', 386)
                            ->where('k.type', '=', 'Received')
                            ->whereRaw("find_in_set('200', k.documents)");
                    })
                    ->where('a.tracking', $tracking)
                    // ->where('c.type', 'Discount')
                    ->orderBy('a.id', 'desc')
                    ->get();

                $servicestotal = DB::connection('visa')
                    ->table('client_services')
                    ->select(DB::raw('
                        sum(IFNULL(cost,0)+IFNULL(charge,0)+IFNULL(tip,0)+IFNULL(com_client,0)+IFNULL(com_agent,0)) as total,
                        service_date
                    '))
                    ->where('tracking', $tracking)
                    ->where('active', 1)
                    ->orderBy('id', 'desc')
                    ->first();

            //get client group id
            $packages = Package::where('tracking', $tracking)->first();
            $groupID = $packages->group_id;
            //get group money details
            if($packages!=null){
            $Gpayment = $this->groupPayment($groupID);
            $Gcost = $this->groupServiceCost($groupID);
            $Gdiscount = $this->groupDiscount($groupID);
            $Grefund = $this->groupRefund($groupID);
            $Gdeposit = $this->groupDeposit($groupID);
            $gMoney = ($Gpayment + $Gdeposit + $Gdiscount)-$Grefund;
            //$GpackageCost = $this->GetGroupCost($groupID->group_id);

            $CostGroupPackage = DB::connection('visa')
                ->table('client_services')
                ->select(DB::raw('sum(IFNULL(cost,0)+IFNULL(charge,0)+IFNULL(tip,0)+IFNULL(com_client,0)+IFNULL(com_agent,0)) as total, tracking'))
                ->where('group_id', $groupID)
                ->groupBy('tracking')
                ->orderBy('id', 'asc')
                ->get();

                foreach($CostGroupPackage as $g){
                  if($gMoney<0){
                      $gMoney = 0;
                  }
                  $gMoney -= $g->total;
                  if($g->tracking == $tracking){
                      if($gMoney>0){
                        $bal = 0;
                      }
                      else{
                        $bal = $gMoney;
                      }
                  }
                }
            }//end if

            $services_total = $servicestotal->total;

            $package_deposit = $this->package_deposit($tracking);
            $package_payment = $this->package_payment($tracking);
            $package_refund = $this->package_refund($tracking);
            $package_discount = $this->package_discount($tracking);

            $initialdeposit = (
                (
                    Common::formatMoney($package_deposit)
                    + Common::formatMoney($package_payment)
                    + Common::formatMoney($package_discount)
                )
            );

            $packagebalance = (
                (Common::formatMoney($package_deposit)
                  + Common::formatMoney($package_payment)
                  + Common::formatMoney($package_discount))
                  -( $services_total)
              );

              if($packagebalance>0){
                 $packagebalance -= Common::formatMoney($package_refund);
              }
              else{
                 $packagebalance += Common::formatMoney($package_refund);
              }

              $current = strtotime('2017-01-01');
              $cpackage_date = strtotime(date('Y-m-d', strtotime($packages->log_date)));
              if($current < $cpackage_date){
                  $bal = $packagebalance;
              }

            $data = array(
                'services' => $services,
                'status' => $packages->status,
                'package_cost' => $services_total,
                'package_date' => $packages->log_date,
                'package_deposit' => $package_deposit,
                'package_payment' => $package_payment,
                'package_refund' => $package_refund,
                'package_discount' => $package_discount,
                'package_balance' => $bal
            );

            else :

                $services = DB::connection('visa')
                    ->table('client_services as a')
                    ->select(DB::raw('a.*, b.name, c.reason,c.amount,c.type, c.id as disc_id, d.parent_id as parent_id, d.months_required as months_required, d.docs_needed as docs_needed, d.docs_optional as docs_optional, a.rcv_docs as rcv, d.docs_released as docs_released, d.docs_released_optional as docs_released_optional, e.documents as _9a_docs_released_optional, f.documents as _9g_docs_released_optional, g.documents as _blacklist_docs_released_optional, h.documents as _downgrading_1_docs_released_optional, i.documents as _downgrading_2_docs_released_optional, j.documents as _srrv_1_docs_released_optional, k.documents as _srrv_2_docs_released_optional'))
                    ->leftjoin(DB::raw('(select * from groups) as b'),'b.id','=','a.group_id')
                    ->leftJoin('client_transactions AS c', function($join){
                            $join->on('c.service_id', '=', 'a.id')
                            ->where('c.type', '=', 'Discount');
                    })
                    // ->leftjoin(DB::raw('(select * from client_transactions) as c'),'c.service_id','=','a.id')
                    ->leftjoin(DB::raw('(select * from services) as d'), 'd.id', '=', 'a.service_id')
                    // 9A
                    ->leftJoin('documents_logs AS e', function($join){
                        $join->on('e.client_service_id', '=', 'a.id')
                            ->where('e.type', '=', 'Received')
                            ->whereRaw("find_in_set('7', e.documents)");
                    })
                    // 9G
                    ->leftJoin('documents_logs AS f', function($join){
                        $join->on('f.client_service_id', '=', 'a.id')
                            ->where('f.type', '=', 'Received')
                            ->whereRaw("find_in_set('10', f.documents)");
                    })
                    // Blacklist
                    ->leftJoin('documents_logs AS g', function($join){
                        $join->on('g.client_service_id', '=', 'a.id')
                            ->where('a.service_id', 334)
                            ->where('g.type', '=', 'Received')
                            ->whereRaw("find_in_set('199', g.documents)");
                    })
                    // Downgrading 1
                    ->leftJoin('documents_logs AS h', function($join){
                        $join->on('h.client_service_id', '=', 'a.id')
                            ->where(function($query) {
                                $query->where('a.service_id', 121)
                                    ->orWhere('a.service_id', 394)
                                    ->orWhere('a.service_id', 423);
                            })
                            ->where('h.type', '=', 'Received')
                            ->whereRaw("find_in_set('131', h.documents)");
                    })
                    // Downgrading 2
                    ->leftJoin('documents_logs AS i', function($join){
                        $join->on('i.client_service_id', '=', 'a.id')
                            ->where(function($query) {
                                $query->where('a.service_id', 307)
                                    ->orWhere('a.service_id', 415)
                                    ->orWhere('a.service_id', 416);
                            })
                            ->where('i.type', '=', 'Received')
                            ->whereRaw("find_in_set('75', i.documents)");
                    })
                    // Special Retiree Resident Visa (SRRV) Conversion 1
                    ->leftJoin('documents_logs AS j', function($join){
                        $join->on('j.client_service_id', '=', 'a.id')
                            ->where('a.service_id', 386)
                            ->where('j.type', '=', 'Received')
                            ->whereRaw("find_in_set('201', j.documents)");
                    })
                    // Special Retiree Resident Visa (SRRV) Conversion 2
                    ->leftJoin('documents_logs AS k', function($join){
                        $join->on('k.client_service_id', '=', 'a.id')
                            ->where('a.service_id', 386)
                            ->where('k.type', '=', 'Received')
                            ->whereRaw("find_in_set('200', k.documents)");
                    })
                    ->where('a.client_id', $client_id)
                    // ->where('c.type', 'Discount')
                    ->orderBy('a.id', 'desc')
                    ->get();

                $data = array(
                    'services' => $services,
                    'status' => null,
                    'package_cost' => 0,
                    'package_date' => null,
                    'package_deposit' => 0,
                    'package_payment' => 0,
                    'package_refund' => 0,
                    'package_discount' => 0,
                    'package_balance' => 0
                );
            endif;
        endif;
        return $data;
    }

    public function clientAddPackage($client_id){
        Repack:
        $tracking = Common::generateIndividualTrackingNumber(7);
        $check_package = Package::where('tracking', $tracking)
            ->count();
        if($check_package > 0) :
            goto Repack;
        endif;

        date_default_timezone_set("Asia/Manila");
        $dates = date("F j, Y, g:i a");

        $new_package_data = array(
            'client_id' => $client_id,
            'tracking' => $tracking,
            'log_date' => $dates
        );

        $new_package = Package::insert($new_package_data);

        if ($new_package) :
            $new_log = 'Created new package';
            Common::saveActionLogs(0,$client_id,$new_log);
            $auth = Auth::user()->first_name;
            $title = $auth." created new package";
            $title_cn = $auth." 创建了新的服务包";
            $body = $auth.' created new package ';
            $body_cn = $auth." 创建了新的服务包";
            $main = $auth.' created new package #'.$tracking;
            $main_cn = $auth." 创建了新的服务包 #".$tracking;
            $parent  = 0;
            $parent_type  = 0;

            Common::savePushNotif($client_id, $title, $body,'Package',$tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
            $result = array('status' => 'success', "tracking" =>$tracking, "client_id" => $client_id);
        else :
            $result = array('status' => 'failed', "tracking" =>$tracking, "client_id" => $client_id);
        endif;
        return json_encode($result);
    }

    public function clientDeletePackage(Request $request){
        $tracking = $request->get('tracking');
        $reason = $request->get('reason');
        $check_registered_service = ClientService::where('tracking', $tracking)->count();
        $package = Package::where('tracking', $tracking)->first();

        if ($check_registered_service < 1) :
            $delete_package = Package::where('tracking', $tracking)->delete();
            if($delete_package) :

                //SynchronizationController::insertUpdate($package->client_id, Carbon::now()->toDateTimeString());

                $new_log = 'Deleted the package number <b>'.$tracking.'</b> with the reason of <i>"'.$reason.'"</i>.';
                Common::saveActionLogs(0,$package->client_id,$new_log);
                $title = "Package Deleted";
                $title_cn = "服务包已删除";
                $body = 'One of your package with reference number '.$tracking.' was deleted';
                $body_cn = "你的服务包及编号为 ".$tracking." 被删除。";
                $main = $body;
                $main_cn = $body_cn;
                $parent = $tracking;
                $parent_type = 1;
                Common::savePushNotif($package->client_id, $title, $body,'Package',$tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
                $result = array('status' => 'success', 'log' => 'Deleted Package');
            else :
                $result = array('status' => 'failed', 'log' => 'Error Deleting Package');
            endif;
        else :
            $result = array('status' => 'failed', 'log' => 'Unable to Delete, <br>it contains services.');
        endif;
        return json_encode($result);
    }

    public function clientAddFunds(Request $request){
        //\Log::info($request->all());
        $tracking = $request->get('tracking');
        $reason = $request->get('reason');
        $type = $request->get('type');
        $client_id = $request->get('client_id');
        $amount = $request->get('amount');
        $authorizer = $request->get('authorizer');
        $auth_name = $request->get('auth_name');
        $storage = $request->get('storage');
        $branch_id = $request->get('branch_id');
        $bank_type = $request->get('bank_type');
        $alipay_reference = $request->get('alipay_reference');
        $clearbalance = $request->get('clearbalance');

        $ali_ref = '';
        $bank_name = '';
        if($alipay_reference != ''){
            $ali_ref = '<br>Reference # : '.$alipay_reference;
        }
        if($storage=='bank'){
            $bank_name = ' - '.$bank_type;
        }

        date_default_timezone_set("Asia/Manila");
        $date = date('m/d/Y h:i:s A');

        $d = Carbon::now('Asia/Manila');
        $notifdate = $d->toDateString();
        $notiftime = $d->toTimeString();
        $dayname = $d->format('l');

        if ($type == "deposit") :
            $depo = new ClientTransactions;
            $depo->client_id = $client_id;
            $depo->type = 'Deposit';
            $depo->group_id = 0;
            $depo->tracking = $tracking;
            $depo->log_date = $date;
            if($storage=='bank'){
                $depo->storage_type = $bank_type;
            }
            if($storage=='alipay'){
                $amount = $amount-($amount*0.0175);
                $depo->storage_type = $bank_type;
                $depo->alipay_reference = $alipay_reference;
            }
            $depo->amount = $amount;
            $depo->save();

            //save transaction logs
            $notes = 'Deposited an amount of Php'.$amount.' on Package '.$tracking.'.';
            $notes_cn = '预存了款项 Php'.$amount.' 在服务包 '.$tracking.'.';
            $total_balance = $this->clientCompleteBalance($client_id);
            $log_data = array(
                'client_id' => $client_id,
                'service_id' => 0,
                'group_id' => 0,
                'type' => 'deposit',
                'amount' => $amount,
                'balance' => $total_balance,
                'detail'=> $notes,
                'detail_cn'=> $notes_cn,
            );
            Common::saveTransactionLogs($log_data);

            //save financing
            $finance = new Finance;
            $finance->user_sn = Auth::user()->id;
            $finance->type = "deposit";
            $finance->record_id = $depo->id;
            $finance->cat_type = "process";
            $finance->cat_storage = $storage;
            $finance->branch_id = $branch_id;
            $finance->storage_type = $bank_type;
            $finance->trans_desc = Auth::user()->first_name.' received deposit from client #'.$client_id.' on Package #'.$tracking;
            if($storage=='alipay'){
                $finance->trans_desc = Auth::user()->first_name.' received deposit from client #'.$client_id.' on Package #'.$tracking.' with Alipay reference: '.$alipay_reference;
            }
            ((strcasecmp($storage,'cash')==0) ? $finance->cash_client_depo_payment = $amount : $finance->bank_client_depo_payment = $amount);
            $finance->save();

            //push notif
            $title = "Deposit received";
            $title_cn = "";
            $body = 'Thank you, we received your deposit worth Php'.$amount.'. <br><br><b>'.ucfirst($storage).'</b>'.$bank_name.$ali_ref;
            $body_cn = "已折扣 Php".$amount;
            $main = $body;
            $main_cn = $body_cn;
            $parent = $notifdate."-".$client_id;
            $parent_type = 1;
            Common::savePushNotif($client_id, $title, $body,'Funds',$dayname."--".$client_id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

            $result = array('status' => 'success', 'log' => 'Deposit added successfully.');

        elseif($type == "payment") :
            $payment = new ClientTransactions;
            $payment->client_id = $client_id;
            $payment->type = 'Payment';
            $payment->group_id = 0;
            $payment->tracking = $tracking;
            $payment->log_date = $date;
            if($storage=='bank'){
                $payment->storage_type = $bank_type;
            }
            if($storage=='alipay'){
                $payment->storage_type = $bank_type;
                $payment->alipay_reference = $alipay_reference;
                $amount = $amount-($amount*0.0175);
            }
            $payment->amount = $amount;
            $payment->save();
            $notes = 'Paid an amount of Php'.$amount.' on Package '.$tracking.'.';
            $notes_cn = '已支付 Php'.$amount.' 到服务包 '.$tracking.'.';
            $total_balance = $this->clientCompleteBalance($client_id);
            $log_data = array(
                'client_id' => $client_id,
                'service_id' => 0,
                'group_id' => 0,
                'type' => 'payment',
                'amount' => $amount,
                'balance' => $total_balance,
                'detail'=> $notes,
                'detail_cn'=> $notes_cn,
            );
            Common::saveTransactionLogs($log_data);

            //for financing
            $finance = new Finance;
            $finance->user_sn = Auth::user()->id;            
            $finance->type = "payment";
            $finance->record_id = $payment->id;
            $finance->cat_type = "process";
            $finance->cat_storage = $storage;
            $finance->branch_id = $branch_id;
            $finance->storage_type = $bank_type;
            $finance->trans_desc = Auth::user()->first_name.' received payment from client #'.$client_id.' on Package #'.$tracking;
            if($storage=='alipay'){
                $finance->trans_desc = Auth::user()->first_name.' received payment from client #'.$client_id.' on Package #'.$tracking.' with Alipay reference: '.$alipay_reference;
            }
            ((strcasecmp($storage,'cash')==0) ? $finance->cash_client_depo_payment = $amount : $finance->bank_client_depo_payment = $amount);
            $finance->save();

            // push notif
            $title = "Payment received";
            $title_cn = "";
            $body = 'Thank you, we received your payment worth Php'.$amount.'. <br><br><b>'.ucfirst($storage).'</b>'.$bank_name.$ali_ref;
            $body_cn = "已折扣 Php".$amount;
            $main = $body;
            $main_cn = $body_cn;
            $parent = $notifdate."-".$client_id;
            $parent_type = 1;
            Common::savePushNotif($client_id, $title, $body,'Funds',$dayname."--".$client_id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

            $result = array('status' => 'success', 'log' => 'Payment added successfully.');

        elseif($type == "refund") :
            $total_balance = $this->clientCompleteBalance($client_id);
            if(($total_balance >= $amount) && ($total_balance > 0)) :
                $refund = new ClientTransactions;
                $refund->client_id = $client_id;
                $refund->type = 'Refund';
                $refund->amount = $amount;
                $refund->group_id = 0;
                $refund->reason = $reason;
                $refund->tracking = $tracking;
                $refund->log_date = $date;
      
            if($storage=='bank'){
                $refund->storage_type = $bank_type;
            }
                $refund->save();

                $total_balance = $this->clientCompleteBalance($client_id);
                $notes = 'Refunded an amount of Php'.$amount.' with the reason of <i>"'.$reason.'"</i> on Package '.$tracking.'.';
                $notes_cn = '退款了 Php'.$amount.' 因为 "'.$reason.'" 于服务包 '.$tracking.'.';
                $log_data = array(
                    'client_id' => $client_id,
                    'service_id' => 0,
                    'group_id' => 0,
                    'type' => 'refund',
                    'amount' => '-'.$amount,
                    'balance' => $total_balance,
                    'detail'=> $notes,
                    'detail_cn'=> $notes_cn,
                );
                Common::saveTransactionLogs($log_data);

                //for financing
                $finance = new Finance;
                $finance->user_sn = Auth::user()->id;
                $finance->type = "refund";
                $finance->record_id = $refund->id;
                $finance->cat_type = "process";
                $finance->cat_storage = $storage;
                $finance->cash_client_refund = $amount;
                $finance->branch_id = $branch_id;
                $finance->trans_desc = Auth::user()->first_name.' refund to client #'.$client_id.' on Package #'.$tracking.' for the reason of '.$reason;
                $finance->save();

            // push notif
            $title = "Made a Refund";
            $title_cn = "";
            $body = 'You\'ve made a refund worth Php'.$amount.'. <br><br><b>'.ucfirst($storage).'</b>'.$bank_name.$ali_ref;
            $body_cn = "已折扣 Php".$amount;
            $main = $body;
            $main_cn = $body_cn;
            $parent = $notifdate."-".$client_id;
            $parent_type = 1;
            Common::savePushNotif($client_id, $title, $body,'Funds',$dayname."--".$client_id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

                $result = array('status' => 'success', 'log' => 'Refund added successfully.', 'balance' => $total_balance );
            elseif(($total_balance <= 0)||($total_balance < $amount)):
                 $result = array('status' => 'error', 'log' => 'Insufficient balance.', 'balance' => $total_balance);
            endif;
        elseif($type == "discount") :
            $discount = new ClientTransactions;
            $discount->client_id = $client_id;
            $discount->type = 'Discount';
            $discount->amount = $amount;
            $discount->group_id = 0;
            $discount->reason = $reason;
            $discount->tracking = $tracking;
            $discount->log_date = $date;
            if($storage=='bank'){
                $discount->storage_type = $bank_type;
            }
            $discount->save();

            if($request->clearbalance){
                $cuser = User::findOrFail($client_id)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
                if($cuser->temp_balance < 0){
                    $cuser->temp_balance += $amount;
                    $cuser->save();
                }
            }

            //save transaction logs
            $notes = 'Discounted an amount of Php'.$amount.' with the reason of <i>"'.$reason.'"</i> on Package '.$tracking.'.';
            $notes_cn = '给于折扣 Php'.$amount.' 因为"'.$reason.'"于服务包 '.$tracking.'.';
            $total_balance = $this->clientCompleteBalance($client_id);
            $log_data = array(
                'client_id' => $client_id,
                'service_id' => 0,
                'group_id' => 0,
                'type' => 'discount',
                'amount' => $amount,
                'balance' => $total_balance,
                'detail'=> $notes,
                'detail_cn'=> $notes_cn,
            );
            Common::saveTransactionLogs($log_data);

            // push notif
            $title = "Received Discount";
            $title_cn = "";
            $body = 'You received a discount worth Php'.$amount.'.';
            $body_cn = "已折扣 Php".$amount;
            $main = $body;
            $main_cn = $body_cn;
            $parent = $notifdate."-".$client_id;
            $parent_type = 1;
            Common::savePushNotif($client_id, $title, $body,'Funds',$dayname."--".$client_id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

            $result = array('status' => 'success', 'log' => 'Discount added successfully.');

        elseif($type == "transfer") :
            $selClient = $request->get('selected_client');
            $selGroup = $request->get('selected_group');
            $total_balance = $this->clientCompleteBalance($client_id);

            $trans = new TransferBalance;
            $trans->transfer_from = $client_id;
            $trans->from_type = 'client';
            $trans->transfer_to = ($request->transfer_type == 'client' ? $selClient : $selGroup);
            $trans->to_type = $request->transfer_type;
            $trans->amount = $amount;
            $trans->reason = $reason;
            $trans->log_date = $date;
            $trans->save();

            // Refund amount to client
            $refund = new ClientTransactions;
            $refund->client_id = $client_id;
            $refund->type = 'Refund';
            $refund->amount = $amount;
            $refund->group_id = 0;
            $refund->reason = $reason;
            $refund->tracking = $tracking;
            $refund->log_date = $date;
            $refund->save();

            $transferred = $trans->transfer_to;
            $leaderId = '';
            if($request->transfer_type == 'group'){
                $transferred = Group::where('id',$selGroup)->first()->name;
                $leaderId = Group::where('id',$selGroup)->first()->leader;
            }
            if($request->transfer_type == 'client'){
                $cl_usr = User::where('id',$selClient)->select('id','first_name','last_name')->first();
                $transferred = $cl_usr->first_name.' '.$cl_usr->last_name;
            }

            $notes = 'Refunded an amount of Php'.$amount.', transferred to '.$request->transfer_type.' '.$transferred;
            $notes_cn = '退款 Php'.$amount.', 转移到了客户 '.$transferred;
            $total_balance = $this->clientCompleteBalance($trans->transfer_from);
            $log_data = array(
                'client_id' => $trans->transfer_from,
                'service_id' => 0,
                'group_id' => 0,
                'type' => 'refund',
                'amount' => '-'.$amount,
                'balance' => $total_balance,
                'detail'=> $notes,
                'detail_cn'=> $notes_cn,
            );
            Common::saveTransactionLogs($log_data);

            // push notif
            $title = "Made a Refund";
            $title_cn = "";
            $body = 'You\'ve made a refund worth Php'.$amount.', transferred to '.$request->transfer_type.' '.$transferred.'.';
            $body_cn = "已折扣 Php".$amount;
            $main = $body;
            $main_cn = $body_cn;
            $parent = $notifdate."-".$trans->transfer_from;
            $parent_type = 1;
            Common::savePushNotif($client_id, $title, $body,'Funds',$dayname."--".$trans->transfer_from,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

            $transTo = $selClient;
            $grid = 0;
            if($request->transfer_type == 'group'){
                $transTo = Group::where('id',$selGroup)->first()->leader;
                $grid = $selGroup;
                // $total_balance = $this->groupBalance($grid);
            }

            // Deposit amount to client or group selected
            $depo = new ClientTransactions;
            $depo->client_id = $transTo;
            $depo->type = 'Deposit';
            $depo->amount = $amount;
            $depo->group_id = $grid;
            $depo->tracking = null;
            $depo->log_date = $date;
            $depo->save();

            //for financing
            $finance = new Finance;
            $finance->user_sn = Auth::user()->id;
            $finance->type = "transfer";
            $finance->record_id = $trans->id;
            $finance->cat_type = "process";
            $finance->cat_storage = $storage;
            $finance->branch_id = $branch_id;
            ((strcasecmp($storage,'cash')==0) ? $finance->cash_client_depo_payment = $amount : $finance->bank_client_depo_payment = $amount);
            ((strcasecmp($storage,'cash')==0) ? $finance->cash_client_refund = $amount : $finance->bank_cost = $amount);
            $finance->trans_desc = Auth::user()->first_name.' transffered funds from client #'.$client_id.' to '.$request->transfer_type.' '.$transferred.'.';
            $finance->save();

            if($request->transfer_type == 'group'){
                $total_balance = $this->groupBalance($selGroup);
            }

            $notes = 'Deposited an amount of Php'.$amount.' from client '.$trans->transfer_from.'.';
            $notes_cn = '预存了款项 Php'.$amount.' 从客户 '.$trans->transfer_from.'.';
            $total_balance = $this->clientCompleteBalance($trans->transfer_to);
            $log_data = array(
                'client_id' => $transTo,
                'service_id' => 0,
                'group_id' => $grid,
                'type' => 'deposit',
                'amount' => $amount,
                'balance' => $total_balance,
                'detail'=> $notes,
                'detail_cn'=> $notes_cn,
            );
            Common::saveTransactionLogs($log_data);

            $cl_usr = User::where('id',$trans->transfer_from)->select('id','first_name','last_name')->first();
            $transfrom= $cl_usr->first_name.' '.$cl_usr->last_name;

            $title = "Deposit received";
            $title_cn = "";
            $body = 'Thank you, we received your deposit worth Php'.$amount.' from client '.$transfrom.'.';
            $body_cn = "已折扣 Php".$amount;
            $main = $body;
            $main_cn = $body_cn;
            $parent = $notifdate."-".$transTo;
            $parent_type = 1;
            Common::savePushNotif($transTo, $title, $body,'Funds',$dayname."--".$transTo,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

            $result = array('status' => 'success', 'log' => 'Transfer Balance successfully.');
        else :
            $result = array('status' => 'failed', 'log' => '.');
        endif;
        return json_encode($result);
    }

    private function getDocumentsOnHand($clientId, $serviceId) {
        $documentLogs = DocumentLogs::whereHas('clientService', function($query) use($clientId) {
            $query->where('client_id', $clientId);
        })->where('document_id', '<>', null)->get();

        $documentsOnHand = [];
        foreach($documentLogs as $dl) {
            if($dl->type == 'Received') {
                $documentsOnHand[] = $dl->document_id;
            } elseif($dl->type == 'Released') {
                $documentsOnHand = array_diff($documentsOnHand, [$dl->document_id]);
            }
        }

        $service = Service::findOrFail($serviceId);
        $docsNeeded = explode(',', $service->docs_needed);
        $docsOptional = explode(',', $service->docs_optional);
        $docsReleased = explode(',', $service->docs_released);
        $docsReleasedOptional = explode(',', $service->docs_released_optional);
        $allServiceDocs = array_merge($docsNeeded, $docsOptional, $docsReleased, $docsReleasedOptional);

        $documentsOnHand = array_unique(array_intersect($documentsOnHand, $allServiceDocs));
        $documentsOnHandString = implode(',', $documentsOnHand);
        
        return strlen($documentsOnHandString) > 0 ? $documentsOnHandString : null;
    }

    private function getDocumentsOnHandToString($commaSeparatedId) {
        $ids = explode(',', $commaSeparatedId);

        $documents = '';

        foreach($ids as $id) {
            if($id) {
                $sd = ServiceDocuments::findOrfail($id);

                $documents .= (\Request::cookie('locale') == 'en') ? $sd->title : $sd->title_cn;
                $documents .= ',';
            }
        }

        return substr($documents, 0, -1);
    }

    public function clientAddService(Request $request) {

            $clientServicesIdArray = [];

            $services = $request->get('services');
            $client_id = $request->get('client_id');
            $tracking = $request->get('track');
            $sid = $request->get('serviceid');
            $docs_needed = $request->get('req_docs');

            if (is_array($services)) :
            $ctr = 0;

            foreach($services as $service) :
                $serv = Service::findOrFail($service);

            $inpcost = $request->get('cost');
            $inpcharge = $request->get('charge');
            $inptip = $request->get('tip');

                $dt = Carbon::now();
                $dt = $dt->toDateString();
                //$user = Auth::user();

                if($request->get('note')) {
                    $author = $request->get('note').' - '. Auth::user()->first_name.' <small>('.$dt.')</small>';
                } else {
                    $author = '';
                }
                if($service == $sid[0]) {
                    $docs = $request->get('docs');
                } else {
                    $docs = '';
                }

                $req_docs = [];
                if($serv->docs_needed!= null || $serv->docs_needed!= ''){
                    $req_docs = explode(",", $serv->docs_needed); //33,16
                }
                // $req_docs = explode(",", $serv->docs_needed);
                $chosen_docs = [];
                if(count($request->docs)> 0){                
                    if($request->docs[$ctr]!= null || $request->docs[$ctr]!= ''){
                        $chosen_docs =explode(",", $request->docs[$ctr]);
                    }
                }

                $compare_docs = array_diff($req_docs, $chosen_docs);

                //status depending on submitted docs
                if(!empty($compare_docs)){
                    $service_status = 'pending';
                }else{
                    $service_status = 'on process';
                }

                $chosen = '';
                $chosen_cn = '';
                foreach($chosen_docs as $ch){
                    $sd = ServiceDocuments::findOrFail($ch);
                    $chosen .= $sd->title.",";
                    $chosen_cn .= $sd->title_cn.",";
                }
                $chosen = rtrim($chosen, ',');
                $chosen_cn = rtrim($chosen_cn, ',');

                $missing = '';
                $missing_cn = '';
                if(!empty($compare_docs)){
                    foreach($compare_docs as $cd){
                        $sd = ServiceDocuments::findOrFail($cd);
                        $missing .= $sd->title.",";
                        $missing_cn .= $sd->title_cn.",";
                    }
                }
                $missing = rtrim($missing, ',');
                $missing_cn = rtrim($missing_cn, ',');

                $branch_id = User::find($client_id)->branch_id;
                if($branch_id == 2){
                    $bcost = ServiceBranchCost::where('service_id',$serv->id)->where('branch_id', $branch_id)->first();
                    $inpcost = ($inpcost < 1 ? $bcost->cost : $inpcost);
                    $inptip = ($inptip < 1 ? $bcost->tip : $inptip);
                    $inpcharge = ($inpcharge < 1 ? $bcost->charge : $inpcharge);
                }
                else{
                    $inpcost = ($inpcost < 1 ? $serv->cost : $inpcost);
                    $inptip = ($inptip < 1 ? $serv->tip : $inptip);
                    $inpcharge = ($inpcharge < 1 ? $serv->charge : $inpcharge);
                }

                if($inpcost == 0 && $inpcharge == 0 && $inptip == 0){
                    $inpcost = $serv->cost;
                    $inpcharge = $serv->charge;
                    $inptip = $serv->tip;
                }
                // $gp = Package::where('tracking',$tracking)->first();
                // $inpcost = ($inpcost < 1 ? 0 : $inpcost);
                // $inptip = ($inptip < 1 ? 0: $inptip);
                // $inpcharge = ($inpcharge < 1 ? 0 : $inpcharge);

                // if($inpcost == 0){
                //     $inpcost = $serv->cost;
                // }

                // if($inptip == 0){
                //     $inptip = $serv->tip;
                // }

                // if($inpcharge == 0){
                //     $inpcharge = $serv->charge;
                // }
                //$inpcharge = ($serv->cost + $serv->tip + $serv->charge);

                $service_status = DocumentLogsController::getClientServiceStatus($service, $request->docs[$ctr]);


                $cs = new ClientService;
                $cs->client_id = $client_id;
                $cs->service_id = $service;
                $cs->detail = $serv->detail;
                $cs->cost = $inpcost;
                $cs->charge = $inpcharge;
                $cs->tip = $inptip;
                $cs->status = $service_status;
                $cs->service_date = Carbon::now()->format('m/d/Y');
                $cs->remarks = $author;
                $cs->group_id = 0;
                $cs->tracking = $tracking;
                $cs->temp_tracking = null;
                $cs->active = 1;
                $cs->extend = null;
                if(count($request->docs) > 0){
                    $cs->rcv_docs = (!is_null($request->docs[$ctr])) ? $request->docs[$ctr] : '';
                }
                $cs->save();

                //add new shop rate if not existing
                $referred = UserOpen::where('client_id',$cs->client_id)->first();
                if($referred){                
                    $sd = explode("/",$cs->service_date);
                    $rateExist = ShopRate::where('client_id',$referred->referral_id)->where('month',$sd[0])->where('year',$sd[2])->first();
                    if(!$rateExist){
                         $prevMaxRate = ShopRate::where('client_id',$referred->referral_id)->max('rate');
                         $rate = ShopRate::create([
                            'client_id' => $referred->referral_id,
                            'month' => $sd[0],
                            'year' => $sd[2],
                            'rate' => ($prevMaxRate > 0.4 ? $prevMaxRate : 0.4),
                        ]);
                    }
                }
                // end

                // ******************** Document Tracker ******************** //
                    // for (63)9A VISA EXTENSION only // TEMPORARY
                    if( $request->docs[$ctr] && $serv->parent_id == 63 ) {
                        // $dl_detail = 'Received document/s from client for service <strong style="color:orange;">'.$serv->detail.'</strong>:';


                        // DocumentLogs::create([
                        //     'client_service_id' => $cs->id,
                        //     'documents' => $request->docs[$ctr],
                        //     'documents_on_hand' => $request->docs[$ctr],
                        //     'processor_id' => Auth::user()->id,
                        //     'type' => 'Received',
                        //     'detail' => $dl_detail,
                        //     'detail_cn' => $dl_detail,
                        //     'log_date' => Carbon::now()->format('Y-m-d')
                        // ]);

                        $originsArray = DocumentLogsController::getOrigins(
                            explode(',', $request->docs[$ctr]),
                            $client_id,
                            $cs->id
                        );

                        DocumentLogsController::saveOrigins($originsArray, $cs->id);
                    }
                // ******************** End of Document Tracker ******************** //

                $clientServicesIdArray[] = $cs;

                // Task Generator
                if($cs->status == 'on process') {
                    TasksController::save([$cs], [], [], [], [], [], [], $request->sameDayFilling);
                }
                //dd($inpcost);
                $this->check_package_updates($tracking);

                $translated = Service::where('id',$service)->first();
                $cnserv =$serv->detail;
                if($translated){
                    $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                }

                // service status
                if($service_status == 'complete'){
                    $status = $service_status;
                    $status_cn = "已完成";
                }
                if($service_status == 'on process'){
                    $status = $service_status;
                    $status_cn = "办理中";
                }
                if($service_status == 'pending'){
                    $status = $service_status;
                    $status_cn = "待办";
                }

                $total_charge = $inpcharge + $inpcost + $inptip;
                //$log ='with <b>total service charge</b> of Php'.($total_charge + '0');
                //$log_cn = '以及总服务费 Php'.($total_charge + '0');
                $log = '';
                $log_cn = '';
                $notes = 'Added a service '.$log.'. Service status is '.$status.'.';
                $notes_cn = '已添加服务 '.$log_cn.'. 服务状态为 '.$status_cn.'。';
                $total_balance = $this->clientCompleteBalance($client_id);
                $log_data = array(
                    'client_id' => $client_id,
                    'service_id' => $cs->id,
                    'group_id' => 0,
                    'type' => 'service',
                    // 'amount' => '-'. ($inpcost + $inpcharge + $inptip),
                    'amount' => 0,
                    'balance' => $total_balance,
                    'detail'=> $notes,
                    'detail_cn'=> $notes_cn,
                );
                 Common::saveTransactionLogs($log_data);

                    if($missing!=''){
                        $missing = "<br><br>- Missing documents (".$missing.").";
                        $missing_cn = "<br><br>- 文件缺失 (".$missing_cn.").";
                        // $missing = $missing." are missing";
                        // $missing_cn = $missing_cn." 目前缺失";
                    }

                    if($chosen!=''){
                        $chosen = "<br><br>- Received documents (".$chosen.").";
                        $chosen_cn = "<br><br>- 已验收文件 (".$chosen_cn.").";
                    }

                    if($chosen!= '' || $missing !=''){
                        $reportDetail = $chosen.$missing;
                        Report::create([
                            'service_id' => $cs->id,
                            'client_id' => $cs->client_id,
                            'group_id' => $cs->group_id,
                            'detail' => $reportDetail,
                            'user_id' => Auth::user()->id,
                            'tracking' => $cs->tracking,
                            'log_date' => Carbon::now()->format('Y-m-d H:i:s a'),
                            'is_read' => 0
                        ]);
                    }

                    //send notif to client
                    // service status
                    if($service_status == 'complete'){
                        $status = "<b><h4>".$service_status."</h4></b>";
                        $status_cn = "<b><h4> 已完成  </h4></b>";
                    }
                    if($service_status == 'on process'){
                        $status = "<b><h5>".$service_status."</h5></b>";
                        $status_cn = "<b><h5> 办理中 </h5></b>";
                    }
                    if($service_status == 'pending'){
                        $status = "<b><h6>".$service_status."</h6></b>";
                        $status_cn = "<b><h6> 待办 </h6></b>";
                    }
                    $title = "New Service";
                    $title_cn = "新的服务";
                    // disable document details in notifications
                    //$body = 'New Service '.$serv->detail.' was added. '.$chosen.$missing.'<br><br> - Service status is '.$status;
                    $body = 'New Service '.$serv->detail.' was added. <br><br> - Service status is '.$status;
                    //$body_cn = "新的服务 ".$cnserv." 已添加. ".$chosen_cn.$missing_cn.'<br><br> - 服务状态为 '.$status_cn;
                    $body_cn = "新的服务 ".$cnserv." 已添加. <br><br> - 服务状态为 ".$status_cn;
                    $main = $serv->detail;
                    $main_cn = $cnserv;
                    $parent = $tracking;
                    $parent_type = 1;
                    Common::savePushNotif($client_id, $title, $body,'Service',$cs->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

                // if($request->get('discount')) {
                //     ClientTransactions::create([
                //         'client_id' => $client_id,
                //         'type' => 'Discount',
                //         'amount' => $request->get('discount'),
                //         'group_id' => 0,
                //         'service_id' => $cs->id,
                //         'reason' => $request->get('reason'),
                //         'tracking' => $tracking,
                //         'log_date' => Carbon::now()->format('m/d/Y h:i:s A')
                //     ]);
                //     $personal_balance = $this->clientBalance($client_id);
                //     $notes = 'Discounted an amount of Php'.$request->get('discount').' with the reason of <i>"'.$request->get('reason').'"</i> on Package '.$tracking.'.';
                //     $notes_cn = '已折扣额度 Php'.$request->get('discount').' 因为<i>"'.$request->get('reason').'"</i> 在服务包 # '.$tracking.'.';
                //     $log_data2 = array(
                //         'client_id' => $client_id,
                //         'service_id' => 0,
                //         'tracking' => $tracking,
                //         'type' => 'discount',
                //         'amount' => $request->get('discount'),
                //         'detail'=> $notes,
                //         'detail_cn'=> $notes_cn,
                //         'balance'=> $personal_balance,
                //     );
                //     Common::saveTransactionLogs($log_data2);

                    // push notif
                    // $title = "Received Discount";
                    // $title_cn = "收到折扣";
                    // $body = 'Discounted an amount of Php'.$request->get('discount').'" on Service '.$cs->detail.'.';
                    // $body_cn = "已折扣 Php".$request->get('discount').'" 服务 '.$cnserv.'.';
                    // $main = $body;
                    // $main_cn = $body_cn;
                    // $parent = $cs->id;
                    // $parent_type = 2;
                    // Common::savePushNotif($client_id, $title, $body,'Discount',$tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

                    // $amount = $request->get('discount');
                    // // push notif
                    // $title = "Received Discount";
                    // $title_cn = "";
                    // $body = 'You received a discount worth Php'.$amount.'.';
                    // $body_cn = "已折扣 Php".$amount;
                    // $main = $body;
                    // $main_cn = $body_cn;
                    // $parent = $notifdate."-".$client_id;
                    // $parent_type = 1;
                    //Common::savePushNotif($client_id, $title, $body,'Funds',$dayname."--".$client_id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
                //}

                $ctr++;
            endforeach;
            endif;

        // ******************** Document Tracker ******************** //
            DocumentLogsController::share([$client_id]);
        // ******************** End of Document Tracker ******************** //

        return json_encode([
            'success' => true,
            'message' => 'Service/s successfully added to client',
            'clientsId' => [$client_id],
            'clientServicesId' => $clientServicesIdArray,
            'trackings' => [$tracking]
        ]);
    }

    private function isRequestingRelease($clientService, $rcvDocsReleased) {
        $flag = false;

        $_currentRcvDocsReleased = ($clientService->rcv_docs_released != '' && $clientService->rcv_docs_released != null) 
            ? explode(',', $clientService->rcv_docs_released) 
            : [];
            
        $_rcvDocsReleased = ($rcvDocsReleased != '' && $rcvDocsReleased != null) 
            ? explode(',', $rcvDocsReleased) 
            : [];

        $newEntryDocuments = array_diff($_rcvDocsReleased, $_currentRcvDocsReleased);

        if( count($newEntryDocuments) > 0 ) {
            $flag = true;
        }

        return $flag;
    }

    public function clientEditService(Request $request) {
        $id = $request->get('id');
        $service = ClientService::findOrFail($id);

        // // Document Logs
        //     // Upon Completion for Document Logs
        //         $uponCompletion = false;
        //         if($service->status != 'complete' && $request->get('status') == 'complete') {
        //             $uponCompletion = true;
        //         }

        //     // If Released
        //         $__translated = Service::where('id', $service->service_id)->first();

        //         $allDocsReleased = [];
        //         if($__translated->docs_released != null && $__translated->docs_released != '') {
        //             $allDocsReleased = explode(",", $__translated->docs_released);
        //         }

        //         $chosenDocsReleased = [];
        //         if($request->get('rcv_docs_released') != null && $request->get('rcv_docs_released') != '') {
        //             $chosenDocsReleased = explode(",", $request->get('rcv_docs_released'));
        //         }

        //         $_released = 0;
        //         $intersect = array_intersect($allDocsReleased, $chosenDocsReleased);
        //         if(count($intersect) == count($allDocsReleased)) {
        //             $_released = 1;
        //         }

        //     // Current rcv_docs
        //         $__currentReceivedDocs = [];
        //         if($service->rcv_docs != '' && $service->rcv_docs != null) {
        //             $__currentReceivedDocs = explode(',', $service->rcv_docs);
        //         }

        //     // Current rcv_docs_released
        //         $__currentReleasedDocs = [];
        //         if($service->rcv_docs_released != '' && $service->rcv_docs_released != null) {
        //             $__currentReleasedDocs = explode(',', $service->rcv_docs_released);
        //         }
        // // End of Document Logs

        //insert budget ouput code for financing
        // if ($service->cost != $request->get('cost') || $service->tip != $request->get('tip')){
        //   BudgetOutput::create([
        //       'user_id' => Auth::user()->id,
        //       'client_id' => $service->client_id,
        //       'service_id' => $service->service_id,
        //       'cost' => $request->get('cost'),
        //       'extra_cost' => $request->get('tip'),
        //   ]);
        // }

        if($request->get('status')=='complete' && $request->get('active')==1){
            
                $visa_type = (($request->get('parent_id') == 63) ? '9A' : (($request->get('parent_id') == 70) ? '9G' : (($request->get('parent_id') == 148) ? 'TRV' :  ($request->get('parent_id')== 238 ? 'CWV' : ($request->get('parent_id')==114 ? '9A' : NULL)))));

                if($visa_type!=''){   
                    if($visa_type==114){
                        User::where('id', $service->client_id)->update(['visa_type'=>'9A','exp_date'=>(($request->get('exp_date')!='') ? Carbon::parse($request->get('exp_date'))->format('Y-m-d') : NULL),'icard'=>NULL,'icard_exp_date'=>NULL,'arrival_date'=>$request->get('arrival_date'),'first_exp_date'=>$request->get('first_exp_date')]);
                    }else{
                        if($visa_type=='9A'){
                        User::where('id', $service->client_id)->update(['visa_type'=>$visa_type,'exp_date'=>(($request->get('exp_date')!='') ? Carbon::parse($request->get('exp_date'))->format('Y-m-d') : NULL),'icard'=>NULL,'icard_exp_date'=>NULL,'arrival_date'=>$request->get('arrival_date'),'first_exp_date'=>$request->get('first_exp_date')]);
                        }else if($visa_type=='9G' || $visa_type=='TRV'){
                        User::where('id', $service->client_id)->update(['visa_type'=>$visa_type,'exp_date'=>(($request->get('exp_date')!='') ? Carbon::parse($request->get('exp_date'))->format('Y-m-d') : NULL),'icard_exp_date'=>$request->get('icard_exp_date'),'issue_date'=>$request->get('issue_date')]);
                        }else{
                        User::where('id', $service->client_id)->update(['visa_type'=>$visa_type,'exp_date'=>(($request->get('exp_date')!='') ? Carbon::parse($request->get('exp_date'))->format('Y-m-d') : NULL)]);
                        }
                    }
                }      
        }
        //end insert

        $activer1 = (($request->get('active') == 1)? 'Active' : 'Inactive');
        $activer2 = (($service->active == 1)? 'Active' : 'Inactive');

        $log = '';
        $translog = '';
        $translog_cn = '';
        $translog1 = '';
        $translog1_cn = '';
        $translog2 = '';
        $translog2_cn = '';

        $newVal = 0;
        $oldVal = 0;

        $translated = Service::where('id',$service->service_id)->first();
        $cnserv =$service->detail;
        if($translated){
            $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
        }

        //SERVICE DOCUMENTS
        $allreq_docs = [];
        if($translated->docs_needed!= null && $translated->docs_needed!= ''){
            $allreq_docs = explode(",", $translated->docs_needed); //33,16,2
        }

        $req_docs = [];
        if($service->rcv_docs!= null && $service->rcv_docs!= ''){
            $req_docs = explode(",", $service->rcv_docs); //33,16
        }
        $chosen_docs = [];
        if($request->get('rcv_docs')!= null && $request->get('rcv_docs') != ''){
            $chosen_docs =explode(",", $request->get('rcv_docs')); //33,16,2
        }

        $compare_docs = array_diff($allreq_docs, $chosen_docs); //null

        $same_docs = array_intersect($req_docs, $chosen_docs); //33,16

        $remove = array_diff($req_docs, $same_docs); //null

        $additional = array_diff($chosen_docs, $same_docs); //2

        //status depending on submitted docs
        if(!empty($req_docs)){
            if($request->get('status') != 'complete'){
                if(!empty($compare_docs)){
                    $service_status = 'pending';
                }else{
                    $service_status = 'on process';
                }
            }
            else{
                $service_status = 'complete';
            }
        }
        else{
            $service_status = $request->get('status');
        }

        $service_status = ($request->status == 'complete') 
            ? 'complete'
            : DocumentLogsController::getClientServiceStatus($service->service_id, $request->rcv_docs);

        $add = '';
        $add_cn = '';
        foreach($additional as $ch){
            $sd = ServiceDocuments::findOrFail($ch);
            $add .= $sd->title.",";
            $add_cn .= $sd->title_cn.",";
        }
        $add = rtrim($add, ',');
        $add_cn = rtrim($add_cn, ',');

        $rem = '';
        $rem_cn = '';
        if(!empty($remove)){
            foreach($remove as $cd){
                $sd = ServiceDocuments::findOrFail($cd);
                $rem .= $sd->title.",";
                $rem_cn .= $sd->title_cn.",";
            }
        }
        $rem = rtrim($rem, ',');
        $rem_cn = rtrim($rem_cn, ',');
        //END SERVICE DOCUMENTS

        if ($service->active != $request->get('active')) :
            if ($log == ''):
                $log = $log.'from '.$activer2.' to '.$activer1;
            else:
                $log = ','.$log.'from '.$activer2.' to '.$activer1;
            endif;
            
            if($request->get('active') == 1) { // Enabled
                $translog2 = 'Service was enabled.';
                $translog2_cn = '服务被标记为已启用.';
                $translog1 = 'Total service charge from Php0 to ' . 'Php'. ($service->cost + $service->charge + $service->tip);
                $translog1_cn = '总服务费从 Php0 到 ' . 'Php'. ($service->cost + $service->charge + $service->tip);
            } elseif($request->get('active') == 0) { // Disabled
                $translog2 = 'Service was disabled.';
                $translog2_cn = '服务被标记为失效.';
                $translog1 = 'Total service charge from Php'. ($service->cost + $service->charge + $service->tip).' to Php'.'0.';
                $translog1_cn = '总服务费从 Php'. ($service->cost + $service->charge + $service->tip).' 到 Php'.'0.';
            }
    
            $newVal +=0;
            $oldVal +=($service->cost + $service->charge + $service->tip);

            $title = "Update for Service ".$service->detail;
            $title_cn = "服务更新 ".$cnserv;
            $body = $service->detail.' is now '.$activer1;
            $main = $body;

            $st = (($activer1 == 'Active')? '现已生效' : '现已无效');
            $body_cn = $cnserv." ".$st;
            $main_cn = $body_cn;
            $parent = $service->id;
            $parent_type = 2;
            Common::savePushNotif($service->client_id, $title, $body,'Service',$service->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
        endif;

        // DISCOUNT
        $oldDiscount = 0;
        $newDiscount = 0;
        $dcflag = false;
        $discnotes = '';
        $discnotes_cn = '';
        if($request->get('discount') > 0) {

            $__oldDiscount = null;

            $dc = ClientTransactions::where("service_id",$id)->where('type','Discount')->withTrashed()->first();

            if($dc){
                $__oldDiscount = $dc->amount;
                $oldDiscount = $dc->amount;
                $dc->amount =  $request->get('discount');
                $dc->reason =  $request->get('reason');
                $dc->deleted_at = null;
                $dc->save();
            } else {
                ClientTransactions::create([
                    'client_id' => $service->client_id,
                    'type' => 'Discount',
                    'amount' => $request->get('discount'),
                    'group_id' => $service->group_id,
                    'service_id' => $id,
                    'reason' => $request->get('reason'),
                    'tracking' => $service->tracking,
                    'log_date' => Carbon::now()->format('m/d/Y h:i:s A')
                ]);
                

                    $title = "Received Discount";
                    $title_cn = "收到折扣";
                    $body = 'Discounted an amount of Php'.$request->get('discount').' on Service '.$service->detail.'.';
                    $body_cn = "已折扣 Php".$request->get('discount'). " 在 ".$cnserv;
                    $main = $body;
                    $main_cn = $body_cn;
                    $parent = $service->id;
                    $parent_type = 2;
                    Common::savePushNotif($service->client_id, $title, $body,'Discount',$service->tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
            }
            $dcflag = true;

            // Update discount
            if($__oldDiscount != null && $__oldDiscount != $request->get('discount')) {
                $discnotes = ' updated discount from Php' . $__oldDiscount . ' to Php' . $request->get('discount').', ';
                $discnotes_cn = ' 已更新折扣 ' . $__oldDiscount . ' 到 ' . $request->get('discount') .', ';
                $oldDiscount = $__oldDiscount;
                $newDiscount = $request->get('discount'); 
                // $transactionLogsData = array(
                //     'client_id' => $service->client_id,
                //     'service_id' => $id,
                //     'tracking' => $service->tracking,
                //     'type' => 'discount',
                //     'amount' => $request->get('discount') - $__oldDiscount,
                //     'detail'=> $notes,
                //     'detail_cn'=> $notes_cn,
                //     'balance'=> $this->clientCompleteBalance($service->client_id),
                // );
                //Common::saveTransactionLogs($transactionLogsData); 
            }

            if($__oldDiscount == $request->get('discount')){
                $oldDiscount = $__oldDiscount;
                $newDiscount = $request->get('discount'); 
            }

            // New Discount
            if($__oldDiscount == null) { 
                $discnotes = ' discounted an amount of Php'.$request->get('discount').', ';
                $discnotes_cn = ' 已折扣额度 Php'.$request->get('discount').', ';
                $newDiscount = $request->get('discount'); 
                // $transactionLogsData = array(
                //     'client_id' => $service->client_id,
                //     'service_id' => $id,
                //     'tracking' => $service->tracking,
                //     'type' => 'discount',
                //     'amount' => $request->get('discount'),
                //     'detail'=> $notes,
                //     'detail_cn'=> $notes_cn,
                //     'balance'=> $this->clientCompleteBalance($service->client_id),
                // );
                //Common::saveTransactionLogs($transactionLogsData);  
            }

        } else {
            $discountExist = ClientTransactions::where('service_id', $request->id)->where('type','Discount')->first();
            if($discountExist){
                $discountExistAmount = $discountExist->amount;

                // Delete from client_transactions
                    $discountExist->forceDelete();

                // When user removed discount
                    $discnotes = ', removed discount of Php ' . $discountExist->amount . ', ';
                    $discnotes_cn = ', 移除折扣 ' . $discountExist->amount.', ';
                    $oldDiscount = $discountExist->amount;
                    // $transactionLogsData = array(
                    //     'client_id' => $service->client_id,
                    //     'service_id' => $id,
                    //     'tracking' => $service->tracking,
                    //     'type' => 'discount',
                    //     'amount' => $discountExistAmount,
                    //     'detail'=> $notes,
                    //     'detail_cn'=> $notes_cn,
                    //     'balance'=> $this->clientCompleteBalance($service->client_id),
                    // );
                    //Common::saveTransactionLogs($transactionLogsData);  
            }
        }
        // END Discount

        // Check if there's changes in amounts
        $oldServiceCost = $service->cost + $service->charge + $service->tip;
        $newServiceCost = $request->get('cost') + $request->get('charge') + $request->get('tip');
        if($newDiscount > 0 || $oldDiscount > 0 ){
            $oldServiceCost -= $oldDiscount;
            $newServiceCost -= $newDiscount;
        }

        if ($oldServiceCost != $newServiceCost || $service_status == 'complete') :
            if($request->get('active') == 1) { // Enabled
                $toAmount = $newServiceCost;
            } elseif($request->get('active') == 0) { // Disabled
                $toAmount = 0;
            }

            //if($service->status == 'complete'){            
                $translog1 = 'Total service charge from Php' . ($oldServiceCost) . ' to Php' . $toAmount;
                $translog1_cn = '总服务费从 Php' . ($oldServiceCost) . ' 到 Php' . $toAmount;
            //}
            if($service_status == 'complete'){
                $translog1 = 'Total service charge is Php' . $toAmount;
                $translog1_cn = '总服务费 Php' . $toAmount;
            }

            $newVal +=$newServiceCost;
            $oldVal +=$oldServiceCost;
        endif;

        if ($oldServiceCost == $newServiceCost && $service_status != 'complete') :
            $toAmount = 0;
           
            if($service->status == 'complete'){ 
                $translog1 = 'Total service charge from Php' . ($oldServiceCost) . ' to Php' . $toAmount;
                $translog1_cn = '总服务费从 Php' . ($oldServiceCost) . ' 到 Php' . $toAmount;
            }
            else{
                $translog1 = '';
                $translog1_cn = '';
            }

            $newVal +=$newServiceCost;
            $oldVal +=$oldServiceCost;
        endif;

        if ($oldServiceCost == $newServiceCost && $service_status == $service->status) :
            $translog1 = '';
            $translog1_cn = '';
        endif;

        // if ($service->cost != $request->get('cost')) :
        //     if($request->get('active') == 1) { // Enabled
        //         $toAmount = ($request->get('cost') + $request->get('charge') + $request->get('tip'));
        //     } elseif($request->get('active') == 0) { // Disabled
        //         $toAmount = 0;
        //     }

        //     $translog1 = 'Total service charge from Php' . ($service->cost + $service->charge + $service->tip) . ' to Php' . $toAmount;
        //     $translog1_cn = '总服务费从 Php' . ($service->cost + $service->charge + $service->tip) . ' 到 Php' . $toAmount;

        //     $newVal +=$request->get('cost');
        //     $oldVal +=$service->cost;
        // endif;

        // if ($service->tip != $request->get('tip')) :
        //     if($request->get('active') == 1) { // Enabled
        //         $toAmount = ($request->get('cost') + $request->get('charge') + $request->get('tip'));
        //     } elseif($request->get('active') == 0) { // Disabled
        //         $toAmount = 0;
        //     }

        //     $translog1 = 'Total service charge from Php' . ($service->cost + $service->charge + $service->tip) . ' to Php' . $toAmount;
        //     $translog1_cn = '总服务费从 Php' . ($service->cost + $service->charge + $service->tip) . ' 到 Php' . $toAmount;

        //     $newVal +=$request->get('tip');
        //     $oldVal +=$service->tip;
        // endif;

        // if ($service->charge != $request->get('charge')) :
        //     $translog1 = 'Total service charge from Php' . ($service->cost + $service->charge + $service->tip) . ' to Php' . ($request->get('cost') + $request->get('charge') + $request->get('tip'));
        //     $translog1_cn = '总服务费从 Php' . ($service->cost + $service->charge + $service->tip) . ' 到 Php' . ($request->get('cost') + $request->get('charge') + $request->get('tip'));
            
        //     $newVal +=$request->get('charge');
        //     $oldVal +=$service->charge;
        // endif;

        if (($service->status != $service_status) || $add !='' || $rem != '') :

          if ($log == ''):
            $log = $log.' <b>Service Status</b> from '.$service->status.' to '.$service_status;
          else:
            $log = ','.$log.' <b>Service Status</b> from '.$service->status.' to '.$service_status;
          endif;

          $addition = '';
          $addition_cn = '';
          if($add != ''){
            $addition = 'Received Additional Document/s ('.$add.'), ';
            $addition_cn = '已验收文件 ('.$add_cn.'), ';
          }
          $rmv = '';
          $rmv_cn = '';
          if($rem != ''){
            $rmv = 'Remove Invalid Document/s ('.$rem.'), ';
            $rmv_cn = '去掉文件 ('.$rem_cn.'), ';
          }

           $title = "Status update for Service ".$service->detail;
           // disable missing and receive documents for notifications
           // $body = $addition.$rmv.'Service status for '.$service->detail.' is now '.$service_status;
           $body = 'Service status for '.$service->detail.' is now '.$service_status;
           $title_cn = "服务更新 ".$cnserv;
           //$st = (($service_status == 'on process')? '已改为正在办理' : ($service_status == 'complete') ? '已改为完成' : '已改为待办(文件缺失或有误)');
           if($service_status  == 'on process'){
             $st = '已改为正在办理';
           }
           else if($service_status  == 'complete'){
             $st = '已改为完成';
           }
           else if($service_status  == 'pending'){
             $st = '已改为待办(文件缺失或有误)';
           }
           //$body_cn = $addition_cn.$rmv_cn." 的服务状态 为".$cnserv." ".$st;
           $body_cn = " 的服务状态 为".$cnserv." ".$st;
           $parent = $service->id;
           $parent_type = 2;

           // if($service->status == $service_status){
              // $body = $addition.$rmv.'.';
              // $body_cn = $addition_cn.$rmv_cn.'.';
           // }

           if($request->get('status') =='complete'){
             $checkDisc = ClientTransactions::where("service_id",$service->id)->where('type','Discount')->first();
             $gTotal = $service->cost + $service->charge + $service->tip;
             if($checkDisc){
                $gTotal -= $checkDisc->amount;
             }
             $body = $body." with total cost of Php".$gTotal;
             $body_cn = $body_cn." 以及总服务费 Php".$gTotal;
           }
           $main = $body;
           $main_cn = $body_cn;

           Common::savePushNotif($service->client_id, $title, $body,'Service',$service->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

           if($add!= '' || $rem !=''){
               Report::create([
                    'service_id' => $service->id,
                    'client_id' => $service->client_id,
                    'group_id' => $service->group_id,
                    'detail' => $addition.$rmv.'.',
                    'user_id' => Auth::user()->id,
                    'tracking' => $service->tracking,
                    'log_date' => Carbon::now()->format('Y-m-d H:i:s a'),
                    'is_read' => 0
                ]);
           }
        endif;

        if(stripslashes($service->remarks) != $request->get('note') && $request->get('note') != '') :
            if ($log == ''):
                $log = $log.' Added new comment <i>"'.$request->get('note').'"</i>';
            else:
                $log = ','.$log.' Added new comment <i>"'.$request->get('note').'"</i>';
            endif;
        endif;

        // for remarks
        $dt = Carbon::now();
        $dt = $dt->toDateString();
        //$user = Auth::user();
        $author = $request->get('note').' - '. Auth::user()->first_name.' <small>('.$dt.')</small>';
        if($request->get('note')==''){
            $author = '';
        }
        $note = $service->remarks;
        if($note!=''){
            if($request->get('note')!=''){
                $note = $note.'</br>'.$author;
            }
        }
        else{
            $note = $author;
        }

        $extend = $request->get('extend');
        $extend_dt = null;
        if($extend!=''){
            $extend_dt = date("Y-m-d", strtotime($extend));
        }

        $data = array(
            'cost'=> $request->get('cost'),
            'tip' => $request->get('tip'),
            //'charge' => $request->get('charge'),
            'remarks' => $note,
            'status' => $service_status,
            'active' => $request->get('active'),
            'extend' => $extend_dt,
            'rcv_docs' => $request->get('rcv_docs')
        );

        $oldStatus = ClientService::findOrfail($id)->status;
        $update_service = ClientService::find($id);
        $update_service->update($data);

        // Task Generator
        if($oldStatus == 'pending' && $service_status == 'on process') {
            TasksController::save([ClientService::findOrfail($id)]);
        }

        // Soft delete discount
        if($request->get('active') == 0) {
            ClientTransactions::where('client_id', $service->client_id)
                ->where('service_id', $id)
                ->where('tracking', $service->tracking)
                ->where('type', 'Discount')
                ->delete();
        } 
        // Restore discount
        elseif($request->get('active') == 1) {
            ClientTransactions::withTrashed()
                ->where('client_id', $service->client_id)
                ->where('service_id', $id)
                ->where('tracking', $service->tracking)
                ->where('type', 'Discount')
                ->restore();
        }

        if ($update_service) :
            $this->check_package_updates($service->tracking);
            if($activer1 == "Inactive"){
                $d = ClientTransactions::where('type', 'Discount')->where('service_id', $id)->first();
                if($d){
                    $d->delete();
                }
            }
            else{
                $d = ClientTransactions::withTrashed()->where('type', 'Discount')->where('service_id', $id)->first();
                if($d){
                    $d->restore();
                }
            }

            $group = Package::where('tracking', $service->tracking)->first();
            if($log != '') :
                if(substr($service->tracking,0,1)!="G"){
                    $new_log = 'Updated Service '.$service->detail.'. '.$log.' on Package '.$service->tracking.'.';
                    Common::saveActionLogs($service->id,$service->client_id,$new_log);
                }
                else{
                    $new_log = 'Log: Updated Client Service '.$id.' - '.$service->detail.'</br>'.$log;
                    Common::saveActionLogs($service->id,$service->client_id,$new_log,$group->group_id);
                }
            endif;
            //additional codes
            $client_id = $service->client_id;
            $personal_payment = $this->clientPayment($client_id);
            $personal_cost = $this->GetCompleteCost($client_id);
            $personal_discount = $this->clientDiscount($client_id);
            $personal_refund = $this->clientRefund($client_id);
            $personal_deposit = $this->clientDeposit($client_id);
            $personal_balance = $this->clientCompleteBalance($client_id);

            $notes =  ' : '.$discnotes .$translog1.'. ' . $translog2;
            $notes_cn =  ' : '.$discnotes_cn . $translog1_cn. '. ' . $translog2_cn;

            $total_balance = ((($this->groupDeposit($group->group_id)+ $this->groupPayment($group->group_id)+$this->groupDiscount($group->group_id))- $this->groupServiceCost($group->group_id)) - $this->groupRefund($group->group_id));
            //\Log::info($translog1);
                if($translog1 != '' || $translog2 != '' || $discnotes != ''){
                    $newVal = $oldVal - $newVal;
                    //$user = Auth::user();
                    if($service->status == 0 && $request->active == 1){
                        $newVal = '-'.$newVal;
                    }
                    $log_data = array(
                        'client_id' => $client_id,
                        'service_id' => $update_service->id,
                        'tracking' => $service->tracking,
                        'user_id' => Auth::user()->id,
                        'type' => 'service',
                        'amount' => $newVal,
                        'detail'=> $notes,
                        'detail_cn'=> $notes_cn,
                    );
                    if(substr($service->tracking,0,1)=="G"){
                        $log_data['group_id'] = $group->group_id;
                        $log_data['balance'] = $total_balance;
                    }
                    else{
                        $log_data['balance'] = $personal_balance;
                    }

                    //\Log::info($update_service->id);
                    if($service->status != 'complete' && $service_status == 'complete'){
                        $log_data['id'] = '';
                        $log_data['detail'] = 'Completed Service '.$notes;
                        $log_data['detail_cn'] = '完成的服务 '.$notes_cn;   
                        $log_data['amount'] = '-'.$newServiceCost;
                        // if($request->get('active') == 0){
                        //     $log_data['amount'] = '-'.$newServiceCost;
                        // }   
                        $update_log = TransactionLogs::where('service_id',$update_service->id)->where('type','service')->first();
                        if($update_log){
                            $log_data['old_detail'] = $update_log->detail.'<br><small>'.Auth::user()->first_name.' - '.$update_log->log_date.'</small><br><br>'.$update_log->old_detail;
                            $log_data['old_detail_cn'] = $update_log->detail_cn.'<br><small>'.Auth::user()->first_name.' - '.$update_log->log_date.'</small><br><br>'.$update_log->old_detail_cn;
                            $update_log->delete();
                        }
                        //else{
                            Common::saveTransactionLogs($log_data);
                        //}
                    }
                    else{
                        if(($service->status != 'complete' && $service_status != 'complete')){
                             $log_data['amount'] = 0;
                        }
                        if($service->status == 'complete' && $service_status != 'complete'){
                            $log_data['amount'] = '-'.$newServiceCost;
                        }
                        if($service->status == 'complete' && $service_status == 'complete'){
                            $log_data['amount'] = '-'.$newServiceCost;
                        }     
                        //$log_data['id'] = '';
                        $log_data['detail'] = 'Updated Service '.$notes;
                        $log_data['detail_cn'] = '服务更新 '.$notes;
                        $update_log = TransactionLogs::where('service_id',$update_service->id)->where('type','service')->first();

                        if($update_log){
                            $log_data['detail'] = 'Updated Service '.$notes;
                            $log_data['detail_cn'] = '服务更新 '.$notes_cn;
                            $log_data['old_detail'] = $update_log->detail.'<br><small>'.Auth::user()->first_name.' - '.$update_log->log_date.'</small><br><br>'.$update_log->old_detail;
                            $log_data['old_detail_cn'] = $update_log->detail_cn.'<br><small>'.Auth::user()->first_name.' - '.$update_log->log_date.'</small><br><br>'.$update_log->old_detail_cn;
                            $update_log->delete();
                        }
                        //else{
                            Common::saveTransactionLogs($log_data);
                        //}
                    }
                }
            $result = array('success' => true);
        else :
            $result = array('success' => true);
        endif;

        // // Document Logs
        // // Active
        // if($request->get('active') == 1) {
        //     $__clientService = ClientService::findOrfail($id);

        //     // Received
        //     $__newReceivedDocs = [];
        //     if($request->get('rcv_docs') != '' && $request->get('rcv_docs') != null) {
        //         $__newReceivedDocs = explode(',', $request->get('rcv_docs'));
        //     }

        //     $__documents = array_diff($__newReceivedDocs, $__currentReceivedDocs);
        //     if(count($__documents) > 0) { // There's a new entry
        //         // Save document log
        //         $__serviceName = Service::findOrfail($__clientService->service_id)->detail;

        //         $__type = 'Received';
        //         $__documents = implode(',', $__documents);
        //         $__detail = '<strong>Received document/s for service '.$__serviceName.':</strong>';
        //         $__detailCn = '<strong>Received document/s for service '.$__serviceName.':</strong>';
        //         $__documentsOnHand = implode(',', array_unique(array_merge($__newReceivedDocs, $__currentReceivedDocs)));

        //         if( $this->hasDocumentLogs($id) != 0 ) {
        //             DocumentLogsController::store($__clientService, $__type, $__documents, $__detail, $__detailCn, $__documentsOnHand);
        //         }
        //     }

        //     // Upon Completion
        //     // Except:
        //         // 70 9G Services 
        //         // 101 Bureau of Immigration Certificates
        //         // 230 Board of Investment (BOI)
        //         // 238 Cagayan Economic Zone Authority(CEZA) Working Visa
        //         // 259 Department of Justice (DOJ)
        //     if($uponCompletion && ($__clientService->parent_id != 70 && $__clientService->parent_id != 101 && $__clientService->parent_id != 230 && $__clientService->parent_id != 238 && $__clientService->parent_id != 259)) {
        //         // Get all service outcome documents
        //         $__serviceDocsReleased = Service::findOrfail($__clientService->service_id)->docs_released;
        //         if($__serviceDocsReleased == null || $__serviceDocsReleased == ''){
        //             $__serviceDocsReleased = null;
        //         }

        //         // Save document log
        //         if($__serviceDocsReleased != null) {
        //             $__serviceName = Service::findOrfail($__clientService->service_id)->detail;

        //             $__type = 'Received';
        //             $__documents = $__serviceDocsReleased;
        //             $__detail = '<strong>Received document/s for service '.$__serviceName.' upon completion:</strong>';
        //             $__detailCn = '<strong>Received document/s for service '.$__serviceName.' upon completion:</strong>';
        //             $__documentsOnHand = $__serviceDocsReleased;

        //             if( $this->hasDocumentLogs($id) != 0 ) {
        //                 DocumentLogsController::store($__clientService, $__type, $__documents, $__detail, $__detailCn, $__documentsOnHand);
        //             }
        //         }
        //     }

        //     // Released
        //     $__newReleasedDocs = [];
        //     if($request->get('rcv_docs_released') != '' && $request->get('rcv_docs_released') != null) {
        //         $__newReleasedDocs = explode(',', $request->get('rcv_docs_released'));
        //     }
        //     $__documents = array_diff($__newReleasedDocs, $__currentReleasedDocs);
        //     if(count($__documents) > 0) { // There's a new entry
        //         // Save document log
        //         $__serviceName = Service::findOrfail($__clientService->service_id)->detail;

        //         $__type = 'Released';
        //         $__documents = implode(',', $__documents);
        //         $__detail = '<strong>Released document/s for service '.$__serviceName.':</strong>';
        //         $__detailCn = '<strong>Released document/s for service '.$__serviceName.':</strong>';

        //         $__currentDocumentsOnHand = [];
        //         $__documentLog = DocumentLogs::where('client_service_id', $id)->orderBy('id', 'desc')->first();
        //         if($__documentLog) {
        //             $__currentDocumentsOnHand = explode(',', $__documentLog->documents_on_hand);
        //         }
        //         $__documentsOnHand = implode(',', array_diff($__currentDocumentsOnHand, array_merge($__newReleasedDocs, $__currentReleasedDocs)));
        //         if($__documentsOnHand == null || $__documentsOnHand == '') { $__documentsOnHand = null; }

        //         if( $this->hasDocumentLogs($id) != 0 ) {
        //             DocumentLogsController::store($__clientService, $__type, $__documents, $__detail, $__detailCn, $__documentsOnHand);
        //         }
        //     }

        // } elseif($request->get('active') == 0) { // Inactive
        //     $__clientService = ClientService::findOrfail($id);

        //     if($__clientService->rcv_docs != '' && $__clientService->rcv_docs != null) {
        //         $__documents = $__clientService->rcv_docs;

        //         $__clientService->update([
        //             'rcv_docs' => null,
        //             'rcv_docs_released' => null,
        //             'released' => 0
        //         ]);

        //         // Save document log
        //         $__serviceName = Service::findOrfail($__clientService->service_id)->detail;

        //         $__type = 'Released';
        //         $__detail = '<strong>Released document/s for service '.$__serviceName.' upon deactivation:</strong>';
        //         $__detailCn = '<strong>Released document/s for service '.$__serviceName.' upon deactivation:</strong>';
        //         $__documentsOnHand = null;

        //         if( $this->hasDocumentLogs($id) != 0 ) {
        //             DocumentLogsController::store($__clientService, $__type, $__documents, $__detail, $__detailCn, $__documentsOnHand);
        //         }
        //     }
        // }
        // // End of Document Logs

        // ******************** Document Tracker ******************** //
            if( DocumentLogsController::hasDocumentLogs($update_service->id) ) {
            
                if($request->get('active') == 0) {
                    $update_service->update([
                        'rcv_docs' => null,
                        'rcv_docs_released' => null,
                        'report_docs' => null
                    ]);

                    DocumentLogsController::updatePackage($update_service->tracking);

                    DocumentLogsController::cancelClientServices([$update_service]);
                } elseif($request->get('active') == 1) {
                    if( $request->status == 'pending' || $request->status == 'on process' ) {
                        $newDocs = ($request->rcv_docs != '' && $request->rcv_docs != null) 
                            ? explode(',', $request->rcv_docs) 
                            : [];
                        $oldDocs = ($service->rcv_docs != '' && $service->rcv_docs != null)
                            ? explode(',', $service->rcv_docs)
                            : [];

                        $update_service->update([
                            'rcv_docs' => $request->rcv_docs,
                            'status' => DocumentLogsController::getClientServiceStatus(
                                $update_service->service_id, 
                                $request->rcv_docs
                            )     
                        ]);

                        DocumentLogsController::updatePackage($update_service->tracking);

                        $added = array_unique(array_diff($newDocs, $oldDocs));
                        if( count($added) > 0 ) {
                            $originsArray = DocumentLogsController::getOrigins(
                                $added,
                                $client_id,
                                $update_service->id
                            );

                            DocumentLogsController::saveOrigins($originsArray, $update_service->id);

                            DocumentLogsController::share([$update_service->client_id]);
                        }

                        $subtracted = array_unique(array_diff($oldDocs, $newDocs));
                        if( count($subtracted) > 0 ) {
                            $_clientId = $update_service->client_id;
                            $dl_detail = 'Removed document/s from client:';

                            ClientServiceDocument::whereHas('clientService', function($query) use($_clientId) {
                                $query->where('client_id', $_clientId);
                            })
                            ->whereIn('document_id', $subtracted)
                            ->delete();

                            DocumentLogs::create([
                                'client_service_id' => $update_service->id,
                                'documents' => implode(',', $subtracted),
                                'documents_on_hand' => implode(',', $newDocs),
                                'processor_id' => Auth::user()->id,
                                'detail' => $dl_detail,
                                'detail_cn' => $dl_detail,
                                'log_date' => Carbon::now()->format('Y-m-d')
                            ]);

                            DocumentLogsController::removeDocuments(
                                $update_service->client_id, 
                                $subtracted, 
                                $update_service->id
                            );
                        }
                    }

                    elseif( $request->status == 'complete' ) {
                        $newDocs = $request->rcv_docs_released;
                        $oldDocs = ($service->rcv_docs_released != '' && $service->rcv_docs_released != null)
                            ? explode(',', $service->rcv_docs_released)
                            : [];

                        $update_service->update([
                            'rcv_docs_released' => (count($request->rcv_docs_released) > 0)
                                ? implode(',', $request->rcv_docs_released)
                                : null
                        ]);

                        DocumentLogsController::updatePackage($update_service->tracking);

                        $added = array_unique(array_diff($newDocs, $oldDocs));
                        if( count($added) > 0 ) {
                            // $dl_detail = 'Released document/s to client for completed service <strong style="color:orange;">'.$translated->detail.'</strong>:';

                            $documentLog = DocumentLogs::where('client_service_id', $update_service->id)->orderBy('id', 'desc')->first();

                            $documentsOnHand = array_unique(
                                array_diff(
                                    explode(',', $documentLog->documents_on_hand),
                                    $added
                                )
                            );

                            $dl_detail = 'Released document/s to client for completed service:';

                            $_clientId = $update_service->client_id;

                            ClientServiceDocument::whereHas('clientService', function($query) use($_clientId) {
                                $query->where('client_id', $_clientId);
                            })
                            ->whereIn('document_id', $added)
                            ->delete();

                            DocumentLogs::create([
                                'client_service_id' => $update_service->id,
                                'documents' => implode(',', $added),
                                'documents_on_hand' => (count($documentsOnHand) > 0)
                                    ? implode(',', $documentsOnHand)
                                    : null,
                                'processor_id' => Auth::user()->id,
                                'detail' => $dl_detail,
                                'detail_cn' => $dl_detail,
                                'log_date' => Carbon::now()->format('Y-m-d')
                            ]);

                            DocumentLogsController::removeDocuments(
                                $update_service->client_id, 
                                $added, 
                                $update_service->id,
                                'complete'
                            );

                            DocumentLogsController::releaseDocuments(
                                $update_service->client_id, 
                                $added, 
                                $update_service->id
                            );
                        }
                    }
                }

            }
        // ******************** End of Document Tracker ******************** //
            
        return json_encode($result);
    }

    public function clientGetService($id) {
            $service = DB::connection('visa')
            ->table('client_services as a')
            ->select(DB::raw('a.*,b.amount,b.reason, b.deleted_at, c.parent_id,c.months_required'))
            ->leftJoin('client_transactions AS b', function($join){
                            $join->on('b.service_id', '=', 'a.id')
                            ->where('b.type', '=', 'Discount');
                    })
            ->leftjoin(DB::raw('(select * from services) as c'), 'c.id', '=', 'a.service_id')
            // ->leftjoin(DB::raw('(select * from client_discounts) as b'), 'b.service_id', '=', 'a.id')
            ->where('a.id', $id)
            ->first();

        $docs = DocumentLogsController::getDocs($service->service_id);

        $service->required_docs = $docs['requiredDocsArray'];
        $service->any_acr_icard_required_docs_array = $docs['anyAcrIcardRequiredDocsArray'];
        $service->optional_docs_array = $docs['optionalDocsArray'];
        $service->any_acr_icard_optional_docs_array = $docs['anyAcrIcardOptionalDocsArray'];

        $reportDocs = explode(',', $service->report_docs);

        // outcome_optional_docs
            $outcomeOptionalDocsArray = $docs['outcomeOptionalDocsArray'];
            $anyAcrIcardOutcomeOptionalDocsArray = $docs['anyAcrIcardOutcomeOptionalDocsArray'];

            $temp = $docs['outcomeDocsArray'];
            $outcomeDocsArray = [];
            foreach($temp as $t) {
                $outcomeDocsArray[] = $t->id;
            }

            $outcomeOptionalDocs = [];
            foreach($outcomeOptionalDocsArray as $o) {
                if( in_array($o->id, $reportDocs) && !in_array($o->id, $outcomeDocsArray) ) {
                    $reportDocs = array_diff($reportDocs, [$o->id]);
                    $outcomeOptionalDocs[] = [
                        'id' => $o->id,
                        'title' => $o->title
                    ];
                }
            }
            foreach($anyAcrIcardOutcomeOptionalDocsArray as $o) {
                if( in_array($o->id, $reportDocs) && !in_array($o->id, $outcomeDocsArray) ) {
                    $reportDocs = array_diff($reportDocs, [$o->id]);
                    $outcomeOptionalDocs[] = [
                        'id' => $o->id,
                        'title' => $o->title
                    ];
                }
            }
            $service->outcome_optional_docs = $outcomeOptionalDocs;

        // outcome_docs
            $service->outcome_docs = DB::connection('visa')->table('service_documents')
                ->whereIn('id', $reportDocs)->get();
                
        return response()->json($service);
    }

    public function clientActionLogs($client_id){
        $action =  ActionLogs::with('user')->where('client_id', $client_id)->where('group_id',0)->orderBy('id', 'desc')->get();
        $arraylogs = [];
        $month = null;
        $day = null;
        $year = null;

        foreach($action as $a){
            $cdate = Carbon::parse($a->log_date)->format('M d Y');
            $dt = explode(" ", $cdate);
            $m = $dt[0];
            $d = $dt[1];
            $y = $dt[2];
            if($y == $year){
                $y = null;
                if($m == $month && $d == $day){
                    $m = null;
                    $d = null;
                }
                else{
                    $month = $m;
                    $day = $d;
                }
            }
            else{
                $year = $y;
                $month = $m;
                $day = $d;
            }

            $arraylogs[] = array(
                'month' => $m,
                'day' => $d,
                'year' => $y,
                'data' => array ( // SELLER'S DETAILS
                    'title' => $a->details,
                    'processor' => $a->user->full_name,
                    'date' => Carbon::parse($a->log_date)->format('F d,Y'),
                )
            );
        }
        // \Log::info($arraylogs);
        return json_encode($arraylogs);

    }

    public function clientCommissionLogs($client_id){
        $transaction =  CommissionLogs::with('user')
        ->where('client_id', $client_id)
        ->where('display',0)
        ->orderBy('id', 'desc')->get();
        

        $arraylogs = [];
        $month = null;
        $day = null;
        $year = null;
        
        foreach($transaction as $a){
            $totalClientCommission = CommissionLogs::select(DB::raw("SUM(commission) as total"))
                        ->where('display',0)
                        ->where('client_id',$a->client_id)
                        ->where('type',$a->type)
                        ->where('id','<=', $a->id) 
                        ->first()->total;
            $client_last_record  =  CommissionLogs::select('commission')
                        ->where('display',0)
                        ->where('client_id',$a->client_id)
                        ->where('type',$a->type)
                        ->where('id','<',$a->id)
                        ->orderby('id', 'desc')->first();

            // $a->balance = $currentBalance;
            // $currentBalance -= $a->amount;
            if($client_last_record['commission']==null){
                 $client_last_record['commission'] = $totalClientCommission;
            }
            $cdate = Carbon::parse($a->log_date)->format('M d Y');
            $dt = explode(" ", $cdate);
            $m = $dt[0];
            $d = $dt[1];
            $y = $dt[2];
            if($y == $year){
                $y = null;
                if($m == $month && $d == $day){
                    $m = null;
                    $d = null;
                }
                else{
                    $month = $m;
                    $day = $d;
                }
            }
            else{
                $year = $y;
                $month = $m;
                $day = $d;
            }
           
            $arraylogs[] = array(
                'month' => $m,
                'day' => $d,
                'year' => $y,
                'data' => array ( // SELLER'S DETAILS
                    'id' => $a->id,
                    'title' => $a->detail_client,
                    'commission' => $totalClientCommission,
                    'prevcommission' => floatval($totalClientCommission)-floatval($client_last_record['commission']),
                    'type' => $a->type,
                    'processor' => $a->user->full_name,
                    'date' => Carbon::parse($a->log_date)->format('F d,Y'),
                )
            );
        }
        //\Log::info(json_encode($arraylogs));
        return json_encode($arraylogs);
    }


    public function clientTransactionLogs($client_id){
        $transaction =  TransactionLogs::where('client_id', $client_id)->where('group_id',0)->where('display',0)->orderBy('id', 'desc')->get();

        // $transaction = TransactionLogs::with('user')->whereHas('clientService', function($query) use($client_id) {
        //     $query->where('client_id', $client_id);
        // })
        // ->where('group_id',0)->where('display',0)
        // ->orderBy('id', 'desc')->get();
        // \Log::info($transaction);

        $arraylogs = [];
        $month = null;
        $day = null;
        $year = null;
        $currentBalance = $this->clientCompleteBalance($client_id);
        foreach($transaction as $a){
            $usr =  User::where('id',$a->user_id)->select('id','first_name','last_name')->limit(1)->get()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);

            $cs = ClientService::where('id',$a->service_id)->first();

            $a->balance = $currentBalance;
            $currentBalance -= $a->amount;
            $cdate = Carbon::parse($a->log_date)->format('M d Y');
            $dt = explode(" ", $cdate);
            $m = $dt[0];
            $d = $dt[1];
            $y = $dt[2];
            if($y == $year){
                $y = null;
                if($m == $month && $d == $day){
                    $m = null;
                    $d = null;
                }
                else{
                    $month = $m;
                    $day = $d;
                }
            }
            else{
                $year = $y;
                $month = $m;
                $day = $d;
            }

            if($cs){
                $csdetail = $cs->detail;
                $cstracking =  $cs->tracking;
                $csstatus =  $cs->status;
                $csactive =  $cs->active;
            }
            else{
                $csdetail = ucfirst($a->type);
                $cstracking = '';
                $csstatus = '';
                $csactive = 'none';
            }

            $arraylogs[] = array(
                'month' => $m,
                'day' => $d,
                'year' => $y,
                'data' => array ( 
                    'id' => $a->id,
                    'title' => $a->detail,
                    'balance' => $a->balance,
                    'prevbalance' => $currentBalance,
                    'amount' => $a->amount,
                    'type' => $a->type,
                    'processor' => $usr[0]->first_name,
                    'old_detail' => $a->old_detail,
                    'date' => Carbon::parse($a->log_date)->format('F d,Y'),
                    'service_name' => $csdetail,
                    'tracking' => $cstracking,
                    'status' => $csstatus,
                    'active' => $csactive,

                )
            );
        }
        //\Log::info($arraylogs);
        return json_encode($arraylogs);
    }

    public function clientDocumentLogs($clientId) {
        $documentLogs = DocumentLogs::with('processor')->whereHas('clientService', function($query) use($clientId) {
            $query->where('client_id', $clientId);
        })
        ->where('document_id', null)
        ->orderBy('id', 'desc')->get();

        $arraylogs = [];
        $month = null;
        $day = null;
        $year = null;

        foreach($documentLogs as $a){
            $cdate = Carbon::parse($a->log_date)->format('M d Y');
            $dt = explode(" ", $cdate);
            $m = $dt[0];
            $d = $dt[1];
            $y = $dt[2];
            if($y == $year){
                $y = null;
                if($m == $month && $d == $day){
                    $m = null;
                    $d = null;
                }
                else{
                    $month = $m;
                    $day = $d;
                }
            }
            else{
                $year = $y;
                $month = $m;
                $day = $d;
            }

            $arraylogs[] = array(
                'month' => $m,
                'day' => $d,
                'year' => $y,
                'data' => array (
                    'detail' => (\Request::cookie('locale') == 'en') ? $a->detail : $a->detail_cn,
                    'documents_on_hand' => $a->custom_documents_on_hand,
                    'processor' => $a->processor->full_name
                )
            );
        }

        return json_encode($arraylogs);
    }

    public function clientDocumentLogsDetails($clientId) {
        $documentLogs = DocumentLogs::whereHas('clientService', function($query) use($clientId) {
                $query->where('client_id', $clientId)->where('released', 0)->where('active', 1);
            })
            ->with('clientService.service_category')
            ->where('document_id', null)
            ->orderBy('id', 'desc')
            ->get()->makeHidden([
                'documents_on_hand_string', 
                'clientService.detail_cn',
                'clientService.docs_needed',
                'clientService.docs_optional',
                'clientService.discount_amount',
                'clientService.group_binded',
                'clientService.parent_id',
            ]);

        $data = [];
        $doneIds = [];
        foreach($documentLogs as $dl) {
            if(!in_array($dl->client_service_id, $doneIds)) {
                $doneIds[] = $dl->client_service_id;

                $documentsOnHand = $this->getDocumentsOnHandToString($this->getDocumentsOnHand($dl->clientService->client_id, $dl->clientService->service_id));

                $data[] = [
                    'service' => (\Request::cookie('locale') == 'en')
                        ? $dl->clientService->service_category->detail
                        : $dl->clientService->service_category->detail_cn,
                    'status' => $dl->clientService->status,
                    'documents_on_hand' => ($documentsOnHand) ? $documentsOnHand : 'N/A'
                ];
            }
        }

        return $data;
    }

    private function checkPackageComplete($pack_id){
        $package_complete = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%complete%')
            ->count();

        return $package_complete;
    }

    private function checkPackagePending($pack_id){
        $package_pending = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%pending%')
            ->count();

        return $package_pending;
    }

    private function checkPackageOnProcess($pack_id){
        $package_onprogress = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%on process%')
            ->count();

        return $package_onprogress;
    }

    private function check_package_updates($pack_id){
        $stat = 0; // empty

        if($this->checkPackageComplete($pack_id)>0){
            $stat = 4; // complete
        }
        if($this->checkPackageOnProcess($pack_id)>0){
            $stat = 1; // process
        }
        if($this->checkPackagePending($pack_id)>0){
            $stat = 2; // pending
        }

        $data = array('status' => $stat);

        DB::connection('visa')
            ->table('packages')
            ->where('tracking', $pack_id)
            ->update($data);
    }

    public function load_client_list(Request $request = null) {

        $paths = storage_path('/app/public/data');
        if (!File::exists($paths)){
          Storage::put($paths.'/Clients.txt', '');
        }
        $myfile = fopen($paths.'/Clients.txt', 'w') or die('Unable to open file!');
        $txt = "{\n";
        fwrite($myfile, $txt);
        $txt = '"data":[';
        fwrite($myfile, $txt);
        $txt = "\n";
        fwrite($myfile, $txt);
        $count = 0;

        $records = DB::connection('mysql')
            ->table('users as a')
            ->select(DB::raw('
                a.*,
                total.total_amount as total_costs,
                total.service_date,
                total6.dates,total6.dateshid, role.role_id,
                srv.sdates,
                srv.sdateshid,
                total2.total_deposit,
                total2.total_refund,
                total2.total_discount,
                total2.total_payment'))
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(sum(b.cost)+sum(b.charge)+sum(b.tip)+sum(b.com_client)+sum(b.com_agent),0) as total_amount,
                        COALESCE(sum(b.tip),0) as total_charge,
                        b.client_id, b.service_date
                        from visa.client_services as b
                        where b.active = 1
                        and b.group_id = 0
                        group by b.client_id
                        order by b.service_date desc
                    ) as total'),
                    'total.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  SUM(IF(c.type = "Deposit", c.amount, 0)) as total_deposit,
                        SUM(IF(c.type = "Refund", c.amount, 0)) as total_refund,
                        SUM(IF(c.type = "Payment", c.amount, 0)) as total_payment,
                        SUM(IF(c.type = "Discount", IF(c.deleted_at = null, c.amount,0), 0)) as total_discount,
                        c.client_id
                        from visa.client_transactions as c
                        where c.group_id = 0
                        group by c.client_id
                    ) as total2'),
                    'total2.client_id', '=', 'a.id')
                // ->leftjoin(DB::raw('
                //     (
                //         Select  IFNULL(sum(c.deposit_amount),0) as total_deposit,
                //         c.client_id
                //         from visa.client_deposits as c
                //         where c.group_id = 0
                //         group by c.client_id
                //     ) as total2'),
                //     'total2.client_id', '=', 'a.id')
                // ->leftjoin(DB::raw('
                //     (
                //         Select  IFNULL(sum(d.payment_amount),0) as total_payment,
                //         d.client_id
                //         from visa.client_payments as d
                //         where d.group_id = 0
                //         group by d.client_id
                //     ) as total3'),
                //     'total3.client_id', '=', 'a.id')
                // ->leftjoin(DB::raw('
                //     (
                //         Select  IFNULL(sum(e.refund_amount),0) as total_refund,
                //         e.client_id
                //         from visa.client_refunds as e
                //         where e.group_id = 0
                //         group by e.client_id
                //     ) as total4'),
                //     'total4.client_id', '=', 'a.id')
                // ->leftjoin(DB::raw('
                //     (
                //         Select  IFNULL(sum(f.discount_amount),0) as total_discount,
                //         f.client_id
                //         from visa.client_discounts as f
                //         where f.group_id = 0
                //         group by f.client_id
                //     ) as total5'),
                //     'total5.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select *
                        from role_user as r
                        where r.role_id = 9
                    ) as role'),
                    'role.user_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as dates,
                        x.client_id,date_format(max(x.dates),"%Y-%m-%d %H:%i:%s") as dateshid
                        from( SELECT STR_TO_DATE(log_date, "%M %e, %Y, %l:%i %p") as dates,
                            client_id, status
                            FROM visa.packages
                            ORDER BY dates desc
                        ) as x
                        group by x.client_id) as total6'),
                    'total6.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as sdates,
                        date_format(max(cs.servdates),"%Y:%m:%d") as sdateshid,cs.client_id
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM visa.client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.client_id) as srv'),
                    'srv.client_id', '=', 'a.id')
                // ->orderBy(DB::raw('
                //     (
                //         (case when total.total_amount > 0 then total.total_amount else 0 end)
                //         - (
                //             (
                //                 case when total2.total_deposit > 0 then total2.total_deposit else 0 end
                //                 + case when total2.total_payment > 0 then total2.total_payment else 0 end
                //                 + case when total2.total_discount > 0 then total2.total_discount else 0 end
                //             )
                //             - case when total2.total_refund > 0 then total2.total_refund else 0 end
                //         ) 
                //     )'), 'desc')
                ->where("role.role_id","9");

        if($request && $request->branch != 0) {
            $records = $records->where(function ($query) use($request) {
                            $query->where('branch_id', $request->branch)
                                  ->orwhere('branch_id', '3');
                        })->get();
            //->where('branch_id', $request->branch)->get();
        } else {
            $records = $records->get();
        }

        //\Log::info($records);
        $collect = 0;

        foreach ($records as $value) {

                $col_balance = 0;
                $total_balance = 0;

                $id = $value->id;
                // if($id >= 1000 && $id <= 1500){
                //     $disc = $this->clientDiscount($id);
                //     $total_balance =  (
                //             (
                //                 $value->total_deposit
                //                 + $value->total_payment
                //                 + $disc
                //             )
                //             - (
                //                 $value->total_refund
                //                 + $value->total_costs
                //             )
                //         );
                // }
                // else{
                    $total_balance = floatval($value->balance);
                //}

                if($total_balance<0){
                    // if($id >= 1000 && $id <= 1500){
                    //     $col_balance = (
                    //         (
                    //             $value->total_deposit
                    //             + $value->total_payment
                    //             + $disc
                    //         )
                    //         - (
                    //             $value->total_refund
                    //             + ($this->GetCompleteCost($id))
                    //         )
                    //     );
                    // }
                    //else{
                        $col_balance = floatval($value->collectable);
                    //}

                    if($col_balance > 0){
                        $col_balance = 0;
                    }
                    else{
                        $collect += $col_balance;
                    }
                }

                $count++;
                $txt = "[";
                fwrite($myfile, $txt);
                $txt = '"'.$value->id.'",';
                fwrite($myfile, $txt);
                // $txt = '"'.ucwords($value->first_name).' '.ucwords($value->last_name).'",';
                if($value->risk == 'High-Risk'){
                    $txt = '"<span style=\"color:red;\" id=\"cl_'.$value->id.'\"><b>'.ucwords($value->first_name).' '.ucwords($value->last_name).'</b></span>",';
                }
                else{
                    $txt = '"<span id=\"cl_'.$value->id.'\"><b>'.ucwords($value->first_name).' '.ucwords($value->last_name).'</b></span>",';
                }


                // fwrite($myfile, $txt);

                // $txt = '"'.Common::formatMoney($value->total_costs*1).'",';
                // fwrite($myfile, $txt);

                // $txt = '"'.Common::formatMoney($value->total_deposit*1).'",';
                // fwrite($myfile, $txt);

                // $txt = '"'.(Common::formatMoney($value->total_payment*1)).'",';
                // fwrite($myfile, $txt);

                // $txt = '"'.(Common::formatMoney($value->total_refund*1)).'",';
                // fwrite($myfile, $txt);

                // $txt = '"'.(Common::formatMoney($value->total_discount*1)).'",';
                fwrite($myfile, $txt);

                if($total_balance==0){
                $txt = '"'.(Common::formatMoney($total_balance)).'",';
                }
                else{
                $txt = '"'.(Common::formatMoney($total_balance*1)).'",';
                }

                fwrite($myfile, $txt);
                if($col_balance==0){
                $txt = '"'.(Common::formatMoney($col_balance)).'",';
                }
                else{
                $txt = '"'.(Common::formatMoney($col_balance*1)).'",';
                }

                // updating of user balance and collectables
                // if($disc > 0){

                // User::where('id', $value->id)
                //  ->update([
                //     'balance' => ($total_balance), 
                //     'collectable' => $col_balance
                // ]);
                // }


                fwrite($myfile, $txt);
                $txt = '"<span style=\"display:none;\">'.$value->dateshid.'--</span>'.$value->dates.'",';

                fwrite($myfile, $txt);
                $txt = '"<span style=\"display:none;\">'.$value->sdateshid.'--</span>'.$value->sdates.'",';

                fwrite($myfile, $txt);
                $txt = '"<center><a href=\"#\" id=\"cs_'.$value->id.'\" client-id=\"'.$value->id.'\" client-status=\"'.$value->risk.'\" class=\"changeStatus\" data-toggle=\"tooltip\" title=\"Change Status\" style=\"margin-right:8px;\"><i class=\"fa fa-bell fa-1x\"></i></a><a href=\"/visa/client/'.$value->id.'\" target=\"_blank\" data-toggle=\"tooltip\" title=\"View Client\"><i class=\"fa fa-arrow-right fa-1x\"></i></a></center>"';
                fwrite($myfile, $txt);
                if(count($records)==$count){
                    $txt = "]\n";
                }else{
                    $txt = "],\n";
                }
                fwrite($myfile, $txt);
            }
            $txt = "\n]\n}";
            fwrite($myfile, $txt);
            fclose($myfile);
            return array(true, $collect);
        }

    public function services(Request $request, $clientId) {
        if($request->option == 'client-to-group') {
            $groupId = 0;
        } elseif($request->option == 'group-to-client') {
            $groupId = $request->groupId;
        }

        return ClientService::where('client_id', $clientId)
                ->where('active', 1)
                ->where('group_id', $groupId)
                ->get();
    }

    public function packages(Request $request, $clientId) {
        if($request->option == 'client-to-group') {
            $groupId = $request->groupId;
        } elseif($request->option == 'group-to-client') {
            $groupId = 0;
        }

        return Package::where('client_id', $clientId)
            // ->where('status', '<>', 4)
            ->where('group_id', $groupId)
            ->get();
    }


    public function typeahead(){

        $serts = $_GET['query'];
        $cids = User::where("contact_number",'LIKE', '%' . $serts .'%')->pluck('id');
        if(preg_match('/\s/',$serts)){
            $q = explode(" ", $serts);
            $q1 = '';
            $q2 = '';
            $spaces = substr_count($serts, ' ');
            if($spaces == 2){
                $q1 = $q[0]." ".$q[1];
                $q2 = $q[2];
            }
            if($spaces == 1){
                $q1 = $q[0];
                $q2 = $q[1];
            }
            $prods = DB::connection()
            ->table('users as a')
            ->select(DB::raw('
                a.id,a.first_name,a.last_name,a.branch_id,a.date_register,srv.sdates,srv.sdates2, srv.checkyear'))
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%m/%d/%Y") as sdates, date_format(max(cs.servdates),"%Y%m%d") as sdates2 ,date_format(max(cs.servdates),"%Y") as checkyear ,cs.client_id
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM visa.client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.client_id
                        order by cs.servdates) as srv'),
                    'srv.client_id', '=', 'a.id')
                // ->orwhere('a.id','LIKE', '%' . $serts .'%')
                // ->orwhere('first_name','=',$serts)
                // ->orwhere('last_name','=',$serts)
                ->orwhere(function ($query) use($q1,$q2) {
                        $query->where('first_name', '=', $q1)
                              ->Where('last_name', '=', $q2);
                    })->orwhere(function ($query) use($q1,$q2) {
                        $query->where('last_name', '=', $q1)
                              ->Where('first_name', '=', $q2);
                    })
                ->orderBy('sdates2','DESC')    
                ->get();
        }
        else{
            $prods = DB::connection()
            ->table('users as a')
            ->select(DB::raw('
                a.id,a.first_name,a.last_name,a.branch_id,a.date_register,srv.sdates,srv.sdates2, srv.checkyear'))
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%m/%d/%Y") as sdates, date_format(max(cs.servdates),"%Y%m%d") as sdates2, date_format(max(cs.servdates),"%Y") as checkyear ,cs.client_id
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM visa.client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.client_id) as srv'),
                    'srv.client_id', '=', 'a.id')
                // ->orwhere('a.id','LIKE', '%' . $serts .'%')
                ->orwhereIn('id',$cids)
                ->orwhere('first_name','=',$serts)
                ->orwhere('last_name','=',$serts)
                ->orderBy('sdates2','DESC')    
                ->get();
            // \Log::info($prods);
            // \Log::info($prods->count());
            if($prods->count() == 0){
                preg_match_all('!\d+!', $serts, $matches);
                $serts = implode("", $matches[0]);
                $serts = ltrim($serts,"0");
                $serts = ltrim($serts,'+');
                $serts = ltrim($serts,'63');
                $cids = User::where("contact_number",'LIKE', '%' . $serts .'%')->pluck('id');

                $prods = DB::connection()
                    ->table('users as a')
                    ->select(DB::raw('
                        a.id,a.first_name,a.last_name,a.branch_id,a.date_register,srv.sdates,srv.sdates2, srv.checkyear'))
                        ->leftjoin(DB::raw('
                            (
                                Select date_format(max(cs.servdates),"%m/%d/%Y") as sdates, date_format(max(cs.servdates),"%Y%m%d") as sdates2, date_format(max(cs.servdates),"%Y") as checkyear ,cs.client_id
                                from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                                    group_id, active,client_id
                                    FROM visa.client_services
                                    ORDER BY servdates desc
                                ) as cs
                                where cs.active = 1
                                group by cs.client_id) as srv'),
                            'srv.client_id', '=', 'a.id')
                        // ->orwhere('a.id','LIKE', '%' . $serts .'%')
                        ->orwhereIn('id',$cids)
                        ->orwhere('first_name','=',$serts)
                        ->orwhere('last_name','=',$serts)
                        ->orderBy('sdates2','DESC')    
                        ->get();
            }
        }

        $json = [];
        foreach($prods as $p){
           if($p->checkyear >= 2016){
              $br = Branch::where('id',$p->branch_id)->first()->name;
              $json[] = array(
                  'id' => $p->id,
                  //'name' => $p->id." -- ".$p->sdates."",
                  'name' => $p->first_name." ".$p->last_name." -- [".$br."] -- ".$p->sdates."",
              );
           }
           if($p->checkyear == null){
              $br = Branch::where('id',$p->branch_id)->first()->name;
              $json[] = array(
                  'id' => $p->id,
                  //'name' => $p->id." -- ".$p->sdates."",
                  'name' => $p->first_name." ".$p->last_name." -- [".$br."] -- No Service",
              );
           } 
        }
        return json_encode($json);
    }

    public function transferBalance($branch_id = 1, $client_id = null){

        $serts = $_GET['query'];
        //dd($serts);
        $cids = User::where("contact_number",'LIKE', '%' . $serts .'%')->pluck('id');
        if(preg_match('/\s/',$serts)){
            $q = explode(" ", $serts);
            $q1 = '';
            $q2 = '';
            $spaces = substr_count($serts, ' ');
            if($spaces == 2){
                $q1 = $q[0]." ".$q[1];
                $q2 = $q[2];
            }
            if($spaces == 1){
                $q1 = $q[0];
                $q2 = $q[1];
            }
            $prods = DB::connection()
            ->table('users as a')
            ->select(DB::raw('
                a.id,a.first_name,a.last_name,a.branch_id,a.date_register,role.role_id'))
            ->leftjoin(DB::raw('
                    (
                        Select *
                        from role_user as r
                        where r.role_id = 9
                    ) as role'),
                    'role.user_id', '=', 'a.id')
                ->orwhere('a.id','LIKE', '%' . $serts .'%')
                ->orwhere(function ($query) use($q1,$q2) {
                        $query->where('first_name', '=', $q1)
                              ->where('last_name', '=', $q2);
                    })->orwhere(function ($query) use($q1,$q2) {
                        $query->where('last_name', '=', $q1)
                              ->where('first_name', '=', $q2);
                    })
                ->get();
        }
        else{
            $prods = DB::connection()
            ->table('users as a')
            ->select(DB::raw('
                a.id,a.first_name,a.last_name,a.branch_id,a.date_register, role.role_id'))
            ->leftjoin(DB::raw('
                    (
                        Select *
                        from role_user as r
                        where r.role_id = 9
                    ) as role'),
                    'role.user_id', '=', 'a.id')
                ->orwhere('a.id','LIKE', '%' . $serts .'%')
                ->orwhere('first_name','=',$serts)
                ->orwhere('last_name','=',$serts)
                ->orwhereIn('id',$cids)
                ->get();
        }

        $prods = $prods->filter(function($item) use ($branch_id, $client_id)
        {
            if($item->branch_id == $branch_id && $item->role_id == '9' && $item->id != $client_id){
                return true;
            }
        });

        $json = [];
        foreach($prods as $p){
          $json[] = array(
              'id' => $p->id,
              'name' => "[".$p->id."] ". $p->first_name." ".$p->last_name,
          );
        }
        return json_encode($json);
    }

    public function groupTransferBalance($branch_id = 1, $group_id = null){

        $serts = $_GET['query'];
        $users = '';
        if(preg_match('/\s/',$serts)){
            $q = explode(" ", $serts);
            $q1 = '';
            $q2 = '';
            $spaces = substr_count($serts, ' ');
            if($spaces == 2){
                $q1 = $q[0]." ".$q[1];
                $q2 = $q[2];
            }
            if($spaces == 1){
                $q1 = $q[0];
                $q2 = $q[1];
            }
            $users = DB::connection()
            ->table('users as a')
            ->select(DB::raw('
                a.id,a.first_name,a.last_name,a.date_register'))
                ->orwhere('a.id','LIKE', '%' . $serts .'%')
                // ->orwhere('first_name','=',$serts)
                // ->orwhere('last_name','=',$serts)
                ->orwhere(function ($query) use($q1,$q2) {
                        $query->where('first_name', '=', $q1)
                              ->Where('last_name', '=', $q2);
                    })->orwhere(function ($query) use($q1,$q2) {
                        $query->where('last_name', '=', $q1)
                              ->Where('first_name', '=', $q2);
                    })
                ->pluck('id');
        }
         else{
            $users = DB::connection()
            ->table('users as a')
            ->select(DB::raw('
                a.id,a.first_name,a.last_name,a.date_register'))
                ->orwhere('a.id','LIKE', '%' . $serts .'%')
                ->orwhere('first_name','=',$serts)
                ->orwhere('last_name','=',$serts)
                ->pluck('id');
        }

        $getLeaderIds = GroupMember::where('leader',1)->pluck('client_id');
        $cids = User::whereIn('id',$getLeaderIds)->where("contact_number",'LIKE', '%' . $serts .'%')->pluck('id');

            $grps = DB::connection('visa')
            ->table('groups as a')
            ->select(DB::raw('
                a.id,a.name,a.branch_id'))
                //->orwhere('a.id','LIKE', '%' . $serts .'%')
                ->orwhere('name','LIKE', '%' . $serts .'%')
                ->orwhereIn('leader',$cids)
                ->orwhereIn('leader',$users)
                ->get();

        $grps = $grps->filter(function($item) use ($branch_id,$group_id)
        {
            if($item->branch_id == $branch_id && $item->id != $group_id){
                return true;
            }
        });


        $json = [];
        foreach($grps as $p){
          $json[] = array(
              'id' => $p->id,
              'name' => $p->name,
          );
        }
        return json_encode($json);
    }

    public function validationBeforeSubmit(Request $request) {
      $invalidDate = false;
      $val9a = false;
      $dateVal = false;
      $arrExpGT = false;
      $frstExpGT = false;
      $extExpGT = false;
      $defArrFrst = false;
      $defFrstExt = false;
      $dsave = false;
      $extendedPresent=false;
      $profileErrors=array();
      $dupEmail=false;
      $emptyContact=false;
      $bindedCount = 0;
      $dateFormat = false;

        // if contact number is already binded in the app
        $contactNumber = $request->contactNumber;
        $contactNumber2 = $request->contactNumber2;
        $group_id = $request->group_id;

        if($group_id > 0){
            $groupLeaderId = Group::findOrFail($group_id)->leader;
            $groupLeader = User::findOrfail($groupLeaderId);
            if($contactNumber == '' || $contactNumber == NULL){
                $contactNumber = $groupLeader->contact_number;
                $bindedCount = 0;
            }else{
                if($request->id){ //for edit
                    if($contactNumber!=User::select('contact_number')->where('id', $request->id)->first()->contact_number){
                        $bindedCount=User::where('contact_number', $contactNumber)->count();
                    }
                }else{
                    $bindedCount=User::where('contact_number', $contactNumber)->count();
                }
            }
        }else{
            if($contactNumber == '' || $contactNumber == NULL){
                $emptyContact = true;
            }else{

                if($request->id){ //for edit
                    if($contactNumber!=User::select('contact_number')->where('id', $request->id)->first()->contact_number){
                        $bindedCount=User::where('contact_number', $contactNumber)->count();
                    }
                }else{
                    $bindedCount=User::where('contact_number', $contactNumber)->count();
                }
            }
        }
          //dd($request->all());
         // \Log::info($request->all());
        
        // exception if add directly to group
    

          //dd($bindedCount);
        // If duplicate entry
            $users = User::where('first_name', $request->firstName)
                ->where('last_name', $request->lastName)
                ->where('gender', $request->gender)
                ->where('birth_date', $request->birthdate);
            if($request->passport != ''){
                $users = User::where('first_name', $request->firstName)
                ->where('last_name', $request->lastName)
                ->where('gender', $request->gender)
                ->where('birth_date', $request->birthdate)
                ->where('passport', $request->passport);
            }

            if($request->id && $request->passport) { // For edit client
                $users = $users->where('id', '<>', $request->id)->where('first_name', $request->firstName)
                ->where('last_name', $request->lastName)
                ->where('gender', $request->gender)
                ->where('birth_date', $request->birthdate)
                ->where('passport', $request->passport);
            }
            else{
                 $users = $users->where('id', '<>', $request->id);
            }
            $users = $users->get();
            //dd(Carbon::now()->format('Y/m/d'));
            // $invalidDate = false;
            // if(Carbon\Carbon::now()->format('Y-m-d')->lte(Carbon\Carbon::parse($request->exp_date)->format('Y-m-d'))){
            //   $invalidDate = true;
            // }

            //$startDate = Carbon::now()->format('Y-m-d');
            //$endDate = Carbon::parse($request->exp_date)->format('Y-m-d');
            //$ndate = $startDate->diffInYears($endDate);



            if($request->email!=null){
              if($request->id!=null){
                $oldEmail = User::select('email')->where('id',$request->id)->first()->email;
                if($oldEmail!=$request->email){
                  $counterDup = User::where('id','!=',$request->id)->where('email',$request->email)->count();
                  if($counterDup>0){
                    $dupEmail = true;

                  }
                }
              }else{

                $counterDup = User::where('email',$request->email)->count();
                if($counterDup>0){
                  $dupEmail = true;
                }
              }
            }

            if($request->visa_type==null || $request->visa_type=="0"){
              $dsave = true;
            }
            //dd($contactNumber.'---'.$group_id);

            if($request->firstName==null){
              array_push($profileErrors,'First Name');
            }
            if($request->lastName==null){
              array_push($profileErrors,'Last Name');
            }
            if($request->gender==null){
              array_push($profileErrors,'Gender');
            }
            if($request->birthdate==null){
              array_push($profileErrors,'Birth Date');
            }
            if($request->contactNumber==null && $request->group_id==0){
              array_push($profileErrors,'Contact Number');
            }
            if($request->nationality==null){
              array_push($profileErrors,'Nationality');
            }
            if($request->birth_country==null){
              array_push($profileErrors,'Birth Country');
            }
            if($request->gender==null || $request->gender=='0'){
              array_push($profileErrors,'Gender');
            }
            if($request->civil_status==null || $request->civil_status=='0'){
              array_push($profileErrors,'Civil Status');
            }
            if($request->address==null){
              array_push($profileErrors,'Address');
            }

            // if($request->passport!=null){
            //     if($request->passport_exp_date==null){
            //         array_push($profileErrors,'Passport validity');
            //     }
            // }
            
            // if($request->city==null){
            //   array_push($profileErrors,'City');
            // }
            // if($request->province==null){
            //   array_push($profileErrors,'Province');
            // }


            //dd($request->visa_type);
            if(!$dsave){
              if($request->visa_type=='9A' && ($request->first_exp_date==null || $request->arrival_date==null)){
                $invalidDate = true;
              }
              if(($request->visa_type=='9G' || $request->visa_type=='TRV') && ($request->service_exp_date==null || $request->issue_date == null || $request->icard_exp_date==null)){
                $invalidDate = true;
              }
              if($request->visa_type=='CWV' && ($request->service_exp_date==null)){
                $invalidDate = true;
              }


              if(!$invalidDate){
                if($request->visa_type=='9A'){
                  $d1  = explode('-', $request->first_exp_date);
                  if(count($d1)>2){
                    if(!checkdate($d1[1],$d1[2],$d1[0])){
                      $dateVal = true;
                    }
                  }else{$dateVal = true;}
                  $d1  = explode('-', $request->arrival_date);
                  if(count($d1)>2){
                    if(!checkdate($d1[1],$d1[2],$d1[0])){
                      $dateVal = true;
                    }
                  }else{$dateVal = true;}
                  if($request->service_exp_date!=null){
                    $d1  = explode('-', $request->service_exp_date);
                    if(count($d1)>2){
                      $extendedPresent = true;
                      if(!checkdate($d1[1],$d1[2],$d1[0])){
                        $dateVal = true;
                      }
                    }else{$dateVal = true;}
                  }
                }else if($request->visa_type=='9G' || $request->visa_type=='TRV'){
                  $d1  = explode('-', $request->service_exp_date);
                  if(count($d1)>2){
                    if(!checkdate($d1[1],$d1[2],$d1[0])){
                      $dateVal = true;
                    }
                  }else{$dateVal = true;}
                  $d1  = explode('-', $request->icard_exp_date);
                  if(count($d1)>2){
                    if(!checkdate($d1[1],$d1[2],$d1[0])){
                      $dateVal = true;
                    }
                  }else{$dateVal = true;}
                  $d1  = explode('-', $request->issue_date);
                  if(count($d1)>2){
                    if(!checkdate($d1[1],$d1[2],$d1[0])){
                      $dateVal = true;
                    }
                  }else{$dateVal = true;}
                }else{
                  $d1  = explode('-', $request->service_exp_date);
                  if(count($d1)>2){
                    if(!checkdate($d1[1],$d1[2],$d1[0])){
                      $dateVal = true;
                    }
                  }else{$dateVal = true;}
                }
              }
            if(!$dateVal){
                if($request->visa_type=='9A'){
                    
                    $end = Carbon::parse($request->arrival_date);
                    $now = Carbon::now();
                    //dd($end->gt($now));
                    if($end->gt($now)){
                    $arrExpGT = true;
                    }

                    if(!$arrExpGT){
                    $end = Carbon::parse($request->arrival_date);
                    $now = Carbon::parse($request->first_exp_date);
                    if($end->gt($now)){
                        $frstExpGT = true;
                    }
                    if(!$frstExpGT){
                        if($end->diffInDays($now)<13){
                        $defArrFrst = true;
                        }
                    }
                    //dd($end->gt($now));
                    }
                    if(!$frstExpGT){
                    $end = Carbon::parse($request->first_exp_date);
                    $now = Carbon::parse($request->service_exp_date);
                    if($request->service_exp_date != null){
                        if($end->gt($now)){
                            $extExpGT = true;
                        }
                    }
                    if(!$extExpGT && $extendedPresent){
                        if($end->diffInDays($now)<28){
                        $defFrstExt = true;
                        }
                    }
                    //dd($end->gt($now));
                    }

                }
            }
            }
            //dd($profileErrors);
            //dd($bindedCount);
            //\Log::info($invalidDate);
        return json_encode([
            'binded' => ($bindedCount > 0) ? true : false,
            'users' => $users,
            'duplicateEntry' => (count($users) > 0) ? true : false,
            'invalidDate'=>$invalidDate,
            'val9a'=>$val9a,
            'dateVal'=>$dateVal,
            'arrExpGT'=>$arrExpGT,
            'frstExpGT'=>$frstExpGT,
            'extExpGT'=>$extExpGT,
            'defArrFrst'=>$defArrFrst,
            'defFrstExt'=>$defFrstExt,
            //'dsave'=>$dsave,
            'errorList'=>$profileErrors,
            'dupEmail'=>$dupEmail,
            'emptyContact' => $emptyContact
            //'val' => Carbon::parse($request->service_exp_date)->format('Y-m-d')
        ]);
    }

    public function getAuthorizers(){
        $role = Role::where('name', 'authorizer')->first();
        $users = DB::connection()
                 ->table('role_user as r')
                 ->where('r.role_id', $role->id)
                 ->join('users as u', 'u.id', '=', 'r.user_id')
                 ->select(DB::raw('u.id, CONCAT(u.first_name," ",u.last_name)  AS fullname'))
                 ->get();
        return json_encode($users);
    }

    public function saveAuthorizer(Request $request){
        $authorizerId = $request->authorizer['id'];
        // \Log::info($request->password);
        $user = User::findOrFail($authorizerId);
        // \Log::info($user->password);
        // $login = Hash::check($request->password, $user->password);
        if(Hash::check($request->password, $user->password)){
            $result = array('status' => 'success');
        }else{
            $result = array('status' => 'failed');
        }

        return json_encode($result);
    }

    public function exportPDF(Request $request){
        $data = $request->all();
        $date = Carbon::now()->format('mdY');
        $bdate = Carbon::parse($data['bday'])->format('d-M-Y');
        $p_exp = Carbon::parse($data['passport_exp'])->format('d-M-Y');
        $l_arr = Carbon::parse($data['latest_arrival'])->format('d-M-Y');
        $l_ext = Carbon::parse($data['ext_valid_date'])->format('d-M-Y');
        $add = $data['address'];

        $id = $data['cid'];
        $serv = $data['service_name'];
        $data['issue_date'] = Carbon::parse($data['issue_date'])->format('d M Y');

        //BIRTHDAY
        $bdate2 = explode('-', $bdate);
        $data['bd'] = $bdate2[0];
        $data['bmonth'] = $bdate2[1];
        $data['byear'] = $bdate2[2];

        //Passport Expiration
        $p_exp2 = explode('-', $p_exp);
        $data['pd'] = $p_exp2[0];
        $data['pmonth'] = $p_exp2[1];
        $data['pyear'] = $p_exp2[2];

        //Date of Latest Arrival
        $l_arr2 = explode('-', $l_arr); 
        $data['ld'] = $l_arr2[0];
        $data['lmonth'] = $l_arr2[1];
        $data['lyear'] = $l_arr2[2];
        
        //Latest Visa Extension
        $l_ext2 = explode('-', $l_ext);
        $data['ed'] = $l_ext2[0];
        $data['emonth'] = $l_ext2[1];
        $data['eyear'] = $l_ext2[2];

        //address - it will divide the string if exceeded the limit of 45 characters
        $add_len = strlen($add);

        if($add_len > 45){
            $add_eq = $add_len/2;

            //checks the placement of comma after computing the middle of the string
            $comma_ib = strrpos(substr($add, 0, $add_eq), ',');
            $comma_ia = strpos($add, ',', $add_eq);

            if ($comma_ib === false) {
                $comma_ib = 0;
            }
                
            if ($comma_ia === false) {
                $comma_ia = $add_len;
            }

            if(($add_eq - $comma_ib) <= ($comma_ia - $add_eq)){
                $comma_i = $comma_ib;
            }else{
                $comma_i = $comma_ia;
            }

            $data['address_1'] = substr($add, 0, $comma_i);
            $data['address_2'] = substr($add, $comma_i + 1); 
        }else{
            $data['address_1'] = $add;
            $data['address_2'] = ""; 
        }

        
        $pdf = PDF::loadView('cpanel.visa.clients.pdfforms.9A-Extension-Form', compact('data'));
        $filename = $id.'_'.$serv.'_'.$date.'.pdf';
        $filename = str_replace(' ', '_', $filename);
        $pdf->setPaper('legal', 'portrait');
        $pdf->save($filename)->stream($filename);
        // $pdf->stream($filename);

        return json_encode([
          'success' => true,
          'filename' => $filename
        ]);
    }

    public static function clientBal($client_id){
        $paid = ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',0)
                    ->where(function($q) {
                                    $q->orwhere('type','Deposit');
                                    $q->orwhere('type','Payment');
                                    $q->orwhere('type','Discount');
                                })
                    ->sum('amount');

        $cost = ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',0)
                    ->where('type','Refund')
                    ->sum('amount');

        $cost += ClientService::where('client_id',$client_id)
            ->where('group_id', 0)
            ->where('active', 1)
            ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));


        $balance = (
                        Common::toInt($paid)
                    )-(
                        Common::toInt($cost)
                    );

        return $balance;
    }

    public static function clientBalDec1($client_id){
        $paid = ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',0)
                    ->where(function($q) {
                                    $q->orwhere('type','Deposit');
                                    $q->orwhere('type','Payment');
                                    // $q->orwhere('type','Discount');
                                })
                    ->where('id','<=',21208)
                    ->sum('amount');

        $paid += ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',0)
                    ->where(function($q) {
                                    $q->orwhere('type','Discount');
                                })
                    ->sum('amount');

        $cost = ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',0)
                    ->where('type','Refund')
                    ->sum('amount');

        $cost += ClientService::where('client_id',$client_id)
            ->where('group_id', 0)
            ->where('active', 1)
            ->where('id','<=',67386)
            ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));


        $balance = (
                        Common::toInt($paid)
                    )-(
                        Common::toInt($cost)
                    );

        return $balance;
    }

    public static function clientCollectable($client_id){
        $paid = ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',0)
                    ->where(function($q) {
                                    $q->orwhere('type','Deposit');
                                    $q->orwhere('type','Payment');
                                    $q->orwhere('type','Discount');
                                })
                    ->sum('amount');

        $cost = ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',0)
                    ->where('type','Refund')
                    ->sum('amount');

        $cost += ClientService::where('client_id',$client_id)
            ->where('group_id', 0)
            ->where('active', 1)
            ->where('status', '=', 'complete')
            ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));


        $col = ((Common::toInt($paid))-(
                    Common::toInt($cost)
                    ));
        if($col > 0){
            $col = 0;
        }
        return $col;
    }

    public static function groupBal($group_id){
        $paid = ClientTransactions::where('group_id',$group_id)
                    ->where(function($q) {
                                    $q->orwhere('type','Deposit');
                                    $q->orwhere('type','Payment');
                                    $q->orwhere('type','Discount');
                                })
                    ->sum('amount');

        $cost = ClientTransactions::where('group_id',$group_id)
                    ->where('type','Refund')
                    ->sum('amount');

        $cost += ClientService::where('group_id',$group_id)
            ->where('active', 1)
            ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));


        $balance = (Common::toInt($paid))-(
                    Common::toInt($cost)
                    );

        return $balance;
    }

    public static function groupBalDec1($group_id){
        $paid = ClientTransactions::where('group_id',$group_id)
                    ->where(function($q) {
                                    $q->orwhere('type','Deposit');
                                    $q->orwhere('type','Payment');
                                })
                    ->where('id','<=',21208)
                    ->sum('amount');

        $paid += ClientTransactions::where('group_id',$group_id)
                    ->where(function($q) {
                                    $q->orwhere('type','Discount');
                                })
                    ->sum('amount');

        $cost = ClientTransactions::where('group_id',$group_id)
                    ->where('type','Refund')
                    ->sum('amount');

        $cost += ClientService::where('group_id',$group_id)
            ->where('active', 1)
            ->where('id','<=',67386)
            ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));


        $balance = (Common::toInt($paid))-(
                    Common::toInt($cost)
                    );

        return $balance;
    }

    public static function groupCollectable($group_id){
        $paid = ClientTransactions::where('group_id',$group_id)
                    ->where(function($q) {
                                    $q->orwhere('type','Deposit');
                                    $q->orwhere('type','Payment');
                                    $q->orwhere('type','Discount');
                                })
                    ->sum('amount');

        $cost = ClientTransactions::where('group_id',$group_id)
                    ->where('type','Refund')
                    ->sum('amount');

        $cost += ClientService::where('group_id',$group_id)
            ->where('active', 1)
            ->where('status', '=', 'complete')
            ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));


        $col = (Common::toInt($paid))-(
                    Common::toInt($cost)
                    );
        if($col > 0){
            $col = 0;
        }
        return $col;
    }

    public static function groupCollectable2($group_id){
        $paid = ClientTransactions::where('group_id',$group_id)
                    ->where(function($q) {
                                    $q->orwhere('type','Deposit');
                                    $q->orwhere('type','Payment');
                                    $q->orwhere('type','Discount');
                                })
                    ->sum('amount');

        $cost = ClientTransactions::where('group_id',$group_id)
                    ->where('type','Refund')
                    ->sum('amount');

        $cost += ClientService::where('group_id',$group_id)
            ->where('active', 1)
            ->where('status', '=', 'complete')
            ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));


        $col = (Common::toInt($paid))-(
                    Common::toInt($cost)
                    );
        return $col;
    }

}
