<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Visa\ReportType;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportTypesController extends Controller
{

	public function index() {
		return ReportType::all();
	}

	public function store(Request $request) {
		$this->validate($request, [
			'title' => 'required',
            'title_cn' => 'required',
            'description' => 'required',
            'services_id' => 'required'
		]);

		ReportType::create([
			'title' => $request->title,
			'title_cn' => $request->title_cn,
			'description' => $request->description,
			'services_id' => implode(",", $request->services_id),
			'with_docs' => $request->with_docs
		]);		

		return json_encode([
			'success' => true,
			'message' => 'Report type successfully added.'
		]);
	}

	public function update(Request $request, $id) {
		$this->validate($request, [
			'title' => 'required',
            'title_cn' => 'required',
            'description' => 'required',
            'services_id' => 'required'
		]);

		ReportType::findOrFail($id)
			->update([
				'title' => $request->title,
				'title_cn' => $request->title_cn,
				'description' => $request->description,
				'services_id' => implode(",", $request->services_id),
				'with_docs' => $request->with_docs
			]);

		return json_encode([
			'success' => true,
			'message' => 'Report type successfully updated.'
		]);
	}

	public function destroy($id) {
		ReportType::findOrFail($id)->delete();

		return json_encode([
			'success' => true,
			'message' => 'Report type successfully deleted.'
		]);
	}

}