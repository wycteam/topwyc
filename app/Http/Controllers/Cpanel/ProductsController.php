<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\User;
use App\Models\Shopping\Product;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('cpanel/product/index');
    }
}
