<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Visa\Task;

use App\Models\Visa\Service;

use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class TasksController extends Controller
{

	public function index() {
		$lang = trans('cpanel/todays-tasks-page');
		$data = [
            'title' => $lang['tasks']
        ];

        return view('cpanel.visa.tasks.index', $data);
	}

	public function getTranslation() {
		return json_encode([
            'translation' => trans('cpanel/todays-tasks-page')
        ]);
	}

	public static function save($clientServices = [], $actions = [], $categories = [], $date1 = [], $date2 = [], $date3 = [], $dateTimeArray = [], $sameDayFlling = false) {

		$today = Carbon::now()->format('Y-m-d');
		
		for($i=0; $i<count($clientServices); $i++) {

			$serviceId = $clientServices[$i]->service_id;

			$service = Service::findOrFail($serviceId);
			$whoIsInChargeArr = explode(',', $service->who_is_in_charge);
			$whereToFileArr = explode(',', $service->where_to_file);
			$estimatedCostArr = explode(',', $service->estimated_cost);
			$statusArr = explode(',', $service->status);

			if( count($actions) == 0 ) {
				$source = 'add-service';

				// Add an on-process service, then change status to pending, then change again to on-process
				self::deleteTask($clientServices[$i]->id, $statusArr[0]);

				if($sameDayFlling) {
					$date = $today;
				} else {
					$date = (Carbon::now()->englishDayOfWeek == 'Friday') 
			            ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			            : Carbon::parse($today)->addDay()->format('Y-m-d');
				}
			} else {
				$source = 'write-report';
				$actionId = $actions[$i];
				$categoryId = $categories[$i];
			}

			switch($serviceId) {

				case 64:
					// 1 Month 9A Visa Extension - Normal
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 66:
					// 2 Months 9A Visa Extension - Normal
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 329:
					// 3 Months 9A Visa Extension - Normal
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 340:
					// 4 Months 9A Visa Extension - Normal
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 342:
					// 5 Months 9A Visa Extension - Normal
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 67:
					// 6 Months 9A Visa Extension - Normal
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 281:
					// 7 Months 9A Visa Extension
					if($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
					            'client_service_id' => $clientServices[$i]->id,
					            'who_is_in_charge' => $whoIsInChargeArr[0],
					            'where_to_file' => $whereToFileArr[0],
					            'estimated_cost' => $estimatedCostArr[0],
					            'status' => $statusArr[0],
					            'date' => Carbon::parse($date2[$i])->format('Y-m-d')
					        ]);
						}
						// Action = Payment
						// Category = Immigration
						elseif($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 282:
					// 8 Months 9A Visa Extension
					if($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
					            'client_service_id' => $clientServices[$i]->id,
					            'who_is_in_charge' => $whoIsInChargeArr[0],
					            'where_to_file' => $whereToFileArr[0],
					            'estimated_cost' => $estimatedCostArr[0],
					            'status' => $statusArr[0],
					            'date' => Carbon::parse($date2[$i])->format('Y-m-d')
					        ]);
						}
						// Action = Payment
						// Category = Immigration
						elseif($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 283:
					// 9 Months 9A Visa Extension
					if($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
					            'client_service_id' => $clientServices[$i]->id,
					            'who_is_in_charge' => $whoIsInChargeArr[0],
					            'where_to_file' => $whereToFileArr[0],
					            'estimated_cost' => $estimatedCostArr[0],
					            'status' => $statusArr[0],
					            'date' => Carbon::parse($date2[$i])->format('Y-m-d')
					        ]);
						}
						// Action = Payment
						// Category = Immigration
						elseif($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 284:
					// 10 Months 9A Visa Extension
					if($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
					            'client_service_id' => $clientServices[$i]->id,
					            'who_is_in_charge' => $whoIsInChargeArr[0],
					            'where_to_file' => $whereToFileArr[0],
					            'estimated_cost' => $estimatedCostArr[0],
					            'status' => $statusArr[0],
					            'date' => Carbon::parse($date2[$i])->format('Y-m-d')
					        ]);
						}
						// Action = Payment
						// Category = Immigration
						elseif($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 285:
					// 11 Months 9A Visa Extension
					if($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
					            'client_service_id' => $clientServices[$i]->id,
					            'who_is_in_charge' => $whoIsInChargeArr[0],
					            'where_to_file' => $whereToFileArr[0],
					            'estimated_cost' => $estimatedCostArr[0],
					            'status' => $statusArr[0],
					            'date' => Carbon::parse($date2[$i])->format('Y-m-d')
					        ]);
						}
						// Action = Payment
						// Category = Immigration
						elseif($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 286:
					// 12 Months to 23 Months 9A Visa Extension
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Payment
						// Category = Immigration
						if($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						} 
						// Action = Filed
						// Category = Immigration
						elseif ($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 287:
					// 2-3 Years 9A Visa Extension
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Payment
						// Category = Immigration
						if($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 349:
					// 1 Month 9a Visa Extension- Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 350:
					// 2 Months 9A Visa Extension - Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 351:
				 	// 3 Months 9A Visa Extension - Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 353:
					// 4 Months 9A Visa Extension - Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 354:
					// 5 Months 9A Visa Extension - Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 355:
					// 6 Months 9A Visa Extension - Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 364:
					// 9A Expired 6-11 Months Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Payment
						// Category = Immigration
						if($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 337:
					// Visa Waiver 9A Visa Extension - Normal
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 361:
					// Visa Waiver 9A Visa Extension - Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 405:
					// Visa Upon Arrival  Extension 1 Month
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 408:
					// Visa Upon Arrival - New Application
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 411:
					// 9A Expired 35 Months + Rush Fee
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Payment
						// Category = Immigration
						if($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 412:
					// Visa Upon Arrival  Extension 2 Months
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 413:
					// 9A visa Extension Regular
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Payment
						// Category = Immigration
						if($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 419:
					//  1 Month 9a Visa Extension- Regular
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Payment
						// Category = Immigration
						if($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 418:
					// 9A Visa Extension Regular -  2 Months
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Payment
						// Category = Immigration
						if($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break; 

				case 420:
					// Up To Date 9a Visa Extension- Express Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 421:
					// 9A Expired 3 - 5 Months Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 325:
					// 9G Working Visa Extension - 1 Year
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DOLE
						if($actionId == 1 && $categoryId == 2) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
						// Action = Released
						// Category = DOLE
						elseif($actionId == 2 && $categoryId == 19) {
							$date = (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d');

							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => $date
				            ]);

							$task = Task::where('client_service_id', $clientServices[$i]->id)
								->where('status', 'Released')
								->first();
							if($task) {
								$estimatedReleasingDate = Carbon::parse($task->date);
								if($estimatedReleasingDate->gt($date)) {
									$task->delete();
								}
							}
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}
						// Action = For implementation
						// Category =
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[4],
				                'where_to_file' => $whereToFileArr[4],
				                'estimated_cost' => $estimatedCostArr[4],
				                'status' => $statusArr[4],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 6 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[5],
				                'where_to_file' => $whereToFileArr[5],
				                'estimated_cost' => $estimatedCostArr[5],
				                'status' => $statusArr[5],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 326:
					// 9G Working Visa Extension - 2 Years
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DOLE
						if($actionId == 1 && $categoryId == 2) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
						// Action = Released
						// Category = DOLE
						elseif($actionId == 2 && $categoryId == 19) {
							$date = (Carbon::now()->englishDayOfWeek == 'Friday') 
								? Carbon::parse($today)->addDays(3)->format('Y-m-d')
								: Carbon::parse($today)->addDay()->format('Y-m-d');

							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => $date
				            ]);

				            $task = Task::where('client_service_id', $clientServices[$i]->id)
								->where('status', 'Released')
								->first();
							if($task) {
								$estimatedReleasingDate = Carbon::parse($task->date);
								if($estimatedReleasingDate->gt($date)) {
									$task->delete();
								}
							}
						}

						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}

						// Action = For implementation
						// Category =
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[4],
				                'where_to_file' => $whereToFileArr[4],
				                'estimated_cost' => $estimatedCostArr[4],
				                'status' => $statusArr[4],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 6 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[5],
				                'where_to_file' => $whereToFileArr[5],
				                'estimated_cost' => $estimatedCostArr[5],
				                'status' => $statusArr[5],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 327:
					// 9G Working Visa Extension - 3 Years
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DOLE
						if($actionId == 1 && $categoryId == 2) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
						// Action = Released
						// Category = DOLE
						elseif($actionId == 2 && $categoryId == 19) {
							$date = (Carbon::now()->englishDayOfWeek == 'Friday') 
								? Carbon::parse($today)->addDays(3)->format('Y-m-d')
								: Carbon::parse($today)->addDay()->format('Y-m-d');

							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => $date
				            ]);

				            $task = Task::where('client_service_id', $clientServices[$i]->id)
								->where('status', 'Released')
								->first();
							if($task) {
								$estimatedReleasingDate = Carbon::parse($task->date);
								if($estimatedReleasingDate->gt($date)) {
									$task->delete();
								}
							}
						}

						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}
						// Action = For implementation
						// Category =
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[4],
				                'where_to_file' => $whereToFileArr[4],
				                'estimated_cost' => $estimatedCostArr[4],
				                'status' => $statusArr[4],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 6 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[5],
				                'where_to_file' => $whereToFileArr[5],
				                'estimated_cost' => $estimatedCostArr[5],
				                'status' => $statusArr[5],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 328:
					// 9G Working Visa Conversion - 1 Year
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DOLE
						if($actionId == 1 && $categoryId == 2) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
						// Action = Released
						// Category = DOLE
						elseif($actionId == 2 && $categoryId == 19) {
							$date = (Carbon::now()->englishDayOfWeek == 'Friday') 
								? Carbon::parse($today)->addDays(3)->format('Y-m-d')
								: Carbon::parse($today)->addDay()->format('Y-m-d');

							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => $date
				            ]);

				            $task = Task::where('client_service_id', $clientServices[$i]->id)
								->where('status', 'Released')
								->first();
							if($task) {
								$estimatedReleasingDate = Carbon::parse($task->date);
								if($estimatedReleasingDate->gt($date)) {
									$task->delete();
								}
							}
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}
						// Action = For implementation
						// Category =
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[4],
				                'where_to_file' => $whereToFileArr[4],
				                'estimated_cost' => $estimatedCostArr[4],
				                'status' => $statusArr[4],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 6 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[5],
				                'where_to_file' => $whereToFileArr[5],
				                'estimated_cost' => $estimatedCostArr[5],
				                'status' => $statusArr[5],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 97:
					// 9G Working Visa Conversion - 2 Years
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DOLE
						if($actionId == 1 && $categoryId == 2) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
						// Action = Released
						// Category = DOLE
						elseif($actionId == 2 && $categoryId == 19) {
							$date = (Carbon::now()->englishDayOfWeek == 'Friday') 
								? Carbon::parse($today)->addDays(3)->format('Y-m-d')
								: Carbon::parse($today)->addDay()->format('Y-m-d');

							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => $date
				            ]);

				            $task = Task::where('client_service_id', $clientServices[$i]->id)
								->where('status', 'Released')
								->first();
							if($task) {
								$estimatedReleasingDate = Carbon::parse($task->date);
								if($estimatedReleasingDate->gt($date)) {
									$task->delete();
								}
							}
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}
						// Action = For implementation
						// Category =
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[4],
				                'where_to_file' => $whereToFileArr[4],
				                'estimated_cost' => $estimatedCostArr[4],
				                'status' => $statusArr[4],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 6 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[5],
				                'where_to_file' => $whereToFileArr[5],
				                'estimated_cost' => $estimatedCostArr[5],
				                'status' => $statusArr[5],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 98:
					// 9G Working Visa Conversion - 3 Years
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DOLE
						if($actionId == 1 && $categoryId == 2) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
						// Action = Released
						// Category = DOLE
						elseif($actionId == 2 && $categoryId == 19) {
							$date = (Carbon::now()->englishDayOfWeek == 'Friday') 
								? Carbon::parse($today)->addDays(3)->format('Y-m-d')
								: Carbon::parse($today)->addDay()->format('Y-m-d');

							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => $date
				            ]);

				            $task = Task::where('client_service_id', $clientServices[$i]->id)
								->where('status', 'Released')
								->first();
							if($task) {
								$estimatedReleasingDate = Carbon::parse($task->date);
								if($estimatedReleasingDate->gt($date)) {
									$task->delete();
								}
							}
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}
						// Action = For implementation
						// Category =
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[4],
				                'where_to_file' => $whereToFileArr[4],
				                'estimated_cost' => $estimatedCostArr[4],
				                'status' => $statusArr[4],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 6 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[5],
				                'where_to_file' => $whereToFileArr[5],
				                'estimated_cost' => $estimatedCostArr[5],
				                'status' => $statusArr[5],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 401:
					// 9G Working Visa Conversion - 2 Years All in Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DOLE
						if($actionId == 1 && $categoryId == 2) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
						// Action = Released
						// Category = DOLE
						elseif($actionId == 2 && $categoryId == 19) {
							$date = (Carbon::now()->englishDayOfWeek == 'Friday') 
								? Carbon::parse($today)->addDays(3)->format('Y-m-d')
								: Carbon::parse($today)->addDay()->format('Y-m-d');

							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => $date
				            ]);

				            $task = Task::where('client_service_id', $clientServices[$i]->id)
								->where('status', 'Released')
								->first();
							if($task) {
								$estimatedReleasingDate = Carbon::parse($task->date);
								if($estimatedReleasingDate->gt($date)) {
									$task->delete();
								}
							}
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}
						// Action = For implementation
						// Category =
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[4],
				                'where_to_file' => $whereToFileArr[4],
				                'estimated_cost' => $estimatedCostArr[4],
				                'status' => $statusArr[4],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 6 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[5],
				                'where_to_file' => $whereToFileArr[5],
				                'estimated_cost' => $estimatedCostArr[5],
				                'status' => $statusArr[5],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 417:
					// 9G Working Visa Conversion - 1 Year All in Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DOLE
						if($actionId == 1 && $categoryId == 2) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
						// Action = Released
						// Category = DOLE
						elseif($actionId == 2 && $categoryId == 19) {
							$date = (Carbon::now()->englishDayOfWeek == 'Friday') 
								? Carbon::parse($today)->addDays(3)->format('Y-m-d')
								: Carbon::parse($today)->addDay()->format('Y-m-d');

							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => $date
				            ]);

				            $task = Task::where('client_service_id', $clientServices[$i]->id)
								->where('status', 'Released')
								->first();
							if($task) {
								$estimatedReleasingDate = Carbon::parse($task->date);
								if($estimatedReleasingDate->gt($date)) {
									$task->delete();
								}
							}
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}
						// Action = For implementation
						// Category =
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[4],
				                'where_to_file' => $whereToFileArr[4],
				                'estimated_cost' => $estimatedCostArr[4],
				                'status' => $statusArr[4],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 6 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[5],
				                'where_to_file' => $whereToFileArr[5],
				                'estimated_cost' => $estimatedCostArr[5],
				                'status' => $statusArr[5],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 400:
					// 9G Working Visa Conversion - 3 Years All in Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DOLE
						if($actionId == 1 && $categoryId == 2) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
						// Action = Released
						// Category = DOLE
						elseif($actionId == 2 && $categoryId == 19) {
							$date = (Carbon::now()->englishDayOfWeek == 'Friday') 
								? Carbon::parse($today)->addDays(3)->format('Y-m-d')
								: Carbon::parse($today)->addDay()->format('Y-m-d');

							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => $date
				            ]);

				            $task = Task::where('client_service_id', $clientServices[$i]->id)
								->where('status', 'Released')
								->first();
							if($task) {
								$estimatedReleasingDate = Carbon::parse($task->date);
								if($estimatedReleasingDate->gt($date)) {
									$task->delete();
								}
							}
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}
						// Action = For implementation
						// Category =
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[4],
				                'where_to_file' => $whereToFileArr[4],
				                'estimated_cost' => $estimatedCostArr[4],
				                'status' => $statusArr[4],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 6 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[5],
				                'where_to_file' => $whereToFileArr[5],
				                'estimated_cost' => $estimatedCostArr[5],
				                'status' => $statusArr[5],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 89:
					// ECC - Normal
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 374:
					// ECC Payment
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 377:
					// ECC-Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 102:
					// Bureau of Immigration Clearace Certificate (BICC) - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 103:
					// Not The Same Person (NTSP) - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 104:
					// Travel Record - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 395:
					// Travel Record - Package Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 121:
					// 9G Working Visa Downgrading - Complete Documents
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

				            // Stage 3 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 394:
					// 9G Working Visa Downgrading - Complete Documents- Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

				            // Stage 3 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 356:
					// 9G Working Visa Downgrading - Incomplete Documents- Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

				            // Stage 3 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 307:
					// CEZA Working Visa Downgrading
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 3 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 415:
					// CEZA Working Visa Downgrading - Incomplete Documents- Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 3 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}

					break;

				case 416:
					// CEZA Working Visa Downgrading - Complete Documents- Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 3 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}

					break;

				case 403:
					// Cancellation of Working Visa
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 128:
					// BLO Lifting
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Payment
						// Category = Immigration
						if($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}

						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 406:
					// Watchlist Lifting
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Payment
						// Category = Immigration
						if($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}

						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 407:
					// Alertlist Lifting
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Payment
						// Category = Immigration
						if($actionId == 11 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
			                    	? Carbon::parse($today)->addDays(3)->format('Y-m-d')
			                    	: Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}

						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 137:
					// ACR - ICARD Renewal
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Implementation
						// Category =
						if($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 209:
					// ACR - ICARD Waiver Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Implementation
						// Category =
						if($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 210:
					// ACR - ICARD Waiver
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Implementation
						// Category =
						if($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 229:
					// ACR - ICARD Replacement
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Implementation
						// Category =
						if($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 265:
					// ACR - ICARD Cancellation
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Implementation
						// Category =
						if($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 318:
					// ACR - ICARD Certification
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Implementation
						// Category =
						if($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}
						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 167:
					// Annual Report Service
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 168:
					// Annual Report Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 142:
					// Special Work Permit (SWP)
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 280:
					// Provisional Working Permit (PWP)
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 357:
					// PRV New Application - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}

						// Action = Implementation
						// Category = 
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 4 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 369:
					// PRV New Inclusion - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}

						// Action = Implementation
						// Category = 
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 4 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 375:
					// PRV 5 Years - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}

						// Action = Implementation
						// Category = 
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 4 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 152:
					// TRV New Application - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}

						// Action = Implementation
						// Category = 
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 4 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 154:
					// TRV 2 Years - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Immigration
						if($actionId == 1 && $categoryId == 1) {
							$_dateTimeArray = $dateTimeArray[$i];
							$customDate = null;
							for($j=0; $j<count($_dateTimeArray); $j++) {
								$customDate .= $_dateTimeArray[$j] . ',';
							}
							if($customDate) {
								$customDate = substr($customDate, 0, -1);
							}
							
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse(min(explode(',', $customDate)))->format('Y-m-d'),
				                'custom_date' => $customDate
				            ]);
						}

						// Action = Implementation
						// Category = 
						elseif($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);

							// Stage 4 - Because no corresponding quick report action
				            Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 291:
					// China Tour Visa Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Chinese Embassy
						if($actionId == 1 && $categoryId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 338:
					// DFA/ Embassy Authentication
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DFA
						if($actionId == 1 && $categoryId == 12) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}

						// Action = Released
						// Category = DFA
						elseif($actionId == 2 && $categoryId == 29) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}

						// Action = Filed
						// Category = Chinese Embassy
						elseif($actionId == 1 && $categoryId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[3],
				                'where_to_file' => $whereToFileArr[3],
				                'estimated_cost' => $estimatedCostArr[3],
				                'status' => $statusArr[3],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 339:
					// Lost Passport/Travel Document Rush - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Chinese Embassy
						if($actionId == 1 && $categoryId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 380:
					// Lost Passport/ Travel Document - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Chinese Embassy
						if($actionId == 1 && $categoryId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 370:
					// DFA Authentication
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DFA
						if($actionId == 1 && $categoryId == 12) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 381:
					// New Passport - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = Chinese Embassy
						if($actionId == 1 && $categoryId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 116:
					// NBI Clearance - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = NBI
						if($actionId == 1 && $categoryId == 15) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 118:
					// Alien Employment Permit (AEP) Filing
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DOLE
						if($actionId == 1 && $categoryId == 2) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 120:
					// Tincard
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 162:
					// Police Report
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 169:
					// Lost I-card Working visa
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = For implementation
						// Category =
						if($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}

						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 215:
					// Amendment
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 212:
					// Re-Stamping of Visa Package - Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = For implementation
						// Category = 
						if($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}

						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 254:
					// SRRV Card Replacement - Package
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 386:
					// SRRV Principal
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = PRA
						if($actionId == 1 && $categoryId == 6) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 387:
					// SRRV Inclusion
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = PRA
						if($actionId == 1 && $categoryId == 6) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 388:
					// SRRV Renewal for 1 year
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 389:
					// SRRV Renewal for 2 years
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 390:
					// SRRV Renewal for 3 years
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 391:
					// SRRV Expedite
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = PRA
						if($actionId == 1 && $categoryId == 6) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 409:
					// SRRV Re-Stamping Expedite
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = PRA
						if($actionId == 1 && $categoryId == 6) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 392:
					// SRRV Medical (Rush Process)
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 393:
					// SRRV Re-Stamping
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = PRA
						if($actionId == 1 && $categoryId == 6) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 223:
					// BOQ - Medical
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = BOQ
						if($actionId == 1 && $categoryId == 10) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 231:
					// Certification
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = BOI
						if($actionId == 1 && $categoryId == 17) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 232:
					// BOI-NTSP (package)
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = BOI
						if($actionId == 1 && $categoryId == 17) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 346:
					// CEZA Certificate
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = CEZA
						if($actionId == 1 && $categoryId == 3) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 358:
					// CEZA Cancellation - Rush
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = CEZA
						if($actionId == 1 && $categoryId == 3) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 359:
					// CEZA Cancellation - Normal
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = CEZA
						if($actionId == 1 && $categoryId == 3) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 376:
					// CEZA Re-Stamping
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = CEZA
						if($actionId == 1 && $categoryId == 3) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 260:
					// Anti Dummy Law Certificate
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = Filed
						// Category = DOJ
						if($actionId == 1 && $categoryId == 4) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

				case 292:
					// Clearance Certificate
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					}
					break;

				case 270:
					// WEG Permit
					if($source == 'add-service') {
						Task::create([
			                'client_service_id' => $clientServices[$i]->id,
			                'who_is_in_charge' => $whoIsInChargeArr[0],
			                'where_to_file' => $whereToFileArr[0],
			                'estimated_cost' => $estimatedCostArr[0],
			                'status' => $statusArr[0],
			                'date' => $date
			            ]);
					} elseif($source == 'write-report') {
						// Action = For implementation
						// Category = 
						if($actionId == 7) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[1],
				                'where_to_file' => $whereToFileArr[1],
				                'estimated_cost' => $estimatedCostArr[1],
				                'status' => $statusArr[1],
				                'date' => (Carbon::now()->englishDayOfWeek == 'Friday') 
				                    ? Carbon::parse($today)->addDays(3)->format('Y-m-d')
				                    : Carbon::parse($today)->addDay()->format('Y-m-d')
				            ]);
						}

						// Action = Filed
						// Category = Immigration
						elseif($actionId == 1 && $categoryId == 1) {
							Task::create([
				                'client_service_id' => $clientServices[$i]->id,
				                'who_is_in_charge' => $whoIsInChargeArr[2],
				                'where_to_file' => $whereToFileArr[2],
				                'estimated_cost' => $estimatedCostArr[2],
				                'status' => $statusArr[2],
				                'date' => Carbon::parse($date2[$i])->format('Y-m-d')
				            ]);
						}
					}
					break;

			}

		}

	}

	public static function deleteTask($clientServiceId, $status) {
		$task = Task::where('client_service_id', $clientServiceId)->where('status', $status)->first();
		
		if($task) {
			$task->delete();
		}
	}

	public function updateWho(Request $request, $id) {
		Task::findOrFail($id)->update([
			'who_is_in_charge' => $request->employeeId
		]);

		return json_encode([
			'success' => true,
			'message' => 'Updated successfully.'
		]);
	}

	public function getHistory($clientServiceId) {
		return Task::with('clientService.user', 'clientService.service_category', 'whoIsInChargeRelation')
			->where('client_service_id', $clientServiceId)
			->get();
	}

}