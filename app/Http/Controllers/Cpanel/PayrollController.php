<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee\Departments;
use App\Models\Employee\DepartmentUser;
use App\Models\Employee\PayrollSettings;
use App\Models\Employee\Attendance;
use App\Models\Employee\PayrollLogs;
use App\Models\Employee\Installments;
use App\Models\Visa\CalendarEvents;
use App\Models\Schedule;
use App\Models\ScheduleType;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class PayrollController extends Controller
{
   public function payrollControl(){
    return view('cpanel.visa.payroll.index');
   }

   public function getDepartment(){
       return Departments::all();
   }

   public function getEmployees(Request $request, $department_id){
       if($department_id==0){
           $emp = DepartmentUser::with('departments')->orderBy('department_id','asc')->get();
            foreach($emp as $emps){
                    $emps['user'] = User::where('id',$emps->user_id)->where('is_active', 1)->with('payrollSettings')->first()->makeHidden(['permissions','document_receive']);
                    $emps['payroll'] = PayrollLogs::where('user_id', $emps->user_id)->orderBy('created_at','desc')->first();
            }
           return $emp;
       }else{
        $emp = DepartmentUser::with('departments')->where('department_id', $department_id)->get();
        foreach($emp as $emps){
                $emps['user'] = User::where('id',$emps->user_id)->where('is_active', 1)->with('payrollSettings')->first()->makeHidden(['permissions','document_receive']);
                $emps['payroll'] = PayrollLogs::where('user_id', $emps->user_id)->orderBy('created_at','desc')->first();
        }
       return $emp;
       }
       
   }
   public function getPayrollLogs(Request $request){
        return PayrollLogs::where('user_id',$request->id)->where('_month',$request->_month)->where('_date',$request->_date)->where('_year',$request->_year)->first();
   }
   public function getAccounts(){
        $dep =  DepartmentUser::all();
        foreach($dep as $list){
            $list['users'] = User::where('id',$list->user_id)->first()->makeHidden(['permissions','document_receive']);
        }
     return $dep;
   }

   public function getProfile($id,$status,$dt){
    $edit = ($status=='new') ? false : true;
    if(PayrollSettings::where('user_id', $id)->count()==0){
        $pSetting = new PayrollSettings;
        $pSetting->user_id = $id;
        $pSetting->_day_from = "MONDAY";
        $pSetting->_day_to = "FRIDAY";
        $pSetting->save();
    }else{
        $ps = PayrollSettings::where('user_id', $id)->first();
        if($ps->_day_from==NULL || $ps->_day_to==NULL || $ps->_day_from=='' || $ps->_day_to==''){
            PayrollSettings::where('user_id', $id)->update(['_day_from'=>'MONDAY','_day_to'=>'FRIDAY']);
        }
    }
    if(Schedule::where('user_id', $id)->count()==0){
        $pSchedule = new Schedule;
        $pSchedule->user_id = $id;
        $pSchedule->time_in = "08:00:00";
        $pSchedule->time_out = "17:00:00";
        $pSchedule->schedule_type_id = 1;
        $pSchedule->save();
    }else{
        $sc = Schedule::where('user_id', $id)->first();
        if($sc->time_in==NULL || $sc->time_out==NULL){
            Schedule::where('user_id', $id)->update(['time_in'=>"08:00:00",'time_out'=>"17:00:00"]);
        }
    }

    $user= User::where('is_active', 1)->where('id', $id)->with(['schedule' =>function($query){
            $query->with('scheduleType');
        }])->with('payrollSettings')->first()->makeHidden(['permissions','document_receive']);
    //\Log::info($user);

    $dep = DepartmentUser::where('user_id', $id)->with('departments')->get();

    foreach($dep as $d){
      $d['user'] = $user;
    }
   
    return view('cpanel.visa.payroll.payroll_profile', array('profile' => json_encode($dep),'editing'=>$edit,'current_payroll'=>$dt));
   }

   public function showList($id){
    if(PayrollSettings::where('user_id', $id)->count()==0){
        $pSetting = new PayrollSettings;
        $pSetting->user_id = $id;
        $pSetting->_day_from = "MONDAY";
        $pSetting->_day_to = "FRIDAY";
        $pSetting->save();
    }else{
        $ps = PayrollSettings::where('user_id', $id)->first();
        if($ps->_day_from==NULL || $ps->_day_to==NULL || $ps->_day_from=='' || $ps->_day_to==''){
            PayrollSettings::where('user_id', $id)->update(['_day_from'=>'MONDAY','_day_to'=>'FRIDAY']);
        }
    }
    if(Schedule::where('user_id', $id)->count()==0){
        $pSchedule = new Schedule;
        $pSchedule->user_id = $id;
        $pSchedule->time_in = "08:00:00";
        $pSchedule->time_out = "17:00:00";
        $pSchedule->schedule_type_id = 1;
        $pSchedule->save();
    }else{
        $sc = Schedule::where('user_id', $id)->first();
        if($sc->time_in==NULL || $sc->time_out==NULL){
            Schedule::where('user_id', $id)->update(['time_in'=>"08:00:00",'time_out'=>"17:00:00"]);
        }
    }
    
        $list = PayrollLogs::where('user_id',$id)->orderBy('id','desc')->get();
        $user= User::where('is_active', 1)->where('id', $id)->with(['schedule' =>function($query){
            $query->with('scheduleType');
        }])->with('payrollSettings')->first()->makeHidden(['permissions','document_receive']);
        $dep = DepartmentUser::where('user_id', $id)->with('departments')->get();

        foreach($dep as $d){
            $d['user'] = $user;
        }
    return view('cpanel.visa.payroll.payroll_list', array('payroll_list' => json_encode($list),'profile'=>json_encode($dep),'full_name'=>$user->full_name));
   }

   public function getPayrollDate(Request $request){
        if($request->_date>10 && $request->_date <= 25){
            $dt = 25;
        }else{
            $dt = 10;
        }
   
        return array('count'=>PayrollLogs::where('user_id',$request->id)->where('_month',$request->_month+1)->where('_date',$dt)->count());
   }

   public function getInstallments(Request $request){
       //\Log::info($request->date_deduction);
       //\Log::info(Installments::where('user_id',$request->id)->where('credit_type',$request->credit_type)->where('date_deduction','<=',date($request->date_deduction))->orderBy('created_at','desc')->get());
        return Installments::where('user_id',$request->id)->where('credit_type',$request->credit_type)->where('date_deduction','>=',date($request->date_deduction))->orderBy('created_at','desc')->get();
   }

   public function getAttendance(Request $request){
    //    $month = (strlen($request->month)==1) ? '0'.$request->month : $request->month;
    //    $prevMonth = (strlen($request->month)==1) ? '0'.($request->month-1) : ($request->month);
       if($request->cutoff==10){
            $from = Carbon::create($request->year, $request->month+1, 11);
            $to = Carbon::create($request->year, $request->month+1, 26);
            $ret = \DB::connection('mysql')->table('attendance_events')->where('user_id',$request->id)->whereBetween('created_at', [$from->subMonth()->toDateString(), $to->subMonth()->toDateString()])->orderBy('created_at', 'asc')->get();
            //$ret = Attendance::where('user_id',$request->id)->whereBetween('created_at', [$from->subMonth()->toDateString(), $to->subMonth()->toDateString()])->orderBy('created_at', 'asc')->get();
        }else{
            $from = Carbon::create($request->year, $request->month+1, 26);
            $to = Carbon::create($request->year, $request->month+1, 11);
            $ret = \DB::connection('mysql')->table('attendance_events')->where('user_id',$request->id)->whereBetween('created_at', [$from->subMonth()->toDateString(), $to->toDateString()])->orderBy('created_at', 'asc')->get();
           //$ret = Attendance::where('user_id',$request->id)->whereBetween('created_at',[$from->subMonth()->toDateString(), $to->toDateString()])->orderBy('created_at', 'asc')->get();
       }
        //\Log::info($ret);
        return $ret;
   }

   public function getHolidays(Request $request){
     $user = Auth::user()->id;
     
       if($user==1193){//show all event if account is sir jason
            $list = CalendarEvents::where('year_added',$request->year)->where('approval','Pending')->orwhere('approval','Approved')->orderBy('start_date','asc')->get();
            foreach($list as $d){
                $d['users'] = ($d->user_id!=0) ? User::where('id',$d->user_id)->first()->makeHidden(['permissions','document_receive']) :[];
            }
            $pending = CalendarEvents::where('year_added',$request->year)->where('approval','Pending')->orderBy('start_date','asc')->get();
            foreach($pending as $d){
                $d['users'] = ($d->user_id!=0) ? User::where('id',$d->user_id)->first()->makeHidden(['permissions','document_receive']) :[];
            }
       }else{
            $list = CalendarEvents::where(function($query) use ($user){
                $query->where('user_id',0);
                $query->orwhere('user_id',$user);
            })->where('year_added',$request->year)->where(function($query){
                $query->where('approval','Pending');
                $query->orwhere('approval','Approved');
            })->orderBy('start_date','asc')->get();
            
            foreach($list as $d){
                $d['users'] = ($d->user_id!=0) ? User::where('id',$d->user_id)->first()->makeHidden(['permissions','document_receive']) :[];
            }
            $pending = CalendarEvents::where(function($query) use ($user){
                $query->where('user_id',0);
                $query->orwhere('user_id',$user);
            })->where('year_added',$request->year)->where('approval','Pending')->orderBy('start_date','asc')->get();
            
            foreach($pending as $d){
                $d['users'] = ($d->user_id!=0) ? User::where('id',$d->user_id)->first()->makeHidden(['permissions','document_receive']) :[];
            }
       }
        $set = PayrollSettings::where('user_id',$user)->first();
        $sched = Schedule::where('user_id',$user)->first();
        //\Log::info($set);
        return array(
            'list'=> $list,
            'year'=>CalendarEvents::orderBy('year_added','desc')->groupBy('year_added')->get(),
            'pending' => $pending,
            'timeremain'=>$set,
            'schedule' => $sched
        );
   }
   public function getHolidaysID(Request $request){
        $list = CalendarEvents::where('id',$request->holiday_id)->first();
        $list['users'] = User::where('id',$list->user_id)->first()->makeHidden(['permissions','document_receive']);
        return $list;
    }
   public function getContribution(Request $request){
        return PayrollSettings::where('user_id',$request->id)->get();
   }
   public function getPayrollCount(Request $request){
        return PayrollLogs::where('user_id', $request->id)->count();
   }

   public function update($id, Request $request){
        switch($request->method){
            case 'time_start':
                Schedule::where('user_id',$id)->update(['time_in' => $request->value]);
            break;
            case 'time_end':
                Schedule::where('user_id',$id)->update(['time_out' => $request->value]);
            break;
            case 'working_days':
                PayrollSettings::where('user_id',$id)->update(['_working_days' => $request->value]);
            break;
            case 'working_hours':
                PayrollSettings::where('user_id',$id)->update(['_working_hours' => $request->value]);
            break;
            case 'schedule_type':
                Schedule::where('user_id',$id)->update(['schedule_type_id' => $request->value]);
            break;
            case 'timein_status':
                $val = ($request->value=='0') ? null : $request->value;
                Attendance::where('user_id',$id)->where('id',$request->id)->update(['timein_status' => $val]);
            break;
            case 'basic_pay':
                $request->value = ($request->value=='') ? 0 : $request->value;
                PayrollSettings::where('user_id',$id)->update(['_basic_pay' => $request->value]);
            break;
            case 'bonus':
                $request->value = ($request->value=='') ? 0 : $request->value;
                PayrollSettings::where('user_id',$id)->update(['_bonus' => $request->value]);
            break;
            case 'sss':
                $request->value = ($request->value=='') ? 0 : $request->value;
                PayrollSettings::where('user_id',$id)->update(['_sss' => $request->value]);
            break;
            case 'pagibig':
                $request->value = ($request->value=='') ? 0 : $request->value;
                PayrollSettings::where('user_id',$id)->update(['_pag_ibig' => $request->value]);
            break;
            case 'philhealth':
                $request->value = ($request->value=='') ? 0 : $request->value;
                PayrollSettings::where('user_id',$id)->update(['_phil_health' => $request->value]);
            break;
            case 'sss_es':
                $request->value = ($request->value=='') ? 0 : $request->value;
                PayrollSettings::where('user_id',$id)->update(['_sss_es' => $request->value]);
            break;
            case 'pagibig_es':
                $request->value = ($request->value=='') ? 0 : $request->value;
                PayrollSettings::where('user_id',$id)->update(['_pag_ibig_es' => $request->value]);
            break;
            case 'philhealth_es':
                $request->value = ($request->value=='') ? 0 : $request->value;
                PayrollSettings::where('user_id',$id)->update(['_phil_health_es' => $request->value]);
            break;
            case 'allowance':
                $request->value = ($request->value=='') ? 0 : $request->value;
                PayrollSettings::where('user_id',$id)->update(['_allowance' => $request->value]);
            break;
            case 'day_from':
                PayrollSettings::where('user_id',$id)->update(['_day_from' => $request->value]);
                $val = PayrollSettings::where('user_id',$id)->first();
                PayrollSettings::where('user_id',$id)->update(['_working_days' => $this->daysUntil($val->_day_from,$val->_day_to)]);
                
            break;
            case 'day_to':
                PayrollSettings::where('user_id',$id)->update(['_day_to' => $request->value]);
                $val = PayrollSettings::where('user_id',$id)->first();
                PayrollSettings::where('user_id',$id)->update(['_working_days' => $this->daysUntil($val->_day_from,$val->_day_to)]);
            break;
            case 'employment_date':
                PayrollSettings::where('user_id',$id)->update(['_employment_date' => $request->value]);
            break;
            case 'SET':
                $val = ($request->value=='0') ? null : $request->value;
                if($request->value=='ABSENT')
                    Attendance::where('user_id',$id)->where('id',$request->id)->update(['timein_status' => $val]);
            break;
            case 'post_timeout':
                Attendance::where('user_id',$id)->where('id',$request->id)->update(['time_out' => $request->value]);
            break;
            case 'post_timein':
                Attendance::where('user_id',$id)->where('id',$request->id)->update(['time_in' => $request->value]);
            break;
            case 'post_payroll':
                $pl = new PayrollLogs;
                $pl->user_id = $id;
                $pl->_total_deductions = $request->_total_deductions;
                $pl->_basic_pay = $request->_basic_pay;
                $pl->_bonus = $request->_bonus;
                $pl->_month = $request->_month;
                $pl->_date = $request->_date;
                $pl->_year = $request->_year;
                $pl->_allowance = $request->_allowance;
                $pl->_absent_count = $request->_absent_count;
                $pl->_late_count = $request->_late_count;
                $pl->_holiday_count = $request->_holiday_count;
                $pl->_leave_count = $request->_leave_count;
                $pl->_total_pay = $request->_total_pay;
                $pl->_total_earnings = $request->_total_earnings;
                $pl->_overtime = $request->_overtime;
                $pl->_total_lates = $request->_total_lates;
                $pl->_loan = $request->_loan;
                $pl->_penalty = $request->_penalty;
                $pl->save();                                                                                                                                                                                                  
                
                for($i=0;$i<count($request->_penalty_array);$i++){
                    //\Log::info($request->_penalty_array[$i]['id']);
                    $old = Installments::where('id',$request->_penalty_array[$i]['id'])->first();
                    $old_payment = $old->payment_amount;
                    $old_count = $old->counter;

                    $new_payment = $old_payment + $request->_penalty_array[$i]['payment_amount'];
                    $new_count = $old_count+1;
                    if($new_count==$old->payment_count){
                        Installments::where('id',$request->_penalty_array[$i]['id'])->update(['payment_amount'=>$new_payment,'counter'=>$new_count,'status'=>'PAID']);
                    }else{
                        Installments::where('id',$request->_penalty_array[$i]['id'])->update(['payment_amount'=>$new_payment,'counter'=>$new_count]);
                    }
                    
                }
                
                for($i=0;$i<count($request->_loan_array);$i++){
                    //\Log::info($request->_penalty_array[$i]['id']);
                    $old = Installments::where('id',$request->_loan_array[$i]['id'])->first();
                    $old_payment = $old->payment_amount;
                    $old_count = $old->counter;

                    $new_payment = $old_payment + $request->_loan_array[$i]['payment_amount'];
                    $new_count = $old_count+1;

                    if($new_count==$old->payment_count){
                        Installments::where('id',$request->_loan_array[$i]['id'])->update(['payment_amount'=>$new_payment,'counter'=>$new_count,'status'=>'PAID']);
                    }else{
                        Installments::where('id',$request->_loan_array[$i]['id'])->update(['payment_amount'=>$new_payment,'counter'=>$new_count]);
                    }
                }
            break;
            
        }
   }
   public function checkSaved(Request $request){
    //\Log::info($request->all());
    //\Log::info(PayrollLogs::where('user_id', $request->id)->where('_month',$request->_month+1)->where('_date',$request->_date)->where('_year',$request->_year)->count());
    return PayrollLogs::where('user_id', $request->id)->where('_month',$request->_month+1)->where('_date',$request->_date)->where('_year',$request->_year)->count();
   }
   private function daysUntil($start,$end){
       //\Log::info($start.'--'.$end);
        $lookup=['SUNDAY'=>0,'MONDAY'=>1,'TUESDAY'=>2,'WEDNESDAY'=>3,'THURSDAY'=>4,'FRIDAY'=>5,'SATURDAY'=>6];
        $days=$lookup[$end]-$lookup[$start];
        return ($days<1?$days+7:$days+1);
    }

   public function getScheduleType(){
       return ScheduleType::all();
   }

   public function postHolidayRequestResult(Request $request){
    //\Log::info($request->project[0]);
    $cal = CalendarEvents::where('id', $request->project[0]['id'])->first();
        if($request->project[0]['act']=='Denied'){
            
            CalendarEvents::where('id', $request->project[0]['id'])->update(['approval' => $request->project[0]['act'],'reason_denied'=>$request->project[0]['reason_reject']]);
            $datediff = (strtotime($cal->end_date) - strtotime($cal->start_date))/ (60 * 60 * 24);

            for($i=0;$i<=$datediff;$i++){
                $this->addTime($request->project[0]['time_start'],$request->project[0]['time_end'],CalendarEvents::where('id', $request->project[0]['id'])->first());
            }

        }else{
            $user_id = CalendarEvents::where('id',$request->project[0]['id'])->first()->user_id;
            $date_from = explode("-",$request->project[0]['start_date']);
            $date_to = explode("-",$request->project[0]['end_date']);
            
            for($i=$date_from[2];$i<=$date_to[2];$i++){
               
                Attendance::where('user_id',$user_id)->where('day',$i)->where('month',$date_from[1])->where('year',$date_from[0])->update(['timein_status'=>strtoupper(str_replace(' ', '', $request->project[0]['type'])),'timeout_status'=>strtoupper(str_replace(' ', '', $request->project[0]['type'])),'event_id'=>$request->project[0]['id']]);
            }
            if($cal->type =='Annual Leave'){
                $dt = date('Y-m-d h:i:s', strtotime("+6 months", strtotime(date('Y-m-d h:i:s'))));
                CalendarEvents::where('id', $request->project[0]['id'])->update(['approval' => $request->project[0]['act']]);
                PayrollSettings::where('user_id', $cal->user_id)->update(['_locked_until'=>$dt]);
            }else{
                CalendarEvents::where('id', $request->project[0]['id'])->update(['approval' => $request->project[0]['act']]);
            }
            
        }
        return  array('success' => true);
   }

   public function postInstallments(Request $request){
       $in = new Installments;
       $in->user_id=$request->user_id;
       $in->full_amount = $request->full_amount;
       $in->credit_type = $request->credit_type;
       $in->payment_terms = $request->payment_terms;
       $in->payment_count = $request->payment_count;
       $in->date_deduction = $request->date_deduction;
       $in->loan_reason = $request->reason;
       $in->status = "NOT PAID";
       $in->save();
   }
   public function postHoliday(Request $request){
   //\Log::info($request->inputs);

       $log = [];
        switch($request->method){
            case 'holiday':

                //if(CalendarEvents::where('start_date', 'like', '%'.$request->start_date.'%')->where('title', 'like', '%'.$request->event_title.'%')->where('year_added',date("Y"))->count()<=0){
                 
                  foreach($request->inputs as $input){
                    $datediff = (strtotime($input['date_to']) - strtotime($input['date_from']))/ (60 * 60 * 24);
                    
                    $yr = explode(" ", $input['date_from']);
                    $ce = new CalendarEvents;
                    $ce->user_id = 0;
                    $ce->start_date = $input['date_from'];
                    $ce->end_date = $input['date_to'];
                    $ce->title = $input['title'];
                    $ce->type = "Holiday";
                    $ce->approval = "Approved";
                    $ce->status = "Active";
                    $ce->year_added = $yr[0];
                    $ce->day_count = $datediff==0 ? 1 : $datediff;
                    $ce->rate = $input['rate'];
                    $ce->save();

                    $nid = CalendarEvents::orderBy('id','desc')->limit(1)->first()->id;
                    $df = date($input['date_from']);
                    $dt = date($input['date_to']);
                    $dt = date('Y-m-d',strtotime($dt. ' + 1 days'));  

                    do{
                        $d = date('d',strtotime($df));
                        $m = date('m',strtotime($df));
                        $y = date('Y',strtotime($df));
                        //\Log::info($d.'|'.$m.'|'.$y);
                        Attendance::where('day',$d)->where('month',$m)->where('year',$y)->update(['timein_status'=>'HOLIDAY','timeout_status'=>'HOLIDAY','event_id'=>$nid]);
                        if($df != $dt)
                            $df = date('Y-m-d',strtotime($df. ' + 1 days'));        
                    }while($df != $dt);
                  }
                    // $yr = explode(" ", $request->start_date);
                    // $ce = new CalendarEvents;
                    // $ce->user_id = 0;
                    // $ce->start_date = $request->start_date;
                    // $ce->end_date = $request->end_date;
                    // $ce->title = $request->event_title;
                    // $ce->type = "Holiday";
                    // $ce->approval = "Approved";
                    // $ce->status = "Active";
                    // $ce->year_added = $yr[0];
                    // $ce->day_count = $request->day_count;
                    // $ce->rate = $request->rate;
                    // $ce->save();
                    $log = array(
                        'message' => "Successfully added new holiday",
                        'success' => true);

                // }else{
                //     $log = array(
                //         'message' => "Date and title already exist",
                //         'success' => false);
                // }
            break;
            case 'leave':
                //$request->number_of_hours  = ($request->type=='Consume OT') ? $request->number_of_hours:0;
                //if(CalendarEvents::where('start_date', 'like', '%'.$request->start_date.'%')->where('title', 'like', '%'.$request->event_title.'%')->where('year_added',date("Y"))->where('user_id',Auth::user()->id)->count()<=0){
                
                        if($request->type=='Consume OT'){
                            if(!$this->isWeekendAndHoliday($request->start_date,$request->end_date)){
                                $yr = explode(" ", $request->start_date);
                                $ce = new CalendarEvents;
                                $ce->user_id = Auth::user()->id;
                                $ce->start_date = $request->start_date;
                                $ce->end_date = $request->end_date;
                                $ce->title =  Auth::user()->full_name.' '.$request->type;
                                $ce->type = $request->type;
                                $ce->approval = "Pending";
                                $ce->status = "Active";
                                $ce->year_added = $yr[0];
                                $ce->day_count = $request->day_count;
                                $ce->reason = $request->reason;
                                $ce->hour_count = $request->number_of_hours;
                                $ce->save();
                                $log = array(
                                    'message' => "Consume OT application is posted",
                                    'success' => true);
                            }else{
                                $log = array(
                                    'message' => "Leave application must not be weekend or holiday",
                                    'success' => false);
                            }
                        }else{
                            $flag2 = false;
                            //\Log::info($request->all());
                            foreach($request->inputs as $input){
                                $request->type = (($request->type == 'Leave with Permission') ? (($input['leave_option']=='deductible') ? $request->type : (($input['leave_option']=='consumeot' ? 'Consume OT' : 'Leave')) ) : $request->type);
                                $flag = $this->isWeekendAndHoliday($input['date_from'],$input['date_to']);
                                    if(!$flag){
                                        $datediff = (strtotime($input['date_to']) - strtotime($input['date_from']))/ (60 * 60 * 24);
                                        $dateloop = $datediff;
                                        //\Log::info($input['title']);
                                        $yr = explode(" ", $input['date_from']);
                                        $ce = new CalendarEvents;
                                        $ce->user_id = Auth::user()->id;
                                        $ce->start_date = $input['date_from'];
                                        $ce->end_date = $input['date_to'];
                                        $ce->title = Auth::user()->full_name.' '.$request->type;
                                        $ce->type = $request->type;
                                        $ce->approval = "Pending";
                                        $ce->status = "Active";
                                        $ce->year_added = $yr[0];
                                        $ce->day_count = $datediff==0 ? 1 : $datediff;
                                        $ce->reason = $input['reason'];
                                        $ce->time_start = $input['time_from'];
                                        $ce->time_end = $input['time_to'];
                                        $ce->hour_count = $this->timeDiff($input['time_from'],$input['time_to']);
                                        $ce->deductible = (($request->type == 'Leave with Permission') ? (($input['leave_option']=='deductible') ? true : false ) : false);
                                        $ce->save();
                                      
                                        for($i = 0; $i<=$dateloop;$i++){
                                            $this->deductTime($request->hour,$request->min,$request->type,$input['leave_option']);
                                        }
                                        
                                    
                                    }else{
                                        $flag2=true;
                                    }
                                
                            }
                            if($flag2){
                                $log = array(
                                    'message' => "Leave application(s) with weekend(s) will not be submitted",
                                    'success' => true);
                            }else{
                                $log = array(
                                    'message' => "Successfully requested leave",
                                    'success' => true);
                            }
                        }
                        
                // }else{
                //     $log = array(
                //         'message' => "Date of leave is already applied",
                //         'success' => false);
                // }
            break;
        }
        return $log;
   }
   private function addTime($start,$end,$schedule){
    $sched = PayrollSettings::where('user_id',$schedule->user_id)->first();
    $start = explode(":", $start);
    $end = explode(":", $end);
    $dif = explode(":", $this->timeDiff($start[0].":".$start[1],$end[0].":".$end[1]));
    $hour = $dif[0];
    $min = $dif[1]; 
    
    if($sched){
        if($schedule->type=='Sick Leave'){
            $time = explode(':', $sched->_sick_leave);
           
                $time[0] = $time[0] + $hour;
                $time[1] = $time[1] + $min;
                
                if($time[1]>=60){
                    $time[1] = $time[1]-60;
                    //$time[0] = $time[0]+1;
                }
                $time[0] = strlen($time[0]) > 1 ? $time[0] : '0'.$time[0];
                $time[1] = strlen($time[1]) > 1 ? $time[1] : '0'.$time[1];

                PayrollSettings::where('user_id',$schedule->user_id)->update(['_sick_leave'=>$time[0].':'.$time[1]]);
        }
        else if($schedule->type=='Annual Leave'){
            $time = explode(':', $sched->_annual_leave);
            
                $time[0] = $time[0] + $hour;
                $time[1] = $time[1] + $min;
                if($time[1]>60){
                    $time[1] = $time[1]-60;
                    $time[0] = $time[0]+1;
                }
                $time[0] = strlen($time[0]) > 1 ? $time[0] : '0'.$time[0];
                $time[1] = strlen($time[1]) > 1 ? $time[1] : '0'.$time[1];
               // \Log::info($time[0].":".$time[1]);
                PayrollSettings::where('user_id',$schedule->user_id)->update(['_annual_leave'=>$time[0].':'.$time[1]]);
        }
        else if($schedule->type=='Leave with Permission'){
            $time = explode(':', $sched->_overtime);
                $time[0] = $time[0] + $hour;
                $time[1] = $time[1] + $min;
                if($time[1]>60){
                    $time[1] = $time[1]-60;
                    $time[0] = $time[0]+1;
                }
                $time[0] = strlen($time[0]) > 1 ? $time[0] : '0'.$time[0];
                $time[1] = strlen($time[1]) > 1 ? $time[1] : '0'.$time[1];
                //\Log::info($time[0].":".$time[1]);
                PayrollSettings::where('user_id',$schedule->user_id)->update(['_overtime'=>$time[0].':'.$time[1]]);
        }   
    }
   }
   private function deductTime($hour,$min,$type,$type2){
        $sched = PayrollSettings::where('user_id',Auth::user()->id)->first();
        if($sched){
            if($type=='Sick Leave'){
                $time = explode(':', $sched->_sick_leave);
                $time[0] = $time[0] - $hour;
                if($time[1] < $min){
                    $tmp = $min - $time[1];
                    $tmp2 = ($time[0]*60)-$tmp;
                    $time[1] = $tmp2%60;
                    $time[0] = $time[0] -1;
                }else{
                    $time[1] = $time[1] - $min;
                }
                $time[0] = strlen($time[0]) > 1 ? $time[0] : '0'.$time[0];
                $time[1] = strlen($time[1]) > 1 ? $time[1] : '0'.$time[1];
                //\Log::info($time[0].":".$time[1]);
                PayrollSettings::where('user_id',Auth::user()->id)->update(['_sick_leave'=>$time[0].':'.$time[1]]);
            }
            else if($type=='Annual Leave'){
                $time = explode(':', $sched->_annual_leave);
                $time[0] = $time[0] - $hour;
                if($time[1] < $min){
                    $tmp = $min - $time[1];
                    $tmp2 = ($time[0]*60)-$tmp;
                    $time[1] = $tmp2%60;
                    $time[0] = $time[0] -1;
                }else{
                    $time[1] = $time[1] - $min;
                }
                $time[0] = strlen($time[0]) > 1 ? $time[0] : '0'.$time[0];
                $time[1] = strlen($time[1]) > 1 ? $time[1] : '0'.$time[1];
                //\Log::info($time[0].":".$time[1]);
                PayrollSettings::where('user_id',Auth::user()->id)->update(['_annual_leave'=>$time[0].':'.$time[1]]);
            }
            else if($type=='Leave with Permission' && $type2=='consumeot'){
                $time = explode(':', $sched->_overtime);
                $time[0] = $time[0] - $hour;
                if($time[1] < $min){
                    $tmp = $min - $time[1];
                    $tmp2 = ($time[0]*60)-$tmp;
                    $time[1] = $tmp2%60;
                    $time[0] = $time[0] -1;
                }else{
                    $time[1] = $time[1] - $min;
                }
                $time[0] = strlen($time[0]) > 1 ? $time[0] : '0'.$time[0];
                $time[1] = strlen($time[1]) > 1 ? $time[1] : '0'.$time[1];
                //\Log::info($time[0].":".$time[1]);
                PayrollSettings::where('user_id',Auth::user()->id)->update(['_overtime'=>$time[0].':'.$time[1]]);
            }
        }
        // if($type=='Sick Leave' || $type=='Annual Leave' || ($type=='Leave with Permission' && $type2=='consumeot')){

        // }

   }
   private function timeDiff($time1,$time2){
        if(preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/", $time1) && preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]$/", $time2)){
            list($hours, $minutes) = explode(':', $time1);
            $startTimestamp = mktime($hours, $minutes);
            
            list($hours, $minutes) = explode(':', $time2);
            $endTimestamp = mktime($hours, $minutes);
            
            $seconds = $endTimestamp - $startTimestamp;
            $minutes = ($seconds / 60) % 60;
            $hours = round($seconds / (60 * 60));
            
            return ($hours-1).':'.$minutes;
        }else{
            return '00:00';
        }
    
   }
   private function isWeekendAndHoliday($date1, $date2) {
        $flag = false;
        $df = date($date1);
        $dt = date($date2);
        $dt = date('Y-m-d',strtotime($dt. ' + 1 days'));  
        
        do{
            $d = date('d',strtotime($df));
            $m = date('m',strtotime($df));
            $y = date('Y',strtotime($df));
            //\Log::info($df);
            $weekDay = date('w', strtotime($df));
            
            if($weekDay == 0 || $weekDay == 6) $flag = true;
            if($df != $dt)
                $df = date('Y-m-d',strtotime($df. ' + 1 days'));        
        }while($df != $dt);

        return $flag;
   }

   private function getAttendance2($year,$month,$cutoff,$id){
        if($cutoff==10){
            $from = Carbon::create($year, $month, 11);
            $to = Carbon::create($year, $month, 26);
            $ret = Attendance::where('user_id',$id)->whereBetween('created_at', [$from->subMonth()->toDateString(), $to->subMonth()->toDateString()])->orderBy('created_at', 'asc')->get();
        }else{
            $from = Carbon::create($year, $month, 26);
            $to = Carbon::create($year, $month, 11);
            $ret = Attendance::where('user_id',$id)->whereBetween('created_at',[$from->subMonth()->toDateString(), $to->toDateString()])->orderBy('created_at', 'asc')->get();
        }
            return $ret;
    }
    private function notWeekend($date) {
        $weekDay = date('w', strtotime($date));
        return ($weekDay == 0 || $weekDay == 6);
    }
    private function getWorkingDays($startDate,$endDate){
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);
    
    
        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;
    
        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);
    
        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);
    
        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        }
        else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)
    
            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;
    
                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            }
            else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }
    
        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
    //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
       $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0 )
        {
          $workingDays += $no_remaining_days;
        }
    
        //We subtract the holidays
        // foreach($holidays as $holiday){
        //     $time_stamp=strtotime($holiday);
        //     //If the holiday doesn't fall in weekend
        //     if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
        //         $workingDays--;
        // }
    
        return $workingDays;
    }
    private function getAbsent($date,$stat) {
        $weekDay = date('w', strtotime($date));
        if(($weekDay != 0 && $weekDay != 6) && $stat=='ABSENT' || $stat=="LEAVEWITHOUTPERMISSION"){
            return 2;
        }else if(($weekDay != 0 && $weekDay != 6) && $stat=="LEAVEWITHPERMISSION"){
            return 1;
        }else{
            return 0;
        }
    }
    public function reCompute(Request $request){
        $get_old_login = Attendance::where('user_id',$request->id)->orderBy('created_at', 'desc')->first()->created_at;
        if($get_old_login!=null){
            $old_month = date("m",strtotime($get_old_login));
            $old_year = date("Y",strtotime($get_old_login));
            $new_month = date("m");
            $new_year = date("Y");
            $cur_date = date("d");
            //\Log::info($new_month);
            // if($old_year < 2019){
            //     $new_month = 7;
            //     $new_year = 2019;
            // }else{
            //     if($old_month>7){
            //         $new_month = $old_month;
            //     }else{
            //         $new_month=7;
            //     }
            // }
            
            $cur_year = $new_year;
            $cur_month = 6;
            $cutoff = 10;
            if($cur_date>10) $new_month++;
                
                //for($i=2;$i<$new_month;$i++){
                    Mick:
                    
                    //$cur_month = $i;
                    $attendance = $this->getAttendance2($cur_year,$cur_month,$cutoff,$request->id);
                    $setting = PayrollSettings::where('user_id', $request->id)->first();
                    $time = Schedule::where('user_id', $request->id)->first();
                    $c_month = date("m",strtotime($attendance->first()->created_at));

                    $basic_pay = $setting->_basic_pay;
                    $hours_difference = (abs(strtotime($time->time_in) - strtotime($time->time_out))/3600)-1;
                    $hours_per_day = $setting->_working_hours;
                    $working_day = 0;
                    $absent = 0;
                    $late_hour = 0;
                    $late_min = 0;
                    $late_hour2 = 0;
                    $late_min2= 0;
                    $absent2 = 0;
                    $min = 0;
                    $hr = 0;
                    //floor((abs(strtotime('09:00:00') - strtotime('10:30:30')) / 60) % 60); min computation
                    //floor(abs(strtotime('09:00:00') - strtotime('10:01:00'))/3600); hour computation
                    foreach($attendance as $at){
                        if($c_month==date("m",strtotime($at->created_at))){
                            $absent = $absent+$this->getAbsent($at->created_at,$at->timein_status);
                        }else{
                            $absent2 = $absent2+$this->getAbsent($at->created_at,$at->timein_status);
                        }
                            if($this->notWeekend($at->created_at)!=1){
                                    if($at->time_in!='' || $at->time_in!=null && $at->time_out!='' || $at->time_out!=null){
                                        if($time->schedule_type_id==1){//fixed
                                            if(strtotime($time->time_in) < strtotime($at->time_in)){
                                                if($c_month==date("m",strtotime($at->created_at))){
                                                    $hr = floor(abs(strtotime($time->time_in) - strtotime($at->time_in))/3600);
                                                    $min = floor(abs((strtotime($time->time_in) - strtotime($at->time_in))/60) % 60);
                                                    
                                                    // \Log::info($time->time_in.'+'.$at->time_in);
                                                    // \Log::info($hr.'hr+'.$min);
                                                    if($at->event_id!=0){
                                                        
                                                        $vnt = CalendarEvents::where('id',$at->event_id)->first();
                                                        if($vnt){
                                                            
                                                            if($vnt->type=="Consume OT"){
                                                                $whole = floor($vnt->hour_count);     
                                                                $fraction = $vnt->hour_count - $whole;
                                                                $hr = $hr - $whole;
                                                                $fraction = ($fraction>0) ? 30:0;
                                                                $min = ($min>0) ? 30:0;
                                                                $min = $min-$fraction;
                                                            }
                                                        }
                                                    }
                                                    if($min>30){
                                                        $min = 0;
                                                        $hr = $hr+1;
                                                    }else if($min>0){
                                                        $min = 30;
                                                      
                                                    }
                                                   
                                                    
                                                    if($hr>0){
                                                        $late_hour = $late_hour + $hr;
                                                    }
                                                    if($min>0){
                                                        $late_min = $late_min + $min; 
                                                    }
                                                    // \Log::info($time->time_in.'+'.$at->time_in);
                                                    // \Log::info($hr.'hr+'.$min);
                                                    // \Log::info($late_hour.'late+'.$late_min);
                                                }else{
                                                    $hr = floor(abs(strtotime($time->time_in) - strtotime($at->time_in))/3600);
                                                    $min = floor(abs((strtotime($time->time_in) - strtotime($at->time_in))/60) % 60);
                                                    
                                                    
                                                    if($at->event_id!=0){
                                                        $vnt = CalendarEvents::where('id',$at->event_id)->first();
                                                        if($vnt){
                                                            if($vnt->type=="Consume OT"){
                                                                $whole = floor($vnt->hour_count);     
                                                                $fraction = $vnt->hour_count - $whole;
                                                                $hr = $hr - $whole;
                                                                $fraction = ($fraction>0) ? 30:0;
                                                                $min = ($min>0) ? 30:0;
                                                                $min = $min-$fraction;
                                                            }
                                                        }
                                                    }
                                                    if($min>30){
                                                        $min = 0;
                                                        $hr = $hr+1;
                                                    }else if($min>0){
                                                        $min = 30;
                                                    }

                                                    if($hr>0){
                                                        $late_hour2 = $late_hour2 + $hr;
                                                    }
                                                    if($min>0){
                                                        $late_min2 = $late_min2 + $min; 
                                                    }
                                                }
                                            
                                            }
                                        }else if($time->schedule_type_id==2){//flexi
                                            if(strtotime($time->time_in_to) < strtotime($at->time_in)){
                                                if($c_month==date("m",strtotime($at->created_at))){
                                                    $hr = floor(abs(strtotime($time->time_in_to) - strtotime($at->time_in))/3600);
                                                    $min = floor(abs((strtotime($time->time_in) - strtotime($at->time_in))/60) % 60);
                                                    
                                                   
                                                    if($at->event_id!=0){
                                                        $vnt = CalendarEvents::where('id',$at->event_id)->first();
                                                        if($vnt){
                                                            if($vnt->type=="Consume OT"){
                                                                $whole = floor($vnt->hour_count);     
                                                                $fraction = $vnt->hour_count - $whole;
                                                                $hr = $hr - $whole;
                                                                $fraction = ($fraction>0) ? 30:0;
                                                                $min = ($min>0) ? 30:0;
                                                                $min = $min-$fraction;
                                                            }
                                                        }
                                                    }
                                                    if($min>30){
                                                        $min = 0;
                                                        $hr = $hr+1;
                                                    }else if($min>0){               
                                                        $min = 30;
                                                        
                                                    }
                                                    if($hr>0){
                                                        $late_hour = $late_hour + $hr;
                                                    }
                                                    if($min>0){
                                                        $late_min = $late_min + $min; 
                                                    }
                                                }else{
                                                    $hr = floor(abs(strtotime($time->time_in_to) - strtotime($at->time_in))/3600);
                                                    $min = floor(abs((strtotime($time->time_in) - strtotime($at->time_in))/60) % 60);
                                                    
                                                    
                                                    if($at->event_id!=0){
                                                        $vnt = CalendarEvents::where('id',$at->event_id)->first();
                                                        if($vnt){
                                                            if($vnt->type=="Consume OT"){
                                                                $whole = floor($vnt->hour_count);     
                                                                $fraction = $vnt->hour_count - $whole;
                                                                $hr = $hr - $whole;
                                                                $fraction = ($fraction>0) ? 30:0;
                                                                $min = ($min>0) ? 30:0;
                                                                $min = $min-$fraction;
                                                            }
                                                        }
                                                    }
                                                    if($min>30){
                                                        $min = 0;
                                                        $hr = $hr+1;
                                                    }else if($min>0){
                                                        $min = 30;
                                                    }

                                                    if($hr>0){
                                                        $late_hour2 = $late_hour2 + $hr;
                                                    }
                                                    if($min>0){
                                                        $late_min2 = $late_min2 + $min; 
                                                    }
                                                }
                                            }
                                        }else{//freetime
                                            $late_hour = 0;
                                            $late_min = 0;
                                            $late_hour2 = 0;
                                            $late_min2= 0;
                                        }
                                        

                                    }

                                    // if($late_min>=60){
                                    //         $late_min = $late_min-60;
                                    //         $late_hour = $late_hour+1;
                                    // }

                                $working_day++;
                            }
                            
                    }
                    if($cutoff==10){
                        //yyyy-mm-dd
                        $days = $this->getWorkingDays($cur_year.'-'.($cur_month-1).'-01',$cur_year.'-'.($cur_month-1).'-'.cal_days_in_month(CAL_GREGORIAN, ($cur_month-1), $cur_year));
                        $bonus = (($setting->_bonus/$days)/$hours_per_day)*($hours_difference*$working_day);
                        $bonus_after = $bonus;
                        $daily = (($basic_pay/$days)/$hours_per_day)*$hours_difference;
                        $daily_bunos = (($setting->_bonus/$days)/$hours_per_day)*$hours_difference;
                        $basic_absent  = $daily*$absent;
                        $bonus_absent = $daily_bunos*$absent;
                        $absent_deduction = $basic_absent+$bonus_absent;
                        $late_min = ($late_min>0 && $late_min<=30) ? 1 : 0;
                        $basic_late_deduction = (($daily/$hours_per_day)*$late_hour) + ((($daily/$hours_per_day)/2)*$late_min);
                        $bonus_late_deduction = (($daily_bunos/$hours_per_day)*$late_hour) + ((($daily_bunos/$hours_per_day)/2)*$late_min);
                        $total_deduction = $setting->_sss/2+$setting->_pag_ibig/2+$setting->_phil_health/2+$basic_late_deduction+$bonus_late_deduction+$absent_deduction;
                        $basic_earning_no_deductions = $daily*$working_day;
                        $basic_earnings = $basic_earning_no_deductions-$total_deduction;
                        $total_earnings = $basic_earning_no_deductions+$bonus+$setting->_allowance;
                        $total_salary = ($total_earnings-$total_deduction);
                        
                        $salary_tax = (($basic_earnings-1500>=10417 && $basic_earnings-1500<16667) ? (($basic_earnings-1500)-10417)*0.20: (($basic_earnings-1500>=16667 && $basic_earnings-1500<33333) ? (($basic_earnings-1500)-16667*0.25)+1250 : (($basic_earnings-1500>=33333) ? (($basic_earnings-1500)-33333*0.30)+5416.67 :0 )));
                        
                        $total_salary = $total_salary - $salary_tax;
                        $total_salary = number_format($total_salary, 2, '.', '');

                        // \Log::info('Late hour||'.$late_hour);
                        // \Log::info('Late min||'.$late_min);
                        // \Log::info('Days||'.$days);
                        // \Log::info('Salary||'.$basic_pay);
                        // \Log::info('Daily||'.$daily);
                        // \Log::info('Absent||'.$absent_deduction);
                        // \Log::info('Late||'.$basic_late_deduction.'||'.$bonus_late_deduction);
                        // \Log::info($total_deduction.'||'.$salary_tax);
                        // \Log::info(number_format($total_deduction+$salary_tax, 2, '.', ''));
                    }else{
                        $setting->_allowance = 0;
                        $day1 = $this->getWorkingDays($cur_year.'-'.($cur_month-1).'-01',$cur_year.'-'.($cur_month-1).'-'.cal_days_in_month(CAL_GREGORIAN, ($cur_month-1), $cur_year));
                        $day2 = $this->getWorkingDays($cur_year.'-'.$cur_month.'-01',$cur_year.'-'.$cur_month.'-'.cal_days_in_month(CAL_GREGORIAN, $cur_month, $cur_year));
                        $workingday1 = $this->getWorkingDays($cur_year.'-'.($cur_month-1).'-26',$cur_year.'-'.($cur_month-1).'-'.cal_days_in_month(CAL_GREGORIAN, ($cur_month-1), $cur_year));
                        $workingday2 =  $this->getWorkingDays($cur_year.'-'.$cur_month.'-01',$cur_year.'-'.$cur_month.'-10');
                        $daily1 = (($basic_pay/$day1)/$hours_per_day)*$hours_difference;
                        $daily2 = (($basic_pay/$day2)/$hours_per_day)*$hours_difference;
                        $daily_bunos1 = (($setting->_bonus/$day1)/$hours_per_day)*$hours_difference;
                        $daily_bunos2 = (($setting->_bonus/$day2)/$hours_per_day)*$hours_difference;
                        $late_min = ($late_min>0 && $late_min<=30) ? 1 : 0;
                        $late_min2 = ($late_min2>0 && $late_min2<=30) ? 1 : 0;
                        $basic_late_deduction = (($daily1/$hours_per_day)*$late_hour) + ((($daily1/$hours_per_day)/2)*$late_min)+(($daily2/$hours_per_day)*$late_hour2) + ((($daily2/$hours_per_day)/2)*$late_min2);
                        $bonus_late_deduction = (($daily_bunos1/$hours_per_day)*$late_hour) + ((($daily_bunos1/$hours_per_day)/2)*$late_min)+(($daily_bunos2/$hours_per_day)*$late_hour2) + ((($daily_bunos2/$hours_per_day)/2)*$late_min2);
                        $basic_absent  = ($daily1*$absent)+($daily2*$absent2);
                        $bonus_absent = ($daily_bunos1*$absent)+($daily_bunos2*$absent2);
                        $absent_deduction = $basic_absent+$bonus_absent;
                        $bonus = (($setting->_bonus/$day1)/$hours_per_day)*($hours_difference*$workingday1)+(($setting->_bonus/$day2)/$hours_per_day)*($hours_difference*$workingday2);
                        $bonus_after = $bonus;
                        $total_deduction = $setting->_sss/2+$setting->_pag_ibig/2+$setting->_phil_health/2+$basic_late_deduction+$bonus_late_deduction+$absent_deduction;
                        $basic_earning_no_deductions = $daily1*$workingday1+$daily2*$workingday2;
                        $basic_earnings = $basic_earning_no_deductions-$total_deduction;
                        $total_earnings = $basic_earning_no_deductions+$bonus+$setting->_allowance;
                        $total_salary = ($total_earnings-$total_deduction);
                        $salary_tax = (($basic_earnings-1500>=10417 && $basic_earnings-1500<16667) ? (($basic_earnings-1500)-10417)*0.20: (($basic_earnings-1500>=16667 && $basic_earnings-1500<33333) ? (($basic_earnings-1500)-16667*0.25)+1250 : (($basic_earnings-1500>=33333) ? (($basic_earnings-1500)-33333*0.30)+5416.67 :0 )));
                        $total_salary = $total_salary - $salary_tax;
                        $total_salary = number_format($total_salary, 2, '.', '');
                        
                        
                    }
                    

                    $pl = new PayrollLogs;
                    $pl->user_id = $request->id;
                    $pl->_total_deductions = number_format($total_deduction+$salary_tax, 2, '.', '');
                    $pl->_basic_pay = $setting->_basic_pay;
                    $pl->_bonus = $setting->_bonus;
                    $pl->_month = $cur_month;
                    $pl->_date = $cutoff;
                    $pl->_year = $cur_year;
                    $pl->_allowance = $setting->_allowance;
                    $pl->_absent_count = $absent+$absent2;
                    $pl->_late_count = 0;
                    $pl->_holiday_count = 0;
                    $pl->_leave_count = 0;
                    $pl->_total_pay = $request->_total_pay;
                    $pl->_total_earnings = $total_salary;
                    //$cur_date<25 && 
                    if($cur_month<$new_month){
                        if($cur_month==$new_month-1){
                            if($cur_date>$cutoff){
                                $pl->save();
                            }
                        }else{
                            $pl->save();
                        }
                    }
                    
                    if($cutoff==10){
                        $cutoff=25;
                        goto Mick;
                    }else{
                        $cutoff=10;
                    }
                    if($cur_month<$new_month){
                        $cur_month++;
                        
                        goto Mick;
                    }

                  
                    
           //}

        }
        

        
        
        //dd();
    }
}

// this.time_start = this.profile[0].user.schedule.time_in;
//         this.time_end = this.profile[0].user.schedule.time_out;
//         this.hours_per_day = this.profile[0].user.payroll_settings._working_hours;
//         this.working_days = this.profile[0].user.payroll_settings._working_days;
//         this.schedule_type = this.profile[0].user.schedule.schedule_type.id;
//         this.basic_pay = this.profile[0].user.payroll_settings._basic_pay;
//         this.bonus = this.profile[0].user.payroll_settings._bonus;
//         this._sss = this.profile[0].user.payroll_settings._sss/2;
//         this._pag_ibig = this.profile[0].user.payroll_settings._pag_ibig/2;
//         this._phil_health = this.profile[0].user.payroll_settings._phil_health/2;
//         this.allowance = this.profile[0].user.payroll_settings._allowance;
//         this.hours_difference = Math.abs((new Date(this.nyear+'/'+this.nmonth+'/01'+' '+this.time_start) - new Date(this.nyear+'/'+this.nmonth+'/01'+' '+this.time_end)) / 36e5)-1;
//         this.employment_date = this.profile[0].user.payroll_settings._employment_date;