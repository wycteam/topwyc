<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Shopping\Order;
use App\Models\Shopping\Store;
use App\Models\Shopping\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ReasonValidation;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Visa\ClientService;
use App\Models\Visa\Task;
use App\Models\RoleUser as role_users;
use App\Models\Role as role;
use App\Models\Visa\DeliverySchedule as deliveryschedule;
use App\Models\Visa\VisaType;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

class ClientServicesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setLocale($locale = 'en') {
        return response(['success'=> true, 'activeLocale' => $locale], 200)->withCookie(cookie()->forever('locale', $locale));
    }

    public function getLocale() {
        $locale = (\Request::cookie('locale')) ? \Request::cookie('locale') : 'en';
        return response(['success' => true, 'activeLocale'=> $locale], 200)->withCookie(cookie()->forever('locale', $locale));
    }


    public function translation() {
        return json_encode([
            'translation' => trans('cpanel/todays-services-component')
        ]);
    }

    public function deliveryScheduleTranslation() {
        return json_encode([
            'translation' => trans('cpanel/delivery-schedule-component')
        ]);
    }

    public function editServiceDashboardTranslation() {
        return json_encode([
            'translation' => trans('cpanel/edit-service-dashboard-component')
        ]);
    }

    public function index()
    {
        $title = trans('cpanel/dashboard');
        $role_id = 9;
        $yesterday = date("m/d/Y", strtotime( '-1 days' ) );
        $data = array(
            'title' => $title['dashboard'],
            'total_client' =>$this->formatMoney(User::whereHas('roles', function ($query) use ($role_id) {
                $query->where('roles.id', '=', $role_id);
            })->where(function ($query) {
                  $query->where('branch_id', Auth::user()->branch_id)
                        ->orwhere('branch_id', '3');
              })->count()),

            'total_service' => $this->formatMoney(ClientService::whereHas('user',function($q){
                 $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
              })->count()),
            'total_service_new' => $this->formatMoney(ClientService::where('service_date', date('m/d/Y'))->where('active',1)->whereHas('user',function($q){
                 $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
              })->count()),
            'total_service_yesterday' => $this->formatMoney(ClientService::where('service_date', $yesterday)->where('active',1)->whereHas('user',function($q){
                 $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
              })->count())
        );

        return view('cpanel.visa.dashboard.index', $data);
    }



    public function formatMoney($number, $fractional=false) {
        if ($fractional) {
            $number = sprintf('%.2f', $number);
        }
        while (true) {
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
            if ($replaced != $number) {
                $number = $replaced;
            } else {
                break;
            }
        }
        return trim($number);
    }



    public function onprocessServices() {

        return ClientService::where('client_services.status','on process')->where('client_services.active', '1')
                ->where(function($query) {
                    return $query->where('checked', '0')
                        ->orWhere('checked', NULL);
                })->whereHas('user',function($q){
                     $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
                })->leftJoin('services','services.id','=','client_services.service_id')->where('services.parent_id','!=',0)
                  ->join('wycgroup.users', 'client_services.client_id', '=', 'wycgroup.users.id')
                  ->get();
    }




    public function getPendingServices(){

      $title = trans('cpanel/dashboard');

      $data = array(
          'title' => $title['dashboard']
      );
      return view('cpanel.visa.client-services.pending-services', $data);
    }


    public function getTodayServices($request) {

      $title = trans('cpanel/dashboard');

      $data = array(
          'title' => $title['dashboard'],
          'mode' => $request
      );

      return view('cpanel.visa.client-services.todays-services', $data);
    }


    public function getServiceByDate(Request $request) {
        $date = $request->dateFilter;
        $services = ClientService::with('user','group', 'discount2')
        ->where('service_date',$date)->where('active', '1')->get();
        return json_encode([
            'success' => true,
            'services' => $services
        ]);
    }


    public function getOnProcessServices(Request $request) {

      $title = trans('cpanel/dashboard');

      $data = array(
          'title' => $title['dashboard']
      );

      return view('cpanel.visa.client-services.on-process-services', $data);
    }



    public function removeService($id) {

        return ClientService::with('user')
                ->where('status','on process')
                ->where('checked','0')
                ->where('id',$id)
                ->update(['checked' => '1']);;

    }

    public function markAsPending(ReasonValidation $request) {

        $data = $request->all();
        return ClientService::with('user')
                ->where('status','on process')
                ->where('checked','0')
                ->where('id',$data['id'])
                ->update(['status' => 'pending','reason' => $data['reason']]);;

    }

    public function getTodaysTasks(Request $request) {
        $dateFilter = $request->dateFilter;

        $results = Task::with('clientService.user', 'clientService.service_category', 'whoIsInChargeRelation')
            ->whereHas('clientService', function($query) {
                $query->where('active', 1)->where('status', 'on process');
            })
            ->whereDate('date', '>=', $dateFilter);

        if($request->employeeFilter != 'All') {
            $results = $results->where('who_is_in_charge', $request->employeeFilter);
        }

        $results = $results->orderBy('date')->get()->groupBy('date');

        $arr = [];
        foreach($results as $key => $value) {
            $temp = [];

            $value = json_decode($value);

            $value = collect($value)->sortBy(function($item, $key) {
                return $item->client_service->service_category->parent_id;
            })->values()->all();

            for($i=0; $i<count($value); $i++) {
                $temp[$value[$i]->client_service->client_id][] = $value[$i];
            }

            $estimatedCostPerDay = 0;
            foreach($temp as $a => $b) {
                foreach($b as $c) {
                    $estimatedCostPerDay += $c->estimated_cost;
                }
            }

            $arr[] = [
                'date' => $key,
                'estimatedCostPerDay' => $estimatedCostPerDay,
                'data' => $temp
            ];
        }

        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        $perPage = 7;

        $paginatedItems = array_slice($arr, ($currentPage * $perPage) - $perPage, $perPage);

        return $paginatedItems;
    }

    public function getReminders(Request $request) {
        $data = User::
            // For 9A
            where(function($query) {
                $query->where('visa_type', '9A')
                    ->where(function($query2) {
                        $query2->whereRaw('exp_date <= DATE_ADD(CURDATE(), INTERVAL 7 DAY)')
                            ->orWhereRaw('first_exp_date <= DATE_ADD(CURDATE(), INTERVAL 7 DAY)');
                    });
            })
            // For 9G, TRV and CWV
            ->orWhere(function($query) {
                $query->where('visa_type', '<>', '9A')
                    ->where(function($query3) {
                            $query3->whereRaw('exp_date <= DATE_ADD(CURDATE(), INTERVAL 30 DAY)');
                        });
            })
            ->orderBy('id')
            ->paginate(5);

        return response()->json($data);
    }

    public function loadCompanyCouriers(){
        $role = role::where('name', 'company-courier')->first();
        $role_users = role_users::where('role_id',$role->id)->with('user')->get();
        return json_encode($role_users);
    }

    public function addDeliverySchedule(Request $request){
        $data = $request->all();
        $schedule = new deliveryschedule();
        $schedule->fill($data)->save();
        return $schedule;
    }

    public function deleteSchedule($id){
        $data = deliveryschedule::findOrFail($id);
        $data->delete();
        return response()->json($data);
    }

    public function getSchedulesbyDate(Request $request) {
        $date = $request->dateFilter;
        $schedules = deliveryschedule::with('rider')->where('scheduled_date', $date)->get();
        return json_encode([
            'success' => true,
            'schedules' => $schedules
        ]);
    }

    public function editSchedule(Request $request){
        $data = deliveryschedule::whereId($request->id)->update([
             'item'           => $request->item
            ,'rider_id'       => $request->rider_id
            ,'location'       => $request->location
            ,'scheduled_date' => $request->scheduled_date
            ,'scheduled_time' => $request->scheduled_time
            ]);

        return response()->json($data);
    }

    public function getSched($id){
        $data = deliveryschedule::findOrFail($id);
        return response()->json($data);
    }


}
