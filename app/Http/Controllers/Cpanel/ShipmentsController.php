<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Shopping\Order;
use App\Models\Rates;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShipmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rates = Rates::get();
        session()->flash('rates', $rates);
        return view('cpanel/shipments/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Order::with(['details.product.store', 'user', 'address'])
                        ->where('id', $id)
                        ->first();

        $data['full_address'] = $data['address']['address']." ".
                                $data['address']['barangay']." ".
                                $data['address']['city']." ".
                                $data['address']['province']." ".
                                $data['address']['country']." ".
                                $data['address']['zip_code'];
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
