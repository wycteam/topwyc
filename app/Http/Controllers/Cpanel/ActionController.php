<?php

namespace App\Http\Controllers\Cpanel;

use DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActionController extends Controller
{
    public function index(){
    	$actions = DB::connection('visa')->table('actions')->select(array('id', 'name'))->orderBy('name')->get();

    	$actionCategory = [];
    	foreach($actions as $action) {
    		$categoryIds = DB::connection('visa')->table('action_category')->where('action_id', $action->id)->pluck('category_id');
    		$data = DB::connection('visa')->table('categories')->select(array('id'))->whereIn('id', $categoryIds)->orderBy('name')->get();

    		$categories = [];
    		foreach($data as $d) {
    			$categories[] = DB::connection('visa')->table('categories')->select(array('id', 'name', 'name_cn'))->where('id', $d->id)->first();
    		}

    		$actionCategory[] = [
    			'action' => $action,
    			'categories' => $categories
    		];
    	}

    	return $actionCategory;
    }
}
