<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Visa\Ads;
use Image;
use File;
class AdsController extends Controller
{
    public function getAds(){
        return view('cpanel.visa.ads.index');
    }

    public function index(Request $request){

        $columns = ['title', 'sizes', 'created_at'];
            $length = $request->input('length');
            $column = $request->input('column'); //Index
            $dir = $request->input('dir');
            $searchValue = $request->input('search');
            $query = Ads::select('id','title','sizes','created_at','status')->orderBy($columns[$column], $dir);
            
            if ($searchValue) {
                $query->where(function($query) use ($searchValue) {
                    $query->where('title', 'like', '%' . $searchValue . '%');
                });
            }
            $projects = $query->paginate($length);
            return ['data' => $projects, 'draw' => $request->input('draw')];
      }
    public function show($id){
         $ret = Ads::findOrFail($id);  
         $ret->image = base64_encode($ret->image);
         $ret->unserialized = unserialize($ret->serialized);
         foreach($ret->unserialized as $d){
            $d->path = url('/').'/images/ads/'.$d->name;
         }
         return $ret;
   }
   public function store(Request $request){
        date_default_timezone_set("Asia/Manila");

        $path = public_path('/images/ads');
        if(!File::exists($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
        $ndata = $request->all();
        //\Log::info(Image::make(file_get_contents($ndata['image'])));
        $image = $ndata['image'];  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $ndata['image'] = base64_decode($image);
        //create seialized
        $size_counter = explode(",", trim($ndata['sizes']));
        $serialized = array();
        for($i=0;$i<count($size_counter);$i++){
            $nImage = \Image::make($request->get('image'));
            $sizes =  explode(":",$size_counter[$i]);
            $nImage->resize($sizes[1], $sizes[0]);//w,h
            $file_name = round(microtime(true) * 1000).'.png';
            $nImage->save($path.'/'.$file_name);
            $object = (object) [
                'name' => $file_name ,
                'width' => $sizes[1],
                'height' => $sizes[0],
            ];
            array_push($serialized,$object);
        }
        $ndata['serialized'] = serialize($serialized);

         Ads::create($ndata);
         return response()->json($request->all());
  }

  public function update(Request $request, $id){
    date_default_timezone_set("Asia/Manila");
 
    $ndata = $request->all();
    
    switch($ndata['method']){
        case 'title':
            Ads::findOrFail($id)->update(['title' => $ndata['title']]);
            break;
        case 'status':
            Ads::findOrFail($id)->update(['status' => $ndata['status']]);
            break;
        case 'item':
                $path = public_path('/images/ads');
                $ret = Ads::findOrFail($id);
                $sizes = explode(',', $ret->sizes);
                $serialized = unserialize($ret->serialized);
                $img_name = null;
                $tmp_w=0;
                $tmp_h=0;

                for($i=0;$i<count($sizes);$i++){
                    if($i==$ndata['index']){
                        $sizes[$i]=$ndata['height'].":".$ndata['width'];
                    }
                }
                foreach($serialized as $key=>$value){
                    if($key==$ndata['index']){
                        $img_name = $value->name;
                        $tmp_w = $value->width;
                        $tmp_h = $value->height;
                        $value->height = $ndata['height'];
                        $value->width = $ndata['width'];
                    }
                }
                if(isset($ndata['image'])){
                    $nImage = \Image::make($ndata['image']);
                    $nImage->resize($ndata['width'], $ndata['height']);//w,h
                    File::delete($path.'/'.$img_name);
                    $nImage->save($path.'/'.$img_name);
                }else{
                    if($tmp_w!=$ndata['width'] || $tmp_h!=$ndata['height']){
                        $nImage = \Image::make($path.'/'.$img_name);
                        $nImage->resize($ndata['width'], $ndata['height']);
                        File::delete($path.'/'.$img_name);
                        $nImage->save($path.'/'.$img_name);
                    }
                }
                
                Ads::findOrFail($id)->update(['sizes' => implode(",", $sizes),'serialized' => serialize($serialized)]);
            
            break;

    }
    // $path = public_path('/images/ads');
    // if(!File::exists($path)) {
    //     File::makeDirectory($path, 0777, true, true);
    // }
        
    //     $ndata = $request->all();
        
    //     $image = $ndata['image'];  // your base64 encoded
    //     $image = str_replace('data:image/png;base64,', '', $image);
    //     $image = str_replace(' ', '+', $image);
    //     $ndata['image'] = base64_decode($image); 
    //     //create seialized
    //     $size_counter = explode(",", trim($ndata['sizes']));
    //     $serialized = array();
    //     for($i=0;$i<count($size_counter);$i++){
    //         // $sizes =  explode(":",$size_counter[$i]);
    //         // $background = \Image::canvas($sizes[1], $sizes[0]);
    //         // $file_name = round(microtime(true) * 1000).'.png';
    //         // $nImage = \Image::make($request->get('image'))->resize($sizes[1], $sizes[0], function ($c) {
    //         //     $c->aspectRatio();
    //         //     $c->upsize();
    //         // });
    //         // $nImage->save($path.'/'.$file_name);
    //         // $background->insert($nImage, 'fill');
    //         // $background->save($path.'/'.$file_name);
            
    //         $nImage = \Image::make($request->get('image'));
    //         $sizes =  explode(":",$size_counter[$i]);
    //         $nImage->resize($sizes[1], $sizes[0]);//w,h
    //         $file_name = round(microtime(true) * 1000).'.png';
    //         $nImage->save($path.'/'.$file_name);

    //         $object = (object) [
    //             'name' => $file_name ,
    //             'width' => $sizes[1],
    //             'height' => $sizes[0],
    //         ];
    //         array_push($serialized,$object);
    //     }
    //     $ndata['serialized'] = serialize($serialized);

    //     Ads::findOrFail($id)->update($ndata);
    //     $del_serial = unserialize($request->get('serialized'));
    //     foreach($del_serial as $d){
    //         File::delete($path.'/'.$d->name);
    //     }
    //     //var_dump($request->form_data);
    return response()->json($request->all()); //response()->json()
  }

  public function destroy($id){
    return Ads::destroy($id);
  }

}
