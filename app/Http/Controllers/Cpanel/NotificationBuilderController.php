<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Visa\Service;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationBuilderController extends Controller
{

	public function index() {
		return view('cpanel.visa.notifications.builder.index');
	}

	public function getServices() {
		return Service::where('cost', '>', 0)->orderBy('detail')->get();
	}

}