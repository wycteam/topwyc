<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ReportValidation;

use App\Models\User;
use App\Models\Visa\Report;
use App\Models\Visa\Remark;
use App\Models\Visa\ClientService;
use App\Models\Visa\Service;
use App\Models\Visa\ClientTransactions;
use App\Models\Visa\GroupMember;
use App\Models\Visa\ByBatch;


use App\Classes\Common;
use DB, Carbon\Carbon, Validator, Excel, PDF;
use DateTime;

class ReportsController extends Controller
{

    public function translation() {
        return json_encode([
            'translation' => trans('cpanel/write-report-page')
        ]);
    }

    public function componentTranslation() {
        return json_encode([
            'translation' => trans('cpanel/write-report-component')
        ]);
    }

	public function write() {
        $lang = trans('cpanel/write-report-page');
		$this->load_client_list();
        $data = array(
            'title' => $lang['write-report'],
        );
		return view('cpanel.visa.reports.index', $data);
	}

    public function selectServices($id){
        session()->flash('data', $id);
        return view('cpanel.visa.reports.services');
    }

    public function getClients($id){
        $id = explode(";",$id);
        $user = User::with('services.report')->whereIn('id', $id)->where(function ($query) {
                                                        $query->where('branch_id', Auth::user()->branch_id)
                                                              ->orwhere('branch_id', '3');
                                                    })->get();
        return $user;
    }

    public function saveReport(ReportValidation $request){
        $data = $request->all();
        $id = explode(";",$data['id']);
        //$user = Auth::user();
        foreach ($id as $service) {
            if($service!=''){
                $sv = ClientService::whereId($service)->first();
                $report = new Report();
                $report->service_id = $sv->id;
                $report->detail = $data['message'];
                $report->user_id = Auth::user()->id;
                $report->tracking = $sv->tracking;
                $report->log_date = Carbon::now();
                $report->save();

                $translated = Service::where('id',$sv->service_id)->first();
                $cnserv =$sv->detail;
                if($translated){
                    $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                }

                $title = "New service report for ".$sv->detail;
                $body = ''.$sv->detail.' with tracking #'.$sv->tracking."-> ".$data['message'];
                $main = $sv->detail." Package #".$sv->tracking;
                $title_cn = "新的服务报告 ".$cnserv;
                $body_cn = ''.$cnserv.' 服务包编号 #'.$sv->tracking."状态更新为 ".$data['message'];
                $main_cn = $cnserv." 服务包编号 #".$sv->tracking;
                $parent  = $sv->id;
                $parent_type  = 2;
                Common::savePushNotif($sv->client_id, $title, $body,'Report',$sv->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
            }

        }
    }

	public function read() {
        $trans = trans('cpanel/read-report-page');
        $title = $trans['list-of-reports'];
        $data = array(
            'title' => $title
        );
        return view('cpanel.visa.reports.read', $data);
	}

    public function getReports(){
        $data = Report::whereDate('log_date', '>=', Carbon::today())
                        ->whereDate('log_date', '<=', Carbon::today())
                        ->orderBy('detail','ASC')
                        ->orderBy('id','DESC')
                        ->get();
        $data->map(function ($d2) {
            $d2['type'] = 'report';
            return $d2;
        });

        $data2 = Remark::whereDate('log_date', '>=', Carbon::today())
                        ->whereDate('log_date', '<=', Carbon::today())
                        ->orderBy('id','DESC')
                        ->get();
        $data2->map(function ($d2) {
            $d2['type'] = 'remark';
            return $d2;
        });
        $all=$data->merge($data2);

        $data = array_reverse(array_sort($all, function ($value) {
                  return $value['log_date'];
                }));
        // \Log::info($data2);


        $ctr=-1;
        $detail = '';
        $serv = '';
        $report = [];
        $cltr = 0;
        foreach($data as $d){
            $client = User::findorFail($d->client_id)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
            $fullname = $client->first_name." ".$client->last_name;
            $client_id = $client->id;

            $service = ClientService::where('id',$d->service_id)->first();

            if($detail == $d->detail && $serv == $service->detail){
                $cltr++;
                $report[$ctr]['detail'] = $d->detail;
                $report[$ctr]['type'] = $d->type;
                $report[$ctr]['processor'] = User::findorFail($d->user_id)->first_name;
                $report[$ctr]['log_date'] = $d->log_date;
                $report[$ctr]['service'] = $service->detail;
                $report[$ctr]['remarks'] = $service->remarks;
                $report[$ctr]['client'][$cltr]['client_id'] = $client_id;
                $report[$ctr]['client'][$cltr]['fullname'] = $fullname;
                $detail = $d->detail;
                $serv = $service->detail;

            }
            else{
                $ctr++;
                $cltr = 0;
                $report[$ctr]['detail'] = $d->detail;
                $report[$ctr]['type'] = $d->type;
                $report[$ctr]['processor'] = User::findorFail($d->user_id)->first_name;
                $report[$ctr]['log_date'] = $d->log_date;
                $report[$ctr]['service'] = $service->detail;
                $report[$ctr]['remarks'] = $service->remarks;
                $report[$ctr]['client'][$cltr]['client_id'] = $client_id;
                $report[$ctr]['client'][$cltr]['fullname'] = $fullname;
                $detail = $d->detail;
                $serv = $service->detail;

            }

        }
        //\Log::info($report);
        return $report;
    }

    public function getReportsYesterday(){

        $lastReportedDate = Report::selectRaw('date(log_date) day')->groupBy('day')->orderBy('day','DESC')->get();

        $r='';
        foreach($lastReportedDate as $d){
            if($d->day!=Carbon::today()->toDateString()){
                if($r=='')
                $r = $d->day;
            }
        }

        $data = Report::whereDate('log_date', '>=', $r)
                        ->whereDate('log_date', '<=', $r)
                        ->orderBy('detail','ASC')
                        ->orderBy('id','DESC')
                        ->get();

        $data->map(function ($d2) {
            $d2['type'] = 'report';
            return $d2;
        });

        $data2 = Remark::whereDate('log_date', '>=', $r)
                        ->whereDate('log_date', '<=', $r)
                        ->orderBy('id','DESC')
                        ->get();

        $data2->map(function ($d2) {
            $d2['type'] = 'remark';
            return $d2;
        });
        $all=$data->merge($data2);

        $data = array_reverse(array_sort($all, function ($value) {
                  return $value['log_date'];
                }));
        // \Log::info($data2);


        $ctr=-1;
        $detail = '';
        $serv = '';
        $report = [];
        $cltr = 0;
        foreach($data as $d){
            $client = User::findorFail($d->client_id)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
            $fullname = $client->first_name." ".$client->last_name;
            $client_id = $client->id;

            $service = ClientService::where('id',$d->service_id)->first();

            if($detail == $d->detail && $serv == $service->detail){
                $cltr++;
                $report[$ctr]['detail'] = $d->detail;
                $report[$ctr]['type'] = $d->type;
                $report[$ctr]['processor'] = User::findorFail($d->user_id)->first_name;
                $report[$ctr]['log_date'] = $d->log_date;
                $report[$ctr]['service'] = $service->detail;
                $report[$ctr]['remarks'] = $service->remarks;
                $report[$ctr]['client'][$cltr]['client_id'] = $client_id;
                $report[$ctr]['client'][$cltr]['fullname'] = $fullname;
                $detail = $d->detail;
                $serv = $service->detail;

            }
            else{
                $ctr++;
                $cltr = 0;
                $report[$ctr]['detail'] = $d->detail;
                $report[$ctr]['type'] = $d->type;
                $report[$ctr]['processor'] = User::findorFail($d->user_id)->first_name;
                $report[$ctr]['log_date'] = $d->log_date;
                $report[$ctr]['service'] = $service->detail;
                $report[$ctr]['remarks'] = $service->remarks;
                $report[$ctr]['client'][$cltr]['client_id'] = $client_id;
                $report[$ctr]['client'][$cltr]['fullname'] = $fullname;
                $detail = $d->detail;
                $serv = $service->detail;

            }

        }
        //\Log::info($report);
        return $report;
    }

     public function getClientReports($group_id = 0, $client_id = 0, $service_id = 0){
        $report = [];

        if($group_id > 0) {
            $data = Report::where('group_id', $group_id)->orderBy('id', 'desc')->get();

            $ctr=0;
            foreach($data as $d){
                $service = ClientService::where('id',$d->service_id)->first();

                $report[$ctr]['rid'] = $d->id;
                $report[$ctr]['detail'] = '<b>Report : </b> : '.$d->detail;
                $report[$ctr]['type'] = $d->type;
                $report[$ctr]['processor'] = User::findorFail($d->user_id)->first_name;
                $report[$ctr]['log_date'] = $d->log_date;
                $report[$ctr]['service'] = $service->detail;
                $report[$ctr]['service_date'] = $service->service_date;
                $report[$ctr]['remarks'] = $service->remarks;
                $ctr++;
            }
        } elseif($client_id > 0){
            $data = Report::where('client_id',$client_id)
                            ->orderBy('id','DESC')
                            ->get();
            $ctr=0;
            $cltr = 0;
            foreach($data as $d){
                $client = User::findorFail($d->client_id)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);

                $service = ClientService::where('id',$d->service_id)->first();
                    $cltr++;
                    $report[$ctr]['rid'] = $d->id;
                    $report[$ctr]['detail'] = '<b>Report : </b> : '.$d->detail;
                    $report[$ctr]['type'] = $d->type;
                    $report[$ctr]['processor'] = User::findorFail($d->user_id)->first_name;
                    $report[$ctr]['log_date'] = $d->log_date;
                    $report[$ctr]['service'] = $service->detail;
                    $report[$ctr]['service_date'] = $service->service_date;
                    $report[$ctr]['remarks'] = $service->remarks;
                    $ctr++;
            }
        } elseif($service_id > 0) {
            $data = Report::where('service_id',$service_id)
                            ->orderBy('id','DESC')
                            ->get();
            $ctr=0;
            $cltr = 0;
            $service = ClientService::where('id',$service_id)->first();
            $reportDetail = '';
            foreach($data as $d){
                    $cltr++;
                    $report[$ctr]['rid'] = $d->id;
                    $reportDetail .= '<b>Report : </b> : '.$d->detail.'<br>';
                    $reportDetail .= '<small><b>'.$d->log_date.' -- '.User::findorFail($d->user_id)->first_name.'</b></small><br><br>';
                    $report[$ctr]['type'] = $d->type;
                    $report[$ctr]['processor'] = '';
                    $report[$ctr]['log_date'] = '';
                    $report[$ctr]['service'] = $service->detail;
                    $report[$ctr]['service_date'] = $service->service_date;
                    $report[$ctr]['remarks'] = $service->remarks;
                    //$ctr++;
            }

            if($cltr != 0) {
                $report[$ctr]['detail'] = $reportDetail;
            }
        }
        return $report;
    }

    public function reportFilter(Request $request) {
        $validator = Validator::make($request->all(), [
            'start' => 'required|date',
            'end' => 'required|date'
        ]);

        if($validator->fails()) {
            return json_encode([
                'errors' => $validator->errors()->getMessages(),
                'code' => 422
            ]);
        }


        $data = Report::whereDate('log_date', '>=', $request->start)
                        ->whereDate('log_date', '<=', $request->end)
                        ->orderBy('detail','ASC')
                        ->orderBy('id','DESC')
                        ->get();
        if($request->client_id!=''){
            $csid = ClientService::where('client_id',$request->client_id)->pluck('id');
            $data = Report::whereIn('service_id', $csid)
                        ->orderBy('id','DESC')
                        ->get();
        }

        foreach($data as $d){
            $rep = Report::where('id',$d->id)->first();
            if($rep){
                if($d->client_id == 0){
                    $getCS = ClientService::where('id',$d->service_id)->first();
                    if($getCS){
                        $d->client_id = $getCS->client_id;
                        $d->group_id = $getCS->group_id;
                        $d->save();
                    }
                }
            }

        }
        $data->map(function ($d2) {
            $d2['type'] = 'report';
            return $d2;
        });


        $data2 = Remark::whereDate('log_date', '>=', $request->start)
                        ->whereDate('log_date', '<=', $request->end)
                        ->orderBy('id','DESC')
                        ->get();
        if($request->client_id!=''){
            $csid = ClientService::where('client_id',$request->client_id)->pluck('id');
            $data2 = Remark::whereIn('service_id', $csid)
                        ->orderBy('id','DESC')
                        ->get();
        }
        $data2->map(function ($d2) {
            $d2['type'] = 'remark';
            return $d2;
        });
        $all=$data->merge($data2);

        $data = array_reverse(array_sort($all, function ($value) {
                  return $value['log_date'];
                }));
        // \Log::info($data2);


        $ctr=-1;
        $detail = '';
        $serv = '';
        $report = [];
        $cltr = 0;
        foreach($data as $d){
            $client = User::findorFail($d->client_id)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
            $fullname = $client->first_name." ".$client->last_name;
            $client_id = $client->id;

            $service = ClientService::where('id',$d->service_id)->first();

            if($detail == $d->detail && $serv == $service->detail){
                $cltr++;
                $report[$ctr]['detail'] = $d->detail;
                $report[$ctr]['type'] = $d->type;
                $report[$ctr]['processor'] = User::findorFail($d->user_id)->first_name;
                $report[$ctr]['log_date'] = $d->log_date;
                $report[$ctr]['service'] = $service->detail;
                $report[$ctr]['remarks'] = $service->remarks;
                $report[$ctr]['client'][$cltr]['client_id'] = $client_id;
                $report[$ctr]['client'][$cltr]['fullname'] = $fullname;
                $detail = $d->detail;
                $serv = $service->detail;

            }
            else{
                $ctr++;
                $cltr = 0;
                $report[$ctr]['detail'] = $d->detail;
                $report[$ctr]['type'] = $d->type;
                $report[$ctr]['processor'] = User::findorFail($d->user_id)->first_name;
                $report[$ctr]['log_date'] = $d->log_date;
                $report[$ctr]['service'] = $service->detail;
                $report[$ctr]['remarks'] = $service->remarks;
                $report[$ctr]['client'][$cltr]['client_id'] = $client_id;
                $report[$ctr]['client'][$cltr]['fullname'] = $fullname;
                $detail = $d->detail;
                $serv = $service->detail;

            }

        }
        //\Log::info($report);
        return $report;
    }

    public function getServices($id){
        $data = ClientService::where('tracking',$id)->get();
        return $data;
    }

	public function load_client_list(){
        $myfile = fopen(storage_path('/app/public/data/ClientsReport.txt'), 'w') or die('Unable to open file!');
        $txt = "{\n";
        fwrite($myfile, $txt);
        $txt = '"data":[';
        fwrite($myfile, $txt);
        $txt = "\n";
        fwrite($myfile, $txt);
        $count = 0;

        $records = DB::connection('mysql')
            ->table('users as a')
            ->select(DB::raw('
                a.*,
                total.total_amount as total_costs,
                total.service_date,
                total2.total_deposit,
                total4.total_refund,
                total5.total_discount,
                total6.dates, role.role_id,
                (
                    (case when total.total_amount > 0 then total.total_amount else 0 end)
                    - (
                        (
                            case when total2.total_deposit > 0 then total2.total_deposit else 0 end
                            + case when total3.total_payment > 0 then total3.total_payment else 0 end
                            + case when total5.total_discount > 0 then total5.total_discount else 0 end
                        )
                        - case when total4.total_refund > 0 then total4.total_refund else 0 end
                    )
                ) as total_balance,
                total3.total_payment'))
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(round(sum(b.cost))+round(sum(b.charge))+round(sum(b.tip))+ round(sum(b.com_client)) + round(sum(b.com_agent)),0) as total_amount,
                        COALESCE(sum(b.tip),0) as total_charge,
                        b.client_id, b.service_date
                        from visa.client_services as b
                        where b.active = 1
                        and b.group_id = 0
                        group by b.client_id
                        order by b.service_date desc
                    ) as total'),
                    'total.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(round(sum(c.amount)),0) as total_deposit,
                        c.client_id
                        from visa.client_transactions as c
                        where c.group_id = 0
                        and c.type = "Deposit"
                        group by c.client_id
                    ) as total2'),
                    'total2.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(round(sum(d.amount)),0) as total_payment,
                        d.client_id
                        from  visa.client_transactions as d
                        where d.group_id = 0
                        and d.type = "Payment"
                        group by d.client_id
                    ) as total3'),
                    'total3.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(round(sum(e.amount)),0) as total_refund,
                        e.client_id
                        from visa.client_transactions as e
                        where e.group_id = 0
                        and e.type = "Refund"
                        group by e.client_id
                    ) as total4'),
                    'total4.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(round(sum(f.amount)),0) as total_discount,
                        f.client_id
                        from visa.client_transactions as f
                        where f.group_id = 0
                        and f.type = "Discount"
                        group by f.client_id
                    ) as total5'),
                    'total5.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select *
                        from role_user as r
                        where r.role_id = 9
                    ) as role'),
                    'role.user_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as dates,
                        x.client_id
                        from( SELECT STR_TO_DATE(log_date, "%M %e, %Y, %l:%i %p") as dates,
                            client_id, status
                            FROM visa.packages
                            ORDER BY dates desc
                        ) as x
                        group by x.client_id) as total6'),
                    'total6.client_id', '=', 'a.id')
                ->orderBy(DB::raw('
                    (
                        (case when total.total_amount > 0 then total.total_amount else 0 end)
                        - (
                            (
                                case when total2.total_deposit > 0 then total2.total_deposit else 0 end
                                + case when total3.total_payment > 0 then total3.total_payment else 0 end
                                + case when total5.total_discount > 0 then total5.total_discount else 0 end
                            )
                            - case when total4.total_refund > 0 then total4.total_refund else 0 end
                        )
                    )'), 'desc')
                ->where("role.role_id","9") ->where(function ($query) {
                                                        $query->where('a.branch_id', Auth::user()->branch_id)
                                                              ->orwhere('a.branch_id', '3');
                                                    })
                ->get();
        //\Log::info($records);

        foreach ($records as $value) {
	            $count++;
	            $txt = "[";
	            fwrite($myfile, $txt);
	            $txt = '"'.$value->id.'",';
	            fwrite($myfile, $txt);
	            $txt = '"'.ucwords($value->first_name).' '.ucwords($value->last_name).'",';
	            fwrite($myfile, $txt);

	            $txt = '"'.Common::formatMoney($value->total_costs*1).'",';
	            fwrite($myfile, $txt);

	            $txt = '"'.$value->birth_date.'",';
	            fwrite($myfile, $txt);

	            $fullname = ucwords($value->first_name)." ".ucwords($value->last_name);

	            $txt = '"<center><a href=\"javascript:void(0)\" id=\"add'.$value->id.'\" onclick=\"addClient('."'".$value->id."',"."'".$fullname."'".');\"><i class=\"fa fa-plus fa-1x\"></i></a></center>"';
	            fwrite($myfile, $txt);

	            if(count($records)==$count){
	                $txt = "]\n";
	            }else{
	                $txt = "],\n";
	            }
	            fwrite($myfile, $txt);
	        }
	        $txt = "\n]\n}";
	        fwrite($myfile, $txt);
	        fclose($myfile);
	        return true;
	    }

    public function getGroupSummary2($id,$type, $lang = 'EN', $services = null, $month = 0, $year = 0) {
        header('Set-Cookie: fileDownload=true; path=/');
        header('Cache-Control: max-age=60, must-revalidate');
        header("Content-type: pplication/vnd.ms-excel");
        $getGroupMembers = DB::connection('visa')
            ->table('group_members as a')
            ->select('a.*')
            ->where('a.group_id', $id)
            ->get();

        $getGroupName = DB::connection('visa')
            ->table('groups as a')
            ->select('a.*')
            ->where('a.id', $id)
            ->first();

        $filename = $lang.$type.'-'.$getGroupName->name .'-'. Carbon::today()->format('n/j/Y');
        Excel::create($filename, function($excel) use ($filename, $getGroupMembers, $id, $type, $lang) {

            // Set the title
            $excel->setTitle($filename);

            // Chain the setters
            $excel->setCreator('WYC')
                  ->setCompany('WYC');

            // Call them separately
            $excel->setDescription('Group Summary');

            //First Sheet -> Group Member's Cash Flow
            if($type=='bymembers'){
                $group_summary = ($lang == 'EN' ? 'Group Summary' : '总结报告');
                $excel->sheet($group_summary, function($sheet1) use ($getGroupMembers, $id, $lang) {
                    $row = 1;
                    $tempTotal = 0;
                    if($lang == 'EN'){
                         $sheet1->row($row, array(
                            'Date', 
                            'Service', 
                            'Status',
                            'Charge',
                            'Group Total Balance' 
                        ));
                    }
                    else{                    
                        $sheet1->row($row, array(
                            '建立日期', 
                            '服务', 
                            '状态',
                            '收费',
                            '总余额' 
                        ));
                    }
                    $cellNum = 'A'.$row.':E'.$row;
                    $sheet1->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                        $cells->setBackground('#6fc4dc');
                    });
                    $sheet1->setWidth(array(
                        'A'     =>  20,
                        'B'     =>  40,
                        'C'     =>  15,
                        'D'     =>  15,
                        'E'     =>  25,
                    ));
                    $row++;

                    foreach ($getGroupMembers as $value) {
                        $getName = User::where('id',$value->client_id)->select('first_name','last_name')->first()->full_name;

                        $row++;
                        $sheet1->row($row, array(
                            ucfirst($getName)
                        ));
                        $cellNum = 'A'.$row.':E'.$row;
                        $sheet1->mergeCells($cellNum);
                        $sheet1->cells($cellNum, function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setBackground('#a6ced9');
                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                            $cells->setFontWeight('bold');
                        });

                        $getServices = DB::connection('visa')
                        ->table('client_services as a')
                        ->select('a.*')
                        ->where('a.client_id', $value->client_id)
                        ->where('a.group_id', $id)
                        ->get();
                        $bal = 0;
                        foreach ($getServices as $s) {
                            $datetime = new DateTime($s->service_date);
                            $getdate = $datetime->format('M d,Y');
                            $gettime = $datetime->format('h:i A');

                            //note
                            // $the_string = $s->remarks;
                            // $the_word  = "<small>";
                            // $the_string = str_replace('</small>', '', $the_string);
                            // $count_occur = substr_count($the_string,$the_word);
                            // for($i = 0; $i<$count_occur; $i++){
                            //     if (strpos($the_string, $the_word) !== false) {
                            //         $the_string = str_replace('</br>', ' --> ', $the_string);
                            //         $postr = strpos($the_string, $the_word);
                            //         $str1 = substr($the_string,0,$postr-8);
                            //         $str2 = substr($the_string,$postr+7);
                            //         $the_string = $str1.$str2;
                            //     }
                            // }

                            //Cost
                            // $amt = $s->cost + $s->tip;
                            // if($s->active == 0){
                            //     $amt = 0;
                            // }

                            //Service Charge
                            $chrg = $s->charge + $s->cost + $s->tip;
                            if($s->active == 0 || $s->status != 'complete'){
                                $chrg = 0;
                            }

                            //Subtotal
                            $sub = $chrg;

                            //Per Person Balance
                            if($s->active == 0){
                                $sub = 0;
                            }
                            $bal += $sub;

                            //I
                            $tempTotal +=$sub;

                            //Cancelled
                            $sp = $s->status;
                            $sp_cn = $this->statusChinese($sp);
                            if($s->active==0){
                                $sp = "CANCELLED";
                                $sp_cn = "取消";
                            }

                            // if($sp != 'complete'){
                            //     $chrg = 0;
                            // }

                            $row++;

                            // $disc = 0;
                            // $getdisc = ClientTransactions::where('group_id', $id)->where('service_id',$s->id)->first();
                            // if($getdisc){
                            //     $disc = $getdisc->amount;
                            // }
                            if($lang == 'EN'){
                                $sheet1->row($row, array(
                                     $getdate, 
                                     $s->detail, 
                                     //ucwords(strtolower($the_string)),
                                     ucfirst($sp), 
                                     $chrg, 
                                     //$disc,
                                     // $sub, 
                                     // $bal, 
                                     $tempTotal
                                 ));
                            }
                            else{
                                $translated = Service::where('id',$s->service_id)->first();
                                $cnserv =$s->detail;
                                if($translated){
                                    $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                                }
                                 $sheet1->row($row, array(
                                     $this->DateChinese($getdate), 
                                     $cnserv, 
                                     $sp_cn, 
                                     $chrg, 
                                     $tempTotal
                                 ));
                            }

                        }

                        $row++;
                        $row++;
                    }

                    $row++;
                    $summaryrow = $row;
                    // $cost = DB::connection('visa')
                    //     ->table('client_services')
                    //     ->select(DB::raw('sum(cost+charge+tip) as total_cost'))
                    //     ->where('group_id', '=', $id)
                    //     ->where('active', '=', 1)
                    //     ->first();

                    $total_deposit = (
                        (
                            $this->GetGroupDeposit($id)
                            + $this->GetGroupPayment($id)
                        )
                    ) + 0;

                    $total_discount = $this->GetGroupDiscount($id);
                    $total_refund = $this->GetGroupRefund($id);
                    $total_group_cost = $this->GetGroupCostComplete($id);

                    $total_balance = (
                        (
                            $total_deposit
                            +$total_discount
                        )
                        - (
                            $total_refund
                            +$total_group_cost
                        )
                    );

                    $textdeposit = ($lang == 'EN' ? 'Total Deposit : ' : '总已付款 : ');
                    $sheet1->cell('D'.$row, function($cell) use($total_deposit, $textdeposit) {
                        $cell->setValue($textdeposit.number_format($total_deposit, 2, '.', ','));
                    });

                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet1->mergeCells($cellNum);
                    $sheet1->cells($cellNum, function($cells)  {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textcost = ($lang == 'EN' ? 'Total Cost : ' : '总花费 : ');
                    $sheet1->cell('D'.$row, function($cell) use($total_group_cost, $textcost) {
                        $cell->setValue($textcost.number_format($total_group_cost, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet1->mergeCells($cellNum);
                    $sheet1->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textdiscount = ($lang == 'EN' ? 'Total Discount : ' : '总折扣 : ');
                    $sheet1->cell('D'.$row, function($cell) use($total_discount, $textdiscount) {
                        $cell->setValue($textdiscount.number_format($total_discount, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet1->mergeCells($cellNum);
                    $sheet1->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textrefund = ($lang == 'EN' ? 'Total Refund : ' : '总退款 : ');
                    $sheet1->cell('D'.$row, function($cell) use($total_refund, $textrefund) {
                        $cell->setValue($textrefund.number_format($total_refund, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet1->mergeCells($cellNum);
                    $sheet1->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textbalance = ($lang == 'EN' ? 'Total Balance : ' : '总余额 : ');
                    $sheet1->cell('D'.$row, function($cell) use($total_balance, $textbalance) {
                        $cell->setValue($textbalance.number_format($total_balance, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet1->mergeCells($cellNum);
                    $sheet1->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setBackground('#a6ced9');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $row++;
                    $row++;

                    //$transactions = ClientTransactions::where('group_id', $id)->get();
                    $transactions = DB::connection('visa')
                    ->table('client_transactions as a')
                    ->select(DB::raw('
                        a.amount,a.type, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%M %d,%Y") as log_date'))
                        ->where('group_id', $id)
                        ->orderBy('tdate','DESC')    
                        ->get();

                    $transhistory = ($lang == 'EN' ? 'Transactions History' : '交易记录');
                    $sheet1->cell('A'.$row, function($cell) use($transhistory) {
                        $cell->setValue($transhistory);
                        $cell->setValignment('center');
                        //$cell->setBackground('#a6ced9');
                        $cell->setFontWeight('bold');
                    });

                    $row++;

                    if($lang == 'EN'){
                        $sheet1->row($row, array(
                            'Amount', 'Date' , 'Type'
                        ));
                    }
                    else{
                        $sheet1->row($row, array(
                            '共计', '日期' , '类型'
                        ));
                    }

                    $cellNum = 'A'.$row.':C'.$row;
                    $sheet1->cells($cellNum, function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setFontWeight('bold');
                        $cell->setBackground('#a6ced9');
                    });
                    $row++;
                    //$ttd = 0;
                    foreach($transactions as $d){
                        $datetime = new DateTime($d->log_date);
                        $getdate = $datetime->format('M d,Y');
                        //$gettime = $datetime->format('h:i A');
                        //预存款 // Deposit
                        //折扣 // Discount
                        //已付款 // Payment
                        //退款 // Refund
                        if($lang == 'EN'){
                            $sheet1->row($row, array(
                              number_format($d->amount, 2, '.', ','), $d->log_date, $d->type
                            ));
                        }
                        else{
                            $dtype = '';
                            if($d->type == 'Deposit'){
                                $dtype = '预存款';
                            }
                            if($d->type == 'Discount'){
                                $dtype = '折扣';
                            }                            
                            if($d->type == 'Payment'){
                                $dtype = '已付款';
                            }                            
                            if($d->type == 'Refund'){
                                $dtype = '退款';
                            }

                            $sheet1->row($row, array(
                              number_format($d->amount, 2, '.', ','), $this->DateChinese($getdate), $dtype
                            ));
                        }
                        
                        $cellNum = 'A'.$row.':C'.$row;
                        $sheet1->cells($cellNum, function($cell) {
                            $cell->setAlignment('center');
                            $cell->setValignment('center');
                            //$cell->setFontWeight('bold');
                        });
                        //$ttd+=$d->amount;
                        $row++;
                    }
                    $row++;
                    // $sheet1->row($row, array(
                    //     'Total Deposit : '.number_format($ttd, 2, '.', ',')
                    // ));
                    // $cellNum = 'A'.$row.':B'.$row;
                    // $sheet2->cells($cellNum, function($cells) {
                    //     $cells->setAlignment('center');
                    //     $cells->setValignment('center');
                    //     $cells->setFontWeight('bold');
                    // });

                }); // First Sheet

            // 2nd sheet
            // $excel->sheet('Deposit History', function($sheet2) use($id) {                
            //     $gdepo = ClientTransactions::where('group_id', $id)->where('type', 'Deposit')->get();

            //     $row = 1;
            //     $sheet2->row($row, array(
            //         'Amount', 'Date'
            //     ));
            //     $cellNum = 'A'.$row.':B'.$row;
            //     $sheet2->cells($cellNum, function($cells) {
            //         $cells->setAlignment('center');
            //         $cells->setValignment('center');
            //         $cells->setFontWeight('bold');
            //         $cells->setBackground('#6fc4dc');
            //     });
            //     $row++;
            //     $ttd = 0;
            //     foreach($gdepo as $d){
            //         $datetime = new DateTime($d->log_date);
            //         $getdate = $datetime->format('M d,Y');
            //         $gettime = $datetime->format('h:i A');
            //         $sheet2->row($row, array(
            //             number_format($d->amount, 2, '.', ','), $getdate.' '.$gettime
            //         ));
            //         $cellNum = 'A'.$row.':B'.$row;
            //         $sheet2->cells($cellNum, function($cells) {
            //             $cells->setAlignment('center');
            //             $cells->setValignment('center');
            //             $cells->setFontWeight('bold');
            //         });
            //         $ttd+=$d->amount;
            //         $row++;
            //     }
            //     $row++;
            //     $sheet2->row($row, array(
            //         'Total Deposit : '.number_format($ttd, 2, '.', ',')
            //     ));
            //     $cellNum = 'A'.$row.':B'.$row;
            //     $sheet2->cells($cellNum, function($cells) {
            //         $cells->setAlignment('center');
            //         $cells->setValignment('center');
            //         $cells->setFontWeight('bold');
            //     });
            // }); // 2nd sheet

            // // 3rd Sheet
            // $excel->sheet('Payments History', function($sheet2) use($id) {
            //         $gdepo = ClientTransactions::where('group_id', $id)->where('type', 'Payment')->get();

            //         $row = 1;
            //         $sheet2->row($row, array(
            //             'Amount', 'Date'
            //         ));
            //         $cellNum = 'A'.$row.':B'.$row;
            //         $sheet2->cells($cellNum, function($cells) {
            //             $cells->setAlignment('center');
            //             $cells->setValignment('center');
            //             $cells->setFontWeight('bold');
            //             $cells->setBackground('#6fc4dc');
            //         });
            //         $row++;
            //         $ttd = 0;
            //         foreach($gdepo as $d){
            //             $datetime = new DateTime($d->log_date);
            //             $getdate = $datetime->format('M d,Y');
            //             $gettime = $datetime->format('h:i A');
            //             $sheet2->row($row, array(
            //                 number_format($d->amount, 2, '.', ','), $getdate.' '.$gettime
            //             ));
            //             $cellNum = 'A'.$row.':B'.$row;
            //             $sheet2->cells($cellNum, function($cells) {
            //                 $cells->setAlignment('center');
            //                 $cells->setValignment('center');
            //                 $cells->setFontWeight('bold');
            //             });
            //             $ttd+=$d->amount;
            //             $row++;
            //         }
            //         $row++;
            //         $sheet2->row($row, array(
            //             'Total Payment : '.number_format($ttd, 2, '.', ',')
            //         ));
            //         $cellNum = 'A'.$row.':B'.$row;
            //         $sheet2->cells($cellNum, function($cells) {
            //             $cells->setAlignment('center');
            //             $cells->setValignment('center');
            //             $cells->setFontWeight('bold');
            //         });
            //     }); // 3rd Sheet

            // // 4th Sheet
            // $excel->sheet('Refunds History', function($sheet2) use($id) {
            //         $gdepo = ClientTransactions::where('group_id', $id)->where('type', 'Refund')->get();

            //         $row = 1;
            //         $sheet2->row($row, array(
            //             'Amount', 'Date'
            //         ));
            //         $cellNum = 'A'.$row.':B'.$row;
            //         $sheet2->cells($cellNum, function($cells) {
            //             $cells->setAlignment('center');
            //             $cells->setValignment('center');
            //             $cells->setFontWeight('bold');
            //             $cells->setBackground('#6fc4dc');
            //         });
            //         $row++;
            //         $ttd = 0;
            //         foreach($gdepo as $d){
            //             $datetime = new DateTime($d->log_date);
            //             $getdate = $datetime->format('M d,Y');
            //             $gettime = $datetime->format('h:i A');
            //             $sheet2->row($row, array(
            //                 number_format($d->amount, 2, '.', ','), $getdate.' '.$gettime
            //             ));
            //             $cellNum = 'A'.$row.':B'.$row;
            //             $sheet2->cells($cellNum, function($cells) {
            //                 $cells->setAlignment('center');
            //                 $cells->setValignment('center');
            //                 $cells->setFontWeight('bold');
            //             });
            //             $ttd+=$d->amount;
            //             $row++;
            //         }
            //         $row++;
            //         $sheet2->row($row, array(
            //             'Total Refunds : '.number_format($ttd, 2, '.', ',')
            //         ));
            //         $cellNum = 'A'.$row.':B'.$row;
            //         $sheet2->cells($cellNum, function($cells) {
            //             $cells->setAlignment('center');
            //             $cells->setValignment('center');
            //             $cells->setFontWeight('bold');
            //         });
            //     }); // 4th Sheet
            }
            // end bymembers type

            // By Service
            if($type=='byservices'){
            $byservices = ($lang == 'EN' ? 'By Service' : '按服务查看');
            $excel->sheet($byservices, function($sheet2) use($id, $lang) {
                $byService = ClientService::where('group_id',$id)->where('active',1)
                        ->with('discount')->groupBy('service_id')->orderBy('id',"DESC")
                        ->get();
                $row = 1;
                if($lang == 'EN'){
                    $sheet2->row($row, array(
                        'Service Name','', 'Latest Date','Total Service Cost',''
                    ));
                }
                else{                
                    $sheet2->row($row, array(
                        '服务明细','', '最近的服务日期','总服务费',''
                    ));
                }
                $cellNum = 'A'.$row.':E'.$row;
                $m1 = 'A'.$row.':B'.$row;
                $sheet2->mergeCells($m1);
                $m2 = 'C'.$row;
               // $sheet2->mergeCells($m2);
                $m3 = 'D'.$row.':E'.$row;
                $sheet2->mergeCells($m3);
                $sheet2->setBorder($m1, 'thin');
                $sheet2->setBorder($m2, 'thin');
                $sheet2->setBorder($m3, 'thin');
                $sheet2->cells($cellNum, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#9ad1e0');
                });
                $sheet2->setWidth(array(
                    'A'     =>  30,
                    'B'     =>  20,
                    'C'     =>  15,
                    'D'     =>  15,
                    'E'     =>  15,
                ));
                $row+=2; //add space after header
                $ttd = 0;
                $tempTotal = 0;
                foreach($byService as $d){
                    $service = ClientService::where('id',$d->id)->select('detail','service_date','service_id')->orderBy('id',"DESC")->first();
                    $totalcost = ClientService::where('service_id',$d->service_id)->where('status','complete')->where('group_id', $id)->where('active', 1)->value(DB::raw("SUM(cost + charge + tip)"));
                    if($totalcost == ''){
                        $totalcost =0;
                    }
                    $datetime = new DateTime($service->service_date);
                    $getdate = $datetime->format('M d,Y');


                    if($lang == 'EN'){
                        $sheet2->row($row, array(
                            $service->detail,'', $getdate , $totalcost,''
                        ));
                    }
                    else{
                        $translated = Service::where('id',$service->service_id)->first();
                        $cnserv =$service->detail;
                        if($translated){
                            $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                        }
                        $sheet2->row($row, array(
                            $cnserv,'', $this->DateChinese($getdate) , $totalcost,''
                        ));
                    }


                    $m1 = 'A'.$row.':B'.$row;
                    $sheet2->mergeCells($m1);
                    $m2 = 'C'.$row;
                    //$sheet2->mergeCells($m2);
                    $m3 = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($m3);
                    $sheet2->setBorder($m1, 'thin');
                    $sheet2->setBorder($m2, 'thin');
                    $sheet2->setBorder($m3, 'thin');
                    $cellNum = 'A'.$row.':E'.$row;
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                        $cells->setBackground('#b3bbbd');
                    });

                    $row++;
                    if($lang == 'EN'){
                        $sheet2->row($row, array(
                            'Package', 
                            'Status', 
                            'Charge', 
                            'Group Total Balance',
                            ''
                        ));
                    }
                    else{
                        $sheet2->row($row, array(
                            '查询编号', 
                            '状态', 
                            '收费', 
                            '总余额',
                            ''
                        ));
                    }

                    $cellNum = 'A'.$row.':E'.$row;
                    $m3 = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($m3);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                        $cells->setBackground('#d8e0e2');
                    });
                    $row++;
                    $query = ClientService::where('service_id',$service->service_id)->where('group_id',$id)->where('active',1)->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('client_id')->get();
                        $ctr2 = 0;
                        $servTotal = 0;
                        $client = [];
                        foreach($query as $m){
                            $ss =  ClientService::where('service_id',$service->service_id)->where('group_id',$id)->where('active',1)->with('discount2')->orderBy('service_date','DESC')->where('client_id',$m->client_id)->get();
                            // return $ss;
                            if(count($ss)){
                                $client = User::where('id',$m->client_id)->select('first_name','last_name')->first()->full_name;
                                $totalcost = ClientService::where('service_id',$id)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
                                $services = $ss;

                                $sheet2->row($row, array(
                                    $client
                                ));
                                $cellNum = 'A'.$row;
                                $sheet2->cells($cellNum, function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                    $cells->setBackground('#9ad1e0');
                                });
                                $row++;

                                foreach($services as $serv){
                                    $tcost = $serv->cost + $serv->tip + $serv->charge + $serv->com_client + $serv->com_agent;
                                    if($serv->status != 'complete'){
                                        $tcost = 0;
                                    }

                                    $sp = $serv->status;
                                    $sp_cn = $this->statusChinese($serv->status);
                                    if($serv->active==0){
                                        $sp = "CANCELLED";
                                        $sp_cn = "取消";
                                    }

                                    $servTotal += $tcost;
                                    $tempTotal += $tcost;

                                    if($lang == 'EN'){
                                        $sheet2->row($row, array(
                                            $serv->tracking,
                                            $sp,
                                            $tcost,
                                            $tempTotal,
                                            ''
                                        ));
                                    }
                                    else{
                                        $sheet2->row($row, array(
                                            $serv->tracking,
                                            $sp_cn,
                                            $tcost,
                                            $tempTotal,
                                            ''
                                        ));
                                    }


                                    $cellNum = 'A'.$row.':E'.$row;
                                    $m2 = 'C'.$row;
                                    //$sheet2->mergeCells($m2);
                                    $m3 = 'D'.$row.':E'.$row;
                                    $sheet2->mergeCells($m3);
                                    $sheet2->cells($cellNum, function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                        // $cells->setBackground('#d8e0e2');
                                    });
                                    $row++;
                                }
                            }
                            $row++;
                            $ctr2++;
                        }

                    $row++;
                }
                $row++;

                    $total_deposit = (
                        (
                            $this->GetGroupDeposit($id)
                            + $this->GetGroupPayment($id)
                        )
                    ) + 0;

                    $total_discount = $this->GetGroupDiscount($id);
                    $total_refund = $this->GetGroupRefund($id);
                    $total_group_cost = $this->GetGroupCostComplete($id);

                    $total_balance = (
                        (
                            $total_deposit
                            +$total_discount
                        )
                        - (
                            $total_refund
                            +$total_group_cost
                        )
                    );

                    $textdeposit = ($lang == 'EN' ? 'Total Deposit : ' : '总已付款 : ');
                    $sheet2->cell('D'.$row, function($cells) use($total_deposit, $textdeposit) {
                        $cells->setValue($textdeposit.number_format($total_deposit, 2, '.', ','));
                    });

                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells)  {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textcost = ($lang == 'EN' ? 'Total Cost : ' : '总花费 : ');
                    $sheet2->cell('D'.$row, function($cells) use($total_group_cost, $textcost) {
                        $cells->setValue($textcost.number_format($total_group_cost, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textdiscount = ($lang == 'EN' ? 'Total Discount : ' : '总折扣 : ');
                    $sheet2->cell('D'.$row, function($cells) use($total_discount, $textdiscount) {
                        $cells->setValue($textdiscount.number_format($total_discount, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textrefund = ($lang == 'EN' ? 'Total Refund : ' : '总退款 : ');
                    $sheet2->cell('D'.$row, function($cells) use($total_refund, $textrefund) {
                        $cells->setValue($textrefund.number_format($total_refund, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textbalance = ($lang == 'EN' ? 'Total Balance : ' : '总余额 : ');
                    $sheet2->cell('D'.$row, function($cells) use($total_balance, $textbalance) {
                        $cells->setValue($textbalance.number_format($total_balance, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setBackground('#a6ced9');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $row++;

                    $row++;

                    //$transactions = ClientTransactions::where('group_id', $id)->get();
                    $transactions = DB::connection('visa')
                    ->table('client_transactions as a')
                    ->select(DB::raw('
                        a.amount,a.type, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%M %d,%Y") as log_date'))
                        ->where('group_id', $id)
                        ->orderBy('tdate','DESC')    
                        ->get();
                        
                    $transhistory = ($lang == 'EN' ? 'Transactions History' : '交易记录');
                    $sheet2->cell('A'.$row, function($cells) use($transhistory) {
                        $cells->setValue($transhistory);
                        $cells->setValignment('center');
                        //$cell->setBackground('#a6ced9');
                        $cells->setFontWeight('bold');
                    });

                    $row++;

                    if($lang == 'EN'){
                        $sheet2->row($row, array(
                            'Amount', 'Date' , 'Type'
                        ));
                    }
                    else{
                        $sheet2->row($row, array(
                            '共计', '日期' , '类型'
                        ));
                    }

                    $cellNum = 'A'.$row.':C'.$row;
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                        $cells->setBackground('#a6ced9');
                    });
                    $row++;
                    //$ttd = 0;
                    foreach($transactions as $d){
                        $datetime = new DateTime($d->log_date);
                        $getdate = $datetime->format('M d,Y');
                        //$gettime = $datetime->format('h:i A');
                        //预存款 // Deposit
                        //折扣 // Discount
                        //已付款 // Payment
                        //退款 // Refund
                        if($lang == 'EN'){
                            $sheet2->row($row, array(
                              number_format($d->amount, 2, '.', ','), $d->log_date, $d->type
                            ));
                        }
                        else{
                            $dtype = '';
                            if($d->type == 'Deposit'){
                                $dtype = '预存款';
                            }
                            if($d->type == 'Discount'){
                                $dtype = '折扣';
                            }                            
                            if($d->type == 'Payment'){
                                $dtype = '已付款';
                            }                            
                            if($d->type == 'Refund'){
                                $dtype = '退款';
                            }

                            $sheet2->row($row, array(
                              number_format($d->amount, 2, '.', ','), $this->DateChinese($getdate), $dtype
                            ));
                        }
                        
                        $cellNum = 'A'.$row.':C'.$row;
                        $sheet2->cells($cellNum, function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            //$cell->setFontWeight('bold');
                        });
                        //$ttd+=$d->amount;
                        $row++;
                    }
                    $row++;

            }); // By Service sheet
        }
        // end by services type


            // By Batch
        if($type=='bybatch'){
            $bybatch = ($lang == 'EN' ? 'By Batch' : '按批次查看');
            $excel->sheet($bybatch, function($sheet2) use($id, $lang) {
                $byBatch = DB::connection('visa')
                        ->table('client_services as a')
                        ->select(DB::raw('id,detail,service_date, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y") as sdate, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%Y%m%d") as tdate'))
                        ->where('active',1)->where('group_id',$id)
                        ->orderBy('id','DESC')
                        ->groupBy('sdate')
                        ->get();
                $byBatchDate = $byBatch->pluck('tdate');

                $transactions = DB::connection('visa')
                    ->table('client_transactions as a')
                    ->select(DB::raw('
                        a.amount,a.type,a.service_id, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%M %d,%Y") as log_date'))
                        ->where('group_id', $id)
                        ->where('service_id', null)
                        ->orderBy('tdate','DESC')    
                        ->get();

                $transDate = $transactions->pluck('tdate');

                $merged = $byBatchDate->merge($transDate);

                $result = $merged->all();
                $tdates = array_unique($result);
                rsort($tdates);
                // \Log::info($tdates);

                $row = 1;

                if($lang == 'EN'){
                    $sheet2->row($row, array(
                        'Service Date','Package','Status', 'Details', 'Charge', 'Group Total Balance', 
                        //'Transaction Amount', 'Type'
                    ));
                }
                else{
                    $sheet2->row($row, array(
                        '服务日期','查询编号', '状态', '服务明细', '收费', '总余额', 
                        //'Transaction Amount', 'Type'
                    ));
                }


                $cellNum = 'A'.$row.':H'.$row;
                //$m1 = 'A'.$row;
                // $sheet2->mergeCells($m1);
                //$m2 = 'B'.$row;
                // $sheet2->mergeCells($m2);
                //$sheet2->setBorder($m1, 'thin');
                //$sheet2->setBorder($m2, 'thin');

                $sheet2->cells($cellNum, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                    //$cells->setBackground('#9ad1e0');
                });
                $sheet2->setWidth(array(
                    'A'     =>  20,
                    'B'     =>  25,
                    'C'     =>  15,
                    'D'     =>  50,
                    'E'     =>  15,
                    'F'     =>  25,
                    'G'     =>  25,
                    'H'     =>  15,
                ));
                $row+=2; //add space after header
                $ttd = 0;
                $tempTotal = 0;

                    $total_deposit = (
                        (
                            $this->GetGroupDeposit($id)
                            + $this->GetGroupPayment($id)
                        )
                    ) + 0;

                    $total_discount = $this->GetGroupDiscount($id);
                    $total_refund = $this->GetGroupRefund($id);
                    $total_group_cost = $this->GetGroupCostComplete($id);

                    $total_balance = (
                        (
                            $total_deposit
                            +$total_discount
                        )
                        - (
                            $total_refund
                            +$total_group_cost
                        )
                    );

                $groupcost = $total_balance;
                $lastcost = 0;
                foreach($tdates as $d){
                    // $service = ClientService::where('id',$d->id)->select('detail','service_date','service_id')->orderBy('id',"DESC")->first();
                    // $totalcost = ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$d->sdate)->where('group_id', $id)->where('active', 1)->where('status','complete')->value(DB::raw("SUM(cost + charge + tip)"));
                    //$totalcost = GroupMember::where('group_id', $id)->value(DB::raw("SUM(total_service_cost)"));
                    // if($totalcost == ''){
                    //     $totalcost = 0;
                    // }

                    //$datetime = new DateTime($service->service_date);
                    //$getdate = $datetime->format('M d,Y');
                    $y =  substr($d,0,4);
                    $m =  substr($d,4,2);
                    $day =  substr($d,6,2);
                    $nDate = $m.'/'.$day.'/'.$y;
                    if($lang == 'EN'){
                        $sheet2->row($row, array(
                            Carbon::parse($nDate)->format('F d,Y')
                        ));
                    }
                    else{
                        $sheet2->row($row, array(
                            $this->DateChinese(Carbon::parse($nDate)->format('F d,Y'))
                        ));
                    }

                    $cellNum = 'A'.$row.':F'.$row;
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                        $cells->setBackground('#ddd9c3');
                    });

                    $row++;

                    // if($lang == 'EN'){
                    //     $sheet2->row($row, array(
                    //         // 'Package', 'Date Recorded','Details', 'Status', 'Cost', 'Service Fee', 'Additional Fee', 'Discount'
                    //         'Package', 'Status', 'Details', 'Charge', 'Group Total Balance'
                    //     ));
                    // }
                    // else{
                    //     $sheet2->row($row, array(
                    //         '查询编号', '状态', '服务明细', '收费', '总余额'
                    //     ));
                    // }

                    // $cellNum = 'B'.$row.':F'.$row;
                    // $sheet2->cells($cellNum, function($cells) {
                    //     $cells->setAlignment('center');
                    //     $cells->setValignment('center');
                    //     //$cells->setFontWeight('bold');
                    //     //$cells->setBackground('#d8e0e2');
                    // });
                    // $row++;

                    $query = ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$nDate)->where('group_id', $id)->where('active', 1)->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('client_id')->get();
                        $ctr2 = 0;
                        //$servTotal = 0;
                        $client = [];
                        $flag = true;
                        foreach($query as $m){
                            $ss =  ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$nDate)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->with('discount2')->get();
                            //\Log::info($ss);
                            if(count($ss) > 0){
                                $client = User::where('id',$m->client_id)->select('first_name','last_name')->first()->full_name;
                                 //\Log::info($ss);
                                $totalcost = ClientService::where('service_id',$id)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip + com_clent + com_agent)"));
                                $services = $ss;

                                $sheet2->row($row, array(
                                    '',$client
                                ));
                                $cellNum = 'B'.$row;
                                $sheet2->cells($cellNum, function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                    //$cells->setBackground('#9ad1e0');
                                });
                                $row++;


                                foreach($services as $serv){
                                    $tcost =  $serv->cost + $serv->tip + $serv->charge + $serv->com_client + $serv->com_agent;
                                    if($serv->status != 'complete'){
                                        $tcost = 0;
                                    }

                                    $sp = $serv->status;
                                    $sp_cn = $this->statusChinese($serv->status);
                                    if($serv->active==0){
                                        $sp = "CANCELLED";
                                        $sp_cn = "取消";
                                    }
                                    //$servTotal += $tcost;

                                    // $tempTotal = $groupcost - $lastcost;
                                    if($flag= false){
                                        $tempTotal = $groupcost - $lastcost;
                                    }
                                    else{
                                        $tempTotal = $groupcost + $lastcost;
                                    }
                                    $translated = Service::where('id',$serv->service_id)->first();
                                    $cnserv =$serv->detail;
                                    if($translated){
                                        $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                                    }

                                    if($lang == 'EN'){
                                        $sheet2->row($row, array(
                                            '',
                                            $serv->tracking,
                                            $sp,
                                            $serv->detail,
                                            '-'.$tcost,
                                            $tempTotal

                                        ));
                                    }
                                    else{
                                        $sheet2->row($row, array(
                                            '',
                                            $serv->tracking,
                                            $sp_cn,
                                            $cnserv,
                                            '-'.$tcost,
                                            $tempTotal

                                        ));
                                    }

                                    $lastcost = $tcost;
                                    $groupcost = $tempTotal;
                                    $cellNum = 'A'.$row.':F'.$row;
                                    $sheet2->cells($cellNum, function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        //$cells->setFontWeight('bold');
                                        // $cells->setBackground('#d8e0e2');
                                    });
                                    $row++;

                                    if($serv->discount_amount>0){
                                        $tempTotal = $groupcost + $lastcost;
                                        $sheet2->row($row, array(
                                            '',
                                            '',
                                            '',
                                            'Discount',
                                            ' +'.$serv->discount_amount,
                                            $tempTotal

                                        ));

                                        $lastcost = -1 *$serv->discount_amount;
                                        $groupcost = $tempTotal;

                                    }

                                    $cellNum = 'A'.$row.':F'.$row;
                                    $sheet2->cells($cellNum, function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        //$cells->setFontWeight('bold');
                                        // $cells->setBackground('#d8e0e2');
                                    });

                                    $row++;
                                    $flag = false;
                                }
                            }
                            $row++;
                            $ctr2++;
                        }

                        // $row++;
                        // $row++;
                        $flag = false;
                        foreach($transactions as $t){
                            //\Log::info($t->tdate.' '.$d);
                            if($t->tdate == $d){
                                if($t->type != 'Discount' || ($t->type == 'Discount' && $t->service_id == null)){
                                    $dtype = '';
                                    $sign = ' +';
                                    if($flag= false){
                                        $tempTotal = $groupcost - $lastcost;
                                    }
                                    else{
                                        $tempTotal = $groupcost + $lastcost;
                                    }

                                    if($t->type == 'Deposit'){
                                        $dtype = '预存款';

                                        $lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }
                                    if($t->type == 'Discount'){
                                        $dtype = '折扣';
                                        $lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }                            
                                    if($t->type == 'Payment'){
                                        $dtype = '已付款';
                                        $lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }                            
                                    if($t->type == 'Refund'){
                                        $dtype = '退款';
                                        $lastcost = $t->amount;
                                        $sign = '-';
                                        //$tempTotal = $groupcost + (-1*$lastcost);
                                    }

                                    
                                        if($lang == 'EN'){
                                            $sheet2->row($row, array(
                                                '',
                                                '',
                                                '',
                                                $t->type,
                                                $sign.$t->amount,
                                                $tempTotal

                                            ));
                                        }
                                        else{
                                            $sheet2->row($row, array(
                                                '',
                                                '',
                                                '',
                                                $dtype,
                                                $t->amount,
                                                $tempTotal

                                            ));
                                        }
                                    }
                                    $groupcost = $tempTotal;

                                    $flag = true;

                                    $cellNum = 'A'.$row.':F'.$row;
                                    $sheet2->cells($cellNum, function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                        // $cells->setBackground('#d8e0e2');
                                    });
                                    $row++;
                            }
                        }

                    $row++;
                }
                $row++;


                    // $total_deposit = (
                    //     (
                    //         $this->GetGroupDeposit($id)
                    //         + $this->GetGroupPayment($id)
                    //     )
                    // ) + 0;

                    // $total_discount = $this->GetGroupDiscount($id);
                    // $total_refund = $this->GetGroupRefund($id);
                    // $total_group_cost = $this->GetGroupCostComplete($id);

                    // $total_balance = (
                    //     (
                    //         $total_deposit
                    //         +$total_discount
                    //     )
                    //     - (
                    //         $total_refund
                    //         +$total_group_cost
                    //     )
                    // );

                    $textdeposit = ($lang == 'EN' ? 'Total Deposit : ' : '总已付款 : ');
                    $sheet2->cell('E'.$row, function($cells) use($total_deposit, $textdeposit) {
                        $cells->setValue($textdeposit.number_format($total_deposit, 2, '.', ','));
                    });

                    $cellNum = 'E'.$row.':F'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells)  {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textcost = ($lang == 'EN' ? 'Total Cost : ' : '总花费 : ');
                    $sheet2->cell('E'.$row, function($cells) use($total_group_cost, $textcost) {
                        $cells->setValue($textcost.number_format($total_group_cost, 2, '.', ','));
                    });
                    $cellNum = 'E'.$row.':F'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textdiscount = ($lang == 'EN' ? 'Total Discount : ' : '总折扣 : ');
                    $sheet2->cell('E'.$row, function($cells) use($total_discount, $textdiscount) {
                        $cells->setValue($textdiscount.number_format($total_discount, 2, '.', ','));
                    });
                    $cellNum = 'E'.$row.':F'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textrefund = ($lang == 'EN' ? 'Total Refund : ' : '总退款 : ');
                    $sheet2->cell('E'.$row, function($cells) use($total_refund, $textrefund) {
                        $cells->setValue($textrefund.number_format($total_refund, 2, '.', ','));
                    });
                    $cellNum = 'E'.$row.':F'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textbalance = ($lang == 'EN' ? 'Total Balance : ' : '总余额 : ');
                    $sheet2->cell('E'.$row, function($cells) use($total_balance, $textbalance) {
                        $cells->setValue($textbalance.number_format($total_balance, 2, '.', ','));
                    });
                    $cellNum = 'E'.$row.':F'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setBackground('#a6ced9');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $row++;

                    $row++;

                    //$transactions = ClientTransactions::where('group_id', $id)->get();
                    // $transactions = DB::connection('visa')
                    // ->table('client_transactions as a')
                    // ->select(DB::raw('
                    //     a.amount,a.type, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%M %d,%Y") as log_date'))
                    //     ->where('group_id', $id)
                    //     ->orderBy('tdate','DESC')    
                    //     ->get();
                        
                    // $transhistory = ($lang == 'EN' ? 'Transactions History' : '交易记录');
                    // $sheet2->cell('A'.$row, function($cells) use($transhistory) {
                    //     $cells->setValue($transhistory);
                    //     $cells->setValignment('center');
                    //     //$cell->setBackground('#a6ced9');
                    //     $cells->setFontWeight('bold');
                    // });

                    // $row++;

                    // if($lang == 'EN'){
                    //     $sheet2->row($row, array(
                    //         'Amount', 'Date' , 'Type'
                    //     ));
                    // }
                    // else{
                    //     $sheet2->row($row, array(
                    //         '共计', '日期' , '类型'
                    //     ));
                    // }

                    // $cellNum = 'A'.$row.':C'.$row;
                    // $sheet2->cells($cellNum, function($cells) {
                    //     $cells->setAlignment('center');
                    //     $cells->setValignment('center');
                    //     $cells->setFontWeight('bold');
                    //     $cells->setBackground('#a6ced9');
                    // });
                    // $row++;
                    //$ttd = 0;
                    // foreach($transactions as $d){
                    //     $datetime = new DateTime($d->log_date);
                    //     $getdate = $datetime->format('M d,Y');
                    //     //$gettime = $datetime->format('h:i A');
                    //     //预存款 // Deposit
                    //     //折扣 // Discount
                    //     //已付款 // Payment
                    //     //退款 // Refund
                    //     if($lang == 'EN'){
                    //         $sheet2->row($row, array(
                    //           number_format($d->amount, 2, '.', ','), $d->log_date, $d->type
                    //         ));
                    //     }
                    //     else{
                    //         $dtype = '';
                    //         if($d->type == 'Deposit'){
                    //             $dtype = '预存款';
                    //         }
                    //         if($d->type == 'Discount'){
                    //             $dtype = '折扣';
                    //         }                            
                    //         if($d->type == 'Payment'){
                    //             $dtype = '已付款';
                    //         }                            
                    //         if($d->type == 'Refund'){
                    //             $dtype = '退款';
                    //         }

                    //         $sheet2->row($row, array(
                    //           number_format($d->amount, 2, '.', ','), $this->DateChinese($getdate), $dtype
                    //         ));
                    //     }
                        
                    //     $cellNum = 'A'.$row.':C'.$row;
                    //     $sheet2->cells($cellNum, function($cells) {
                    //         $cells->setAlignment('center');
                    //         $cells->setValignment('center');
                    //         //$cell->setFontWeight('bold');
                    //     });
                    //     //$ttd+=$d->amount;
                    //     $row++;
                    // }
                    $row++;

            });
            // END By Batch sheet
        }
        // end by batch type

        })->export('xls'); // Excel
    }

    public function getGroupSummary($id,$type, $lang = 'EN', $services = null, $month = 0, $year = 0) {
        header('Set-Cookie: fileDownload=true; path=/');
        header('Cache-Control: max-age=60, must-revalidate');
        header("Content-type: pplication/vnd.ms-excel");
        $getGroupMembers = DB::connection('visa')
            ->table('group_members as a')
            ->select('a.*')
            ->where('a.group_id', $id)
            ->get();

        $getGroupName = DB::connection('visa')
            ->table('groups as a')
            ->select('a.*')
            ->where('a.id', $id)
            ->first();

        $filename = $lang.strtoupper($type).'- '.$getGroupName->name .' -'. Carbon::now();
        Excel::create($filename, function($excel) use ($filename, $getGroupMembers, $id, $type, $lang, $services, $month, $year) {

            // Set the title
            $excel->setTitle($filename);

            // Chain the setters
            $excel->setCreator('WYC')
                  ->setCompany('WYC');

            // Call them separately
            $excel->setDescription('Group Summary');

            //First Sheet -> Group Member's Cash Flow
            if($type=='bymembers'){
                $group_summary = ($lang == 'EN' ? 'Group Summary' : '总结报告');
                $excel->sheet($group_summary, function($sheet1) use ($getGroupMembers, $id, $lang) {
                    $row = 1;
                    $tempTotal = 0;
                    if($lang == 'EN'){
                         $sheet1->row($row, array(
                            'Date', 
                            'Service', 
                            'Status',
                            // 'Note',
                            'Charge',
                            'Group Total Cost' 
                        ));
                    }
                    else{                    
                        $sheet1->row($row, array(
                            '建立日期', 
                            '服务', 
                            '状态',
                            // '备注',
                            '收费',
                            '群组总花费' 
                        ));
                    }
                    $cellNum = 'A'.$row.':E'.$row;
                    $sheet1->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                        $cells->setBackground('#6fc4dc');
                    });
                    $sheet1->setWidth(array(
                        'A'     =>  20,
                        'B'     =>  40,
                        'C'     =>  15,
                        'D'     =>  15,
                        'E'     =>  25,
                    ));
                    $row++;

                    foreach ($getGroupMembers as $value) {
                        $getName = User::where('id',$value->client_id)->select('first_name','last_name')->first()->full_name;

                        $row++;
                        $sheet1->row($row, array(
                            ucfirst($getName)
                        ));
                        $cellNum = 'A'.$row.':E'.$row;
                        $sheet1->mergeCells($cellNum);
                        $sheet1->cells($cellNum, function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setBackground('#a6ced9');
                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                            $cells->setFontWeight('bold');
                        });

                        $getServices = DB::connection('visa')
                        ->table('client_services as a')
                        ->select('a.*')
                        ->where('a.client_id', $value->client_id)
                        ->where('a.group_id', $id)
                        ->get();

                        $bal = 0;
                        foreach ($getServices as $s) {
                            $datetime = new DateTime($s->service_date);
                            $getdate = $datetime->format('M d,Y');
                            $gettime = $datetime->format('h:i A');

                            //Service Charge
                            $chrg = $s->charge + $s->cost + $s->tip + $s->com_client + $s->com_agent;
                            if($s->status != 'complete' && $s->status != 'Complete'){
                                $chrg = 0;
                            }

                            if(strpos($s->detail, "9A") === false && $s->status != 'complete'){
                                $chrg = $s->charge + $s->cost + $s->tip + $s->com_client + $s->com_agent;
                            }

                            //Subtotal
                            //$sub = $chrg;

                            //Per Person Balance
                            // if($s->active == 0){
                            //     //$sub = 0;
                            //     $chrg = 0;
                            // }


                            //Cancelled
                            $sp = $s->status;
                            $sp_cn = $this->statusChinese($sp);
                            if($s->active==0){
                                $sp = "CANCELLED";
                                $sp_cn = "取消";
                                $chrg = 0;

                            }
                            $bal += $chrg;

                            //I
                            $tempTotal +=$chrg;

                            //note
                            $the_string = '';
                            $the_string = $s->remarks;
                            $the_word  = "<small>";
                            $the_string = str_replace('</small>', '', $the_string);
                            $count_occur = substr_count($the_string,$the_word);
                            for($i = 0; $i<$count_occur; $i++){
                                if (strpos($the_string, $the_word) !== false) {
                                    $the_string = str_replace('</br>', ' --> ', $the_string);
                                    $postr = strpos($the_string, $the_word);
                                    $str1 = substr($the_string,0,$postr-8);
                                    $str2 = substr($the_string,$postr+7);
                                    $the_string = $str1.$str2;
                                }
                            }


                            $row++;

                            if($lang == 'EN'){
                                $sheet1->row($row, array(
                                     $getdate, 
                                     $s->detail, 
                                     ucfirst($sp), 
                                     $chrg, 
                                     '-'.$tempTotal
                                 ));
                            }
                            else{
                                $translated = Service::where('id',$s->service_id)->first();
                                $cnserv =$s->detail;
                                if($translated){
                                    $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                                }
                                 $sheet1->row($row, array(
                                     $this->DateChinese($getdate), 
                                     $cnserv, 
                                     $sp_cn, 
                                     $chrg, 
                                     '-'.$tempTotal
                                 ));
                            }

                            //add cell comment
                            // if($the_string != ''){
                            //     $sheet1->getComment('B'.$row)->getText()->createTextRun($the_string);
                            // }

                            if($the_string != ''){
                                $row++;
                                $sheet1->row($row, array(
                                     '', 
                                     'NOTE : '.strtolower($the_string), 
                                     '', 
                                     '', 
                                     ''
                                 ));
                            $cellNum = 'A'.$row.':E'.$row;
                            $sheet1->cells($cellNum, function($cells) {
                                $cells->setFontSize(10);
                                $cells->setFontColor('#bf4743');
                            });
                            }

                        }
                        $row++;
                        $row++;

                        if($lang == 'EN'){
                                $sheet1->row($row, array(
                                     '', 
                                     '-- Member Subtotal --', 
                                     '', 
                                     $bal, 
                                     ''
                                 ));
                            }
                            else{
                                 $sheet1->row($row, array(
                                     '', 
                                     '-- 成员小计 --', 
                                     '', 
                                     $bal, 
                                     ''
                                 ));
                            }

                        $cellNum = 'A'.$row.':E'.$row;
                        $sheet1->cells($cellNum, function($cells) {
                            $cells->setFontWeight('bold');
                        });

                        //$row++;
                        $row++;
                    }

                    $row++;
                    $summaryrow = $row;

                    $total_deposit = (
                        (
                            $this->GetGroupDeposit($id)
                            + $this->GetGroupPayment($id)
                        )
                    ) + 0;

                    $total_discount = $this->GetGroupDiscount($id);
                    $total_refund = $this->GetGroupRefund($id);
                    $total_group_cost = $this->GetGroupCostComplete($id);

                    // temp 
                    $total_group_cost = $tempTotal;

                    $total_balance = (
                        (
                            $total_deposit
                            + $total_discount
                        )
                        - (
                            $total_refund
                            + $total_group_cost
                        )
                    );

                    $textdeposit = ($lang == 'EN' ? 'Total Deposit : ' : '总已付款 : ');
                    $sheet1->cell('D'.$row, function($cell) use($total_deposit, $textdeposit) {
                        $cell->setValue($textdeposit.number_format($total_deposit, 2, '.', ','));
                    });

                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet1->mergeCells($cellNum);
                    $sheet1->cells($cellNum, function($cells)  {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textcost = ($lang == 'EN' ? 'Total Cost : ' : '总花费 : ');
                    $sheet1->cell('D'.$row, function($cell) use($total_group_cost, $textcost) {
                        $cell->setValue($textcost.'-'.number_format($total_group_cost, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet1->mergeCells($cellNum);
                    $sheet1->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textdiscount = ($lang == 'EN' ? 'Total Discount : ' : '总折扣 : ');
                    $sheet1->cell('D'.$row, function($cell) use($total_discount, $textdiscount) {
                        $cell->setValue($textdiscount.number_format($total_discount, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet1->mergeCells($cellNum);
                    $sheet1->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textrefund = ($lang == 'EN' ? 'Total Refund : ' : '总退款 : ');
                    $sheet1->cell('D'.$row, function($cell) use($total_refund, $textrefund) {
                        $cell->setValue($textrefund.number_format($total_refund, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet1->mergeCells($cellNum);
                    $sheet1->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textbalance = ($lang == 'EN' ? 'Total Balance : ' : '总余额 : ');
                    $sheet1->cell('D'.$row, function($cell) use($total_balance, $textbalance) {
                        $cell->setValue($textbalance.number_format($total_balance, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet1->mergeCells($cellNum);
                    $sheet1->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setBackground('#a6ced9');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $row++;
                    $row++;

                    //$transactions = ClientTransactions::where('group_id', $id)->get();
                    $transactions = DB::connection('visa')
                    ->table('client_transactions as a')
                    ->select(DB::raw('
                        a.amount,a.type, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%M %d,%Y") as log_date'))
                        ->where('group_id', $id)
                        ->orderBy('tdate','DESC')    
                        ->get();

                    $transhistory = ($lang == 'EN' ? 'Transactions History' : '交易记录');
                    $sheet1->cell('A'.$row, function($cell) use($transhistory) {
                        $cell->setValue($transhistory);
                        $cell->setValignment('center');
                        //$cell->setBackground('#a6ced9');
                        $cell->setFontWeight('bold');
                    });

                    $row++;

                    if($lang == 'EN'){
                        $sheet1->row($row, array(
                            'Amount', 'Date' , 'Type'
                        ));
                    }
                    else{
                        $sheet1->row($row, array(
                            '共计', '日期' , '类型'
                        ));
                    }

                    $cellNum = 'A'.$row.':C'.$row;
                    $sheet1->cells($cellNum, function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                        $cell->setFontWeight('bold');
                        $cell->setBackground('#a6ced9');
                    });
                    $row++;
                    //$ttd = 0;
                    foreach($transactions as $d){
                        $datetime = new DateTime($d->log_date);
                        $getdate = $datetime->format('M d,Y');
                        //$gettime = $datetime->format('h:i A');
                        //预存款 // Deposit
                        //折扣 // Discount
                        //已付款 // Payment
                        //退款 // Refund
                        if($lang == 'EN'){
                            $sheet1->row($row, array(
                              number_format($d->amount, 2, '.', ','), $d->log_date, $d->type
                            ));
                        }
                        else{
                            $dtype = '';
                            if($d->type == 'Deposit'){
                                $dtype = '预存款';
                            }
                            if($d->type == 'Discount'){
                                $dtype = '折扣';
                            }                            
                            if($d->type == 'Payment'){
                                $dtype = '已付款';
                            }                            
                            if($d->type == 'Refund'){
                                $dtype = '退款';
                            }

                            $sheet1->row($row, array(
                              number_format($d->amount, 2, '.', ','), $this->DateChinese($getdate), $dtype
                            ));
                        }
                        
                        $cellNum = 'A'.$row.':C'.$row;
                        $sheet1->cells($cellNum, function($cell) {
                            $cell->setAlignment('center');
                            $cell->setValignment('center');
                            //$cell->setFontWeight('bold');
                        });
                        //$ttd+=$d->amount;
                        $row++;
                    }
                    $row++;

                }); // First Sheet
            }
            // end bymembers type

            // By Service
            if($type=='byservices'){
            $byservices = ($lang == 'EN' ? 'By Service' : '按服务查看');
            $excel->sheet($byservices, function($sheet2) use($id, $lang, $services, $month, $year) {
                $selectedservice = $services;
                $expservices = explode(',',rtrim($selectedservice, ','));
                if($selectedservice != 0 || ($month >0 && year > 0)){      
                    // \Log::info($month);
                    // \Log::info($year);
                    // \Log::info($expservices);
                    $month2 = $month;
                    if($month < 10){
                        $month2 = ltrim($month2, "0");
                    }
                    $byService = ClientService::where('group_id',$id)
                            ->with('discount')
                            ->whereIn('service_id',$expservices)
                           // ->where('service_date','LIKE','%'.$login)
                            ->where(function($q) use($month,$year, $month2){
                                    $q->where('service_date','LIKE', $month.'%')->where('service_date','LIKE', '%'.$year);
                                    $q->orWhere('service_date','LIKE', $month2.'%')->where('service_date','LIKE', '%'.$year);
                                })
                            ->groupBy('service_id')->orderBy('id',"DESC")
                            ->get();
                    //\Log::info($byService);


                // $byService = DB::connection('visa')
                //     ->table('client_services as a')
                //     ->select(DB::raw('
                //         a.id,a.service_id, date_format(STR_TO_DATE(a.service_date, "%m/%d/%Y"),"%Y%m%d") as tdate'))
                //         ->where('group_id', $id)
                //         ->whereIn('service_id',$services)
                //         ->orderBy('tdate','DESC')   
                //         ->get();

                // $getdate = $byService->pluck('tdate');
                // $tdates = array_unique($getdate->toArray());
                    
                }
                else{
                    $byService = ClientService::where('group_id',$id)
                            ->with('discount')->groupBy('service_id')->orderBy('id',"DESC")
                            ->get();
                }


                $row = 1;
                if($lang == 'EN'){
                    $sheet2->row($row, array(
                        'Service Name',
                        '', '',
                        //'Latest Date',
                        'Total Service Cost',''
                    ));
                }
                else{                
                    $sheet2->row($row, array(
                        '服务明细',
                        '', '',
                        //'最近的服务日期',
                        '总服务费',''
                    ));
                }
                $cellNum = 'A'.$row.':D'.$row;
                $m1 = 'A'.$row.':C'.$row;
                $sheet2->mergeCells($m1);
                $m2 = 'C'.$row;
               // $sheet2->mergeCells($m2);
                $m3 = 'D'.$row.':E'.$row;
                $sheet2->mergeCells($m3);
                $sheet2->setBorder($m1, 'thin');
                $sheet2->setBorder($m2, 'thin');
                $sheet2->setBorder($m3, 'thin');
                $sheet2->cells($cellNum, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#6fc4dc');
                });
                $sheet2->setWidth(array(
                    'A'     =>  30,
                    'B'     =>  20,
                    'C'     =>  15,
                    'D'     =>  15,
                    'E'     =>  15,
                ));
                $row+=2; //add space after header
                $ttd = 0;
                $tempTotal = 0;
                $ts = 0;
                $totalservdisc = 0;

                foreach($byService as $d){
                    $service = ClientService::where('id',$d->id)->select('detail','service_date','service_id')->first();
                    $totalcost = ClientService::where('service_id',$d->service_id)
                                    //->where('status','complete')
                                    ->where('group_id', $id)
                                    ->where('active', 1)
                                    ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
                    if($totalcost == ''){
                        $totalcost = 0;
                    }
                    $datetime = new DateTime($service->service_date);
                    $getdate = $datetime->format('M d,Y');


                    if($lang == 'EN'){
                        $sheet2->row($row, array(
                            $service->detail,
                            '', '',
                            //$getdate , 
                            '-'.$totalcost,''
                        ));
                    }
                    else{
                        $translated = Service::where('id',$service->service_id)->first();
                        $cnserv =$service->detail;
                        if($translated){
                            $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                        }
                        $sheet2->row($row, array(
                            $cnserv,'', '',
                            //$this->DateChinese($getdate) , 
                            '-'.$totalcost,''
                        ));
                    }


                    $m1 = 'A'.$row.':C'.$row;
                    $sheet2->mergeCells($m1);
                    //$m2 = 'C'.$row;
                    //$sheet2->mergeCells($m2);
                    $m3 = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($m3);
                    $sheet2->setBorder($m1, 'thin');
                    //$sheet2->setBorder($m2, 'thin');
                    $sheet2->setBorder($m3, 'thin');
                    $cellNum = 'A'.$row.':E'.$row;
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                        $cells->setBackground('#a6ced9');
                    });

                    $row++;
                    $row++;
                    if($lang == 'EN'){
                        $sheet2->row($row, array(
                            //'Service Date', 
                            'Package', 
                            'Status', 
                            'Charge', 
                            'Group Total Balance',
                            ''
                        ));
                    }
                    else{
                        $sheet2->row($row, array(
                            //'服务日期', 
                            '查询编号', 
                            '状态', 
                            '收费', 
                            '总余额',
                            ''
                        ));
                    }

                    $cellNum = 'A'.$row.':E'.$row;
                    $m3 = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($m3);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                        //$cells->setBackground('#d8e0e2');
                    });
                    //$row++;
                    $row++;
                    // $query = ClientService::where('service_id',$service->service_id)->where('group_id',$id)->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('service_date')->get();

                    $query = DB::connection('visa')
                                    ->table('client_services as a')
                                    ->select(DB::raw('a.id,a.service_id,a.service_date, date_format(STR_TO_DATE(a.service_date, "%m/%d/%Y"),"%Y%m%d") as tdate'))
                                        ->where('group_id', $id)
                                        ->where('service_id',$service->service_id)
                                        ->orderBy('tdate','DESC')  
                                        ->groupBy('service_date')
                                        ->get();
                    
                    // \Log::info($services);
                    // \Log::info($month);
                    // \Log::info($year);
                    if($selectedservice != 0 || ($month > 0 && $year > 0)){  
                            $month2 = $month;
                            if($month < 10){
                                $month2 = ltrim($month2, "0");
                            }
                        $query = DB::connection('visa')
                            ->table('client_services as a')
                            ->select(DB::raw('a.id,a.service_id,a.service_date, date_format(STR_TO_DATE(a.service_date, "%m/%d/%Y"),"%Y%m%d") as tdate'))
                                ->where('group_id', $id)
                                ->where('service_id',$service->service_id)
                                ->where(function($q) use($month,$year,$month2){
                                    $q->where('service_date','LIKE', $month.'%')->where('service_date','LIKE', '%'.$year);
                                    $q->orWhere('service_date','LIKE', $month2.'%')->where('service_date','LIKE', '%'.$year);
                                })
                                ->orderBy('tdate','DESC')  
                                ->groupBy('service_date')
                                ->get();
                    }

                        $ctr2 = 0;
                        $servTotal = 0;
                        $client = [];
                        foreach($query as $m){

                            if($lang == 'EN'){
                                $sheet2->row($row, array(
                                    Carbon::parse($m->service_date)->format('F d,Y')
                                ));
                            }
                            else{
                                $sheet2->row($row, array(
                                    $this->DateChinese(Carbon::parse($m->service_date)->format('F d,Y'))
                                ));
                            }

                            $cellNum = 'A'.$row.':E'.$row;
                            $sheet2->mergeCells($cellNum);
                            $sheet2->cells($cellNum, function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                                $cells->setBackground('#ddd9c3');
                            });

                            $row++;

                            $ss =  ClientService::where('service_id',$service->service_id)->where('group_id',$id)->with('discount2')->orderBy('service_date','DESC')->where('service_date',$m->service_date)->get();
                            $ts += count($ss);
                            if(count($ss)){
                                
                                // $totalcost = ClientService::where('service_id',$id)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
                                $services = $ss;
                                $cname = '';

                                foreach($services as $serv){
                                    $servdisc = 0;
                                    if($cname != ''){
                                        $row++;
                                    }
                                    $client = User::where('id',$serv->client_id)->select('first_name','last_name')->first()->full_name;
                                    if($cname != $client){                                    
                                        $sheet2->row($row, array(
                                            $client
                                        ));
                                        $cellNum = 'A'.$row;
                                        $sheet2->cells($cellNum, function($cells) {
                                            $cells->setAlignment('center');
                                            $cells->setValignment('center');
                                            $cells->setFontWeight('bold');
                                            //$cells->setBackground('#9ad1e0');
                                        });
                                        $row++;
                                        $cname = $client;
                                    }
                                    //$row++;

                                    $tcost = $serv->cost + $serv->tip + $serv->charge + $serv->com_agent + $serv->com_client;
                                    if($serv->discount2 && count($serv->discount2) > 0){
                                        $servdisc = $serv->discount2[0]['amount'];
                                    }

                                    if($serv->status != 'complete' && $serv->status != 'Complete'){
                                        $tcost = 0;
                                    }

                                    if(strpos($serv->detail, "9A") === false && $serv->status != 'complete'){
                                        $tcost = $serv->charge + $serv->cost + $serv->tip + $serv->com_client + $serv->com_agent;
                                        if($serv->discount2 && count($serv->discount2) > 0){
                                            $servdisc = $serv->discount2[0]['amount'];
                                        }
                                    }

                                    if($servdisc > 0 ){
                                        $tcost -= $servdisc;
                                        $totalservdisc += $servdisc;
                                    }

                                    $sp = $serv->status;
                                    $sp_cn = $this->statusChinese($serv->status);
                                    if($serv->active==0){
                                        $sp = "CANCELLED";
                                        $sp_cn = "取消";
                                        $tcost = 0;
                                    }

                                    $servTotal += $tcost;
                                    $tempTotal += $tcost;

                                    if($lang == 'EN'){
                                        $sheet2->row($row, array(
                                            $serv->tracking,
                                            $sp,
                                            $tcost,
                                            '-'.$tempTotal,
                                            ''
                                            //,$ts
                                        ));
                                    }
                                    else{
                                        $sheet2->row($row, array(
                                            $serv->tracking,
                                            $sp_cn,
                                            $tcost,
                                            '-'.$tempTotal,
                                            ''
                                        ));
                                    }


                                    $cellNum = 'A'.$row.':E'.$row;
                                    $m2 = 'C'.$row;
                                    //$sheet2->mergeCells($m2);
                                    $m3 = 'D'.$row.':E'.$row;
                                    $sheet2->mergeCells($m3);
                                    $sheet2->cells($cellNum, function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        // $cells->setFontWeight('bold');
                                        // $cells->setBackground('#d8e0e2');
                                    });
                                    $row++;
                                }
                            }
                            $row++;
                            $ctr2++;
                        }

                    $row++;
                }
                $row++;

                    $total_deposit = (
                        (
                            $this->GetGroupDeposit($id)
                            + $this->GetGroupPayment($id)
                        )
                    ) + 0;

                    $total_discount = $this->GetGroupDiscount($id) - $totalservdisc;
                    $total_refund = $this->GetGroupRefund($id);
                    $total_group_cost = $this->GetGroupCostComplete($id); 
                    $total_group_cost = $tempTotal;

                    $total_balance = (
                        (
                            $total_deposit
                            +$total_discount
                        )
                        - (
                            $total_refund
                            +$total_group_cost
                        )
                    );

                    $textdeposit = ($lang == 'EN' ? 'Total Deposit : ' : '总已付款 : ');
                    $sheet2->cell('D'.$row, function($cells) use($total_deposit, $textdeposit) {
                        $cells->setValue($textdeposit.number_format($total_deposit, 2, '.', ','));
                    });

                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells)  {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textcost = ($lang == 'EN' ? 'Total Cost : ' : '总花费 : ');
                    $sheet2->cell('D'.$row, function($cells) use($total_group_cost, $textcost) {
                        $cells->setValue($textcost.'-'.number_format($total_group_cost, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textdiscount = ($lang == 'EN' ? 'Total Discount : ' : '总折扣 : ');
                    $sheet2->cell('D'.$row, function($cells) use($total_discount, $textdiscount) {
                        $cells->setValue($textdiscount.number_format($total_discount, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textrefund = ($lang == 'EN' ? 'Total Refund : ' : '总退款 : ');
                    $sheet2->cell('D'.$row, function($cells) use($total_refund, $textrefund) {
                        $cells->setValue($textrefund.number_format($total_refund, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textbalance = ($lang == 'EN' ? 'Total Balance : ' : '总余额 : ');
                    $sheet2->cell('D'.$row, function($cells) use($total_balance, $textbalance) {
                        $cells->setValue($textbalance.number_format($total_balance, 2, '.', ','));
                    });
                    $cellNum = 'D'.$row.':E'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setBackground('#a6ced9');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $row++;

                    $row++;

                    //$transactions = ClientTransactions::where('group_id', $id)->get();
                    $transactions = DB::connection('visa')
                    ->table('client_transactions as a')
                    ->select(DB::raw('
                        a.amount,a.type, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%M %d,%Y") as log_date'))
                        ->where('group_id', $id)
                        ->orderBy('tdate','DESC')    
                        ->get();
                        
                    $transhistory = ($lang == 'EN' ? 'Transactions History' : '交易记录');
                    $sheet2->cell('A'.$row, function($cells) use($transhistory) {
                        $cells->setValue($transhistory);
                        $cells->setValignment('center');
                        //$cell->setBackground('#a6ced9');
                        $cells->setFontWeight('bold');
                    });

                    $row++;

                    if($lang == 'EN'){
                        $sheet2->row($row, array(
                            'Amount', 'Date' , 'Type'
                        ));
                    }
                    else{
                        $sheet2->row($row, array(
                            '共计', '日期' , '类型'
                        ));
                    }

                    $cellNum = 'A'.$row.':C'.$row;
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                        $cells->setBackground('#a6ced9');
                    });
                    $row++;
                    //$ttd = 0;
                    foreach($transactions as $d){
                        $datetime = new DateTime($d->log_date);
                        $getdate = $datetime->format('M d,Y');
                        //$gettime = $datetime->format('h:i A');
                        //预存款 // Deposit
                        //折扣 // Discount
                        //已付款 // Payment
                        //退款 // Refund
                        if($lang == 'EN'){
                            $sheet2->row($row, array(
                              number_format($d->amount, 2, '.', ','), $d->log_date, $d->type
                            ));
                        }
                        else{
                            $dtype = '';
                            if($d->type == 'Deposit'){
                                $dtype = '预存款';
                            }
                            if($d->type == 'Discount'){
                                $dtype = '折扣';
                            }                            
                            if($d->type == 'Payment'){
                                $dtype = '已付款';
                            }                            
                            if($d->type == 'Refund'){
                                $dtype = '退款';
                            }

                            $sheet2->row($row, array(
                              number_format($d->amount, 2, '.', ','), $this->DateChinese($getdate), $dtype
                            ));
                        }
                        
                        $cellNum = 'A'.$row.':C'.$row;
                        $sheet2->cells($cellNum, function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            //$cell->setFontWeight('bold');
                        });
                        //$ttd+=$d->amount;
                        $row++;
                    }
                    $row++;

            }); // By Service sheet
        }
        // end by services type


            // By Batch
        if($type=='bybatch'){ //  jeff
            $bybatch = ($lang == 'EN' ? 'By Batch' : '按批次查看');
            $excel->sheet($bybatch, function($sheet2) use($id, $lang) {
                $byBatch = DB::connection('visa')
                        ->table('by_batch')
                        ->where('group_id',$id)
                        ->orderBy('date','DESC')
                        ->get();
                //$byBatchDate = $byBatch->pluck('tdate');

                // $transactions = DB::connection('visa')
                //     ->table('client_transactions as a')
                //     ->select(DB::raw('
                //         a.amount,a.type,a.service_id, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%M %d,%Y") as log_date'))
                //         ->where('group_id', $id)
                //         ->where('service_id', null)
                //         ->orderBy('tdate','DESC')    
                //         ->get();

                // $transDate = $transactions->pluck('tdate');

                // $merged = $byBatchDate->merge($transDate);

                // $result = $merged->all();
                // $tdates = array_unique($result);
                // rsort($tdates);
                // \Log::info($tdates);

                $row = 1;

                if($lang == 'EN'){
                    $sheet2->row($row, array(
                        'Service Date','Package','Status', 'Details', 'Charge', 'Group Total Balance', 
                        //'Transaction Amount', 'Type'
                    ));
                }
                else{
                    $sheet2->row($row, array(
                        '服务日期','查询编号', '状态', '服务明细', '收费', '总余额', 
                        //'Transaction Amount', 'Type'
                    ));
                }


                $cellNum = 'A'.$row.':F'.$row;
                //$m1 = 'A'.$row;
                // $sheet2->mergeCells($m1);
                //$m2 = 'B'.$row;
                // $sheet2->mergeCells($m2);
                //$sheet2->setBorder($m1, 'thin');
                //$sheet2->setBorder($m2, 'thin');

                $sheet2->cells($cellNum, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#6fc4dc');
                });
                $sheet2->setWidth(array(
                    'A'     =>  20,
                    'B'     =>  25,
                    'C'     =>  15,
                    'D'     =>  50,
                    'E'     =>  15,
                    'F'     =>  25,
                    'G'     =>  25,
                    'H'     =>  15,
                ));
                $row+=2; //add space after header
                $ttd = 0;
                $tempTotal = 0;
                $servTotal = 0;

                    $total_deposit = (
                        (
                            $this->GetGroupDeposit($id)
                            + $this->GetGroupPayment($id)
                        )
                    ) + 0;

                    $total_discount = $this->GetGroupDiscount($id);
                    $total_refund = $this->GetGroupRefund($id);
                    //$total_group_cost = $this->GetGroupCostComplete($id);

                    $total_group_cost = ClientService::where('group_id', $id)->where('active', 1)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
                    //\Log::info($total_group_cost);
                    $total_group_cost -= ClientService::where('group_id', $id)->where('active', 1)->where('detail', 'LIKE', '%9A%')->where('status','!=','complete')->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
                    // foreach($gserv as $gs){
                    //     if(strpos($serv['detail'], "9A") === false && $serv['status'] != 'complete'){
                    //                     $tcost = $serv['total_cost'];
                    //                 }
                    // }
                    //\Log::info($a9);

                    $total_balance = (
                        (
                            $total_deposit
                            +$total_discount
                        )
                        - (
                            $total_refund
                            +$total_group_cost
                        )
                    );

                $groupcost = $total_balance;
                $lastcost = 0;
                foreach($byBatch as $d){
                    // $service = ClientService::where('id',$d->id)->select('detail','service_date','service_id')->orderBy('id',"DESC")->first();
                    // $totalcost = ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$d->sdate)->where('group_id', $id)->where('active', 1)->where('status','complete')->value(DB::raw("SUM(cost + charge + tip)"));
                    //$totalcost = GroupMember::where('group_id', $id)->value(DB::raw("SUM(total_service_cost)"));
                    // if($totalcost == ''){
                    //     $totalcost = 0;
                    // }

                    //$datetime = new DateTime($service->service_date);
                    //$getdate = $datetime->format('M d,Y');
                    $y =  substr($d->date,0,4);
                    $m =  substr($d->date,4,2);
                    $day =  substr($d->date,6,2);
                    $nDate = $m.'/'.$day.'/'.$y;
                    if($lang == 'EN'){
                        $sheet2->row($row, array(
                            Carbon::parse($nDate)->format('F d,Y')
                        ));
                    }
                    else{
                        $sheet2->row($row, array(
                            $this->DateChinese(Carbon::parse($nDate)->format('F d,Y'))
                        ));
                    }

                    $cellNum = 'A'.$row.':F'.$row;
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                        $cells->setBackground('#ddd9c3');
                    });

                    $row++;


                    $query =  json_decode( $d->services, true );
                    $transactions =  json_decode( $d->transactions, true );
                        $client = [];
                        $flag = true;
                        foreach($query as $m){
                            // $ss =  ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$nDate)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->with('discount2')->get();
                            //\Log::info($ss);
                            //if(count($ss) > 0){
                                $client = $m['name'];
                                $services = $m['services'];

                                $sheet2->row($row, array(
                                    '',$client
                                ));
                                $cellNum = 'B'.$row;
                                $sheet2->cells($cellNum, function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                    //$cells->setBackground('#9ad1e0');
                                });
                                $row++;

                                $flag= true;
                                foreach($services as $serv){
                                    $tcost =  $serv['total_cost'];
                                    if($serv['status'] != 'complete'){
                                        $tcost = 0;
                                    }

                                    if(strpos($serv['detail'], "9A") === false && $serv['status'] != 'complete'){
                                        $tcost = $serv['total_cost'];
                                    }

                                    $sp = $serv['status'];
                                    $sp_cn = $serv['status'];
                                    if($sp=="CANCELLED"){
                                        $sp_cn = "取消";
                                        $tcost = 0;
                                    }
                                    $servTotal += $tcost;

                                    // $tempTotal = $groupcost - $lastcost;
                                    if($flag== false){
                                        $tempTotal = $groupcost - $lastcost;
                                    }
                                    else{
                                        $tempTotal = $groupcost + $lastcost;
                                    }
                                    $cnserv = $serv['detail_cn'];
                                    // $translated = Service::where('id',$serv->service_id)->first();
                                    // $cnserv =$serv->detail;
                                    // if($translated){
                                    //     $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                                    // }
                                    $sign = '-';
                                    $lastcost = $tcost;
                                    if($serv['detail'] == 'Discount'){
                                        $tcost = $serv['total_cost'];
                                        $sign = ' +';
                                        $lastcost = -1 * $tcost;
                                    }

                                    if($lang == 'EN'){
                                        $sheet2->row($row, array(
                                            '',
                                            $serv['tracking'],
                                            $sp,
                                            $serv['detail'],
                                            $sign.$tcost,
                                            $tempTotal

                                        ));
                                    }
                                    else{
                                        $sheet2->row($row, array(
                                            '',
                                            $serv['tracking'],
                                            $sp_cn,
                                            $cnserv,
                                            $sign.$tcost,
                                            $tempTotal

                                        ));
                                    }

                                    $groupcost = $tempTotal;
                                    $cellNum = 'A'.$row.':F'.$row;
                                    $sheet2->cells($cellNum, function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        //$cells->setFontWeight('bold');
                                        // $cells->setBackground('#d8e0e2');
                                    });
                                    $row++;

                                    // if($serv->discount_amount>0){
                                    //     $tempTotal = $groupcost + $lastcost;
                                    //     $sheet2->row($row, array(
                                    //         '',
                                    //         '',
                                    //         '',
                                    //         'Discount',
                                    //         ' +'.$serv->discount_amount,
                                    //         $tempTotal

                                    //     ));

                                    //     $lastcost = -1 *$serv->discount_amount;
                                    //     $groupcost = $tempTotal;

                                    // }

                                    $cellNum = 'A'.$row.':F'.$row;
                                    $sheet2->cells($cellNum, function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        //$cells->setFontWeight('bold');
                                        // $cells->setBackground('#d8e0e2');
                                    });

                                    $row++;
                                    $flag = true;
                                }
                            //}
                            $row++;
                            //$ctr2++;
                        }

                        // $row++;
                        // $row++;
                        //$flag = false;
                        foreach($transactions as $t){
                            //\Log::info($t->tdate.' '.$d);
                            //if($t->tdate == $d){
                                //if($t->type != 'Discount' || ($t->type == 'Discount' && $t->service_id == null)){
                                    $dtype = '';
                                    $sign = ' +';
                                    if($flag== false){
                                        $tempTotal = $groupcost - $lastcost;
                                    }
                                    else{
                                        $tempTotal = $groupcost + $lastcost;
                                    }

                                    if($t['detail'] == 'Deposit' || $t['detail'] == 'Discount' || $t['detail'] == 'Payment'){
                                        $lastcost = -1 *$t['total_cost'];
                                    }
                                    // if($t->detail == 'Discount' ){
                                    //     $lastcost = -1 *$t->amount;
                                    // }                            
                                    // if($t->type == 'Payment'){
                                    //     $dtype = '已付款';
                                    //     $lastcost = -1 *$t->amount;
                                    //     //$tempTotal = $groupcost - $lastcost;
                                    // }                            
                                    if($t['detail']  == 'Refund'){
                                        $lastcost = $t['total_cost'];
                                        $sign = '-';
                                        //$tempTotal = $groupcost + (-1*$lastcost);
                                    }

                                    
                                        if($lang == 'EN'){
                                            $sheet2->row($row, array(
                                                '',
                                                '',
                                                '',
                                                $t['detail'],
                                                $sign.$t['total_cost'],
                                                $tempTotal

                                            ));
                                        }
                                        else{
                                            $sheet2->row($row, array(
                                                '',
                                                '',
                                                '',
                                                $t['detail_cn'],
                                                $sign.$t['total_cost'],
                                                $tempTotal

                                            ));
                                        }
                                    //}
                                    $groupcost = $tempTotal;

                                    $flag = true;

                                    $cellNum = 'A'.$row.':F'.$row;
                                    $sheet2->cells($cellNum, function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                        // $cells->setBackground('#d8e0e2');
                                    });
                                    $row++;
                            //}
                        }

                    $row++;
                }
                $row++;


                    // $total_deposit = (
                    //     (
                    //         $this->GetGroupDeposit($id)
                    //         + $this->GetGroupPayment($id)
                    //     )
                    // ) + 0;

                    // $total_discount = $this->GetGroupDiscount($id);
                    // $total_refund = $this->GetGroupRefund($id);
                    $total_group_cost = $servTotal;

                    $total_balance = (
                        (
                            $total_deposit
                            +$total_discount
                        )
                        - (
                            $total_refund
                            +$total_group_cost
                        )
                    );

                    $textdeposit = ($lang == 'EN' ? 'Total Deposit : ' : '总已付款 : ');
                    $sheet2->cell('E'.$row, function($cells) use($total_deposit, $textdeposit) {
                        $cells->setValue($textdeposit.number_format($total_deposit, 2, '.', ','));
                    });

                    $cellNum = 'E'.$row.':F'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells)  {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textcost = ($lang == 'EN' ? 'Total Cost : ' : '总花费 : ');
                    $sheet2->cell('E'.$row, function($cells) use($total_group_cost, $textcost) {
                        $cells->setValue($textcost.'-'.number_format($total_group_cost, 2, '.', ','));
                    });
                    $cellNum = 'E'.$row.':F'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textdiscount = ($lang == 'EN' ? 'Total Discount : ' : '总折扣 : ');
                    $sheet2->cell('E'.$row, function($cells) use($total_discount, $textdiscount) {
                        $cells->setValue($textdiscount.number_format($total_discount, 2, '.', ','));
                    });
                    $cellNum = 'E'.$row.':F'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textrefund = ($lang == 'EN' ? 'Total Refund : ' : '总退款 : ');
                    $sheet2->cell('E'.$row, function($cells) use($total_refund, $textrefund) {
                        $cells->setValue($textrefund.number_format($total_refund, 2, '.', ','));
                    });
                    $cellNum = 'E'.$row.':F'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $textbalance = ($lang == 'EN' ? 'Total Balance : ' : '总余额 : ');
                    $sheet2->cell('E'.$row, function($cells) use($total_balance, $textbalance) {
                        $cells->setValue($textbalance.number_format($total_balance, 2, '.', ','));
                    });
                    $cellNum = 'E'.$row.':F'.$row;
                    $sheet2->mergeCells($cellNum);
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setValignment('center');
                        $cells->setBackground('#a6ced9');
                        $cells->setFontWeight('bold');
                    });

                    $row++;
                    $row++;

                    $row++;

                    //$transactions = ClientTransactions::where('group_id', $id)->get();
                    // $transactions = DB::connection('visa')
                    // ->table('client_transactions as a')
                    // ->select(DB::raw('
                    //     a.amount,a.type, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%M %d,%Y") as log_date'))
                    //     ->where('group_id', $id)
                    //     ->orderBy('tdate','DESC')    
                    //     ->get();
                        
                    // $transhistory = ($lang == 'EN' ? 'Transactions History' : '交易记录');
                    // $sheet2->cell('A'.$row, function($cells) use($transhistory) {
                    //     $cells->setValue($transhistory);
                    //     $cells->setValignment('center');
                    //     //$cell->setBackground('#a6ced9');
                    //     $cells->setFontWeight('bold');
                    // });

                    // $row++;

                    // if($lang == 'EN'){
                    //     $sheet2->row($row, array(
                    //         'Amount', 'Date' , 'Type'
                    //     ));
                    // }
                    // else{
                    //     $sheet2->row($row, array(
                    //         '共计', '日期' , '类型'
                    //     ));
                    // }

                    // $cellNum = 'A'.$row.':C'.$row;
                    // $sheet2->cells($cellNum, function($cells) {
                    //     $cells->setAlignment('center');
                    //     $cells->setValignment('center');
                    //     $cells->setFontWeight('bold');
                    //     $cells->setBackground('#a6ced9');
                    // });
                    // $row++;
                    //$ttd = 0;
                    // foreach($transactions as $d){
                    //     $datetime = new DateTime($d->log_date);
                    //     $getdate = $datetime->format('M d,Y');
                    //     //$gettime = $datetime->format('h:i A');
                    //     //预存款 // Deposit
                    //     //折扣 // Discount
                    //     //已付款 // Payment
                    //     //退款 // Refund
                    //     if($lang == 'EN'){
                    //         $sheet2->row($row, array(
                    //           number_format($d->amount, 2, '.', ','), $d->log_date, $d->type
                    //         ));
                    //     }
                    //     else{
                    //         $dtype = '';
                    //         if($d->type == 'Deposit'){
                    //             $dtype = '预存款';
                    //         }
                    //         if($d->type == 'Discount'){
                    //             $dtype = '折扣';
                    //         }                            
                    //         if($d->type == 'Payment'){
                    //             $dtype = '已付款';
                    //         }                            
                    //         if($d->type == 'Refund'){
                    //             $dtype = '退款';
                    //         }

                    //         $sheet2->row($row, array(
                    //           number_format($d->amount, 2, '.', ','), $this->DateChinese($getdate), $dtype
                    //         ));
                    //     }
                        
                    //     $cellNum = 'A'.$row.':C'.$row;
                    //     $sheet2->cells($cellNum, function($cells) {
                    //         $cells->setAlignment('center');
                    //         $cells->setValignment('center');
                    //         //$cell->setFontWeight('bold');
                    //     });
                    //     //$ttd+=$d->amount;
                    //     $row++;
                    // }
                    $row++;

            });
            // END By Batch sheet
        }
        // end by batch type

        })->export('xls'); // Excel
    }

    public function getGroupSummaryChinese($id, $type) {
        $this->getGroupSummary($id, $type, 'CN');
        // header('Set-Cookie: fileDownload=true; path=/');
        // header('Cache-Control: max-age=60, must-revalidate');
        // header("Content-type: pplication/vnd.ms-excel");
        // $getGroupMembers = DB::connection('visa')
        //     ->table('group_members as a')
        //     ->select('a.*')
        //     ->where('a.group_id', $id)
        //     ->get();

        // $getGroupName = DB::connection('visa')
        //     ->table('groups as a')
        //     ->select('a.*')
        //     ->where('a.id', $id)
        //     ->first();

        // $filename = 'CN--'.$getGroupName->name .'--'. Carbon::today()->format('n/j/Y');
        // Excel::create($filename, function($excel) use ($filename, $getGroupMembers, $id, $type) {

        //     // Set the title
        //     $excel->setTitle($filename);

        //     // Chain the setters
        //     $excel->setCreator('WYC')
        //           ->setCompany('WYC');

        //     // Call them separately
        //     $excel->setDescription('Group Summary');

        //     //First Sheet -> Group Member's Cash Flow
        //     if($type=='bymembers'){
        //     $excel->sheet('总结报告', function($sheet1) use ($getGroupMembers, $id) {
        //         $row = 1;
        //         $tempTotal = 0;
        //         $sheet1->row($row, array(
        //             // 'Date', 'Service', 'Note' , 'Status' , 'Cost', 'Charge', 'Subtotal', 'Member Balance', 'Group Total Balance' ,' '
        //             '建立日期', '服务', '备注' , '状态' , '收费', '小计', '会员 平衡', '总余额' ,' '
        //         ));
        //         $cellNum = 'A'.$row.':J'.$row;
        //         $sheet1->cells($cellNum, function($cells) {
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //             $cells->setBackground('#6fc4dc');
        //         });
        //         $sheet1->setWidth(array(
        //             'A'     =>  25,
        //             'B'     =>  30,
        //             'C'     =>  15,
        //             'D'     =>  15,
        //             'E'     =>  15,
        //             'F'     =>  15,
        //             'G'     =>  15,
        //             'H'     =>  15,
        //             'I'     =>  15,
        //             'J'     =>  15,
        //         ));
        //         $row++;

        //         foreach ($getGroupMembers as $value) {
        //             $getName = User::where('id',$value->client_id)->select('first_name','last_name')->first()->full_name;

        //             $row++;
        //             $sheet1->row($row, array(
        //                 ucfirst($getName)
        //             ));
        //             $cellNum = 'A'.$row.':J'.$row;
        //             $sheet1->mergeCells($cellNum);
        //             $sheet1->cells($cellNum, function($cells) {
        //                 $cells->setAlignment('center');
        //                 $cells->setValignment('center');
        //                 $cells->setBackground('#a6ced9');
        //                 $cells->setBorder('solid', 'solid', 'solid', 'solid');
        //                 $cells->setFontWeight('bold');
        //             });

        //             $getServices = DB::connection('visa')
        //             ->table('client_services as a')
        //             ->select('a.*')
        //             ->where('a.client_id', $value->client_id)
        //             ->where('a.group_id', $id)
        //             ->get();
        //             $bal = 0;
        //             foreach ($getServices as $s) {
        //                 $datetime = new DateTime($s->service_date);
        //                 $getdate = $datetime->format('M d,Y');
        //                 $gettime = $datetime->format('h:i A');

        //                 //note
        //                 $the_string = $s->remarks;
        //                 $the_word  = "<small>";
        //                 $the_string = str_replace('</small>', '', $the_string);
        //                 $count_occur = substr_count($the_string,$the_word);
        //                 for($i = 0; $i<$count_occur; $i++){
        //                     if (strpos($the_string, $the_word) !== false) {
        //                         $the_string = str_replace('</br>', ' --> ', $the_string);
        //                         $postr = strpos($the_string, $the_word);
        //                         $str1 = substr($the_string,0,$postr-8);
        //                         $str2 = substr($the_string,$postr+7);
        //                         $the_string = $str1.$str2;
        //                     }
        //                 }

        //                 //Cost
        //                 // $amt = $s->cost + $s->tip;
        //                 // if($s->active == 0){
        //                 //     $amt = 0;
        //                 // }

        //                 //Service Charge
        //                 $chrg = $s->charge + $s->cost + $s->tip;
        //                 if($s->active == 0){
        //                     $chrg = 0;
        //                 }

        //                 //Subtotal
        //                 $sub = $chrg;

        //                 //Per Person Balance
        //                 if($s->active == 0){
        //                     $sub = 0;
        //                 }
        //                 $bal += $sub;

        //                 //I
        //                 $tempTotal +=$sub;

        //                 //Cancelled
        //                 $sp = "";
        //                 if($s->active==0){
        //                     $sp = "取消";
        //                 }
        //                 $row++;
        //                 $translated = Service::where('id',$s->service_id)->first();
        //                 $cnserv =$s->detail;
        //                 if($translated){
        //                     $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
        //                 }
        //                  $sheet1->row($row, array(
        //                      $this->DateChinese($getdate), $cnserv, ucwords(strtolower($the_string)) , $this->statusChinese($s->status) , $chrg, $sub, $bal , $tempTotal  ,$sp
        //                  ));

        //             }

        //             $row++;
        //             $row++;
        //         }

        //         $row++;
        //         $summaryrow = $row;
        //         // $cost = DB::connection('visa')
        //         //     ->table('client_services')
        //         //     ->select(DB::raw('sum(cost+charge+tip) as total_cost'))
        //         //     ->where('group_id', '=', $id)
        //         //     ->where('active', '=', 1)
        //         //     ->first();

        //         $total_deposit = (
        //             (
        //                 $this->GetGroupDeposit($id)
        //                 + $this->GetGroupPayment($id)
        //             )
        //         ) + 0;

        //         $total_discount = $this->GetGroupDiscount($id);
        //         $total_refund = $this->GetGroupRefund($id);
        //         $total_group_cost = $this->GetGroupCost($id);

        //         $total_balance = (
        //             (
        //                 $total_deposit
        //                 +$total_discount
        //             )
        //             - (
        //                 $total_refund
        //                 +$total_group_cost
        //             )
        //         );
        //         $sheet1->cell('I'.$row, function($cell) use($total_deposit) {
        //             $cell->setValue('总预存款 : '.number_format($total_deposit, 2, '.', ','));
        //         });

        //         $cellNum = 'I'.$row.':J'.$row;
        //         $sheet1->mergeCells($cellNum);
        //         $sheet1->cells($cellNum, function($cells)  {
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //         });

        //         $row++;
        //         $sheet1->cell('I'.$row, function($cell) use($total_group_cost) {
        //             $cell->setValue('总花费 : '.number_format($total_group_cost, 2, '.', ','));
        //         });
        //         $cellNum = 'I'.$row.':J'.$row;
        //         $sheet1->mergeCells($cellNum);
        //         $sheet1->cells($cellNum, function($cells) {
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //         });

        //         $row++;
        //         $sheet1->cell('I'.$row, function($cell) use($total_discount) {
        //             $cell->setValue('总折扣 : '.number_format($total_discount, 2, '.', ','));
        //         });
        //         $cellNum = 'I'.$row.':J'.$row;
        //         $sheet1->mergeCells($cellNum);
        //         $sheet1->cells($cellNum, function($cells) {
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //         });

        //         $row++;
        //         $sheet1->cell('I'.$row, function($cell) use($total_refund) {
        //             $cell->setValue('总退款 : '.number_format($total_refund, 2, '.', ','));
        //         });
        //         $cellNum = 'I'.$row.':J'.$row;
        //         $sheet1->mergeCells($cellNum);
        //         $sheet1->cells($cellNum, function($cells) {
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //         });

        //         $row++;
        //         $sheet1->cell('I'.$row, function($cell) use($total_balance) {
        //             $cell->setValue('总余额 : '.number_format($total_balance, 2, '.', ','));
        //         });
        //         $cellNum = 'I'.$row.':J'.$row;
        //         $sheet1->mergeCells($cellNum);
        //         $sheet1->cells($cellNum, function($cells) {
        //             $cells->setValignment('center');
        //             $cells->setBackground('#a6ced9');
        //             $cells->setFontWeight('bold');
        //         });

        //         $row++;

        //     }); // First Sheet


        //     // 2nd sheet
        //     $excel->sheet('预存款', function($sheet2) use($id) {
        //         $gdepo = ClientTransactions::where('group_id', $id)->where('type', 'Deposit')->get();

        //         $row = 1;
        //         $sheet2->row($row, array(
        //             '共计', '日期'
        //         ));
        //         $cellNum = 'A'.$row.':B'.$row;
        //         $sheet2->cells($cellNum, function($cells) {
        //             $cells->setAlignment('center');
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //             $cells->setBackground('#6fc4dc');
        //         });
        //         $row++;
        //         $ttd = 0;
        //         foreach($gdepo as $d){
        //             $datetime = new DateTime($d->log_date);
        //             $getdate = $datetime->format('M d,Y');
        //             $gettime = $datetime->format('h:i A');
        //             $sheet2->row($row, array(
        //                 number_format($d->amount, 2, '.', ','), $this->DateChinese($getdate).' '.$gettime
        //             ));
        //             $cellNum = 'A'.$row.':B'.$row;
        //             $sheet2->cells($cellNum, function($cells) {
        //                 $cells->setAlignment('center');
        //                 $cells->setValignment('center');
        //                 $cells->setFontWeight('bold');
        //             });
        //             $ttd+=$d->amount;
        //             $row++;
        //         }
        //         $row++;
        //         $sheet2->row($row, array(
        //             '总预存款 : '.number_format($ttd, 2, '.', ',')
        //         ));
        //         $cellNum = 'A'.$row.':B'.$row;
        //         $sheet2->cells($cellNum, function($cells) {
        //             $cells->setAlignment('center');
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //         });
        //     }); // 2nd sheet

        //     // 3rd Sheet
        //     $excel->sheet('已 付款', function($sheet2) use($id) {
        //         $gdepo = ClientTransactions::where('group_id', $id)->where('type', 'Payment')->get();

        //         $row = 1;
        //         $sheet2->row($row, array(
        //             '共计', '日期'
        //         ));
        //         $cellNum = 'A'.$row.':B'.$row;
        //         $sheet2->cells($cellNum, function($cells) {
        //             $cells->setAlignment('center');
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //             $cells->setBackground('#6fc4dc');
        //         });
        //         $row++;
        //         $ttd = 0;
        //         foreach($gdepo as $d){
        //             $datetime = new DateTime($d->log_date);
        //             $getdate = $datetime->format('M d,Y');
        //             $gettime = $datetime->format('h:i A');
        //             $sheet2->row($row, array(
        //                 number_format($d->amount, 2, '.', ','), $this->DateChinese($getdate).' '.$gettime
        //             ));
        //             $cellNum = 'A'.$row.':B'.$row;
        //             $sheet2->cells($cellNum, function($cells) {
        //                 $cells->setAlignment('center');
        //                 $cells->setValignment('center');
        //                 $cells->setFontWeight('bold');
        //             });
        //             $ttd+=$d->amount;
        //             $row++;
        //         }
        //         $row++;
        //         $sheet2->row($row, array(
        //             '总付款 : '.number_format($ttd, 2, '.', ',')
        //         ));
        //         $cellNum = 'A'.$row.':B'.$row;
        //         $sheet2->cells($cellNum, function($cells) {
        //             $cells->setAlignment('center');
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //         });
        //     }); // 3rd Sheet

        //     // 4th Sheet
        //     $excel->sheet('退款', function($sheet2) use($id) {
        //         $gdepo = ClientTransactions::where('group_id', $id)->where('type', 'Refund')->get();

        //         $row = 1;
        //         $sheet2->row($row, array(
        //             '共计', '日期'
        //         ));
        //         $cellNum = 'A'.$row.':B'.$row;
        //         $sheet2->cells($cellNum, function($cells) {
        //             $cells->setAlignment('center');
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //             $cells->setBackground('#6fc4dc');
        //         });
        //         $row++;
        //         $ttd = 0;
        //         foreach($gdepo as $d){
        //             $datetime = new DateTime($d->log_date);
        //             $getdate = $datetime->format('M d,Y');
        //             $gettime = $datetime->format('h:i A');
        //             $sheet2->row($row, array(
        //                 number_format($d->amount, 2, '.', ','), $this->DateChinese($getdate).' '.$gettime
        //             ));
        //             $cellNum = 'A'.$row.':B'.$row;
        //             $sheet2->cells($cellNum, function($cells) {
        //                 $cells->setAlignment('center');
        //                 $cells->setValignment('center');
        //                 $cells->setFontWeight('bold');
        //             });
        //             $ttd+=$d->amount;
        //             $row++;
        //         }
        //         $row++;
        //         $sheet2->row($row, array(
        //             '总退款 : '.number_format($ttd, 2, '.', ',')
        //         ));
        //         $cellNum = 'A'.$row.':B'.$row;
        //         $sheet2->cells($cellNum, function($cells) {
        //             $cells->setAlignment('center');
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //         });
        //     }); // 4th Sheet
        // }


        // if($type=='byservices'){
        //     // By Service
        //     $excel->sheet('按服务查看', function($sheet2) use($id) {
        //         $byService = ClientService::where('group_id',$id)->where('active',1)
        //                 ->with('discount')->groupBy('service_id')->orderBy('id',"DESC")
        //                 ->get();
        //         $row = 1;
        //         $sheet2->row($row, array(
        //             '服务明细','', '最近的服务日期','','总服务费',''
        //         ));
        //         $cellNum = 'A'.$row.':F'.$row;
        //         $m1 = 'A'.$row.':B'.$row;
        //         $sheet2->mergeCells($m1);
        //         $m2 = 'C'.$row.':D'.$row;
        //         $sheet2->mergeCells($m2);
        //         $m3 = 'E'.$row.':F'.$row;
        //         $sheet2->mergeCells($m3);
        //         $sheet2->setBorder($m1, 'thin');
        //         $sheet2->setBorder($m2, 'thin');
        //         $sheet2->setBorder($m3, 'thin');
        //         $sheet2->cells($cellNum, function($cells) {
        //             $cells->setAlignment('center');
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //             $cells->setBackground('#9ad1e0');
        //         });
        //         $sheet2->setWidth(array(
        //             'A'     =>  30,
        //             'B'     =>  20,
        //             'C'     =>  15,
        //             'D'     =>  15,
        //             'E'     =>  15,
        //             'F'     =>  15,
        //         ));
        //         $row+=2; //add space after header
        //         $ttd = 0;
        //         $tempTotal = 0;
        //         foreach($byService as $d){
        //             $service = ClientService::where('id',$d->id)->select('detail','service_date','service_id')->orderBy('id',"DESC")->first();
        //             $totalcost = ClientService::where('service_id',$d->service_id)->where('group_id', $id)->where('active', 1)->value(DB::raw("SUM(cost + charge + tip)"));
        //             $datetime = new DateTime($service->service_date);
        //             $getdate = $datetime->format('M d,Y');

        //             $translated = Service::where('id',$service->service_id)->first();
        //             $cnserv =$service->detail;
        //             if($translated){
        //                 $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
        //             }

        //             $sheet2->row($row, array(
        //                 $cnserv,'', $this->DateChinese($getdate),'' , $totalcost,''
        //             ));
        //             $m1 = 'A'.$row.':B'.$row;
        //             $sheet2->mergeCells($m1);
        //             $m2 = 'C'.$row.':D'.$row;
        //             $sheet2->mergeCells($m2);
        //             $m3 = 'E'.$row.':F'.$row;
        //             $sheet2->mergeCells($m3);
        //             $sheet2->setBorder($m1, 'thin');
        //             $sheet2->setBorder($m2, 'thin');
        //             $sheet2->setBorder($m3, 'thin');
        //             $cellNum = 'A'.$row.':F'.$row;
        //             $sheet2->cells($cellNum, function($cells) {
        //                 $cells->setAlignment('center');
        //                 $cells->setValignment('center');
        //                 $cells->setFontWeight('bold');
        //                 $cells->setBackground('#b3bbbd');
        //             });

        //             $row++;
        //             $sheet2->row($row, array(
        //                 // 'Package', 'Date Recorded','Details', 'Status', 'Cost', 'Service Fee', 'Additional Fee', 'Discount'
        //                 '查询编号', '状态', '收费', '折扣', '服务 平衡', '总余额'
        //             ));
        //             $cellNum = 'A'.$row.':F'.$row;
        //             $sheet2->cells($cellNum, function($cells) {
        //                 $cells->setAlignment('center');
        //                 $cells->setValignment('center');
        //                 $cells->setFontWeight('bold');
        //                 $cells->setBackground('#d8e0e2');
        //             });
        //             $row++;
        //             $query = ClientService::where('service_id',$service->service_id)->where('group_id',$id)->where('active',1)->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('client_id')->get();
        //                 $ctr2 = 0;
        //                 $servTotal = 0;
        //                 $client = [];
        //                 foreach($query as $m){
        //                     $ss =  ClientService::where('service_id',$service->service_id)->where('group_id',$id)->where('active',1)->with('discount2')->orderBy('service_date','DESC')->where('client_id',$m->client_id)->get();
        //                     // return $ss;
        //                     if(count($ss)){
        //                         // $client = User::where('id',$m->client_id)->select('first_name','last_name')->first()->makeHidden(['permissions','access_control','document_receive','binded','unread_notif','group_binded']);
        //                         $client = User::where('id',$m->client_id)->select('first_name','last_name')->first()->full_name;
        //                         //$totalcost = ClientService::where('service_id',$id)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip)"));
        //                         $services = $ss;

        //                         $sheet2->row($row, array(
        //                             $client
        //                         ));
        //                         $cellNum = 'A'.$row;
        //                         $sheet2->cells($cellNum, function($cells) {
        //                             $cells->setAlignment('center');
        //                             $cells->setValignment('center');
        //                             $cells->setFontWeight('bold');
        //                             $cells->setBackground('#9ad1e0');
        //                         });
        //                         $row++;

        //                         foreach($services as $serv){
        //                             $tcost = $serv->cost + $serv->tip + $serv->charge;
        //                             $servTotal += $tcost;
        //                             $tempTotal += $tcost;
        //                             $sheet2->row($row, array(
        //                                 $serv->tracking,
        //                                 // $serv->service_date,
        //                                 // $serv->detail,
        //                                 $this->statusChinese($serv->status),
        //                                 $tcost,
        //                                 // $serv->charge,
        //                                 // $serv->tip,
        //                                 $serv->amount,
        //                                 $servTotal,
        //                                 $tempTotal

        //                             ));
        //                             $cellNum = 'A'.$row.':F'.$row;
        //                             $sheet2->cells($cellNum, function($cells) {
        //                                 $cells->setAlignment('center');
        //                                 $cells->setValignment('center');
        //                                 $cells->setFontWeight('bold');
        //                                 // $cells->setBackground('#d8e0e2');
        //                             });
        //                             $row++;
        //                         }
        //                     }
        //                     $row++;
        //                     $ctr2++;
        //                 }

        //             $row++;
        //         }
        //         $row++;

        //     }); // By Service sheet
        // }

        // if($type=='bybatch'){
        //     // By Batch
        //     $excel->sheet('按批次查看', function($sheet2) use($id) {
        //         $byBatch = DB::connection('visa')
        //                 ->table('client_services as a')
        //                 ->select(DB::raw('id,detail,service_date, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y") as sdate'))
        //                 ->where('active',1)->where('group_id',$id)
        //                 ->orderBy('id','DESC')
        //                 ->groupBy('sdate')
        //                 ->get();
        //         $row = 1;
        //         $sheet2->row($row, array(
        //             '服务日期','总服务费'
        //         ));
        //         $cellNum = 'A'.$row.':B'.$row;
        //         $m1 = 'A'.$row;
        //         // $sheet2->mergeCells($m1);
        //         $m2 = 'B'.$row;
        //         // $sheet2->mergeCells($m2);
        //         $sheet2->setBorder($m1, 'thin');
        //         $sheet2->setBorder($m2, 'thin');

        //         $sheet2->cells($cellNum, function($cells) {
        //             $cells->setAlignment('center');
        //             $cells->setValignment('center');
        //             $cells->setFontWeight('bold');
        //             $cells->setBackground('#9ad1e0');
        //         });
        //         $sheet2->setWidth(array(
        //             'A'     =>  30,
        //             'B'     =>  20,
        //             'C'     =>  50,
        //             'D'     =>  15,
        //             'E'     =>  15,
        //             'F'     =>  15,
        //             'G'     =>  15,
        //         ));
        //         $row+=2; //add space after header
        //         $ttd = 0;
        //         $tempTotal = 0;
        //         foreach($byBatch as $d){
        //             $service = ClientService::where('id',$d->id)->select('detail','service_date','service_id')->orderBy('id',"DESC")->first();
        //             $totalcost = ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$d->sdate)->where('group_id', $id)->where('active', 1)->value(DB::raw("SUM(cost + charge + tip)"));
        //             $datetime = new DateTime($service->service_date);
        //             $getdate = $datetime->format('M d,Y');
        //             $sheet2->row($row, array(
        //                 $this->DateChinese(Carbon::parse($d->sdate)->format('M d,Y')), $totalcost
        //             ));
        //             $cellNum = 'A'.$row.':B'.$row;
        //             $m1 = 'A'.$row;
        //             // $sheet2->mergeCells($m1);
        //             $m2 = 'B'.$row;
        //             // $sheet2->mergeCells($m2);
        //             $sheet2->setBorder($m1, 'thin');
        //             $sheet2->setBorder($m2, 'thin');
        //             $cellNum = 'A'.$row.':B'.$row;
        //             $sheet2->cells($cellNum, function($cells) {
        //                 $cells->setAlignment('center');
        //                 $cells->setValignment('center');
        //                 $cells->setFontWeight('bold');
        //                 $cells->setBackground('#b3bbbd');
        //             });

        //             $row++;
        //             $sheet2->row($row, array(
        //                 // 'Package', 'Date Recorded','Details', 'Status', 'Cost', 'Service Fee', 'Additional Fee', 'Discount'
        //                 '查询编号', '状态', '服务明细', '收费', '折扣', '总余额'
        //             ));
        //             $cellNum = 'A'.$row.':G'.$row;
        //             $sheet2->cells($cellNum, function($cells) {
        //                 $cells->setAlignment('center');
        //                 $cells->setValignment('center');
        //                 $cells->setFontWeight('bold');
        //                 $cells->setBackground('#d8e0e2');
        //             });
        //             $row++;
        //             $query = ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$d->sdate)->where('group_id', $id)->where('active', 1)->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('client_id')->get();
        //                 $ctr2 = 0;
        //                 //$servTotal = 0;
        //                 $client = [];
        //                 foreach($query as $m){
        //                     $ss =  ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$d->sdate)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->with('discount2')->get();
        //                     // return $ss;
        //                     if(count($ss)){
        //                         $client = User::where('id',$m->client_id)->select('first_name','last_name')->first()->full_name;
        //                         //$totalcost = ClientService::where('service_id',$id)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip)"));
        //                         $services = $ss;

        //                         $sheet2->row($row, array(
        //                             $client
        //                         ));
        //                         $cellNum = 'A'.$row;
        //                         $sheet2->cells($cellNum, function($cells) {
        //                             $cells->setAlignment('center');
        //                             $cells->setValignment('center');
        //                             $cells->setFontWeight('bold');
        //                             $cells->setBackground('#9ad1e0');
        //                         });
        //                         $row++;

        //                         foreach($services as $serv){
        //                             $translated = Service::where('id',$serv->service_id)->first();
        //                             $cnserv =$serv->detail;
        //                             if($translated){
        //                                 $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
        //                             }
        //                             $tcost = $serv->cost + $serv->tip + $serv->charge;
        //                             //$servTotal += $tcost;
        //                             $tempTotal += $tcost;
        //                             $sheet2->row($row, array(
        //                                 $serv->tracking,
        //                                 // $serv->service_date,
        //                                 $this->statusChinese($serv->status),
        //                                 $cnserv,
        //                                 $tcost,
        //                                 // $serv->charge,
        //                                 // $serv->tip,
        //                                 $serv->amount,
        //                                 //$servTotal,
        //                                 $tempTotal

        //                             ));
        //                             $cellNum = 'A'.$row.':G'.$row;
        //                             $sheet2->cells($cellNum, function($cells) {
        //                                 $cells->setAlignment('center');
        //                                 $cells->setValignment('center');
        //                                 $cells->setFontWeight('bold');
        //                                 $cells->setBackground('#d8e0e2');
        //                             });
        //                             $row++;
        //                         }
        //                     }
        //                     $row++;
        //                     $ctr2++;
        //                 }

        //             $row++;
        //         }
        //         $row++;

        //     });
        //     // END By Service sheet
        // }

        // })->export('xls'); // Excel

    }


    // view pdf before downloading
    public function getGroupSummaryPDF($id) {
        // return PDF::loadFile('http://cpanel.topwyc.test/visa/report/view-group-summary-pdf/115')->inline('your-mess.pdf');
        $pdf = PDF::loadView('cpanel.visa.groups.summarypdf');
        //$pdf->setOption('javascript-delay', 3000);
        return $pdf->download('invoice.pdf');
    }

    //pass details to blade to be downloaded
    public function viewGroupSummaryPDF($id) {
        $getGroupMembers = DB::connection('visa')
            ->table('group_members as a')
            ->select('a.*')
            ->where('a.group_id', $id)
            ->get();

        $getGroupName = DB::connection('visa')
            ->table('groups as a')
            ->select('a.*')
            ->where('a.id', $id)
            ->first();

        $group = [];
        $html = '<br><br>';
        $group['group'] = $getGroupName->name;
        $ctr = 0;
                foreach ($getGroupMembers as $value) {
                    $getName = DB::table('users as a')
                    ->select('a.*')
                    ->where('a.id', $value->client_id)
                    ->first();

                    $html .=  '<table><tr style="page-break-inside: avoid"><td><h4 style="margin-bottom:-5px;margin-top: 15px; font-size: 18px; ">'.ucfirst($getName->first_name.' '.$getName->last_name).'</h4></td></tr></table>';


                    $getServices = DB::connection('visa')
                                    ->table('client_services as a')
                                    ->select('a.*')
                                    ->where('a.client_id', $value->client_id)
                                    ->where('a.group_id', $id)
                                    ->get();
                    $bal = 0;
                    $tempTotal = 0;

                    $current = 0;
                    foreach ($getServices as $s) {
                        $datetime = new DateTime($s->service_date);
                        $getdate = $datetime->format('M d,Y');
                        $gettime = $datetime->format('h:i A');

                        //Cost
                        $amt = ($s->cost + $s->tip);
                        if($s->active == 0){
                            $amt = 0;
                        }

                        // //Service Charge
                        $chrg = $s->charge;
                        if($s->active == 0){
                            $chrg = 0;
                        }

                        // //Subtotal
                        $sub = $chrg + $amt;

                        // //Per Person Balance
                        if($s->active == 0){
                            $sub = 0;
                        }
                        $bal += $sub;

                        // //I
                        $tempTotal +=$sub;

                        // //Cancelled
                        $sp = "";
                        if($s->active==0){
                            $sp = "CANCELLED";
                        }

                        if($current != $s->client_id){
                            $html .= '<table cellspacing="2" cellpadding="10" style="text-align: center; width: 100%;"><tr style="page-break-inside: avoid">';
                            $html .= '<td width="15%">Date</td>';
                            $html .= '<td width="35%">Service Detail</td>';
                            $html .= '<td width="6%">Status</td>';
                            $html .= '<td width="7%">Cost</td>';
                            $html .= '<td width="7%">Charge</td>';
                            $html .= '<td width="10%">Subtotal</td>';
                            $html .= '<td width="10%">Member Balance</td>';
                            $html .= '<td width="10%">Group Balance</td></tr>';
                            $current = $s->client_id;
                        }


                        $html .= '<tr style="page-break-inside: avoid">';
                        $html .= '<td>'.$getdate.'</td>';
                        $html .= '<td style="text-align: left;">'.$s->detail.'</td>';
                        $html .= '<td>'.ucfirst($s->status).'</td>';
                        $html .= '<td>'.($amt).'</td>';
                        $html .= '<td>'.($chrg).'</td>';
                        $html .= '<td>'.($sub).'</td>';
                        $html .= '<td>'.($bal).'</td>';
                        $html .= '<td>'.($tempTotal).'</td>';
                        $html .= '</tr>';
                        //note
                        // $the_string = $s->remarks;
                        // $the_word  = "<small>";
                        // $the_string = str_replace('</small>', '', $the_string);
                        // $count_occur = substr_count($the_string,$the_word);
                        // for($i = 0; $i<$count_occur; $i++){
                        //     if (strpos($the_string, $the_word) !== false) {
                        //         $the_string = str_replace('</br>', ' --> ', $the_string);
                        //         $postr = strpos($the_string, $the_word);
                        //         $str1 = substr($the_string,0,$postr-8);
                        //         $str2 = substr($the_string,$postr+7);
                        //         $the_string = $str1.$str2;
                        //     }
                        // }


                        // $row++;
                        //  $sheet1->row($row, array(
                        //      $getdate, $s->detail, ucwords(strtolower($the_string)) , ucfirst($s->status) , $amt, $chrg, $sub, $bal , $tempTotal  ,$sp
                        //  ));

                    }
                    $html .= '</table>';
                }

        // session()->flash('data', $group);

         $pdf = PDF::loadHTML($html)->setPaper('a4')->setOrientation('portrait')->setOption('header-html', 'http://www.wyc-group.com/header.html');
         $pdf->save('group_summary.pdf',true);
        return \Response::download('group_summary.pdf');

    }


    private function statusChinese($status){
        $s = strtolower(trim($status," "));
        $stat = '';
        if($s == 'complete'){
            $stat = '已完成';
        }
        if($s == 'on process'){
            $stat = '办理中';
        }
        if($s == 'pending'){
            $stat=  '待办';
        }
        return $stat;
    }

    private function DateChinese($date){
        $d = explode(" ",strtolower($date));
        switch($d[0]){
            case "jan":
                return "一月"." ".$d[1];
                break;
            case "feb":
                return "二月"." ".$d[1];
                break;
            case "mar":
                return "三月"." ".$d[1];
                break;
            case "apr":
                return "四月"." ".$d[1];
                break;
            case "may":
                return "五月"." ".$d[1];
                break;
            case "jun":
                return "六月"." ".$d[1];
                break;
            case "jul":
                return "七月"." ".$d[1];
                break;
            case "aug":
                return "八月"." ".$d[1];
                break;
            case "sep":
                return "九月"." ".$d[1];
                break;
            case "oct":
                return "十月"." ".$d[1];
                break;
            case "nov":
                return "十一月"." ".$d[1];
                break;
            case "dec":
                return "十二月"." ".$d[1];
                break;
            default:
                return $date;
        }
      // "feb": "二月",
      // "march": "三月",
      // "april": "四月",
      // "may": "五月",
      // "jun": "六月",
      // "jul" : "七月",
      // "aug": "八月",
      // "sep": "九月",
      // "oct": "十月",
      // "nov": "十一月",
      // "dec": "十二月"
    }

    private function GetGroupDeposit($group_id){
        $total_deposit = ClientTransactions::where('group_id', $group_id)->where('type', 'Deposit')->sum('amount');
        return $total_deposit;
    }

    public function GetGroupPayment($group_id){
        $total_payment = ClientTransactions::where('group_id', $group_id)->where('type', 'Payment')->sum('amount');
        return $total_payment;
    }

    public function GetGroupDiscount($group_id){
        $total_discount = ClientTransactions::where('group_id', $group_id)->where('type', 'Discount')->sum('amount');
        return $total_discount;
    }

    public function GetGroupRefund($group_id){
        $total_refund = ClientTransactions::where('group_id', $group_id)->where('type', 'Refund')->sum('amount');
        return $total_refund;
    }

    public function GetGroupCost($group_id){
        $group_cost = DB::connection('visa')
            ->table('client_services')
            ->select(DB::raw('sum(cost+charge+tip + com_client + com_agent) as total_cost'))
            ->where('group_id', '=', $group_id)
            ->where('active', '=', 1)
            ->first();

        return $group_cost->total_cost;
    }

    public function GetGroupCostComplete($group_id){
        $group_cost = DB::connection('visa')
            ->table('client_services')
            ->select(DB::raw('sum(cost+charge+tip  + com_client + com_agent) as total_cost'))
            ->where('group_id', '=', $group_id)
            ->where('active', '=', 1)
            ->where('status', '=', 'complete')
            ->first();

        return $group_cost->total_cost;
    }

    public function getVisaLinker(Request $request) {
        return User::whereIn('id', $request->userId)->get();
    }

}
