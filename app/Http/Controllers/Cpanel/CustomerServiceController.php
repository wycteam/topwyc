<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\User;
use App\Models\Issue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notifications\NewChatMessage;
use App\Notifications\IssueAssignment;
use Illuminate\Support\Facades\Notification;
use App\Support\Collection;

use App\Classes\Common;

class CustomerServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cpanel.customer-service.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $issue = Issue::with(['chat_room.messages.user', 'chat_room.users'])->findOrFail($id);

        $supportUsers = User::whereHas('roles', function ($query) {
            $query->where('name', 'cpanel-admin')
                ->orWhere('name', 'cpanel-user')
                ->orWhere('name', 'support');
        })->get();

        $normalUsers = User::whereHas('roles', function ($query) {
            $query->where('name', 'vendor')
                ->orWhere('name', 'shop-user');
        })->get();

        $users = new Collection();
        $all =($users->merge($supportUsers)->merge($normalUsers));

        //get unique ids
        $unique = $all->unique('id');
        $all->values()->all();

        $issuer = User::where('id','=',$issue->user_id)->first();

        session()->flash('data', compact('issue', 'supportUsers','normalUsers','all','issuer'));

        return view('cpanel.customer-service.show', compact('issue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function createConvo($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DB::transaction(function () use ($request, $id) {
            $issue = Issue::findOrFail($id);
            $issue->fill($request->all());

            if($issue->isDirty()){
                //getOriginal() -> get original values of model
                //getDirty -> get all fields updated with value
                $changes = $issue->getDirty();
                $detail = 'Updated Issue -> ';
                foreach ($changes as $key => $value) {
                    $old = $issue->getOriginal($key);
                    $detail .= "Change ".$key." from ".$old." to ".$value.". ";
                }
                Common::saveActionLogs('Issue', $id, $detail);
            }

            $issue->save();

            if ($issue->status != 'closed') {
                $supportIds = collect($request->get('supportIds', []));
                $supportUsers = [];

                if (! $issue->chat_room && ! $supportIds->isEmpty()) {
                    $supportUsers = User::whereIn('id', $supportIds->toArray())->get();
                    $chatRoom = $issue->assignSupportToChatRoom($supportIds->toArray());
                    $message = $chatRoom->messages()
                        ->create([
                            'user_id' => $issue->user_id,
                            'body' => "Subject: ".$issue->subject,
                        ]);
                    $message = $chatRoom->messages()
                        ->create([
                            'user_id' => $issue->user_id,
                            'body' => "Description: ".$issue->body,
                        ]);

                    $detail = '';
                    $supportIdsWithIssueCreator = $request->supportIds;
                    $supportIdsWithIssueCreator[] = $issue->user_id;
                    foreach($supportIdsWithIssueCreator as $supportId) {
                        $userFullName = User::find($supportId)->first_name . ' ' . User::find($supportId)->last_name;

                        $detail .= 'User ' . $userFullName . ' is added to the conversation.';
                    }
                    if($detail != '') {
                        $detail = substr_replace($detail, 'Updated Issue -> ' . $detail, 0);
                        Common::saveActionLogs('Issue', $id, $detail);
                    }

                    if (! empty($supportUsers)) {
                        Notification::send($supportUsers, new NewChatMessage($issue->user, $message));
                    }
                } elseif ($issue->chat_room) {
                    $chatRoomUserIds = $issue->chat_room->users->pluck('id');

                    $newSupportUserIds = $supportIds->diff($chatRoomUserIds)->toArray();

                    $detail = '';
                    
                    // Added
                    foreach($newSupportUserIds as $newSupportUserId) {
                        $userFullName = User::find($newSupportUserId)->first_name . ' ' . User::find($newSupportUserId)->last_name;

                        $detail .= 'User ' . $userFullName . ' is added to the conversation.';
                    }
                    
                    // Removed
                    $supportIdsWithIssueCreator = $request->supportIds;
                    $supportIdsWithIssueCreator[] = $issue->user_id;
                    foreach($chatRoomUserIds as $chatRoomUserId) {
                        if(!in_array($chatRoomUserId, $supportIdsWithIssueCreator)) {
                            $userFullName = User::find($chatRoomUserId)->first_name . ' ' . User::find($chatRoomUserId)->last_name;

                            $detail .= 'User ' . $userFullName . ' is removed from the conversation.';
                        }
                    }

                    if($detail != '') {
                        $detail = substr_replace($detail, 'Updated Issue -> ' . $detail, 0);
                        Common::saveActionLogs('Issue', $id, $detail); 
                    }

                    $supportUsers = User::whereIn('id', $newSupportUserIds)->get();

                    $issue->chat_room->users()
                        ->sync(array_merge($supportIds->toArray(), [$issue->user_id]));
                }

                if (! empty($supportUsers)) {
                    Notification::send($supportUsers, new IssueAssignment($issue, Auth::user()));
                }
            }

            return $issue;
        });

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
