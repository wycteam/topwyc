<?php

namespace App\Http\Controllers\Cpanel;

use Auth;
use App\Models\User;
use App\Models\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\DocumentDenied;
use App\Notifications\DocumentApproved;
use App\Http\Requests\CpanelDocumentsValidation;
use App\Http\Requests\CpanelDocumentsDenyValidation;
use Carbon\Carbon;

use App\Classes\Common;

class DocumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cpanel.documents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data = Document::with(['uploader'])->orderBy('id','desc')->get();
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($status, $id)
    {
        $user = Auth::user();
        $data = Document::whereId($id)->update([
             'status' => $status
            ,'user_id' => $user->id
            ]);
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showApproved()
    {
        $data = Document::with(['uploader'])
                ->whereStatus('Verified')->orderBy('id','desc')->get();
        return $data;
    }

    public function showDeclined()
    {
        $data = Document::with(['uploader'])
                ->whereStatus('Denied')->orderBy('id','desc')->get();
        return $data;
    }

    public function showPending()
    {
        $data = Document::with(['uploader'])
                ->whereStatus('Pending')->orderBy('id','desc')->get();
        return $data;
    }

    public function docCount()
    {
        $data['all'] = count($this->show());
        $data['pending'] = count($this->showPending());
        $data['verified'] = count($this->showApproved());
        $data['denied'] = count($this->showDeclined());
        return $data;
    }

    public function getUpload($id)
    {
        $data = Document::find($id)->value('name');
        return $data;

    }

    public function getDetails($id)
    {
        $data = Document::whereId($id)->first();
        return $data;
    }

    public function verify(CpanelDocumentsValidation $request)
    {        
        $authUser = Auth::user();
        $data = Document::findOrFail($request->get('id'));
        $data->fill([
            'user_id' => $authUser->id,
            'status' => 'Verified',
            'expires_at' => $request->get('expires_at'),
        ]);

        if($data->isDirty()) {
            //getOriginal() -> get original values of model
            //getDirty -> get all fields updated with value
            $changes = $data->getDirty();
            $detail = 'Updated Document -> ';
            foreach ($changes as $key => $value) {
                $old = (is_null($data->getOriginal($key))) ? 'NULL' : $data->getOriginal($key);

                if($key == 'user_id') {
                    $key = 'user';

                    if($old != 'NULL') {
                        $user = User::find($old);
                        $old = $user->first_name . ' ' . $user->last_name;
                    }

                    $user = User::find($value);
                    $value = $user->first_name . ' ' . $user->last_name;
                }

                $detail .= "Change ".$key." from ".$old." to ".$value.". ";
            }
            Common::saveActionLogs('Document', $request->get('id'), $detail);
        }

        $data->save();

        $toUser = User::find($data->documentable_id);
        $toUser->notify(new DocumentApproved($data, $authUser));

        return $data;
    }

    public function deny(CpanelDocumentsDenyValidation $request)
    {
        // $data = Document::whereId($request['id'])
        //         ->update([
        //           'status' => $request['status'],
        //           'reason' => $request['reason']
        //         ]);

        $authUser = Auth::user();

        $data = Document::findOrFail($request->get('id'));
        $data->fill([
            'user_id' => $authUser->id,
            'status' => 'Denied',
            'reason' => $request->get('reason'),
        ]);

        if($data->isDirty()) {
            //getOriginal() -> get original values of model
            //getDirty -> get all fields updated with value
            $changes = $data->getDirty();
            $detail = 'Updated Document -> ';
            foreach ($changes as $key => $value) {
                $old = (is_null($data->getOriginal($key))) ? 'NULL' : $data->getOriginal($key);

                if($key == 'user_id') {
                    $key = 'user';

                    if($old != 'NULL') {
                        $user = User::find($old);
                        $old = $user->first_name . ' ' . $user->last_name;
                    }

                    $user = User::find($value);
                    $value = $user->first_name . ' ' . $user->last_name;
                }

                $detail .= "Change ".$key." from ".$old." to ".$value.". ";
            }
            Common::saveActionLogs('Document', $request->get('id'), $detail);
        }

        $data->save();

        $toUser = User::find($data->documentable_id);
        $toUser->notify(new DocumentDenied($data, $authUser));

        return $data;
    }
}
