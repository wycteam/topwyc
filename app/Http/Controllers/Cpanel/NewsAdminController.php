<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Visa\News as News;
use App\Models\Visa\Author as Author;
use File;
class NewsAdminController extends Controller
{

  public function getTranslation() {
    return json_encode([
      'translation' => trans('cpanel/news-page')
    ]);
  }

  public function getNews(){
    return view('cpanel.visa.news.news-panel');
  }

  public function index(Request $request){

    $columns = ['header', 'name', 'created_at'];
        $length = $request->input('length');
        $column = $request->input('column'); //Index
        $dir = $request->input('dir');
        $searchValue = $request->input('search');
        $query = DB::connection('visa')->table('newslist')->select(DB::raw('header, name, created_at, news_id'))->orderBy($columns[$column], $dir);
        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('header', 'like', '%' . $searchValue . '%')
                ->orWhere('name', 'like', '%' . $searchValue . '%');
            });
        }
        $projects = $query->paginate($length);
        return ['data' => $projects, 'draw' => $request->input('draw')];
  }

  public function destroy($id){
        return News::destroy($id);
  }
  public function show($id){
       $ret = News::findOrFail($id);  
        $ret->content = base64_decode($ret->content);
        return $ret;
  }
  public function update(Request $request, $id){
    date_default_timezone_set("Asia/Manila");
      $this->validate($request, [
        'header' => 'required|min:5',
        'content' => 'required_if:<p><br></p>,==,<p><br></p>|min:50',
      ]);

          $path = public_path('/images/news/'.$id);
            if(!File::exists($path)) {
                File::makeDirectory($path, 0777, true, true);
            }else{
                File::deleteDirectory($path);
                File::makeDirectory($path, 0777, true, true);
            }

      $ndata = $request->all();

        $dom = new \domdocument();
        libxml_use_internal_errors(true);
        $dom->loadHtml( mb_convert_encoding($ndata['content'], 'HTML-ENTITIES', "UTF-8"),LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');

        foreach($images as $image){
          $image->setAttribute('style', 'width:100%;');
          $image->removeAttribute('width');
          $image->removeAttribute('height');
          //\Log::info(strpos($image->getAttribute('src'), 'topwyc'));
          //if ((strpos($image->getAttribute('src'), 'topwyc') === false) || (strpos($image->getAttribute('src'), 'wyc-group') === false)) {//if base64 
            
            $nimage = \Image::make($image->getAttribute('src'));
            $mime = $nimage->mime();
            if ($mime == 'image/jpeg')
                $ext= '.jpg';
            elseif ($mime == 'image/png')
                $ext= '.png';
            elseif ($mime == 'image/gif')
                $ext= '.gif';
            elseif($mime == 'image/x-icon')
                $ext = '.ico';
            elseif($mime == 'image/bmp')
                $ext = '.bmp';
            else {
                $ext = '.jpg';
            }
            $file_name = round(microtime(true) * 1000).$ext;
             $nimage->save($path.'/'.$file_name);
            
             $image->setAttribute('src',str_replace('cpanel.','',url('/')).'/images/news/'.$id.'/'.$file_name);
         // }
          
        }
        //\Log::info($dom->savehtml());
        $ndata['mobile_content'] = $dom->savehtml();
       
        //$ndata['mobile_content'] = stripslashes(str_replace("|-|","\"",str_replace("mick_mick","' width='",str_replace("mick-mick","='",str_replace("mickmick","'",html_entity_decode(str_replace("&quot;","'",str_replace("family: &quot;","family: |-|",str_replace("&quot;;","|-|;",str_replace("&quot; style","' style",str_replace("=&quot;","mick-mick", str_replace("&quot; width=&quot;","mick_mick",str_replace("px&quot;","px'",htmlentities($dom->savehtml()))))))))))))));

        $ndata['content'] = base64_encode($ndata['content']);//$this->make_excerpt($dom->savehtml());
        
        News::findOrFail($id)->update($ndata);
        
        if($request->get('form_data')){
          $image = $request->get('form_data');
          //$name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          $nImage = \Image::make($request->get('form_data'));
          $nnImage = $nImage;
          $nnImage->resize(1280, 852);
          $nnImage->save(public_path('/images/news/large/').$request->get('image'));
          $nImage->resize(230, 153);
          $nImage->save(public_path('/images/news/').$request->get('image'));
          File::delete(public_path('/images/news/').$request->get('temp_data'));
          File::delete(public_path('/images/news/large/').$request->get('temp_data'));
        }

        if (Author::where('user_id', $request->input('author_id'))->count() == 0) {
            $author = new Author;
            $author->user_id = $request->input('author_id');
            $author->name = $request->input('name');
            $author->ip_address = trim(shell_exec("dig +short myip.opendns.com @resolver1.opendns.com"));
            $author->save();
        }
        //var_dump($request->form_data);
        return response()->json($request->all()); //response()->json()
  }


  public function store(Request $request){
    date_default_timezone_set("Asia/Manila");
        $this->validate($request, [
          'header' => 'required|min:5',
          'content' => 'required_if:<p><br></p>,==,<p><br></p>|min:50',
        ]);
        $ndata = $request->all();
        if(!File::exists(public_path('images/news'))) {
          File::makeDirectory(public_path('images/news'), 0777, true, true);
        }
        if(!File::exists(public_path('images/news/large'))) {
          File::makeDirectory(public_path('images/news/large'), 0777, true, true);
        }
        if(!File::exists(public_path('images/news/summernote'))) {
          File::makeDirectory(public_path('images/news/summernote'), 0777, true, true);
        } 
        $id = News::max('news_id')+1;
        $path = public_path('/images/news/'.$id);
        if(!File::exists($path)) {
            File::makeDirectory($path, 0777, true, true);
        }else{
            File::deleteDirectory($path);
            File::makeDirectory($path, 0777, true, true);
        }

        ////get image from summernote
        $dom = new \domdocument();
        libxml_use_internal_errors(true);
          $dom->loadHtml( mb_convert_encoding($ndata['content'], 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');

        foreach($images as $image){
          $image->setAttribute('style', 'width:100%;');
          $image->removeAttribute('width');
          $image->removeAttribute('height');

          //if ((strpos($image->getAttribute('src'), 'topwyc') === false) || (strpos($image->getAttribute('src'), 'wyc-group') === false)) {//if base64 
            
            $nimage = \Image::make($image->getAttribute('src'));
            $mime = $nimage->mime();
            if ($mime == 'image/jpeg')
                $ext= '.jpg';
            elseif ($mime == 'image/png')
                $ext= '.png';
            elseif ($mime == 'image/gif')
                $ext= '.gif';
            elseif($mime == 'image/x-icon')
                $ext = '.ico';
            elseif($mime == 'image/bmp')
                $ext = '.bmp';
            else {
                $ext = '.jpg';
            }
            $file_name = round(microtime(true) * 1000).$ext;
             $nimage->save($path.'/'.$file_name);
             $image->setAttribute('src',str_replace('cpanel.','',url('/')).'/images/news/'.$id.'/'.$file_name);
          }
        //}
        //$ndata['mobile_content'] = stripslashes(str_replace("|-|","\"",str_replace("mick_mick","' width='",str_replace("mick-mick","='",str_replace("mickmick","'",html_entity_decode(str_replace("&quot;","'",str_replace("family: &quot;","family: |-|",str_replace("&quot;;","|-|;",str_replace("&quot; style","' style",str_replace("=&quot;","mick-mick", str_replace("&quot; width=&quot;","mick_mick",str_replace("px&quot;","px'",htmlentities($dom->savehtml()))))))))))))));
        $ndata['mobile_content'] = $dom->savehtml();

         $ndata['content'] = base64_encode($ndata['content']); //$this->make_excerpt($dom->savehtml());

         News::create($ndata);


         if($request->get('form_data')){
           $image = $request->get('form_data');
           //$name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
           $nImage = \Image::make($request->get('form_data'));
           $nnImage = $nImage;
           $nnImage->resize(1280, 852);
           $nnImage->save(public_path('/images/news/large/').$request->get('image'));
           $nImage->resize(230, 153);
           $nImage->save(public_path('/images/news/').$request->get('image'));
         }



        if (Author::where('user_id', $request->input('author_id'))->count() == 0) {
            $author = new Author;
            $author->user_id = $request->input('author_id');
            $author->name = $request->input('name');
            $author->ip_address = trim(shell_exec("dig +short myip.opendns.com @resolver1.opendns.com"));
            $author->save();
        }
        return response()->json($request->all());
  }


  function make_excerpt ($rawHtml, $length = 500000) {
    // append an ellipsis and "More" link
    $content = substr($rawHtml, 0, $length);
  
    // Detect the string encoding
    $encoding = mb_detect_encoding($content);
  
    // pass it to the DOMDocument constructor
    $doc = new \DOMDocument('', $encoding);
  
    // Must include the content-type/charset meta tag with $encoding
    // Bad HTML will trigger warnings, suppress those
    @$doc->loadHTML('<html><head>'
      . '<meta http-equiv="content-type" content="text/html; charset='
      . $encoding . '"></head><body>' . trim($content) . '</body></html>');
  
    // extract the components we want
    $nodes = $doc->getElementsByTagName('body')->item(0)->childNodes;
    $html = '';
    $len = $nodes->length;
    for ($i = 0; $i < $len; $i++) {
      $html .= $doc->saveHTML($nodes->item($i));
    }
    return $html;
  }


}
