<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\AccessControl;
use App\Http\Requests\RoleValidation;
use DB, DateTime;
use Carbon\Carbon;
use Excel;

class FinanceController extends Controller
{

    public function index() {
        $this->LoadAjax();

        $file = file_get_contents(storage_path('/app/public/data/VisaFinancing.csv'));
        $data = array_map("str_getcsv", preg_split('/\r*\n+|\r+/', $file));
        $data = array_reverse($data);
        $stats = $data[1];
        $ctr =0;
        $fp = fopen(storage_path('/app/public/data/VisaFinancingReverse.csv'), 'w') or die('Unable to open file!');
        $fp2 = fopen(storage_path('/app/public/data/VisaFinancingPending.csv'), 'w') or die('Unable to open file!');
        foreach ($data as $fields) {
            if($ctr!=0&&$ctr<=500){
            fputcsv($fp, $fields);            
                if($fields[1]!=""){
                    fputcsv($fp2, $fields);
                }  
            }
            $ctr++;
        }
        $cym = date('Y-m');
        //dd($cym);
        $cMonth = DB::connection('visa')
            ->table('financing2 as a')
            ->where("dt", 'LIKE', '%'.$cym.'%')
            ->get();
        $prev ='';
        if ($cMonth->first()){
        $first = $cMonth->first();
        $prev = DB::connection('visa')
            ->table('financing2')
            ->where('id','<',$first->id)
            ->max('id');
        }
        else{
           $prev = DB::connection('visa')
            ->table('financing2')
            ->max('id'); 
        }
        $ini = DB::connection('visa')
            ->table('financing2')
            ->select('cash_balance','bank_balance')
            ->first();
        if($prev){
            $prev = DB::connection('visa')
            ->table('financing2')
            ->where('id',$prev)
            ->select('cash_balance','bank_balance')
            ->first();
            $t = $prev->cash_balance; $u = $prev->bank_balance;
        }
        else{
            $t = 0; $u =0;
        }
        //dd($prev);
        $p = 0; $q = 0; $r = 0; $s = 0;  $v = 0; $w = 0; 
        $tempTotal = $t + $u;
        $initial = array(
            'cashInitial' => $this->formatMoney($t*1),            
            'bankInitial' => $this->formatMoney($u*1),  
            'allcashInitial' =>$this->formatMoney($ini->cash_balance*1),            
            'allbankInitial' => $this->formatMoney($ini->bank_balance*1),
        );
        foreach($cMonth as $value){
            $f = $value->cash_client_depo_payment;
            $g = $value->cash_client_refund;
            $h = $value->cash_client_process_budget_return;
            $i = $value->cash_process_cost;
            $j = $value->borrowed_process_cost;
            $k = $value->cash_admin_budget_return;
            $l = $value->borrowed_admin_cost;
            $m = $value->cash_admin_cost;
            $n = $value->bank_client_depo_payment;
            $o = $value->bank_cost;
            $p = $p + $i - $h + $j;
            $p = ($p == 0 ) ? 0:$p;
            //$txt = '"'.$this->formatMoney($p*1).'",';
            $q = $q + $f + $n - $g + $j -$o;
            $q = ($q == 0 ) ? 0:$q;
            //$txt = '"'.$this->formatMoney($q*1).'",';
            $r = $r + $f - $i - $g + $n + $h;
            $r = ($r == 0 ) ? 0:$r;
            //$txt = '"'.$this->formatMoney($r*1).'",';
            $s = $m - $k + $s;
            $s = ($s == 0 ) ? 0:$s;
            //$txt = '"'.$this->formatMoney($s*1).'",';
            if($value->cat_type == 'initial'){
                $t = $value->cash_balance;
            }
            else{
                if($f >0 && $j > 0){
                    $t = $t + $f - $g - $i - $m + $h + $k - $l-$j ; // formula for cash balance if borrowed with return                
                }
                else{
                    $t = $t + $f - $g - $i - $m + $h + $k - $l; // formula for cash balance if borrowed then no return
                }
            }
            //$txt = '"'.($this->formatMoney($t*1)).'",';
            if($value->cat_type == 'initial'){
                $u = $value->bank_balance;
            }
            else{
                $u = $u + $n - $o;
            }
            //$txt = '"'.($this->formatMoney($u*1)).'",';
            //For Sum of Cash Balance & Bank Balance
            $v = $t + $u;
            //$txt = '"'.($this->formatMoney($v*1)).'",';
            if($tempTotal == 0){
                $tempTotal = $v;
            }
            else{
                $w = $v - $tempTotal;
            }
            $w = ($w == 0 ) ? 0:$w;
        }
        
       $mnth = array(
            'p' => $this->formatMoney($p*1),            
            'q' => $this->formatMoney($q*1),            
            'r' => $this->formatMoney($r*1),
            's' => $this->formatMoney($s*1),
            't' => $this->formatMoney($t*1),
            'u' => $this->formatMoney($u*1),
            'v' => $this->formatMoney($v*1),
            'w' => $this->formatMoney($w*1),
        );
        
        //dd($mnth);
        
        $process = DB::connection('visa')
            ->table('financing2 as a')
            ->where("cash_process_cost",">",0)
            ->orderBy('id','Desc')
            ->get();
        
        $admin = DB::connection('visa')
            ->table('financing2 as a')
            ->where("cash_admin_cost",">",0)
            ->orderBy('id','Desc')
            ->get(); 
               
        $deftJason = DB::connection('visa')
            ->table('jason_excel')
            ->where("debt",">",0)
            ->orderBy('id','Desc')
            ->get();    

        $deftJerry = DB::connection('visa')
            ->table('jerry_excel')
            ->where("debt",">",0)
            ->orderBy('id','Desc')
            ->get();  
        
        fclose($fp);

        $this->LoadJason();
        $file = file_get_contents(storage_path('/app/public/data/Jason.csv'));
        $data = array_map("str_getcsv", preg_split('/\r*\n+|\r+/', $file));
        $data = array_reverse($data);
        // if(count($data)!=0){
        //     $stats = $data[1];
        // }
        $ctr =0;
        $fp = fopen(storage_path('/app/public/data/JasonReverse.csv'), 'w') or die('Unable to open file!');
        foreach ($data as $fields) {
            if($ctr!=0&&$ctr<=500){
            fputcsv($fp, $fields);             
            }
            $ctr++;
        }  
        
        fclose($fp);

        $this->LoadJerry();
        $file = file_get_contents(storage_path('/app/public/data/Jerry.csv'));
        $data = array_map("str_getcsv", preg_split('/\r*\n+|\r+/', $file));
        $data = array_reverse($data);
        // if(count($data)!=0){
        //     $stats = $data[1];
        // }
        $ctr =0;
        $fp = fopen(storage_path('/app/public/data/JerryReverse.csv'), 'w') or die('Unable to open file!');
        foreach ($data as $fields) {
            if($ctr!=0&&$ctr<=500){
            fputcsv($fp, $fields);             
            }
            $ctr++;
        }  
        
        fclose($fp);

        return view('cpanel/visa/financing/index', array('stats'=>$stats,'admin'=>$admin,'process'=>$process,'mnth'=>$mnth,'ini'=>$initial,'deftJason'=>$deftJason,'deftJerry'=>$deftJerry));

        // if($check==1)
        //     return view('cpanel/visa/financing/index');
        // else
        //     abort(404);
    }

    //remove public path before uploading
    public function LoadAjax(){
        $myfile = fopen(storage_path('/app/public/data/VisaFinancing.csv'), 'w') or die('Unable to open file!');
        $count = 0;
        
        $records = DB::connection('visa')
            ->table('financing2 as a')
            ->select(DB::raw('a.*, b.first_name, b.last_name, b.id'))
            ->leftjoin(DB::raw('(select * from wycgroup.users) as b'),'b.id','=','a.user_sn')
            ->orderBy('a.id')
            ->get();
        $p = 0; $q = 0; $r = 0; $s = 0; $t = 0; $u = 0; $v = 0; $w = 0; 
        $tempTotal = 0;
        $col = 0; $row =count($records);
        foreach ($records as $value) {

            $count++;
            
            $txt = '<a href="#" class="addNote" d-id="'.$value->id.'"> <span class="fa fa-clipboard"></span></a>'.',';
            fwrite($myfile, $txt);
            //if($dis->visa_financing_markresolved){
                if($value->cash_process_cost !="" || $value->cash_admin_cost!= ""){
                    if($value->vf_btn=="1"){
                        $amt = $value->cash_process_cost == "" ? $value->cash_process_cost : $value->cash_admin_cost;
                        $txt = '<a href="#" class="rcv_or" amt="'.$amt.'" o-id="'.$value->id.'" orStat="'.$value->with_or.'"> <span class="fa fa-check-square-o"></span></a>'.',';
                    }
                    else{
                        $txt = ''.',';
                    }
                }
                else{
                    $txt = ''.',';
                }
//            }
//            else{
//                $txt = ''.',';
//            }
            fwrite($myfile, $txt);
            
            $datetime = new DateTime($value->dt);

            $getdate = $datetime->format('Y M d');
            $gettime = $datetime->format('h:i A');

            //A
            $txt = '"'.$getdate.'",';
            fwrite($myfile, $txt);

            //B
            $txt = '"'.$gettime.'",';
            fwrite($myfile, $txt);

            //C
            $rw = $row-1;
//            $allnotes = DB::connection('visa')
//            ->table('tbl_notes as a')
//            ->where("vf_id",$value->id)
//            ->select("text","user_sn")
//            ->get();
            
            $allnotes = DB::connection('visa')
                ->table('financing_notes as a')
                ->select(DB::raw('a.*, b.first_name, b.last_name, b.id'))
                ->leftjoin(DB::raw('(select * from wycgroup.users) as b'),'b.id','=','a.user_id')
                ->where('a.vf_id', $value->id)
                ->orderBy('a.id','Desc')
                ->get();
            $nt = "";
            
            if($allnotes){
                foreach($allnotes as $an){
                    if($nt!=""){
                        $nt = "<small>created by ".$value->first_name."</small><br>";
                    }
                    $cdt = substr($an->dt, 5);                   
                    $cdt = substr($cdt, 0,-3);
                    $nt .= "<span style='font-size:11px;'>".$an->text."<small><b> -".$an->first_name." (".$cdt.")</b></small></span><br>";
                }                
            }
            else{
                $nt = "<small>created by ".$value->first_name."</small><br>";
            }
            $out = strlen($value->trans_desc) > 45 ? substr($value->trans_desc,0,45)."..." : $value->trans_desc;
            if($nt!=""){
                $out = "<b>".$out."</b>";
            }
            $txt = '<span id="cell4-'.$rw.'" full_desc="'.$value->trans_desc.'" data-toggle="tooltip" data-html="true" data-placement="right" title="'.$nt.'">'.$out.'</span>';
            $txt = $txt.',';
            fwrite($myfile, $txt);

            //D
            $txt = '"'.$value->cat_type.'",';
            fwrite($myfile, $txt);

            //E
            $txt = '"'.$value->cat_storage.'",';
            fwrite($myfile, $txt);

            //F
            $f = $value->cash_client_depo_payment;
            $txt = '"'.$this->formatMoney($f*1).'",';
            fwrite($myfile, $txt);

            //G
            $g = $value->cash_client_refund;
            $txt = '"'.$this->formatMoney($g*1).'",';
            fwrite($myfile, $txt);

            //H
            $h = $value->cash_client_process_budget_return;
            $txt = '"'.($this->formatMoney($h*1)).'",';
            fwrite($myfile, $txt);

            //I
            $i = $value->cash_process_cost;
            $txt = '"'.($this->formatMoney($i*1)).'",';
            fwrite($myfile, $txt);

            //J
            $j = $value->borrowed_process_cost;
            $txt = '"'.($this->formatMoney($j*1)).'",';
            fwrite($myfile, $txt);

            //K
            $k = $value->cash_admin_budget_return;
            $txt = '"'.($this->formatMoney($k*1)).'",';
            fwrite($myfile, $txt);

            //L
            $l = $value->borrowed_admin_cost;
            if($l>$k){
                $classic = $value->whereDeduct."_deft";
                $txt = '<span class="'.$classic.'">'.$l.'</span>'.',';
            }
            else{
                $txt = '"'.($this->formatMoney($l*1)).'",';
            }
            fwrite($myfile, $txt);

            //M
            $m = $value->cash_admin_cost;
            $txt = '"'.($this->formatMoney($m*1)).'",';
            fwrite($myfile, $txt);

            //N
            $n = $value->bank_client_depo_payment;
            $txt = '"'.($this->formatMoney($n*1)).'",';
            fwrite($myfile, $txt);

            //O
            $o = $value->bank_cost;
            $txt = '"'.($this->formatMoney($o*1)).'",';
            fwrite($myfile, $txt);

            //P
            $p = $p + $i - $h + $j;
            $p = ($p == 0 ) ? 0:$p;
            $txt = '"'.$this->formatMoney($p*1).'",';
            fwrite($myfile, $txt);

            //Q
            $q = $q + $f + $n - $g + $j -$o;
            $q = ($q == 0 ) ? 0:$q;
            $txt = '"'.$this->formatMoney($q*1).'",';
            fwrite($myfile, $txt);

            //R
            $r = $r + $f - $i - $g + $n + $h;
            $r = ($r == 0 ) ? 0:$r;
            $txt = '"'.$this->formatMoney($r*1).'",';
            fwrite($myfile, $txt);

            //S
            $s = $m - $k + $s;
            $s = ($s == 0 ) ? 0:$s;
            $txt = '"'.$this->formatMoney($s*1).'",';
            fwrite($myfile, $txt);

            //T
            if($value->cat_type == 'initial'){
                $t = $value->cash_balance;
            }
            else{
                if($f >0 && $j > 0){
                    $t = $t + $f - $g - $i - $m + $h + $k - $l-$j ; // formula for cash balance if borrowed process with return                
                }
//                else if($l >0 && $k <= 0){
//                    $t = $t + $f - $g - $i - $m + $h + $k -$j ; // formula for cash balance if borrowed with return                
//                }
                else{
                    $t = $t + $f - $g - $i - $m + $h + $k - $l; // formula for cash balance if borrowed then no return
                }
            }
            $txt = '"'.($this->formatMoney($t*1)).'",';
            fwrite($myfile, $txt);

            //U
            if($value->cat_type == 'initial'){
                $u = $value->bank_balance;
            }
            else{
                $u = $u + $n - $o;
            }
            $txt = '"'.($this->formatMoney($u*1)).'",';
            fwrite($myfile, $txt);

            if($value->cat_type != 'initial'){
                $log_data2 = array(
                    'cash_balance' => $t,                    
                    'bank_balance' => $u,
                );
                  $save_return = DB::connection('visa')
                    ->table('financing2')
                    ->where('id', $value->id)
                    ->update($log_data2);
            }
            //V
            //For Sum of Cash Balance & Bank Balance
            $v = $t + $u;
            $txt = '"'.($this->formatMoney($v*1)).'",';
            fwrite($myfile, $txt);

            //W
            if($tempTotal == 0){
                $tempTotal = $v;
            }
            else{
                $w = $v - $tempTotal;
            }
            $w = ($w == 0 ) ? 0:$w;
                
            $txt = '"'.$this->formatMoney($w*1).'",';
            fwrite($myfile, $txt);

            $txt = '"'.$value->postdated_checks.'"';
            fwrite($myfile, $txt);
            //X
            // $txt = '"'.$value->postdated_checks.'",';
            // fwrite($myfile, $txt);

            // if($value->total_balance==0){
            // $txt = '"'.($this->formatMoney($value->total_balance)).'",';
            // }
            // else{
            // $txt = '"'.($this->formatMoney($value->total_balance*-1)).'",';
            // }
            // fwrite($myfile, $txt);

            // $txt = '"<center><a href=\"javascript:void(0)\" onclick=\"OpenClient('."'".$value->client_id."'".');\"><i class=\"fa fa-arrow-right fa-1x\"></i></a></center>"';
            // fwrite($myfile, $txt);

            $txt = "\n";
            fwrite($myfile, $txt);
            $col++;
            $row--;
        }
        // $txt = "\n\n";
        // fwrite($myfile, $txt);
        fclose($myfile);
        return true;
    }

    public function LoadJason(){
        $myfile = fopen(storage_path('/app/public/data/Jason.csv'), 'w') or die('Unable to open file!');
        $count = 0;
        $user = Auth::user();
        
        $records = DB::connection('visa')
            ->table('jason_excel as a')
            ->select(DB::raw('a.*, b.dt'))
            ->leftjoin(DB::raw('(select * from financing2) as b'),'b.id','=','a.trans_id')
            ->orderBy('id')
            ->get();
        $credit=0; $debt=0; $t = 0; 
        $tempTotal = 0;
        $col = 0; $row =0;
        foreach ($records as $value) {

            $count++;
            //$txt = "[";
            //fwrite($myfile, $txt);
            
            $datetime = new DateTime($value->dt);
            $getdate = ''; $gettime = '';
            if($value->initial != 'initial'){
                $getdate = $datetime->format('Y M d');
                $gettime = $datetime->format('h:i A');   
            }
            //A
            $txt = '"'.$getdate.'",';
            fwrite($myfile, $txt);

            //B
            $txt = '"'.$gettime.'",';
            fwrite($myfile, $txt);

            //D
            $txt = '"'.$value->excel.'",';
            fwrite($myfile, $txt);
            
            //C
            $rw = $row-1;
            
            $out = strlen($value->trans_desc) > 45 ? substr($value->trans_desc,0,45)."..." : $value->trans_desc;
            $txt = '<span id="cell4-'.$rw.'" full_desc="'.$value->trans_desc.'">'.$out.'</span>';
            $txt = $txt.',';
            fwrite($myfile, $txt);

            //E
            $credit = $value->credit;
            $txt = '"'.($this->formatMoney($credit*1)).'",';
            fwrite($myfile, $txt);

            //L
            $debt = $value->debt;
            $txt = '"'.($this->formatMoney($debt*1)).'",';
            fwrite($myfile, $txt);

            //T
            if($value->initial == 'initial'){
                $t = $value->cash_balance;
            }
            else{
                $t = ($t - $debt)+$credit;
            }
            if($t=='0'){
                $txt = '"0"';
            }
            else{
                $txt = '"'.($this->formatMoney($t*1)).'"';
            }
            fwrite($myfile, $txt);
            
            if($value->initial != 'initial'||$value->settled =='0'){
                $log_data2 = array(
                    'cash_balance' => $t,                    
                );
                  $save_return = DB::connection('visa')
                    ->table('jason_excel')
                    ->where('id', $value->id)
                    ->update($log_data2);
            }

            //X
            // $txt = '"'.$value->postdated_checks.'",';
            // fwrite($myfile, $txt);

            // if($value->total_balance==0){
            // $txt = '"'.($this->formatMoney($value->total_balance)).'",';
            // }
            // else{
            // $txt = '"'.($this->formatMoney($value->total_balance*-1)).'",';
            // }
            // fwrite($myfile, $txt);

            // $txt = '"<center><a href=\"javascript:void(0)\" onclick=\"OpenClient('."'".$value->client_id."'".');\"><i class=\"fa fa-arrow-right fa-1x\"></i></a></center>"';
            // fwrite($myfile, $txt);

            $txt = "\n";
            fwrite($myfile, $txt);
            $col++;
            $row++;
        }
        // $txt = "\n\n";
        // fwrite($myfile, $txt);
        fclose($myfile);
        return true;
    }

public function LoadJerry(){
        $myfile = fopen(storage_path('/app/public/data/Jerry.csv'), 'w') or die('Unable to open file!');
        $count = 0;
        $user = Auth::user();
        
        $records = DB::connection('visa')
            ->table('jerry_excel as a')
            ->select(DB::raw('a.*, b.dt'))
            ->leftjoin(DB::raw('(select * from financing2) as b'),'b.id','=','a.trans_id')
            ->orderBy('id')
            ->get();
        $credit=0; $debt=0; $t = 0; 
        $tempTotal = 0;
        $col = 0; $row =0;
        foreach ($records as $value) {

            $count++;
            //$txt = "[";
            //fwrite($myfile, $txt);
            
            $datetime = new DateTime($value->dt);
            $getdate = ''; $gettime = '';
            if($value->initial != 'initial'){
                $getdate = $datetime->format('Y M d');
                $gettime = $datetime->format('h:i A');   
            }
            //A
            $txt = '"'.$getdate.'",';
            fwrite($myfile, $txt);

            //B
            $txt = '"'.$gettime.'",';
            fwrite($myfile, $txt);

            //D
            $txt = '"'.$value->excel.'",';
            fwrite($myfile, $txt);
            
            //C
            $rw = $row-1;
            
            $out = strlen($value->trans_desc) > 45 ? substr($value->trans_desc,0,45)."..." : $value->trans_desc;
            $txt = '<span id="cell4-'.$rw.'" full_desc="'.$value->trans_desc.'">'.$out.'</span>';
            $txt = $txt.',';
            fwrite($myfile, $txt);

            //E
            $credit = $value->credit;
            $txt = '"'.($this->formatMoney($credit*1)).'",';
            fwrite($myfile, $txt);

            //L
            $debt = $value->debt;
            $txt = '"'.($this->formatMoney($debt*1)).'",';
            fwrite($myfile, $txt);

            //T
            if($value->initial == 'initial'){
                $t = $value->cash_balance;
            }
            else{
                $t = ($t - $debt)+$credit;
            }
            if($t=='0'){
                $txt = '"0"';
            }
            else{
                $txt = '"'.($this->formatMoney($t*1)).'"';
            }
            fwrite($myfile, $txt);
            
            if($value->initial != 'initial'||$value->settled =='0'){
                $log_data2 = array(
                    'cash_balance' => $t,                    
                );
                  $save_return = DB::connection('visa')
                    ->table('jerry_excel')
                    ->where('id', $value->id)
                    ->update($log_data2);
            }

            //X
            // $txt = '"'.$value->postdated_checks.'",';
            // fwrite($myfile, $txt);

            // if($value->total_balance==0){
            // $txt = '"'.($this->formatMoney($value->total_balance)).'",';
            // }
            // else{
            // $txt = '"'.($this->formatMoney($value->total_balance*-1)).'",';
            // }
            // fwrite($myfile, $txt);

            // $txt = '"<center><a href=\"javascript:void(0)\" onclick=\"OpenClient('."'".$value->client_id."'".');\"><i class=\"fa fa-arrow-right fa-1x\"></i></a></center>"';
            // fwrite($myfile, $txt);

            $txt = "\n";
            fwrite($myfile, $txt);
            $col++;
            $row++;
        }
        // $txt = "\n\n";
        // fwrite($myfile, $txt);
        fclose($myfile);
        return true;
    }

    //Global Function
    public function formatMoney($number, $fractional=false) {
        if($number!=''){
            if ($fractional) {
                $number = sprintf('%.2f', $number);
            }
            while (true) {
                $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
                if ($replaced != $number) {
                    $number = $replaced;
                } else {
                    break;
                }
            }
        }
        else{
            $number = '';
        }
        return $number;
    }

}