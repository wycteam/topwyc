<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Shopping\OrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EwalletController extends Controller
{
    public function index(){
    	return view('cpanel.ewallet.index');
    }

    public function pool(){
    	//pending orders
    	$data['unfinished_orders'] = OrderDetail::with('order.user','product.user')
    		->where('status', 'Pending')
            ->orWhere('status', 'In Transit')
            ->orWhere('status', 'On Hold')
    		->orWhere(function($q){
                $q->where('status','Returned');
                $q->whereNull('returned_received_date');
            })
    		->get();
        //On Hold status >> unsettled issues between seller & buyer (return)
        $subtotal = $data['unfinished_orders']->pluck('subtotal')->sum();
        $data['onhold']   = OrderDetail::Where('status', 'In Transit')
                    ->orWhere('status', 'On Hold')
                    ->orWhere(function($q){
                        $q->where('status','Returned');
                        $q->whereNull('returned_received_date');
                    })
                    ->get();
        $charge      = $data['onhold']->pluck('charge')->sum();
        $vat      = $data['onhold']->pluck('vat')->sum();
    	$fee      = $data['onhold']->pluck('shipping_fee')->sum();
        $data['total_pool'] = $subtotal + $charge + $vat + $fee;
    	return view('cpanel.ewallet.pool',$data);
    }
}
