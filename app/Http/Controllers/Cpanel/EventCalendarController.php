<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Shopping\Order;
use App\Models\Shopping\Store;
use App\Models\Shopping\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ReasonValidation;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Visa\ClientService;
use App\Models\Visa\Task;
use App\Models\RoleUser as role_users;
use App\Models\Role as role;
use App\Models\Visa\DeliverySchedule as deliveryschedule;
use App\Models\Visa\VisaType;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

class EventCalendarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setLocale($locale = 'en') {
        return response(['success'=> true, 'activeLocale' => $locale], 200)->withCookie(cookie()->forever('locale', $locale));
    }

    public function getLocale() {
        $locale = (\Request::cookie('locale')) ? \Request::cookie('locale') : 'en';

        return response(['success' => true, 'activeLocale'=> $locale], 200)->withCookie(cookie()->forever('locale', $locale));
    }

    public function translation() {
        return json_encode([
            'translation' => trans('cpanel/event-calendar')
        ]);
    }


    public function index()
    {
        $translation = trans('cpanel/event-calendar');
        $role_id = 9;
        $yesterday = date("m/d/Y", strtotime( '-1 days' ) );

        $data = array(
            'title' => $translation['event_calendar']
        );

        return view('cpanel.visa.event-calendar.index', $data);
    }

    public function formatMoney($number, $fractional=false) {
        if ($fractional) {
            $number = sprintf('%.2f', $number);
        }
        while (true) {
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
            if ($replaced != $number) {
                $number = $replaced;
            } else {
                break;
            }
        }
        return trim($number);
    }


    public function loadCompanyCouriers(){
        $role = role::where('name', 'company-courier')->first();
        $role_users = role_users::where('role_id',$role->id)->with('user')->get();
        return json_encode($role_users);
    }


    public function deleteSchedule($id){
        $data = deliveryschedule::findOrFail($id);
        $data->delete();
        return response()->json($data);
    }

    public function getSchedulesbyDate(Request $request) {
        $date = $request->dateFilter;
        $schedules = deliveryschedule::with('rider')->where('scheduled_date', $date)->get();
        return json_encode([
            'success' => true,
            'schedules' => $schedules
        ]);
    }

    public function editSchedule(Request $request){
        $data = deliveryschedule::whereId($request->id)->update([
             'item'           => $request->item
            ,'rider_id'       => $request->rider_id
            ,'location'       => $request->location
            ,'scheduled_date' => $request->scheduled_date
            ,'scheduled_time' => $request->scheduled_time
            ]);

        return response()->json($data);
    }

    public function getSched($id){
        $data = deliveryschedule::findOrFail($id);
        return response()->json($data);
    }

}
