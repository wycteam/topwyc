<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\UserOpen;
use App\Models\ShopRate;
use App\Models\Role;
use App\Models\RoleUser;
use App\Models\User;
use App\Models\Visa\ClientService;
use App\Models\Visa\Group;
use App\Models\Visa\Package;
use App\Models\Visa\Branch;
use Response, Validator;
use App\Classes\Common;

class AgentsController extends Controller
{
    public function getTranslation() {
        return json_encode([
            'translation' => trans('cpanel/agents-middleman-page')
        ]);
    }

    public function index()
    {
        $lang = trans('cpanel/agents-middleman-page');
        $data = [
            'title' => $lang['shops'],
            'home'  => $lang['home']
        ];

        return view('cpanel.visa.agents.index', $data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $trans= trans('cpanel/agents-validation');
        $validator = Validator::make($request->all(), [
            // 'fname'                         => 'required',
            // 'lname'                         => 'required',
            // 'address'                       => 'required',
            // 'contact_number'                => 'required',
            // 'email'                         => 'required|email|unique:users,email'
            'agent' => 'required',
        ],[
            // 'fname.required'                => $trans['fname-required'],
            // 'lname.required'                => $trans['lname-required'],
            // 'address.required'              => $trans['address-required'],
            // 'contact_number.required'       => $trans['contact-number-required'],
            // 'email.required'                => $trans['email-required'],
            // 'email.email'                   => $trans['email-email'],
            // 'email.unique'                  => $trans['email-unique']
            'agent.required'                   => $trans['agent-required'],

        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();

            return $response;
        } else {
            // $user = User::create([
            //     'first_name' => $request->fname,
            //     'last_name' => $request->lname,
            //     'address' => $request->address,
            //     'contact_number' => $request->contact_number,
            //     'alternate_contact_number' => ($request->alternate_contact_number) ? $request->alternate_contact_number : null,
            //     'email' => $request->email,
            //     'middleman' => $request->middleman
            // ]);
 
            // $user->roles()->attach(18);
            // $user->roles()->attach(9);

            // if($request->agent == $request->middleman){
            //     $response['status'] = 'Failed';
            //     $response['errors'] = $trans['agent-not-same-midman'];
            // }else{
                $role = Role::where('name', 'agent')->first();
                $roleAgent = $role->id;

                $role2 = Role::where('name', 'visa-client')->first();
                $roleVisaClient = $role2->id;
                
                $checkrole1 = RoleUser::where('user_id', $request->agent)->where('role_id', $roleAgent)->first();
                $checkrole2 = RoleUser::where('user_id', $request->agent)->where('role_id', $roleVisaClient)->first();

                if($checkrole1 == null){
                    $user = User::whereId($request->agent)->first();
                    $update = $user->update([
                        'middleman' => $request->middleman
                        ]);
                
                    if($checkrole2 == null){
                        $user->roles()->attach($roleVisaClient);
                    }

                    $user->roles()->attach($roleAgent);

                    $rate = ShopRate::create([
                        'client_id' => $user->id,
                        'month' => date('m'),
                        'year' => date('Y'),
                        'rate' => 0.4,
                    ]);

                    //automatically create group for this shop
                    $groupname = 'Shop : '.$user->first_name." ".$user->last_name;
                    $group = Group::create([
                        'name' => $groupname,
                        'leader' => $user->id,
                        'address' => $user->address,
                        'tracking' => $this->generateGroupTracking(),
                        'branch_id' => $user->branch_id,
                        'is_shop' => 1,
                    ]);

                    $group->groupmember()->create([
                        'client_id' => $user->id,
                        'leader' => 1
                    ]);

                    // Action Log
                    $branchName = Branch::findOrFail($user->branch_id)->name;
                    $detail = "Created new group -> " . $groupname . ' [' . $branchName . ']';
                    Common::saveActionLogs(0, 0, $detail, $group->id);
                    // end creating group

                    $response['status'] = 'Success';
                    $response['message'] = $trans['you-added-agent'];
                }else{
                    $response['status'] = 'Failed';
                    $response['errors'] =  $trans['client-already-agent'];
                }
            // }

            return $response;
        }
    }

    // display specific agent
    public function show($id)
    {
        $role = Role::where('name', 'agent')->first();
        $roleId = $role->id;

        $agent = User::whereHas('roles', function($query) use($roleId) {
                $query->where('role_id', $roleId);
            })->findOrFail($id);

        $data = [
            'title' => 'Shop::'.$agent->first_name
        ];

        return view('cpanel.visa.agents.profile', $data);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'contact_number' => 'required',
            'email' => 'required|email|unique:users,email,'.$id
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['errors'] = $validator->errors();

            return $response;
        } else {
            User::findOrFail($id)
                ->update([
                    'first_name' => $request->name,
                    'last_name' => $request->name,
                    'address' => $request->address,
                    'contact_number' => $request->contact_number,
                    'alternate_contact_number' => ($request->alternate_contact_number) ? $request->alternate_contact_number : null,
                    'email' => $request->email,
                    'middleman' => $request->middleman
                ]);

            $response['status'] = 'Success';
            $response['message'] = 'You have successfully updated an agent.';

            return $response;
        }
    }

    public function destroy($id)
    {
       
    }

    public function getAgent($agentId) {
        $role = Role::where('name', 'agent')->first();
        $roleId = $role->id;

        $agent = User::whereHas('roles', function($query) use($roleId) {
            $query->where('role_id', $roleId);
        })->find($agentId);

        if($agent) {
            $agent->commission = $this->getAgentCommission($agentId);
            
            $rate = ShopRate::where('client_id', $agentId)->where('month',date('m'))->where('year',date('Y'))->first();
            $agent['current_rate'] = 0.4;
            if($rate){
                $agent['current_rate'] = $rate->rate;
            }

            return $agent->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_balance', 'branch', 'three_days']);
        }

        return null;
    }

    private function getAgentCommission($agentId) {
        $referredClientIds = UserOpen::where('referral_id', $agentId)->where('is_verified', 1)->pluck('client_id');

        $commission = ClientService::whereIn('client_id', $referredClientIds)
            ->where('status', 'Complete')
            ->where('active', 1)->where('detail','!=', 'Other Payment')
            ->sum('com_agent');

        return number_format($commission, 2);
    }

    public function getMiddleman($agentId) {
        $role = Role::where('name', 'agent')->first();
        $roleId = $role->id;

        $agent = User::select('middleman')->whereHas('roles', function($query) use($roleId) {
            $query->where('role_id', $roleId);
        })->find($agentId);

        if($agent && $agent->middleman) {
            return User::select('id', 'first_name', 'last_name')->find($agent->middleman)
                ->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_balance', 'branch', 'three_days']);
        }

        return null;
    }

    public function getRefferedClients($agentId) {
        $userOpen = UserOpen::with('user')->where('referral_id', $agentId)->where('is_verified', 1)->get()
            ->makeHidden(['user.full_name', 'user.avatar', 'user.permissions', 'user.access_control', 'user.binded', 'user.unread_notif', 'user.group_binded', 'user.document_receive', 'user.is_leader', 'user.total_deposit', 'user.total_discount', 'user.total_refund', 'user.total_payment', 'user.total_cost', 'user.total_balance', 'user.branch', 'user.three_days']);

        $data = [];
        foreach($userOpen as $key => $u) {
            $data[$key]['id'] = $u->user->id;
            $data[$key]['full_name'] = $u->user->first_name . ' ' . $u->user->last_name;
            $data[$key]['total_cost'] = $this->getClientCashFlow($u->user->id)['total'];
            $data[$key]['points_earned'] = $this->getClientCashFlow($u->user->id)['points'];
            $data[$key]['agent_points_earned'] = $this->getClientCashFlow($u->user->id)['agent_points'];
            $data[$key]['shop_points_earned'] = $this->getClientCashFlow($u->user->id)['shop_points'];
            $data[$key]['services'] = $this->referredClientServies($u->client_id);
        }

        return $data;
    }

    public function getRefferedClientsViaMiddleman(Request $request) {
        $userOpen = UserOpen::with('user')->whereIn('referral_id', $request->agentsId)->where('is_verified', 1)->get()
            ->makeHidden(['user.full_name', 'user.avatar', 'user.permissions', 'user.access_control', 'user.binded', 'user.unread_notif', 'user.group_binded', 'user.document_receive', 'user.is_leader', 'user.total_deposit', 'user.total_discount', 'user.total_refund', 'user.total_payment', 'user.total_cost', 'user.total_balance', 'user.branch', 'user.three_days']);


        $data = [];
        foreach($userOpen as $key => $u) {
            $data[$key]['id'] = $u->user->id;
            $data[$key]['full_name'] = $u->user->first_name . ' ' . $u->user->last_name;
            $data[$key]['total_cost'] = $this->getClientCashFlow($u->user->id)['total'];
            $data[$key]['points_earned'] = $this->getClientCashFlow($u->user->id)['points'];
            $data[$key]['services'] = $this->referredClientServies($u->client_id);
        }

        return $data;
    }

    private function referredClientServies($clientId) {
        return ClientService::where('client_id', $clientId)->where('com_client','>',0)
            ->orderBy('id', 'desc')
            ->get()
            ->makeHidden(['detail_cn', 'docs_needed', 'docs_optional', 'group_binded', 'parent_id']);
    }

    // Lists all users with 'agent' role
    public function getAllAgents(){
        $role = Role::where('name','agent')->first();
        $roleUsers = RoleUser::where('role_id', $role->id)->with('user')->orderBy('user_id')->get();
        return $roleUsers;
    }



    private function getTotalCashflow($id) {
        $referredClientIds = UserOpen::where('referral_id', $id)->where('is_verified', 1)->pluck('client_id');

        $getClientServices = ClientService::whereIn('client_id', $referredClientIds)
            ->where('status', 'Complete')
            ->where('detail', '!=', 'Other Payment')
            ->where('active', 1)
            ->get();
        $totalCost =  $getClientServices->sum('cost');
        $totalCharge = $getClientServices->sum('charge');
        $totalTip = $getClientServices->sum('tip');

        return $totalCost + $totalCharge + $totalTip;
    }


    public function getAgents() {
        $agents = $this->getAllAgents(); 

        foreach($agents as $key => $agent) {
            $referrals = $this->getreferrals($agent->user_id);
            $midman = $this->getMidMan($agent->user->middleman);
            $total_cashflow = $this->getTotalCashflow($agent->user_id);

            $agents[$key]['midman'] = $midman;
            $agents[$key]['ref'] = $referrals;
            $agents[$key]['total_cashflow'] = $total_cashflow;
        }

        return json_encode($agents);
    }

    public function getMiddlemen(Request $request) {
        $role = Role::where('name', 'visa-client')->first();
        $roleVisaClient = $role->id;

        $query = User::select('id', 'first_name', 'last_name')
            ->whereHas('roles', function($query) use($roleVisaClient){
                $query->where('role_id', $roleVisaClient); // visa-client
            });

        if($request->q) {
            $q = $request->q;

            $query = $query->where('id', $q)->orWhere(function($query2) use($q) {
                $query2->where('first_name', $q)->orWhere('last_name', $q);
            });
        }

        $query = $query->get()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_balance', 'branch', 'three_days']);

        $json = [];
        foreach($query as $q){
              $json[] = array(
                  'id' => $q->id,
                  'name' => "[".$q->id."]"." ".$q->first_name." ".$q->last_name,
              );
        
        }
        return json_encode($json);
    }

    public function getNotAgent(Request $request) {
        $role = Role::where('name', 'visa-client')->first();
        $roleVisaClient = $role->id;

        $query = User::select('id', 'first_name', 'last_name')
            ->whereHas('roles', function($query) use($roleVisaClient){
               $query->where(['role_id', $roleVisaClient]);
                // visa-client
            });

        if($request->q) {
            $q = $request->q;

            $query = $query->where('id', $q)->orWhere(function($query2) use($q) {
                $query2->where('first_name', $q)->orWhere('last_name', $q);
            });
        }

        $query = $query->get()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_balance', 'branch', 'three_days']);

        return $query;
    }

    //MIDDLEMAN
    public function showMidMan($agent, $midman){
        $midman = User::where('id', $midman)->first();
        $agents = $this->getMidAgents($midman->id);
        $commi = $this->getMidmanCommi($midman->id);
        $data = [
            'title' => 'Middleman::'.$midman->full_name,
        ];

        session()->flash('data', array('midman'=>$midman, 'agents' => $agents, 'commi' => $commi));

        return view('cpanel.visa.agents.middle-man.profile', $data);;
    }

    public function getMidMan($id){
        $role = Role::where('name','visa-client')->first();
        $roleId = $role->id;
        $agent = User::whereHas('roles', function ($query) use($roleId){
            $query->where('role_id', $roleId);
        })->where('id', $id)->first();
        return json_decode($agent);
    }

    public function getMidAgents($id){
        $role = Role::where('name','agent')->first();
        $roleId = $role->id;
        $agents = User::whereHas('roles', function ($query) use($roleId){
            $query->where('role_id', $roleId);
        })->where('middleman', $id)->get();

        foreach($agents as $key => $agent) {
            $referrals = $this->getreferrals($agent->id);
            $cashFlow = $this->getTotalCashflow($agent->id);

            $agents[$key]['ref'] = count($referrals);
            $agents[$key]['cashflow'] = number_format($cashFlow, 2);

        }
        return $agents;
    }

    public function getClientCashFlow($id){
        $getClientServices = ClientService::where('client_id',$id)
                            ->where('status', 'Complete')
                            ->where('detail','!=', 'Other Payment')
                            ->where('com_agent','>',0)
                            ->where('active', 1)
                            ->get();

        $totalCost =  $getClientServices->sum('cost');
        $totalCharge = $getClientServices->sum('charge');
        $totalTip = $getClientServices->sum('tip');
        $total = $totalCost + $totalCharge + $totalTip;
        $points = $getClientServices->sum('com_client');
        $agent_points = $getClientServices->sum('com_middleman');
        $shop_points = $getClientServices->sum('com_agent');

        $data = [
            'total' => number_format($total, 2),
            'services' => $getClientServices,
            'points' => number_format($points,2),
            'agent_points' => number_format($agent_points,2),
            'shop_points' => number_format($shop_points,2),
        ];
        return $data;
    }

        // referrals
    public function getreferrals($id){
        $reff = UserOpen::with('user')->where('referral_id',$id)->where('is_verified', 1)->get();
        //get also total cost below
        foreach($reff as $key => $r) {
            $cashFlow = $this->getClientCashFlow($r->client_id);
            $reff[$key]['cashflow'] = $cashFlow;
        }
        return $reff;
    }

    public function getAllReferrals($midman){
        $role = Role::where('name','agent')->first();
        $roleId = $role->id;
        $agents = User::whereHas('roles', function ($query) use($roleId){
            $query->where('role_id', $roleId);
        })->where('middleman', $midman)->get();

        $allreferrals[] ='';

        foreach($agents as $key => $agent) {
            $referrals = UserOpen::with('user')->where('referral_id', $agent->id)->where('is_verified', 1)->get();

            foreach($referrals as $key => $r) {
                $cashFlow = $this->getClientCashFlow($r->client_id);
                $referrals[$key]['cashflow'] =  $cashFlow;              
               if (empty($allreferrals)){
                    $allreferrals = $r;
                }else{
                    array_push($allreferrals, $r);
      
                }
            }
            
        }

        return $allreferrals;
    }

    public function getMidmanCommi($id){
        $role = Role::where('name','agent')->first();
        $roleId = $role->id;
        $agents = User::whereHas('roles', function ($query) use($roleId){
            $query->where('role_id', $roleId);
        })->where('middleman', $id)->get();

        $commi=0;
        
        foreach($agents as $key => $agent) {
            $referrals = UserOpen::with('user')->where('referral_id', $agent->id)->where('is_verified', 1)->get();
            foreach($referrals as $key => $r) {                       
                $getClientServices = ClientService::where('client_id',$r->client_id)
                            ->where('status', 'Complete')
                            ->where('detail','!=', 'Other Payment')
                            ->where('active', 1)
                            ->get();
                $sum = $getClientServices->sum('com_middleman');
                $commi += $sum;
            }

        }

        return number_format($commi,2);
    }


    private function generateGroupTracking() {
        Repack:
        $packId = 'GL' . $this->generateRandomString();
        $checkPackage = $this->checkPackage($packId);
        if($checkPackage > 0) :
            goto Repack;
        endif;
        return $packId;
    }

    private function checkPackage($packId){
        return Package::where('tracking', $packId)->count();
    }

    private function generateRandomString($length = 7) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i=0; $i<$length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
