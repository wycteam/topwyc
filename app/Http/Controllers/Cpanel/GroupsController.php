<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\Visa\Group;
use App\Models\Visa\Branch;
use App\Models\Visa\GroupMember;
use App\Models\Visa\Package;
// use App\Models\Visa\PackagesServices;
// use App\Models\Visa\Deposit;
// use App\Models\Visa\Refund;
// use App\Models\Visa\Payment;
// use App\Models\Visa\Discount;
use App\Models\Visa\ClientService;
use App\Models\Visa\ServiceProfileCost;
use App\Models\Visa\Minimized\ClientServiceMini;
use App\Models\Visa\Service;
use App\Models\Visa\ServiceDocuments;
use App\Models\Visa\ClientTransactions;
// use App\Models\Address;
use App\Models\User;

use App\Classes\Common;
use DB;

class GroupsController extends Controller
{

    public function getTranslation() {
      
        return json_encode([
            'translation' => trans('cpanel/groups-page')
        ]);
    }

    public function editAddressTranslation() {
        return json_encode([
            'translation' => trans('cpanel/edit-address-component')
        ]);
    }

    public function addNewMemberTranslation() {
        return json_encode([
            'translation' => trans('cpanel/add-new-member-component')
        ]);
    }

    public function addNewServiceTranslation() {
        return json_encode([
            'translation' => trans('cpanel/add-new-service-component')
        ]);
    }

    public function editServiceTranslation() {
        return json_encode([
            'translation' => trans('cpanel/edit-service-group-component')
        ]);
    }

    public function addFundTranslation() {
        return json_encode([
            'translation' => trans('cpanel/add-fund-component')
        ]);
    }

    public function transferTranslation() {
        return json_encode([
            'translation' => trans('cpanel/transfer-component')
        ]);
    }

    public function groupMemberTypeTranslation() {
        return json_encode([
            'translation' => trans('cpanel/group-member-type-component')
        ]);
    }

    public function deleteMemberTranslation() {
        return json_encode([
            'translation' => trans('cpanel/delete-member-component')
        ]);
    }

    public function clientServicesTranslation() {
        return json_encode([
            'translation' => trans('cpanel/client-services-component')
        ]);
    }

    public function groupMembersTranslation() {
        return json_encode([
            'translation' => trans('cpanel/group-members-component')
        ]);
    }

	public function index() {
        $title = trans('cpanel/groups-page');
        $ajax = $this->load_group_list();
        $bal = Group::where('balance','<',0)->sum('balance');
        $col = Group::where('collectable','<',0)->sum('collectable');
        $data = array(
            'title' => $title['groups'],
            // 'collect' => 0,
            'balance' => Common::formatMoney($bal*1),
            'collect' => Common::formatMoney($col*1),
        );

		return view('cpanel.visa.groups.index', $data);
	}

    public function allBalance() {
        $title = trans('cpanel/groups-page');
        //$ajax = $this->load_group_list();
        //$bal = Group::where('balance','<',0)->sum('balance');
        //$col = Group::where('collectable','<',0)->sum('collectable');
        $data = array(
            'title' => 'All Balance',
            // 'collect' => 0,
            //'balance' => Common::formatMoney($bal*1),
            //'collect' => Common::formatMoney($col*1),
        );

        return view('cpanel.visa.groups.all-balance', $data);
    }

	public function show($id) {
		$grp = Group::findOrFail($id);

		$data = [
			'title' => strtoupper($grp->name)
		];

		return view('cpanel.visa.groups.show', $data);
	}

	public function load_group_list(Request $request = null){
        $myfile = fopen(storage_path('/app/public/data/Group.txt'), 'w') or die('Unable to open file!');
        $txt = "{\n";
        fwrite($myfile, $txt);
        $txt = '"data":[';
        fwrite($myfile, $txt);
        $txt = "\n";
        fwrite($myfile, $txt);
        $count = 0;

        // $records = DB::connection('visa')
        //      ->table('groups as a')
        //      ->select(DB::raw('a.*, b.*'))
        //      ->leftjoin(DB::raw('(select * from wycgroup.users) as b'), 'b.id', '=', 'a.leader')
        //      ->get();

        $records = DB::connection('visa')
            ->table('groups as a')
            ->select(DB::raw('
                a.*,a.id as gr_id,a.balance as gr_bal, a.collectable as gr_col, b.*,
                total6.dates,total6.dateshid,
                srv.sdates,srv.sdateshid
                '))
                ->leftjoin(DB::raw('(select * from wycgroup.users) as b'), 'b.id', '=', 'a.leader')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as dates,
                        x.group_id,date_format(max(x.dates),"%Y-%m-%d %H:%i:%s") as dateshid
                        from( SELECT STR_TO_DATE(log_date, "%M %e, %Y, %l:%i %p") as dates,
                            group_id, status
                            FROM packages
                            ORDER BY dates desc
                        ) as x
                        group by x.group_id) as total6'),
                    'total6.group_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as sdates,cs.client_id,
                        date_format(max(cs.servdates),"%Y:%m:%d") as sdateshid,cs.group_id
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.group_id) as srv'),
                    'srv.group_id', '=', 'a.id');

        if($request && $request->branch != 0) {
            $records = $records->where('a.branch_id', $request->branch)->get();
        } else {
            $records = $records->get();
        }

        $bal = 0;
        $collect = 0;
        foreach ($records as $value) {

            $id = $value->gr_id;
                // $total_balance = (
                //     (
                //         $this->getGroupDeposit($id)
                //         + $this->getGroupPayment($id)
                //         +$this->getGroupDiscount($id)
                //     )
                //     - (
                //         $this->getGroupRefund($id)
                //         + $this->getGroupCost($id)
                //     )
                // );
                $total_balance = $value->gr_bal;
                $col_balance = 0;
                if($total_balance<0){
                // $col_balance = (
                //     (
                //         $this->getGroupDeposit($id)
                //         + $this->getGroupPayment($id)
                //         +$this->getGroupDiscount($id)
                //     )
                //     - (
                //         $this->getGroupRefund($id)
                //         + ($this->getCompleteCost($id))
                //     )
                // );
                $col_balance = $value->gr_col;
                if($col_balance > 0){
                    $col_balance = 0;
                }
                else{
                    $collect += $col_balance;
                }

                }

                if($total_balance<0){
                    $bal += $total_balance;
                }
            $count++;
            $txt = "[";
            fwrite($myfile, $txt);
            if($value->risk == 'High-Risk'){
                $txt = '"<span style=\"color:red;\" id=\"cl_'.$value->gr_id.'\"><b>'.ucwords($value->name).'</b></span>",';
            }
            else{
                $txt = '"<span id=\"cl_'.$value->gr_id.'\"><b>'.ucwords($value->name).'</b></span>",';
            }

            fwrite($myfile, $txt);
            $txt = '"'.Common::formatMoney($total_balance*1).'",';
            fwrite($myfile, $txt);
            $txt = '"'.(Common::formatMoney($col_balance*1)).'",';

            //Group::where('id', $value->gr_id)
                //->update(['balance' => $total_balance, 'collectable' => $col_balance]);

            fwrite($myfile, $txt);
            $txt = '"'.ucwords($value->first_name).' '.ucwords($value->last_name).'",';
            fwrite($myfile, $txt);
            // $txt = '"'.ucwords($value->dates).'",';
            $txt = '"<span style=\"display:none;\">'.$value->dateshid.'--</span>'.ucwords($value->dates).'",';
            fwrite($myfile, $txt);
            // $txt = '"'.ucwords($value->sdates).'",';
            $txt = '"<span style=\"display:none;\">'.$value->sdateshid.'--</span>'.ucwords($value->sdates).'",';
            fwrite($myfile, $txt);
            $txt = '"<center><a href=\"#\" id=\"cs_'.$value->gr_id.'\" client-id=\"'.$value->gr_id.'\" client-status=\"'.$value->risk.'\" class=\"changeStatus\" data-toggle=\"tooltip\" title=\"Change Status\" style=\"margin-right:8px;\"><i class=\"fa fa-bell fa-1x\"></i></a><a href=\"#\"  data-toggle=\"tooltip\" title=\"Export to Excel\" class=\"exportExcel\" group-id=\"'.$value->gr_id.'\" style=\"margin-right:8px;\"><i class=\"fa fa-file-text fa-1x\" ></i></a><a href=\"/visa/group/'.$value->gr_id.'\" target=\"_blank\" data-toggle=\"tooltip\" title=\"View Group\"><i class=\"fa fa-arrow-right fa-1x\"></i></a></center>"';

            //<a href=\"/visa/report/get-group-summary/'.$value->gr_id.'\" class=\"groupToExcel\" group-id=\"'.$value->gr_id.'\" style=\"margin-right:8px;\" data-toggle=\"tooltip\" title=\"Export to Excel\"><i class=\"fa fa-file-text fa-1x\"></i></a>

            fwrite($myfile, $txt);
            if(count($records)==$count){
                $txt = "]\n";
            }else{
                $txt = "],\n";
            }

            fwrite($myfile, $txt);
        }
        $txt = "\n]\n}";
        fwrite($myfile, $txt);
        fclose($myfile);
        return array($count, $bal, $collect);
    }

    public function changeStatus($group_id,$stat){

        $grp = Group::where('id',$group_id)->first();
        if($grp){
            $grp->risk = $stat;
            $grp->save();
        }

        return $grp;
    }

	public function getGroupDeposit($group_id){
		return ClientTransactions::where('group_id',$group_id)->where('type','Deposit')->sum('amount');
	}

	public function getGroupDiscount($group_id){
		return ClientTransactions::where('group_id',$group_id)->where('type','Discount')->sum('amount');

	}

	public function getGroupPayment($group_id){
		return ClientTransactions::where('group_id',$group_id)->where('type','Payment')->sum('amount');
	}

	public function getGroupRefund($group_id){
		return ClientTransactions::where('group_id',$group_id)->where('type','Refund')->sum('amount');
	}

    public function getCompleteCost($group_id){
        $group_cost = ClientService::select(DB::raw('sum(cost+charge+tip) as total_cost'))
            ->where('group_id', '=', $group_id)
            ->where('active', '=', 1)
            ->where('status', '=', 'complete')
            ->first();

        return $group_cost->total_cost;
    }
	public function getGroupCost($group_id){
        $group_cost = ClientService::where('group_id', $group_id)
            ->where('active', 1)
            ->value(DB::raw("SUM(cost + charge + tip)"));

        return $group_cost;
    }

    public function getGroupBalance($group_id){
		return (
            (
                $this->getGroupDeposit($group_id)
                + $this->getGroupPayment($group_id)
                +$this->getGroupDiscount($group_id)
            )
            -
            (
                $this->getGroupRefund($group_id)
                + $this->getGroupCost($group_id)
            )
        );
	}

    private function uniqueDocumentsOnHand($clientId) {
        $clientServices = ClientService::has('documentLogs')
            ->with(['documentLogs' => function($query) {
                $query->orderBy('id', 'desc');
            }])
            ->where('active', 1)
            ->where('client_id', $clientId)
            ->get()
            ->makeHidden([
                'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
            ]);

        $documentsOnHand = [];

        foreach($clientServices as $clientService) {
            if( $clientService->documentLogs[0]->documents_on_hand  ) {
                $documentsOnHand[] = $clientService->documentLogs[0]->documents_on_hand;
            }
        }

        $docsIdArr = explode(',', implode(',', $documentsOnHand));

        return DB::connection('visa')->table('service_documents')->where('is_unique', 1)->whereIn('id', $docsIdArr)->pluck('id');
    }

    public function packagesServices($groupId) {
        // return GroupMember::with(['client.packages' => function($query) use($groupId) {
        //     $query->where('group_id', $groupId);
        // }])->where('group_id', $groupId)->get();
        $member_count_null = GroupMember::where('group_id', $groupId)->where('client',null)->count();

        if($member_count_null > 0){  
            $arr = [];
            $group_members = GroupMember::where('group_id', $groupId)->get();
            //$arr[] = $group_members;
            $ctr=0;
            foreach($group_members as $gm){
                $usr =  User::where('id',$gm->client_id)->select('id','first_name','last_name')->limit(1)->get()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
                if($usr){
                    //$usr = $usr->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
                    $arr[$ctr] = $gm;
                    $arr[$ctr]['client'] =$usr[0];
                    $arr[$ctr]['client']['packages'] = Package::where('client_id',$gm->client_id)->where('group_id',$gm->group_id)->get();
                    $arr[$ctr]['client']['uniqueDocumentsOnHand'] = $this->uniqueDocumentsOnHand($gm->client_id);
                    $ctr++;
                }
                
            }
            return $arr;
        }

       $ps = GroupMember::where('group_id', $groupId)->get();

       foreach($ps as $p){
            $p->client = json_decode( $p->client, true );
       }

       return $ps;
    }

    public function editServices(Request $request, $groupId) {
        $pattern = ClientService::where('group_id', $groupId)
            ->where('client_id', $request->clientId)
            ->where('id', $request->cid)
            ->select('charge', 'cost', 'status', 'active','service_date','tip')
            ->first();

        return ClientService::with('user')
            ->with(['discount' => function($query) use($groupId) {
                $query->where('group_id', $groupId);
            }])
            ->where('group_id', $groupId)
            ->where('service_id', $request->serviceId)
            ->where('charge', $pattern->charge)
            ->where('cost', $pattern->cost)
            ->where('tip', $pattern->tip)
            ->where('status', $pattern->status)
            ->where('active', $pattern->active)
            //->where('service_date', $pattern->service_date)
            ->groupBy('client_id')
            ->get();
    }

    public function serviceDocs($id) {
        $docs = explode(",", $id);
        
        return DB::connection('visa')->table('service_documents')->orderBy('title')->whereIn('id',$docs)->get();
    }

    public function serviceDocsIndex() {
        return ServiceDocuments::orderBy('title')->get();
    }

    public function typeahead(){

        $serts = $_GET['query'];
        $users = '';
        if(preg_match('/\s/',$serts)){
            $q = explode(" ", $serts);
            $q1 = '';
            $q2 = '';
            $spaces = substr_count($serts, ' ');
            if($spaces == 2){
                $q1 = $q[0]." ".$q[1];
                $q2 = $q[2];
            }
            if($spaces == 1){
                $q1 = $q[0];
                $q2 = $q[1];
            }
            $users = DB::connection()
            ->table('users as a')
            ->select(DB::raw('
                a.id,a.first_name,a.last_name,a.branch_id,a.date_register'))
                // ->orwhere('a.id','LIKE', '%' . $serts .'%')
                // ->orwhere('first_name','=',$serts)
                // ->orwhere('last_name','=',$serts)
                ->orwhere(function ($query) use($q1,$q2) {
                        $query->where('first_name', '=', $q1)
                              ->Where('last_name', '=', $q2);
                    })->orwhere(function ($query) use($q1,$q2) {
                        $query->where('last_name', '=', $q1)
                              ->Where('first_name', '=', $q2);
                    })
                ->pluck('id');
        }
         else{
            $users = DB::connection()
            ->table('users as a')
            ->select(DB::raw('
                a.id,a.first_name,a.last_name,a.branch_id,a.date_register'))
                // ->orwhere('a.id','LIKE', '%' . $serts .'%')
                ->orwhere('first_name','=',$serts)
                ->orwhere('last_name','=',$serts)
                ->pluck('id');
        }

        $getLeaderIds = GroupMember::where('leader',1)->pluck('client_id');
        $cids = User::whereIn('id',$getLeaderIds)->where("contact_number",'LIKE', '%' . $serts .'%')->pluck('id');
            $grps = DB::connection('visa')
            ->table('groups as a')
            ->select(DB::raw('
                a.id,a.name,a.branch_id'))
                //->orwhere('a.id','LIKE', '%' . $serts .'%')
                ->orwhere('name','LIKE', '%' . $serts .'%')
                ->orwhereIn('leader',$cids)
                ->orwhereIn('leader',$users)
                ->get();


        $json = [];
        foreach($grps as $p){
          $br = Branch::where('id',$p->branch_id)->first()->name;
          $json[] = array(
              'id' => $p->id,
              'name' => $p->name." [".$br."]",
          );
        }
        return json_encode($json);
    }

    public function groupMembers($groupId) {
        // return GroupMember::with('client')->where('group_id', $groupId)->get();
        $gm = DB::connection('visa')
                ->table('group_members as g')
                ->select(DB::raw('g.*, u.id, u.first_name, u.last_name, u.date_register'))
                ->leftjoin(DB::raw('(select * from wycgroup.users) as u'),'u.id','=','g.client_id')
                //->leftjoin(DB::raw('(select amount,group_id from client_transactions) as ct'),'ct.group_id','=','g.group_id')
                ->where('g.group_id', $groupId)
                ->get();

        foreach($gm as $groupMember) {
            $clientId = $groupMember->client_id;

            $discount = ClientTransactions::whereHas('service', function($query) use($clientId) {
                    $query->where('client_id', $clientId);
                })
                ->where('group_id', $groupId)
                ->where('type', 'Discount')
                ->where('deleted_at', null)
                ->sum('amount');

            $groupMember->discount = $discount;
        }

        return $gm;

    }

    public function groupMemberServices($groupId, $memberId) {
        $serv = DB::connection('visa')->table('client_services')->where('client_services.group_id', $groupId)->where('client_services.client_id', $memberId)->orderBy('client_services.id','Desc')->get();
       
        foreach($serv as $s) {
            $s->user =  DB::table('users')->where('id',$s->client_id)->get();
            $s->discounts = DB::connection('visa')->table('client_transactions')->where('service_id',$s->id)->where('type','Discount')->get();
        }

        return $serv;
        //return ClientService::with('user', 'discounts')->where('group_id', $groupId)->where('client_id', $memberId)->orderBy('id','Desc')->get();
        //return DB::connection('visa')->table('client_services')->leftJoin('wycgroup.users', 'wycgroup.users.id', '=', 'client_services.client_id')->leftJoin('client_transactions','client_transactions.service_id', '=', 'client_services.service_id')->where('client_services.group_id', $groupId)->where('client_services.client_id', $memberId)->orderBy('client_services.id','Desc')->get();
    }

    public function groupSelectedServices($clientServiceId){
        $serv = DB::connection('visa')->table('client_services')->where('id', $clientServiceId)->get();
        foreach($serv as $s) {
            $s->user =  DB::table('users')->where('id',$s->client_id)->get();
            $s->discounts = DB::connection('visa')->table('client_transactions')->where('service_id',$clientServiceId)->where('type','Discount')->get();
        }
        return $serv;
        //\Log::info(json_decode($serv));
    }
    public function checkCommissionApplication(Request $request){
        $client_flag = true;
        $agent_flag = true;
        
        foreach ($request->services as $key => $value) {
            if($request->profile_id>0){
                $serv = DB::connection('visa')->table('service_profile_cost')->where('profile_id',$request->profile_id)->where('branch_id',$request->branch_id)->where('service_id',$value)->first();
                if($serv->com_client > 1 || $serv->com_agent > 1){
                    if($serv){
                        if($request->client_id>0){
                            if($serv->com_client<1){
                                $client_flag = false;
                            }
                        }else{
                            if($serv->com_client>0){
                                $client_flag = false;
                            }
                        }
                        if($request->agent_id>0){
                            if($serv->com_agent<1){
                                $agent_flag = false;
                            }
                        }else{
                            if($serv->com_agent>0){
                                $agent_flag = false;
        
                            }
                        }
                    }else{
                        $s = new ServiceProfileCost;
                        $s->service_id = $value;
                        $s->profile_id = $request->profile_id;
                        $s->branch_id = $request->branch_id;
                        $s->cost = 0.00;
                        $s->charge = 0.00;
                        $s->tip = 0.00;
                        $s->com_client = 0.00;
                        $s->com_agent = 0.00;
                        $s->save();
    
                        $serv = DB::connection('visa')->table('service_profile_cost')->where('profile_id',$request->profile_id)->where('branch_id',$request->branch_id)->where('service_id',$value)->first();
                        if($serv->com_client > 1 || $serv->com_agent > 1){
                            if($request->client_id>0){
                                if($serv->com_client<1){
                                    $client_flag = false;
                                }
                            }else{
                                if($serv->com_client>0){
                                    $client_flag = false;
                                }
                            }
                            if($request->agent_id>0){
                                if($serv->com_agent<1){
                                    $agent_flag = false;
                                }
                            }else{
                                if($serv->com_agent>0){
                                    $agent_flag = false;
                                }
                            }
                        }
                    }
                }
               

            }else{
                if($request->branch_id>1){
                    $serv = DB::connection('visa')->table('service_branch_cost')->where('branch_id',$request->branch_id)->where('service_id',$value)->first();
                    if($request->client_id>0){
                        if($serv->com_client<1){
                            $client_flag = false;
                        }
                    }else{
                        if($serv->com_client>0){
                            $client_flag = false;
                        }
                    }
                    if($request->agent_id>0){
                        if($serv->com_agent<1){
                            $agent_flag = false;
                        }
                    }else{
                        if($serv->com_agent>0){
                            $agent_flag = false;
                        }
                    }
                }else{
                    $serv = DB::connection('visa')->table('services')->where('branch_id',$request->branch_id)->where('id',$value)->first();
                    if($request->client_id>0){
                        if($serv->com_client<1){
                           
                            $client_flag = false;
                        }
                    }else{
                        if($serv->com_client>0){
                            $client_flag = false;
                        }
                    }
                    if($request->agent_id>0){
                        if($serv->com_agent<1){
                            $agent_flag = false;
                        }
                    }else{
                        if($serv->com_agent>0){
                            $agent_flag = false;
                        }
                    }
                }
                
            }
        }
       return array('client_flag'=>$client_flag, 'agent_flag'=>$agent_flag);
    }
    public function updateServices(Request $request){
       
        // foreach ($request->ids as $key => $value) {
        //     $serv = ClientService::where('id',$value)->first();

        //     \Log::info($request->all());
        //     if($request->charge!=NULL && $serv->charge!=$request->charge){
        //         DB::connection('visa')->table('client_services')
        //         ->where('id', $value)
        //         ->update(['charge' =>$request->charge]);
        //     }
        //     if($request->tip!=null && floatval($serv->tip)!=$request->tip){
        //         DB::connection('visa')->table('client_services')
        //         ->where('id', $value)
        //         ->update(['tip' =>$request->tip]);
        //     }
        //     if($request->cost!=null && floatval($serv->cost)!=$request->cost){
        //         DB::connection('visa')->table('client_services')
        //         ->where('id', $value)
        //         ->update(['cost' =>$request->cost]);
        //     }
        //     if($request->status!=null && $serv->status!=$request->status){
        //         DB::connection('visa')->table('client_services')
        //         ->where('id', $value)
        //         ->update(['status' =>$request->status]);
        //     }
        //     if($request->active!=null && $serv->active!=$request->active){
        //         DB::connection('visa')->table('client_services')
        //         ->where('id', $value)
        //         ->update(['active' =>$request->active]);
        //     }
        //     if($request->discount!=null){
        //         $dis =  DB::connection('visa')->table('client_transactions')
        //         ->where('service_id', $value)->get();
        //         //\Log::info($dis[0]->amount);
        //         if(floatval($dis[0]->amount)!=$request->discount){
        //             DB::connection('visa')->table('client_transactions')
        //             ->where('service_id', $value)
        //             ->update(['amount' =>$request->discount]);
        //         }
              
        //     }
            
        // }
       
    }

}
