<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Visa\Synchronization;
use App\Models\Visa\Updates;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class SynchronizationController extends Controller
{

	public static function update($module, $dateLastModified) {
		Synchronization::where('module', $module)->update(['data_last_modified' => $dateLastModified]);
	}

	public static function insertUpdate($client_id, $dateLastModified) {
		$up = new Updates();
        $up->client_id = $client_id;
        $up->date_modified = $dateLastModified;
        $up->save();
	}

}