<?php

namespace App\Http\Controllers\Cpanel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Visa\Finance as Finance;
use App\Models\Visa\Initials;
use App\Models\Visa\ClientTransactions as Trans;
use App\Models\Visa\TransferBalance;
use Auth;
use Storage;
use Carbon\Carbon;
class NewFinancingController extends Controller
{

    public function getTranslation() {
      return json_encode([
        'translation' => trans('cpanel/daily-cost-page')
      ]);
    }

    public function index(Request $request){
      //check if the latest input month is same as month now

      //dd($request->input('branch'));
      $columns = ['trans_desc', 'cat_type', 'cat_storage','cash_client_depo_payment','cash_client_refund','cash_client_process_budget_return','cash_process_cost','borrowed_process_cost','cash_admin_budget_return','borrowed_admin_cost','cash_admin_cost','	bank_client_depo_payment','bank_cost','cash_balance','bank_balance','	postdated_checks','cost_other','deposit_other', 'created_at', 'updated_at','cost_type','additional_budget'];
          $length = $request->input('length');
          $column = $request->input('column'); //Index
          $dir = $request->input('dir');
          $searchValue = $request->input('search');
          //dd($request->input('dateSelector'));
          $dateSelector = Carbon::parse($request->input('dateSelector').'/01')->toDateTimeString();
          $now = Carbon::now();
          // $counter = DB::connection('visa')
          // ->table('financing2')
          // ->select(DB::raw('id,trans_desc, cat_type, cat_storage, cash_client_depo_payment, cash_client_refund, cash_client_process_budget_return, cash_process_cost, borrowed_process_cost, cash_admin_budget_return, borrowed_admin_cost, cash_admin_cost, bank_client_depo_payment, bank_cost, cash_balance, bank_balance, postdated_checks,cost_other, deposit_other, created_at, updated_at, cost_type, additional_budget'))
          // ->where('branch_id',$request->input('branch'))->whereRaw('MONTH(created_at) = MONTH("'.$dateSelector.'")')->orderBy('created_at', $dir)->count();
          $checkInitial = DB::connection('visa')->table('financing2')->select(DB::raw('id,trans_desc, cat_type, cat_storage, cash_client_depo_payment, cash_client_refund, cash_client_process_budget_return, cash_process_cost, borrowed_process_cost, cash_admin_budget_return, borrowed_admin_cost, cash_admin_cost, bank_client_depo_payment, bank_cost, cash_balance, bank_balance, postdated_checks,cost_other, deposit_other, created_at, updated_at, cost_type, additional_budget'))->where('cat_type','initial')->where('branch_id',$request->input('branch'))->whereRaw('MONTH(created_at) = MONTH("'.$dateSelector.'")')->where('deleted_at',null)->orderBy('created_at', $dir)->count();
          $checkData = DB::connection('visa')->table('financing2')->where('branch_id',$request->input('branch'))->count();
          $month = Carbon::parse($dateSelector);
          //$checkInitial = 0;
          if(($checkInitial==0 && $checkData>0) && $now->month==$month->month){
            $this->fixInitial($request->input('branch'),0);
          }
            //$checkInitial = DB::connection('visa')->table('financing2')->select(DB::raw('id,trans_desc, cat_type, cat_storage, cash_client_depo_payment, cash_client_refund, cash_client_process_budget_return, cash_process_cost, borrowed_process_cost, cash_admin_budget_return, borrowed_admin_cost, cash_admin_cost, bank_client_depo_payment, bank_cost, cash_balance, bank_balance, postdated_checks,cost_other, deposit_other, created_at, updated_at, cost_type, additional_budget'))->where('cat_type','initial')->where('branch_id',$request->input('branch'))->whereRaw('MONTH(created_at) = MONTH("'.$dateSelector.'")')->orderBy('created_at', $dir)->count();

            $query = DB::connection('visa')->table('financing2')->select(DB::raw('id,trans_desc, cat_type, cat_storage, cash_client_depo_payment, cash_client_refund, cash_client_process_budget_return, cash_process_cost, borrowed_process_cost, cash_admin_budget_return, borrowed_admin_cost, cash_admin_cost, bank_client_depo_payment, bank_cost, cash_balance, bank_balance, postdated_checks,cost_other, deposit_other, created_at, updated_at, cost_type, additional_budget, storage_type, metrobank, securitybank, aub, eastwest'))->where('branch_id',$request->input('branch'))->whereRaw('MONTH(created_at) = MONTH("'.$dateSelector.'")')->where('deleted_at',null)->orderBy('created_at', $dir);

          //$query = DB::connection('visa')->table('financing2')->select(DB::raw('id,trans_desc, cat_type, cat_storage, cash_client_depo_payment, cash_client_refund, cash_client_process_budget_return, cash_process_cost, borrowed_process_cost, cash_admin_budget_return, borrowed_admin_cost, cash_admin_cost, bank_client_depo_payment, bank_cost, cash_balance, bank_balance, postdated_checks,cost_other, deposit_other, created_at, updated_at, cost_type, additional_budget'))->where('branch_id',$request->input('branch'))->whereRaw('MONTH(created_at) = MONTH(NOW())')->orderBy('id', $dir);
          if ($searchValue) {
              $query->where(function($query) use ($searchValue) {
                  $query->where('trans_desc', 'like', '%' . $searchValue . '%')
                  ->orWhere('cat_type', 'like', '%' . $searchValue . '%')
                  ->orWhere('cat_storage', 'like', '%' . $searchValue . '%')
                  ->orWhere('cash_client_depo_payment', 'like', '%' . $searchValue . '%')
                  ->orWhere('cash_client_refund', 'like', '%' . $searchValue . '%')
                  ->orWhere('cash_client_process_budget_return', 'like', '%' . $searchValue . '%')
                  ->orWhere('cash_process_cost', 'like', '%' . $searchValue . '%')
                  ->orWhere('borrowed_process_cost', 'like', '%' . $searchValue . '%')
                  ->orWhere('cash_admin_budget_return', 'like', '%' . $searchValue . '%')
                  ->orWhere('borrowed_admin_cost', 'like', '%' . $searchValue . '%')
                  ->orWhere('cash_admin_cost', 'like', '%' . $searchValue . '%')
                  ->orWhere('bank_client_depo_payment', 'like', '%' . $searchValue . '%')
                  ->orWhere('bank_cost', 'like', '%' . $searchValue . '%')
                  ->orWhere('cash_balance', 'like', '%' . $searchValue . '%')
                  ->orWhere('bank_balance', 'like', '%' . $searchValue . '%')
                  ->orWhere('postdated_checks', 'like', '%' . $searchValue . '%')
                  ->orWhere('cost_other', 'like', '%' . $searchValue . '%')
                  ->orWhere('deposit_other', 'like', '%' . $searchValue . '%')
                  ->orWhere('created_at', 'like', '%' . $searchValue . '%')
                  ->orWhere('updated_at', 'like', '%' . $searchValue . '%')
                  ;
              });
          }

          $projects = $query->paginate($length);
          //$this->loadAjax($projects);

          return ['data' => $projects, 'draw' => $request->input('draw'), 'initial'=>$checkInitial];
    }

    public function fixInitial($branch_id,$dd){
     
      $now = Carbon::now();
      if($dd==0){
        $initial = DB::connection('visa')->table('financing2')->where('branch_id',$branch_id)->where('cat_type','initial')->orderBy('created_at', 'desc')->first();
      }else{
        $initial = DB::connection('visa')->table('financing2')->where('branch_id',$branch_id)->where('cat_type','initial')->orderBy('created_at', 'desc')->skip(1)->first();
      }
   
      if($initial){
        
        $month = Carbon::parse($initial->created_at);
        
        $cash = $initial->cash_balance;
        //$bank = $initial->bank_balance;
        $metrobank = $initial->metrobank;
        $securitybank = $initial->securitybank;
        $aub = $initial->aub;
        $eastwest = $initial->eastwest;

        for($i=$month->month;$i<$now->month;$i++){

          $query = DB::connection('visa')->table('financing2')->select(DB::raw('id,trans_desc, cat_type, cat_storage, cash_client_depo_payment, cash_client_refund, cash_client_process_budget_return, cash_process_cost, borrowed_process_cost, cash_admin_budget_return, borrowed_admin_cost, cash_admin_cost, bank_client_depo_payment, bank_cost, cash_balance, bank_balance, postdated_checks,cost_other, deposit_other, created_at, updated_at, cost_type, additional_budget, metrobank, securitybank, aub, storage_type, eastwest'))->where('branch_id',$branch_id)->whereRaw('MONTH(created_at) = "'.$i.'"')->where('cat_type','!=','initial')->orderBy('created_at', 'desc')->where('deleted_at',null)->get();
            if(count($query)>0){
              foreach($query as $q){
                    $cash =  ($cash+
                            +$q->cash_balance
                            +$q->cash_client_depo_payment
                            +($q->cat_storage=='cash' ? $q->cash_client_process_budget_return:0)
                            +($q->cat_storage=='cash' ? $q->cash_admin_budget_return:0)
                            +($q->cat_storage=='cash' ? $q->deposit_other:0))
                            -(($q->cat_storage=='cash' ? $q->cash_client_refund:0)
                            +($q->cat_storage=='cash' ? $q->cash_process_cost:0)
                            +($q->cat_storage=='cash' ? $q->cash_admin_cost:0)
                            +($q->cat_storage=='cash' ? $q->additional_budget:0)
                            +$q->borrowed_admin_cost
                            +$q->borrowed_process_cost
                            //+(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other==null && $q->deposit_other=='') && $q->cat_storage=='cash' ? $q->cost_other:0)
                            //+($q->cost_other!=null && $q->deposit_other==null && $q->cat_storage=='cash' ? $q->cost_other:0)
                            +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other==null || $q->deposit_other=='') && $q->cat_storage=='cash' ? $q->cost_other:0)
                            +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other!=null && $q->deposit_other!='') && $q->cat_storage=='bank' ? $q->cost_other:0));

                    $metrobank = ($metrobank
                            +$q->metrobank
                            +($q->cat_storage=='bank' && $q->storage_type=='metrobank' ? $q->bank_client_depo_payment:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='metrobank' ? $q->cash_client_process_budget_return:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='metrobank' ? $q->cash_admin_budget_return:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='metrobank' ? $q->deposit_other:0))
                            -(
                            // $q->bank_cost+
                            ($q->cat_storage=='bank' && $q->storage_type=='metrobank' ? $q->cash_admin_cost:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='metrobank' ? $q->cash_client_refund:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='metrobank' ? $q->cash_process_cost:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='metrobank' ? $q->additional_budget:0)
                            +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other==null || $q->deposit_other=='') && $q->cat_storage=='bank' && $q->storage_type=='metrobank'? $q->cost_other:0)
                            +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other!=null && $q->deposit_other!='') && $q->cat_storage=='cash' && $q->storage_type=='metrobank' ? $q->cost_other:0));

                    $securitybank = ($securitybank
                            +$q->securitybank
                            +($q->cat_storage=='bank' && $q->storage_type=='securitybank' ? $q->bank_client_depo_payment:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='securitybank' ? $q->cash_client_process_budget_return:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='securitybank' ? $q->cash_admin_budget_return:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='securitybank' ? $q->deposit_other:0))
                            -(
                            // $q->bank_cost+
                            ($q->cat_storage=='bank' && $q->storage_type=='securitybank' ? $q->cash_admin_cost:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='securitybank' ? $q->cash_client_refund:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='securitybank' ? $q->cash_process_cost:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='securitybank' ? $q->additional_budget:0)
                            +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other==null || $q->deposit_other=='') && $q->cat_storage=='bank' && $q->storage_type=='securitybank'? $q->cost_other:0)
                            +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other!=null && $q->deposit_other!='') && $q->cat_storage=='cash' && $q->storage_type=='securitybank' ? $q->cost_other:0));
                    $aub = ($aub
                            +$q->aub
                            +(($q->cat_storage=='bank' || $q->cat_storage=='alipay') && $q->storage_type=='aub' ? $q->bank_client_depo_payment:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='aub' ? $q->cash_client_process_budget_return:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='aub' ? $q->cash_admin_budget_return:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='aub' ? $q->deposit_other:0))
                            -(
                            // $q->bank_cost+
                            ($q->cat_storage=='bank' && $q->storage_type=='aub' ? $q->cash_admin_cost:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='aub' ? $q->cash_client_refund:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='aub' ? $q->cash_process_cost:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='aub' ? $q->additional_budget:0)
                            +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other==null || $q->deposit_other=='') && $q->cat_storage=='bank' && $q->storage_type=='aub'? $q->cost_other:0)
                            +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other!=null && $q->deposit_other!='') && $q->cat_storage=='cash' && $q->storage_type=='aub' ? $q->cost_other:0));
                           
                    $eastwest = ($eastwest
                            +$q->eastwest
                            +(($q->cat_storage=='bank' || $q->cat_storage=='alipay') && $q->storage_type=='eastwest' ? $q->bank_client_depo_payment:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='eastwest' ? $q->cash_client_process_budget_return:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='eastwest' ? $q->cash_admin_budget_return:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='eastwest' ? $q->deposit_other:0))
                            -(
                            // $q->bank_cost+
                            ($q->cat_storage=='bank' && $q->storage_type=='eastwest' ? $q->cash_admin_cost:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='eastwest' ? $q->cash_client_refund:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='eastwest' ? $q->cash_process_cost:0)
                            +($q->cat_storage=='bank' && $q->storage_type=='eastwest' ? $q->additional_budget:0)
                            +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other==null || $q->deposit_other=='') && $q->cat_storage=='bank' && $q->storage_type=='eastwest'? $q->cost_other:0)
                            +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other!=null && $q->deposit_other!='') && $q->cat_storage=='cash' && $q->storage_type=='eastwest' ? $q->cost_other:0));  

                            // $bank = ($bank
                        //     +$q->bank_balance
                        //     +$q->bank_client_depo_payment
                        //     +($q->cat_storage=='bank' ? $q->cash_client_process_budget_return:0)
                        //     +($q->cat_storage=='bank' ? $q->cash_admin_budget_return:0)
                        //     +($q->cat_storage=='bank' ? $q->deposit_other:0))
                        //     -($q->bank_cost
                        //     +($q->cat_storage=='bank' ? $q->cash_admin_cost:0)
                        //     +($q->cat_storage=='bank' ? $q->cash_client_refund:0)
                        //     +($q->cat_storage=='bank' ? $q->cash_process_cost:0)
                        //     +($q->cat_storage=='bank' ? $q->additional_budget:0)
                        //     +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other==null || $q->deposit_other=='') && $q->cat_storage=='bank' ? $q->cost_other:0)
                        //     +(($q->cost_other!=null && $q->cost_other!='') && ($q->deposit_other!=null && $q->deposit_other!='') && $q->cat_storage=='cash' ? $q->cost_other:0));
              }
            }
            $ndate1 = explode(' ',$now);
            $ndate2 = explode('-',$ndate1[0]);
            $ndate3 = Carbon::parse($ndate2[0].'/'.($i+1).'/01')->toDateTimeString();
            //dd($cash);
            
            if($dd==0){
              Finance::create([
                'cat_type'=>'initial',
                'cash_balance'=>$cash,
                //'bank_balance'=>$bank,
                'metrobank'=>$metrobank,
                'securitybank'=>$securitybank,
                'aub'=>$aub,
                'eastwest'=>$eastwest,
                'branch_id'=>$branch_id,
                'created_at'=>$ndate3
              ]);
            }else{
              $initial2 = DB::connection('visa')->table('financing2')->where('branch_id',$branch_id)->where('cat_type','initial')->orderBy('created_at', 'desc')->first();
              Finance::where('id',$initial2->id)->update(['cash_balance'=>$cash,'metrobank'=>$metrobank,'securitybank'=>$securitybank,'aub'=>$aub,'eastwest'=>$eastwest]);
            }
             

              //echo ($i+1).'-------';
              $initial = DB::connection('visa')->table('financing2')->where('branch_id',$branch_id)->where('cat_type','initial')->orderBy('created_at', 'desc')->first();
              $cash = $initial->cash_balance;
             // $bank = $initial->bank_balance;
              $metrobank = $initial->metrobank;
              $securitybank = $initial->securitybank;
              $aub = $initial->aub;
              $eastwest = $initial->eastwest;
              //dd($initial->cash_balance);
             

        }
      }
    }

    public function saveBalance(Request $request){
      //dd($request->all());
      $dateSelector = Carbon::parse($request->get('dateSelector').'/01')->toDateTimeString();
      $month = Carbon::parse($dateSelector);
      $now = Carbon::now();
      $res = false;
      if($now->gte($dateSelector)){
        // if(Initials::whereRaw('MONTH(created_at) = MONTH("'.$dateSelector.'")')->where('branch_id',Auth::user()->branch_id)->count()<1){
        //   Initials::create([
        //     'cash_balance'=>$request->get('cash_balance'),
        //     'bank_balance'=>$request->get('bank_balance'),
        //     'branch_id'=>$request->get('branch_id'),
        //     'created_at'=>$dateSelector
        //   ]);
        // }
        //dd($month->month);
        if($now->month==$month->month){

          $oldVal = Initials::orderBy('id','desc')->first();
          if($oldVal->cash_balance!=$request->get('cash_balance') || $oldVal->bank_balance!=$request->get('bank_balance')){
            Initials::create([
              'cash_balance'=>$request->get('cash_balance'),
              'bank_balance'=>$request->get('bank_balance'),
              'branch_id'=>$request->get('branch_id'),
              //'created_at'=>$dateSelector
            ]);
          }
        }
          //dd(Finance::whereRaw('MONTH(created_at) = MONTH(NOW())')->where('branch_id',$request->get('branch_id'))->where('cat_type','initial')->count());
          // if(Finance::whereRaw('MONTH(created_at) = MONTH("'.$dateSelector.'")')->where('branch_id',Auth::user()->branch_id)->where('cat_type','initial')->count()<1){
          //   Finance::create([
          //     'cat_type'=>'initial',
          //     'cash_balance'=>$request->get('cash_balance'),
          //     'bank_balance'=>$request->get('bank_balance'),
          //     'branch_id'=>$request->get('branch_id'),
          //     'created_at'=>$dateSelector
          //   ]);
          //   $res = true;
          // }
        }
        return json_encode([
            'success' => true,
            'restart' => $res
        ]);
        // else{
        //   $id = Initials::select('id')->where('branch_id',$request->get('branch_id'))->orderBy('id','desc')->first()->id;
        //   Initials::where('id',$id)->update(['cash_balance'=>$request->get('cash_balance'),'bank_balance'=>$request->get('bank_balance')]);
        // }
    }

    private function loadAjax($query){
      //$nquery = $query->get();
      $parentArray = array();
      foreach($query as $q){
        $arr = array();
        array_push($arr, $q->created_at);
        array_push($arr, $q->updated_at);
        array_push($arr, $q->trans_desc);
        array_push($arr, $q->cat_type);
        array_push($arr, $q->cat_storage);
        array_push($arr, $q->cash_client_depo_payment);
        array_push($arr, $q->cash_client_refund);
        array_push($arr, $q->cash_client_process_budget_return);
        array_push($arr, $q->cash_process_cost);
        array_push($arr, $q->cash_admin_budget_return);
        array_push($arr, $q->borrowed_admin_cost);
        array_push($arr, $q->cash_admin_cost);
        array_push($arr, $q->bank_client_depo_payment);
        array_push($arr, $q->bank_cost);
        array_push($arr, $q->deposit_other);
        array_push($arr, $q->cost_other);
        array_push($arr, $q->additional_budget);
        array_push($arr,0);
        array_push($arr,0);
        array_push($arr,0);
        array_push($arr, $q->cash_balance);
        array_push($arr, $q->bank_balance);
        array_push($arr,0);
        array_push($arr, $q->postdated_checks);
        array_push($arr,0);
        array_push($arr,0);
        array_push($arr,0);
        array_push($arr,0);
        array_push($arr,0);

        //push to parent
        array_push($parentArray,$arr);
      }

      $sample['data']=$parentArray;
      Storage::put('/public/data/finance.txt', "{".str_replace("{","[",str_replace("}","]",ltrim(rtrim(str_replace("null","0",json_encode($sample)),"}"), "{")))."}");
    }

    // private function reQuery(){
    //   $queryManila = DB::connection('visa')->table('financing2')->where('branch_id',1)->orderBy('id','asc')->get();
    //   $currentMonth = null;
    //     foreach($q1 as $queryManila){
    //       if($currentMonth==null){
    //         $currentMonth = Carbon::parse($q1->created_at)->format('M');
    //       }else{
    //         if($currentMonth==Carbon::parse($q1->created_at)->format('M')){
    //            $currentMonth = Carbon::parse($q1->created_at)->format('M');
    //         }else{
    //
    //         }
    //       }
    //
    //     }
    //   $queryCebu = DB::connection('visa')->table('financing2')->where('branch_id',2)->orderBy('id','asc')->get();
    //
    // }
    private function updateWithoutData($id){
      $branch_id = DB::connection('visa')->table('financing2')->select('branch_id')->where('id', $id)->first()->branch_id;
      $cat_storage = DB::connection('visa')->table('financing2')->select('cat_storage')->where('id', $id)->first()->cat_storage;
      $query = DB::connection('visa')->table('financing2')->where('branch_id',$branch_id)->where('cat_storage', $cat_storage)->where('id','>=',$id)->orderBy('id','asc')->get();
      $oldVal = Finance::where('id','<',$id)->where('branch_id',$branch_id)->orderBy('id','desc')->first();
      //dd($query);

      foreach($query as $q){
        Finance::where('id',$q->id)->update(['bank_balance'=>$oldVal->bank_balance,'cash_balance'=>$oldVal->cash_balance]);
        //echo $q->id.'---'.$oldVal->bank_balance.'--'.$oldVal->cash_balance.'||||';
        $oldVal = Finance::where('id',$q->id)->orderBy('id','desc')->first();
      }

      // $pquery = DB::connection('visa')->table('financing2')->where('branch_id',$branch_id)->where('cat_storage', $cat_storage)->where('process_output','!=',null)->where('id','>=',$id)->orderBy('id','asc')->get();
      // $oldVal2 = Finance::where('id','<',$id)->where('branch_id',$branch_id)->where('process_output','!=',null)->orderBy('id','desc')->first();
      // foreach($pquery as $qq){
      //   Finance::where('id',$qq->id)->update(['process_output'=>$oldVal2->process_output]);
      //   $oldVal2 = Finance::where('id',$qq->id)->orderBy('id','desc')->first();
      // }

    }

    // private function updateProcess($id){
    //   $branch_id = DB::connection('visa')->table('financing2')->select('branch_id')->where('id', $id)->first()->branch_id;
    //   $cat_storage = DB::connection('visa')->table('financing2')->select('cat_storage')->where('id', $id)->first()->cat_storage;
    //   $pquery = DB::connection('visa')->table('financing2')->where('branch_id',$branch_id)->where('cat_storage', $cat_storage)->where('process_output','!=',null)->where('id','>=',$id)->orderBy('id','asc')->get();
    //   $oldVal2 = Finance::where('id','<',$id)->where('branch_id',$branch_id)->where('process_output','!=',null)->orderBy('id','desc')->first();
    //   foreach($pquery as $qq){
    //     Finance::where('id',$qq->id)->update(['process_output'=>$oldVal2->process_output]);
    //     $oldVal2 = Finance::where('id',$qq->id)->orderBy('id','desc')->first();
    //   }
    // }

    // private function fixComputation($id){
    //   $branch_id = DB::connection('visa')->table('financing2')->select('branch_id')->where('id', $id)->first()->branch_id;
    //   $query = DB::connection('visa')->table('financing2')->where('branch_id',$branch_id)->where('process_output','!=',null)->where('id','>=',$id)->orderBy('id','asc')->get();
    //   //dd($query);
    //   $firstOutput = 0;
    //   if(count($query)>1){
    //       foreach($query as $q){
    //           if($firstOutput==0){
    //             $firstOutput = $q->process_output;
    //           }else{
    //             $firstOutput = $firstOutput + $q->cash_process_cost + $q->additional_budget + $q->borrowed_process_cost - ($q->cash_client_process_budget_return!=null ? $q->cash_client_process_budget_return:0);
    //
    //             Finance::where('id',$q->id)->update(['process_output'=>$firstOutput]);
    //             //dd($firstOutput);
    //           }
    //       }
    //  }
    //  $cashOutput = 0;
    //  $bankOutput = 0;
    //  $qr = Finance::where('branch_id',$branch_id)->where('id','>=',$id)->orderBy('id','asc')->get();
    //  if(count($qr)>1){
    //      foreach($qr as $q){
    //          if($cashOutput==0){
    //            $cashOutput = $q->cash_balance;
    //          }else{
    //            $cashOutput = ($cashOutput
    //            +$q->cash_client_depo_payment
    //            +$q->cash_client_process_budget_return
    //            +$q->cash_admin_budget_return
    //            +($q->cat_storage=='cash' ? $q->deposit_other:0))
    //            -(($q->cat_storage=='cash' ? $q->cash_client_refund:0)
    //            +($q->cat_storage=='cash' ? $q->cash_process_cost:0)
    //            +($q->cat_storage=='cash' ? $q->cash_admin_cost:0)
    //            +($q->cat_storage=='cash' ? $q->additional_budget:0)
    //            +$q->borrowed_admin_cost
    //            +$q->borrowed_process_cost
    //            +(($q->cost_other!=null && $q->cost_other!='')&&($q->deposit_other==null && $q->deposit_other=='')&&($q->cat_storage=='cash') ? $q->cost_other : 0)
    //            +(($q->cost_other!=null && $q->cost_other!='')&&($q->deposit_other!=null && $q->deposit_other!='')&&($q->cat_storage=='bank') ? $q->cost_other : 0));
    //            Finance::where('id',$q->id)->update(['cash_balance'=>$cashOutput]);
    //          }
    //
    //          if($bankOutput==0){
    //            $bankOutput = $q->bank_balance;
    //          }else{
    //            $bankOutput = ($bankOutput
    //            +$q->bank_client_depo_payment
    //            +($q->cat_storage=='bank' ? $q->deposit_other:0))
    //            -($q->bank_cost
    //            +($q->cat_storage=='bank' ? $q->cash_admin_cost:0)
    //            +($q->cat_storage=='bank' ? $q->cash_client_refund:0)
    //            +($q->cat_storage=='bank' ? $q->cash_process_cost:0)
    //            +($q->cat_storage=='bank' ? $q->cash_admin_cost:0)
    //            +($q->cat_storage=='bank' ? $q->additional_budget:0)
    //            +(($q->cost_other!=null && $q->cost_other!='')&&($q->deposit_other==null && $q->deposit_other=='')&&($q->cat_storage=='bank') ? $q->cost_other : 0)
    //            +(($q->cost_other!=null && $q->cost_other!='')&&($q->deposit_other!=null && $q->deposit_other!='')&&($q->cat_storage=='cash') ? $q->cost_other : 0));
    //            Finance::where('id',$q->id)->update(['bank_balance'=>$bankOutput]);
    //          }
    //
    //      }
    // }
    //   //dd($query);
    // }
    public function store(Request $request){
      //dd($request->all());
           Finance::create($request->all());

           return response()->json($request->all());
    }

    public function destroy($id){
          return Finance::destroy($id);
    }
    public function show($id){
          return Finance::findOrFail($id);
    }
    public function getView(){
      return view('cpanel.visa.nfinancing.index');
    }
    public function getDaily(){
       //dd(json_decode(DB::connection('visa')->table('dailycost')->select(DB::raw('*'))->groupBy('user_id')->get()));
      $trans = trans('cpanel/daily-cost-page');
      $data = array(
        'title' => $trans['daily-cost']
      );
      return view('cpanel.visa.nfinancing.daily', $data);
    }

    public function getBorrowed(Request $request){
      //dd($request->get('cat_type'));
      $cat = (($request->get('cat_type')=='pbr' || $request->get('cat_type')=='pab') ? 'process' : 'admin');
      if($cat=='admin'){
          if($request->get('cat_type')=='aab'){
            return Finance::where('cat_type',$cat)->whereRaw('(cash_admin_cost != "")')->where('cost_type',1)->where('additional_budget',NULL)->where('cash_admin_budget_return',NULL)->where('branch_id',Auth::user()->branch_id)->orderBy('id', 'desc')->get();
          }else{
            return Finance::where('cat_type',$cat)->whereRaw('(cash_admin_cost != "")')->where('cost_type',1)->where('cash_admin_budget_return',NULL)->where('branch_id',Auth::user()->branch_id)->orderBy('id', 'desc')->get();
          }
      }else{
        if($request->get('cat_type')=='pab'){
          return Finance::where('cat_type',$cat)->whereRaw('(cash_process_cost != "")')->where('cost_type',1)->where('additional_budget',NULL)->where('cash_client_process_budget_return',NULL)->where('branch_id',Auth::user()->branch_id)->orderBy('id', 'desc')->get();
        }else{
          return Finance::where('cat_type',$cat)->whereRaw('(cash_process_cost != "")')->where('cost_type',1)->where('cash_client_process_budget_return',NULL)->where('branch_id',Auth::user()->branch_id)->orderBy('id', 'desc')->get();
        }

      }
    }

    public function updateFinance(Request $request){

      $str = ($request->get('params')['cat_type'] == 'pc' ? 'cash_process_cost' : ($request->get('params')['cat_type'] =='abr' ? 'cash_admin_budget_return' : ($request->get('params')['cat_type'] == 'pbr' ? 'cash_client_process_budget_return' : ($request->get('params')['cat_type'] == 'aab' || $request->get('params')['cat_type'] == 'pab' ? 'additional_budget' : 'cash_admin_cost'))));
      //dd($request->all());
      //$nid = (int)$request->get('id');
      $vas = Finance::where('id', $request->get('params')['id'])->first();
      $timestamp = strtotime($vas->created_at);
      $oldMonth = date('m', $timestamp);
      $curMonth = date('m');
      
      if($str=='cash_admin_budget_return'){
        $nAmount = Finance::select('cash_admin_cost')->where('id', $request->get('params')['id'])->first()->cash_admin_cost;
      }else{
        $nAmount = Finance::select('cash_process_cost')->where('id', $request->get('params')['id'])->first()->cash_process_cost;
      }
      //dd($nAmount);
      if($request->get('params')['cat_type'] != 'aab' && $request->get('params')['cat_type'] != 'pab'){
        $nAmount = $nAmount + (Finance::select('additional_budget')->where('id', $request->get('params')['id'])->first()->additional_budget != null && Finance::select('additional_budget')->where('id', $request->get('params')['id'])->first()->additional_budget != '' ? Finance::select('additional_budget')->where('id', $request->get('params')['id'])->first()->additional_budget : 0 );
      }

      if($nAmount<$request->get('params')['amount'] && ($request->get('params')['cat_type'] != 'aab' && $request->get('params')['cat_type'] != 'pab')){
        return json_encode([
            'success' => false,
            'message' => 'Amount entered is higher than the amount borrowed'
        ]);
      }else{
         Finance::where('id', intval($request->get('params')['id']))->update([$str=>$request->get('params')['amount']]);

         //$this->fixComputation(intval($request->get('params')['id']));
         //$this->updateProcess(intval($request->get('params')['id']));

         //$this->updateWithoutData(intval($request->get('params')['id']));
          if($oldMonth!=$curMonth){
        
            $this->fixInitial($vas->branch_id,1);
          }
  
         return json_encode([
             'success' => true,
             'message' => 'Data has been saved!'
         ]);
      }
    }

    public function getUsers(Request $request){

      return DB::connection('mysql')->table('users')
            ->rightJoin('role_user', 'users.id', '=', 'role_user.user_id')
            ->select('users.id', 'users.first_name', 'users.last_name','role_user.role_id')
            ->where('role_user.role_id','!=',9)
            ->where('users.branch_id', $request->branch_id)
            ->groupBy('users.id')
            ->get();
    }

    public function getBranch(){
      return DB::connection('visa')->table('branches')->select('id','name')->get();
    }

    public function dataDailyCost(Request $request){
      $tmpBudget = 0;
      $subtotal = 0;
      $searchFrom= $request->get('from');
      $searchTo= $request->get('to');
      $currentUser = "";
      //dd($searchFrom);
      ////////////get user with budget default today
      $query =   DB::connection('visa')->table('financing2')->select(DB::raw('SUM(cash_process_cost) as total, SUM(additional_budget) as total2,user_sn, cash_process_cost, additional_budget'))->whereRaw('DATE(created_at)=?',[$searchFrom])->where('user_sn','>',0)->groupBy('user_sn')->get();
      //dd($query);
      if($query->isEmpty()){
        return [];
      }

      foreach($query as $q){
        $grandTotal = 0;
        $outer[] = array();

        // get a reference to the newly added array element
        end($outer);
        $currentItem = & $outer[key($outer)];


          $tmpBudget = $q->total + $q->total2;
          //dd($q->user_sn);
          $currentItem['first_name'] = Auth::user()->select('first_name')->where('id', $q->user_sn)->first()->first_name;
          $currentItem['last_name'] = Auth::user()->select('last_name')->where('id', $q->user_sn)->first()->last_name;
          $currentItem['budget'] = $q->cash_process_cost;
          $currentItem['add_budget'] = $q->additional_budget;
          $currentItem['total'] = $tmpBudget;
          $currentItem['user_id'] = $q->user_sn;
          $currentItem['inner1'] = array();

          //$tmpBudget = $tmpBudget + $q->cash_process_cost + ($q->additional_budget!='' || $q->additional_budget!=null ? $q->additional_budget : 0);


        $query2 = DB::connection('visa')->table('dailycost')->select(DB::raw('*'))->where('user_id',$q->user_sn)->whereRaw('DATE(created_at)=?',[$searchFrom])->groupBy('client_id')->get();

        foreach($query2 as $q2){

          $currentItem2['first_name'] = $q2->first_name;
          $currentItem2['last_name'] = $q2->last_name;
          $currentItem2['inner2'] = array();
          $query3 = DB::connection('visa')->table('dailycost')->select(DB::raw('*'))->where('client_id',$q2->client_id)->whereRaw('DATE(created_at)=?',[$searchFrom])->get();
          foreach($query3 as $q3){
              $subtotal = ($q3->cost !='' && $q3->cost!=null ? $q3->cost : 0 )+($q3->extra_cost !='' && $q3->extra_cost!=null ? $q3->extra_cost : 0 );
              $grandTotal = $grandTotal+$subtotal;
              $tmpBudget = $tmpBudget - (($q3->cost !='' && $q3->cost!=null ? $q3->cost : 0 )+($q3->extra_cost !='' && $q3->extra_cost!=null ? $q3->extra_cost : 0 ));
              array_push($currentItem2['inner2'], array("cost" => $q3->cost, "extra_cost" => $q3->extra_cost, "detail"=>$q3->detail,"total"=>$tmpBudget, "subtotal"=>$subtotal));
          }

          array_push($currentItem['inner1'],$currentItem2);
          $currentItem['grand']=$grandTotal;
          $currentItem['return']=$tmpBudget;

        }
      }
      //////////

      //dd($searchTo);
      // $data = DB::connection('visa')->table('dailycost')->select(DB::raw('*'))->whereBetween(DB::raw('DATE(created_at)'), [$searchFrom, $searchTo])->get();
      // $output = array();
      // $currentUser = "";
      // $currentBorrower = "";
      //
      //   foreach ($data as $datum) {
      //
      //     if ($datum->client_id != $currentUser) {
      //       $output[] = array();
      //
      //       // get a reference to the newly added array element
      //       end($output);
      //       $currentItem = & $output[key($output)];
      //
      //       $currentUser = $datum->client_id;
      //       $currentItem['client_id'] = $currentUser;
      //       $currentItem['user_id'] = $datum->user_id;
      //       $currentItem['first_name'] = $datum->first_name;
      //       $currentItem['last_name'] = $datum->last_name;
      //       $currentItem['fname'] = Auth::user()->select('first_name')->where('id', $datum->user_id)->first()->first_name;
      //       $currentItem['lname'] = Auth::user()->select('last_name')->where('id', $datum->user_id)->first()->last_name;
      //       $currentItem['nested'] = array();
      //     }
      //     $currentItem['nested'][] = array("cost" => $datum->cost, "extra_cost" => $datum->extra_cost, "detail"=>$datum->detail);
      //   }
        //return $output;
        return ['data' => $outer, 'draw' => $request->input('draw')];
    }

    public function deleteRow($id){
            $checkRecord =   Finance::findOrFail($id);
            if($checkRecord){
                  $type = $checkRecord->type;
                  $rec_id = $checkRecord->record_id;

                  if($type == "refund" || $type == "deposit" || $type == "payment"){
                      $trans = Trans::where('type',ucfirst($type))->where('id',$rec_id)->first();
                        if($trans){
                          $trans->delete();
                        }
                  }
                  else if($type == "transfer"){
                      $trans = TransferBalance::where('id',$rec_id)->first();
                        if($trans){

                          if($trans->from_type == 'client'){
                              $trans2 = Trans::where('type','Refund')->where('client_id',$trans->transfer_from)
                                             ->where('amount', number_format((float)$trans->amount, 2, '.', ''))->where('group_id',0)
                                             ->first();
                                if($trans2){
                                  $trans2->delete();
                                }
                          }
                          else{
                            $trans2 = Trans::where('type','Refund')->where('amount', number_format((float)$trans->amount, 2, '.', ''))
                                             ->where('group_id',$trans->transfer_from)
                                             ->first();
                                if($trans2){
                                  $trans2->delete();
                                }
                          }


                          if($trans->to_type == 'client'){
                              $trans2 = Trans::where('type','Deposit')->where('client_id',$trans->transfer_to)
                                             ->where('amount', number_format((float)$trans->amount, 2, '.', ''))->where('group_id',0)
                                             ->first();
                                if($trans2){
                                  $trans2->delete();
                                }
                          }
                          else{
                            $trans2 = Trans::where('type','Deposit')->where('amount', number_format((float)$trans->amount, 2, '.', ''))
                                             ->where('group_id',$trans->transfer_to)
                                             ->first();
                                if($trans2){
                                  $trans2->delete();
                                }
                          }

                          $trans->delete();
                        }
                  }
                  $checkRecord->delete();
            }
      return json_encode([
             'success' => true,
             'message' => 'Data has been deleted!'
         ]);
    }

}
