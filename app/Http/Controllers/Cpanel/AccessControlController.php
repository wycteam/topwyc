<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\Models\PermissionRole;
use App\Models\AccessControl;
use App\Http\Requests\RoleValidation;
use DB;
use QrCode, Image;

class AccessControlController extends Controller
{

    public function index() {
        $permissions = User::with('roles.permissions')->whereId(Auth::id())->get();
        $check = 0;
        foreach($permissions[0]->permissions as $p) {
            if($p->name == 'Access Control') {
                $check = 1;
            }
        }

        if($check==1)
            return view('cpanel/visa/access-control/index');
        else
            abort(404);
    }

    public function getUser(){
        return User::with('roles.permissions')->whereId(Auth::id())->get();
    }

    public function getRoles(){
        return Role::all();
    }

    public function getPermissions(){
        return Permission::all();
    }

    public function getSelectedPermission($id){
        return PermissionRole::whereRoleId($id)->get();
    }

    public function getSelectedPermissions($id){
        $id = explode(",", $id);

        $permissionRole = PermissionRole::with('permissions')->whereIn('role_id',$id)->get();

        $prs = '';
        
        foreach($permissionRole as $pr) {
            $prs .= $pr->permission_id . ',';
        }

        return $prs;
    }

    public function getSelectedPermissions2($id) {
        $id = explode(",", $id);

        return Permission::whereIn('id',$id)->get();
    }

    public function getPermissionType(){
        return Permission::select('id','type')->groupBy('type')->get();
    }

    public function downloadQR($id){
        $id = base64_encode($id); 
        $headers = $_SERVER['SERVER_NAME'];
        $host = 'http://'.str_replace('cpanel.','',$headers);
        $url = $host.'/open-register/'.$id;
        $qr =  QrCode::format('png')->merge('images/icon.png', 0.3, true)->size(500)->errorCorrection('H')->generate($url); 
        return response($qr)->header('Content-type','image/png');
    }


    public function updateAccessControl(Request $request){

    	$permissions = $request->get('selpermissions');

    	PermissionRole::where('role_id', $request->get('roleid'))->delete();

		foreach ($permissions as $per) {

			$data = new PermissionRole($request->all());
            $data->permission_id = $per['permission_id'];
            $data->role_id = $per['role_id'];
            $data->save();

		}
		
        return $request->all();
    }

    public function addRole(RoleValidation $request){

        $r = $request->get('role');

        $role = new Role($request->all());
        $role->name = str_replace(" ","-",strtolower($r));
        $role->label = $r;
        $role->save();
        
        return $request->all();
    }

    public function getRole($id){

        return Role::whereId($id)->get();

        $r = $request->get('role');

        $role = new Role($request->all());
        $role->name = str_replace(" ","-",strtolower($r));
        $role->label = $r;
        $role->save();
        
        return $request->all();
    }

    public function saveRole(RoleValidation $request){

        $r = $request->get('role');

        $values=array('name'=>str_replace(" ","-",strtolower($r)),'label'=>$r);
        Role::where('id','=', $request->get('id'))->update($values);
        
        return $request->all();
    }

    public function toggle(Request $request){
        $setting = $request->setting;
        $status = DB::table('access_control')->update(['setting' => $setting]);
    }
}