<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Visa\ImageUpload as ImageUpload;
use File;
use Image;
use Illuminate\Support\Facades\DB;
use App\Models\Visa\Albums as Albums;
class GalleryController extends Controller
{

  public function getGalleryDetailPageTranslation() {
    return json_encode([
      'translation' => trans('cpanel/gallery-detail-page')
    ]);
  }

  public function getGalleryPageTranslation() {
    return json_encode([
      'translation' => trans('cpanel/gallery-page')
    ]);
  }

  public function index(Request $request){
        $columns = ['title'];
        $length = $request->input('length');
        $column = $request->input('column'); //Index
        $dir = $request->input('dir');
        $searchValue = $request->input('search');
        //$query = DB::connection('visa')->table('newslist')->select(DB::raw('header, name, created_at, news_id'))->orderBy($columns[$column], $dir);
        $query = Albums::select('title', 'id')->orderBy($columns[$column], $dir);
        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('title', 'like', '%' . $searchValue . '%');
            });
        }
        $albums = $query->paginate($length);
        return ['data' => $albums, 'draw' => $request->input('draw')];
  }

  public function getList(){
    return Albums::all();
  }
  public function getSingleAlbum($id){
    return Albums::findOrFail($id);
  }
  public function getAlbum(){
    return view('cpanel.visa.gallery.gallery-table');
  }

  public function getGallery($id){
    return view('cpanel.visa.gallery.gallery-panel');
  }
  public function featuredImage($id){
    return ImageUpload::select('filename')->where('album_id', $id)->inRandomOrder()->first();
  }
  public function store(Request $request){
        $this->validate($request, [
          'title' => 'required|unique:visa.image_album,title'
        ]);
        Albums::create($request->all());
        return response()->json($request->all());
  }
  public function albumUpdate(Request $request, $id){
    if(!$request->get('update_public')){
        $this->validate($request, [
          'title' => 'required|unique:visa.image_album,title'
        ]);
    }
        Albums::findOrFail($id)->update($request->all());
        return response()->json($request->all()); //response()->json()
  }

  public function show($id){
        return Albums::findOrFail($id);
  }

  public function destroy($id){
      if(ImageUpload::where('album_id', $id)->count()>0){
        return response()->json(['response'=>'failed']);
      }else{
        Albums::destroy($id);
        return response()->json(['response'=>'success']);
      }
  }

  public function fileStore(Request $request){

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        //$image->move(public_path('images'),$imageName);
        //$img = Image::make($request->file('file')->getRealPath());
        //$image->resize(120, 72);
        if(!File::exists(public_path('images/gallery'))) {
          File::makeDirectory(public_path('images/gallery'), 0777, true, true);
        }
        if(!File::exists(public_path('images/thumbnail'))) {
          File::makeDirectory(public_path('images/thumbnail'), 0777, true, true);
        }
        $img = Image::make($image);
        $img->resize(660, 480)
        ->save(public_path('images/gallery/'.$imageName))
        ->resize(120, 120)
        ->save(public_path('images/thumbnail/'.$imageName));
        //$img->move(public_path('images/thumbnail'),$imageName);

        $imageUpload = new ImageUpload();
        $imageUpload->filename = $imageName;
        $imageUpload->album_id = $request->get('album_id');
        $imageUpload->save();
        return response()->json(['success'=>$imageName]);
    }

    public function fileDestroy(Request $request){
        $filename =  $request->get('filename');
        ImageUpload::where('filename',$filename)->delete();
        $path=public_path().'/images/gallery/'.$filename;
        $path2=public_path().'/images/thumbnail/'.$filename;
        if (file_exists($path)) {
            File::delete($path);
        }
        if (file_exists($path2)) {
            File::delete($path2);
        }
        return $path;
    }

    public function addAlbum(Request $request) {

        // $this->validate($request, [
        //     'name' => 'required'
        // ]);

        $album = new Albums;
        $album->title = $request->get('name');
        $album->save();

        // return json_encode([
        //     'success' => true,
        //     'message' => 'New album added.'
        // ]);

    }

    public function getServerImages($id){
        $images = ImageUpload::select('filename')->where('album_id', $id)->orderBy('id','asc')->get();

        $imageAnswer = [];

        foreach ($images as $image) {
            $imageAnswer[] = [
                'server' => $image->filename,
                'size' => File::size(public_path('images/gallery/' . $image->filename))
            ];
        }

        return response()->json([
            'images' => $imageAnswer,
        ]);
    }
}
