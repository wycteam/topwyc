<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Shopping\Order;
use App\Models\Shopping\Store;
use App\Models\Shopping\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ReasonValidation;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\Visa\ClientService;
use App\Models\Visa\Task;
use App\Models\RoleUser as role_users;
use App\Models\Role as role;
use App\Models\Visa\DeliverySchedule as deliveryschedule;
use App\Models\Visa\VisaType;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function setLocale($locale = 'en') {
        return response(['success'=> true, 'activeLocale' => $locale], 200)->withCookie(cookie()->forever('locale', $locale));
    }

    public function getLocale() {
        $locale = (\Request::cookie('locale')) ? \Request::cookie('locale') : 'en';

        return response(['success' => true, 'activeLocale'=> $locale], 200)->withCookie(cookie()->forever('locale', $locale));
    }

    public function translation() {
        return json_encode([
            'translation' => trans('cpanel/dashboard')
        ]);
    }

    public function todaysServicesTranslation() {
        return json_encode([
            'translation' => trans('cpanel/todays-services-component')
        ]);
    }

    public function deliveryScheduleTranslation() {
        return json_encode([
            'translation' => trans('cpanel/delivery-schedule-component')
        ]);
    }

    public function editServiceDashboardTranslation() {
        return json_encode([
            'translation' => trans('cpanel/edit-service-dashboard-component')
        ]);
    }

    public function index()
    {
        $title = trans('cpanel/dashboard');
        $role_id = 9;
        $dt = Carbon::now();
        $dt = $dt->toDateString();
        $yesterday = date("m/d/Y", strtotime( '-1 days' ) );


        $data = array(
            'title' => $title['dashboard'],
            'total_client' =>$this->formatMoney(User::whereHas('roles', function ($query) use ($role_id) {
                    $query->where('roles.id', '=', $role_id);
            })->where(function ($query) {
                  $query->where('branch_id', Auth::user()->branch_id)
                        ->orwhere('branch_id', '3');
            })->count()),

            'total_service' => $this->formatMoney(ClientService::whereHas('user',function($q){
                 $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
              })->count())
        );


         $todayServices = ClientService::where('service_date', date('m/d/Y'))->where('active',1)->whereHas('user',function($q){
             $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
         })->get();

         $totalTodayServiceAmount = 0;
         $totalNoTodayServices = 0;
         foreach ($todayServices as $item) {
             $totalTodayServiceAmount += (($item->cost + $item->charge + $item->tip) - $item->discount_amount);
             $totalNoTodayServices ++;
         }

        $data['total_service_new'] =  $this->formatMoney($totalNoTodayServices);
        $data['total_today_services_amount'] =  "Php " .$this->formatMoney($totalTodayServiceAmount);



        $totalServiceYesterday = ClientService::where('service_date', $yesterday)->where('active',1)->whereHas('user',function($q){
             $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
          })->get();

        $totalYesterdayServiceAmount = 0;
        $totalNoYesterdayServices = 0;
        foreach ($totalServiceYesterday as $item) {
              $totalYesterdayServiceAmount += (($item->cost + $item->charge + $item->tip) - $item->discount_amount);
              $totalNoYesterdayServices ++;
          }

        $data['total_service_yesterday'] =  $this->formatMoney($totalNoYesterdayServices);
        $data['total_yesterday_services_amount'] =  "Php " . $this->formatMoney($totalYesterdayServiceAmount);


        $oldestPendingServicesRecords = DB::connection('mysql')
            ->table('visa.client_services as cs')
            ->selectRaw("cs.service_date, cs.extend, cs.client_id, cs.id, u.last_name, u.first_name, u.middle_name, cs.detail, cs.cost, cs.remarks, cs.status")
             ->leftjoin(DB::raw('(SELECT last_name, first_name, middle_name, id, branch_id  FROM wycgroup.users WHERE  branch_id = '.Auth::user()->branch_id.' OR branch_id = 3 ) as u'), 'u.id', '=', 'cs.client_id')
                    ->leftjoin(DB::raw('(SELECT parent_id, detail, id from visa.services WHERE parent_id != 0) as s'), 's.id', '=', 'cs.service_id')
                     ->where('cs.status', '=', 'pending')
                     ->where('u.branch_id', Auth::user()->branch_id)
                     ->where('cs.active', '=', 1)
                     ->where('cs.service_date','!=',$dt)->orderByRaw(
                           "CASE WHEN cs.extend = null THEN cs.id ELSE cs.extend END, cs.id ASC"
                       )
                     ->skip(0)->take(3)->get();


         $oldestPendingServices = [];
         $ctr = 0;
         foreach ($oldestPendingServicesRecords as $value) {
             $currentDateTime = strtotime($dt);
             $serviceDateTime = strtotime($value->service_date);
             $hourDiff = round(abs($serviceDateTime - $currentDateTime) / (60*60*24),0);

             $oldestPendingServices[$ctr]['client_name'] = ucwords($value->first_name).' '.ucwords($value->last_name);
             $oldestPendingServices[$ctr]['client_id'] = $value->client_id;
             $oldestPendingServices[$ctr]['cost'] = $value->cost;
             $oldestPendingServices[$ctr]['remarks'] = str_replace("</br>",", ",$value->remarks);
             $oldestPendingServices[$ctr]['status'] = $value->status;
             $oldestPendingServices[$ctr]['detail'] = $value->detail;
             $oldestPendingServices[$ctr]['service_date'] = $value->service_date;
             $oldestPendingServices[$ctr]['extend'] = $value->extend;
             $oldestPendingServices[$ctr]['no_days'] = ($hourDiff >= 30) ? "is ".$hourDiff. " days pending." : "";
             $ctr++;
         }
         $data['oldest_pending_services'] = json_encode($oldestPendingServices);



        //Oldest On process Services
        $oldestOnprocessServicesRecords = DB::connection('mysql')
             ->table('visa.client_services as cs')
             ->select(DB::raw('
                 cs.client_id, cs.service_date, cs.id, u.last_name, u.first_name, u.middle_name, cs.detail, cs.cost, cs.remarks, cs.status
                 ')) ->leftjoin(DB::raw('(SELECT last_name, first_name, middle_name, id, branch_id  FROM wycgroup.users WHERE branch_id = '.Auth::user()->branch_id.' OR branch_id = 3 ) as u'), 'u.id', '=', 'cs.client_id')
                     ->leftjoin(DB::raw('(SELECT parent_id, detail, id from visa.services WHERE parent_id != 0) as s'), 's.id', '=', 'cs.service_id')
                      ->where('cs.active', '=', 1)
                      ->where('cs.status', '=', 'on process')
                      ->where('u.branch_id', Auth::user()->branch_id)
                    //  ->orderBy('cs.service_date', 'asc')->skip(0)->take(3)->get();
                        ->orderByRaw(
                            "CASE WHEN cs.extend = null THEN cs.id ELSE cs.extend END, cs.id ASC"
                        )
                        ->skip(0)->take(3)->get();

          $oldestOnprocessServices = [];
          $ctr = 0;
          foreach ($oldestOnprocessServicesRecords as $value) {

            $currentDateTime = strtotime($dt);
            $serviceDateTime = strtotime($value->service_date);
            $hourDiff = round(abs($serviceDateTime - $currentDateTime) / (60*60*24),0);

              $oldestOnprocessServices[$ctr]['client_name'] = ucwords($value->first_name).' '.ucwords($value->last_name);
              $oldestOnprocessServices[$ctr]['client_id'] = $value->client_id;
              $oldestOnprocessServices[$ctr]['cost'] = $value->cost;
              $oldestOnprocessServices[$ctr]['remarks'] = str_replace("</br>",", ",$value->remarks);
              $oldestOnprocessServices[$ctr]['status'] = $value->status;
              $oldestOnprocessServices[$ctr]['detail'] = $value->detail;
              $oldestOnprocessServices[$ctr]['service_date'] = $value->service_date;
              $oldestOnprocessServices[$ctr]['no_days'] = ($hourDiff >= 30) ? "is ".$hourDiff. " days being on process." : "";
              $ctr++;
          }
          $data['oldest_on_process_services'] = json_encode($oldestOnprocessServices);


          $todayTasksRecords = DB::connection('mysql')
              ->table('visa.tasks as t')
              ->select(DB::raw('
                  t.client_service_id, t.who_is_in_charge, t.where_to_file, t.status, t.estimated_cost, t.date, c.lname, c.fname, cs.client_id, u.last_name, u.first_name, u.middle_name, cs.detail, cs.cost, cs.remarks
                  ')) ->leftjoin(DB::raw('(SELECT last_name, first_name, middle_name, branch_id, id  from wycgroup.users) as u'), 'u.id', '=', 't.who_is_in_charge')
                      ->leftjoin(DB::raw("(SELECT detail, cost, remarks, status, client_id, id  from visa.client_services WHERE status = 'on process' AND active = 1) as cs"), 'cs.id', '=', 't.client_service_id')
                      ->rightjoin(DB::raw('(SELECT last_name as lname, first_name as fname, id  from wycgroup.users) as c'), 'c.id', '=', 'cs.client_id')
                      ->where('u.branch_id', Auth::user()->branch_id)
                    ->whereDate('t.date', '=', $dt)->orderBy('t.date')->get();

          $todayTasks = [];
          $ctr = 0;
          $totalEstimatedCostForToday = 0;
          foreach ($todayTasksRecords as $value) {
              $todayTasks[$ctr]['client_name'] = ucwords($value->fname).' '.ucwords($value->lname);
              $todayTasks[$ctr]['processor'] = ucwords($value->first_name).' '.ucwords($value->last_name);
              $todayTasks[$ctr]['client_id'] = $value->client_id;
              $todayTasks[$ctr]['estimated_cost'] = $value->estimated_cost;
              $todayTasks[$ctr]['cost'] = $value->cost;
              $todayTasks[$ctr]['remarks'] = str_replace("</br>",", ",$value->remarks);
              $todayTasks[$ctr]['date'] = $value->date;
              $todayTasks[$ctr]['status'] = $value->status;
              $todayTasks[$ctr]['detail'] = $value->detail;
              $todayTasks[$ctr]['where_to_file'] = $value->where_to_file;
              $totalEstimatedCostForToday += $value->estimated_cost;
              $ctr++;
          }
          $data['today_tasks'] = json_encode($todayTasks);
          $data['total_estimated_cost_for_today'] = $this->formatMoney($totalEstimatedCostForToday);


          $reminderRecords = DB::connection('mysql')
              ->table('visa.tasks as t')
              ->select(DB::raw('
                  t.client_service_id, t.who_is_in_charge, t.where_to_file, t.status, t.estimated_cost, t.date, c.lname, c.fname, cs.client_id, u.last_name, u.first_name, u.middle_name, cs.detail, cs.cost, cs.remarks
                  ')) ->leftjoin(DB::raw('(SELECT last_name, first_name, middle_name, id, branch_id  from wycgroup.users) as u'), 'u.id', '=', 't.who_is_in_charge')
                      ->leftjoin(DB::raw('(SELECT detail, cost, remarks, status, active,  client_id, id  from visa.client_services WHERE active = 1) as cs'), 'cs.id', '=', 't.client_service_id')
                      ->leftjoin(DB::raw('(SELECT last_name as lname, first_name as fname, id  from wycgroup.users) as c'), 'c.id', '=', 'cs.client_id')
                        ->where('u.branch_id', Auth::user()->branch_id)
                       ->whereDate('t.date', '>', $dt)->orderBy('date')->get();

          $reminders = [];
          $ctr = 0;
          $totalEstimatedCostForYesterday = 0;
          foreach ($reminderRecords as $value) {
              $reminders[$ctr]['client_name'] = ucwords($value->fname).' '.ucwords($value->lname);
              $reminders[$ctr]['processor'] = ucwords($value->first_name).' '.ucwords($value->last_name);
              $reminders[$ctr]['client_id'] = $value->client_id;
              $reminders[$ctr]['estimated_cost'] = $value->estimated_cost;
              $reminders[$ctr]['cost'] = $value->cost;
              $reminders[$ctr]['remarks'] = str_replace("</br>",", ",$value->remarks);
              $reminders[$ctr]['date'] = $value->date;
              $reminders[$ctr]['status'] = $value->status;
              $reminders[$ctr]['detail'] = $value->detail;
              $reminders[$ctr]['where_to_file'] = $value->where_to_file;
              $totalEstimatedCostForYesterday += $value->estimated_cost;
              $ctr++;
          }

          $data['reminders'] = json_encode($reminders);
          $data['total_estimated_cost_for_yesterday'] = $this->formatMoney($totalEstimatedCostForYesterday);

        return view('cpanel.visa.dashboard.index', $data);
    }

    public function formatMoney($number, $fractional=false) {
        if ($fractional) {
            $number = sprintf('%.2f', $number);
        }
        while (true) {
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
            if ($replaced != $number) {
                $number = $replaced;
            } else {
                break;
            }
        }
        return trim($number);
    }

    public function pendingServices() {
        $dateTimeToday = Carbon::today()->toDateTimeString();

        $dt = Carbon::now();

        $dt = $dt->toDateString();

        // $today =  ClientService::with('group', 'discount2')->where('status','pending')->where('active',1)->where('extend','=',$dt)->whereHas('user',function($q){
        //      $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id);
        //   })->get();
        $today =  ClientService::with('group', 'discount2')->where('client_services.status','pending')->where('client_services.active',1)->where('client_services.extend','=',$dt)->whereHas('user',function($q){
               $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
            })->leftJoin('services','services.id','=','client_services.service_id')->where('services.parent_id','!=',0)->get();

          //dd($today);
        $pending =  ClientService::with('group', 'discount2')->where('client_services.status','pending')->where('client_services.active',1)->where('client_services.extend',null)->whereHas('user',function($q){
             $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
          })->leftJoin('services','services.id','=','client_services.service_id')->where('services.parent_id','!=',0)->get();

        $lowprio =  ClientService::with('group', 'discount2')->where('client_services.status','pending')->where('client_services.active',1)->where('client_services.extend','!=',$dt)->whereHas('user',function($q){
             $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
          })->leftJoin('services','services.id','=','client_services.service_id')->where('services.parent_id','!=',0)->get();

        $all=$today->merge($pending)->merge($lowprio);


        return $all;
    }


    public function servicesSummaryReport(){

      $dt = Carbon::now();
      $dt = $dt->toDateString();

      $returnPayload = app()->make('stdClass');

      //Oldest pending services
      $returnPayload->oldest_pending_services =  ClientService::with('group', 'discount2')->where('client_services.status','pending')->where('client_services.active',1)->where('client_services.extend','!=',$dt)->whereHas('user',function($q){
           $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
        })->leftJoin('services','services.id','=','client_services.service_id')->where('services.parent_id','!=',0)->orderBy('client_services.extend', 'desc')->skip(0)->take(3)->get();
      //end oldest pending services


     //Oldest On process Services
      $returnPayload->oldest_on_process_services =  ClientService::where('client_services.status','on process')->where('client_services.active', '1')
             ->where(function($query) {
                 return $query->where('checked', '0')
                     ->orWhere('checked', NULL);
             })->whereHas('user',function($q){
                  $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
             })->leftJoin('services','services.id','=','client_services.service_id')->where('services.parent_id','!=',0)
               ->join('wycgroup.users', 'client_services.client_id', '=', 'wycgroup.users.id')
               ->orderBy('client_services.extend', 'desc')->skip(0)->take(3)->get();
       //end oldest on process services


      //Today Tasks
      $returnPayload->today_tasks = Task::with('clientService.user', 'clientService.service_category', 'whoIsInChargeRelation')
          ->whereHas('clientService', function($query) {
              $query->where('active', 1)->where('status', 'on process');
          })
          ->whereDate('date', '=', '2019-08-21')->orderBy('date')->get();


      //Reminders
      $returnPayload->reminders  = Task::with('clientService.user', 'clientService.service_category', 'whoIsInChargeRelation')
          ->whereHas('clientService', function($query) {
              $query->where('active', 1);
          })
          ->whereDate('date', '>', '2018-08-21')->orderBy('date')->get();


       return response()->json($returnPayload);

    }




    public function onprocessServices() {

        return ClientService::where('client_services.status','on process')->where('client_services.active', '1')
                ->where(function($query) {
                    return $query->where('checked', '0')
                        ->orWhere('checked', NULL);
                })->whereHas('user',function($q){
                     $q->from('wycgroup.users')->where('branch_id', Auth::user()->branch_id)->orwhere('branch_id', '3');
                })->leftJoin('services','services.id','=','client_services.service_id')->where('services.parent_id','!=',0)
                  ->join('wycgroup.users', 'client_services.client_id', '=', 'wycgroup.users.id')
                  ->get();
    }





    public function removeService($id) {

        return ClientService::with('user')
                ->where('status','on process')
                ->where('checked','0')
                ->where('id',$id)
                ->update(['checked' => '1']);;

    }

    public function markAsPending(ReasonValidation $request) {

        $data = $request->all();
        return ClientService::with('user')
                ->where('status','on process')
                ->where('checked','0')
                ->where('id',$data['id'])
                ->update(['status' => 'pending','reason' => $data['reason']]);;

    }

    public function getTodaysTasks(Request $request) {
        $dateFilter = $request->dateFilter;

        $page = $request->page;
        $take = 1;

        $tasks = [];

        $dates = Task::distinct()
            ->whereHas('clientService', function($query) {
                $query->where('active', 1)->where('status', 'on process');
            })
            ->select('date')
            ->whereDate('date', '>=', $dateFilter)
            ->orderBy('date')
            ->skip(($page-1) * $take)
            ->take($take)
            ->get();

        foreach($dates as $d) {
            $estimatedCostPerDay = DB::connection('visa')->table('tasks')->whereDate('date', $d->date);
            if($request->employeeFilter != 'All') {
                $estimatedCostPerDay = $estimatedCostPerDay->where('who_is_in_charge', $request->employeeFilter);
            }
            $estimatedCostPerDay = $estimatedCostPerDay->sum('estimated_cost');

            $ts = DB::connection('visa')->table('tasks')->whereDate('date', $d->date);
            if($request->employeeFilter != 'All') {
                $ts = $ts->where('who_is_in_charge', $request->employeeFilter);
            }
            $ts = $ts->orderBy('date')->get();

            $data = [];
            foreach($ts as $t) {
                $clientService = DB::connection('visa')->table('client_services')
                    ->where('id', $t->client_service_id)->first();

                if($clientService) {
                    $user = DB::table('users')->where('id', $clientService->client_id)->first();
                    $avatar = ($user->gender == 'Male') ? '/images/avatar/boy-avatar.png' : '/images/avatar/girl-avatar.png';
                    $service = DB::connection('visa')->table('services')->where('id', $clientService->service_id)->first();
                    $whoIsInCharge = DB::table('users')->where('id', $t->who_is_in_charge)->first();

                    $data[] = [
                        'id' => $t->id,
                        'userId' => $user->id,
                        'avatar' => $avatar,
                        'userFullName' => $user->first_name . ' ' . $user->last_name,
                        'estimatedCost' => $t->estimated_cost,
                        'serviceDetail' => $service->detail,
                        'status' => $t->status,
                        'whereToFile' => $t->where_to_file,
                        'customDate' => $t->custom_date,
                        'date' => $t->date,
                        'whoIsInChargeId' => $whoIsInCharge->id,
                        'whoIsInChargeFullName' => $whoIsInCharge->first_name . ' ' . $whoIsInCharge->last_name
                    ];
                }
            }

            $tasks[] = [
                'date' => $d->date,
                'estimatedCostPerDay' => $estimatedCostPerDay,
                'data' => $data
            ];
        }

        return json_encode([
            'tasks' => $tasks,
            'page' => $page
        ]);
    }

    public function getReminders(Request $request) {
        $data = User::
            // For 9A
            where(function($query) {
                $query->where('visa_type', '9A')
                    ->where(function($query2) {
                        $query2->whereRaw('exp_date <= DATE_ADD(CURDATE(), INTERVAL 7 DAY)')
                            ->orWhereRaw('first_exp_date <= DATE_ADD(CURDATE(), INTERVAL 7 DAY)');
                    });
            })
            // For 9G, TRV and CWV
            ->orWhere(function($query) {
                $query->where('visa_type', '<>', '9A')
                    ->where(function($query3) {
                            $query3->whereRaw('exp_date <= DATE_ADD(CURDATE(), INTERVAL 30 DAY)');
                        });
            })
            ->orderBy('id')
            ->paginate(5);

        return response()->json($data);
    }

    public function loadCompanyCouriers(){
        $role = role::where('name', 'company-courier')->first();
        $role_users = role_users::where('role_id',$role->id)->with('user')->get();
        return json_encode($role_users);
    }

    public function addDeliverySchedule(Request $request){
        $data = $request->all();
        $schedule = new deliveryschedule();
        $schedule->fill($data)->save();
        return $schedule;
    }

    public function deleteSchedule($id){
        $data = deliveryschedule::findOrFail($id);
        $data->delete();
        return response()->json($data);
    }

    public function getSchedulesbyDate(Request $request) {
        $date = $request->dateFilter;
        $schedules = deliveryschedule::with('rider')->where('scheduled_date', $date)->get();
        return json_encode([
            'success' => true,
            'schedules' => $schedules
        ]);
    }

    public function editSchedule(Request $request){
        $data = deliveryschedule::whereId($request->id)->update([
             'item'           => $request->item
            ,'rider_id'       => $request->rider_id
            ,'location'       => $request->location
            ,'scheduled_date' => $request->scheduled_date
            ,'scheduled_time' => $request->scheduled_time
            ]);

        return response()->json($data);
    }

    public function getSched($id){
        $data = deliveryschedule::findOrFail($id);
        return response()->json($data);
    }




}
