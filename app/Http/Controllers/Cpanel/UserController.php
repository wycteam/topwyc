<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\Role;
use App\Models\User;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CpanelUserValidation;

use DB;
use App\Classes\Common;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('cpanel/user/index');
    }

    public function create()
    {
        $roles = Role::all();
        $permissions = Permission::all();

        return view('cpanel.user.create', compact('roles', 'permissions'));
    }

    public function store(CpanelUserValidation $request)
    {
        $user = new User($request->all());
        $user->contact_number = $request->get('number');
        $user->is_verified = true;
        $user->save();

        // $user->addresses()->create($request->get('address'));
        // $user->numbers()->create($request->get('number'));

        $user->assignRole($request->get('roles'));
        $user->givePermission($request->get('permissions'));

        session()->flash('message', 'User has been created successfully.');

        $selectedUser = $user;
        $assignedRoles = $selectedUser->roles;
        $assignedPermissions = $selectedUser->permissions;
        $roles = Role::all();
        $permissions = Permission::all();
        session()->flash('data', $selectedUser);
        $act_id = $user->id;
        $detail = "Created new user account -> ".$user->first_name.' '.$user->middle_name.' '.$user->last_name;
        Common::saveActionLogs('User',$act_id,$detail);

        return view('cpanel/user/profile', compact('selectedUser', 'assignedRoles', 'assignedPermissions', 'roles', 'permissions'));
    }

    public function show($id)
    {
        $selectedUser = User::where('id',$id)->first();
        $assignedRoles = $selectedUser->roles;
        $assignedPermissions = $selectedUser->permissions;

        $roles = Role::all();
        $permissions = Permission::all();

        session()->flash('data', $selectedUser);

        return view('cpanel/user/profile', compact('selectedUser', 'assignedRoles', 'assignedPermissions', 'roles', 'permissions'));
    }

    public function edit(User $user)
    {
        $user->load([
            'roles', 'permissions',
            'addresses' => function ($query) {
                $query->whereType('Home');
            },
            'numbers' => function ($query) {
                $query->whereType('Home');
            },
        ]);

        return view('cpanel.user.edit', [
            'selectedUser' => $user,
            'roles' => Role::all(),
            'permissions' => Permission::all(),
        ]);
    }

    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'first_name' => 'bail|required|string|max:255',
            'last_name' => 'bail|required|string|max:255',
            'email' => 'bail|required|string|email|max:255|unique:users,email,'.$user->id,
            'password' => 'bail|nullable|string|min:6|max:255|confirmed',
            'birth_date' => 'bail|required|date',
            'gender' => 'bail|required|string',
            'address' => 'bail|required|string',
            'contact_number' => 'required',
            'roles' => 'bail|required|array',
            'permissions' => 'bail|required|array',
        ], [
            'address.required' => 'Address is required.',
            'contact_number.required' => 'Contact Number is required.',
        ]);

        if ($request->has('password')) {
            $user->contact_number = $request->input('number');
            $user->fill($request->all());
        } else {
            $user->fill($request->except('password'));
        }

        if($user->isDirty()){
            //getOriginal() -> get original values of model
            //getDirty -> get all fields updated with value
            $changes = $user->getDirty();
            $detail = "Updated user Account.";
            foreach ($changes as $key => $value) {
                $old = $user->getOriginal($key);
                $act_id = $user->id;
                $detail .= "Change ".$key." from ".$old." to ".$value.". ";
            }
            Common::saveActionLogs('User',$act_id,$detail); // save to action logs
            $user->save(); // save changes to model
        }

        // $user->addresses()->updateOrCreate(['id' => $request->input('address.id')], $request->input('address'));
        // $user->numbers()->updateOrCreate(['id' => $request->input('number.id')], $request->input('number'));
        
        //Roles & Permissions
        $arr1 = $user->roles()->pluck('id')->toArray(); // get all user roles
        $arr2 = $request->get('roles'); // get new user roles
        //check if theres changes
        //dd(count(array_diff($arr1, $arr2))." ".count(array_diff($arr2, $arr1)));
        $act_id = $user->id;
        //if roles reduced
        if(count(array_diff($arr1, $arr2))>0){
            $diff = array_diff($arr1, $arr2);
            $detail = "Removed Role/s ";
            foreach ($diff as $key => $value) {
                $role = Role::where('id',$value)->select('label')->first();
                $detail .=$role->label.", ";
            }
            Common::saveActionLogs('User',$act_id,$detail); // save to action logs
        }
        // if roles added
        if(count(array_diff($arr2, $arr1))>0){
            $diff = array_diff($arr2, $arr1);
            $detail = "Added Role/s ";
            foreach ($diff as $key => $value) {
                $role = Role::where('id',$value)->select('label')->first();
                $detail .=$role->label.", ";
            }
            Common::saveActionLogs('User',$act_id,$detail);
        }

        $user->roles()->sync($request->get('roles'));
        $user->permissions()->sync($request->get('permissions'));
        session()->flash('message', 'User has been updated successfully.');
        return back();
    }

    public function updateRolesAndPermissions(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $roleIds = $request->get('roles');
        $permissionIds = $request->get('permissions');

        $user->roles()->sync($roleIds);
        $user->permissions()->sync($permissionIds);

        return back();
    }

    public function destroy(Request $request, $id)
    {
        $data = User::findOrFail($id);
        $data->delete();

        return back();
    }

    public function getUsersByRole($role) {
        $role = DB::table('roles')->where('name', $role)->first();
        $roleUser = DB::table('role_user')->select(array('user_id'))->where('role_id', $role->id)->get();
        $users = [];
        foreach($roleUser as $ru) {
            $user = DB::table('users')->select(array('first_name', 'last_name', 'contact_number'))->where('id', $ru->user_id)->first();

            if($user) {
                $users[] = [
                    'first_name' => $user->first_name,
                    'last_name' => $user->last_name,
                    'contact_number' => $user->contact_number
                ];
            }
        }

        return $users;
    }

}
