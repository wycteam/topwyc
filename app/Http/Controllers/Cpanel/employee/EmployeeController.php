<?php

namespace App\Http\Controllers\Cpanel\employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CpanelUserValidation;

use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\Models\Visa\ActionLogs;
use App\Models\CalendarEvents as eve;

use App\Classes\Common;

use Image, DB, Carbon\Carbon;
use MaddHatter\LaravelFullcalendar\Event;

class EmployeeController extends Controller
{

	public function index() {
        $permissions = User::with('roles.permissions')->whereId(Auth::id())->get();
        $check = 0;
        foreach($permissions[0]->permissions as $p) {
            if($p->name == 'Employees') {
                $check = 1;
            }
        }

        if($check==1)
            return view('cpanel/employee/accounts/index');
        else
            abort(404);
	}

	public function create() {

        $data = [
            'title' => 'Create New Account'
        ];

        $roles = Role::all();
        $permissions = Permission::all();

        return view('cpanel.employee.accounts.create', $data, compact('roles', 'permissions'));

	}

    public function store(CpanelUserValidation $request)
    {
        $user = new User($request->all());
        $user->date_register = Carbon::now()->format('m/d/Y');
        $user->is_verified = true;
        $user->save();

        // $user->addresses()->create($request->get('address'));

        $user->assignRole($request->get('roles'));
        $user->givePermission($request->get('permissions'));

        session()->flash('message', 'Account has been created successfully.');

        $account =  User::with('roles','permissions')->whereId($user->id)->get();

        $assignedRoles = $account[0]->roles;
        $assignedPermissions = $account[0]->permissions;

        $roles = Role::all();
        $permissions = Permission::all();
        session()->flash('data', $account[0]);
        $act_id = $user->id;
        $detail = "Created new user account -> ".$user->first_name.' '.$user->middle_name.' '.$user->last_name;
        Common::saveActionLogs(0,$act_id,$detail);

        $data = [
            'title' => 'Edit Account'
        ];

        return view('cpanel/visa/accounts/edit', $data, compact('account', 'assignedRoles', 'assignedPermissions', 'roles', 'permissions'));

    }

    public function show($id) {
        $events = [];
        $data = eve::all();
        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = \Calendar::event(
                    $value->title,
                    true,
                    new \DateTime($value->start_date),
                    new \DateTime($value->end_date.' +1 day'),
                    null,
                    // Add color and link on event
                    [
                        'color' => '#f05050',
                        'url' => 'pass here url and any route',
                    ]
                );
            }
        }
        $calendar = \Calendar::addEvents($events);
        return view('cpanel.employee.accounts.show', compact('calendar'));
    }

    public function showDetail($id) {
        return User::with('schedule.scheduleType')->findOrFail($id);
    }

    public function edit($id)
    {
        $data = [
            'title' => 'Edit Account'
        ];

        $account =  User::with('roles','permissions')->whereId($id)->get();

        $assignedRoles = $account[0]->roles;
        $assignedPermissions = $account[0]->permissions;
        $roles = Role::all();
        $permissions = Permission::all();
        session()->flash('data', $account[0]);

        return view('cpanel.visa.accounts.edit', $data, compact('account', 'assignedRoles', 'assignedPermissions', 'roles', 'permissions'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'bail|required|string|max:255',
            'last_name' => 'bail|required|string|max:255',
            'email' => 'bail|required|string|email|max:255|unique:users,email,'.$id,
            'password' => 'bail|nullable|string|min:6|max:255|confirmed',
            'birth_date' => 'bail|required|date',
            'gender' => 'bail|required|string',
            'civil_status' => 'bail|required|string',
            'address' => 'bail|required|string',
            'contact_number' => 'bail|required_if:email,==,null',

            'roles' => 'bail|required|array',
            'permissions' => 'bail|required|array'
        ], [
            'address.required' => 'Address is required.',
            'contact_number.required_if' => 'Contact Number is required.'
        ]);

        $user = User::find($id);

        if ($request->has('password')) {
            $user->fill($request->all());
        } else {
            $user->fill($request->except('password'));
        }

        if($user->isDirty()){
            //getOriginal() -> get original values of model
            //getDirty -> get all fields updated with value
            $changes = $user->getDirty();
            $detail = "Updated user Account.";
            foreach ($changes as $key => $value) {
                $old = $user->getOriginal($key);
                $act_id = $user->id;
                $detail .= "Change ".$key." from ".$old." to ".$value.". ";
            }
            Common::saveActionLogs(0,$act_id,$detail);
            $user->save();
        }

        // $user->addresses()->updateOrCreate(['id' => $request->input('address.id')], $request->input('address'));

        
        //Roles & Permissions
        $arr1 = $user->roles()->pluck('id')->toArray(); // get all user roles
        $arr2 = $request->get('roles'); // get new user roles
        //check if theres changes
        //dd(count(array_diff($arr1, $arr2))." ".count(array_diff($arr2, $arr1)));
        $act_id = $user->id;
        //if roles reduced
        if(count(array_diff($arr1, $arr2))>0){
            $diff = array_diff($arr1, $arr2);
            $detail = "Removed Role/s ";
            foreach ($diff as $key => $value) {
                $role = Role::where('id',$value)->select('label')->first();
                $detail .=$role->label.", ";
            }
            Common::saveActionLogs(0,$act_id,$detail); // save to action logs
        }
        // if roles added
        if(count(array_diff($arr2, $arr1))>0){
            $diff = array_diff($arr2, $arr1);
            $detail = "Added Role/s ";
            foreach ($diff as $key => $value) {
                $role = Role::where('id',$value)->select('label')->first();
                $detail .=$role->label.", ";
            }
            Common::saveActionLogs(0,$act_id,$detail);
        }

        $user->roles()->sync($request->get('roles'));
        $user->permissions()->sync($request->get('permissions'));

        session()->flash('message', 'Account has been updated successfully.');
        return back();

    }

    public function updateAvatar(Request $request, $id)
    {
        $user = User::find($id);

        $width = $request->get('width');
        $height = $request->get('height');
        $image = $request->get('image');

        $img = Image::make($image)->resize($width, null, function ($constraint) {
          $constraint->aspectRatio();
        })
        ->resizeCanvas($width, $height, 'center')
        ->encode('data-url');

        $img2 = clone $img;

        Image::make($img)
          ->encode('jpg', 100)
          ->save(public_path().'/images/avatar/'.$user->id.'.jpg');

        $user->images()->create([
          'name' => $user->id.'.jpg',
          'type' => 'avatar',
        ]);

        return $img2;
    }

    public function employeeAttendance()
    {
        return view('cpanel/employee/attendance/index');
    }

    public function getAllEmployees() {
        return User::whereHas('roles', function($query) {
            $query->where('name', 'employee');
        })->get()
        ->makeHidden([
            "total_points",
            "total_deposit",
            "total_discount",
            "total_refund",
            "total_payment",
            "total_cost",
            "total_balance",
            "document_receive",
            "unread_notif",
            "group_binded",
            "access_control",
            "permissions",
            "three_days",
            "collectable",
            "total_complete_cost",
        ]);
    }

}