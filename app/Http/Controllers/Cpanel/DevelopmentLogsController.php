<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\DevelopmentLogs as devLogs;
use App\Http\Requests\DevelopmentLogsValidation;

class DevelopmentLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = User::with('roles.permissions')->whereId(Auth::id())->get();
        $check = 0;
        foreach($permissions[0]->permissions as $p) {
            if($p->name == 'Development Logs') {
                $check = 1;
            }
        }

        if($check==1)
            return view('cpanel.development-logs.index');
        else
            abort(404);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(DevelopmentLogsValidation $request)
    {
        $data = devLogs::create($request->all());
        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   
        // Show old updates
        $count = devLogs::count();
        $skip = 1;
        $limit = $count - $skip;
        $data = devLogs::orderBy('id', 'DESC')->skip($skip)->take($limit)->get();
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(DevelopmentLogsValidation $request, $id)
    {
        $data = devLogs::whereId($id)->update([
             'title'    => $request->title
            ,'body'     => $request->body
            ,'version'  => $request->version
            ,'date'     => $request->date
            ]);

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete
        $data = devLogs::findOrFail($id);
        $data->delete();
        return response()->json($data);
    }

    public function newLogs()
    {
        // show last update
        $data = devLogs::orderBy('id', 'DESC')->first();
        return response()->json($data);
    }

    public function getLog($id){
        $data = devLogs::findOrFail($id);
        return response()->json($data);
    }
}
