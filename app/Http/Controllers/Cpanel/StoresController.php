<?php

namespace App\Http\Controllers\Cpanel;

use App\Models\User;
use App\Models\Shopping\Store;
use App\Http\Controllers\Controller;

class StoresController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('cpanel/store/index');
    }
}
