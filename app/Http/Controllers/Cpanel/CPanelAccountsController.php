<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CpanelUserValidation;

use App\Models\User;
use App\Models\Schedule;
use App\Models\Role;
use App\Models\Permission;
use App\Models\Visa\ActionLogs;
use App\Models\Employee\PayrollSettings;
use App\Classes\Common;
use Image, DB, Carbon\Carbon;

class CPanelAccountsController extends Controller
{

	public function index() {
        $permissions = User::with('roles.permissions')->whereId(Auth::id())->get();
        $check = 0;
        foreach($permissions[0]->permissions as $p) {
            if($p->name == 'CPanel Accounts') {
                $check = 1;
            }
        }

        if($check==1)
            return view('cpanel/visa/accounts/index');
        else
            abort(404);
	}

	public function create() {

        $data = [
            'title' => 'Create New Account'
        ];

        $roles = Role::all();
        $permissions = Permission::all();

        return view('cpanel.visa.accounts.create', $data, compact('roles', 'permissions'));

	}

    public function store(CpanelUserValidation $request)
    {
        $user = new User($request->all());
        $user->date_register = Carbon::now()->format('m/d/Y');
        $user->is_verified = true;
        $user->save();

        $user->assignRole($request->get('roles'));
        $user->givePermission($request->get('permissions'));

        session()->flash('message', 'Account has been created successfully.');

        $account =  User::with('roles','permissions')->whereId($user->id)->get();

        $assignedRoles = $account[0]->roles;
        $assignedPermissions = $account[0]->permissions;

        $roles = Role::all();
        $permissions = Permission::all();
        session()->flash('data', $account[0]);
        $act_id = $user->id;
        $detail = "Created new user account -> ".$user->first_name.' '.$user->middle_name.' '.$user->last_name;
        Common::saveActionLogs(0,$act_id,$detail);

        $data = [
            'title' => 'Edit Account'
        ];

        return redirect('/visa/cpanel-accounts/' . $user->id . '/edit')->with([
            'data' => $data,
            'account' => $account,
            'assignedRoles' => $assignedRoles,
            'assignedPermissions' => $assignedPermissions,
            'roles' => $roles,
            'permissions' => $permissions
        ]);
    }

    public function edit($id)
    {
        if(PayrollSettings::where('user_id', $id)->count()==0){
            $pSetting = new PayrollSettings;
            $pSetting->user_id = $id;
            $pSetting->_day_from = "MONDAY";
            $pSetting->_day_to = "FRIDAY";
            $pSetting->save();
        }else{
            $ps = PayrollSettings::where('user_id', $id)->first();
            if($ps->_day_from==NULL || $ps->_day_to==NULL || $ps->_day_from=='' || $ps->_day_to==''){
                PayrollSettings::where('user_id', $id)->update(['_day_from'=>'MONDAY','_day_to'=>'FRIDAY']);
            }
        }
        $data = [
            'title' => 'Edit Account'
        ];

        $account =  User::with('roles','permissions', 'schedule.scheduleType')->whereId($id)->get();

        $assignedRoles = $account[0]->roles;
        $assignedPermissions = $account[0]->permissions;
        $roles = Role::all();
        $permissions = Permission::all();
        session()->flash('data', $account[0]);

        return view('cpanel.visa.accounts.edit', $data, compact('account', 'assignedRoles', 'assignedPermissions', 'roles', 'permissions'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'bail|required|string|max:255',
            'last_name' => 'bail|required|string|max:255',
            'email' => 'bail|required|string|email|max:255|unique:users,email,'.$id,
            'password' => 'bail|nullable|string|min:6|max:255|confirmed',
            'birth_date' => 'bail|required|date',
            'gender' => 'bail|required|string',
            'civil_status' => 'bail|required|string',

            'scheduleType' => 'required',
            'timeIn' => 'required_if:scheduleType, ==, 1',
            'timeOut' => 'required_if:scheduleType, ==, 1',
            'timeInFrom' => 'required_if:scheduleType, ==, 2',
            'timeInTo' => 'required_if:scheduleType, ==, 2',

            'address' => 'bail|required|string',
            'contact_number' => 'bail|required|string',

            'roles' => 'bail|required|array',
            'permissions' => 'bail|required|array'
        ], [
            'timeIn.required_if' => 'The time in field is required when schedule type is Fixed.',
            'timeOut.required_if' => 'The time out field is required when schedule type is Fixed.',
            'timeInFrom.required_if' => 'The time in from field is required when schedule type is Flexi.',
            'timeInTo.required_if' => 'The time in to field is required when schedule type is Flexi.',

            'address.required' => 'Address is required.',
            'contact_number.required' => 'Contact Number is required.'
        ]);

        $user = User::find($id);

        if ($request->has('password')) {
            $user->fill($request->all());
        } else {
            $user->fill($request->except('password'));
        }

        if($user->isDirty()){
            //getOriginal() -> get original values of model
            //getDirty -> get all fields updated with value
            $changes = $user->getDirty();
            $detail = "Updated user Account.";
            foreach ($changes as $key => $value) {
                $old = $user->getOriginal($key);
                $act_id = $user->id;
                $detail .= "Change ".$key." from ".$old." to ".$value.". ";
            }
            Common::saveActionLogs(0,$act_id,$detail);
            $user->save();
        }

        if($request->scheduleType == 1) {
            $data = [
                'schedule_type_id' => 1,
                'time_in' => Carbon::createFromFormat('g:i A', $request->timeIn)->toDateTimeString(),
                'time_out' => Carbon::createFromFormat('g:i A', $request->timeOut)->toDateTimeString(),
                'time_in_from' => null,
                'time_in_to' => null
            ];
        } elseif($request->scheduleType == 2) {
            $data = [
                'schedule_type_id' => 2,
                'time_in' => null,
                'time_out' => null,
                'time_in_from' => Carbon::createFromFormat('g:i A', $request->timeInFrom)->toDateTimeString(),
                'time_in_to' => Carbon::createFromFormat('g:i A', $request->timeInTo)->toDateTimeString()
            ];
        } elseif($request->scheduleType == 3) {
            $data = [
                'schedule_type_id' => 3,
                'time_in' => null,
                'time_out' => null,
                'time_in_from' => null,
                'time_in_to' => null
            ];
        }

        if($request->scheduleType != 0) {
            Schedule::updateOrCreate(
                ['user_id' => $id],
                $data
            );
        }

        //Roles & Permissions
        $arr1 = $user->roles()->pluck('id')->toArray(); // get all user roles
        $arr2 = $request->get('roles'); // get new user roles
        //check if theres changes
        //dd(count(array_diff($arr1, $arr2))." ".count(array_diff($arr2, $arr1)));
        $act_id = $user->id;
        //if roles reduced
        if(count(array_diff($arr1, $arr2))>0){
            $diff = array_diff($arr1, $arr2);
            $detail = "Removed Role/s ";
            foreach ($diff as $key => $value) {
                $role = Role::where('id',$value)->select('label')->first();
                $detail .=$role->label.", ";
            }
            Common::saveActionLogs(0,$act_id,$detail); // save to action logs
        }
        // if roles added
        if(count(array_diff($arr2, $arr1))>0){
            $diff = array_diff($arr2, $arr1);
            $detail = "Added Role/s ";
            foreach ($diff as $key => $value) {
                $role = Role::where('id',$value)->select('label')->first();
                $detail .=$role->label.", ";
            }
            Common::saveActionLogs(0,$act_id,$detail);
        }

        $user->roles()->sync($request->get('roles'));
        $user->permissions()->sync($request->get('permissions'));

        session()->flash('message', 'Account has been updated successfully.');
        return back();

    }

    public function updateAvatar(Request $request, $id)
    {
        $user = User::find($id);

        $width = $request->get('width');
        $height = $request->get('height');
        $image = $request->get('image');

        $img = Image::make($image)->resize($width, null, function ($constraint) {
          $constraint->aspectRatio();
        })
        ->resizeCanvas($width, $height, 'center')
        ->encode('data-url');

        $img2 = clone $img;

        Image::make($img)
          ->encode('jpg', 100)
          ->save(public_path().'/images/avatar/'.$user->id.'.jpg');

        $user->images()->create([
          'name' => $user->id.'.jpg',
          'type' => 'avatar',
        ]);

        return $img2;
    }

}