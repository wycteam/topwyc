<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class CommonController extends Controller
{
    public function resizeImage(Request $request)
    {
        $width = $request->get('width');
        $height = $request->get('height');
        $image = $request->get('image');

        ob_start();

        $img = Image::make($image)->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        })
        ->resizeCanvas($width, $height, 'center')
        ->encode('data-url');

        ob_end_clean();
        
        return $img;
    }

    public function resizeWholeImage(Request $request)
    {
        $width = $request->get('width');
        $height = $request->get('height');
        $image = $request->get('image');

        ob_start();
        
        if($request->get('type') != '' && ($request->get('type') == 'color' || $request->get('type') == 'primary')) {
            $img = Image::make($image)
                ->fit(518, 454, function ($constraint) {
                    $constraint->upsize();
                })
                ->encode('data-url');
        } elseif($request->get('type') != '' && $request->get('type') == 'description') {
            $img = Image::make($image)
                ->resize(998, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->encode('data-url');
        } else {
            $img = Image::make($image)->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->encode('data-url');
        }

        ob_end_clean();

        return $img;
    }
}
