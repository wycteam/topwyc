<?php

namespace App\Http\Controllers\Shopping\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Rates;
use App\Models\Shopping\Store;
use App\Models\Shopping\Product;
use App\Models\Shopping\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\Http\Requests\ProductValidation;

class ProductController extends Controller
{

    public function index()
    {
        $categoriesArray = [];

        $categoriesGrandParents = Category::where('parent_id', 0)->get();

        foreach($categoriesGrandParents as $categoriesGrandParent) {
            $relatedCategoriesArray = Category::where('id', $categoriesGrandParent->id)
                ->orWhere('tree_parent', $categoriesGrandParent->id)
                ->orWhere('tree_parent', 'LIKE', $categoriesGrandParent->id.'-%')->pluck('id');

            $count = Product::
                whereHas('categories', function($query) use($relatedCategoriesArray) {
                    $query->whereIn('category_id', $relatedCategoriesArray);
                })->where('is_searchable', 1)->count();

            if($count) {
                $categoriesArray[] = [
                    'name' => $categoriesGrandParent->name,
                    'cnName' => $categoriesGrandParent->name_cn,
                    'slug' => $categoriesGrandParent->slug,
                    'count' => $count
                ];
            }
        }

        $data = [
            'lang' => trans('products'),
            'brands' => Product::where('brand', '<>', null)->orderBy('brand')->groupBy('brand')->pluck('brand'),
            'selectedBrands' => explode("--", request()->input('brands')),
            'categoriesArray' => $categoriesArray
        ];

        return view('product.products', $data);
    }

    //creating a new product
    public function create()
    {
        $rates = Rates::get();
        session()->flash('rates', $rates);
        return view('product.create.create-product')
                ->with('lang',trans('products'));
    }

    public function details()
    {
        $data['user'] = Auth::user();
        return view('product.details',$data)
                ->with('lang',trans('products'));
    }

    public function store(ProductValidation $request)
    {
        $prod = DB::transaction(function () use ($request) {
            $data = new Product($request->all());
            $data->user_id = Auth::user()->id;
            $data->save();

            //save categories to category_product table
            $this->saveCategories($data, $request);

            if ($request->has('image')) {
                $this->saveImage($request->get('image'), $data->image_path, $data, 'primary');
                $this->saveImageThumbnail($request->get('image'), $data->image_path, $data, 'primary-thumbnail');
            }

            if ($request->has('image2')) {
                $this->saveImage($request->get('image2'), $data->image_path, $data, 'primary2');
            }

            if ($request->has('image3')) {
                $this->saveImage($request->get('image3'), $data->image_path, $data, 'primary3');
            }

            if ($request->has('image4')) {
                $this->saveImage($request->get('image4'), $data->image_path, $data, 'primary4');
            }

            if ($request->has('image5')) {
                $this->saveImage($request->get('image5'), $data->image_path, $data, 'primary5');
            }

            if ($request->has('image6')) {
                $this->saveImage($request->get('image6'), $data->image_path, $data, 'primary6');
            }

            if ($request->has('descimage1')) {
                $this->saveImage($request->get('descimage1'), $data->image_path, $data, 'descimage1');
            }

            if ($request->has('descimage2')) {
                $this->saveImage($request->get('descimage2'), $data->image_path, $data, 'descimage2');
            }

            if ($request->has('descimage3')) {
                $this->saveImage($request->get('descimage3'), $data->image_path, $data, 'descimage3');
            }

            if ($request->has('descimage4')) {
                $this->saveImage($request->get('descimage4'), $data->image_path, $data, 'descimage4');
            }

            if ($request->has('descimage5')) {
                $this->saveImage($request->get('descimage5'), $data->image_path, $data, 'descimage5');
            }

            if ($request->has('descimage6')) {
                $this->saveImage($request->get('descimage6'), $data->image_path, $data, 'descimage6');
            }

            return $data;
        });
        $prod->success ='success';
        return $prod;
    }

    public function show(Product $product)
    {
        $user = null;
        $view = false;
        if(Auth::user()!=null){
            $user = Auth::user();
            if($user->id == $product->user_id){
                $view = true;
            }
        }

        if($product->is_searchable){
            $view = true;
        }
        else{
            if($user == null){
                $view = false;
            }
            else{
                $canview = $user->all_orders->where('product_id',$product->id)->count();
                if($canview > 0){
                    $view = true;
                }
            }
        }

        if($view){
            session()->flash('data', $product);
            return view('product.details', compact('product'))
                    ->with('lang',trans('products'));
        }
        else{
            return abort(404);
        }
    }

    public function edit(Product $product)
    {
        $product->load(['store', 'categories']);
        //save product data to session
        $rates = Rates::get();
        session()->flash('rates', $rates);
        session()->flash('data', $product);
        return view('product.create.edit-product', compact('product'))
                ->with('lang',trans('products'));
    }

    public function update(ProductValidation $request, Product $product)
    {
        authorize('products.update', $product);

        $product = DB::transaction(function () use ($request, $product) {
            // $product->fill($request->all())
            //     ->save();

            $product = $product->fill($request->all());
            // $product->is_draft = 0;
            // $product->is_active = 1;
            $product->save();
            
            $product->color_variations()->delete();
            $product->size_variations()->delete();
            $product->images()->delete();

            if (! empty($varcolor = $request->get('varcolor'))) {
                foreach ($varcolor as $vcolor) {
                    if($vcolor['label']!=''){
                        $color = $product->color_variations()
                            ->updateOrCreate(['id' => $vcolor['id']], $vcolor);

                            if($vcolor['temp_id']==1){
                                if (! empty($varsize = $request->get('varsize1'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 1, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==2){
                                if (! empty($varsize = $request->get('varsize2'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 2, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==3){
                                if (! empty($varsize = $request->get('varsize3'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 3, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==4){
                                if (! empty($varsize = $request->get('varsize4'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 4, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==5){
                                if (! empty($varsize = $request->get('varsize5'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 5, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==6){
                                if (! empty($varsize = $request->get('varsize6'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 6, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==7){
                                if (! empty($varsize = $request->get('varsize7'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 7, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==8){
                                if (! empty($varsize = $request->get('varsize8'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 8, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==9){
                                if (! empty($varsize = $request->get('varsize9'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 9, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==10){
                                if (! empty($varsize = $request->get('varsize10'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 10, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }

                    }
                }
            }

            //save categories to category_product table
            $this->saveCategories($product, $request);

            if ($request->has('image')) {
                $this->saveImage($request->get('image'), $product->image_path, $product, 'primary');
                $this->saveImageThumbnail($request->get('image'), $product->image_path, $product, 'primary-thumbnail');
            }

            if ($request->has('image2')) {
                $this->saveImage($request->get('image2'), $product->image_path, $product, 'primary2');
            }

            if ($request->has('image3')) {
                $this->saveImage($request->get('image3'), $product->image_path, $product, 'primary3');
            }

            if ($request->has('image4')) {
                $this->saveImage($request->get('image4'), $product->image_path, $product, 'primary4');
            }

            if ($request->has('image5')) {
                $this->saveImage($request->get('image5'), $product->image_path, $product, 'primary5');
            }

            if ($request->has('image6')) {
                $this->saveImage($request->get('image6'), $product->image_path, $product, 'primary6');
            }

            if ($request->has('descimage1')) {
                $this->saveImage($request->get('descimage1'), $product->image_path, $product, 'descimage1');
            }

            if ($request->has('descimage2')) {
                $this->saveImage($request->get('descimage2'), $product->image_path, $product, 'descimage2');
            }

            if ($request->has('descimage3')) {
                $this->saveImage($request->get('descimage3'), $product->image_path, $product, 'descimage3');
            }

            if ($request->has('descimage4')) {
                $this->saveImage($request->get('descimage4'), $product->image_path, $product, 'descimage4');
            }

            if ($request->has('descimage5')) {
                $this->saveImage($request->get('descimage5'), $product->image_path, $product, 'descimage5');
            }

            if ($request->has('descimage6')) {
                $this->saveImage($request->get('descimage6'), $product->image_path, $product, 'descimage6');
            }

            if ($request->has('descimage7')) {
                $this->saveImage($request->get('descimage7'), $product->image_path, $product, 'descimage7');
            }

            if ($request->has('descimage8')) {
                $this->saveImage($request->get('descimage8'), $product->image_path, $product, 'descimage8');
            }

            if ($request->has('descimage9')) {
                $this->saveImage($request->get('descimage9'), $product->image_path, $product, 'descimage9');
            }

            if ($request->has('descimage10')) {
                $this->saveImage($request->get('descimage10'), $product->image_path, $product, 'descimage10');
            }

            if ($request->has('descimage11')) {
                $this->saveImage($request->get('descimage11'), $product->image_path, $product, 'descimage11');
            }

            if ($request->has('descimage12')) {
                $this->saveImage($request->get('descimage12'), $product->image_path, $product, 'descimage12');
            }

            if ($request->has('descimage13')) {
                $this->saveImage($request->get('descimage13'), $product->image_path, $product, 'descimage13');
            }

            if ($request->has('descimage14')) {
                $this->saveImage($request->get('descimage14'), $product->image_path, $product, 'descimage14');
            }

            if ($request->has('descimage15')) {
                $this->saveImage($request->get('descimage15'), $product->image_path, $product, 'descimage15');
            }

            if ($request->has('descimage16')) {
                $this->saveImage($request->get('descimage16'), $product->image_path, $product, 'descimage16');
            }

            if ($request->has('descimage17')) {
                $this->saveImage($request->get('descimage17'), $product->image_path, $product, 'descimage17');
            }

            if ($request->has('descimage18')) {
                $this->saveImage($request->get('descimage18'), $product->image_path, $product, 'descimage18');
            }

            if ($request->has('descimage19')) {
                $this->saveImage($request->get('descimage19'), $product->image_path, $product, 'descimage19');
            }

            if ($request->has('descimage20')) {
                $this->saveImage($request->get('descimage20'), $product->image_path, $product, 'descimage20');
            }

            if ($request->has('colorImage1')) {
                $this->saveImage2($request->get('colorImage1'), $product->image_path_color, 'color1');
            }

            if ($request->has('colorImage2')) {
                $this->saveImage2($request->get('colorImage2'), $product->image_path_color, 'color2');
            }

            if ($request->has('colorImage3')) {
                $this->saveImage2($request->get('colorImage3'), $product->image_path_color, 'color3');
            }

            if ($request->has('colorImage4')) {
                $this->saveImage2($request->get('colorImage4'), $product->image_path_color, 'color4');
            }

            if ($request->has('colorImage5')) {
                $this->saveImage2($request->get('colorImage5'), $product->image_path_color, 'color5');
            }

            if ($request->has('colorImage6')) {
                $this->saveImage2($request->get('colorImage6'), $product->image_path_color, 'color6');
            }

            if ($request->has('colorImage7')) {
                $this->saveImage2($request->get('colorImage7'), $product->image_path_color, 'color7');
            }

            if ($request->has('colorImage8')) {
                $this->saveImage2($request->get('colorImage8'), $product->image_path_color, 'color8');
            }

            if ($request->has('colorImage9')) {
                $this->saveImage2($request->get('colorImage9'), $product->image_path_color, 'color9');
            }

            if ($request->has('colorImage10')) {
                $this->saveImage2($request->get('colorImage10'), $product->image_path_color, 'color10');
            }

            return $product;
        });

        return $product;
    }

    public function destroy(Product $product)
    {
        authorize('products.delete', $product);
        $product->delete();

        // return redirect()->route('shopping.stores.index');

        return back();
    }


    ////////////////////////
    /* Private Functions */
    //////////////////////


    private function saveCategories($data, $request)
    {
        if ($request->has('category_id')) {
            $categoryIds = collect($request->get('category_id'))
                ->pluck('id')
                ->toArray();

            $data->categories()->sync($categoryIds);
        }
    }

    private function saveImage($image, $path, $data, $type)
    {
        $resizeThis1 = ['primary', 'primary2', 'primary3', 'primary4', 'primary5', 'primary6'
        ];

        $resizeThis2 = ['descimage1', 'descimage2', 'descimage3', 'descimage4', 'descimage5', 'descimage6', 'descimage7', 'descimage8', 'descimage9', 'descimage10', 'descimage11', 'descimage12', 'descimage13', 'descimage14', 'descimage15', 'descimage16', 'descimage17', 'descimage18', 'descimage19', 'descimage20'];

        $fileName = $type . '.jpg';

        if(in_array($type, $resizeThis1)) {
            Image::make($image)
                ->fit(518, 454, function ($constraint) {
                    $constraint->upsize();
                })
                ->encode('jpg', 100)
                ->save($path . $fileName);
        } elseif(in_array($type, $resizeThis2)) {
            Image::make($image)
                ->resize(998, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->encode('jpg', 100)
                ->save($path . $fileName);
        } else {
            Image::make($image)
                ->encode('jpg', 100)
                ->save($path . $fileName);    
        }

        $data->images()->create([
            'name' => $fileName,
            'type' => $type,
        ]);
    }

    private function saveImage2($image, $path, $type)
    {
        $resizeThis = ['color1', 'color2', 'color3', 'color4', 'color5', 'color6', 'color7', 'color8', 'color9', 'color10'];

        $fileName = $type . '.jpg';

        if(in_array($type, $resizeThis)) {
            Image::make($image)
                ->fit(518, 454, function ($constraint) {
                    $constraint->upsize();
                })
                ->encode('jpg', 100)
                ->save($path . $fileName);
        } else {
            Image::make($image)
                ->encode('jpg', 100)
                ->save($path . $fileName);
        }
    }

    private function saveImageThumbnail($image, $path, $data, $type)
    {
        $fileName = $type . '.jpg';

        Image::make($image)->resize(480, null, function ($constraint) {
            $constraint->aspectRatio();
        })
        ->resizeCanvas(320, 280, 'center')
        ->encode('jpg', 100)
        ->save($path . $fileName);

        $data->images()->create([
            'name' => $fileName,
            'type' => $type,
        ]);
    }
}
