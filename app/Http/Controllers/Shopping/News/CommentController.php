<?php

namespace App\Http\Controllers\Shopping\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Visa\Comment as Comment;
use App\Models\Visa\CommentVote as CommentVote;
use App\Models\Visa\CommentSpam as CommentSpam;
use App\Models\Visa\CommentAuthor as CommentAuthor;

use App\Models\User as User;
use auth;
class CommentController extends Controller
{
  public function index($pageId){

       //

       $comments = Comment::where('page_id',$pageId)->get();

       $commentsData = [];
       if (!empty($comments)){
       foreach ($comments as $key) {

           $user = CommentAuthor::find($key->users_id);

           $name = $user['name'];

           $replies = $this->replies($key->id);

           $photo = '/images/avatar/default.jpg';

           // dd($photo->photo_url);

           $reply = 0;

           $vote = 0;

           $voteStatus = 0;

           $spam = 0;

           if(Auth::user()){

               $voteByUser = CommentVote::where('comment_id',$key->id)->where('user_id',Auth::user()->id)->first();

               $spamComment = CommentSpam::where('comment_id',$key->id)->where('user_id',Auth::user()->id)->first();

               if($voteByUser){

                   $vote = 1;

                   $voteStatus = $voteByUser->vote;

               }

               if($spamComment){

                   $spam = 1;

               }

           }

           if(sizeof($replies) > 0){

               $reply = 1;

           }

           if(!$spam){

               array_push($commentsData,[

                   "name" => $name,

                   "photo_url" => (string)$photo,

                   "commentid" => $key->id,

                   "comment" => $key->comment,

                   "votes" => $key->votes,

                   "reply" => $reply,

                   "votedByUser" =>$vote,

                   "vote" =>$voteStatus,

                   "spam" => $spam,

                   "replies" => $replies,

                   "date" => $key->created_at->toDateTimeString()

               ]);

           }

       }
     }

       $collection = collect($commentsData);

       return $collection->sortBy('votes');

   }

   protected function replies($commentId){

       $comments = Comment::where('reply_id',$commentId)->get();

       $replies = [];

       foreach ($comments as $key) {

           $user = User::find($key->users_id);

           $name = $user['first_name'];

           $photo = '/images/avatar/default.jpg';

           $vote = 0;

           $voteStatus = 0;

           $spam = 0;

           if(Auth::user()){

               $voteByUser = CommentVote::where('comment_id',$key->id)->where('user_id',Auth::user()->user_id)->first();

               $spamComment = CommentSpam::where('comment_id',$key->id)->where('user_id',Auth::user()->user_id)->first();

               if($voteByUser){

                   $vote = 1;

                   $voteStatus = $voteByUser->vote;

               }

               if($spamComment){

                   $spam = 1;

               }

           }

           if(!$spam){

               array_push($replies,[

                   "name" => $name,

                   "photo_url" => $photo,

                   "commentid" => $key->id,

                   "comment" => $key->comment,

                   "votes" => $key->votes,

                   "votedByUser" => $vote,

                   "vote" => $voteStatus,

                   "spam" => $spam,

                   "date" => $key->created_at->toDateTimeString()

               ]);

       }

       $collection = collect($replies);

       return $collection->sortBy('votes');

   }
 }
  public function store(Request $request){

      $this->validate($request, [

      'comment' => 'required',

      'reply_id' => 'filled',

      'page_id' => 'filled',

      'users_id' => 'required',

      'name' => 'required',

      'email_address' => 'required|email'

      ]);
      $nip = trim(shell_exec("dig +short myip.opendns.com @resolver1.opendns.com"));
      if(CommentAuthor::where('ip_address',$nip)->where('name','like','%'.$request->get('name').'%')->count()==0){
        $nuser = new CommentAuthor;
        $nuser->name = $request->get('name');
        $nuser->email_address = $request->get('email_address');
        $nuser->ip_address = $nip;
        $nuser->save();
      }
      $ncomment = $request->all();
      $ncomment['users_id'] = CommentAuthor::where('ip_address',$nip)->where('name','like',$request->get('name').'%')->value('id');

      $comment = Comment::create($ncomment);


      if($comment)

          return [ "status" => "true","commentId" => $comment->id ];

  }
  public function update(Request $request, $commentId,$type){

       if($type == "vote"){

           $this->validate($request, [

           'vote' => 'required',

           'users_id' => 'required',

           ]);



           $comments = Comment::find($commentId);

           $data = [

               "comment_id" => $commentId,

               'vote' => $request->vote,

               'user_id' => $request->users_id,

           ];


           if($request->vote == "up"){

               $comment = $comments->first();

               $vote = $comment->votes;

               $vote++;

               $comments->votes = $vote;

               $comments->save();

           }


           if($request->vote == "down"){

               $comment = $comments->first();

               $vote = $comment->votes;

               $vote--;

               $comments->votes = $vote;

               $comments->save();

           }


           if(CommentVote::create($data))

               return "true";

       }

       if($type == "spam"){

           $this->validate($request, [

               'users_id' => 'required',

           ]);

           $comments = Comment::find($commentId);

           $comment = $comments->first();

           $spam = $comment->spam;

           $spam++;

           $comments->spam = $spam;

           $comments->save();

           $data = [

               "comment_id" => $commentId,

               'user_id' => $request->users_id,

           ];



           if(CommentSpam::create($data))

               return "true";

       }

   }

}
