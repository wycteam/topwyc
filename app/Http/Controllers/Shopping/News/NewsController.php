<?php

namespace App\Http\Controllers\Shopping\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Visa\News;
use Response;

class NewsController extends Controller
{
  public function index(Request $request){
    // //$news = News::orderBy('created_at', 'desc')->take(5)->get();
    // $news = News::all();
    // return Response::json($news);

    // return json_encode([
    //     'news' => News::orderBy('created_at', 'desc')->paginate(5)
    // ]);
      $ret = News::where('only_private',0)->where('news_id','!=', $request->get('id'))->orderBy('created_at', 'desc')->take(4)->offset(1)->get();
      foreach($ret as &$row){
        $row->content = base64_decode($row->content);
        }
      return  $ret;
  }
  public function getFeatured(){
    $ret = News::where('featured',1)->where('only_private',0)->orderBy('created_at', 'desc')->take(3)->get();
    foreach($ret as &$row){
        $row->content = base64_decode($row->content);
    }
   
    return $ret;
  }

  public function mostLatest(Request $request){
    $ret = News::where('only_private',0)->where('news_id','!=', $request->get('id'))->orderBy('created_at', 'desc')->take(1)->get();
    foreach($ret as &$row){
      $row->content = base64_decode($row->content);
    }
    return $ret;
  }

  public function showNews($news_id){
    $data['lang'] = trans('services');
    $data['news'] = News::where('news_id',$news_id)->get();
    $data['news_id'] = $news_id;
    return view('visa.news.index')->with('data', $data);
  }
}
