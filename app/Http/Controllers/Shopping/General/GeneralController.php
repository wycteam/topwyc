<?php

namespace App\Http\Controllers\Shopping\General;

use App\Classes\Common;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use DB;
use Auth;
use Image;
use App\Notifications\EmailVerification;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Models\User;
use App\Models\UserOpen;
use App\Models\Shopping\Categories;
use App\Models\Shopping\Store;
use App\Models\Shopping\Product;

use App\Models\Visa\ClientService;
use App\Models\Visa\Deposit;
use App\Models\Visa\Payment;
use App\Models\Visa\Refund;
use App\Models\Visa\Discount;
use App\Models\Visa\Group;
use App\Models\Visa\GroupMember;
use App\Models\Visa\Report;
use App\Models\Visa\Service;
use App\Models\Visa\Albums as Albums;
use App\Models\Visa\ImageUpload as ImageUpload;
use App\Models\Visa\News as News;
use App\Models\Visa\ClientTransactions;
use Activity;

use App\Http\Requests\OpenRegisterValidation;

class GeneralController extends Controller
{

	protected $res = [];
    private $tree = [];
    private $categories = [];


	public function __construct()
    {
        // Common::feedSidebarCategoryContents($this->res);
        // Common::feedFooterPopularProducts($this->res);

    }
    public function index()
    {
        // $data['featStore']= DB::table('shopping.products as p')
        //          ->leftJoin('shopping.stores as s', 'p.store_id', '=', 's.id')
        //          ->leftJoin('wyc.users as u', 's.user_id', '=', 'u.id')
        //          ->select(DB::raw("s.*,u.first_name as 'first_name',u.last_name as 'last_name',u.is_verified as 'is_verified', SUM(p.sold_count) as 'sold count', COUNT(p.id) as 'products_count'"))
        //          ->where('s.is_active',1)
        //          ->orderBy(DB::raw("SUM(p.sold_count)"),'DESC')
        //          ->limit(4)
        //          ->groupBy('s.id')
        //          ->get();
       
        return view('home', ['lang'=>trans('home'),'newsCount'=>News::where('only_private',0)->count()]);
    }

    public function openRegistration($id = null)
    {
        return view('register', ['lang'=>trans('register')]);
    }

    public function getOpenRegTranslation()
    {
       return json_encode([
            'translation' => trans('register')
        ]);
    }

    public function iosPage($id = null)
    {
        return view('ios-page', ['lang'=>trans('register')]);
    }

    public function androidPage($id = null)
    {
        return view('android-page', ['lang'=>trans('android-page')]);
    }

    public function about()
    {
        return view('about', ['lang'=>trans('about')]);
    }

    public function termsCondition()
    {
        return view('terms-conditions');
    }

    public function privacyPolicy()
    {
        return view('privacy-policy');
    }

    public function faq()
    {
        return view('faq');
    }

    public function faqContent()
    {
        return view('includes.faq.faq-contents');
    }

    public function notifications()
    {
        return view('notifications');
    }

    public function favorites()
    {
        if(!Auth::check())
        return redirect('/');

        return view('favorites')
                ->with('lang',trans('favorites'));

    }

    public function compare()
    {
        if(!Auth::check())
        return redirect('/');

        return view('compare')
                ->with('lang',trans('compare'));
    }

    public function categories()
    {
        return view('categories');
    }

    public function setLocale(Request $request, $locale = 'en') {

        return response('Locale has been changed to ' . $locale, 200)
            ->withCookie(cookie()->forever('locale', $locale));
    }

    public function getTranslation($lang_file)
    {
        $data = trans($lang_file);
        if(is_array($data))
        {
            $return['message'] = 'File found';
            $return['data'] = $data;
            return response()->json($return);
        }else
        {
            $message['message'] = 'Language file not found';

            return response()->json($message);

        }
    }

    public function sitemap()
    {
        return view('sitemap');
    }

    //VISA PAGES

    //Open registration
     public function getDecodedReferral($code)
     {
        $id = base64_decode($code);
        $usr = User::where('id',$id)->first();
        $found = [];
        $found['found'] = false;
        if($usr){
            $found['id'] = $usr->id; 
            $found['fullname'] = $usr->first_name.' '.$usr->last_name; 
            $found['found'] = true; 
        }
        return response()->json($found);
     }

    public function saveRegistration(OpenRegisterValidation $request)
    {
        $num = $request->cp_num;
        $q1 = $request->first_name;
        $q2 = $request->last_name;
        preg_match_all('!\d+!', $num, $matches);
        $num = implode("", $matches[0]);
        $num = ltrim($num,"0");
        $num = ltrim($num,'+');
        $num = ltrim($num,'63');
        $checkUser = User::where('contact_number', 'like', '%'.$num)
                            ->where(function ($query) use($q1,$q2) {
                                $query->where('first_name', '=', $q1)
                                      ->Where('last_name', '=', $q2);
                            })->orwhere(function ($query) use($q1,$q2) {
                                $query->where('last_name', '=', $q1)
                                      ->Where('first_name', '=', $q2);
                            })->first();

        $limit = 4;
        //$unqCode = strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit));
        $unqCode = str_pad(rand(0, pow(10, $limit)-1), $limit, '0', STR_PAD_LEFT);
        // $unqCode = '1234';
        $open = [];
        $open['success'] = false;

        // $d = explode("/", $request->bday);
        // $month = $request->month;
        // $day = $request->day;
        // $year = $request->year;
        // $bday = $year . '-' . $month . '-' . $day;
        

        // $bday = $d[2].'-'.$d[0].'-'.$d[1];
        // \Log::info($request->bday);
        if(!$checkUser){       
            $open = DB::transaction(function () use ($request, $unqCode) {
                $data = new UserOpen($request->all());
                $data->verification_code = $unqCode;
                $data->save();
                $data['success'] = true;
                return $data;
            });

            // SEND SMS
            $username = 'jepandan';
            $password = 'w8913720';
            $body = 'Your One-Time PIN is ( '.$unqCode.' ) . For your safety, never share this PIN to anyone. Please call WYC if you did not request for a PIN. - '.date("h:i:s");
            //$body = $unqCode;
            //get last 10 digits of contact number entered
            $login = $request->cp_num;
            preg_match_all('!\d+!', $login, $matches);
            $login = implode("", $matches[0]);
            $login = ltrim($login,"0");
            $login = ltrim($login,'+');
            $login = ltrim($login,'63');

            $messages = array(
                array('to'=>'+63'.$login, 'body'=>$body)
            );

            $result = $this->send_message( json_encode($messages), 'https://api.bulksms.com/v1/messages', $username, $password );

            if ($result['http_status'] != 201) {
                $msg = "Error sending.  HTTP status " . $result['http_status'];
                $msg .=  " Response was " .$result['server_response'];
            } else {
                $msg = "Response " . $result['server_response'];
                // Use json_decode($result['server_response']) to work with the response further
            } 
            //$jsonArray = json_decode($result['server_response']);    
            //$msgID = $jsonArray[0]->id;  
            // \Log::info($login.' : '.$jsonArray[0]->id);
            $open['msg'] = $msg;
        }
        return $open;
    }

    
    public function resendCode(Request $request)
    {
        $open = [];
        $open['success'] = false;
        $checkUser = UserOpen::findorfail($request->user_id);
        if($checkUser){
            $unqCode = $checkUser->verification_code;
            // SEND SMS
            $username = 'jepandan';
            $password = 'w8913720';
            $body = 'Your One-Time PIN is ( '.$unqCode.' ) . For your safety, never share this PIN to anyone. Please call WYC if you did not request for a PIN. - '.date("h:i:s");
            //$body = $unqCode;
            //get last 10 digits of contact number entered
            $login = $checkUser->cp_num;
            preg_match_all('!\d+!', $login, $matches);
            $login = implode("", $matches[0]);
            $login = ltrim($login,"0");
            $login = ltrim($login,'+');
            $login = ltrim($login,'63');

            $messages = array(
                array('to'=>'+63'.$login, 'body'=>$body)
            );

            $result = $this->send_message( json_encode($messages), 'https://api.bulksms.com/v1/messages', $username, $password );

            if ($result['http_status'] != 201) {
                $msg = "Error sending.  HTTP status " . $result['http_status'];
                $msg .=  " Response was " .$result['server_response'];
            } else {
                $msg = "Response " . $result['server_response'];
                // Use json_decode($result['server_response']) to work with the response further
            }        
            //$jsonArray = json_decode($result['server_response']);    
            //$msgID = $jsonArray[0]->id;  
            // \Log::info($login.' : '.$jsonArray[0]->id);
            $open['success'] = true;
            $open['msg'] = $msg;
        }
        return $open;
    }

    public function saveVerification(Request $request)
    {
        $id = $request->user_id;
        $code = $request->verification;

        $data = [];
        $data['success'] = false;

        $open = UserOpen::where('id',$id)->where('verification_code',$code)->first();
        if($open){
            $vcode = $open->verification_code;
            if($vcode == trim($code)){
                $data['success'] = true;
                $cnum = $open->cp_num;
                preg_match_all('!\d+!', $cnum, $matches);
                $login = implode("", $matches[0]);
                $login = ltrim($login,"0");
                $login = ltrim($login,'+');
                $login = ltrim($login,'63');
                // check if other users are using same contact number, delete if theres any
                $found = User::where('contact_number','like','%'.$login)->update([
                        'contact_number' => null
                    ]);

                $q1 = $open->first_name;
                $q2 = $open->last_name;
                $checkUser = User::where('contact_number', 'like', '%'.$login)
                            ->where(function ($query) use($q1,$q2) {
                                $query->where('first_name', '=', $q1)
                                      ->Where('last_name', '=', $q2);
                            })->orwhere(function ($query) use($q1,$q2) {
                                $query->where('last_name', '=', $q1)
                                      ->Where('first_name', '=', $q2);
                            })->first();
                if(!$checkUser){
                    $user = new User();
                    $user->first_name = $open->first_name;
                    $user->last_name = $open->last_name;
                    $user->email = $open->email;
                    $user->password = bcrypt($open->cp_num);
                    $user->birth_date = $open->bday;
                    $user->gender = $open->gender;
                    $user->civil_status = 'Single';
                    $user->date_register = Carbon::now()->format('m/d/Y');
                    $user->group_id = 0;
                    $user->is_verified = true;
                    $user->contact_number = $open->cp_num;
                    $user->save();

                    $leader = $open->referral_id;
                    $group = Group::where('leader',$leader)->where('is_shop',1)->orderBy('id','Desc')->first();
                    if($group){                    
                        GroupMember::create([
                            'group_id' => $group->id,
                            'client_id' => $user->id,
                            'leader' => 0
                        ]);

                        $memberLabel = '[' . $user->id . '] ' . $user->first_name.' '.$user->last_name;
                        $details = 'Added <strong>' . $memberLabel . '</strong> as new member of the group.';
                        Common::saveActionLogs(0, 0, $details, $id);
                    }

                    // $addressesArray = [];
                    // $addressesArray['contact_number'] = $open->cp_num;
                    // $user->addresses()->create($addressesArray);

                    $user->assignRole(['0' => 9]);

                    $open->is_verified = 1;
                    $open->client_id = $user->id;
                    $open->save();

                    $act_id = $user->id;
                    $detail = "Created new client -> ".$user->first_name.' '.$user->middle_name.' '.$user->last_name;
                    Common::saveActionLogs(0,$act_id,$detail);
                }
            }
        }
        return $data;

    }

    public function send_message ( $post_body, $url, $username, $password) {
        $ch = curl_init( );
        $headers = array(
          'Content-Type:application/json',
          'Authorization:Basic '. base64_encode("$username:$password")
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, 1 );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $post_body );
        // Allow cUrl functions 20 seconds to execute
        curl_setopt ( $ch, CURLOPT_TIMEOUT, 20 );
        // Wait 10 seconds while trying to connect
        curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
        $output = array();
        $output['server_response'] = curl_exec( $ch );
        $curl_info = curl_getinfo( $ch );
        $output['http_status'] = $curl_info[ 'http_code' ];
        curl_close( $ch );
        return $output;
    } 

    //end open registration

    public function services(){
        $data['services'] = Service::where('parent_id', 0)->orderBy('detail')->get();
        $data['lang'] = trans('services');
        return view('visa.services.index')->with('data', $data);
    }

    public function contactUs(){
        return view('visa.contactUs.index',['lang'=> trans('contactus')]);
    }

    public function tracking(){
        return view('visa.tracking.index', ['lang'=> trans('tracking')]);
    }

    public function quiz(){
        return view('visa.quiz.index');
    }

    public function quiz2(){
        return view('visa.quiz.index2');
    }

    public function viewGallery(){
        return view('visa.gallery.index');
    }
		public function showAlbum($id){
				$data['images']=ImageUpload::where('album_id',$id)->get();
				$data['gallery']=Albums::select('title')->where('id',$id)->first();
				return view('visa.gallery.index',$data);
		}
		public function getAlbums(){
			return Albums::all();
			//return response()->json(Albums::all());
		}
		public function getImages(){
			return ImageUpload::orderBy('id','desc')->get();
			//return response()->json(Albums::all());
		}

    public function getTracking($id){
        $data['tracking'] = $id;
        $data['str'] = substr($id, 0, 2);
        if($data['str'] != 'GL'){
            $data['type'] = 'package';
            $services = ClientService::with('user','discount', 'report')->where('tracking', strtoupper($id))
                ->where('active', 1)
                ->orderBy('id', 'desc')
                ->get();
            if(count($services) != 0){
                $data['services'] = $services;
                $cost = 0;
                $tip = 0;
                $charge = 0;
                $total_service_cost = 0;
                foreach ($services as $service) {
                    $cost = floatval($service->cost);
                    $charge = floatval($service->charge);
                    $tip = floatval($service->tip);
                    $total_service_cost = $cost + $charge + $tip ;
                    $service['total_service_cost'] = $total_service_cost;
                }

                if(count($services)!=0){

                    $data['client'] = $services[0]->user->first_name . ' ' . $services[0]->user->last_name;
                    $data['client_id'] = $services[0]->user->id;


                    $data['group'] = Group::find($services[0]->user->group_id);

                    $package_cost = ClientService::select(DB::raw('sum(cost+charge+tip) as total'))
                        ->where('tracking', strtoupper($id))
                        ->where('active', 1)
                        ->get();

                    $data['package_cost'] = $package_cost[0]->total;

                    $package_deposit = ClientTransactions::where('group_id', $services[0]->user->group_id)
                        ->where('tracking', strtoupper($id))
                        ->where('type', 'Deposit')
                        ->sum('amount');

                    $data['package_deposit'] = $package_deposit;

                    $package_payment = ClientTransactions::where('group_id', $services[0]->user->group_id)
                        ->where('tracking', strtoupper($id))
                        ->where('type', 'Payment')
                        ->sum('amount');

                    $data['package_payment'] = $package_payment;

                    $package_refund = ClientTransactions::where('group_id', $services[0]->user->group_id)
                        ->where('tracking', strtoupper($id))
                        ->where('type', 'Refund')
                        ->sum('amount');

                    $data['package_refund'] = $package_refund;

                    $package_discount = ClientTransactions::where('group_id', $services[0]->user->group_id)
                        ->where('tracking', strtoupper($id))
                        ->where('type', 'Discount')
                        ->sum('amount');

                    $data['package_discount'] = $package_discount;

                    $data['available_balance'] = ((($data['package_deposit'] + $data['package_payment'] + $data['package_discount']) - $data['package_refund']) - $data['package_cost']);
                }
                $data['no_results'] = false;
            }else{
                $data['no_results'] = true;
            }

        }elseif($data['str'] == 'GL'){
            $data['type'] = 'group';
            $groups = Group::with('groupmember.client','groupmember.services.discount','groupmember.services.report')->where('tracking', strtoupper($id))->get();
            $data['groups'] = $groups;
            $data['group_name'] = $groups[0]->name;
            $data['groupmembers'] = $groups[0]['groupmember'];
            $data['group_leader'] = $groups[0]->leader;

            foreach ($data['groupmembers'] as $member) {
                $id = $member->client_id;
                $cost = 0;
                $tip = 0;
                $charge = 0;
                $total_service_cost = 0;
                $active = 0;
                foreach ($member['services'] as $services) {
                    if($services->group_id != 0){
                        if($services->active == 1){
                            $cost += floatval($services->cost);
                            $charge += floatval($services->charge);
                            $tip += floatval($services->tip);
                            $active += 1;
                        }
                    }
                    $total_service_cost = $cost + $charge + $tip;
                }
                $member['total_service_cost'] = $total_service_cost;
                $member['total_services'] = $active;
            }

            if(count($groups)!=0){
                $totalcost = ClientService::
                select(DB::raw('sum(cost+charge+tip) as total'))
                    ->where('group_id', $groups[0]->id)
                    ->where('active', 1)
                    ->get();

                $data['total_cost'] = $totalcost[0]->total;

                $package_deposit = ClientTransactions::
                    where('group_id', $groups[0]->id)
                    ->where('type', 'Deposit')
                    ->sum('amount');

                $data['package_deposit'] = $package_deposit;

                $package_payment = ClientTransactions::
                    where('group_id', $groups[0]->id)
                    ->where('type', 'Payment')
                    ->sum('amount');

                $data['package_payment'] = $package_payment;

                $package_refund = ClientTransactions::
                    where('group_id', $groups[0]->id)
                    ->where('type', 'Refund')
                    ->sum('amount');

                $data['package_refund'] = $package_refund;

                $package_discount = ClientTransactions::
                    where('group_id', $groups[0]->id)
                    ->where('type', 'Discount')
                    ->sum('discount_amount');

                $data['package_discount'] = $package_discount;

                $data['available_balance'] = ((($data['package_deposit'] + $data['package_payment'] + $data['package_discount']) - $data['package_refund']) - $data['total_cost']);
            }
        }

        $data['lang'] =  trans('tracking');
        return view('visa.tracking.result')->with('data', $data);
    }

    public function getReports($id){
        $reports = Report::where('service_id', $id)->get();
        return response()->json($reports);
    }
}
