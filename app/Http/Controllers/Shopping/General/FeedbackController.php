<?php

namespace App\Http\Controllers\Shopping\General;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\Shopping\Feedbacks;
use Auth;
class FeedbackController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->model=new Feedbacks();
    }

    public function index(){
        $user = Auth::user();
        $data['user'] = $user;
        $data['lang'] = trans('feedback');
    	return view('feedback',$data);
    }


}
