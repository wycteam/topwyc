<?php

namespace App\Http\Controllers\Shopping\User;

use App\Models\User;
use App\Models\Rates;
use App\Models\Awb;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use data;
use Carbon\Carbon;

use App\Models\Shopping\Order;
use App\Models\User as shop_user;
use App\Models\Shopping\OrderDetail as Details;
use App\Models\Shopping\Store;
use App\Models\Shopping\Product;
use App\Models\Shopping\Ewallet;

use App\Notifications\OrderStatusUpdate;

class PurchaseReportController extends Controller
{
    public function index(){
        $rates = Rates::get();
        session()->flash('rates', $rates);
    	return view('user.purchase-report')->with('lang',trans('purchase-report'));
    }

    public function getOrders(){
    	$user = Auth::user();
    	$orders =  Order::with(['details.product.store'])
        ->whereUser_id($user->id)
        ->orderBy('id', 'desc')
        ->get();
    	return $orders;
    }

    public function getProduct($id){
    	$orders = Details::with(['order.user', 'order.details.product', 'order.details.fileImage', 'order.details.fileVideo', 'product.store', 'order.address', 'fileImage', 'fileVideo'])->find($id);
        $orders['product_code']       = substr($orders['product']['name'],0,3);
        $orders['product_name']       = $orders['product']['name'];
        $orders['product_cn_name']    = $orders['product']['cn_name']!=null?$orders['product']['cn_name'] : $orders['product']['name'];
        $orders['product_main_image'] = $orders['product']['main_image'];
        $orders['product_slug']       = $orders['product']['slug'];
        $orders['product_description'] = $orders['product']['description'];
        $orders['product_boxContent'] = $orders['product']['boxContent'];
        $orders['product_brand']      = $orders['product']['brand'];
        $orders['product_package_type']      = $orders['product']['package_type'];
        $orders['product_dimension']  = $orders['product']['length'] . ' X ' . $orders['product']['width'] . ' X ' . $orders['product']['height'];
        $orders['product_length']     = $orders['product']['length'];
        $orders['product_width']      = $orders['product']['width'];
        $orders['product_height']     = $orders['product']['height'];
        $orders['product_weight']     = $orders['product']['weight'];
        $orders['store_name']         = $orders['product']['store']['name'];
        $orders['store_slug']         = $orders['product']['store']['slug'];
        $orders['order_shipping_fee'] = $orders['order']['shipping_fee'];
        $orders['order_receiver']     = $orders['order']['name_of_receiver'];
        $orders['order_alternate']    = $orders['order']['alternate_contact_number'];
        $orders['order_contact']      = $orders['order']['contact_number'];

        $cb = explode("*", $orders['order']['address']['city']);
        $orders['full_address']         = $orders['order']['address']['address'].", ".
                                          ucwords(strtolower(trim($cb[1]))).", ".
                                          ucwords(strtolower(trim($cb[0]))).", ".
                                          $orders['order']['address']['province']." ".
                                          $orders['order']['address']['country']." ".
                                          $orders['order']['address']['zip_code'];
        $orders['order_landmark']     = $orders['order']['address']['landmark'];
        $orders['city']               = $orders['order']['address']['city'];
        $orders['province']           = $orders['order']['address']['province'];
        $orders['zip_code']           = $orders['order']['address']['zip_code'];  

        $orders['order_realname']        = $orders['order']['user']['first_name']." ".$orders['order']['user']['last_name'];
        $orders['order_address']         = $orders['full_address']; 

        $prod_id = $orders->product_id;
        $order_id = $orders->order_id;
        $stid = Product::where('id',$prod_id)->select('store_id')->first();
        $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
        $awb = Awb::where('order_id',$order_id)->whereIn('product_id',$allprods)->first(); 
        $orders['awb_id'] = ''; 
        if($awb){
            if($awb->id < 1000){
                $awb_id = "W1000".$awb->id."B"; //AWB CODE
            }
            else{
                $awb_id = "W100".$awb->id."B"; //AWB CODE
            }
            $orders['awb_id'] = $awb_id;        
        }
    	return $orders;

        $jsonData = $this->createResponseData($orders,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function getRecentProducts()
    {
        $data = Details::with(['product'])
                ->whereHas('order', function ($query) {
                    $user = Auth::user();
                    $query->where('user_id', $user->id);
                })
                ->latest()
                ->take(4)
                ->get();
        return $data;
    }

    public function updateReceive($id)
    {
        $current_time = Carbon::now()->toDateTimeString();
        $update = Details::whereId($id)->update(['status' => 'Received','received_date' => $current_time]);
        
        //add the sold product's price to the seller's ewallet
        if($update){
            $order_detail = Details::findOrFail($id);
            $seller_ewallet = $order_detail
                ->product()->first()
                ->user()->first()
                ->ewallet()->first();

            $seller_ewallet->amount += $order_detail->subtotal;
            $order_detail->balance = $seller_ewallet->amount;
            $order_detail->save();
            $seller_ewallet->save();

            //change order status to complete if all orders are received or delivered
            $this->ParentOrderUpdateStatus($order_detail->order()->first()->id);
        }
        $order_detail->product->user->notify(new OrderStatusUpdate($order_detail, Auth::user()));

        return $update;
    }

     private function ParentOrderUpdateStatus($parent_order_id){
        $co_orders = Details::where('order_id',$parent_order_id)->get();
        $order_details_count = count($co_orders);

        $order = Order::find($parent_order_id);

        $status_complete = 0;
        $status_cancelled = 0;

        foreach ($co_orders as $key => $value) {
            switch ($value->status) {
                case 'Cancelled':
                    $status_cancelled++;
                    break;
                
                case 'Pending':
                case 'In Transit':
                    break;

                case 'Received':
                case 'Delivered':
                case 'Returned':
                case 'On Hold':
                    $status_complete++;
                    break;
                default:
                    # code...
                    break;
            }
        }

        if($status_cancelled == $order_details_count)
        {
            $order->status = 'Cancelled';
            $order->save();
        }else if( ($status_complete + $status_cancelled) == $order_details_count)
        {
            $order->status = 'Complete';
            $order->save();
        }

    }

    public function returnItem(Request $request)
    {
        $orderDetail = Details::findOrFail($request->get('id'));
        $orderDetail->status = 'Returned';
        $orderDetail->reason = $request->get('reason');
        $orderDetail->case_number = $request->get('case_number');
        $orderDetail->returned_date = Carbon::now();
        $orderDetail->save();

        //Debited the shipping fee to user
        //$ewallet_bal = Ewallet::where('user_id','=',Auth::user()->id)->value('amount')->first();
        //$updated_ewallet_bal = $ewallet_bal - $request->get('shipping');
        //Ewallet::where('user_id','=', Auth::user()->id)->update(['amount' => $updated_ewallet_bal]);

        $orderDetail->product->user->notify(new OrderStatusUpdate($orderDetail, Auth::user()));

        return $orderDetail;
    }

    public function uploadFiles(Request $request, $id)
    {
        $orderDetail = Details::findOrFail($id);
        $path = '';

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $path = $request->file('file')->store('order-details/returned/'.$id, 'public');

            $orderDetail->files()->create([
                'file' => $path
            ]);
        }

        return $path;
    }
}
