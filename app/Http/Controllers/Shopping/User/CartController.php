<?php

namespace App\Http\Controllers\Shopping\User;

use Auth, Cart, Input;
use App\Notifications\NewOrderConfirmation;
use App\Notifications\NewPurchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\CartValidation;

use App\Models\Address;
use App\Models\Shopping\Ewallet;
use App\Models\Shopping\Favorites;
use App\Models\Shopping\Order;
use App\Models\Shopping\OrderDetail;
use App\Models\Shopping\Product;
use App\Models\Shopping\ProductVariationColors;
use App\Models\Shopping\ProductVariationSizes;

class CartController extends Controller
{
    protected $res = [];

    public function __construct(Cart $model)
    {
        $this->model = $model;
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function index(){
        return view('user.cart')
                ->with('lang',trans('cart'));

    }

    public function add()
    {
        $ret = [];
        $options = [];
        $input = Input::all();

        $product = Product::find($input['prod_id']);

        $options['slug'] = $product->slug;
        $options['stock'] = $product->stock;
        $options['image'] = $product->main_image;
        $options['discount'] = $product->discount;
        $options['fee_included'] = $product->fee_included;
        if($product->fee_included==1){
            $options['sale_price'] = (is_null($product->sale_price) ? number_format(round($product->price + $product->shipping_fee + $product->charge + $product->vat, 2), 2) : number_format(round($product->sale_price + $product->shipping_fee + $product->charge + $product->vat, 2), 2));
            $options['sale_price2'] = $product->sale_price;
            $options['shipping_fee'] = 0;
            $options['charge'] = 0;
            $options['vat'] = 0;
            $options['shipping_fee2'] = $product->shipping_fee;
            $options['charge2'] = $product->charge;
            $options['vat2'] = $product->vat;
        } else {
            $options['sale_price'] = (is_null($product->sale_price) ? $product->price : $product->sale_price);
            $options['sale_price2'] = $product->sale_price;
            $options['shipping_fee'] = $product->shipping_fee;
            $options['charge'] = $product->charge;
            $options['vat'] = $product->vat;
            $options['shipping_fee2'] = $product->shipping_fee;
            $options['charge2'] = $product->charge;
            $options['vat2'] = $product->vat;
        }
        $options['seller_id'] = $product->user_id;
        $options['store'] = $product->store->slug;
        $options['color'] = (isset($input['color']) ? $input['color'] : '');
        $options['size'] = (isset($input['size']) ? $input['size'] : '');
        $options['weight'] = $product->weight;
        $options['length'] = $product->length;
        $options['width'] = $product->width;
        $options['height'] = $product->height;

        $ret['error_msg'] = false;
        
        if(count($product->product_color_variations) == 0 && count($product->product_size_variations) == 0){
            if($options['stock']==0){
                $ret['error_msg'] = trans('cart.out-of-stock');
            } else if($options['stock']<$input['qty']){
                $ret['error_msg'] = trans('cart.qty-higher');
            } else {
                $proceed = true;
                if($this->user){
                    if($product->user_id == $this->user->id){
                        $ret['error_msg'] = $product->name . ' ' . trans('cart.cannot-be-added');
                        $proceed = false;
                    }
                }

                if($proceed)
                {   
                    if($this->isOwnerVerified($product)==true && $this->isOwnerBalanceNotFull($product)==true){
                        Cart::add($product->id, $product->name, (int)$input['qty'], (float)$product->price, $options);
                    }
                    else if($this->isOwnerVerified($product)==false)
                    {
                        $ret['error_msg'] = trans('cart.owner');
                    }
                    else if($this->isOwnerBalanceNotFull($product)==false)
                    {
                        $ret['error_msg'] = trans('cart.seller');
                    }
                }
            }
        } else {
            $ret['error_msg'] = 'Redirect';
            $ret['slug'] = $product->slug;           
        }


        $ret['cart_item_count'] = Cart::count(false);
        return $ret;
    }

    public function add2()
    {
        $ret = [];
        $options = [];
        $input = Input::all();

        $product = Product::find($input['prod_id']);

        $options['slug'] = $product->slug;
        $options['stock'] = (isset($input['colorStock']) ? $input['colorStock'] : $product->stock);
        $options['image'] = (isset($input['colorImage']) ? $input['colorImage'] : $product->main_image);
        $options['discount'] = $product->discount;
        $options['fee_included'] = $product->fee_included;
        if($product->fee_included==1){
            $options['sale_price'] = (is_null($product->sale_price) ? number_format(round($product->price + $product->shipping_fee + $product->charge + $product->vat, 2), 2) : number_format(round($product->sale_price + $product->shipping_fee + $product->charge + $product->vat, 2), 2));
            $options['sale_price2'] = $product->sale_price;
            $options['shipping_fee'] = 0;
            $options['charge'] = 0;
            $options['vat'] = 0;
            $options['shipping_fee2'] = $product->shipping_fee;
            $options['charge2'] = $product->charge;
            $options['vat2'] = $product->vat;  
        } else {
            $options['sale_price'] = (is_null($product->sale_price) ? $product->price : $product->sale_price);
            $options['sale_price2'] = $product->sale_price;
            $options['shipping_fee'] = $product->shipping_fee;
            $options['charge'] = $product->charge;
            $options['vat'] = $product->vat;
            $options['shipping_fee2'] = $product->shipping_fee;
            $options['charge2'] = $product->charge;
            $options['vat2'] = $product->vat;              
        }
        $options['seller_id'] = $product->user_id;
        $options['store'] = $product->store->slug;
        $options['color_id'] = (isset($input['colorId']) ? $input['colorId'] : '');
        $options['color'] = (isset($input['color']) ? $input['color'] : '');
        $options['size_id'] = (isset($input['sizeId']) ? $input['sizeId'] : '');
        $options['size'] = (isset($input['size']) ? $input['size'] : '');
        $options['weight'] = $product->weight;
        $options['length'] = $product->length;
        $options['width'] = $product->width;
        $options['height'] = $product->height;

        $ret['error_msg'] = false;

        if($options['stock']==0){
            $ret['error_msg'] = trans('cart.out-of-stock');
        } else if($options['stock']<$input['qty']){
            $ret['error_msg'] = trans('cart.qty-higher');
        } else {
            
            $proceed = true;
            if($this->user){
                
                if($product->user_id == $this->user->id){
                    $ret['error_msg'] = $product->name . ' ' . trans('cart.cannot-be-added');
                    $proceed = false;
                }
            }
            
            if($proceed)
            {   
                if($this->isOwnerVerified($product)==true && $this->isOwnerBalanceNotFull($product)==true){
                    Cart::add($product->id, $product->name, (int)$input['qty'], (float)$product->price, $options);
                }
                else if($this->isOwnerVerified($product)==false)
                {
                    $ret['error_msg'] = trans('cart.owner');
                }
                else if($this->isOwnerBalanceNotFull($product)==false)
                {
                    $ret['error_msg'] = trans('cart.seller');
                }
            }
        }

        $ret['cart_item_count'] = Cart::count(false);
        return $ret;
    }

    private function isOwnerVerified($product){
        return $product->user->is_docs_verified;
    }

    private function isOwnerBalanceNotFull($product)
    {
        $user = $product->user;
        $seller_ewallet = $user->ewallet()->first()->amount;

        $max_balance = 1000000;

        $docs = $user->documents()->latest()->get()->unique('type')->values();

        $is_enterprise = 0;
        foreach ($docs as  $doc) {
            //individual
            switch ($doc->type) {
                case 'bir':
                case 'permit':
                case 'sec':
                    if($doc->status == 'Verified')
                    {
                        $is_enterprise++;
                    }
                    break;
            }
        }

        if($is_enterprise == 3){
            $max_balance = 9999999.99;
        }

        //subtract the product's price
        $max_balance -= $product->sale_price?$product->sale_price:$product->price;

        return $seller_ewallet < $max_balance;
    }

    public function view()
    {
        $this->res['grand_shipping'] = 0;
        $this->res['cart'] = Cart::content();
        $this->res['grand_subtotal'] = Cart::total();
        $this->res['cart_item_count'] = Cart::count(false);
        $this->res['grand_total'] = $this->res['grand_shipping'] + (float)$this->res['grand_subtotal'];

        return $this->res;
    }

    public function update()
    {
        $input = Input::all();

        $product = Product::find($input['prod_id']);

        $options['slug'] = $product->slug;
        $options['stock'] = (isset($input['colorStock']) ? $input['colorStock'] : $product->stock);
        $options['image'] = (isset($input['colorImage']) ? $input['colorImage'] : $product->main_image);
        $options['discount'] = $product->discount;
        $options['fee_included'] = $product->fee_included;
        if($product->fee_included==1){
            $options['sale_price'] = (is_null($product->sale_price) ? number_format(round($product->price + $product->shipping_fee + $product->charge + $product->vat, 2), 2) : number_format(round($product->sale_price + $product->shipping_fee + $product->charge + $product->vat, 2), 2));
            $options['sale_price2'] = $product->sale_price;
            $options['shipping_fee'] = 0;
            $options['charge'] = 0;
            $options['vat'] = 0;
            $options['shipping_fee2'] = $product->shipping_fee;
            $options['charge2'] = $product->charge;
            $options['vat2'] = $product->vat;  
        } else {
            $options['sale_price'] = (is_null($product->sale_price) ? $product->price : $product->sale_price);
            $options['sale_price2'] = $product->sale_price;
            $options['shipping_fee'] = $product->shipping_fee;
            $options['charge'] = $product->charge;
            $options['vat'] = $product->vat;
            $options['shipping_fee2'] = $product->shipping_fee;
            $options['charge2'] = $product->charge;
            $options['vat2'] = $product->vat;              
        }
        $options['seller_id'] = $product->user_id;
        $options['store'] = $product->store->slug;
        $options['color_id'] = (isset($input['colorId']) ? $input['colorId'] : '');
        $options['color'] = (isset($input['color']) ? $input['color'] : '');
        $options['size_id'] = (isset($input['sizeId']) ? $input['sizeId'] : '');
        $options['size'] = (isset($input['size']) ? $input['size'] : '');
        $options['weight'] = $product->weight;
        $options['length'] = $product->length;
        $options['width'] = $product->width;
        $options['height'] = $product->height;

        $product = ['qty' => $input['qty'],'options' => $options];

        Cart::update($input['row_id'], $product);

        $item = Cart::get($input['row_id']);

        $shipping = 0;
        $subtotal = Cart::instance('shopping')->total();

        return json_encode([
            'shipping' => '₱ '.number_format($shipping, 2),
            'item_subtotal' => '₱ '.number_format($item->subtotal, 2)
        ]);
    }

    public function updateQty()
    {
        $input = Input::all();

        Cart::update($input['row_id'], $input['qty']);

        $item = Cart::get($input['row_id']);

        $shipping = 0;
        $subtotal = Cart::instance('shopping')->total();

        return json_encode([
            'shipping' => '₱ '.number_format($shipping, 2),
            'item_subtotal' => '₱ '.number_format($item->subtotal, 2)
        ]);
    }

    public function delete()
    {
        $input = Input::all();

        $rowId = $input['row_id'];

        $arrlength=count($rowId);

        for($x=0;$x<$arrlength;$x++){
          if(is_array($rowId))
            Cart::remove($rowId[$x]);
          else
            Cart::remove($rowId);
        }

        $shipping = 0;
        $subtotal = Cart::total();

        return json_encode([
            'shipping' => '₱ '.number_format($shipping, 2),
            'cart_item_count' => Cart::count(false)
        ]);
    }

    /**
     * Order a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkout(CartValidation $request)
    {
        $ret = [];
        $user = Auth::user();
        $order = new Order($request->all());
        $order->user_id = $user->id;
        $order->address_id = 1;
        $order->total = $request->input('discountedPrice');
        $order->subtotal = $request->input('subtotal');
        $order->tax = $request->input('tax');
        $order->shipping_fee = $request->input('shipping');
        $order->charge = $request->input('charge');
        $order->tracking_number = 'OR-' . mt_rand(0, 99999); // Don't remove
        $order->status = 'Incomplete';
        $order->name_of_receiver = $request->input('name');
        $order->contact_number = $request->input('number');
        $order->alternate_contact_number = $request->input('alternate_number');

        $ewallet_total = Ewallet::where('user_id','=',$user->id)->value('amount');
        $ewallet_bal = $user->ewallet->usable_ewallet; // usable ewallet


        //Check Stock
        $prod = [];
        $message = '';
        foreach (Cart::content() as $item) {
            $product = Product::find($item->id);
            if($item->options->color == '' && $item->options->size == ''){
                if($item->qty>$product->stock){
                    $mess = trans('cart.stock-for') . ' ' . $product->name . ' --> ' . $product->stock;
                    array_push($prod,$mess);
                    $message =  $message . '<br><br>'. $mess . ' ' . trans('cart.item');         
                }               
            } elseif($item->options->color != '' && $item->options->size == '') {
                foreach($product->product_color_variations as $color) {
                    if($color->label == $item->options->color) {
                        if($item->qty>$color->stock){
                            $mess = trans('cart.stock-for') . ' ' . $product->name . ' (' . $color->label . ') --> ' . $color->stock;
                            array_push($prod,$mess);
                            $message =  $message . '<br><br>'. $mess . ' ' . trans('cart.item');         
                        }
                    }
                }
            } else {
                foreach($product->product_size_variations as $size) {
                    if($size->label == $item->options->size) {
                        if($item->qty>$size->stock){
                            $mess = trans('cart.stock-for') . ' ' . $product->name . ' (' . $item->options->color . ') (' . $size->label . ') --> ' . $size->stock;
                            array_push($prod,$mess);
                            $message =  $message . '<br><br>'. $mess . ' ' . trans('cart.item');
                        }
                    }
                }
            }

        }

        if ($prod) { // Check Stock
            $ret['response'] = '0';
            $ret['message'] = trans('cart.exceeded') . '<br>' . $message;
        } elseif ($ewallet_bal < $request->input('total')) { // Check E-wallet
            $ret['response'] = '0';
            $ret['message'] = trans('cart.enough') . '<br>' . '<a href="/user/ewallet">' . trans('cart.add-balance')  . '</a>';
        } else {
            $order->balance = Ewallet::where('user_id', $user->id)->first()->amount - $order->total;
            $order->save();

            $ret['address'] = $this->saveAddress($request);
            $ret['orderaddress'] = $this->saveOrderAddress($order->id,$request);

            $_sellerIdArray = [];
            foreach (Cart::content() as $item) {
                $order_detail = new OrderDetail();
                $order_detail->order_id = $order->id;
                $order_detail->product_id = $item->id;
                // $order_detail->tracking_number = 'OR-' . mt_rand();
                $order_detail->price_per_item = $item->price;
                $order_detail->sale_price = (is_null($item->options->sale_price2) ? '0.00': str_replace("," , "" , $item->options->sale_price2));
                $order_detail->discount = $item->options->discount;
                $order_detail->shipping_fee = $item->options->shipping_fee2;
                $order_detail->charge = $item->options->charge2;
                $order_detail->vat = $item->options->vat2;
                $order_detail->quantity = $item->qty;
                $order_detail->fee_included = $item->options->fee_included;
                $order_detail->color_id = (is_null($item->options->color_id) ? 0 : $item->options->color_id);
                $order_detail->color = $item->options->color;
                $order_detail->image = $item->options->image;
                $order_detail->size_id = (is_null($item->options->size_id) ? 0 : $item->options->size_id);
                $order_detail->size = $item->options->size;
                if($item->options->fee_included==1)
                    $order_detail->subtotal = $item->qty*str_replace("," , "" , $item->options->sale_price);
                else
                    $order_detail->subtotal = $item->qty*(str_replace("," , "" , $item->options->sale_price)+$order_detail->shipping_fee+$order_detail->charge+$order_detail->vat);
                $order_detail->status = 'Pending';
                $order_detail->weight = $item->options->weight;
                $order_detail->length = $item->options->length;
                $order_detail->width = $item->options->width;
                $order_detail->height = $item->options->height;
                $order_detail->courier = '2GO';
                $order_detail->remarks = $request->input('notes');
                $order_detail->save();

                $prod = Product::find($item->id);

                $stock = $prod->stock - $item->qty;
                $sold_count = $prod->sold_count + $item->qty;

                $values=array('stock'=>$stock,'sold_count'=>$sold_count);
                Product::where('id','=', $item->id)->update($values);


                if($item->options->color != ''){
                    foreach($prod->product_color_variations as $color) {
                        if($color->id == $item->options->color_id) {
                            $color_stock = $color->stock - $item->qty;
                            $color_sold_count = $color->sold_count + $item->qty;
                            $color_values=array('stock'=>$color_stock,'sold_count'=>$color_sold_count);
                            ProductVariationColors::where('id','=', $item->options->color_id)->update($color_values);
                        }
                    }
                }

                if($item->options->size != ''){
                    foreach($prod->product_size_variations as $size) {
                        if($size->id == $item->options->size_id) {
                            $size_stock = $size->stock - $item->qty;
                            $size_sold_count = $size->sold_count + $item->qty;
                            $size_values=array('stock'=>$size_stock,'sold_count'=>$size_sold_count);
                            ProductVariationSizes::where('id','=', $item->options->size_id)->update($size_values);
                        }
                    }
                }

                // $prod->user->notify(new NewPurchase($prod, $user, $order_detail));

                if(!in_array($prod->user->id, $_sellerIdArray)) {
                    $_sellerIdArray[] = $prod->user->id;
                }
            }

            //Send Mail to Buyer

            $_prods = [];
            $_orderDetails = [];
            $_total = 0;
            $_discount = 0;

            $discountPerSeller = $request->get('discountPerSeller');
            foreach ($discountPerSeller as $item) {
                    $_discount += $item['amount']; 
            }

            foreach (Cart::content() as $item) {
                $_product = Product::find($item->id);
                    $_prods[] = $_product;
                    if($item->options->color == ''){
                        $_orderDetail = OrderDetail::where('order_id', $order->id)
                            ->where('product_id', $item->id)
                            ->first();
                    } else {
                         $_orderDetail = OrderDetail::where('order_id', $order->id)
                            ->where('color_id', $item->options->color_id)
                            ->first();                           
                    }
                    $_total += $_orderDetail->subtotal;
                    $_orderDetails[] = $_orderDetail;
            }

            $_total = $request->input('total');

            $user->notify(new NewOrderConfirmation($user, $_prods , $order, $_orderDetails, $_total, $_discount));

            //Send Mail to Seller
            $_prods = [];
            $_orderDetails = [];

            for($i=0; $i<count($_sellerIdArray); $i++) {

                $_total = 0;
                $_discount = 0;
                $discountPerSeller = $request->get('discountPerSeller');
                foreach ($discountPerSeller as $item) {
                    if($item['seller_id'] == $_sellerIdArray[$i]){
                        $_discount += $item['amount']; 
                    }
                }

                foreach (Cart::content() as $item) {
                    $_product = Product::find($item->id);
                    if($_product->user_id == $_sellerIdArray[$i]) {
                        $_prods[] = $_product;
                        if($item->options->color == ''){
                            $_orderDetail = OrderDetail::where('order_id', $order->id)
                                ->where('product_id', $item->id)
                                ->first();
                        } else {
                             $_orderDetail = OrderDetail::where('order_id', $order->id)
                                ->where('color_id', $item->options->color_id)
                                ->first();                           
                        }
                        $_total += $_orderDetail->subtotal;
                        $_orderDetails[] = $_orderDetail;
                    }
                }

                $_total = $request->input('total');

                \App\Models\User::find($_sellerIdArray[$i])->notify(new NewPurchase($_prods, $user, $_orderDetails, $_total, $_discount));

                // Reset
                $_prods = [];
                $_orderDetails = [];
            }

            $address_id = $order->addresses()->where('is_active','=',1)->value('id');
            Order::where('id',$order->id)->update(['address_id' => $address_id]);

            if($order)
            {
                //Debited the total amount to user
                $updated_ewallet_bal = $ewallet_total - $request->input('discountedPrice');
                Ewallet::where('user_id','=', $user->id)->update(['amount' => $updated_ewallet_bal]);
                //Cart::destroy();
            }

            $ret['id'] = $order->id;
            $ret['name'] = $order->name_of_receiver;
            $ret['ewallet_bal'] = $updated_ewallet_bal;
            $ret['subtotal'] = $request->input('total');
            $ret['discount'] = $request->input('discount');
            $ret['total'] = $request->input('discountedPrice');

        }

        return $ret;
    }

    public function getType()
    {
        $user = Auth::user();
        $user = $user->addresses()->get();
        $type = $user->map(
            function($data) {
                 return [
                     "id" => $data->id,
                     "type" => $data->type,
                     "is_active" => $data->is_active
                 ];
             }
         );
        $type = $type->toArray();
        return $type;
    }

    public function getAddressDetails($id)
    {
        $user = Auth::user();
        $address = $user->addresses()->where('id','=',$id)->get();
        return $address;
    }

    public function getActive()
    {
        $user = Auth::user();
        $user = $user->addresses()->where('is_active','=',1)->get();
        $type = $user->map(
            function($data) {
                 return [
                     "id" => $data->id,
                     "type" => $data->type
                 ];
             }
         );
        $type = $type->toArray();
        return $type;
    }

    public function saveProfile(CartValidation $request)
    {
        $this->saveAddress($request);
    }

    public function deleteProfile(CartValidation $request)
    {
        $data = Address::where('id', '=', $request->input('type_id'))->delete();
        return $data;
    }


    ///////////////////////
    // Private functions //
    ///////////////////////

    private function saveAddress($request)
    {

        $user = Auth::user();
        $user->addresses()->update(['is_active' => 0]);

        $address['type'] = $request->input('type');
        $address['address'] = $request->input('address');
        $address['barangay'] = $request->input('barangay');
        $address['city'] = $request->input('city');
        $address['city_id'] = $request->input('city_id');
        $address['province'] = $request->input('province');
        $address['province_id'] = $request->input('province_id');
        $address['zip_code'] = $request->input('zip_code');
        $address['landmark'] = $request->input('landmark');
        $address['remarks'] = $request->input('notes');
        $address['name_of_receiver'] = $request->input('name');
        $address['contact_number'] = $request->input('number');
        $address['alternate_contact_number'] = $request->input('alternate_number');
        $address['is_active'] = 1;

        $user->addresses()
             ->updateOrCreate(['id' => $request->input('type_id')], $address);

        $address = $address['address'] . ' ' . $address['barangay'] . ' ' . $address['city'] . ' ' . $address['province'] . ' ' . $address['zip_code'];

        return $address;
    }

    private function saveOrderAddress($order_id,$request)
    {

        $order = Order::find($order_id);
        $order->addresses()->update(['is_active' => 0]);

        $address['type'] = $request->input('type');
        $address['address'] = $request->input('address');
        $address['barangay'] = $request->input('barangay');
        $address['city'] = $request->input('city');
        $address['city_id'] = $request->input('city_id');
        $address['province'] = $request->input('province');
        $address['province_id'] = $request->input('province_id');
        $address['zip_code'] = $request->input('zip_code');
        $address['landmark'] = $request->input('landmark');
        $address['remarks'] = $request->input('notes');
        $address['name_of_receiver'] = $request->input('name');
        $address['contact_number'] = $request->input('number');
        $address['alternate_contact_number'] = $request->input('alternate_number');
        $address['is_active'] = 1;

        $order->addresses()
             ->updateOrCreate(['id' => $order_id], $address);

        $address = $address['address'] . ' ' . $address['barangay'] . ' ' . $address['city'] . ' ' . $address['province'] . ' ' . $address['zip_code'];

        return $address;
    }

}
