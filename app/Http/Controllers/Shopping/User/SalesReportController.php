<?php

namespace App\Http\Controllers\Shopping\User;

use DB;
use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Rates;
use App\Models\Awb;
use App\Models\City;
use Illuminate\Http\Request;
use App\Model\Store as store;
use App\Http\Controllers\Controller;
use App\Notifications\OrderStatusUpdate;
use App\Models\Shopping\OrderDetail as details;
use App\Models\Shopping\Store as ShoppingStore;
use App\Models\Shopping\Order;
use App\Models\Shopping\Product;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class SalesReportController extends Controller
{
    //
    public function index(){
        if(isset($_GET['storeid'])){
            if(!empty($_GET['storeid']))
                $storeid=$_GET['storeid'];
            else
                $storeid=1;
        }
        else 
            $storeid=1;
        $data = [
            'lang' => trans('sales-report'),
            'usable_ewallet' => Auth::user()->ewallet->usable_ewallet,
            'stores' => ShoppingStore::where('user_id', Auth::user()->id)->get(),
            'param' => $storeid 
        ];
        $rates = Rates::get();
        session()->flash('rates', $rates);
        return view('user.sales-report', $data);
        // return view('user.sales-report')
        // ->with('lang',trans('sales-report'));
    }

    public function getStores(){
        $user = Auth::user();
        $stores = User::with([
             'stores.delivered_order_count.product'
            ,'stores.pending_order_count.product'
            ,'stores.shipped_order_count.product'
            ,'stores.cancelled_order_count.product'
            ,'stores.zero_product_count'
            ,'stores.order_details.product'
            ,'stores.transit_order_count.product'
            ])->find($user->id);
        return $stores;
    }

    //get id
    public function getDetailsByStore($order_id,$store_id){
        $allprods = Product::where('store_id',$store_id)->pluck('id');
        $getsame = details::whereIn('product_id',$allprods)->where('order_id',$order_id)->first();
        // \Log::info($getsame->id);
        // return $this->getDetails($getsame->id);
        return $getsame->id;
    }

    //get whole details
    public function getWholeDetailsByStore($order_id,$store_id){
        $allprods = Product::where('store_id',$store_id)->pluck('id');
        $getsame = details::whereIn('product_id',$allprods)->where('order_id',$order_id)->first();
        // \Log::info($getsame->id);
        // return $this->getDetails($getsame->id);
        return $getsame;
    }

    public function getDetails($id){
        //\Log::info($id);

        $det = details::where('id',$id)->first();
        $stid = Product::where('id',$det->product_id)->select('store_id')->first();
        $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
        $order = details::whereIn('product_id',$allprods)->where('order_id',$det->order_id)->with(['order.user', 'order.details.product', 'order.details.fileImage', 'order.details.fileVideo', 'product.store', 'order.address', 'fileImage', 'fileVideo'])->first();
        $order['img_main_image']        = $order['product']['main_image'];
        $order['product_code']          = substr($order['product']['name'],0,3);
        $order['product_name']          = $order['product']['name'];
        $order['product_slug']          = $order['product']['slug'];
        $order['product_boxContent']    = $order['product']['boxContent'];
        $order['product_brand']         = $order['product']['brand'];
        $order['product_weight']        = $order['product']['weight'];
        $order['order_fullname']        = $order['order']['user']['fullname'];
        $order['order_realname']        = $order['order']['user']['first_name']." ".$order['order']['user']['last_name'];
        $order['order_receiver']        = $order['order']['name_of_receiver'];
        $order['order_alternate']       = $order['order']['alternate_contact_number'];
        $order['order_contact']         = $order['order']['contact_number'];
        $order['product_shipment_type'] = $order['product']['shipment_type'];
        $order['order_landmark']        = $order['order']['address']['landmark'];

        $cb = explode("*", $order['order']['address']['city']);
        $order['order_address']         = $order['order']['address']['address'].", ".
                                          ucwords(strtolower(trim($cb[1]))).", ".
                                          ucwords(strtolower(trim($cb[0]))).", ".
                                          $order['order']['address']['province']." ".
                                          $order['order']['address']['country']." ".
                                          $order['order']['address']['zip_code'];
        $order['province']              = $order['order']['address']['province'];
        $order['order_tracking_number'] = $order['order']['tracking_number'];
        $order['package_type']          = $order['product']['package_type'];
        $order['weight']                = $order['product']['weight'];
        $order['dimension']             = $order['product']['length']." x ".
                                          $order['product']['width']." x ".
                                          $order['product']['height'];
        $order['length']                = $order['product']['length'];
        $order['height']                = $order['product']['height'];
        $order['width']                 = $order['product']['width'];
        $order['prod_desc']             = $order['product']['description'];
        $order['store_name']            = $order['product']['store']['name'];
        $order['store_address']         = $order['product']['store']['full_address'];    

        $prod_id = $order->product_id;
        $order_id = $order->order_id;
        $stid = Product::where('id',$prod_id)->select('store_id')->first();
        $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
        $awb = Awb::where('order_id',$order_id)->whereIn('product_id',$allprods)->first(); 
        if($awb) {
            $order['awb_id'] = $awb->code; 
        } else {
            $order['awb_id'] = '';
        }
        
        return $order;
    }

    public function getAirway($id){
        $order = details::with(['order.user', 'product.store', 'order.address', 'fileImage', 'fileVideo'])->find($id);
        $order['img_main_image']        = $order['product']['main_image'];
        $order['product_code']          = substr($order['product']['name'],0,3);
        $order['product_name']          = $order['product']['name'];
        $order['product_slug']          = $order['product']['slug'];
        $order['product_boxContent']    = $order['product']['boxContent'];
        $order['product_brand']         = $order['product']['brand'];
        $order['product_weight']        = $order['product']['weight'];
        $order['order_fullname']        = $order['order']['user']['fullname'];
        $order['order_realname']        = $order['order']['user']['first_name']." ".$order['order']['user']['last_name'];
        $order['order_receiver']        = $order['order']['name_of_receiver'];
        $order['order_alternate']       = $order['order']['alternate_contact_number'];
        $order['order_contact']         = $order['order']['contact_number'];
        $order['product_shipment_type'] = $order['product']['shipment_type'];
        $order['order_landmark']        = $order['order']['address']['landmark'];
        $order['order_address']         = $order['order']['address']['address']." ".
                                          $order['order']['address']['barangay']." ".
                                          $order['order']['address']['city']." ".
                                          $order['order']['address']['province']." ".
                                          $order['order']['address']['country']." ".
                                          $order['order']['address']['zip_code'];
        $order['province']              = $order['order']['address']['province'];
        $order['order_tracking_number'] = $order['order']['tracking_number'];
        $order['package_type']          = $order['product']['package_type'];
        $order['weight']                = $order['product']['weight'];
        $order['dimension']             = $order['product']['length']." x ".
                                          $order['product']['width']." x ".
                                          $order['product']['height'];
        $order['length']                = $order['product']['length'];
        $order['height']                = $order['product']['height'];
        $order['width']                 = $order['product']['width'];
        $order['prod_desc']             = $order['product']['description'];  

        $prod_id = $order->product_id;
        $order_id = $order->order_id;
        $stid = Product::where('id',$prod_id)->select('store_id')->first();
        $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
        $awb = Awb::where('order_id',$order_id)->whereIn('product_id',$allprods)->first();
        if($awb) {
            if($awb->id < 1000){
                $awb_id = "W1000".$awb->id."B"; //AWB CODE
            }
            else{
                $awb_id = "W100".$awb->id."B"; //AWB CODE
            }
            $order['awb_id'] = $awb_id;
        } else {
            $order['awb_id'] = '';
        }

        return $order;
    }

    // Pending
    public function updateStatus($id, $val){
        $orderDetail = details::findOrFail($id);
        $orderDetail->status = $val;
        $orderDetail->tracking_number = null;
        $orderDetail->shipment_type = null;
        $orderDetail->courier = null;
        $orderDetail->shipped_date = null;
        $orderDetail->reason = null;
        $orderDetail->save();

        $detail_amount = $orderDetail->subtotal;
        $ewallet_seller = $orderDetail->product()->first()->user()->first()->ewallet()->first();

        if ($val == 'Cancelled') {
            $this->orderedCancelled($id);
        }

        // if($val == 'Received' || $val == 'Delivered')
        // {
        //     $ewallet_seller->amount += $detail_amount;
        // }

        // $orderDetail->balance = $ewallet_seller->amount;

        // $ewallet_seller->save();

        $orderDetail->order->user->notify(new OrderStatusUpdate($orderDetail, Auth::user()));

        return $orderDetail;
    }

    public function updateOrderRemarks(Request $request){
        $data = $request->all();
        $update = details::whereId($request->id)->update(['seller_notes' => $request->seller_notes]);
        return $update;
    }

    public function orderedCancelled($order_detail_id){
        $order_detail = details::findOrFail($order_detail_id);

        $order = $order_detail->order;
        $ewallet_buyer = $order->user->ewallet;
        // $ewallet_seller = $order_detail->product->user->ewallet;
        
        //return stocks because cancelled
        $product = $order_detail->product()->first();
        //get qty
        $qty_ordered = $order_detail->quantity;
        $product->stock += $qty_ordered;
        $product->save();

 
        $returnAmount = $order_detail->subtotal;
        //return to buyer
        $ewallet_buyer->amount += $returnAmount;
        //remove from seller
        // $ewallet_seller->amount -= $returnAmount;
        // echo $ewallet_seller->save() .'<Br>';
        // echo $ewallet_buyer->save();

        //save the balance for ewallet transactions
        $order_detail->balance = $ewallet_buyer->amount;
        $order_detail->save();

        $ewallet_buyer->save();
    }

    public function updateToTransit(Request $request)
    {
        // $prod_id = $request->get('product_id');
        // $order_id = $request->get('order_id');
        // $fee = $request->get('fee');
        // $stid = Product::where('id',$prod_id)->select('store_id')->first();
        // $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
        // $getsame = details::where('order_id',$order_id)->where('status','Pending')->whereIn('product_id',$allprods)->get();
        // $countitems =  $getsame->count();
        // $_prods = [];
        // $_orderDetails = [];
        // $ctr = 0;
        // $prc = 0;
        // $oprc = 0;
        // foreach($getsame as $gs){
        //     $orderDetail = details::findOrFail($gs->id);
        //     $orderDetail->status = 'In Transit';
        //     $orderDetail->shipment_type = $request->get('shipment_type');
        //     $orderDetail->courier = $request->get('courier');
        //     $orderDetail->shipped_date = Carbon::now();
        //     $orderDetail->reason = null;
        //     $orderDetail->save();

        //     $prc = floatval($prc) + (floatval($orderDetail->sale_price) > 0 ? floatval($orderDetail->sale_price) : floatval($orderDetail->price_per_item));

        //     $oprc += floatval($orderDetail->subtotal);

        //     $_orderDetails[] = details::where('id',$gs->id)->with('product')->first();
        //     $ctr++;
        //     if($countitems == $ctr){
        //         //$fee = floatval($fee) + floatval($prc);
        //         if($countitems == 1){
        //             $fee = floatval($oprc);
        //         }
        //         else{
        //             $fee = floatval($oprc) - floatval($fee);
        //         }
        //         $orderDetail->order->user->notify(new OrderStatusUpdate($orderDetail, Auth::user(), $_orderDetails , $fee));
        //     }
        // }

        // return $orderDetail;


        $prod_id = $request->get('product_id');
        $order_id = $request->get('order_id');
        $fee = $request->get('fee');
        $fee2 = $request->get('fee');
        $usrid = Order::where('id',$order_id)->select('user_id','contact_number')->first();
        $stid = Product::where('id',$prod_id)->select('store_id')->first();
        $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
        $getsame = details::where('order_id',$order_id)->where('status','Pending')->whereIn('product_id',$allprods)->with('product')->get();
        $prds = details::where('order_id',$order_id)->where('status','Pending')->whereIn('product_id',$allprods)->pluck('product_id');
        $comma_separated = implode(",", json_decode($prds));
        $prod_ctr = $getsame->sum(function ($getsame) {
            return $getsame->quantity;
        });
        //$prod_ctr = sum($getsame);
        $security_deposit = $request->get('security_deposit');
        //save awb
        $awb = new Awb();
        $awb->order_id = $order_id;
        $awb->product_id = $prod_id;
        $awb->included = $comma_separated;
        $awb->total_price = $request->get('totaldiscountedprice');
        $awb->wataf_cost = $request->get('watafcost');
        $awb->twogo_cost = $request->get('twogocost');
        $awb->save();

        if($awb->id < 1000){
            $awb_id = "W1000".$awb->id."B"; //AWB CODE
        }
        else{
            $awb_id = "W100".$awb->id."B"; //AWB CODE
        }
        // $awbCode = Awb::find($awb->id);
        // $awbCode->code = $awb_id;
        // $awbCode->save();

        $countitems =  $getsame->count();
        $_prods = [];
        $_orderDetails = [];
        $ctr = 0;
        $prc = 0;
        $oprc = 0;
        $dpr = 0;

        $total_qty = 0;
        $total_weight = 0;
        $total_length = 0;
        $total_width = 0;
        $total_height = 0;
        $total_price = 0;
        $prod_ids = 'Prd -> ';
        $slugs = '';
        $allnames = '';
        foreach($getsame as $gs){
            $orderDetail = details::findOrFail($gs->id);
            $orderDetail->status = 'Pending Request';
            $orderDetail->shipment_type = $request->get('shipment_type');
            $orderDetail->courier = $request->get('courier');
            //$orderDetail->shipped_date = Carbon::now();
            $orderDetail->reason = null;
            $disc_fee = (floatval($security_deposit)/$prod_ctr)*$orderDetail->quantity;
            $orderDetail->disc_fee = $disc_fee;
            $orderDetail->save();

            $prc = floatval($prc) + (floatval($orderDetail->sale_price) > 0 ? floatval($orderDetail->sale_price) : floatval($orderDetail->price_per_item));
            $pr = (floatval($orderDetail->sale_price) > 0 ? floatval($orderDetail->sale_price) : floatval($orderDetail->price_per_item));
            $dpr += ((floatval($pr) + floatval($orderDetail->disc_fee)));
            $oprc += floatval($orderDetail->subtotal);

            $_orderDetails[] = details::where('id',$gs->id)->with('product')->first();

            $price_per_item = (floatval($orderDetail->sale_price) > 0 ? floatval($orderDetail->sale_price) : floatval($orderDetail->price_per_item));
            $prd = Product::where('id',$gs->product_id)->select('id','length','width','height','weight','slug','name')->first();
            $total_qty += floatval($gs->quantity);
            $total_weight += floatval($prd->weight);
            $total_length += floatval($prd->length);
            $total_width += floatval($prd->width);
            $total_height += floatval($prd->height);
            $total_price += floatval($price_per_item);
            $prod_ids .= ($gs->quantity.'x ->'.$prd->id.' : ');
            $slugs .= ($prd->slug.',');
            $allnames .= ($prd->name.', ');
            $ctr++;

            if($countitems == $ctr){
                $px_dtls[] = [
                    'item_qty' => $total_qty,
                    'pkg_code' => 'BOX',
                    'actual_wt' => $total_weight,
                    'length' => $total_length,
                    'width' => $total_width,
                    'height' => $total_height,
                    'declared_value' => $total_price,
                    'item_description' => $allnames,
                ];
                if($countitems == 1){
                    $fee = floatval($oprc);
                }
                else{
                    $fee = floatval($oprc) - floatval($fee);
                }

                $fee_2go = floatval($oprc) - (floatval($fee2) + floatval($prc)); 
                $orders = Details::with(['order.address'])->where('product_id' , $prod_id)->where('order_id' , $order_id)->first();
                $city = City::findOrFail($orders['order']['address']['city_id']);
                $cb = explode("*", $city->name);
                $usr = User::where('id',$usrid->user_id)->first();
                $dtls['name'] = $usr->first_name." ".$usr->last_name;
                $dtls['street'] = $orders['order']['address']['address'];
                $dtls['barangay'] = ucwords(strtolower(trim($cb[1])));
                $dtls['city'] = ucwords(strtolower(trim($cb[0])));
                $dtls['area_code'] = $city->area_code;
                $dtls['mobile_number'] = $usrid->contact_number;
                $dtls['email_address'] = $usr->email;

                //pickup details
                $store_owner = Auth::user();
                $pickup_dtls['name'] = $store_owner->first_name." ".$store_owner->last_name;
                $prod = Product::findOrFail($prod_id);
                $store = ShoppingStore::findOrFail($prod->store_id);
                $store_city = City::where('name',$store->address_detail->city)->first();
                $ct = explode("*", $store->address_detail->city);
                $pickup_dtls['street'] = $store->address_detail->address.', '.ucwords(strtolower(trim($ct[1]))).', '.ucwords(strtolower(trim($ct[0])));
                $pickup_dtls['area_code'] = $store_city->area_code;
                $pickup_dtls['contact_number'] = $store->contact_number;
                $pickup_date = Carbon::now()->addDays(2);
                $pickup_dtls['pickup_date'] = $pickup_date->format('m/d/Y');;

                $data2['d'] = [
                    'id_' => "WATAF",
                    'token_' => "9Hs2JL21z",
                    'account_no' => "9602032400",
                    'bk_request' => false,
                    'awb_code' => $awb_id,
                    'origin_port' => "MNL",
                    'paymode' => "CS",
                    'cod_amt' => 0,
                    'instruction' => '',
                    'attachment' => 'Order ID : '.$order_id,
                    'pickup_dtls' => $pickup_dtls,
                    'cnee_dtls' => $dtls,
                    'px_dtls' => $px_dtls,
                ];
                // \Log::info($data2);
                //send post request API
                try {
                    $client = new Client([
                        'Content-Type' => 'application/json'
                    ]);

                    $r = $client->request('POST', 
                        'https://expressapps.2go.com.ph/2gowebdev/model/API/shipment_request.asmx/create', 
                        [
                            'json' => $data2
                        ]
                    );

                    $status = json_decode(json_decode($r->getBody()->getContents(), true)['d'])->status;

                    if($status == 'T') { // Success
                        $awbCode = Awb::find($awb->id);
                        $awbCode->code = $awb_id;
                        $awbCode->save();
                    }
                
                } catch (\Exception $ex) {
                    // \Log::error($ex);
                    // return json_encode((string)$ex->getResponse()->getBody());
                }
                //$orderDetail->order->user->notify(new OrderStatusUpdate($orderDetail, Auth::user(), $_orderDetails , $dpr));
        return $orderDetail;
                
            }
        }


        // $dtls['name'] = "Juan Dela Cruz";
        // $dtls['street'] = "L10 B20 PUTATAN VILLAGE";
        // $dtls['barangay'] = "PUTATAN";
        // $dtls['city'] = "MUNTINLUPA CITY";
        // $dtls['area_code'] = "1958";
        // $dtls['mobile_number'] = "09088946526";
        // $dtls['email_address'] = "jeffrey.gatpandan656@gmail.com";

        // $px_dtls[] = [
        //     'item_qty' => 1,
        //     'pkg_code' => 'BOX',
        //     'actual_wt' => 1,
        //     'length' => 5,
        //     'width' => 5,
        //     'height' => 5,
        //     'declared_value' => 1000,
        //     'item_description' => "CP UNIT",
        // ];
        // $data2['d'] = [
        //     'id_' => "WATAF",
        //     'token_' => "9Hs2JL21z",
        //     'account_no' => "9602032400",
        //     'bk_request' => false,
        //     'awb_code' => 'W1000198B',
        //     'origin_port' => "MNL",
        //     'paymode' => "CS",
        //     'cod_amt' => 0,
        //     'instruction' => 'Near train station',
        //     'attachment' => 'MDS-132131232',
        //     'cnee_dtls' => $dtls,
        //     'px_dtls' => $px_dtls,
        // ];

        // try {
        //     $client = new Client([
        //         'Content-Type' => 'application/json'
        //     ]);

        //     $r = $client->request('POST', 
        //         'https://expressapps.2go.com.ph/2gowebdev/model/API/shipment_request.asmx/create', 
        //         [
        //             'json' => $data2
        //         ]
        //     );

        //     \Log::info($r->getBody());
        //     // echo $r->getBody();
        //     // echo $r->getStatusCode();
        // } catch (\Exception $ex) {
        //     return json_encode((string)$ex->getResponse()->getBody());
        // }
    }

    public function cancelOrder(Request $request)
    {
        $orderDetail = details::findOrFail($request->get('id'));
        $orderDetail->status = 'Cancelled';
        $orderDetail->reason = $request->get('reason');
        $orderDetail->tracking_number = null;
        $orderDetail->shipment_type = null;
        $orderDetail->courier = null;
        $orderDetail->shipped_date = null;
        $orderDetail->save();

        //change order status to complete if all orders are received or delivered
        $order_detail = Details::findOrFail($request->get('id'));
        $order_id = $order_detail->order_id;
        $this->updateParentOrderStatus($order_id);

        //get product id
        $product_id = $orderDetail->product_id;
        $prod = Product::findOrFail($product_id);
        $prod->is_searchable = false;
        $prod->save();

        $orderDetail->order->user->notify(new OrderStatusUpdate($orderDetail, Auth::user()));

        $this->orderedCancelled($orderDetail->id);

        return $orderDetail;
    }

    public function updateParentOrderStatus($id){
        $all_orders = Details::where('order_id',$id)->get();
        $incomplete = 0;
        $cancelled = 0;
        $complete = 0;
        foreach($all_orders as $all){
            if($all->status == "Pending" || $all->status == "In Transit"){
                $incomplete++;
            }
            else if($all->status == "Cancelled"){
                $cancelled++;
            }
            else{
                $complete++;
            }
        }
        $order = Order::findOrFail($id);

        if($incomplete>0){
            $order->status = "Incomplete";
        }
        else if($cancelled == $all_orders->count()){
            $order->status = "Cancelled";
        }
        else{
            $order->status = "Complete";
        }
        $order->save();
    }

    public function checkIfSameStore($prod_id,$order_id){
        $stid = Product::where('id',$prod_id)->select('store_id')->first();
        $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
        //$stores = ShoppingStore::with(['order_details'])->where('id',$stid->store_id)->get();
        $getsame = details::where('order_id',$order_id)->with('product','product.store')->get();
        return $getsame;
    }

    public function getProductsFromSameStore($prod_id,$order_id){
        $stid = Product::where('id',$prod_id)->select('store_id')->first();
        $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
        //$stores = ShoppingStore::with(['order_details'])->where('id',$stid->store_id)->get();
        $getsame = details::with('product','product.store')->whereIn('product_id',$allprods)->where('order_id',$order_id)->get();
        return $getsame;
    }

    public function getProductsFromSameStore2($order_id,$store_id){
        $allprods = Product::where('store_id',$store_id)->pluck('id');
        $getsame = details::with('product','product.store')->whereIn('product_id',$allprods)->where('order_id',$order_id)->get();
        return $getsame;
    }

    public function getProductIdsFromSameStore($prod_id,$order_id){
        $stid = Product::where('id',$prod_id)->select('store_id')->first();
        $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
        return $allprods;
    }

}