<?php

namespace App\Http\Controllers\Shopping\User;
use Illuminate\Support\Facades\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Image;
use App\Models\Shopping\EwalletTransactions;
use App\Models\User;
use App\Notifications\NewEwalletTransaction;

class EwalletController extends Controller
{
    public function index(){
        $user = Auth::user();
        $onhold = $user->fee_on_hold;
        $not_usable = $user->ewallet->transactions()
            ->where(function($query){
                $query->where('status','Pending');
                $query->orWhere('status','Processing');
                $query->orWhere('status','On Hold');
            })
            ->where('type','<>','Deposit')
            ->get()
            ->pluck('amount')
            ->sum();
        $breakdown = '<p><b>Ewallet Total : </b> P<span id="eTotal">'.$user->ewallet->amount.'</span></p>';
        $breakdown .= '<p><b>Pending Ewallet Transaction : </b> P<span id="notUsable">'.$not_usable.'</span></p>';
        $breakdown .= '<p><b>Shipment Security Deposit : </b> P<span id="onHold">'.$onhold.'</span></p>';
        $data = [
            'breakdown' => $breakdown,
            'lang' => trans('ewallet')
        ];
    	return view('user.ewallet')
            ->with($data);
    }

    public function upload(Request $request)
    {
        $user = Auth::user();
        $image = $request->file('image');
        $width = 300;
        $height = 400;
        $transaction_id = $request->input('id');
        $filename = null;
        if($transaction_id)
        {
            $etrans = EwalletTransactions::find($transaction_id);
            $filename = $etrans->ref_number.'.jpg';
            $etrans->deposit_slip = $filename;

            //make it pending
            $etrans->status = 'Pending';
            
            $etrans->save();

            $adminUsers = User::whereHas('roles', function ($query) {
                $query->where('name', 'master')
                    ->orWhere('name', 'cpanel-admin')
                    ->orWhere('name', 'support');
            })->get();

            Notification::send($adminUsers, new NewEwalletTransaction(EwalletTransactions::findOrFail($etrans->id)));
            
        }

        if (!file_exists(public_path().'/shopping/images/deposit-slip')) {
            mkdir(public_path().'/shopping/images/deposit-slip', 0777, true);
        }

        if($filename)
        {
            Image::make($image->getRealPath())
              ->encode('jpg', 100)
              ->save(public_path().'/shopping/images/deposit-slip/'.$filename);

            $user->images()->create([
              'name' => $filename,
              'type' => 'deposit-slip',
            ]);
        }

        return redirect('/user/ewallet');
    }
}
