<?php

namespace App\Http\Controllers\Shopping\User;

use DB;
use App\Models\User;
use App\Models\Address;
use Illuminate\Http\Request;
use App\Acme\Filters\UserFilters;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserValidation;
use App\Models\Shopping\Friends;
use App\Http\Controllers\Controller;
use App\Models\Shopping\Store;
use App\Models\Shopping\Product;

class UsersController extends Controller
{
    protected $model;
    protected $filters;
    protected $validation;

    public function __construct(User $model, UserFilters $filters)
    {
        $this->model = $model;
        $this->filters = $filters;
        $this->validation = new UserValidation;
    }

    public function index()
    {

        $query = $this->filters?$this->model->filter($this->filters):$this->model;
        $data = $query->get();

        foreach ($data as $value) {
            
            $returned = $this->getRelationshipStatus($value->id);
            
            $value->rel_status = $returned['rel_status'];
            $value->initiator = $returned['initiator'];
        }

        return $this->setStatusCode(200)
            ->respond($data);
    }


    public function show($id)
    {
        $data['viewuser'] = User::findOrFail($id);
        $data['stores'] = Store::where('user_id', $id)->latest()->get();
        $data['lang'] = trans('store-management');

        return view('user.viewprofile')
            ->with($data);
    } 
}
