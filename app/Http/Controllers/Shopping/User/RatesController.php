<?php

namespace App\Http\Controllers\Shopping\User;

use App\Models\User;
use App\Models\Rates;
use Illuminate\Http\Request;
use App\Acme\Filters\UserFilters;
use App\Http\Requests\UserValidation;
use Auth;
use App\Http\Controllers\Controller;

class RatesController extends Controller
{
	protected $featured;
    protected $latest;
	protected $user;

    public function __construct(Rates $model)
    {
        $this->model = $model;
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();   
            return $next($request);
        });
    }

    public function index(){
    	$data['rates'] = $this->model::all();
    	return $data;
    }

}
