<?php

namespace App\Http\Controllers\Shopping\User;

use DB;
use Auth;
use File;
use Hash;
use data;
use Image;
use App\Models\User;
use App\Http\Requests;
use App\Classes\Common;
use App\Models\Document;
use App\Models\Question;
use App\Models\City as city;
use Illuminate\Http\Request;
use App\Models\User as users;
use Illuminate\Validation\Rule;
use App\Models\Province as prov;
use App\Notifications\NewDocument;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddressValidation;
use App\Http\Requests\ProfileValidation;
use App\Notifications\EmailVerification;
use App\Http\Requests\PasswordValidation;
use Illuminate\Support\Facades\Notification;
use App\Models\QuestionUser as question_user;
use App\Http\Requests\SecurityQuestionsValidation;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $profile_image_path = public_path() . '/images/avatar/'. $user->id . '.jpg';
        $data['hasProfileImage'] = File::exists($profile_image_path)?'/images/avatar/'. $user->id . '.jpg':false;

        $data['user'] = $user;


        $nbi_status =  $user->documents()->where('type', 'nbi')->latest()->first();
        $birthCert_status =  $user->documents()->where('type', 'birthCert')->latest()->first();
        $govID_status =  $user->documents()->where('type', 'govID')->latest()->first();
        $sec_status =  $user->documents()->where('type', 'sec')->latest()->first();
        $bir_status =  $user->documents()->where('type', 'bir')->latest()->first();
        $permit_status =  $user->documents()->where('type', 'permit')->latest()->first();

        $data['nbi_reason'] = $nbi_status? $user->documents()->where('type', 'nbi')->latest()->first()->reason:false;
        $data['birthCert_reason'] = $birthCert_status? $user->documents()->where('type', 'birthCert')->latest()->first()->reason:false;
        $data['govID_reason'] = $govID_status? $user->documents()->where('type', 'govID')->latest()->first()->reason:false;
        $data['sec_reason'] = $sec_status? $user->documents()->where('type', 'sec')->latest()->first()->reason:false;
        $data['bir_reason'] = $bir_status? $user->documents()->where('type', 'bir')->latest()->first()->reason:false;
        $data['permit_reason'] = $permit_status? $user->documents()->where('type', 'permit')->latest()->first()->reason:false;
        $data['lang'] = trans('profile-settings');

        return view('user.profile.index')
          ->with($data);
    }

    public function settings()
    {
      return view('user.profile.settings');
    }

    public function accountUpdate(ProfileValidation $request)
    {
        $user = Auth::user();
        $data = $request->all();

        $user->fill($request->all())
          ->save();
        return $user;
    }

    public function changePassword(PasswordValidation $request)
    {
      $user = Auth::user();
      $data = $request->all();
      $updatePassword = users::find($user->id);
      $updatePassword->password = $data['new_password'];
      $updatePassword->save();
    }

    public function getSecurityQuestions()
    {
      $questions = Question::whereType('security')->get();
      return $questions;
    }

    public function answerSecurityQuestions(SecurityQuestionsValidation $request)
    {
      $user = Auth::user();
      $data = $request->all();
      $data['que']  = $request->question['id'];
      $data['que1'] = $request->question1['id'];
      $data['que2'] = $request->question2['id'];

      $checkRecord = question_user::where('user_id', $user->id)->get();
      if(count($checkRecord) == 0)
      {
        for($i=0;$i<3;$i++){
          $ctr = ($i == 0 ? '' : $i);
          $create = question_user::create([
               'question_id'=> $data['que'.$ctr]
              ,'user_id'    => $user->id
              ,'answer'     => $data['answer'.$ctr]
          ]);
        }
        return $create;
      }
    }

    public function getSecurityAnswer()
    {
      $user = Auth::user()->load(['questions']);
      return $user;
    }

    public function addressCreate(Request $request)
    {
      $user = Auth::user();
      $data = $request->all();
      dd($data);
    }

    public function getProvinces()
    {
      $prov = prov::all();
      return $prov;
    }

    public function getCities($name)
    {
      $determineCity = prov::whereName($name)->first();
      $city = city::whereProvince_id($determineCity->id)->get();
      return $city;
    }

    public function homeAddressCreate(AddressValidation $request)
    {
      $user = Auth::user();
      $data = $request->all();
      $data['city'] = $data['city'];
      $data['province'] = $data['province'];
      $data['label'] = "Home";
      $data['is_active'] = 1;
      $user->addresses()->updateOrCreate(['type' => "Home"], $data);
      if($data['landline'] != ''){
        $user->numbers()->updateOrCreate(['type' => "Home", 'number_type' => 'Landline'],
          [
           'type'         => "Home"
          ,'area_code'       => $data['area_code']
          ,'number'       => $data['landline']
          ,'number_type'  => 'Landline'
          ,'remarks'      => $data['remarks']
          ,'is_active'    => 1
          ]);
      }
      if($data['mobile'] != ''){
        $user->numbers()->updateOrCreate(['type' => "Home", 'number_type' => 'Mobile'],
          [
           'type'         => "Home"
          ,'number'       => $data['mobile']
          ,'number_type'  => 'Mobile'
          ,'remarks'      => $data['remarks']
          ,'is_active'    => 1
        ]);
      }
      return $user;
    }

    public function getHomeAddress()
    {
      $user = Auth::user();
      $this->data['residence'] = $user->addresses()->whereType('Home')->first();
      $this->data['mobile'] = $user->numbers()->whereType('Home')->whereNumber_type('Mobile')->value('number');
      $this->data['landline'] = $user->numbers()->whereType('Home')->whereNumber_type('Landline')->value('number');
      $this->data['area_code'] = $user->numbers()->whereType('Home')->whereNumber_type('Landline')->value('area_code');
      return $this->data;

    }

    public function updateAvatar(Request $request)
    {
      $user = Auth::user();

      $width = $request->get('width');
      $height = $request->get('height');
      $image = $request->get('image');

      $img = Image::make($image)->resize($width, null, function ($constraint) {
          $constraint->aspectRatio();
      })
      ->resizeCanvas($width, $height, 'center')
      ->encode('data-url');

      $img2 = clone $img;

      Image::make($img)
          ->encode('jpg', 100)
          ->save(public_path().'/images/avatar/'.$user->id.'.jpg');

      $user->images()->create([
          'name' => $user->id.'.jpg',
          'type' => 'avatar',
      ]);

      return $img2;
    }

    public function uploadImage(Request $request)
    {
        $user = Auth::user();

        $width = $request->get('width');
        $height = $request->get('height');
        $image = $request->get('image');
        $type = $request->get('type');
        $img = Image::make($image)->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        })
        ->encode('data-url');

        $img2 = clone $img;

        if (!file_exists(public_path().'/images/documents/'.$user->id)) {
            mkdir(public_path().'/images/documents/'.$user->id, 0777, true);
        }
        $unique_value = str_random('5');

        Image::make($img)
          ->encode('jpg', 100)
          ->save(public_path().'/images/documents/'.$user->id.'/'.$type.$unique_value.'.jpg');

        $docsNbiId = $user->documents()->where('type','=','nbi')->get();
        $docsGovId = $user->documents()->where('type','=','govId')->get();

        
        if(count($docsNbiId) == 0 || count($docsGovId) == 0){

          $user->images()->create([
            'name' => $user->id.'.jpg',
            'type' => $type,
          ]);

          $document = $user->documents()->create([
            'type' => $type,
            'name' => $type.$unique_value.'.jpg'
          ]);

          $this->notifyAdmins($document);

        } else {

          $docs = $user->documents()->latest()->get()->unique('type')->values();

          foreach ($docs as  $doc) {

          if($doc->status=='Pending'){

            $document = $user->documents()->where('type','=',$type)->where('status','=','Pending')->update([
                'name' => $type.$unique_value.'.jpg'
            ]);
          }

          if($doc->status=='Expired' || $doc->status=='Denied'){

            $document = $user->documents()->create([
              'type' => $type,
              'name' => $type.$unique_value.'.jpg'
            ]);

            $this->notifyAdmins($document);
          }

          }      

        }


      return $img2;
    }

    public function docExist(Request $request){
        $user = Auth::user();
        $file = $request->get('file');
        return response()->json(file_exists(public_path().'/images/documents/'.$user->id.'/'.$file));
    }

    public function getNotif(){
        $user = Auth::user();
        return response()->json($user->notifications);
    }

    private function notifyAdmins($document)
    {
      $authUser = Auth::user();

      $adminUsers = User::whereHas('roles', function ($query) {
          $query->where('name', 'master')
              ->orWhere('name', 'cpanel-admin');
      })->get();

      Notification::send($adminUsers, new NewDocument($document, $authUser));
    }
}
