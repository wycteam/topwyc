<?php

namespace App\Http\Controllers\Shopping;

use App\Models\Number;
use App\Models\Address;
use Illuminate\Http\Request;
use App\Models\Shopping\Store;
use App\Models\Shopping\Product;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Shopping\StoreCompany;
use Intervention\Image\Facades\Image;
use App\Http\Requests\StoreValidation;

class StoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $data['stores'] = Store::where('user_id', $user->id)->latest()->get();
        // $products = Product::with([
        //     'ratings',
        //     'images' => function ($query) {
        //         $query->whereType('primary');
        //     }])
        //     ->where('user_id', $user->id)
        //     ->latest()
        //     ->get();
        $data['lang'] = trans('store-management');
        return view('store.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('store.create')->with('lang',trans('store-management'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreValidation $request)
    {
        $store = DB::transaction(function () use ($request) {
            $data = new Store($request->all());
            $data->user_id = Auth::user()->id;
            $data->save();

            $this->saveAddress($data, $request);
            $this->saveNumber($data, $request);
            $this->saveCategories($data, $request);
            $this->saveCompany($data, $request);

            if ($request->has('cover')) {
                $this->saveImage($request->get('cover'), $data->image_path, $data, 'cover');
            }

            if ($request->has('profile')) {
                $this->saveImage($request->get('profile'), $data->image_path, $data, 'profile');
            }

            return $data;
        });

        return $store;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        if(Auth::user()!=null){
            $user = Auth::user();
        }
        $user = null;

        $data['store'] = $store->load([
            'categories',
            'products.ratings',
            'products.images' => function ($query) {
                $query->whereType('primary-thumbnail');
            },
            'products' => function ($query) {
                $query->latest();
            }
        ]);

        // Check if there are pending transactions
        $enableDeleteButton = 1;
        $store->products->each(function ($item, $key) use(&$enableDeleteButton) {
            foreach($item->order_details as $orderDetail) {
                if($orderDetail->status == 'Pending' || $orderDetail->status == 'In Transit') {
                    $enableDeleteButton = 0;
                    return false;
                }
            }
        });

        $lang = trans('store-management');
        return view('store.show', compact('store','lang', 'enableDeleteButton'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        $data['store'] = $store->load(['images', 'categories', 'addresses', 'numbers', 'company']);
        $store->address = $store->addresses->first() ?: new Address(['id' => null, 'address' => null, 'city' => null, 'province' => null]);
        $store->number = $store->numbers->first() ?: new Number(['id' => null, 'number' => null]);

        if (! $store->company) {
            $store->setRelation('company', new StoreCompany(['id' => null, 'store_id' => null, 'name' => null, 'address' => null, 'facebook' => null, 'twitter' => null, 'instagram' => null]));
        }

        session()->flash('data', $store);
        $lang = trans('store-management');

        return view('store.edit', compact('store','lang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreValidation $request, Store $store)
    {
        authorize('stores.update', $store);

        $store = DB::transaction(function () use ($request, $store) {
            $store->fill($request->all())
                ->save();

            $this->saveAddress($store, $request);
            $this->saveNumber($store, $request);
            $this->saveCategories($store, $request);
            $this->saveCompany($store, $request);

            if ($request->has('cover')) {
                $this->saveImage($request->get('cover'), $store->image_path, $store, 'cover');
            }

            if ($request->has('profile')) {
                $this->saveImage($request->get('profile'), $store->image_path, $store, 'profile');
            }

            return $store;
        });

        return $store;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        authorize('stores.delete', $store);

        $store->delete(); // Soft Delete store
        $store->products->each->delete(); // Soft Delete products

        return redirect()->route('shopping.stores.index');
    }

    ///////////////////////
    // Private functions //
    ///////////////////////

    private function saveAddress($data, $request)
    {
        $address = $request->input('address');
        $address['id'] = $request->input('address.id', null);
        $address['is_active'] = true;

        $data->addresses()
            ->updateOrCreate(['id' => $address['id']], [
                'city' => $address['city']['name'],
                'city_id' => $address['city']['id'],
                'province' => $address['province'],
                'province_id' => $address['city']['province_id'],
                'landmark' => $address['landmark'],
                'address' => $address['address'],
                'is_active' => true
            ]);
    }

    private function saveNumber($data, $request)
    {
        $number = $request->input('number');
        $number['id'] = $request->input('number.id', null);
        $number['is_active'] = true;

        $data->numbers()
            ->updateOrCreate(['id' => $number['id']], $number);
    }

    private function saveCategories($data, $request)
    {
        if ($request->has('categories')) {
            $categoryIds = collect($request->get('categories'))
                ->pluck('id')
                ->toArray();

            $data->categories()->sync($categoryIds);
        }
    }

    public function saveCompany($data, $request)
    {
        if ($request->has('company')) {
            if (! $data->company) {
                $data->company()->create($request->get('company'));
            } else {
                $data->company->fill($request->get('company'))
                    ->save();
            }
        }
    }

    private function saveImage($image, $path, $data, $type)
    {
        $fileName = $type . '.jpg';

        Image::make($image)
            ->encode('jpg', 100)
            ->save($path . $fileName);

        $data->images()->create([
            'name' => $fileName,
            'type' => $type,
        ]);
    }
}
