<?php

namespace App\Http\Controllers\Shopping;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Issue;
use Auth;

class IssuesController extends Controller
{

	public function index()
    {
        $user = null;
        if(Auth::user()){
            $user = Auth::user();
        }
    	$status = request()->input('status');

    	if($status == 'all-tickets' || ($status != 'open-tickets' && $status != 'closed-tickets')) { 
    		$pageTitle = 'All Tickets'; 
    		$issues =Issue::with('chat_room')->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
    	} elseif($status == 'open-tickets') { 
    		$pageTitle = 'Open Tickets'; 
    		$issues =Issue::where('user_id', Auth::user()->id)->where('status', 'open')->orderBy('id', 'desc')->get();
    	} elseif($status == 'closed-tickets') { 
    		$pageTitle = 'Closed Tickets'; 
    		$issues =Issue::where('user_id', Auth::user()->id)->where('status', 'closed')->orderBy('id', 'desc')->get();
    	}

        $data = [
            'user' => $user,
        	'lang' => trans('messages'),
        	'issues' => $issues,
        	'openTicketsCount' => Issue::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->where('status', 'open')->count(),
        	'pageTitle' => $pageTitle
        ];

        return view('issues.index')->with($data);
    }

    public function create() {
        
    }

    public function store(Request $request) {

    }

    public function show($id) {
        
    }

    public function edit($id) {
        
    }

    public function update(Request $request, $id) {
        
    }

    public function destroy($id) {
        
    }

}