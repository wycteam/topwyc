<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Shopping\Product;
use App\Models\Shopping\Category;
use App\Http\Requests\ProductValidation;
use Auth,DB;
use Intervention\Image\Facades\Image;

class ProductController extends ApiController
{
    protected $user;
	protected $validation;

    public function __construct(Product $model)
    {
        $this->model = $model;
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
        $this->validation = new ProductValidation;
    }

    public function index(){
        //get all products (soft deleted products included)
        $data = $this->model::withTrashed()
                ->with('user')
                ->get();
        $product = $data->map(
            function($data) {
                return [
                    "id" => $data->id,
                    "is_active" => $data->is_active,
                    "deleted_at" => $data->deleted_at,
                    "is_draft" => $data->is_draft,
                    "status" => $data->status,
                    "slug" => $data->slug,
                    "name" => $data->name,
                    "main_image" => $data->main_image,
                    "created_at" => $data->created_at,
                    "user_id" => $data->user['id'],
                    "first_name" => $data->user['first_name'],
                    "last_name" => $data->user['last_name'],
                    "is_verified" => $data->user['is_verified'],
                    "stock" => $data->stock,
                    "sold_count" => $data->sold_count
                ];
            }
        );
        return $this->setStatusCode(200)
            ->respond($product);
    }

    public function update(Request $request, $id){
        //get store by id
        $upprod = $this->model->withTrashed()->findOrFail($id);

        if($upprod){
            if($request->get('req')=='promote') // prmote store
            {
                $upprod->status = 'featured';
                $upprod->save();
                return $this->respondUpdated($upprod);
            }
            else if($request->get('req')=='demote') // demote store
            {
                $upprod->status = null;
                $upprod->save();
                return $this->respondUpdated($upprod);
            }
            else if($request->get('req')=='delete') // soft delete
            {
                $upprod->delete();
                return $this->respondUpdated($upprod);
            }
            else if($request->get('req')=='deactivate') // deactivate
            {
                $upprod->is_active = 0;
                $upprod->save();
                return $this->respondUpdated($upprod);
            }
            else if($request->get('req')=='activate') // activate
            {
                $upprod->is_active = 1;
                $upprod->save();
                return $this->respondUpdated($upprod);
            }
            else if($request->get('req')=='searchable') // searchable
            {
                $upprod->is_searchable = 1;
                $upprod->save();
                return $this->respondUpdated($upprod);
            }
            else if($request->get('req')=='restore') // restore
            {
                $upprod->restore();
                return $this->respondUpdated($upprod);
            }
            else if($request->get('req')=='permanent') // restore
            {
                $upprod->forceDelete();
                return $this->respondUpdated($upprod);
            }
        }
    }




    ///////////////
    //additional///
    ///////////////
    public function saveDrafts(Request $request)
    {
         $data = new Product($request->all());
            $data->user_id = Auth::user()->id;
            $data->is_draft = 1;
            $data->is_active = 0;
            $data->save();

            $data->color_variations()->delete();
            $data->size_variations()->delete();
            $data->images()->delete();

            if (! empty($varcolor = $request->get('varcolor'))) {
                foreach ($varcolor as $vcolor) {
                    if($vcolor['label']!=''){
                        $color = $data->color_variations()
                            ->updateOrCreate(['id' => $vcolor['id']], $vcolor);

                            if($vcolor['temp_id']==1){
                                if (! empty($varsize = $request->get('varsize1'))) {
                                    foreach ($varsize as $vsize) {
                                            $data->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $data->id, 'color_id' => $color->id, 'count_id' => 1, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==2){
                                if (! empty($varsize = $request->get('varsize2'))) {
                                    foreach ($varsize as $vsize) {
                                            $data->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $data->id, 'color_id' => $color->id, 'count_id' => 2, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==3){
                                if (! empty($varsize = $request->get('varsize3'))) {
                                    foreach ($varsize as $vsize) {
                                            $data->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $data->id, 'color_id' => $color->id, 'count_id' => 3, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==4){
                                if (! empty($varsize = $request->get('varsize4'))) {
                                    foreach ($varsize as $vsize) {
                                            $data->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $data->id, 'color_id' => $color->id, 'count_id' => 4, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==5){
                                if (! empty($varsize = $request->get('varsize5'))) {
                                    foreach ($varsize as $vsize) {
                                            $data->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $data->id, 'color_id' => $color->id, 'count_id' => 5, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==6){
                                if (! empty($varsize = $request->get('varsize6'))) {
                                    foreach ($varsize as $vsize) {
                                            $data->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $data->id, 'color_id' => $color->id, 'count_id' => 6, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==7){
                                if (! empty($varsize = $request->get('varsize7'))) {
                                    foreach ($varsize as $vsize) {
                                            $data->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $data->id, 'color_id' => $color->id, 'count_id' => 7, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==8){
                                if (! empty($varsize = $request->get('varsize8'))) {
                                    foreach ($varsize as $vsize) {
                                            $data->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $data->id, 'color_id' => $color->id, 'count_id' => 8, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==9){
                                if (! empty($varsize = $request->get('varsize9'))) {
                                    foreach ($varsize as $vsize) {
                                            $data->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $data->id, 'color_id' => $color->id, 'count_id' => 9, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==10){
                                if (! empty($varsize = $request->get('varsize10'))) {
                                    foreach ($varsize as $vsize) {
                                            $data->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $data->id, 'color_id' => $color->id, 'count_id' => 10, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }

                    }
                }
            }

            //save categories to category_product table
            $this->saveCategories($data, $request);

            if ($request->has('image')) {
                $this->saveImage($request->get('image'), $data->image_path, $data, 'primary');
                $this->saveImageThumbnail($request->get('image'), $data->image_path, $data, 'primary-thumbnail');
            }

            if ($request->has('image2')) {
                $this->saveImage($request->get('image2'), $data->image_path, $data, 'primary2');
            }

            if ($request->has('image3')) {
                $this->saveImage($request->get('image3'), $data->image_path, $data, 'primary3');
            }

            if ($request->has('image4')) {
                $this->saveImage($request->get('image4'), $data->image_path, $data, 'primary4');
            }

            if ($request->has('image5')) {
                $this->saveImage($request->get('image5'), $data->image_path, $data, 'primary5');
            }

            if ($request->has('image6')) {
                $this->saveImage($request->get('image6'), $data->image_path, $data, 'primary6');
            }

            if ($request->has('descimage1')) {
                $this->saveImage($request->get('descimage1'), $data->image_path, $data, 'descimage1');
            }

            if ($request->has('descimage2')) {
                $this->saveImage($request->get('descimage2'), $data->image_path, $data, 'descimage2');
            }

            if ($request->has('descimage3')) {
                $this->saveImage($request->get('descimage3'), $data->image_path, $data, 'descimage3');
            }

            if ($request->has('descimage4')) {
                $this->saveImage($request->get('descimage4'), $data->image_path, $data, 'descimage4');
            }

            if ($request->has('descimage5')) {
                $this->saveImage($request->get('descimage5'), $data->image_path, $data, 'descimage5');
            }

            if ($request->has('descimage6')) {
                $this->saveImage($request->get('descimage6'), $data->image_path, $data, 'descimage6');
            }

            if ($request->has('descimage7')) {
                $this->saveImage($request->get('descimage7'), $data->image_path, $data, 'descimage7');
            }

            if ($request->has('descimage8')) {
                $this->saveImage($request->get('descimage8'), $data->image_path, $data, 'descimage8');
            }

            if ($request->has('descimage9')) {
                $this->saveImage($request->get('descimage9'), $data->image_path, $data, 'descimage9');
            }

            if ($request->has('descimage10')) {
                $this->saveImage($request->get('descimage10'), $data->image_path, $data, 'descimage10');
            }

            if ($request->has('descimage11')) {
                $this->saveImage($request->get('descimage11'), $data->image_path, $data, 'descimage11');
            }

            if ($request->has('descimage12')) {
                $this->saveImage($request->get('descimage12'), $data->image_path, $data, 'descimage12');
            }

            if ($request->has('descimage13')) {
                $this->saveImage($request->get('descimage13'), $data->image_path, $data, 'descimage13');
            }

            if ($request->has('descimage14')) {
                $this->saveImage($request->get('descimage14'), $data->image_path, $data, 'descimage14');
            }

            if ($request->has('descimage15')) {
                $this->saveImage($request->get('descimage15'), $data->image_path, $data, 'descimage15');
            }

            if ($request->has('descimage16')) {
                $this->saveImage($request->get('descimage16'), $data->image_path, $data, 'descimage16');
            }

            if ($request->has('descimage17')) {
                $this->saveImage($request->get('descimage17'), $data->image_path, $data, 'descimage17');
            }

            if ($request->has('descimage18')) {
                $this->saveImage($request->get('descimage18'), $data->image_path, $data, 'descimage18');
            }

            if ($request->has('descimage19')) {
                $this->saveImage($request->get('descimage19'), $data->image_path, $data, 'descimage19');
            }

            if ($request->has('descimage20')) {
                $this->saveImage($request->get('descimage20'), $data->image_path, $data, 'descimage20');
            }

            if ($request->has('colorImage1')) {
                $this->saveImage2($request->get('colorImage1'), $data->image_path_color, 'color1');
            }

            if ($request->has('colorImage2')) {
                $this->saveImage2($request->get('colorImage2'), $data->image_path_color, 'color2');
            }

            if ($request->has('colorImage3')) {
                $this->saveImage2($request->get('colorImage3'), $data->image_path_color, 'color3');
            }

            if ($request->has('colorImage4')) {
                $this->saveImage2($request->get('colorImage4'), $data->image_path_color, 'color4');
            }

            if ($request->has('colorImage5')) {
                $this->saveImage2($request->get('colorImage5'), $data->image_path_color, 'color5');
            }

            if ($request->has('colorImage6')) {
                $this->saveImage2($request->get('colorImage6'), $data->image_path_color, 'color6');
            }

            if ($request->has('colorImage7')) {
                $this->saveImage2($request->get('colorImage7'), $data->image_path_color, 'color7');
            }

            if ($request->has('colorImage8')) {
                $this->saveImage2($request->get('colorImage8'), $data->image_path_color, 'color8');
            }

            if ($request->has('colorImage9')) {
                $this->saveImage2($request->get('colorImage9'), $data->image_path_color, 'color9');
            }

            if ($request->has('colorImage10')) {
                $this->saveImage2($request->get('colorImage10'), $data->image_path_color, 'color10');
            }


            $jsonData = $this->createResponseData($data,'success');
            return $this->setStatusCode(200)
                ->respond($jsonData);
            //return $data;
    }

    public function saveProduct(ProductValidation $request, Product $product)
    {
        $product = DB::transaction(function () use ($request, $product) {

            $product = $product->fill($request->all());
            $product->user_id = Auth::user()->id;
            $product->save();
            
            if (! empty($varcolor = $request->get('varcolor'))) {
                foreach ($varcolor as $vcolor) {
                    if($vcolor['label']!=''){
                        $color = $product->color_variations()
                            ->updateOrCreate(['id' => $vcolor['id']], $vcolor);

                            if($vcolor['temp_id']==1){
                                if (! empty($varsize = $request->get('varsize1'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 1, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==2){
                                if (! empty($varsize = $request->get('varsize2'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 2, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==3){
                                if (! empty($varsize = $request->get('varsize3'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 3, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==4){
                                if (! empty($varsize = $request->get('varsize4'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 4, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==5){
                                if (! empty($varsize = $request->get('varsize5'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 5, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==6){
                                if (! empty($varsize = $request->get('varsize6'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 6, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==7){
                                if (! empty($varsize = $request->get('varsize7'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 7, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==8){
                                if (! empty($varsize = $request->get('varsize8'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 8, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==9){
                                if (! empty($varsize = $request->get('varsize9'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 9, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }
                            if($vcolor['temp_id']==10){
                                if (! empty($varsize = $request->get('varsize10'))) {
                                    foreach ($varsize as $vsize) {
                                            $product->size_variations()
                                                ->updateOrCreate(['id' => $vsize['id']], ['product_id' => $product->id, 'color_id' => $color->id, 'count_id' => 10, 'label' => $vsize['label'], 'stock' => $vsize['stock']]);
                                    }
                                }                                
                            }

                    }
                }
            }

            //save categories to category_product table
            $this->saveCategories($product, $request);

            if ($request->has('image')) {
                $this->saveImage($request->get('image'), $product->image_path, $product, 'primary');
                $this->saveImageThumbnail($request->get('image'), $product->image_path, $product, 'primary-thumbnail');
            }

            if ($request->has('image2')) {
                $this->saveImage($request->get('image2'), $product->image_path, $product, 'primary2');
            }

            if ($request->has('image3')) {
                $this->saveImage($request->get('image3'), $product->image_path, $product, 'primary3');
            }

            if ($request->has('image4')) {
                $this->saveImage($request->get('image4'), $product->image_path, $product, 'primary4');
            }

            if ($request->has('image5')) {
                $this->saveImage($request->get('image5'), $product->image_path, $product, 'primary5');
            }

            if ($request->has('image6')) {
                $this->saveImage($request->get('image6'), $product->image_path, $product, 'primary6');
            }
            
            if ($request->has('descimage1')) {
                $this->saveImage($request->get('descimage1'), $product->image_path, $product, 'descimage1');
            }

            if ($request->has('descimage2')) {
                $this->saveImage($request->get('descimage2'), $product->image_path, $product, 'descimage2');
            }

            if ($request->has('descimage3')) {
                $this->saveImage($request->get('descimage3'), $product->image_path, $product, 'descimage3');
            }

            if ($request->has('descimage4')) {
                $this->saveImage($request->get('descimage4'), $product->image_path, $product, 'descimage4');
            }

            if ($request->has('descimage5')) {
                $this->saveImage($request->get('descimage5'), $product->image_path, $product, 'descimage5');
            }

            if ($request->has('descimage6')) {
                $this->saveImage($request->get('descimage6'), $product->image_path, $product, 'descimage6');
            }

            if ($request->has('descimage7')) {
                $this->saveImage($request->get('descimage7'), $product->image_path, $product, 'descimage7');
            }

            if ($request->has('descimage8')) {
                $this->saveImage($request->get('descimage8'), $product->image_path, $product, 'descimage8');
            }

            if ($request->has('descimage9')) {
                $this->saveImage($request->get('descimage9'), $product->image_path, $product, 'descimage9');
            }

            if ($request->has('descimage10')) {
                $this->saveImage($request->get('descimage10'), $product->image_path, $product, 'descimage10');
            }

            if ($request->has('descimage11')) {
                $this->saveImage($request->get('descimage11'), $product->image_path, $product, 'descimage11');
            }

            if ($request->has('descimage12')) {
                $this->saveImage($request->get('descimage12'), $product->image_path, $product, 'descimage12');
            }

            if ($request->has('descimage13')) {
                $this->saveImage($request->get('descimage13'), $product->image_path, $product, 'descimage13');
            }

            if ($request->has('descimage14')) {
                $this->saveImage($request->get('descimage14'), $product->image_path, $product, 'descimage14');
            }

            if ($request->has('descimage15')) {
                $this->saveImage($request->get('descimage15'), $product->image_path, $product, 'descimage15');
            }

            if ($request->has('descimage16')) {
                $this->saveImage($request->get('descimage16'), $product->image_path, $product, 'descimage16');
            }

            if ($request->has('descimage17')) {
                $this->saveImage($request->get('descimage17'), $product->image_path, $product, 'descimage17');
            }

            if ($request->has('descimage18')) {
                $this->saveImage($request->get('descimage18'), $product->image_path, $product, 'descimage18');
            }

            if ($request->has('descimage19')) {
                $this->saveImage($request->get('descimage19'), $product->image_path, $product, 'descimage19');
            }

            if ($request->has('descimage20')) {
                $this->saveImage($request->get('descimage20'), $product->image_path, $product, 'descimage20');
            }            

            if ($request->has('colorImage1')) {
                $this->saveImage2($request->get('colorImage1'), $product->image_path_color, 'color1');
            }

            if ($request->has('colorImage2')) {
                $this->saveImage2($request->get('colorImage2'), $product->image_path_color, 'color2');
            }

            if ($request->has('colorImage3')) {
                $this->saveImage2($request->get('colorImage3'), $product->image_path_color, 'color3');
            }

            if ($request->has('colorImage4')) {
                $this->saveImage2($request->get('colorImage4'), $product->image_path_color, 'color4');
            }

            if ($request->has('colorImage5')) {
                $this->saveImage2($request->get('colorImage5'), $product->image_path_color, 'color5');
            }

            if ($request->has('colorImage6')) {
                $this->saveImage2($request->get('colorImage6'), $product->image_path_color, 'color6');
            }

            if ($request->has('colorImage7')) {
                $this->saveImage2($request->get('colorImage7'), $product->image_path_color, 'color7');
            }

            if ($request->has('colorImage8')) {
                $this->saveImage2($request->get('colorImage8'), $product->image_path_color, 'color8');
            }

            if ($request->has('colorImage9')) {
                $this->saveImage2($request->get('colorImage9'), $product->image_path_color, 'color9');
            }

            if ($request->has('colorImage10')) {
                $this->saveImage2($request->get('colorImage10'), $product->image_path_color, 'color10');
            }

            return $product;
        });

        $jsonData = $this->createResponseData($product,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function getRelatedItems(Request $request)
    {
        $product = Product::findOrFail($request->get('product'));
        $categories = $product->categories()->get();
        $related_products = [];

        foreach ($categories as $key => $value) {
            //push to related products
            $category = Category::findOrFail($value->id);
            $products_to_push = $this->getFillableProducts($category, count($related_products),$request->get('product'));

            $related_products = array_merge($related_products,$products_to_push->all());// $related_products->merge($products_to_push);
            $related_products = $this->unique_multidim_array($related_products,'id');// $related_products->merge($products_to_push);
            
            if(count($related_products) > 8){
                break;
            }
        }

        return $this->setStatusCode(200)
                ->respond($related_products);
    }

    function unique_multidim_array($array, $key) { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 
        
        foreach($array as $val) { 
            if (!in_array($val[$key], $key_array)) { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 

    private function getFillableProducts($category,$fetched_count, $current_product){
        return $category->products()
            ->where('id','<>',$current_product)
            ->select(
                'id'
                ,'name'
                ,'cn_name'
                ,'slug'
                ,'price'
                ,'sale_price'
                ,'shipping_fee'
                ,'charge'
                ,'vat'
                ,'fee_included'               
                ,'discount'
                ,DB::raw('CONCAT("/shopping/images/product/",id,"/primary-thumbnail.jpg") as image')
            )
            ->take(8 - $fetched_count)
            ->get();
    }

    public function getGrandParentCategories(Request $request){
        $getChildrenCount = 30;
        $getGrandParent = 8;
        $categories = Category::
            with(['products' => function($q){
                $q->orderBy('created_at','desc');
                $q->take(3);
            }])
            ->where('parent_id',0)
            ->take($getGrandParent)
            ->get()
            ->values()
            ->toArray();

        foreach ($categories as $key => $value) {

            $p_categories = Category::
                with(['products' => function($q){
                    $q->orderBy('created_at','desc');
                    $q->take(3);
                }])
                ->where('parent_id',$value["id"])
                ->get();

            $categories[$key]["children"] = $p_categories
                ->toArray();

            //grandchild
            // $lacking = 0;
            foreach ($categories[$key]["children"] as $ckey => $cvalue) {
                
                $take = ($getChildrenCount / count($p_categories));// + $lacking ;

                $c_categories = Category::
                    with(['products' => function($q){
                        $q->orderBy('created_at','desc');
                        $q->take(3);
                    }])
                    ->where('parent_id',$cvalue["id"])
                    ->take(   $take  )
                    ->get();

                // $lacking = $take - count($c_categories);

                $categories[$key]["children"][$ckey]["children"] = $c_categories->toArray();
            }

        }
       
        return $this->setStatusCode(200)
            ->respond($categories);
    }

    public function getAllCategories(Request $request){
        $getChildrenCount = 30;
        $categories = Category::
            with(['products' => function($q){
                $q->orderBy('created_at','desc');
                $q->take(3);
            }])
            ->where('parent_id',0)
            ->get()
            ->values()
            ->toArray();

        foreach ($categories as $key => $value) {

            $p_categories = Category::
                with(['products' => function($q){
                    $q->orderBy('created_at','desc');
                    $q->take(3);
                }])
                ->where('parent_id',$value["id"])
                ->get();

            $categories[$key]["children"] = $p_categories
                ->toArray();

            //grandchild
            // $lacking = 0;
            foreach ($categories[$key]["children"] as $ckey => $cvalue) {
                
                $take = ($getChildrenCount / count($p_categories));// + $lacking ;

                $c_categories = Category::
                    with(['products' => function($q){
                        $q->orderBy('created_at','desc');
                        $q->take(3);
                    }])
                    ->where('parent_id',$cvalue["id"])
                    ->get();

                // $lacking = $take - count($c_categories);

                $categories[$key]["children"][$ckey]["children"] = $c_categories->toArray();
            }

        }
       
        return $this->setStatusCode(200)
            ->respond($categories);
    }

    public function formen(Request $request)
    {
        $mainCategory = "mensfashion";
        $category = Category::where('slug',$mainCategory)->first();
        $cats   = Category::where('id',$category->id)
                    ->orwhere(function ($query) use($category) {
                        $query->where('tree_parent', '=', $category->id)
                              ->orWhere('tree_parent', 'LIKE', '%' . $category->id."-" .'%');
                    })
                    ->get();

        $cats = $cats->pluck('id');

        $mens= $this->model::whereHas('categories' , function($query) use ($cats) {
            $query->whereIn('category_id', $cats);
        })
        ->select('id', 'name','cn_name','slug',DB::raw('CONCAT("/product/",id,"/primary-thumbnail.jpg") as image'),'price','sale_price','discount','shipping_fee','charge','vat','fee_included')
        ->where('is_active','1')->where('is_searchable','1')->skip(0)->take(10)->orderBy('id','desc')->get();

        return $this->setStatusCode(200)
            ->respond($mens);
    }

    public function forwomen(Request $request)
    {
        $mainCategory = "womensfashion";
        $category = Category::where('slug',$mainCategory)->first();
        $cats   = Category::where('id',$category->id)
                    ->orwhere(function ($query) use($category) {
                        $query->where('tree_parent', '=', $category->id)
                              ->orWhere('tree_parent', 'LIKE', '%' . $category->id."-" .'%');
                    })
                    ->get();

        $cats = $cats->pluck('id');

        $womens= $this->model::whereHas('categories' , function($query) use ($cats) {
            $query->whereIn('category_id', $cats);
        })
        ->select('id', 'name','cn_name','slug',DB::raw('CONCAT("/product/",id,"/primary-thumbnail.jpg") as image'),'price','sale_price','discount','shipping_fee','charge','vat','fee_included')
        ->where('is_active','1')->where('is_searchable','1')->skip(0)->take(10)->orderBy('id','desc')->get();

        return $this->setStatusCode(200)
            ->respond($womens);
    }

    public function forcyber(Request $request)
    {
        $mainCategory = "electronics";
        $category = Category::where('slug',$mainCategory)->first();
        $cats   = Category::where('id',$category->id)
                    ->orwhere(function ($query) use($category) {
                        $query->where('tree_parent', '=', $category->id)
                              ->orWhere('tree_parent', 'LIKE', '%' . $category->id."-" .'%');
                    })
                    ->get();

        $cats = $cats->pluck('id');

        $cyber= $this->model::whereHas('categories' , function($query) use ($cats) {
            $query->whereIn('category_id', $cats);
        })
        ->select('id', 'name','cn_name','slug',DB::raw('CONCAT("/product/",id,"/primary-thumbnail.jpg") as image'),'price','sale_price','discount','shipping_fee','charge','vat','fee_included')
        ->where('is_active','1')->where('is_searchable','1')->skip(0)->take(10)->orderBy('id','desc')->get();

        return $this->setStatusCode(200)
            ->respond($cyber);
    }



    //Private Functions

    private function saveCategories($data, $request)
    {
        if ($request->has('category_id')) {
            $categoryIds = collect($request->get('category_id'))
                ->pluck('id')
                ->toArray();

            $data->categories()->sync($categoryIds);
        }
    }

    private function saveImage($image, $path, $data, $type)
    {
        $resizeThis1 = ['primary', 'primary2', 'primary3', 'primary4', 'primary5', 'primary6'
        ];

        $resizeThis2 = ['descimage1', 'descimage2', 'descimage3', 'descimage4', 'descimage5', 'descimage6', 'descimage7', 'descimage8', 'descimage9', 'descimage10', 'descimage11', 'descimage12', 'descimage13', 'descimage14', 'descimage15', 'descimage16', 'descimage17', 'descimage18', 'descimage19', 'descimage20'];

        $fileName = $type . '.jpg';

        if(in_array($type, $resizeThis1)) {
            Image::make($image)
                ->fit(518, 454, function ($constraint) {
                    $constraint->upsize();
                })
                ->encode('jpg', 100)
                ->save($path . $fileName);
        } elseif(in_array($type, $resizeThis2)) {
            Image::make($image)
                ->resize(998, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->encode('jpg', 100)
                ->save($path . $fileName);
        } else {
            Image::make($image)
                ->encode('jpg', 100)
                ->save($path . $fileName);    
        }

        $data->images()->create([
            'name' => $fileName,
            'type' => $type,
        ]);
    }

    private function saveImage2($image, $path, $type)
    {
        $resizeThis = ['color1', 'color2', 'color3', 'color4', 'color5', 'color6', 'color7', 'color8', 'color9', 'color10'];

        $fileName = $type . '.jpg';

        if(in_array($type, $resizeThis)) {
            Image::make($image)
                ->fit(518, 454, function ($constraint) {
                    $constraint->upsize();
                })
                ->encode('jpg', 100)
                ->save($path . $fileName);
        } else {
            Image::make($image)
                ->encode('jpg', 100)
                ->save($path . $fileName);
        }
    }

    private function saveImageThumbnail($image, $path, $data, $type)
    {
        $fileName = $type . '.jpg';

        Image::make($image)->resize(480, null, function ($constraint) {
            $constraint->aspectRatio();
        })
        ->resizeCanvas(320, 280, 'center')
        ->encode('jpg', 100)
        ->save($path . $fileName);

        $data->images()->create([
            'name' => $fileName,
            'type' => $type,
        ]);
    }
}
