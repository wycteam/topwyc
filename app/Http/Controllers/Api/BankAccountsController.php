<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Shopping\BankAccounts;
use App\Http\Requests\NewBankAccountRequest;
use Auth;

class BankAccountsController extends ApiController
{
    protected $user;
	protected $validation;

    public function __construct(BankAccounts $model)
    {
        $this->model = $model;
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();   
            return $next($request);
        });
        $this->validation = new NewBankAccountRequest;
    }

    public function index(){
    	$data['bank_accounts'] = $this->model::with('bank')
    		->whereUserId($this->user->id)
    		->get();
        
    	$jsonData = $this->createResponseData($data,'success');
    	
    	return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function store(Request $request)
    {
        app(get_class($this->validation));
        $add['user_id'] = Auth::user()->id;

        $data = array_merge($request->all(), $add);
        $data = $this->model->create($data);
        $data->load('bank');

        return $this->respondCreated($data);
    }

    public function update_bank_account(Request $request, $user_id, $bank_account_id)
    {
        app(get_class($this->validation));

        $data = $this->model
            ->findOrFail($bank_account_id)
        ;

        $data->fill($request->all())
            ->save();
        $data->load('bank');
            
        return $this->respondUpdated($data);
    }

    public function show_bank_account(Request $request, $user_id, $bank_account_id)
    {
        $data = $this->model
            ->findOrFail($bank_account_id)
        ;
        $data->all();
        return $this->respondUpdated($data);
    }


}
