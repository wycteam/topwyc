<?php

namespace App\Http\Controllers\Api;

use App\Models\Rating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Acme\Filters\FeedbackFilters;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\ApiController;
use App\Models\Shopping\ProductReviews;
use App\Notifications\NewProductReview;
use App\Notifications\NewProductReviewComment;
use App\Http\Requests\ProductReviewsValidation;

class ProductReviewsController extends ApiController
{
    public function __construct(ProductReviews $model,FeedbackFilters $filters)
    {
        $this->model = $model;
        $this->filters = $filters;
        $this->validation = new ProductReviewsValidation;

        $this->middleware('auth:api', ['only' => ['store']]);
    }

    public function index(){
        $query = $this->filters?$this->model->filter($this->filters):$this->model;
        $data = $query
            ->whereParentId(0)
            ->get();
        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function store(Request $request)
    {
        app(get_class($this->validation));

        $authUser = Auth::user();

        $data = $this->model->create($request->all());

        if(!$data->parent_id){
            $rate = new Rating;
            $rate->user_id = $data->user_id;
            $rate->rateable_id = $data->product_id;
            $rate->rateable_type = 'App\Models\Shopping\Product';
            $rate->rating = $data->rating_id;
            $rate->review = $request->review;
            $rate->save();
            $data->rating_id = $rate->id;

            $data->products->user->notify(new NewProductReview($data, $authUser));
        }  elseif ($parentProductReview = ProductReviews::find($data->parent_id)) {
            if ($parentProductReview->user_id != $authUser->id) {
                $parentProductReview->user->notify(new NewProductReviewComment($data, $authUser));
            } else {
                $data->products->user->notify(new NewProductReviewComment($data, $authUser));
            }
        }

        $data->save();
        if($request->has('imageTemp'))
        {
            $this->saveImage($request->get('imageTemp'), $data->image_path, $data, 'review');
        }

        $data = $this->model->whereId($data->id)->first();
        return $this->respondCreated($data);
    }

    public function count(Request $request){
        if($request->has('product'))
        {
            $product_id = $request->get('product');
            $data = $this->model
                ->where('parent_id',0)
                ->where('product_id',$product_id)
                ->count();
            return $this->setStatusCode(200)
                ->respond($data);
        }
    }

    private function saveImage($image, $path, $data, $type)
    {
        $fileName = $type . '.jpg';

        $var=Image::make($image)
            ->encode('jpg', 100)
            ->save($path . $fileName);

        $data->images()->create([
            'name' => $fileName,
            'type' => $type,
        ]);
    }
}
