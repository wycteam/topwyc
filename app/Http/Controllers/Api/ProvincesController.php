<?php

namespace App\Http\Controllers\Api;

use App\Models\Province;
use Illuminate\Http\Request;
use App\Acme\Filters\ProvinceFilters;
use App\Http\Controllers\ApiController;
use App\Http\Requests\ProvinceValidation;

class ProvincesController extends ApiController
{
    protected $model;
    protected $filters;
    protected $validation;

    public function __construct(Province $model, ProvinceFilters $filters)
    {
        $this->model = $model;
        $this->filters = $filters;
        $this->validation = new ProvinceValidation;
    }
}
