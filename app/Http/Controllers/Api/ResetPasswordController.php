<?php

namespace App\Http\Controllers\Api;

use Auth, Input;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\User;
use App\Models\QuestionUser;
use App\Http\Requests\ResetPasswordValidation;

class ResetPasswordController extends ApiController
{
    public function searchEmail(ResetPasswordValidation $request){

        $email = $request->input('email2');

        $data = User::where('email',$email)->get();

    	if(count($data) != 0){
            $ret['id'] = $data[0]->id;
            $ret['response'] = '1';
        } else{
            $ret['response'] = '0';
        }

        return $ret;
    }

    public function searchAltEmail(ResetPasswordValidation $request){

        $primary_email = $request->input('primary_email');
        $email = $request->input('email2');

        $data1 = User::where('email',$primary_email)->get();

        if($data1[0]->secondary_email=='')
            $ret['response'] = '3';
        else {

            $data2 = User::where('secondary_email',$email)->get();

            if(count($data2) != 0){
                $ret['id'] = $data2[0]->id;
                $ret['response'] = '1';
            } else{
                $ret['response'] = '0';
            }

        }

        return $ret;
    }

    public function getQuestion($id){

        $question = QuestionUser::with('question')
                    ->whereUserId($id)->get();

        return $this->setStatusCode(200)
            ->respond($question);
    }

    public function answerQuestion($id,$userid){

        $input = Input::all();

        $question = QuestionUser::whereQuestionId($id)
                    ->whereUserId($userid)->get();

        if($question[0]->answer == $input['answer']){
            $ret['response'] = '1';
        } else{
            $ret['response'] = '0';
        }
                 
        return $ret;
    }

}
