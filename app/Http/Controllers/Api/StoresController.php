<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Shopping\Store;
use App\Models\Shopping\Product;
use App\Models\Shopping\OrderDetail;
use Illuminate\Http\Request;
use App\Acme\Filters\UserFilters;
use App\Http\Requests\UserValidation;
use App\Http\Requests\StoreValidation;
use Auth, DB;
use App\Http\Controllers\ApiController;

class StoresController extends ApiController
{
	protected $stores;
	protected $user;

    public function __construct(Store $model)
    {
        $this->model = $model;
        $this->middleware(function ($request, $next) {
            $user = Auth::user();   
            return $next($request);
        });

    }

    public function index(){
        //get all stores (soft deleted stores included)
        $data = $this->model::withTrashed()
                ->with('user')
                ->withCount('products')
                ->get();

        $store = $data->map(
            function($data) {
                return [
                    "id" => $data->id,
                    "is_active" => $data->is_active,
                    "deleted_at" => $data->deleted_at,
                    "status" => $data->status,
                    "slug" => $data->slug,
                    "name" => $data->name,
                    "profile_image" => $data->profile_image,
                    "created_at" => $data->created_at,
                    "user_id" => $data->user['id'],
                    "first_name" => $data->user['first_name'],
                    "last_name" => $data->user['last_name'],
                    "is_verified" => $data->user['is_verified'],
                    "products_count" => $data->products_count
                ];
            }
        );

        return $this->setStatusCode(200)
            ->respond($store);
    }

    public function update(Request $request, $id){
        //get store by id
        $upstore = $this->model->withTrashed()->findOrFail($id);

        if($upstore){
            if($request->get('req')=='promote') // prmote store
            {
                $upstore->status = 'featured';
                $upstore->save();
                return $this->respondUpdated($upstore);
            }
            else if($request->get('req')=='demote') // demote store
            {
                $upstore->status = null;
                $upstore->save();
                return $this->respondUpdated($upstore);
            }
            else if($request->get('req')=='delete') // soft delete
            {
                $upstore->delete();
                return $this->respondUpdated($upstore);
            }
            else if($request->get('req')=='deactivate') // deactivate
            {
                $upstore->is_active = 0;
                $upstore->save();
                return $this->respondUpdated($upstore);
            }
            else if($request->get('req')=='activate') // activate
            {
                $upstore->is_active = 1;
                $upstore->save();
                return $this->respondUpdated($upstore);
            }
            else if($request->get('req')=='restore') // restore
            {
                $upstore->restore();
                return $this->respondUpdated($upstore);
            }
            else if($request->get('req')=='permanent') // restore
            {
                $upstore->forceDelete();
                return $this->respondUpdated($upstore);
            }
        } 
    }




    ////////////////////////
    //additional functions//
    ///////////////////////
    public function getAllProds(){
        $data['prods'] = Product::get();
        $jsonData = $this->createResponseData($data,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function updateAllProds(Request $request, $id){
        $prod = Product::findOrFail($id);
        $prod->charge = $request->get('charge');
        $prod->shipping_fee = $request->get('shipping_fee');
        $prod->shipping_fee_2go = $request->get('shipping_fee_2go');
        $prod->vat = $request->get('vat');
        $prod->vat_2go = $request->get('vat_2go');
        $prod->save();
        $jsonData = $this->createResponseData($prod,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function getUserStores(){
    	$data['stores'] = $this->model::where('user_id', Auth::user()->id)->where('is_active',1)->get();
    	$jsonData = $this->createResponseData($data,'success');
    	return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function featured(Request $request)
    {

        $store = DB::table('shopping.products as p')
                 ->leftJoin('shopping.stores as s', 'p.store_id', '=', 's.id')
                 ->leftJoin('wyc.users as u', 's.user_id', '=', 'u.id')
                 ->select(DB::raw("s.*,u.first_name as 'first_name',u.last_name as 'last_name',u.is_verified as 'is_verified', SUM(p.sold_count) as 'sold count', COUNT(p.id) as 'products_count'"))
                 ->where('s.is_active',1)
                 ->where('s.status','featured')
                 //->orderBy(DB::raw("SUM(p.sold_count)"),'DESC')
                 ->orderBy('s.id','DESC')
                 ->limit(4)
                 ->groupBy('s.id')
                 ->get();

        $storeid = $store->pluck('id');

        $data = $this->model
                ->whereIn('id', $storeid)
                ->select('id','name','cn_name','slug','description')
                ->get();

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function featuredstores(Request $request)
    {
        $data = DB::table('shopping.products as p')
                 ->leftJoin('shopping.stores as s', 'p.store_id', '=', 's.id')
                 ->leftJoin('wyc.users as u', 's.user_id', '=', 'u.id')
                 ->select(DB::raw("s.*,u.first_name as 'first_name',u.last_name as 'last_name',u.is_verified as 'is_verified', SUM(p.sold_count) as 'sold count', COUNT(p.id) as 'products_count'"))
                 ->where('s.is_active',1)
                 ->orderBy(DB::raw("SUM(p.sold_count)"),'DESC')
                 ->limit(4)
                 ->groupBy('s.id')
                 ->get();

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function productstore(Request $request, $store_id)
    {
        $data = $this->model::where('id',$store_id)->get();

        $jsonData = $this->createResponseData($data,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function countorder($store_id)
    {
        $data = Product::where('store_id',$store_id)->get();

        $prodid = $data->pluck('id');

        $order = OrderDetail::whereIn('product_id', $prodid)->get();

        return $this->setStatusCode(200)
            ->respond($order);
    }
}
