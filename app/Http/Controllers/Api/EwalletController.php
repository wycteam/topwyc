<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Shopping\Ewallet;
use Illuminate\Http\Request;
use App\Acme\Filters\UserFilters;
use App\Http\Requests\UserValidation;
use Auth;
use App\Http\Controllers\ApiController;

class EwalletController extends ApiController
{
	protected $amount;
    protected $user;
	protected $usable;

    public function __construct(Ewallet $model)
    {
        $this->model = $model;
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();   
            $this->amount = $this->model::where('user_id', $this->user->id)->first()->amount;
	        $this->usable = $this->user->ewallet->usable_ewallet;
            return $next($request);
        });

    }

    public function index(){
    	$data['amount'] = $this->amount;
    	$jsonData = $this->createResponseData($data,'success');
    	return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    
    public function getUsable(){
        $data['usable'] = $this->usable;
        $jsonData = $this->createResponseData($data,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

}
