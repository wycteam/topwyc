<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Shopping\OrderDetail;
use App\Models\Shopping\Order;
use App\Models\Shopping\Product;
use App\Models\Awb;

use Illuminate\Http\Request;
use App\Acme\Filters\UserFilters;
use App\Http\Requests\UserValidation;
use App\Http\Controllers\ApiController;
use App\Notifications\OrderStatusUpdate;

use DB, Auth, Carbon\Carbon, Validator;

class OrderDetailsController extends ApiController
{
    protected $model;
    protected $filters;
    protected $validation;

    public function __construct(OrderDetail $model)
    {
        $this->model = $model;
        $this->middleware(function ($request, $next) {
            $user = Auth::user();   
            return $next($request);
        });
    }

    public function getDetails(Request $request, $order_id)
    {
        $data = $this->model::with('product')->where('order_id',$order_id)->get();
        $jsonData = $this->createResponseData($data,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function getSummary(Request $request, $order_id)
    {
        $data = $this->model::with('product')->where('order_id',$order_id)->get();
        $subtotal = 0;
        foreach($data as $d){
            $subtotal += ($d->quantity * $d->price_per_item);
        }
        $summary['subtotal'] = $subtotal;
        $summary['tax'] = $subtotal * 0.02;

        $jsonData = $this->createResponseData($summary,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function orders()
    {
        $data = OrderDetail::with('order.user','product.store')->get();
        $orders = $data->map(
            function($data) {
                return [
                    "id" => $data->id,
                    "status" => $data->status,
                    "tracking_number" => $data->tracking_number,
                    "product_name" => $data->product->name,
                    "full_name" => $data->order->user->first_name . ' ' . $data->order->user->last_name,
                    "store_name" => $data->product->store->name
                ];
            }
        );

        $jsonData = $this->createResponseData($orders,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);

    }


    public function inTransitOrders()
    {
        $data = Order::with(['details', 'details.product.store'])->where('status','Complete')->get();
        foreach ($data as $dt) {
           foreach ($dt->details as $ds) {
                $order_id = $ds->order_id;
                $prod_id = $ds->product_id;
                $tracking_number = $ds->combined_tracking_number;
                $stid = Product::where('id',$prod_id)->select('store_id')->first();
                $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
                $awb = Awb::where('order_id',$order_id)->whereIn('product_id',$allprods)->first(); 
                $dt['awb_id'] = '';
                if($awb){
                    $dt['awb_id'] = $awb->code;
                    $dt['total_price'] = $awb->total_price;
                    $dt['wataf_cost'] = $awb->wataf_cost;
                    $dt['twogo_cost'] = $awb->twogo_cost;
                }
           }
        }

        $jsonData = $this->createResponseData($data,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function shipmentFilter(Request $request) {
        $validator = Validator::make($request->all(), [
            'filter' => 'required',
            'date' => ($request->filter == 'Daily' || $request->filter == 'Monthly') ? 'required' : '',
            'start' => ($request->filter == 'DateRange') ? 'required|date' : '',
            'end' => ($request->filter == 'DateRange') ? 'required|date' : ''
        ]);

        if($validator->fails()) {
            return json_encode([
                'errors' => $validator->errors()->getMessages(),
                'code' => 422
            ]);
        }

        $data = Order::with(['details', 'details.product.store'])->where('status','Complete');

        if($request->filter == 'Daily') {
            $data = $data->whereDate('created_at', '=', $request->date);
        } elseif($request->filter == 'Monthly') {
            $date = explode('/', $request->date);
            $data = $data->whereYear('created_at', '=', $date[0])
                ->whereMonth('created_at', '=', $date[1]);
        } elseif($request->filter == 'DateRange') {
            $data = $data->whereDate('created_at', '>=', $request->start)
                ->whereDate('created_at', '<=', $request->end);
        }

        $data = $data->get();
        foreach ($data as $dt) {
           foreach ($dt->details as $ds) {
                $order_id = $ds->order_id;
                $prod_id = $ds->product_id;
                $tracking_number = $ds->combined_tracking_number;
                $stid = Product::where('id',$prod_id)->select('store_id')->first();
                $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
                $awb = Awb::where('order_id',$order_id)->whereIn('product_id',$allprods)->first(); 
                $dt['awb_id'] = '';
                if($awb){
                    $dt['awb_id'] = $awb->code;
                    $dt['total_price'] = $awb->total_price;
                    $dt['wataf_cost'] = $awb->wataf_cost;
                    $dt['twogo_cost'] = $awb->twogo_cost;
                }
           }
        }

        $jsonData = $this->createResponseData($data,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function orderdetails($order_id)
    {
        $orders = OrderDetail::with(['order.user', 'order.details.product', 'product.store', 'order.address', 'fileImage', 'fileVideo'])->find($order_id);
        $orders['product_name']       = $orders['product']['name'];
        $orders['product_main_image'] = $orders['product']['main_image'];
        $orders['product_slug']       = $orders['product']['slug'];
        $orders['product_boxContent'] = $orders['product']['boxContent'];
        $orders['product_brand']      = $orders['product']['brand'];
        $orders['product_weight']     = $orders['product']['weight'];
        $orders['store_name']         = $orders['product']['store']['name'];
        $orders['store_slug']         = $orders['product']['store']['slug'];
        $orders['order_shipping_fee'] = $orders['order']['shipping_fee'];
        $orders['order_receiver']     = $orders['order']['name_of_receiver']; 
        $orders['order_alternate']    = $orders['order']['alternate_contact_number'];
        $orders['order_contact']      = $orders['order']['contact_number']; 
        $orders['full_address']       = $orders['order']['address']['address']." ".
                                        $orders['order']['address']['barangay']." ".
                                        $orders['order']['address']['city']." ".
                                        $orders['order']['address']['province']." ".
                                        $orders['order']['address']['country']." ".
                                        $orders['order']['address']['zip_code'];
        $orders['landmark']           = $orders['order']['address']['landmark'];

        $jsonData = $this->createResponseData($orders,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function getDetailsViaCaseNumber($caseNumber) {
        $order = OrderDetail::with(['order.user', 'order.details.product', 'product.store', 'order.address', 'fileImage', 'fileVideo'])->where('case_number', $caseNumber)->first();

        $jsonData = $this->createResponseData($order,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    //if the returned item is received by the seller
    public function receiveReturnedItem(Request $request){
        $order_id = $request->get('id');
        $order_details = OrderDetail::findOrFail($order_id);

        $order_details->returned_received_date = Carbon::now();

        $pr = ($order_details->sale_price > 0 ? $order_details->sale_price : $order_details->price_per_item);
        $amount = ($pr * $order_details->quantity) + $order_details->disc_fee;
        $buyer_id = $order_details->order()->first()->user()->first()->id;
        $product_id = $order_details->product_id;

        $prod = Product::findOrFail($product_id);
        $seller = User::findOrFail($prod->user_id);
        $total_fee = $order_details->shipping_fee + $order_details->charge + $order_details->vat;
        //deduct shipping fee to seller
        //$wallet = $seller->ewallet()->first();
        //$wallet->amount -= $total_fee;
        //$wallet->save();

        $this->returnStocks($product_id,$order_details->quantity);
        $order_details->balance = $this->returnEwallet($buyer_id,$amount);

        $this->updateParentOrderStatus($order_details->order_id);
        //notify the buyer that the item is received by the seller
        $order_details->order->user->notify(new OrderStatusUpdate($order_details, Auth::user()));

        $order_details->save();

        return $this->setStatusCode(200)
            ->respond($order_details);
    }

    private function returnStocks($product_id,$quantity){
        $product = Product::findOrFail($product_id);
        $product->stock += $quantity;
        $product->save();
    }

    private function returnEwallet($buyer_id,$amount){
        $buyer = User::findOrFail($buyer_id);
        $ewallet = $buyer->ewallet()->first();
        $ewallet->amount += $amount;
        $ewallet->save();
        return $ewallet->amount;
    }

    private function updateParentOrderStatus($id){
        $all_orders = OrderDetail::where('order_id',$id)->get();
        $incomplete = 0;
        $cancelled = 0;
        $complete = 0;
        foreach($all_orders as $all){
            if($all->status == "Pending" || $all->status == "In Transit"){
                $incomplete++;
            }
            else if($all->status == "Cancelled"){
                $cancelled++;
            }
            else{
                $complete++;
            }
        }
        $order = Order::findOrFail($id);

        if($incomplete>0){
            $order->status = "Incomplete";
        }
        else if($cancelled == $all_orders->count()){
            $order->status = "Cancelled";
        }
        else{
            $order->status = "Complete";
        }
        $order->save();
    }

    // public function checkIfSameStore($prod_id,$order_id){
    //     $stid = Product::where('id',$prod_id)->select('store_id')->first();
    //     $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
    //     //$stores = ShoppingStore::with(['order_details'])->where('id',$stid->store_id)->get();
    //     $getsame = OrderDetail::where('order_id',$order_id)->where('status','Pending')->whereIn('product_id',$allprods)->with('product')->get();
    //     return $getsame;
    // }
    public function checkIfSameStore($prod_id,$order_id){
        $stid = Product::where('id',$prod_id)->select('store_id')->first();
        $allprods = Product::where('store_id',$stid->store_id)->pluck('id');
        //$stores = ShoppingStore::with(['order_details'])->where('id',$stid->store_id)->get();
        $getsame = OrderDetail::where('order_id',$order_id)->with('product','product.store')->get();
        return $getsame;
    }
    

}
