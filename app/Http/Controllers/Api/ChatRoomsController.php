<?php

namespace App\Http\Controllers\Api;

use App\Models\ChatRoom;
use Illuminate\Http\Request;
use App\Acme\Filters\ChatRoomFilters;
use App\Http\Controllers\ApiController;
use App\Http\Requests\ChatRoomValidation;

class ChatRoomsController extends ApiController
{
    protected $model;
    protected $filters;
    protected $validation;

    public function __construct(ChatRoom $model, ChatRoomFilters $filters)
    {
        $this->model = $model;
        $this->filters = $filters;
        $this->validation = new ChatRoomValidation;
    }
}
