<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Message;
use App\Models\ChatRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Views\LatestMessage;
use App\Acme\Filters\MessageFilters;
use App\Models\DatabaseNotification;
use Illuminate\Support\Facades\Auth;
use App\Notifications\NewChatMessage;
use App\Http\Controllers\ApiController;
use App\Http\Requests\MessageValidation;
use Illuminate\Support\Facades\Notification;

class MessagesController extends ApiController
{
    protected $model;
    protected $filters;
    protected $validation;

    public function __construct(Message $model, MessageFilters $filters)
    {
        $this->model = $model;
        $this->filters = $filters;
        $this->validation = new MessageValidation;
    }

    public function index()
    {
        $user = Auth::user();

        $chatRoomIds = $user->chat_rooms->pluck('id')->toArray();

        $data = LatestMessage::with([
            'chat_room.users' => function ($query) use ($user) {
                $query->select('id', 'first_name', 'last_name', 'nick_name', 'gender')
                    ->where('id', '!=', $user->id);
            }
        ])
        ->with('user')
        ->whereIn('chat_room_id', $chatRoomIds)
        ->get();


        return $this->setStatusCode(200)
            ->respond($data);
    }


    public function store(Request $request)
    {
        app(get_class($this->validation));

        $data = DB::transaction(function () use ($request) {
            $authUser = Auth::user();
            $toUser = User::find($request->get('to_user'));
            $toChatRoom = ChatRoom::find($request->get('to_chat_room'));

            if ($toUser) {
                if (! ($chatRoom = $authUser->getMutualChatRoom($toUser->id))) {
                    $chatRoom = ChatRoom::create();
                    $chatRoom->addMember([$authUser->id, $toUser->id]);
                }

                $message = $authUser->messages()
                    ->create([
                        'chat_room_id' => $chatRoom->id,
                        'body' => $request->get('body'),
                    ]);

                $toUser->notify(new NewChatMessage($authUser, $message));
            } elseif ($toChatRoom) {
                $message = $authUser->messages()
                    ->create([
                        'chat_room_id' => $toChatRoom->id,
                        'body' => $request->get('body'),
                    ]);

                $toUsers = $toChatRoom->users()
                    ->where('id', '!=', $authUser->id)
                    ->get();

                Notification::send($toUsers, new NewChatMessage($authUser, $message));
            }


            return $message;
        });



        return $this->respondCreated($data);
    }

    public function getUserConversation(Request $request, User $user)
    {
        $authUser = Auth::user();

        $data = [];

        if ($chatRoom = $authUser->getMutualChatRoom($user->id)) {
            $data = $chatRoom->messages()
                ->with('user')
                ->orderBy('id', 'desc')
                ->paginate($request->get('limit', 5));
        }

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function getChatRoomMessages(Request $request, ChatRoom $chatRoom)
    {
        $authUser = Auth::user();

        $data = [];

        //if ($authUser->isMemberOf($chatRoom->id)) {
            $data = $chatRoom->messages()
                ->with('user')
                ->orderBy('id', 'desc')
                ->paginate($request->get('limit', 5));
        //}

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function getUsers(Request $request)
    {
        $user = Auth::user();

        $search = $request->get('search');

        $data = User::with(['chat_rooms' => function ($query) use ($user) {
            $query->whereHas('users', function ($query) use ($user) {
                $query->whereId($user->id);
            });
        }])
        ->select('id', 'first_name', 'last_name', 'email', 'nick_name', 'gender')
        ->where(function ($query) use ($search) {
            $query->where('first_name', 'like', '%'.$search.'%')
                ->orWhere('last_name', 'like', '%'.$search.'%')
                ->orWhere('email', 'like', '%'.$search.'%')
                ->orWhere('nick_name', 'like', '%'.$search.'%');
        })
        ->where('id', '!=', $user->id)
        ->orderBy('first_name')
        ->orderBy('last_name')
        ->get();

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function getNotifications(Request $request)
    {
        $user = Auth::user();

        $data = [];

        $data['notifications'] = $user->latest_message_notifications()
            ->latest()
            ->get();

        $data['notseen_count'] = $user->latest_message_notifications()->where('seen_at', null)->count();

        return $this->setStatusCode(200)
            ->respond($data);
    }

     public function getNotifications2(Request $request)
    {

    //     $user = Auth::user();

    //     $chatRoomIds = $user->chat_rooms->pluck('id')->toArray();

    //     $data = LatestMessage::with([
    //         'chat_room.users' => function ($query) use ($user) {
    //             $query->select('id', 'first_name', 'last_name', 'nick_name', 'gender')
    //                 ->where('id', '!=', $user->id);
    //         }
    //     ])
    //     ->with('user')
    //     ->whereIn('chat_room_id', $chatRoomIds)
    //     ->get();


    //     // foreach ($data as $key => $value) {
    //     //     $data[$key]['data']['user'] = array(
    //     //         'avatar' => $value['chat_room']['users'][0]['avatar']
    //     //     );
    //     // }
        $data = [];
         return $this->setStatusCode(200)
             ->respond($data);
     }

    public function markAllAsSeen(Request $request)
    {
        $user = Auth::user();

        $notificationIds = $user->latest_message_notifications()
            ->where('seen_at', null)
            ->get()
                ->pluck('id')
                ->toArray();

        $data = DatabaseNotification::whereIn('id', $notificationIds)
            ->update(['seen_at' => Carbon::now()]);

        return $this->respondUpdated($data);
    }

    public function markAllAsSeen2(Request $request)
    {
        $user = Auth::user();

        $chatRoomIds = $user->chat_rooms->pluck('id')->toArray();

        $notificationIds = LatestMessage::whereIn('chat_room_id', $chatRoomIds)
        ->where('user_id','<>',$user->id)
        ->get()
            ->pluck('id')
            ->toArray();

        $data = Message::whereIn('id', $notificationIds)
            ->update(['seen_at' => Carbon::now()]);

        return $this->respondUpdated($data);
    }



    public function markAsRead(Request $request, $id)
    {
        $user = Auth::user();
        
        $data = Message::whereId($id)
            ->where('user_id','<>',$user->id)
            ->update(['read_at' => Carbon::now()]);

        return $this->respondUpdated($data);
    }

    // Temporary: mark user conversation as read and seen at the same time
    public function markAllAsRead(Request $request)
    {
        $authUser = Auth::user();
        $toUserId = $request->get('to_user');
        $chatRoomId = $request->get('to_chat_room');
        $dateNow = Carbon::now();
        $chatRoom = null;

        if ($request->has('to_user') && ($chatRoom = $authUser->getMutualChatRoom($toUserId))) {
            $chatRoom->messages()
                ->where('read_at', null)
                ->where('user_id', $toUserId)
                ->update([
                    'seen_at' => $dateNow,
                    'read_at' => $dateNow,
                ]);
        } elseif ($request->has('to_chat_room') && ($authUser->isMemberOf($chatRoomId))) {
            $chatRoom = ChatRoom::find($chatRoomId);

            if ($chatRoom) {
                $chatRoom->messages()
                    ->where('read_at', null)
                    ->where('user_id', '!=', $authUser->id)
                    ->update([
                        'seen_at' => $dateNow,
                        'read_at' => $dateNow,
                    ]);
            }
        }

        return $this->respondUpdated($chatRoom);
    }
}
