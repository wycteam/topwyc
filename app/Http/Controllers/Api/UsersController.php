<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Models\User;
use App\Models\Address;
use Illuminate\Http\Request;
use App\Acme\Filters\UserFilters;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserValidation;
use App\Models\Shopping\Friends;
use App\Http\Controllers\ApiController;

class UsersController extends ApiController
{
    protected $model;
    protected $filters;
    protected $validation;

    public function __construct(User $model)
    {
        $this->model = $model;
        $this->validation = new UserValidation;
    }

    public function index()
    {
        $query = $this->model::with('roles', 'schedule.scheduleType');
            $data = $query->whereHas('roles', function ($query) {
                $query->where('name', 'master')
                    ->orWhere('name', 'vice-master')
                    ->orWhere('name', 'cpanel-admin')
                    ->orWhere('name', 'employee');
            })->where('is_active', 1)->get();


        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function getRelationshipStatus($id){
        $user = Auth::user();
        $friend = Friends::where(function($query) use ($id,$user){
            $query->where('user_id',$id);
            $query->where('user_id2',$user->id);
        })->orWhere(function($query) use ($id,$user){
            $query->where('user_id2',$id);
            $query->where('user_id',$user->id);
        })->first();

        $data['rel_status'] = null;
        $data['initiator'] = false;

        if($friend)
        {
            $data['rel_status'] = $friend->status;

            $initiator = Friends::
                where('user_id',$user->id)
                ->where('user_id2',$id)
                ->count();

            if($initiator)
            {
                $data['initiator'] = true;
            }
        }

        return $data;
    }



    public function show($id)
    {
        if ($include = request()->get('include')) {
            $authUser = Auth::user();

            $data = DB::select(
                DB::raw('
                    SELECT u.id
                         , u.email
                         , u.first_name
                         , u.last_name
                         , u.nick_name
                         , u.gender
                         -- , f.id AS friend_id
                         -- , f.user_id AS friend_user_id
                         -- , f.status AS friend_status
                         , i.name AS avatar
                    FROM wycgroup.users AS u
                    -- LEFT JOIN shopping.friends AS f
                    -- ON u.id = (
                    --     CASE
                    --         WHEN f.user_id = :uid AND f.user_id2 = u.id THEN f.user_id2
                    --         WHEN f.user_id2 = :uid AND f.user_id = u.id THEN f.user_id
                    --     END
                    -- )
                    LEFT JOIN wycgroup.images AS i
                    ON u.id = i.imageable_id
                        AND i.imageable_type = "App\Models\User"
                        AND i.type = "avatar"
                    WHERE u.id = :uid2
                '),
                // ['uid' => $authUser->id, 'uid2' => $id]
                ['uid2' => $id]
            )[0];
        } else {
            $data = $this->model->findOrFail($id);
        }

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function update(Request $request, $id){
        //get store by id
        $upUser = $this->model->withTrashed()->findOrFail($id);

        if($upUser){
            if($request->get('mode')=='bday') 
            {
                $upUser->birth_date = $request->get('req');
                $upUser->save();
                return $this->respondUpdated($upUser);
            }
            if($request->get('mode')=='address') 
            {                
                $add = Address::where('addressable_id','=',$id)->first();
                $add->address = $request->get('req');
                $add->save();
                return $this->respondUpdated($upUser);
            }
        }
    }

    public function getSupports(){
        $supp = $this->model->whereHas('roles', function ($query) {
            $query->where('name', 'support');
        })->limit(3)->get();

        return $this->setStatusCode(200)
            ->respond($supp);
    }

    public function getAllAvailableSupports($chatRoomId) {
        $chatRoomUsers = \DB::table('chat_room_user')->where('chat_room_id', $chatRoomId)->pluck('user_id');

        $supports = $this->model->whereHas('roles', function($query) {
            $query->where('name', 'support');
        })->whereNotIn('id', $chatRoomUsers)->get();

        return $this->setStatusCode(200)
            ->respond($supports);
    }
        
    public function getCurrentSupports($chatRoomId) {
        $chatRoomUsers = \DB::table('chat_room_user')->where('chat_room_id', $chatRoomId)->pluck('user_id');

        $supports = $this->model->whereHas('roles', function($query) {
            $query->where('name', 'support');
        })->whereIn('id', $chatRoomUsers)->get();

        return $this->setStatusCode(200)
            ->respond($supports);
    }

    //////////////////
    //user functions//
    //////////////////

    public function products(Request $request, User $user)
    {
        return $this->setStatusCode(200)
            ->respond($user->products);
    }

    public function activeProducts(Request $request, User $user)
    {
        return $this->setStatusCode(200)
            ->respond($user->products->where('is_active', "1"));
    }

    public function stores(Request $request, User $user)
    {
        return $this->setStatusCode(200)
            ->respond($user->stores);
    }

    public function activeStores(Request $request, User $user)
    {
        return $this->setStatusCode(200)
        ->respond($user->stores->where('is_active', "1"));
    }


    public function sales(Request $request, User $user)
    {
        return $this->setStatusCode(200)
        ->respond($user->stores->where('is_active', "1"));
    }

    public function documents(Request $request, User $user)
    {
        $type = $request->get('doctype');
        if($type)
        {
            $data = $user->documents()
                ->where('type', $type)
                ->latest()
                ->get();

        }else
        {

            $data = $user->documents()
                ->latest()
                ->get()
                ->unique('type')
                ->values()
                ;
        }

        return $this->setStatusCode(200)
            ->respond($data);
    }

    //PURCHASE ORDERS
    public function orders(Request $request, User $user)
    {
        return $this->setStatusCode(200)
            ->respond($user->orders()->orderBy('id','desc')->get());
    }

    public function completeOrders(Request $request, User $user)
    {
        return $this->setStatusCode(200)
            ->respond($user->orders()->where('status', 'Complete')->get());
    }
    

    public function roles(Request $request, User $user)
    {
        return $this->setStatusCode(200)
            ->respond($user->roles);
    }

    public function permissions(Request $request, User $user)
    {
        return $this->setStatusCode(200)
            ->respond($user->permissions);
    }

    public function updateSettings(Request $request)
    {
        $user = Auth::user();

        if (! $user->settings) {
            return $this->respondCreated(
                $user->settings()->create(['settings' => $request->all()])
            );
        }

        $data = $user->settings;
        $data->settings = array_merge($data->settings, $request->all());
        $data->save();

        return $this->respondUpdated($data);
    }

}