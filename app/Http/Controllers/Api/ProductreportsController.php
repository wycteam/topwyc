<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Shopping\ProductReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;


class ProductReportsController extends ApiController
{
    protected $model;

    public function __construct(ProductReport $model)
    {
        $this->model = $model;
    }


    public function index()
    {
        $data = $this->model->all();

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function store(Request $request)
    {

        $data = $this->model->create(array_merge($request->all()));

        return $this->respondCreated($data);
    }
}
