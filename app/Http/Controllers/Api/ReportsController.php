<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Visa\Group;
use App\Models\Visa\GroupMember;
use App\Models\Visa\Report;
use App\Models\Visa\Category;
use App\Models\Visa\ClientService;
use App\Models\Visa\Service;
use App\Models\Visa\ServiceDocuments;
use App\Models\Visa\Task;
use App\Models\Visa\Discount;
use App\Models\Visa\Package;
use App\Models\Visa\ClientTransactions;
use App\Models\Visa\DocumentLogs;
use App\Http\Controllers\Cpanel\TasksController;
use App\Http\Controllers\Cpanel\DocumentLogsController;
use App\Models\Visa\ClientServiceDocument;

use Carbon\Carbon;
use App\Models\Visa\DateLogs as DateLogs;
use App\Models\Visa\VisaType as VisaType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use App\Classes\Common;

class ReportsController extends ApiController
{

	public $_actionId_ = null;
	public $_catName_ = null;
	public $_releasingDate_ = null;

	private function _addStep($serviceCategory, $request, $i, $j) {
		$_clientService = (object)$request->clientServicesId[$j];

		if($serviceCategory == '9g') {
			$outcomeDocuments[0]['documents'] = $request->nineGDocument[$i];
			$outcomeDocuments[0]['client_service_id'] = $_clientService->id;
		} elseif($serviceCategory == 'downgrading') {
			$outcomeDocuments[0]['documents'] = $request->downgradingDocument[$i];
			$outcomeDocuments[0]['client_service_id'] = $_clientService->id;
		} else {
			$outcomeDocuments = $request->outcomeDocuments[$i];
		}

		foreach($outcomeDocuments as $oD) {
			if($_clientService->id == $oD['client_service_id'] && count($oD['documents']) > 0) {
				$clientService = ClientService::findOrfail($oD['client_service_id']);

				$documentLog = DocumentLogs::where('client_service_id', $clientService->id)
								->orderBy('id', 'desc')->first();

                $documentsOnHand = ($documentLog->documents_on_hand) 
					? explode(',', $documentLog->documents_on_hand)
					: [];

				$documentsOnHand = implode(',', array_unique(array_merge($oD['documents'], $documentsOnHand)));

				$type = 'Received';

				$documents = implode(',', $oD['documents']);

				$govtAgency = Category::findOrfail($request->categoryId[$i])->name;
				$serviceName = Service::findOrfail($clientService->service_id)->detail;
				$detail = '<strong>Received document/s from ' . $govtAgency . ' for service ' . $serviceName . ':</strong>';
				$detailCn = '<strong>Received document/s from ' . $govtAgency . ' for service ' . $serviceName . ':</strong>';

				DocumentLogsController::store($clientService, $type, $documents, $detail, $detailCn, $documentsOnHand);
			}
		}
	}

	private function _lastAddStep($serviceCategory, $request, $i, $j, $additionalDocsToAdd) {
		$_clientService = (object)$request->clientServicesId[$j];

		$outcomeDocuments = $request->outcomeDocuments[$i];
		foreach($outcomeDocuments as $oD) {
			if($_clientService->id == $oD['client_service_id'] && count($oD['documents']) > 0) {
				$clientService = ClientService::findOrfail($oD['client_service_id']);

                $documentsOnHand = implode(',', array_unique(array_merge($oD['documents'], $additionalDocsToAdd)));

				$type = 'Received';

				$documents = implode(',', $oD['documents']);

				$govtAgency = Category::findOrfail($request->categoryId[$i])->name;
				$serviceName = Service::findOrfail($clientService->service_id)->detail;
				$detail = '<strong>Received document/s from ' . $govtAgency . ' for service ' . $serviceName . ':</strong>';
				$detailCn = '<strong>Received document/s from ' . $govtAgency . ' for service ' . $serviceName . ':</strong>';

				DocumentLogsController::store($clientService, $type, $documents, $detail, $detailCn, $documentsOnHand);
			}
		}
	}

	private function _singleStep($serviceCategory, $request, $i, $j, $reportString) {
		$_clientService = (object)$request->clientServicesId[$j];

		$outcomeDocuments = $request->outcomeDocuments[$i];
		foreach($outcomeDocuments as $oD) {
			if($_clientService->id == $oD['client_service_id'] && count($oD['documents']) > 0) {
				$clientService = ClientService::findOrfail($oD['client_service_id']);

				$count = Report::has('service.documentLogs')
                    ->where('service_id', $oD['client_service_id'])
                    ->where('detail', 'LIKE', '%'.$reportString.'%')
                    ->count();

				$documentLog = DocumentLogs::where('client_service_id', $clientService->id)
								->orderBy('id', 'desc')->first();

                $documentsOnHand = ($documentLog->documents_on_hand) 
					? explode(',', $documentLog->documents_on_hand)
					: [];

				if( $clientService->service_id == 427 ) { // Travel Record with Amendment - Package
					$documentsOnHand = implode(',', array_unique(array_merge($oD['documents'], ($count <= 1) ? [] : $documentsOnHand)));
				} else {
					$documentsOnHand = implode(',', array_unique(array_merge($oD['documents'], ($count == 0) ? [] : $documentsOnHand)));
				}

				$type = 'Received';

				$documents = implode(',', $oD['documents']);

				$govtAgency = Category::findOrfail($request->categoryId[$i])->name;
				$serviceName = Service::findOrfail($clientService->service_id)->detail;
				$detail = '<strong>Received document/s from ' . $govtAgency . ' for service ' . $serviceName . ':</strong>';
				$detailCn = '<strong>Received document/s from ' . $govtAgency . ' for service ' . $serviceName . ':</strong>';

				DocumentLogsController::store($clientService, $type, $documents, $detail, $detailCn, $documentsOnHand);
			}
		}
	}

	private function hasDocumentLogs($clientServiceId) {
		return ClientService::has('documentLogs')->where('id', $clientServiceId)->count() > 0;
	}

	private function getServiceDocuments($outcomeDocuments) {
		$documents = '';

		foreach($outcomeDocuments['documents'] as $document) {
			$serviceDocument = ServiceDocuments::find($document);

			if($serviceDocument) {
				$documents .= $serviceDocument->title . ',';
			}
		}

		if( strlen($documents) > 0 ) {
			$documents = '(' . substr($documents, 0, -1) . ')';
		}

		return $documents;
	}

	public function saveQuickReport(Request $request) {
		// // START: Document Logs
		// for($i=0; $i<count($request->distinctClientServices); $i++) {
		// 	for($j=0; $j<count($request->clientServicesId); $j++) {
		// 		if( $this->hasDocumentLogs($request->clientServicesId[$j]['id']) != 0 && $request->distinctClientServices[$i] == $request->clientServicesId[$j]['detail'] ) {

		// 			// START: 9A
		// 			if( $request->clientServicesId[$j]['parent_id'] == 63 ) {
		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$this->_singleStep('9a', $request, $i, $j, 'Released from Immigration');
		// 				}
		// 			}
		// 			// END: 9A

		// 			// START: 9G
		// 			if( $request->clientServicesId[$j]['parent_id'] == 70 ) {
		// 				// Released // DOLE
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 19 ) {
		// 					$clientService = (object)$request->clientServicesId[$j];

		// 					if($clientService->service_id == 325 || $clientService->service_id == 326 || $clientService->service_id == 327 || $clientService->service_id == 328 || $clientService->service_id == 97 || $clientService->service_id == 98 || $clientService->service_id == 401 || $clientService->service_id == 417 || $clientService->service_id == 400) {
		// 						$this->_addStep('9g', $request, $i, $j);
		// 					} elseif($clientService->service_id == 429 || $clientService->service_id == 430 || $clientService->service_id == 431) {
		// 						$this->_singleStep('9g', $request, $i, $j, 'Released from DOLE');
		// 					}
							
		// 				}

		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$clientService = (object)$request->clientServicesId[$j];
							
		// 					if($clientService->service_id == 432 ||$clientService->service_id == 433 || $clientService->service_id == 434 || $clientService->service_id == 435 || $clientService->service_id == 436 || $clientService->service_id == 437) {
		// 						$this->_singleStep('9g', $request, $i, $j, 'Released from Immigration');
		// 					} else {
		// 						$additionalDocsToAdd = [];

		// 						$clientService = (object)$request->clientServicesId[$j];

		// 						$documentLog = DocumentLogs::where('client_service_id', $clientService->id)
		// 							->orderBy('id', 'desc')->first();

		// 						if($documentLog) {
		// 							$documentsOnHand = ($documentLog->documents_on_hand) 
		// 								? explode(',', $documentLog->documents_on_hand)
		// 								: [];

		// 							$count = Report::has('service.documentLogs')
		// 			                    ->where('service_id', $clientService->id)
		// 			                    ->where('detail', 'LIKE', '%Released from Immigration%')
		// 			                    ->count();

		// 			                if($count == 0) {
		// 			                	if(in_array(10, $documentsOnHand)) {
		// 			                		$documentsOnHand = [10];
		// 			                	} else {
		// 			                		$documentsOnHand = [];
		// 			                	}
		// 			                }
									
		// 							// Outcome Documents:
		// 								// Passport Original = 1
		// 								// Visa Page with Latest Arrival and Extension Original = 117
		// 								// Working Visa Order Original = 214
		// 								// ACR I-Card Original = 7
		// 							// Alien Employment Permit (AEP) Card Original = 10
		// 							$docsArray = [1, 117, 214, 7, 10];
		// 							$additionalDocsToAdd = [];
		// 							foreach($docsArray as $d) {
		// 								if(in_array($d, $documentsOnHand)) {
		// 									$additionalDocsToAdd[] = $d;
		// 								}
		// 							}
		// 						}

		// 						$this->_lastAddStep('9g', $request, $i, $j, $additionalDocsToAdd);
		// 					}
		// 				}
		// 			}
		// 			// END: 9G

		// 			// START: ECC
		// 			if( $request->clientServicesId[$j]['parent_id'] == 88 ) {
		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$this->_singleStep('ecc', $request, $i, $j, 'Released from Immigration');
		// 				}
		// 			}
		// 			// END: ECC

		// 			// START: Bureau of Immigration Certificates
		// 			if( $request->clientServicesId[$j]['parent_id'] == 101 ) {
		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$this->_singleStep('bureau-of-immigration-certificates', $request, $i, $j, 'Released from Immigration');
		// 				}
		// 			}
		// 			// END: Bureau of Immigration Certificates

		// 			// START: Downgrading
		// 			if( $request->clientServicesId[$j]['parent_id'] == 114 ) {
		// 				$clientService = (object)$request->clientServicesId[$j];

		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					// 9G Working Visa Downgrading - Complete Documents = 121
		// 					// 9G Working Visa Downgrading - Rush = 394
		// 					// 9G Working Visa Downgrading - Incomplete Documents- Rush = 423
		// 					if($clientService->service_id == 121 || $clientService->service_id == 394 || $clientService->service_id == 423) {
		// 						$this->_singleStep('downgrading', $request, $i, $j, 'Released from Immigration');
		// 					}

		// 					// CEZA Working Visa Downgrading = 307
		// 					// CEZA Working Visa Downgrading - Incomplete Documents- Rush = 415
		// 					// CEZA Working Visa Downgrading - Complete Documents- Rush = 416
		// 					elseif($clientService->service_id == 307 || $clientService->service_id == 415 || $clientService->service_id == 416) {
		// 						$additionalDocsToAdd = [];

		// 						$documentLog = DocumentLogs::where('client_service_id', $clientService->id)
		// 							->orderBy('id', 'desc')->first();

		// 						if($documentLog) {
		// 							$documentsOnHand = ($documentLog->documents_on_hand) 
		// 								? explode(',', $documentLog->documents_on_hand)
		// 								: [];

		// 							$count = Report::has('service.documentLogs')
		// 			                    ->where('service_id', $clientService->id)
		// 			                    ->where('detail', 'LIKE', '%Released from Immigration%')
		// 			                    ->count();

		// 			                if($count == 0) {
		// 			                	if(in_array(75, $documentsOnHand)) {
		// 			                		$documentsOnHand = [75];
		// 			                	} else {
		// 			                		$documentsOnHand = [];
		// 			                	}
		// 			                }

		// 							// Outcome Documents:
		// 								// Passport Original = 1
		// 								// Visa Page with Latest Arrival and Extension Original = 117
		// 								// CEZA Working Visa = 215
		// 								// Downgrading Order with latest valid stay = 121
		// 							// CEZA Cancellation Order Original = 75
		// 							$docsArray = [1, 117, 215, 121, 75];
		// 							$additionalDocsToAdd = [];
		// 							foreach($docsArray as $d) {
		// 								if(in_array($d, $documentsOnHand)) {
		// 									$additionalDocsToAdd[] = $d;
		// 								}
		// 							}
		// 						}

		// 						$this->_lastAddStep('downgrading', $request, $i, $j, $additionalDocsToAdd);
		// 					}

		// 					// Cancellation of Working Visa = 403
		// 					elseif($clientService->service_id == 403) {
		// 						$additionalDocsToAdd = [];

		// 						$documentLog = DocumentLogs::where('client_service_id', $clientService->id)
		// 							->orderBy('id', 'desc')->first();

		// 						if($documentLog) {
		// 							$documentsOnHand = ($documentLog->documents_on_hand) 
		// 								? explode(',', $documentLog->documents_on_hand)
		// 								: [];

		// 							// Cancellation Letter from DOLE = 122
		// 							$docsArray = [122];
		// 							$additionalDocsToAdd = [];
		// 							foreach($docsArray as $d) {
		// 								if(in_array($d, $documentsOnHand)) {
		// 									$additionalDocsToAdd[] = $d;
		// 								}
		// 							}
		// 						}

		// 						$this->_lastAddStep('downgrading', $request, $i, $j, $additionalDocsToAdd);
		// 					}
		// 				}

		// 				// Released // CEZA
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 20 ) {
		// 					$this->_addStep('downgrading', $request, $i, $j);
		// 				}

		// 				// Released // DOLE
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 19 ) {
		// 					$this->_addStep('downgrading', $request, $i, $j);
		// 				}
		// 			}
		// 			// END: Downgrading

		// 			// START: Driver's License
		// 			if( $request->clientServicesId[$j]['parent_id'] == 123 ) {
		// 				// Released // LTO
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 31 ) {
		// 					$this->_singleStep('drivers-license', $request, $i, $j, 'Released from LTO');
		// 				}
		// 			}
		// 			// END: Driver's License

		// 			// START: Blacklist
		// 			if( $request->clientServicesId[$j]['parent_id'] == 126 ) {
		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$this->_singleStep('blacklist', $request, $i, $j, 'Released from Immigration');
		// 				}
		// 			}
		// 			// END: Blacklist

		// 			// START: NSO Certificate
		// 			if( $request->clientServicesId[$j]['parent_id'] == 133 ) {
		// 				// Released // NSO
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 25 ) {
		// 					$this->_singleStep('nso-certificate', $request, $i, $j, 'Released from NSO');
		// 				}

		// 				// Released // Cityhall
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 26 ) {
		// 					$this->_singleStep('nso-certificate', $request, $i, $j, 'Released from Cityhall');
		// 				}
		// 			}
		// 			// END: NSO Certificate

		// 			// START: ACR I-CARD
		// 			if( $request->clientServicesId[$j]['parent_id'] == 136 ) {
		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$this->_singleStep('acr-i-card', $request, $i, $j, 'Released from Immigration');
		// 				}
		// 			}
		// 			// END: ACR I-CARD

		// 			// START: Annual Report
		// 			if( $request->clientServicesId[$j]['parent_id'] == 167 ) {
		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$this->_singleStep('annual-report', $request, $i, $j, 'Released from Immigration');
		// 				}
		// 			}
		// 			// END: Annual Report

		// 			// START: Special Permits
		// 			if( $request->clientServicesId[$j]['parent_id'] == 141 ) {
		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$this->_singleStep('special-permits', $request, $i, $j, 'Released from Immigration');
		// 				}
		// 			}
		// 			// END: Special Permits

		// 			// START: Permanent Resident Visa (PRV- Proc)
		// 			if( $request->clientServicesId[$j]['parent_id'] == 147 ) {
		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$this->_singleStep('prv-proc', $request, $i, $j, 'Released from Immigration');
		// 				}
		// 			}
		// 			// END: Permanent Resident Visa (PRV- Proc)

		// 			// START: Temporary Resident Visa (TRV)
		// 			if( $request->clientServicesId[$j]['parent_id'] == 148 ) {
		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$this->_singleStep('trv', $request, $i, $j, 'Released from Immigration');
		// 				}
		// 			}
		// 			// END: Temporary Resident Visa (TRV)

		// 			// START: Department of Foreign Affairs (DFA)
		// 			if( $request->clientServicesId[$j]['parent_id'] == 158 ) {
		// 				// Released // DFA
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 29 ) {
		// 					$this->_singleStep('dfa', $request, $i, $j, 'Released from DFA');
		// 				}

		// 				// Released // Chinese Embassy
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 24 ) {
		// 					$this->_singleStep('dfa', $request, $i, $j, 'Released from Chinese Embassy');
		// 				}
		// 			}
		// 			// END: Department of Foreign Affairs (DFA)

		// 			// START: Other Services
		// 			if( $request->clientServicesId[$j]['parent_id'] == 160 ) {
		// 				// Released // NBI
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 32 ) {
		// 					$this->_singleStep('other-services', $request, $i, $j, 'Released from NBI');
		// 				}

		// 				// Released // DOLE
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 19 ) {
		// 					$this->_singleStep('other-services', $request, $i, $j, 'Released from DOLE');
		// 				}

		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$this->_singleStep('other-services', $request, $i, $j, 'Released from Immigration');
		// 				}

		// 				// Released
		// 				if( $request->actionId[$i] == 2) {
		// 					if($request->categoryId[$i] == 148) { // PAL
		// 						$this->_singleStep('other-services', $request, $i, $j, 'Released from PAL');
		// 					} elseif($request->categoryId[$i] == 149) { // Cebu Pacific
		// 						$this->_singleStep('other-services', $request, $i, $j, 'Released from Cebu Pacific');
		// 					} elseif($request->categoryId[$i] == 150) { // China Southern Airlines
		// 						$this->_singleStep('other-services', $request, $i, $j, 'Released from China Southern Airlines');
		// 					} elseif($request->categoryId[$i] == 151) { // China East Airlines
		// 						$this->_singleStep('other-services', $request, $i, $j, 'Released from China East Airlines');
		// 					}
		// 				}
		// 			}
		// 			// END: Other Services

		// 			// START: Re-Stamping of Visa
		// 			if( $request->clientServicesId[$j]['parent_id'] == 199 ) {
		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {
		// 					$this->_singleStep('re-stamping-of-visa', $request, $i, $j, 'Released from Immigration');
		// 				}
		// 			}
		// 			// END: Re-Stamping of Visa

		// 			// START: Special Retiree Resident Visa (SRRV) Conversion
		// 			if( $request->clientServicesId[$j]['parent_id'] == 217 ) {
		// 				// Released // PRA
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 23 ) {
		// 					$this->_singleStep('srrv', $request, $i, $j, 'Released from PRA');
		// 				}
		// 			}
		// 			// END: Special Retiree Resident Visa (SRRV) Conversion

		// 			// START: Bureau of Quarantine (BOQ)
		// 			if( $request->clientServicesId[$j]['parent_id'] == 222 ) {
		// 				// Released // BOQ
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 27 ) {							
		// 					$this->_singleStep('boq', $request, $i, $j, 'Released from BOQ');
		// 				}
		// 			}
		// 			// END: Bureau of Quarantine (BOQ)

		// 			// START: Company Registration
		// 			if( $request->clientServicesId[$j]['parent_id'] == 228 ) {
		// 				// Released // SEC/DTI
		// 				if( $request->actionId[$i] == 2 && ($request->categoryId[$i] == 22 || $request->categoryId[$i] == 30) ) {
		// 					$this->_addStep('company-registration', $request, $i, $j);
		// 				}

		// 				// Released // Barangay Hall
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 126 ) {
		// 					$this->_addStep('company-registration', $request, $i, $j);
		// 				}

		// 				// Released // City Hall
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 26 ) {
		// 					$this->_addStep('company-registration', $request, $i, $j);
		// 				}

		// 				// Released // BIR
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 125 ) {
		// 					$additionalDocsToAdd = [];

		// 					$clientService = (object)$request->clientServicesId[$j];

		// 					$documentLog = DocumentLogs::where('client_service_id', $clientService->id)
		// 						->orderBy('id', 'desc')->first();

		// 					if($documentLog) {
		// 						$documentsOnHand = ($documentLog->documents_on_hand) 
		// 							? explode(',', $documentLog->documents_on_hand)
		// 							: [];

		// 						// SEC Registration/DTI = 198
		// 						// Barangay Clearance for Business = 138
		// 						// Locational Clearance = 139
		// 						// Mayors Permit = 149
		// 						// BIR 2303 Certificate of Registration = 141
		// 						$docsArray = [198, 138, 139, 149, 141];
		// 						$additionalDocsToAdd = [];
		// 						foreach($docsArray as $d) {
		// 							if(in_array($d, $documentsOnHand)) {
		// 								$additionalDocsToAdd[] = $d;
		// 							}
		// 						}
		// 					}

		// 					$this->_lastAddStep('company-registration', $request, $i, $j, $additionalDocsToAdd);
		// 				}
		// 			}
		// 			// END: Company Registration

		// 			// START: Board of Investment (BOI)
		// 			if( $request->clientServicesId[$j]['parent_id'] == 230 ) {
		// 				// Released // Immigration
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 18 ) {							
		// 					$this->_singleStep('boi', $request, $i, $j, 'Released from Immigration');
		// 				}
		// 			}
		// 			// END: Board of Investment (BOI)

		// 			// START: Cagayan Economic Zone Authority(CEZA) Working Visa
		// 			if( $request->clientServicesId[$j]['parent_id'] == 238 ) {
		// 				// Released // CEZA
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 20 ) {
		// 					$this->_singleStep('ceza', $request, $i, $j, 'Released from CEZA');
		// 				}
		// 			}
		// 			// END: Cagayan Economic Zone Authority(CEZA) Working Visa

		// 			// START: Department of Justice (DOJ)
		// 			if( $request->clientServicesId[$j]['parent_id'] == 259 ) {
		// 				// Released // DOJ
		// 				if( $request->actionId[$i] == 2 && $request->categoryId[$i] == 21 ) {
		// 					$this->_singleStep('doj', $request, $i, $j, 'Released from DOJ');
		// 				}
		// 			}
		// 			// END: Department of Justice (DOJ)

		// 		}
		// 	}
		// }
		// END: Document Logs

		// ******************** Document Tracker ******************** //
			for($i=0; $i<count($request->distinctClientServices); $i++) {
				for($j=0; $j<count($request->clientServicesId); $j++) {
					if( $this->hasDocumentLogs($request->clientServicesId[$j]['id']) 
						&& 
						$request->distinctClientServices[$i] == $request->clientServicesId[$j]['detail'] 
					) {

						$clientService = (object)$request->clientServicesId[$j];

						$data = $request->outcomeDocuments[$i];
						foreach($data as $d) {
							if($clientService->status != 'pending' && $clientService->id == $d['client_service_id'] && count($d['documents']) > 0) {
								$govtAgency = Category::findOrfail($request->categoryId[$i])->name;

								if($clientService->report_docs == null) {
									$_clientId = $clientService->client_id;

									ClientServiceDocument::whereHas('clientService', function($query) use($_clientId) {
					                    $query->where('client_id', $_clientId);
					                })
					                ->whereIn('document_id', ($clientService->rcv_docs) ? explode(',', $clientService->rcv_docs) : [])
					                ->delete();

									$dl_detail = 'Submitted document/s to ' . $govtAgency . ':';

									DocumentLogs::create([
				                        'client_service_id' => $clientService->id,
				                        'documents' => $clientService->rcv_docs,
				                        'documents_on_hand' => null,
				                        'processor_id' => Auth::user()->id,
				                        'type' => 'Submitted',
				                        'detail' => $dl_detail,
				                        'detail_cn' => $dl_detail,
				                        'log_date' => Carbon::now()->format('Y-m-d')
				                    ]);

				                    $dl_detail = 'Submitted document/s to ' . $govtAgency . ' for service <strong style="color:orange;">'.$clientService->detail.'</strong>:';

				                    DocumentLogsController::removeDocuments(
				                    	$clientService->client_id,
				                    	explode(',', $clientService->rcv_docs),
				                    	$clientService->id,
				                    	'report',
				                    	$dl_detail
				                    );
								}

								$cs = ClientService::findOrFail($clientService->id);
								$reportDocs = ($cs->report_docs) ? explode(',', $cs->report_docs) : [];
								$reportDocs = array_unique(array_merge($reportDocs, $d['documents']));
				                $cs->update([
				                    'report_docs' => implode(',', $reportDocs),
				                    'status' => (DocumentLogsController::isComplete($cs->service_id, implode(',', $reportDocs))) 
				                    	? 'complete' 
				                    	: $cs->status
				                ]);

				                DocumentLogsController::updatePackage($cs->tracking);

				                foreach($d['documents'] as $_d) {
				                	ClientServiceDocument::create([
				                		'client_service_id' => $clientService->id,
					                    'document_id' => $_d,
					                    'created_at' => Carbon::now()
				                	]);
				                }

								$dl_detail = 'Received document/s from ' . $govtAgency . ':';

				                DocumentLogs::create([
				                    'client_service_id' => $clientService->id,
				                    'documents' => implode(',', $d['documents']),
				                    'documents_on_hand' => implode(',', $reportDocs),
				                    'processor_id' => Auth::user()->id,
				                    'detail' => $dl_detail,
				                    'detail_cn' => $dl_detail,
				                    'log_date' => Carbon::now()->format('Y-m-d')
				                ]);

				                $dl_detail = 'Received document/s from ' . $govtAgency . ' for service <strong style="color:orange;">'.$clientService->detail.'</strong>:';

				                DocumentLogsController::share([$clientService->client_id], 'report', $dl_detail);
							}
						}

					}
				}
			}
		// ******************** End of Document Tracker ******************** //

		// Task Generator
		$taskActionArray = [];
		$taskCategoryArray = [];
		$__date1Array = [];
		$__date2Array = [];
		$__date3Array = [];
		$__dateTimeArray = [];
		$__exp_date=[];
		$__estimate_exp_date=[];
		$__user_id=[];
		$__tmpHolder=[];

		//\Log::info($request->all());

		foreach ($request->exp_date as $exp_date ) {
			  foreach ( $exp_date as $key => $value ) {
					array_push($__exp_date,Carbon::parse($value)->format('Y-m-d'));
			  }
		}
	
		foreach ($request->estimated_date as $estimated_date ) {
			if($estimated_date != null && count($estimated_date)!=0){
				foreach ($estimated_date as $estimated_date2 ) {
				   foreach ( $estimated_date2 as $i => $row ) {
						 if($i=='_value'){
							 	array_push($__estimate_exp_date,$row);
						 }
				  }
				}
			}
		}

		foreach ($request->user_id as $user_id ) {
			if($user_id != null && count($user_id)!=0){
				foreach ($user_id as $user_id2 ) {
				   foreach ( $user_id2 as $i => $row ) {
						 if($i=='_value'){
							 	array_push($__user_id,$row);
						 }
				  }
				}
			}
		}
		//\Log::info($request->user_id);
		$counter = 0;

		foreach($request->clientServicesId as $clientServicesId){
			if($clientServicesId['parent_id']==63){
				$user_id = '';
				$exp_date = '';
				$est_exp = '';
				$found = false;
				for($i=0;$i<count($__user_id) && !$found;$i++){
					//\Log::info($clientServicesId['client_id'].'----'.$__user_id[$i]);
					if($clientServicesId['client_id']==$__user_id[$i]){
						$user_id = $__user_id[$i];
						$exp_date = $__exp_date[$i];
						$est_exp = $__estimate_exp_date[$i];
						$__exp_date[$i] = '';
						$__user_id[$i]='';
						$__estimate_exp_date[$i] = '';
						$found =true;
					}
				}
				$client = (object)array("srv_id"=>$clientServicesId['id'],"client_id"=>$clientServicesId['client_id'],"client_id2"=>$user_id,"exp_date"=>$exp_date,'estimated_date'=>$est_exp);
				array_push($__tmpHolder,$client);
				$counter++;
			}
		}

		$c = collect($__tmpHolder);

		$sorted = $c->sortBy('srv_id');

		$__tmpHolder = $sorted->values()->all();

		//\Log::info($__tmpHolder);
		
		foreach($__tmpHolder as $__tmpHolder1){
			$dt = new DateLogs;
			$dt->client_id = $__tmpHolder1->client_id;
			$dt->exp_date =  $__tmpHolder1->exp_date;
			$dt->service_id =  $__tmpHolder1->srv_id;
			$dt->estimated_date = $__tmpHolder1->estimated_date;
			$dt->save();
			if($__tmpHolder1->exp_date == ''){
				$__tmpHolder1->exp_date = null;
			}
			User::where('id',$__tmpHolder1->client_id)->update(['exp_date' => $__tmpHolder1->exp_date]);
		}

		$distinctClientServices = $request->distinctClientServices;

		$clientsId = $request->clientsId;
		$clientServicesId = $request->clientServicesId;
		$trackings = $request->trackings;

		if($request->groupReport) {
			$groupId = $request->groupId;
			$groupLeaderId = Group::find($groupId)->leader;
		} else {
			$groupId = null;
			$groupLeaderId = null;
		}

		$categoryNameArray = [];
		$categoryNameChinese = [];
		for($i=0; $i<count($request->categoryId); $i++) {
			if($request->categoryId[$i] == 0){
				$categoryNameArray[] = $request->categoryName[$i];
				$categoryNameChinese[] = $request->categoryName[$i];
			}
			elseif(!is_null($request->categoryId[$i])){
				$categoryNameArray[] = Category::find($request->categoryId[$i])->name;
				$getcat = Category::find($request->categoryId[$i]);
				$categoryNameChinese[] = ($getcat->name_cn !=null ? $getcat->name_cn : $getcat->name);
			}
			else{
				$categoryNameArray[] = null;
				$categoryNameChinese[] = null;
			}
		}

		$date1Array = [];
		for($i=0; $i<count($request->date1); $i++) { $date1Array[] = (!is_null($request->date1[$i])) ? $request->date1[$i] : null; }

		$date2Array = [];
		for($i=0; $i<count($request->date2); $i++) { $date2Array[] = (!is_null($request->date2[$i])) ? $request->date2[$i] : null; }

		$date3Array = [];
		for($i=0; $i<count($request->date3); $i++) { $date3Array[] = (!is_null($request->date3[$i])) ? $request->date3[$i] : null; }

		$dateTimeArray = [];
		if(count($request->dateTimeArray) > 0) {
			for($i=0; $i<count($request->dateTimeArray); $i++) {
				$str = '';
				for($j=0; $j<count($request->dateTimeArray[$i]); $j++) {
					$str .= $request->dateTimeArray[$i][$j] . ' and ';
				}
				$str = substr($str, 0, -5);

				$dateTimeArray[] = $str;
			}
		}

		$extraDetailsArray = [];
		$extraDetailsArrayChinese = [];
		if(count($request->extraDetails) > 0) {
			for($i=0; $i<count($request->extraDetails); $i++) {
				$str = '';
				$str_cn = '';
				for($j=0; $j<count($request->extraDetails[$i]); $j++) {
					$str .= ($j+1) . '. ' . $request->extraDetails[$i][$j] . ' ';
					$doc_cn = ServiceDocuments::where('title',$request->extraDetails[$i][$j])->first();
					if($doc_cn){
						$str_cn .= ($j+1) . '. ' . $doc_cn->title_cn . ' ';
					}
					elseif($request->extraDetails[$i][$j]=='Additional cost maybe charged.'){
						$str_cn .= ($j+1) . '. 可能产生附加的费用 ';
					}
					elseif($request->extraDetails[$i][$j]=='Application maybe forfeited and has to re-apply.'){
						$str_cn .= ($j+1) . '. 该次申请作废 ';
					}
					elseif($request->extraDetails[$i][$j]=='Initial deposit maybe forfeited.'){
						$str_cn .= ($j+1) . '. 预付款作废 ';
					}
					else{
						$str_cn .= ($j+1) . '. ' . $request->extraDetails[$i][$j] . ' ';
					}
				}

				$extraDetailsArray[] = $str;
				$extraDetailsArrayChinese[] = $str_cn;
			}
		}

		// For Group Members
		for($i=0; $i<count($clientsId); $i++) {
			for($j=0; $j<count($clientServicesId); $j++) {

				for($k=0; $k<count($request->distinctClientServices); $k++) {
					if($request->distinctClientServices[$k] == $clientServicesId[$j]['detail']) {
						$getReport = $this->getReportDetail(
							null,
							$request->actionId[$k],
							$request->categoryId[$k],
							$categoryNameArray[$k],
							$date1Array[$k],
							$date2Array[$k],
							$date3Array[$k],
							$dateTimeArray[$k],
							$extraDetailsArray[$k],
							$categoryNameChinese[$k],
							$extraDetailsArrayChinese[$k]
						);
						$detail = $getReport[0];
						foreach($request->outcomeDocuments[$k] as $_oD) {
							if( $_oD['client_service_id'] == $clientServicesId[$j]['id']) {
								$detail = $detail . ' ' . $this->getServiceDocuments($_oD);
							}
						}
						$detail_cn = $getReport[1];

						//if released to immigration for 9A Extensions, ECC & Annual Report, auto mark as complete
						$this->_catName_ = $categoryNameArray[$k];
						$this->_actionId_ = $request->actionId[$k];

						// Task Generator
						$taskActionArray[] = $request->actionId[$k];
						$taskCategoryArray[] = $request->categoryId[$k];
						$__date1Array[] = $date1Array[$k];
						$__date2Array[] = $date2Array[$k];
						$__date3Array[] = $date3Array[$k];
						$__dateTimeArray[] = explode('and', $dateTimeArray[$k]);
					}
				}


				if($clientServicesId[$j]['client_id'] == $clientsId[$i]) {					
					Report::create([
						'client_id' => $clientsId[$i],
						'group_id' => ($clientServicesId[$j]['group_id'] != 0) ? $clientServicesId[$j]['group_id'] : null,
						'service_id' => $clientServicesId[$j]['id'],
						'detail' => $detail,
						'user_id' => Auth::user()->id,
						'tracking' => $clientServicesId[$j]['tracking'],
						'log_date' => Carbon::now()->format('Y-m-d H:i:s a'),
						'is_read' => 0
					]);

					$clientServiceId = $clientServicesId[$j]['id'];

					//if released to immigration for 9A Extensions, ECC & Annual Report, auto mark as complete
					if( !$this->hasDocumentLogs($clientServiceId) ) {
						$cService = ClientService::where('id', $clientServiceId)
							->where(function($query) {
				                return $query->where('detail', 'LIKE', '%9A%')
				                    ->orWhere('detail', 'LIKE', '%Annual Report%')
				                    ->orWhere('detail', 'LIKE', '%ECC%');
					        })
							->first();
						if($cService){
							if($this->_actionId_ == 2 && $this->_catName_ == 'Immigration') { // Released to Immigration
								if($cService->status != 'complete'){
									$oldstatus = $cService->status;
									$group_id = $cService->group_id;
									$cService->status = 'complete';
                					$cService->completed_at = Carbon::now();
									$cService->save();
									if($group_id == 0){
										$this->clientEditService($clientServiceId, $oldstatus);
									}
									else if($group_id > 0){
										$this->groupEditService($clientServiceId, $group_id);
									}
								}
							}
						}
					}
        			//\Log::info($this->checkIfGroupLeader($clientServicesId[$j]['group_id'], $clientServicesId[$j]['client_id']));

					if( !$this->checkIfGroupLeader($clientServicesId[$j]['group_id'], $clientServicesId[$j]['client_id']) ) {
						$this->sendPushNotif($clientServicesId[$j]['id'],$detail,null,null,$detail_cn);
					}
				}

			}
		}

		// For Group Leader
		$groupIdArrayDone = [];
		$serviceIdArrayDone = [];
		for ( $i=0; $i<count($clientServicesId); $i++ ) {
        	//\Log::info($clientServicesId[$i]['group_id']);

			if ( $clientServicesId[$i]['group_id'] != 0 ) {
				$done = false;
				for ( $k=0; $k<count($groupIdArrayDone); $k++ ) {
					if( $groupIdArrayDone[$k] == $clientServicesId[$i]['group_id'] && $serviceIdArrayDone[$k] == $clientServicesId[$i]['service_id'] ) {
						$done = true;
					}
				}

				if(!$done) {
					$groupIdArrayDone[] = $clientServicesId[$i]['group_id'];
					$serviceIdArrayDone[] = $clientServicesId[$i]['service_id'];

					$clientsIdArray = [];
					for ( $j=0; $j<count($clientServicesId); $j++ ) {
						if ( $clientServicesId[$i]['group_id'] == $clientServicesId[$j]['group_id'] && $clientServicesId[$i]['service_id'] == $clientServicesId[$j]['service_id'] ) {
							$clientsIdArray[] = $clientServicesId[$j]['client_id'];
						}
					}

					$groupLeaderId = Group::find($clientServicesId[$i]['group_id'])->leader;

					for( $k=0; $k<count($request->distinctClientServices); $k++ ) {
						if($request->distinctClientServices[$k] == $clientServicesId[$i]['detail']) {
							$getReport = $this->getReportDetail(
								$this->getAllClientNames($clientsIdArray, $groupLeaderId),
								$request->actionId[$k],
								$request->categoryId[$k],
								$categoryNameArray[$k],
								$date1Array[$k],
								$date2Array[$k],
								$date3Array[$k],
								$dateTimeArray[$k],
								$extraDetailsArray[$k],
								$categoryNameChinese[$k],
								$extraDetailsArrayChinese[$k]
							);
							$detail = $getReport[0];
							$detail_cn = $getReport[1];
						}
					}

					$this->sendPushNotif($clientServicesId[$i]['id'], $detail, $groupLeaderId,$this->getAllClientNames($clientsIdArray, $groupLeaderId),$detail_cn);
				}
			}

		}

		// Task Generator
		$csArray = [];
		for ( $i=0; $i<count($request->clientServicesId); $i++ ) {
			$csArray[] = json_decode(json_encode($request->clientServicesId[$i]));
		}

		TasksController::save(
			$csArray,
			$taskActionArray,
			$taskCategoryArray,
			$__date1Array,
			$__date2Array,
			$__date3Array,
		 	$__dateTimeArray
		);

		return json_encode([
			'success' => true,
			'message' => 'Report successfully created.'
		]);
	}

	public function addDocs(Request $request) {

        $values=array('rcv_docs'=>$request->docs);
        ClientService::whereId($request->serviceid)->update($values);

		return json_encode([
			'success' => true,
			'message' => 'Docs successfully saved.'
		]);
	}

	//individual edit service //mark as complete only
	private function clientEditService($id, $oldstatus){
        $service = ClientService::findOrFail($id);

        $log = '';

        $translated = Service::where('id',$service->service_id)->first();
        $cnserv =$service->detail;
        if($translated){
            $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
        }

		$log = '&nbsp;<b>Service Status</b> from '.$oldstatus.' to '.$service->status;

           $title = "Status update for Service ".$service->detail;
           $body = 'Service status for '.$service->detail.' is now '.$service->status;
           $title_cn = "服务更新 ".$cnserv;
		   $st = '已改为完成';
           $body_cn = " 的服务状态 为".$cnserv." ".$st;
           $parent = $service->id;
           $parent_type = 2;

           	//include total cost
             $checkDisc = ClientTransactions::where("service_id",$service->id)->first();
             $gTotal = $service->cost + $service->charge + $service->tip;
             if($checkDisc){
                $gTotal -= $checkDisc->amount;
             }
             $body = $body." with total cost of Php".$gTotal;
             $body_cn = $body_cn." 以及总服务费 Php".$gTotal;

           $main = $body;
           $main_cn = $body_cn;

           Common::savePushNotif($service->client_id, $title, $body,'Service',$service->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

            Common::check_package_updates($service->tracking);
            $group = Package::where('tracking', $service->tracking)->first();
            if($log != '') :
                if(substr($service->tracking,0,1)!="G"){
                    $new_log = 'Updated Service &nbsp;'.$service->detail.'&nbsp;'.$log.'&nbsp; on Package '.$service->tracking.'.';
                    Common::saveActionLogs($service->id,$service->client_id,$new_log);
                }
            endif;
            $result = array('success' => true);

        //return json_encode($result);
    }

    public function groupEditService($id, $groupId) {
        $collection = ClientService::where('id', $id);
        $oldCollect = $collection;
        $clientServices = $collection->get();
        $getGroup = Group::findOrFail($groupId);

        $userId = Auth::user()->id;
        $cdetail = '';
        $colClients = [];
        $cls = '';

        foreach($clientServices as $clientService) {
            $updCollect = '';
            $forleadCollect ='';
            $forleadChinese ='';

            $srv = ClientService::findOrFail($clientService->id);
            $translated = Service::where('id',$srv->service_id)->first();
            $cnserv =$srv->detail;
            if($translated){
                $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
            }

            $updCollect = '';
            $updChinese = '';

			$status = "<b><h4>".$srv->status."</h4></b>";
            $status_cn = "<b><h4> 已完成  </h4></b>";
			$updCollect .= 'Service status is now '.$status;
			$updChinese .= '服务状态为 '.$status_cn;

			$new_log = 'Log: Updated Client Service '.$id.' - '.$srv->detail.'</br><b>Service status</b> is now complete';
            Common::saveActionLogs($id,$srv->client_id,$new_log,$groupId);

            $client = User::findOrFail($srv->client_id);

            if($updCollect!=''){
                if (!in_array($srv->client_id, $colClients)) {
                    $checkIfVice = [];
                    $checkIfVice[] = $srv->client_id;
                    $groupVice = GroupMember::where('group_id',$groupId)->where('leader',2)->whereIn('client_id',$checkIfVice)->count();
                    if($srv->client_id != $getGroup->leader || $groupVice < 1){
                        $title = "Update for Service ".$srv->detail;
                        $title_cn = "服务更新 ".$cnserv;
                        $body = "Update for Service ".$srv->detail.", ".$updCollect;
                        $body_cn = "服务更新 ".$cnserv.", ".$updChinese;
                        $main = "Update for Service ".$srv->detail.", ".$updCollect;
                        $main_cn = "服务更新 ".$cnserv.", ".$updChinese;
                        $parent = $srv->id;
                        $parent_type = 2;
                        Common::savePushNotif($srv->client_id, $title, $body,'Service',$srv->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
                        $cls = $cls.$client->first_name." ".$client->last_name.", ";
                    }
                    else{
                        $cls = $cls.$client->first_name." ".$client->last_name.", ";
                    }
                    $colClients[] = $srv->client_id;
                }
            }

            // Check package updates
                ClientService::where('group_id', $clientService->group_id)
                    ->where('client_id', $clientService->client_id)
                    ->where('service_id', $clientService->service_id)
                    ->where('tracking', $clientService->tracking)
                    ->select('tracking')
                    ->get()
                    ->map(function($d) {
                        Common::check_package_updates($d->tracking);
                    });
        }

            $cls = substr($cls, 0, -2);
            $nms = explode(", ", $cls);
            sort($nms);
            $clientNames = '';
            $crt = 1;
            foreach($nms as $n){
                $clientNames .=$crt.".".$n." ";
                $crt++;
            }
            if($updCollect!='' || $forleadCollect!=''){
                $title = "Update for Service ".$srv->detail;
                $title_cn = "服务更新 ".$cnserv;
                $body = "Update for ".$clientNames." Service ".$srv->detail;
                $body_cn = "更新了 ".$clientNames." 服务 ".$cnserv;
                $main = "Update for ".$clientNames." Service ".$srv->detail.", ".$updCollect.$forleadCollect;
                $main_cn = "更新了 ".$clientNames." 服务 ".$cnserv.", ".$updChinese.$forleadChinese;
                $parent_type = 1;
                $sDate = $srv->service_date;
                $parent = $clientNames;

                Common::savePushNotif($getGroup->leader, $title, $body,'GService',$sDate."--".$getGroup->tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$clientNames);

                $vices = GroupMember::where('group_id',$getGroup->id)->where('leader',2)->get();
                foreach($vices as $v){
                    $par = $clientNames;
                    $parent = $v->client_id."-".$par;
                    Common::savePushNotif($v->client_id, $title, $body,'GService',$sDate."--".$getGroup->tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$clientNames);
                }
            }
    }

	private function checkIfGroupLeader($groupId, $clientId) {
		$ifGroupLeader = false;
		if($groupId != 0) {
			$groupLeaderId = Group::find($groupId)->leader;

			if($clientId == $groupLeaderId) {
				$ifGroupLeader = true;
			}
			$clt[] = $clientId;
			$groupVice = GroupMember::where('group_id',$groupId)->where('leader',2)->whereIn('client_id',$clt)->count();
			if($groupVice > 0){
				$ifGroupLeader = true;
			}
		}

		return $ifGroupLeader;
	}

	private function getAllClientNames($clientsId, $groupLeaderId) {
    	$clients = User::whereIn('id', $clientsId)->select('first_name', 'last_name')->get();

		$names = '';
		foreach($clients as $client) {
			$names .= $client->first_name . ' ' . $client->last_name . ', ';
		}

		return substr($names, 0, -2);
    }

    private function getReportDetail($clientNames, $actionId, $categoryId, $categoryName, $date1, $date2, $date3, $dateTime, $extraDetails,$categoryNameChinese = null,$extraDetailsChinese = null) {
    	$detail_cn = '';
    	if($actionId == 1) {
    		$detail_cn = $date3;
    		if( ($categoryId==6 || $categoryId==7 || $categoryId==10 || $categoryId==15) && !is_null($date3) ) {
    			$detail = 'Filed at ' . $categoryName . ' on ' . $date1 . '. The scheduled appointment at ' . $categoryName . ' is on ' . $date3 . '. Failure to attend the appointment date may delay the application.';
    			$detail_cn = '以提交于 ' . $categoryNameChinese. ' 在 ' . $date1 .'. 安排的面试日期和时间为 '. $date3 .'. 未能参加预约日期可能会延迟申请.';
    		} elseif( $categoryId==1 && !is_null($dateTime) && $dateTime!='' && $dateTime!=false ) {
    			$detail = 'Filed at ' . $categoryName . ' on ' . $date1 . '. The scheduled hearing date is on ' . $dateTime . '. Failure to attend both hearing date will automatic result abandonment of application. The estimated releasing date is on ' . $date2 . '.';
    			$detail_cn = '以提交于 ' . $categoryNameChinese . ' 在 ' . $date1 . '. 安排的面试日期和时间为 ' . $dateTime . '. 在预定日期不出席面试将自动视为放弃该申请。 预计完成日期于 ' . $date2 . '.';
    		} elseif( ($categoryId==6 || $categoryId==7 || $categoryId==10 || $categoryId==14 || $categoryId==15) && !is_null($date3) ) {
    			$detail = 'Scheduled appointment at ' . $categoryName . ' is on ' . $date3 . ' with estimated releasing date of ' . $date2 . '.';
    			$detail_cn = '预定的预约 ' . $categoryNameChinese . ' 在 ' . $date3 . ' 和预计完成日期于 ' . $date2 . '.';
    		} elseif( $categoryId==1 && !is_null($dateTime) && $dateTime!='' && $dateTime!=false ) {
    			$detail = 'Filed at ' . $categoryName . ' on ' . $date1 . '. The scheduled hearing date is on ' . $dateTime . ' with estimated releasing date of ' . $date2 . '.';
    			$detail_cn = '以提交于 ' . $categoryNameChinese . ' 在 ' . $date1 . '. 安排的面试日期和时间为 ' . $dateTime . ' 预计完成日期于: ' . $date2 . '.';
    		} else {
    			$detail = 'Filed at ' . $categoryName . ' on ' . $date1 . '. The estimated releasing date is on ' . $date2 . '.';
    			$detail_cn = '以提交于 ' . $categoryNameChinese . ' 在 ' . $date1 . '. 预计完成日期于 ' . $date2 . '.';
    		}
    	} elseif($actionId == 2) {
    		$detail = 'Released from ' . $categoryName . '.';
    		$detail_cn = '已从'.$categoryNameChinese.'完成';
    	} elseif($actionId == 3) {
    		$detail = 'Paid OPS in ' . $categoryName . ' on ' . $date1 . '.';
    		$detail_cn = '已支付收据款于 ' . $categoryNameChinese .'在'.$date1;
    	} elseif($actionId == 4) {
    		$detail = 'Unable to file. Need additional requirements on ' . $date1 . ': ' . $extraDetails . ' are missing.';
    		$detail_cn = '无法提交，需要额外的文件于日期 ' . $date1 . ': ' . $extraDetailsChinese . ' 目前缺失.';
    	}
    	// elseif($actionId == 5) {
    	// 	$detail = 'Unable to file. Need additional requirements on ' . $date1 . ': ' . $extraDetails . ' are missing.';
    	// 	$detail_cn = '无法提交，需要额外的文件于日期 ' . $date1 . ': ' . $extraDetailsChinese . ' 目前缺失.';
    	// }
    	elseif($actionId == 6) {
    		if( is_null($extraDetails) || $extraDetails=='' || $extraDetails==false ) {
    			$detail = 'Deadline of submission: ' . $date1 . '.';
    			$detail_cn = '提交截止日期于： ' . $date1 . '.';
    		} else {
    			$detail = 'Deadline of submission: '. $date1 . '. Consequence of failure of submission of documents: ' . $extraDetails;
    			$detail_cn = '提交截止日期于： '. $date1 . '. 无法如期提交文件的结果： ' . $extraDetailsChinese;
    		}
    	} elseif($actionId == 7) {
    		$detail = 'For Implementation then wait for releasing of icard on ' . $date1 . '. Estimated time of finishing is on ' . $date2 . '.';
    		$detail_cn = '等待盖章然后等待I-Card完成在 ' . $date1 . '. 预计完成日期于 ' . $date2 . '.';
    	} elseif($actionId == 8) {
    		if(is_null($date3)) {
				$detail = 'Unable to file due to ' . $categoryName . ' from ' . $date1 . ' to ' . $date2 . '.';
				$detail_cn = '未能提交因为： ' . $categoryNameChinese . ' 从 ' . $date1 . ' 到 ' . $date2 . '.';
			} else {
				$detail = 'Unable to file due to ' . $categoryName . ' from ' . $date1 . ' to ' . $date2 . '. We will try to resume filing on ' . $date3 . '.';
				$detail_cn = '未能提交因为： ' . $categoryNameChinese . ' 从 ' . $date1 . ' 到 ' . $date2 . '. 我们会回复提交于： ' . $date3 . '.';
			}
    	} elseif($actionId == 9) {
    		$detail = 'Unable to finish due to suspension of work from ' . $date1 . ' to ' . $date2 .' due to ' . $categoryName . '.';
    		$detail_cn = '未能完成因为提前放工 ' . $date1 . ' 到 ' . $date2 .' 因为 ' . $categoryNameChinese . '.';
    	} elseif($actionId == 10) {
    		$detail = 'Extended processing period to ' . $date1 . ' ' .  $categoryName . '.';
    		$detail_cn = '延长办理时间到 ' . $date1 . ' ' .  $categoryNameChinese . '.';
    	} elseif($actionId == 11) {
    		$date1 = (Carbon::parse($date1)->englishDayOfWeek == 'Friday')
			    ? Carbon::parse($date1)->addDays(3)->format('m/d/Y')
			    : Carbon::parse($date1)->addDay()->format('m/d/Y');

    		$detail = 'For payment at ' . $categoryName . ' on ' . $date1 . '. Estimated releasing date is on ' . $date2 . '.';
    		$detail_cn = '已付款于 ' . $categoryNameChinese . ' o在 ' . $date1 . '. 预计完成日期于 ' . $date2 . '.';
    	} elseif($actionId == 12) {
    		$detail = 'Picked up documents from client on ' . $date1 . '.';
    		$detail_cn = '已从客户方取件于 ' . $date1 . '.';
    	} elseif($actionId == 13) {
    		$detail = 'For delivery on ' . $date1 . '.';
    		$detail_cn = '将递送于 ' . $date1 . '.';
    	} elseif($actionId == 14) {
    		$extraDetails = substr($extraDetails, 0, -1);
    		$extraDetails = substr($extraDetails, 3, strlen($extraDetails));

    		if($categoryId == 128) {
    			$detail = 'Delivered by ' . $categoryName . ' ' . $extraDetails . ' on ' . $date1 . '.';
    			$detail_cn = '已由 '.$categoryNameChinese.' 递送 ' . $extraDetails . ' 在 ' . $date1 . '.';
    		} else {
    			$detail = 'Delivered by ' . $categoryName . ' on ' . $date1 . ' with tracking number ' . $extraDetails . '.';
    			$detail_cn = '已由 '.$categoryNameChinese.' 递送 ' . ' 在 ' . $date1 . ' 及查询单号： ' . $extraDetails . '.';
    		}
    	} elseif($actionId == 15) {
    		$detail = 'For processing of ' . $categoryName;
    		$detail_cn = '接着办理 ' . $categoryNameChinese;
    		if(!is_null($date2)) {
    			$detail .= ' with estimated releasing date of ' . $date2. '.';
    			$detail_cn .= ' 预计完成日期于 ' . $date2. '.';
    		} else {
    			$detail .= '.';
    			$detail_cn .= '.';
    		}
    	} elseif($actionId == 16) {
    		$detail = 'Pending: ' . $categoryName . '.';
    		$detail_cn = '待办： ' . $categoryNameChinese . '.';
    	}

    	if( !is_null($clientNames) ) {
    		$detail = $detail . ' (' . $clientNames . ')';
    		$detail_cn = $detail_cn . ' (' . $clientNames . ')';
    	}

    	// return $detail;
    	//$detail_cn = '';
    	return array($detail, $detail_cn);
    }


    //PUSH NOTIFICATIONS
    private function sendPushNotif($csID, $report,$leader = null,$clientNames = null,$report_cn) {
    	$clientService = ClientService::findorFail($csID);

		$translated = Service::where('id',$clientService->service_id)->first();
		
        $cnserv = $translated->detail;
        if($translated){
            $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
        }

		$title = "New service report for ".$clientService->detail;
        $body = $report;
        $title_cn = "新的服务报告 ".$cnserv;
        $body_cn = $report_cn;
        if($leader!=null){
        	$nms = explode(", ", $clientNames);
            sort($nms);
            //$clientNames = implode (", ", $nms);
            $clientNames = '';
            $crt = 1;
            foreach($nms as $n){
            	$clientNames .=$crt.".".$n." ";
            	$crt++;
            }
        	$grp = Group::findorFail($clientService->group_id);
        	$main = $report;
        	$main_cn = $report_cn;
        	$parent = $clientNames;
        	$parent_type = 1;
        	$type = 'GReport';
        	$type_id = $clientService->service_date."--".$grp->tracking;
        }
        else{
        	$clientNames = null;
        	$main = $report;
        	$main_cn = $report_cn;
        	$parent = $clientService->id;
        	$parent_type = 2;
        	$type = 'Report';
        	$type_id = $clientService->id;
        }
        $sendto = ($leader == null ? $clientService->client_id : $leader);
        //\Log::info($sendto);

        Common::savePushNotif($sendto, $title, $body,$type,$type_id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$clientNames);

        //send push notif to vice leaders
        if($leader != null){
        	$vices = GroupMember::where('group_id',$clientService->group_id)->where('leader',2)->get();
        	foreach($vices as $v){
        		$parent = $v->client_id."-".$parent;
        		Common::savePushNotif($v->client_id, $title, $body,$type,$type_id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$clientNames);
        	}
        }
    }

}
