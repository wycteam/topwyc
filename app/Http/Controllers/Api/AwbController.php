<?php

namespace App\Http\Controllers\Api;

use App\Models\Awb;
use App\Models\User;
use Response;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AwbController extends Controller
{

	public function shippingFee(Request $request) {
		$validator = Validator::make($request->all(), [
			'token' => 'required',
			'code' => 'required',
			'shipping_fee' => 'required|numeric'
		]);

		$response = [];

		if( $validator->fails() ) {
			$response['status'] = 'Failed';
			$response['desc'] = $validator->errors();

			$httpStatusCode = 400; // Request Error
		} else {
			if( $this->validateToken($request->token) ) {
				$awb = Awb::where('code', $request->code)->first();

				if( $awb ) {
					$awb->shipping_fee = $request->shipping_fee;
					$awb->save();

					$response['status'] = 'Success';
					$response['desc'] = 'AWB code found';

					$httpStatusCode = 200; // Ok
				} else {
					$response['status'] = 'Failed';
					$response['desc'] = 'Code does not exists';

					$httpStatusCode = 204; // No Content
				}

			} else {
				$response['status'] = 'Failed';
				$response['desc'] = 'Client authentication failed';

				$httpStatusCode = 401; // Client Authentication failed
			}
		}

		return Response::json($response, $httpStatusCode);
	}

	private function validateToken($token) {
		// 66f7f36926407c6ea44beb365c37578b
		$accessToken = User::where('email', 'jason.tan656@gmail.com')->first()->access_token;

		return md5($accessToken) == $token;
	}

}