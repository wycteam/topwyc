<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use App\Models\DatabaseNotification as Notification;

class NotificationsController extends ApiController
{
   	protected $model;
    protected $filters;
    protected $validation;

    public function __construct(Notification $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $user = Auth::user();

        $data = $user->notifications()
            ->where(function ($query) {
                $query->where('type', 'like', '%NewFeedback')
                    ->orWhere('type', 'like', '%NewDocument')
                    ->orWhere('type', 'like', '%NewIssue')
                    ->orWhere('type', 'like', '%FriendRequestAccepted')
                    ->orWhere('type', 'like', '%FeedbackApproved')
                    ->orWhere('type', 'like', '%DocumentApproved')
                    ->orWhere('type', 'like', '%DocumentDenied')
                    ->orWhere('type', 'like', '%ExpiredDocument')
                    ->orWhere('type', 'like', '%NewOrderConfirmation')
                    ->orWhere('type', 'like', '%NewPurchase')
                    ->orWhere('type', 'like', '%OrderStatusUpdate')
                    ->orWhere('type', 'like', '%IssueAssignment')
                    ->orWhere('type', 'like', '%ChangeEwalletTransactionStatus')
                    ->orWhere('type', 'like', '%NewEwalletTransaction')
                    ->orWhere('type', 'like', '%NewProductReview')
                    ->orWhere('type', 'like', '%NewProductReviewComment')
                    ->orWhere('type', 'like', '%NewUser');
            })->get();

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function markAllAsSeen(Request $request)
    {
        $user = Auth::user();

        $data = $user->notifications()
            ->where(function ($query) {
                $query->where('type', 'like', '%NewFeedback')
                    ->orWhere('type', 'like', '%NewDocument')
                    ->orWhere('type', 'like', '%NewIssue')
                    ->orWhere('type', 'like', '%FriendRequestAccepted')
                    ->orWhere('type', 'like', '%FeedbackApproved')
                    ->orWhere('type', 'like', '%DocumentApproved')
                    ->orWhere('type', 'like', '%DocumentDenied')
                    ->orWhere('type', 'like', '%ExpiredDocument')
                    ->orWhere('type', 'like', '%NewOrderConfirmation')
                    ->orWhere('type', 'like', '%NewPurchase')
                    ->orWhere('type', 'like', '%OrderStatusUpdate')
                    ->orWhere('type', 'like', '%IssueAssignment')
                    ->orWhere('type', 'like', '%ChangeEwalletTransactionStatus')
                    ->orWhere('type', 'like', '%NewEwalletTransaction')
                    ->orWhere('type', 'like', '%NewProductReview')
                    ->orWhere('type', 'like', '%NewProductReviewComment')
                    ->orWhere('type', 'like', '%NewUser');
            })->where('seen_at', null)
            ->update(['seen_at' => Carbon::now()]);

        return $this->respondUpdated($data);
    }

    public function markAsRead(Request $request, $id)
    {
        $authUser = Auth::user();

        $data = $authUser->unreadNotifications()
            ->whereId($id)
            ->update(['read_at' => Carbon::now()]);

        return $this->respondUpdated($data);
    }
}
