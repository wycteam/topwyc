<?php

namespace App\Http\Controllers\Api;

use App\Models\ScheduleType;

use Illuminate\Http\Request;

use App\Http\Controllers\ApiController;

class ScheduleTypeController extends ApiController
{
    
	public function index() {
		return ScheduleType::all();
	}

}
