<?php

namespace App\Http\Controllers\Api\Helpers;

use App\Models\Visa\Category;
use App\Models\Visa\ClientService;
use App\Models\Visa\Group;
use App\Models\Visa\GroupMember;
use App\Models\Visa\Service;
use App\Models\Visa\ServiceDocuments;
use App\Models\User;

use App\Classes\Common;

use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WriteReportHelperController extends Controller {

    public static function getData($clientServices) {
        $distinctClientServices = [];
        $clientsId = [];
        $trackings = [];

        foreach($clientServices as $clientService) {
            if(!in_array($clientService['detail'], $distinctClientServices)) {
                $distinctClientServices[] = $clientService['detail'];
            }

            if(!in_array($clientService['client_id'], $clientsId)) {
                $clientsId[] = $clientService['client_id'];
            }

            if(!in_array($clientService['tracking'], $trackings)) {
                $trackings[] = $clientService['tracking'];
            }
        }

        return [
            'distinctClientServices' => $distinctClientServices,
            'clientsId' => $clientsId,
            'trackings' => $trackings
        ];
    }

    private static function concatenateDateTimeArray($dateTime) {
        $str = '';
        for($i=0; $i<count($dateTime); $i++) {
            $str .= $dateTime[$i] . ' and ';
        }
        $str = substr($str, 0, -5);

        return $str;
    }

    private static function concatenateExtraDetailsArray($extraDetails) {
        $str = '';
        for($i=0; $i<count($extraDetails); $i++) {
            $str .= ($i+1) . '. ' . $extraDetails[$i] . ' ';
        }

        return $str;
    }

    private static function concatenateExtraDetailsArrayChinese($extraDetails) {
        $str_cn = '';
        for($i=0; $i<count($extraDetails); $i++) {
            $doc_cn = ServiceDocuments::where('title', $extraDetails[$i])->first();

			if($doc_cn) {
				$str_cn .= ($i+1) . '. ' . $doc_cn->title_cn . ' ';
			} elseif($extraDetails[$i] == 'Additional cost maybe charged.') {
				$str_cn .= ($i+1) . '. 可能产生附加的费用 ';
			} elseif($extraDetails[$i] == 'Application maybe forfeited and has to re-apply.') {
				$str_cn .= ($i+1) . '. 该次申请作废 ';
			} elseif($extraDetails[$i] == 'Initial deposit maybe forfeited.') {
				$str_cn .= ($i+1) . '. 预付款作废 ';
			} else {
				$str_cn .= ($i+1) . '. ' . $extraDetails[$i] . ' ';
			}
        }

        return $str_cn;
    }

    public static function getData2($data) {
    	$actionIdArray = [];
    	$categoryIdArray = [];
        $categoryNameArray = [];
        $categoryNameChinese = [];
        $date1Array = [];
        $date2Array = [];
        $date3Array = [];
        $dateTimeArray = [];
        $extraDetailsArray = [];
        $extraDetailsArrayChinese = [];

        for($i=0; $i<count($data); $i++) {
        	$actionIdArray[] = $data[$i]['action_id'];

            if($data[$i]['action_id'] == 1) {
            	$categoryIdArray[] = $data[$i]['action_1_category_id'];
                $categoryNameArray[] = Category::find($data[$i]['action_1_category_id'])->name;
                $getcat = Category::find($data[$i]['action_1_category_id']);
				$categoryNameChinese[] = ($getcat->name_cn !=null ? $getcat->name_cn : $getcat->name);

                $date1Array[] = $data[$i]['action_1_current_date'];
                $date2Array[] = $data[$i]['action_1_estimated_releasing_date'];
                $date3Array[] = $data[$i]['action_1_scheduled_appointment_date_and_time'];
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray($data[$i]['action_1_scheduled_hearing_date_and_time']);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]);
            }
            elseif($data[$i]['action_id'] == 2) {
            	$categoryIdArray[] = $data[$i]['action_2_category_id'];
                $categoryNameArray[] = Category::find($data[$i]['action_2_category_id'])->name;
                $getcat = Category::find($data[$i]['action_2_category_id']);
				$categoryNameChinese[] = ($getcat->name_cn !=null ? $getcat->name_cn : $getcat->name);

                $date1Array[] = null;
                $date2Array[] = null;
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]);
            }
            elseif($data[$i]['action_id'] == 3) {
            	$categoryIdArray[] = $data[$i]['action_3_category_id'];
                $categoryNameArray[] = Category::find($data[$i]['action_3_category_id'])->name;
                $getcat = Category::find($data[$i]['action_3_category_id']);
				$categoryNameChinese[] = ($getcat->name_cn !=null ? $getcat->name_cn : $getcat->name);

                $date1Array[] = $data[$i]['action_3_current_date'];
                $date2Array[] = null;
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]); 
            }
            elseif($data[$i]['action_id'] == 4) {
            	$categoryIdArray[] = null;
                $categoryNameArray[] = null;
                $categoryNameChinese[] = null;

                $date1Array[] = $data[$i]['action_4_current_date'];
                $date2Array[] = null;
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray($data[$i]['action_4_documents']);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese($data[$i]['action_4_documents']); 
            }
            elseif($data[$i]['action_id'] == 6) {
            	$categoryIdArray[] = null;
                $categoryNameArray[] = null;
                $categoryNameChinese[] = null;
                
                $date1Array[] = $data[$i]['action_6_submission_date'];
                $date2Array[] = null;
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray($data[$i]['action_6_consequence_of_non_submission']);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese($data[$i]['action_6_consequence_of_non_submission']); 
            }
            elseif($data[$i]['action_id'] == 7) {
            	$categoryIdArray[] = null;
                $categoryNameArray[] = null;
                $categoryNameChinese[] = null;

                $date1Array[] = $data[$i]['action_7_current_date'];
                $date2Array[] = $data[$i]['action_7_estimated_time_of_finishing'];
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]);
            }
            elseif($data[$i]['action_id'] == 8) {
            	$categoryIdArray[] = $data[$i]['action_8_category_id'];
                $categoryNameArray[] = Category::find($data[$i]['action_8_category_id'])->name;
                $getcat = Category::find($data[$i]['action_8_category_id']);
				$categoryNameChinese[] = ($getcat->name_cn !=null ? $getcat->name_cn : $getcat->name);

                $date1Array[] = $data[$i]['action_8_affected_date_from'];
                $date2Array[] = $data[$i]['action_8_affected_date_to'];
                $date3Array[] = $data[$i]['action_8_target_filling_date'];
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]);
            }
            elseif($data[$i]['action_id'] == 9) {
                if($data[$i]['action_9_category_id'] == 0) {
                	$categoryIdArray[] = null;
                    $categoryNameArray[] = $data[$i]['action_9_category_name'];
                    $categoryNameChinese[] = null;
                } else {
                	$categoryIdArray[] = $data[$i]['action_9_category_id'];
                    $categoryNameArray[] = Category::find($data[$i]['action_9_category_id'])->name;
                    $getcat = Category::find($data[$i]['action_9_category_id']);
					$categoryNameChinese[] = ($getcat->name_cn !=null ? $getcat->name_cn : $getcat->name);
                }

                $date1Array[] = $data[$i]['action_9_date_range_from'];
                $date2Array[] = $data[$i]['action_9_date_range_to'];
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]);
            }
            elseif($data[$i]['action_id'] == 10) {
            	$categoryIdArray[] = $data[$i]['action_10_category_id'];
                $categoryNameArray[] = Category::find($data[$i]['action_10_category_id'])->name;
                $getcat = Category::find($data[$i]['action_10_category_id']);
				$categoryNameChinese[] = ($getcat->name_cn !=null ? $getcat->name_cn : $getcat->name);

                $date1Array[] = $data[$i]['action_10_extended_period_for_processing'];
                $date2Array[] = null;
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]);
            }
            elseif($data[$i]['action_id'] == 11) {
            	$categoryIdArray[] = $data[$i]['action_11_category_id'];
                $categoryNameArray[] = Category::find($data[$i]['action_11_category_id'])->name;
                $getcat = Category::find($data[$i]['action_11_category_id']);
				$categoryNameChinese[] = ($getcat->name_cn !=null ? $getcat->name_cn : $getcat->name);

                $date1Array[] = $data[$i]['action_11_current_date'];
                $date2Array[] = $data[$i]['action_11_estimated_releasing_date'];
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]);
            }
            elseif($data[$i]['action_id'] == 12) {
            	$categoryIdArray[] = null;
                $categoryNameArray[] = null;
                $categoryNameChinese[] = null;

                $date1Array[] = $data[$i]['action_12_date_and_time_pickup'];
                $date2Array[] = null;
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]);
            }
            elseif($data[$i]['action_id'] == 13) {
            	$categoryIdArray[] = null;
                $categoryNameArray[] = null;
                $categoryNameChinese[] = null;

                $date1Array[] = $data[$i]['action_13_date_and_time_deliver'];
                $date2Array[] = null;
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]);
            }
            elseif($data[$i]['action_id'] == 14) {
            	$categoryIdArray[] = $data[$i]['action_14_category_id'];
                $categoryNameArray[] = Category::find($data[$i]['action_14_category_id'])->name;
                $getcat = Category::find($data[$i]['action_14_category_id']);
				$categoryNameChinese[] = ($getcat->name_cn !=null ? $getcat->name_cn : $getcat->name);

                $date1Array[] = $data[$i]['action_14_date_and_time_delivered'];
                $date2Array[] = null;
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray(($data[$i]['action_14_category_id'] == 128) ? $data[$i]['action_14_company_courier'] : $data[$i]['action_14_tracking_number']);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese(($data[$i]['action_14_category_id'] == 128) ? $data[$i]['action_14_company_courier'] : $data[$i]['action_14_tracking_number']);
            }
            elseif($data[$i]['action_id'] == 15) {
            	$categoryIdArray[] = $data[$i]['action_15_category_id'];
                $categoryNameArray[] = Category::find($data[$i]['action_15_category_id'])->name;
                $getcat = Category::find($data[$i]['action_15_category_id']);
				$categoryNameChinese[] = ($getcat->name_cn !=null ? $getcat->name_cn : $getcat->name);

                $date1Array[] = $data[$i]['action_15_current_date'];
                $date2Array[] = $data[$i]['action_15_estimated_releasing_date'];
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]);
            }
            elseif($data[$i]['action_id'] == 16) {
            	$categoryIdArray[] = $data[$i]['action_16_category_id'];
                $categoryNameArray[] = Category::find($data[$i]['action_16_category_id'])->name;
                $getcat = Category::find($data[$i]['action_16_category_id']);
				$categoryNameChinese[] = ($getcat->name_cn !=null ? $getcat->name_cn : $getcat->name);

                $date1Array[] = null;
                $date2Array[] = null;
                $date3Array[] = null;
                $dateTimeArray[] = WriteReportHelperController::concatenateDateTimeArray([]);
                $extraDetailsArray[] = WriteReportHelperController::concatenateExtraDetailsArray([]);
                $extraDetailsArrayChinese[] = WriteReportHelperController::concatenateExtraDetailsArrayChinese([]);
            }
        }

        return [
        	'actionIdArray' => $actionIdArray,
        	'categoryIdArray' => $categoryIdArray,
            'categoryNameArray' => $categoryNameArray,
            'categoryNameChinese' => $categoryNameChinese,
            'date1Array' => $date1Array,
            'date2Array' => $date2Array,
            'date3Array' => $date3Array,
            'dateTimeArray' => $dateTimeArray,
            'extraDetailsArray' => $extraDetailsArray,
            'extraDetailsArrayChinese' => $extraDetailsArrayChinese
        ];
    }

    public static function getReportDetail($clientNames, $actionId, $categoryId, $categoryName, $date1, $date2, $date3, $dateTime, $extraDetails, $categoryNameChinese = null, $extraDetailsChinese = null) {

    	$detail_cn = '';

    	if($actionId == 1) {
    		$detail_cn = $date3;

    		if( ($categoryId==6 || $categoryId==7 || $categoryId==10 || $categoryId==15) && !is_null($date3) ) {
    			$detail = 'Filed at ' . $categoryName . ' on ' . $date1 . '. The scheduled appointment at ' . $categoryName . ' is on ' . $date3 . '. Failure to attend the appointment date may delay the application.';
    			$detail_cn = '以提交于 ' . $categoryNameChinese. ' 在 ' . $date1 .'. 安排的面试日期和时间为 '. $date3 .'. 未能参加预约日期可能会延迟申请.';
    		} elseif( $categoryId==1 && !is_null($dateTime) && $dateTime!='' && $dateTime!=false ) {
    			$detail = 'Filed at ' . $categoryName . ' on ' . $date1 . '. The scheduled hearing date is on ' . $dateTime . '. Failure to attend both hearing date will automatic result abandonment of application. The estimated releasing date is on ' . $date2 . '.';
    			$detail_cn = '以提交于 ' . $categoryNameChinese . ' 在 ' . $date1 . '. 安排的面试日期和时间为 ' . $dateTime . '. 在预定日期不出席面试将自动视为放弃该申请。 预计完成日期于 ' . $date2 . '.';
    		} elseif( ($categoryId==6 || $categoryId==7 || $categoryId==10 || $categoryId==14 || $categoryId==15) && !is_null($date3) ) {
    			$detail = 'Scheduled appointment at ' . $categoryName . ' is on ' . $date3 . ' with estimated releasing date of ' . $date2 . '.';
    			$detail_cn = '预定的预约 ' . $categoryNameChinese . ' 在 ' . $date3 . ' 和预计完成日期于 ' . $date2 . '.';
    		} elseif( $categoryId==1 && !is_null($dateTime) && $dateTime!='' && $dateTime!=false ) {
    			$detail = 'Filed at ' . $categoryName . ' on ' . $date1 . '. The scheduled hearing date is on ' . $dateTime . ' with estimated releasing date of ' . $date2 . '.';
    			$detail_cn = '以提交于 ' . $categoryNameChinese . ' 在 ' . $date1 . '. 安排的面试日期和时间为 ' . $dateTime . ' 预计完成日期于: ' . $date2 . '.';
    		} else {
    			$detail = 'Filed at ' . $categoryName . ' on ' . $date1 . '. The estimated releasing date is on ' . $date2 . '.';
    			$detail_cn = '以提交于 ' . $categoryNameChinese . ' 在 ' . $date1 . '. 预计完成日期于 ' . $date2 . '.';
    		}
    	} elseif($actionId == 2) {
    		$detail = 'Released from ' . $categoryName . '.';
    		$detail_cn = '已从'.$categoryNameChinese.'完成';
    	} elseif($actionId == 3) {
    		$detail = 'Paid OPS in ' . $categoryName . ' on ' . $date1 . '.';
    		$detail_cn = '已支付收据款于 ' . $categoryNameChinese .'在'.$date1;
    	} elseif($actionId == 4) {
    		$detail = 'Unable to file. Need additional requirements on ' . $date1 . ': ' . $extraDetails . ' are missing.';
    		$detail_cn = '无法提交，需要额外的文件于日期 ' . $date1 . ': ' . $extraDetailsChinese . ' 目前缺失.';
    	} 
    	// elseif($actionId == 5) {
    	// 	$detail = 'Unable to file. Need additional requirements on ' . $date1 . ': ' . $extraDetails . ' are missing.';
    	// } 
    	elseif($actionId == 6) {
    		if( is_null($extraDetails) || $extraDetails=='' || $extraDetails==false ) {
    			$detail = 'Deadline of submission: ' . $date1 . '.';
    			$detail_cn = '提交截止日期于： ' . $date1 . '.';
    		} else {
    			$detail = 'Deadline of submission: ' . $date1 . '. Consequence of failure of submission of documents: ' . $extraDetails;
    			$detail_cn = '提交截止日期于： '. $date1 . '. 无法如期提交文件的结果： ' . $extraDetailsChinese;
    		}
    	} elseif($actionId == 7) {
    		$detail = 'For Implementation then wait for releasing of icard on ' . $date1 . '. Estimated time of finishing is on ' . $date2 . '.';
    		$detail_cn = '等待盖章然后等待I-Card完成在 ' . $date1 . '. 预计完成日期于 ' . $date2 . '.'; 
    	} elseif($actionId == 8) {
    		if(is_null($date3)) {
				$detail = 'Unable to file due to ' . $categoryName . ' from ' . $date1 . ' to ' . $date2 . '.';
				$detail_cn = '未能提交因为： ' . $categoryNameChinese . ' 从 ' . $date1 . ' 到 ' . $date2 . '.';
			} else {
				$detail = 'Unable to file due to ' . $categoryName . ' from ' . $date1 . ' to ' . $date2 . '. We will try to resume filing on ' . $date3 . '.';
				$detail_cn = '未能提交因为： ' . $categoryNameChinese . ' 从 ' . $date1 . ' 到 ' . $date2 . '. 我们会回复提交于： ' . $date3 . '.';
			}
    	} elseif($actionId == 9) {
    		$detail = 'Unable to finish due to suspension of work from ' . $date1 . ' to ' . $date2 .' due to ' . $categoryName . '.';
    		$detail_cn = '未能完成因为提前放工 ' . $date1 . ' 到 ' . $date2 .' 因为 ' . $categoryNameChinese . '.';
    	} elseif($actionId == 10) {
    		$detail = 'Extended processing period to ' . $date1 . ' ' .  $categoryName . '.';
    		$detail_cn = '延长办理时间到 ' . $date1 . ' ' .  $categoryNameChinese . '.';
    	} elseif($actionId == 11) {
            $date1 = (Carbon::parse($date1)->englishDayOfWeek == 'Friday') 
                ? Carbon::parse($date1)->addDays(3)->format('m/d/Y')
                : Carbon::parse($date1)->addDay()->format('m/d/Y');
                
    		$detail = 'For payment at ' . $categoryName . ' on ' . $date1 . '. Estimated releasing date is on ' . $date2 . '.';
    		$detail_cn = '已付款于 ' . $categoryNameChinese . ' o在 ' . $date1 . '. 预计完成日期于 ' . $date2 . '.';
    	} elseif($actionId == 12) {
    		$detail = 'Picked up documents from client on ' . $date1 . '.';
    		$detail_cn = '已从客户方取件于 ' . $date1 . '.';
    	} elseif($actionId == 13) {
    		$detail = 'For delivery on ' . $date1 . '.';
    		$detail_cn = '将递送于 ' . $date1 . '.';
    	} elseif($actionId == 14) {
    		$extraDetails = substr($extraDetails, 0, -1);
    		$extraDetails = substr($extraDetails, 3, strlen($extraDetails));

    		if($categoryId == 128) {
    			$detail = 'Delivered by ' . $categoryName . ' ' . $extraDetails . ' on ' . $date1 . '.';
    			$detail_cn = '已由 '.$categoryNameChinese.' 递送 ' . $extraDetails . ' 在 ' . $date1 . '.';
    		} else {
    			$detail = 'Delivered by ' . $categoryName . ' on ' . $date1 . ' with tracking number ' . $extraDetails . '.';
    			$detail_cn = '已由 '.$categoryNameChinese.' 递送 ' . ' 在 ' . $date1 . ' 及查询单号： ' . $extraDetails . '.';
    		}
    	} elseif($actionId == 15) {
    		$detail = 'For processing of ' . $categoryName;
    		$detail_cn = '接着办理 ' . $categoryNameChinese;
    		if(!is_null($date2)) {
    			$detail .= ' with estimated releasing date of ' . $date2. '.';
    			$detail_cn .= ' 预计完成日期于 ' . $date2. '.';
    		} else {
    			$detail .= '.';
    			$detail_cn .= '.';
    		}
    	} elseif($actionId == 16) {
    		$detail = 'Pending: ' . $categoryName . '.';
    		$detail_cn = '待办： ' . $categoryNameChinese . '.';
    	}

    	if( !is_null($clientNames) ) {
    		$detail = $detail . ' (' . $clientNames . ')';
    		$detail_cn = $detail_cn . ' (' . $clientNames . ')';
    	}

    	return array($detail, $detail_cn);
    }

    public static function getAllClientNames($clientsId, $groupLeaderId) {
    	$clients = User::whereIn('id', $clientsId)->select('first_name', 'last_name')->get();

		$names = '';
		foreach($clients as $client) {
			$names .= $client->first_name . ' ' . $client->last_name . ', ';
		}
		
		return substr($names, 0, -2);
    }

    public static function checkIfGroupLeader($groupId, $clientId) {
		$ifGroupLeader = false;
		if($groupId != 0) {
			$groupLeaderId = Group::find($groupId)->leader;

			if($clientId == $groupLeaderId) {
				$ifGroupLeader = true;
			}
			$clt[] = $clientId; 
			$groupVice = GroupMember::where('group_id',$groupId)->where('leader',2)->whereIn('client_id',$clt)->count();
			if($groupVice > 0){
				$ifGroupLeader = true;
			}
		}

		return $ifGroupLeader;
	}

	public static function sendPushNotif($csID, $report,$leader = null,$clientNames = null, $report_cn) {
    	$clientService = ClientService::findorFail($csID);

        $translated = Service::where('id',$clientService->service_id)->first();
        $cnserv = $translated->detail;
        if($translated){
            $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
        }

		$title = "New service report for ".$clientService->detail;
        $body = $report;
        $title_cn = "新的服务报告 ".$cnserv;
        $body_cn = $report_cn;
        if($leader!=null){
        	$nms = explode(", ", $clientNames);
            sort($nms);
            //$clientNames = implode (", ", $nms);
            $clientNames = '';
            $crt = 1;
            foreach($nms as $n){
            	$clientNames .=$crt.".".$n." ";
            	$crt++;
            }
        	$grp = Group::findorFail($clientService->group_id);
        	$main = $report;
        	$main_cn = $report_cn;
        	$parent = $clientNames;
        	$parent_type = 1;
        	$type = 'GReport';
        	$type_id = $clientService->service_date."--".$grp->tracking;
        }
        else{
        	$clientNames = null;
        	$main = $report;
        	$main_cn = $report_cn;
        	$parent = $clientService->id;
        	$parent_type = 2;
        	$type = 'Report';
        	$type_id = $clientService->id;
        }
        $sendto = ($leader == null ? $clientService->client_id : $leader);
        //\Log::info($sendto);

        Common::savePushNotif($sendto, $title, $body,$type,$type_id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$clientNames);

        //send push notif to vice leaders
        if($leader != null){
        	$vices = GroupMember::where('group_id',$clientService->group_id)->where('leader',2)->get();
        	foreach($vices as $v){
        		$parent = $v->client_id."-".$parent;
        		Common::savePushNotif($v->client_id, $title, $body,$type,$type_id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$clientNames);
        	}
        }
    }
	
}