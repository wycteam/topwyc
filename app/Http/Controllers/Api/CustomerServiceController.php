<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Issue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;
use App\Notifications\NewChatMessage;
use App\Notifications\IssueAssignment;
use Illuminate\Support\Facades\Notification;
use App\Models\Views\LatestMessage;

class CustomerServiceController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cpanel.customer-service.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $issue = Issue::with(['chat_room.messages.user', 'chat_room.users'])->findOrFail($id);

        $supportUsers = User::whereHas('roles', function ($query) {
            $query->where('name', 'cpanel-admin')
                ->orWhere('name', 'cpanel-user')
                ->orWhere('name', 'support');
        })->orderBy('first_name')->get();

        session()->flash('data', compact('issue', 'supportUsers'));

        return view('cpanel.customer-service.show', compact('issue'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DB::transaction(function () use ($request, $id) {
            $issue = Issue::findOrFail($id);
            $issue->fill($request->all())
                ->save();

            if ($issue->status != 'closed') {
                $supportIds = collect($request->get('supportIds', []));
                $supportUsers = [];

                if (! $issue->chat_room && ! $supportIds->isEmpty()) {
    
                    $supportUsers = User::whereIn('id', $supportIds->toArray())->get();
                    //assign support to chat room together with the issuer
                    $chatRoom = $issue->assignSupportToChatRoom($supportIds->toArray());
                    //primary message of convo subject and body of issue
                    $message = $chatRoom->messages()
                        ->create([
                            'user_id' => $issue->user_id,
                            'body' => "Subject: ".$issue->subject,
                        ]);
                    $message = $chatRoom->messages()
                        ->create([
                            'user_id' => $issue->user_id,
                            'body' => "Description: ".$issue->body,
                        ]);
                    $fUser = $supportUsers->first();
                    $message = $chatRoom->messages()
                        ->create([
                            'user_id' => $fUser->id,
                            'body' => "We’re currently experiencing a high volume of tickets but will reply back as soon as we can.",
                        ]);

                    if (! empty($supportUsers)) {
                        Notification::send($supportUsers, new NewChatMessage($issue->user, $message));
                    }
                } elseif (! $issue->chat_room && $supportIds->isEmpty()) {

                    $chatRoom = $issue->createChatRoom();
                    //primary message of convo subject and body of issue
                    $message = $chatRoom->messages()
                        ->create([
                            'user_id' => $issue->user_id,
                            'body' => "Subject: ".$issue->subject,
                        ]);
                    $message = $chatRoom->messages()
                        ->create([
                            'user_id' => $issue->user_id,
                            'body' => "Description: ".$issue->body,
                        ]);
                    $message = $chatRoom->messages()
                        ->create([
                            'user_id' => $issue->user_id,
                            'body' => "We’re currently experiencing a high volume of tickets but will reply back as soon as we can.",
                        ]);
                } elseif ($issue->chat_room) {
                    $chatRoomUserIds = $issue->chat_room->users->pluck('id');
                    $newSupportUserIds = $supportIds->diff($chatRoomUserIds)->toArray();
                    $supportUsers = User::whereIn('id', $newSupportUserIds)->get();

                    $issue->chat_room->users()
                        ->sync(array_merge($supportIds->toArray(), [$issue->user_id]));
                }

                if (! empty($supportUsers)) {
                    Notification::send($supportUsers, new IssueAssignment($issue, Auth::user()));
                }
            }
            
            if($request->flag) {
                return $chatRoom;
            }

            return $issue;
        });

        //return $this->respondCreated($data);

        $user = Auth::user();

        //$chatRoomIds = $user->chat_rooms->pluck('id')->toArray();

        $data2 = LatestMessage::with([
            'chat_room.users' => function ($query) use ($user) {
                $query->select('id', 'first_name', 'last_name', 'nick_name', 'gender', 'is_display_name')
                    ->where('id', '!=', $user->id);
            }
        ])
        ->where('chat_room_id','=',$data['id'])
        ->get();

        return $this->respondCreated($data2);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
