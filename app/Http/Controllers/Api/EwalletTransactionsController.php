<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

use App\Models\Shopping\EwalletTransactions;
use App\Models\Shopping\EwalletLogs;
use App\Models\Shopping\Ewallet;
use App\Models\Shopping\Order;
use App\Models\Shopping\Product;
use App\Models\Shopping\OrderDetail;
use App\Models\User;
use App\Notifications\NewEwalletTransaction;

use App\Http\Requests\LoadOfflineRequest;
use Auth;
use Image;
use DB;
use Carbon\Carbon;

use App\Acme\Filters\EwalletTransactionFilters;

use App\Classes\Common;

class EwalletTransactionsController extends ApiController
{
    public function __construct(EwalletTransactions $model, EwalletTransactionFilters $filters)
    {
        $this->filters = $filters;
        $this->model = $model;
        $this->validation = new LoadOfflineRequest ;
    }

    public function deposit(EwalletTransactions $model){
        return $model->where('type','Deposit')->get();
    }

    private function isWatafPersonnel(){
        $cpanel_account = Auth::user();
        if(request()->has('wataf_user_id') && request()->isMethod('post'))
        {
            if(
                $cpanel_account->hasRole('master') 
                || $cpanel_account->hasRole('vice-master') 
                || $cpanel_account->hasRole('cpanel-admin') 
                || $cpanel_account->hasRole('shop-admin') 
                || $cpanel_account->hasRole('support') 
                || $cpanel_account->hasRole('cpanel-user') 
            ){
                return request()->get('wataf_user_id');
            }
            return false;
        }
        return false;
    }

    public function getPurchasedOrders(){
        $user = Auth::user();
        $findUserId = $this->isWatafPersonnel()? : $user->id;
        $data = Order::where('user_id',$findUserId)->where('status','!=','Cancelled')->get();

        $data->map(function($item, $key){
            $item->ref_number = $item->tracking_number;
            $item->type = 'Purchase';
            $item->description = 'Made a purchase';
            $item->amount = $item->total;
        });

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function getCancelledOrders(){
        $user = Auth::user();
        $findUserId = $this->isWatafPersonnel()? : $user->id;

        $data = OrderDetail::whereHas('order',function($q) use ($findUserId){
            $q->where('user_id',$findUserId);
        })->where(function($q){
            $q->where(function($que){
                $que->where('status','Returned')
                    ->whereNotNull('returned_received_date');
            });
            $q->orWhere('status','Cancelled');
        })
        ->orderBy('product_id')
        ->get();
        
       $data->map(function($item, $key){
            $item->ref_number = $item->order->tracking_number;

            if($item->status == 'Returned')
            {
                $item->type = 'Returned Order';
                $item->description = 'Returned by Seller';

            }else if($item->status == 'Cancelled')
            {
                $item->type = 'Cancelled Order';
                $item->description = 'Cancelled by Seller';
            }
            $item->status = 'Complete';
            $pr = ($item->sale_price > 0 ? $item->sale_price : $item->price_per_item);
            $amount = $item->subtotal;
            $item->amount = $amount;
            $item->user_id = $item->order->user_id;
        });

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function getSoldOrders(){
        $user = Auth::user();
        $findUserId = $this->isWatafPersonnel()? : $user->id;

        $data = OrderDetail::with('product')
            ->whereHas('product', function($q) use ($findUserId){
                $q->where('user_id',$findUserId) ;
            })
            ->where(function($q){
                $q->where('status','Received');
                $q->orWhere('status','Delivered');
            })
            ->orderBy('product_id')
            ->get();
        
       $data->map(function($item, $key) use ($findUserId){
            $item->ref_number = $item->combined_tracking_number;
            $item->type = 'Sold';
            $item->description = 'Product Sold: '.$item->product->name.' '.$item->quantity.'x';
            $item->amount = (($item->sale_price > 0 ? $item->sale_price : $item->price_per_item )*  $item->quantity) + $item->disc_fee;
            $item->user_id = $findUserId;
            //manually changed
            $item->status = 'Completed';
        });

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function getReturnedOrders(){
        $user = Auth::user();
        $findUserId = $this->isWatafPersonnel()? : $user->id;

        $data = OrderDetail::with('product')
            ->whereHas('product', function($q) use ($findUserId){
                $q->where('user_id',$findUserId) ;
            })
            ->where(function($q){
                $q->where('status','Returned');
                $q->Where('returned_received_date','!=','');
            })
            ->get();
       $wallet = $user->ewallet()->first();
       $total  = $wallet->amount;
       $data->map(function($item, $key) use ($findUserId,$total){
            $item->ref_number = $item->combined_tracking_number;
            $item->type = 'Product Returned by Buyer';
            $item->description = 'Product Returned: '.$item->product->name.' '.$item->quantity.'x';
            $pr = ($item->sale_price > 0 ? $item->sale_price : $item->price_per_item);
            $amount = ($pr * $item->quantity) + $item->disc_fee;
            $item->amount = $amount;
            $item->user_id = $findUserId;
            $item->balance = $total;
            //manually changed
            $item->status = 'Completed';
        });

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function store(Request $request)
    {
        app(get_class($this->validation));

        $data = $this->model->create($request->all());

        $adminUsers = User::whereHas('roles', function ($query) {
            $query->where('name', 'master')
                ->orWhere('name', 'vice-master')
                ->orWhere('name', 'cpanel-admin')
                ->orWhere('name', 'support');
        })->get();
        
        if($data->type == 'Withdraw')
        {
            Notification::send($adminUsers, new NewEwalletTransaction($this->model->findOrFail($data->id)));
        }
        else if($data->type == 'Deposit')
        {
            Notification::send($adminUsers, new NewEwalletTransaction($this->model->findOrFail($data->id)));
        }
        else if($data->type == 'Transfer')
        {
            $receiver = User::findOrFail($data->rcvr_id);
            $receiver->notify(new NewEwalletTransaction($this->model->findOrFail($data->id)));
        }

        return $this->respondCreated($data);
    }

    public function update(Request $request, $id)
    {
        $etransaction = $this->model
            ->findOrFail($id);
        $etransaction->reason = $request->get('reason');
        if($etransaction && $request->get('status')=='Completed') // updated and found
        {
            $ewallet_account = Ewallet::whereUserId($etransaction->user_id)->first();
            //update the user's ewallet replenish or diminish
            if($this->updateUserEwallet($etransaction,$ewallet_account, $request->get('status')))
            {
                return $this->respondUpdated($etransaction);
            }else
            {
                return $this->respondNotFound();
            }
        }else if($request->get('status') == 'Processing' || $request->get('status') == 'Cancelled')
        {
            $etransaction->status = $request->get('status');
            
            $transaction_amount = $etransaction->amount;
            $returnMoney = false;

            if($request->get('status') == 'Cancelled')
            {
                //if there are already logs about the transaction
                if($etransaction->logs()->count())
                {
                    //get the latest log
                    $log = $etransaction->logs()->orderBy('created_at','desc')->first();
                    $recent_status = strtolower($log->action);
                    //if the latest status is completed
                    if(strpos($recent_status, 'complete'))
                    {
                        //should return the money
                        $returnMoney = true;
                        //update the balance
                        $etransaction->balance = $etransaction->user->ewallet->amount -= $transaction_amount;
                    }
                }
            }

            if($etransaction->isDirty()){
                //getOriginal() -> get original values of model
                //getDirty -> get all fields updated with value
                $changes = $etransaction->getDirty();
                $detail = 'Updated E-Wallet -> ';
                foreach ($changes as $key => $value) {
                    $old = $etransaction->getOriginal($key);

                    if($key == 'reason') {
                        if(is_null($old)) {
                            $old = 'NULL';
                        }
                        if(is_null($value)) {
                            $value = 'NULL';
                        }
                    }

                    $detail .= "Change ".$key." from ".$old." to ".$value.". ";
                }
                Common::saveActionLogs('Ewallet', $etransaction->id, $detail);
            }

            //save the new status
            $etransaction->save();

            if($returnMoney)
            {
                //subtract the wrong amount given
                $etransaction->user->ewallet->amount = $etransaction->balance;
                //update
                $etransaction->user->ewallet->save();
            }


            return $this->respondUpdated($etransaction);
        }else
        {
            return $this->respondUpdated($etransaction);
        }

    }

    public function receiveEwallet(Request $request){
        if($request->has('refnum'))
        {   
            $refnum = $request->get('refnum');
            $etransaction = $this->model
                ->where('ref_number',$refnum)
                ->where('status','Pending')
                ->where('type','Transfer')
                ->where('rcvr_id',Auth::user()->id)
                ->first();

            if($etransaction)
            {
                $receiver = Auth::user();

                $receiver_account = Ewallet::where('user_id',$etransaction->rcvr_id)->first();
                $sender_account = Ewallet::where('user_id',$etransaction->user_id)->first();

                $receiver_account->amount += $etransaction->amount;
                $sender_account->amount -= $etransaction->amount;
                $sender_balance = $sender_account->amount;

                $receiver_current_balance = $receiver_account->amount;

                if($receiver_account->save() && $sender_account->save())
                {
                    
                    $etransaction->status = 'Completed';
                    $etransaction->balance = $sender_balance;
                    $etransaction->save();

                        $sender_account->user->notify(new NewEwalletTransaction($this->model->findOrFail($etransaction->id)));
                        //create a transaction for the receiver
                        $create = [
                             'user_id'      => $receiver->id
                            ,'type'         => 'Received'
                            ,'ref_number'   => $refnum
                            ,'rcvr_id'      => $sender_account->id
                            ,'amount'       =>  $etransaction->amount
                            ,'description'  => 'Received from '. $sender_account->user->full_name
                            ,'status'       => 'Completed'
                            ,'balance'      => $receiver_current_balance
                        ];

                        $created = $this->model->create($create);
                        $updated_at = EwalletTransactions::where('ref_number',$refnum)->first()->updated_at;
                        $create['updated_at'] = Carbon::parse($updated_at)->format('Y-m-d G:i:s');
                        return $this->respondUpdated($create);

                }

            }else
            {
                return response()->json(false);
            }

        }else
        {
            return response()->json(false);
        }
    }

    private function updateUserEwallet($etransaction,$account, $status){
        
        switch ($etransaction->type) {
            case 'Deposit':
                //increase the user's ewallet
                $account->amount += $etransaction->amount;
                //update the balance on the transaction made
                $etransaction->balance = $account->amount;

                break;
            case 'Withdraw':

                //check logs for completed
                    //add the amount to the owner of the ewallet
                //else

                //decrease the user's ewallet
                $account->amount -= $etransaction->amount;
                 //update the balance on the transaction made
                $etransaction->balance = $account->amount;

                break;
           
            default:
                # code...
                break;
        }
        $etransaction->status = $status;

        if($etransaction->isDirty()){
            //getOriginal() -> get original values of model
            //getDirty -> get all fields updated with value
            $changes = $etransaction->getDirty();
            $detail = 'Updated E-Wallet -> ';
            foreach ($changes as $key => $value) {
                $old = $etransaction->getOriginal($key);

                if($key == 'reason') {
                    if(is_null($old)) {
                        $old = 'NULL';
                    }
                    if(is_null($value)) {
                        $value = 'NULL';
                    }
                }

                $detail .= "Change ".$key." from ".$old." to ".$value.". ";
            }
            Common::saveActionLogs('Ewallet', $etransaction->id, $detail);
        }

        $etransaction->save();
        return $account->save();
    }

    public function updateBreakdown() {
        $user = Auth::user();
        $onhold = $user->fee_on_hold;
        $not_usable = $user->ewallet->transactions()
            ->where(function($query){
                $query->where('status','Pending');
                $query->orWhere('status','Processing');
                $query->orWhere('status','On Hold');
            })
            ->where('type','<>','Deposit')
            ->get()
            ->pluck('amount')
            ->sum();
        $breakdown = '<p><b>Ewallet Total : </b> P<span id="eTotal">'.$user->ewallet->amount.'</span></p>';
        $breakdown .= '<p><b>Pending Ewallet Transaction : </b> P<span id="notUsable">'.$not_usable.'</span></p>';
        $breakdown .= '<p><b>Shipment Security Deposit : </b> P<span id="onHold">'.$onhold.'</span></p>';

        return json_encode([
            'breakdown' => $breakdown
        ]);
    }
}
