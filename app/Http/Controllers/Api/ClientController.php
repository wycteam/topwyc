<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Address;
use App\Models\Visa\Deposit;
use App\Models\Visa\Payment;
use App\Models\Visa\Discount;
use App\Models\Visa\Refund;
use App\Models\Visa\Group;
use App\Models\Visa\GroupMember;
use App\Models\Visa\Package;
use App\Models\Visa\Service;
use App\Models\Visa\ClientService;
use App\Models\Visa\Branch;
use App\Models\Visa\ClientTransactions;

use App\Http\Requests\AddTemporaryClientValidation;

use DB;
use Auth;
use App\Classes\Common;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ClientController extends ApiController
{

	public function getVisaClients(Request $request) {
        return User::select(DB::raw("
             		users.id AS id,
                    CONCAT('[', users.id, ']', ' ', users.first_name, ' ', users.last_name) AS name
                "))
        		->where(function($query) use($request) {
        			$query->where('users.first_name', 'LIKE', '%'.$request->q.'%')
        				->orWhere('users.last_name', 'LIKE', '%'.$request->q.'%')
	            		->orWhere('users.id', 'LIKE', '%'.$request->q.'%');	
        		})
                ->whereHas('roles', function($query) {
                    $query->where('role_id', 9);
                })
                ///->where('branch_id', $request->branch)
                ->where(function ($query) use($request){
                        $query->where('branch_id', $request->branch)
                            ->orwhere('branch_id', '3');
                         })
                ->get();
    }

    public function getAvailableVisaClients(Request $request) {
        $group_members = GroupMember::where('group_id', $request->groupId)->pluck('client_id');

        return User::select(DB::raw("
                    users.id AS id,
                    CONCAT('[', users.id, ']', ' ', users.first_name, ' ', users.last_name) AS name
                "))
        		->where(function($query) use($request) {
        			$query->where('users.first_name', 'LIKE', '%'.$request->q.'%')
        				->orWhere('users.last_name', 'LIKE', '%'.$request->q.'%')
	            		->orWhere('users.id', 'LIKE', '%'.$request->q.'%');	
        		})
                // ->where('branch_id', $request->branchId)
                ->where(function ($query) use($request){
                        $query->where('branch_id', $request->branchId)
                            ->orwhere('branch_id', '3');
                         })
                ->whereNotIn('id', $group_members)
                ->whereHas('roles', function($query) {
                    $query->where('role_id', 9);
                })
                //->has('addresses')
                ->get();
    }

    private function checkPackageComplete($pack_id){
        $package_complete = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%complete%')
            ->count();

        return $package_complete;
    }

    private function checkPackagePending($pack_id){
        $package_pending = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%pending%')
            ->count();

        return $package_pending;
    }

    private function checkPackageOnProcess($pack_id){
        $package_onprogress = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%on process%')
            ->count();

        return $package_onprogress;
    }

    private function checkPackageUpdates($pack_id){
        $stat = 0; // empty

        if($this->checkPackageComplete($pack_id)>0){
            $stat = 4; // complete
        }
        if($this->checkPackageOnProcess($pack_id)>0){
            $stat = 1; // process
        }
        if($this->checkPackagePending($pack_id)>0){
            $stat = 2; // pending
        }

        $data = array('status' => $stat);

        DB::connection('visa')
            ->table('packages')
            ->where('tracking', $pack_id)
            ->update($data);
    }

    private function generateTracking($option) {
        Repack:
            $tracking = ($option == 'group') 
                ? Common::generateGroupTrackingNumber(7) 
                : Common::generateIndividualTrackingNumber(7);
            $check_package = Package::where('tracking', $tracking)->count();
        if($check_package > 0) :
            goto Repack;
        endif;

        return $tracking;
    }

    public function transfer(Request $request, $id) {
        if($request->option == 'client-to-group') {
            $groupId = 0;
            $newGroupId = $request->groupId;
        } elseif($request->option == 'group-to-client') {
            $groupId = $request->groupId;
            $newGroupId = 0;
        }
        //\Log::info($groupId);
        //\Log::info($newGroupId);
        $gentracking = null;
        for($i=0; $i<count($request->services); $i++) {
            if($request->packages[$i] == 'Generate new package') {
                if($gentracking == null){
                    $type = ($request->option == 'client-to-group') ? 'group' : 'individual';
                    $tracking = $this->generateTracking($type);
                    $gentracking = $tracking;

                    Package::create([
                        'client_id' => $request->memberId,
                        'group_id' => $newGroupId,
                        'log_date' => Carbon::now()->format('F j, Y, g:i a'),
                        'tracking' => $tracking,
                        'status' => '0'
                    ]);
                }
                else{
                    $tracking = $gentracking;
                }
            } else {
                $tracking = $request->packages[$i];
            }

            $oldtrack = null;
            $getServ = ClientService::where('id', $request->services[$i])
                ->where('client_id', $request->memberId)
                ->where('group_id', $groupId)
                ->first();
            if($getServ){
                $oldtrack = $getServ->tracking;
                $getServ->group_id = $newGroupId;
                $getServ->tracking = $tracking;
                $getServ->save();
            }

            // ClientService::where('id', $request->services[$i])
            //     ->where('client_id', $request->memberId)
            //     ->where('group_id', $groupId)
            //     ->update([
            //         'group_id' => $newGroupId,
            //         'tracking' => $tracking
            //     ]);

            $this->checkPackageUpdates($tracking);
            $this->checkPackageUpdates($oldtrack);
            // Transaction log
                $clientService = ClientService::where('id', $request->services[$i])
                    ->where('client_id', $request->memberId)
                    ->where('group_id', $newGroupId)
                    ->first();
                $cost = $clientService->cost + $clientService->charge + $clientService->tip + $clientService->com_client + $clientService->com_agent;

                $translated = Service::where('id',$clientService->service_is)->first();
                $cnserv =$clientService->detail;
                if($translated){
                    $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                }

                if($request->option == 'client-to-group') {
                    $details = 'Transfer service <strong>' . $clientService->detail . '</strong> to Group Package #<strong>' . $tracking .'</strong> with <strong>Total Service Cost</strong> of ' . $cost;
                    $details_cn = '转移了服务 ' . $cnserv . '到服务包(团体)#' . $tracking .'以及总服务费 Php' . $cost;

                    // Group log
                        $_groupId = $newGroupId; 
                        $_balance = $this->groupBalance($_groupId); 
                } elseif($request->option == 'group-to-client') {
                    $details = 'Transfer service <strong>' . $clientService->detail . '</strong> to Package #<strong>' . $tracking .'</strong> with <strong>Total Service Cost</strong> of ' . $cost;
                    $details_cn = '转移了服务 ' . $cnserv . '到服务包#' . $tracking .'以及总服务费 Php' . $cost;

                    // Group log
                        $_groupId = $request->groupId; 
                        $_balance = $this->groupBalance($_groupId); 
                }

                // Client log
                    $log_data['client_id'] = $request->memberId; 
                    $log_data['group_id'] = 0; 
                    $log_data['service_id'] = $request->services[$i]; 
                    $log_data['tracking'] = $tracking; 
                    $log_data['user_id'] = Auth::user()->id; 
                    $log_data['type'] = 'service'; 
                    $log_data['amount'] = $cost; 
                    $log_data['balance'] = $this->clientBalance($request->memberId); 
                    $log_data['detail'] = $details; 
                    $log_data['detail_cn'] = $details_cn; 
                    Common::saveTransactionLogs($log_data); 

                // Group log
                    $log_data['group_id'] = $_groupId; 
                    $log_data['balance'] = $_balance; 
                    Common::saveTransactionLogs($log_data); 

        }

        return json_encode([
            'success' => true,
            'message' => 'Transfer successfully completed'
        ]);
    }

    private function clientBalance($client_id){
        $balance = ((
            $this->clientDeposit($client_id)
            + $this->clientPayment($client_id)
            +$this->clientDiscount($client_id)
        )-(
            $this->clientRefund($client_id)
            + $this->clientServiceCost($client_id)
        ));

        return $balance;
    }

        private function clientDeposit($client_id){
            return ClientTransactions::where('client_id',$client_id)
                        ->where('group_id',0)
                        ->where('type', 'Deposit')
                        ->sum('amount');
        }

        private function clientPayment($client_id){
            return ClientTransactions::where('client_id',$client_id)
                        ->where('group_id',0)
                        ->where('type', 'Payment')
                        ->sum('amount');
        }

        private function clientDiscount($client_id){
            return ClientTransactions::where('client_id',$client_id)
                        ->where('group_id',0)
                        ->where('type', 'Discount')
                        ->sum('amount');
        }

        private function clientRefund($client_id){
            return ClientTransactions::where('client_id',$client_id)
                        ->where('group_id',0)
                        ->where('type', 'Refund')
                        ->sum('amount');
        }

        private function clientServiceCost($client_id){
            $cost = ClientService::where('client_id',$client_id)
                ->where('group_id', 0)
                ->where('active', 1)
                ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));

            return $cost;
        }

    private function groupBalance($group_id) {
        $balance = (
            (
                (
                    $this->groupDeposit($group_id) 
                    + 
                    $this->groupPayment($group_id) 
                    + 
                    $this->groupDiscount($group_id)
                )
                - 
                $this->groupServiceCost($group_id)
            ) 
            - 
            $this->groupRefund($group_id));

        return $balance;
    }

        private function groupDeposit($group_id) {
            return ClientTransactions::where('group_id', $group_id)->where('type', 'Deposit')->sum('amount');
        }

        private function groupPayment($group_id) {
            return ClientTransactions::where('group_id', $group_id)->where('type', 'Payment')->sum('amount');
        }

        private function groupDiscount($group_id) {
            return ClientTransactions::where('group_id', $group_id)->where('type', 'Discount')->sum('amount');
        }

        private function groupServiceCost($group_id) {
            $cost = ClientService::where('group_id', $group_id)
                ->where('active', 1)
                ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));

            return $cost;
        }

        private function groupRefund($group_id) {
            return ClientTransactions::where('group_id', $group_id)->where('type', 'Refund')->sum('amount');
        }

    public function GetCompleteCost($client_id){
        $cost = ClientService::select(DB::raw('sum(cost+charge+tip + com_client + com_agent) as total_cost'))
            ->where('client_id', '=', $client_id)
            ->where('group_id', 0)
            ->where('active', 1)
            ->where('status', '=', 'complete')
            ->first();

        return $cost->total_cost;
    }

    public function load_client_list(){
        $myfile = fopen(storage_path('/app/public/data/Clients.txt'), 'w') or die('Unable to open file!');
        $txt = "{\n";
        fwrite($myfile, $txt);
        $txt = '"data":[';
        fwrite($myfile, $txt);
        $txt = "\n";
        fwrite($myfile, $txt);
        $count = 0;

        $records = DB::connection('mysql')
            ->table('users as a')
            ->select(DB::raw('
                a.*,
                total.total_amount as total_costs,
                total.service_date,
                total2.total_deposit,
                total4.total_refund,
                total5.total_discount,
                total6.dates, role.role_id,
                srv.sdates,
                (
                    (case when total.total_amount > 0 then total.total_amount else 0 end)
                    - (
                        (
                            case when total2.total_deposit > 0 then total2.total_deposit else 0 end
                            + case when total3.total_payment > 0 then total3.total_payment else 0 end
                            + case when total5.total_discount > 0 then total5.total_discount else 0 end
                        )
                        - case when total4.total_refund > 0 then total4.total_refund else 0 end
                    )
                ) as total_balance,
                total3.total_payment'))
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(sum(b.cost)+sum(b.charge)+sum(b.tip)+sum(b.com_client)+sum(b.com_agent),0) as total_amount,
                        COALESCE(sum(b.tip),0) as total_charge,
                        b.client_id, b.service_date
                        from visa.client_services as b
                        where b.active = 1
                        and b.group_id = 0
                        group by b.client_id
                        order by b.service_date desc
                    ) as total'),
                    'total.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(sum(c.deposit_amount),0) as total_deposit,
                        c.client_id
                        from visa.client_deposits as c
                        where c.group_id = 0
                        group by c.client_id
                    ) as total2'),
                    'total2.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(sum(d.payment_amount),0) as total_payment,
                        d.client_id
                        from visa.client_payments as d
                        where d.group_id = 0
                        group by d.client_id
                    ) as total3'),
                    'total3.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(sum(e.refund_amount),0) as total_refund,
                        e.client_id
                        from visa.client_refunds as e
                        where e.group_id = 0
                        group by e.client_id
                    ) as total4'),
                    'total4.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(sum(f.discount_amount),0) as total_discount,
                        f.client_id
                        from visa.client_discounts as f
                        where f.group_id = 0
                        group by f.client_id
                    ) as total5'),
                    'total5.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select *
                        from role_user as r
                        where r.role_id = 9
                    ) as role'),
                    'role.user_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as dates,
                        x.client_id
                        from( SELECT STR_TO_DATE(log_date, "%M %e, %Y, %l:%i %p") as dates,
                            client_id, status
                            FROM visa.packages
                            ORDER BY dates desc
                        ) as x
                        group by x.client_id) as total6'),
                    'total6.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as sdates,cs.client_id
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM visa.client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.client_id) as srv'),
                    'srv.client_id', '=', 'a.id')
                ->orderBy(DB::raw('
                    (
                        (case when total.total_amount > 0 then total.total_amount else 0 end)
                        - (
                            (
                                case when total2.total_deposit > 0 then total2.total_deposit else 0 end
                                + case when total3.total_payment > 0 then total3.total_payment else 0 end
                                + case when total5.total_discount > 0 then total5.total_discount else 0 end
                            )
                            - case when total4.total_refund > 0 then total4.total_refund else 0 end
                        )
                    )'), 'desc')
                ->where("role.role_id","9")
                ->get();
        //\Log::info($records);
        $collect = 0;

        foreach ($records as $value) {

                $col_balance = 0;
                
                $id = $value->id;

                $total_balance = floatval($value->total_balance);

                if($total_balance>0){
                    // $col_balance = (
                    //     (
                    //         $this->clientDeposit($id)
                    //         + $this->clientPayment($id)
                    //         +$this->clientDiscount($id)
                    //     )
                    //     - (
                    //         $this->clientRefund($id)
                    //         + ($this->clientServiceCost($id))
                    //     )
                    // );
                    $col_balance = (
                        (
                            $value->total_deposit
                            + $value->total_payment
                            + $value->total_discount
                        )
                        - (
                            $value->total_refund
                            + ($this->GetCompleteCost($id))
                        )
                    );
                    if($col_balance > 0){
                        $col_balance = 0;
                    }
                    else{
                        $collect += $col_balance;
                    }
                }

                $count++;
                $txt = "[";
                fwrite($myfile, $txt);
                $txt = '"'.$value->id.'",';
                fwrite($myfile, $txt);
                $txt = '"'.ucwords($value->first_name).' '.ucwords($value->last_name).'",';
                // fwrite($myfile, $txt);

                // $txt = '"'.Common::formatMoney($value->total_costs*1).'",';
                // fwrite($myfile, $txt);

                // $txt = '"'.Common::formatMoney($value->total_deposit*1).'",';
                // fwrite($myfile, $txt);

                // $txt = '"'.(Common::formatMoney($value->total_payment*1)).'",';
                // fwrite($myfile, $txt);

                // $txt = '"'.(Common::formatMoney($value->total_refund*1)).'",';
                // fwrite($myfile, $txt);

                // $txt = '"'.(Common::formatMoney($value->total_discount*1)).'",';
                fwrite($myfile, $txt);

                if($value->total_balance==0){
                $txt = '"'.(Common::formatMoney($value->total_balance)).'",';
                }
                else{
                $txt = '"'.(Common::formatMoney($value->total_balance*-1)).'",';
                }

                fwrite($myfile, $txt);
                if($col_balance==0){
                $txt = '"'.(Common::formatMoney($col_balance)).'",';
                }
                else{
                $txt = '"'.(Common::formatMoney($col_balance*1)).'",';
                }

                fwrite($myfile, $txt);
                $txt = '"'.$value->dates.'",';

                fwrite($myfile, $txt);
                $txt = '"'.$value->sdates.'",';

                fwrite($myfile, $txt);
                $txt = '"<center><a href=\"/visa/client/'.$value->id.'\" target=\"_blank\" ><i class=\"fa fa-arrow-right fa-1x\"></i></a></center>"';
                fwrite($myfile, $txt);
                if(count($records)==$count){
                    $txt = "]\n";
                }else{
                    $txt = "],\n";
                }
                fwrite($myfile, $txt);
            }
            $txt = "\n]\n}";
            fwrite($myfile, $txt);
            fclose($myfile);
            return array(true, $collect);
    }

    public function addTemporaryValidation(Request $request) {
        $contactNumbers =  $request->contact_numbers;

        $ifExistArray = [];
        $ifBindedArray = [];
        $ifGroupLeaderArray = [];
        $userArray = [];
        foreach($contactNumbers as $contactNumber) {
            $initQuery = User::where('contact_number', $contactNumber['country_code'] . $contactNumber['value'])
                   ->orWhere('contact_number', '0' . $contactNumber['value']);

            $ifExist = $initQuery->first();
            $ifExistArray[] = ($ifExist) ? true : false;

            $ifBinded = $initQuery->where(function($query) {
                $query->where('password', '<>', '')->where('password', '<>', null);
            })->first();
            $ifBindedArray[] = ($ifBinded) ? true : false;

            if($ifBinded) {
                $userArray[] = $ifBinded;
            } elseif($ifExist) {
                $userArray[] = $ifExist;
            } else {
                $userArray[] = null;
            }

            if($ifExist) {
                $userId = $ifExist->id;
                $ifGroupLeader = Group::where('leader', $userId)->first();
                $ifGroupLeaderArray[] = ($ifGroupLeader) ? true : false;
            } else {
                $ifGroupLeaderArray[] = false;
            }
        }

        return json_encode([
            'success' => true,
            'ifExistArray' => $ifExistArray,
            'ifBindedArray' => $ifBindedArray,
            'ifGroupLeaderArray' => $ifGroupLeaderArray,
            'userArray' => $userArray
        ]);
    }

    private function _saveTemporaryClient($contactNumber) {
        $user = new User;
        $user->password = bcrypt('0'.$contactNumber['value']);
        $user->first_name = $contactNumber['first_name'];
        $user->last_name = $contactNumber['last_name'];
        $user->passport = $contactNumber['passport'];
        $user->birth_date = $contactNumber['birthdate'];
        $user->gender = $contactNumber['gender'];
        $user->branch_id = $contactNumber['branch'];
        $user->contact_number = $contactNumber['country_code'].$contactNumber['value'];
        $user->save();

        // $address = new Address;
        // $address->addressable_id = $user->id;
        // $address->addressable_type = 'App\Models\User';
        // $address->contact_number = $contactNumber['country_code'].$contactNumber['value'];
        // $address->save();

        $user->assignRole(['0' => 9]);

        // Action logs
        $act_id = $user->id;
        $branchName = Branch::findOrFail($contactNumber['branch'])->name;
        $detail = "Created new client -> " . $contactNumber['first_name'] . ' ' . $contactNumber['last_name'] . ' [' . $branchName . ']';
        Common::saveActionLogs(0, $act_id, $detail);
    }

    public function addTemporary(Request $request) {
        $contactNumbers =  $request->contact_numbers;

        foreach($contactNumbers as $contactNumber) {
            if(!$contactNumber['ifExist']) {
                if($contactNumber['process'] == 'complete') {
                    // Save Client
                    $user = new User;
                    $user->password = bcrypt('0'.$contactNumber['value']);
                    $user->first_name = '0'.$contactNumber['value'];
                    $user->last_name = 'N/A';
                    $user->branch_id = $contactNumber['branch'];
                    $user->contact_number = $contactNumber['country_code'].$contactNumber['value'];
                    $user->save();

                    // $address = new Address;
                    // $address->addressable_id = $user->id;
                    // $address->addressable_type = 'App\Models\User';
                    // $address->contact_number = $contactNumber['country_code'].$contactNumber['value'];
                    // $address->save();

                    $user->assignRole(['0' => 9]);

                    // Action logs
                    $act_id = $user->id;
                    $branchName = Branch::findOrFail($contactNumber['branch'])->name;
                    $detail = "Created new client -> " . '0'.$contactNumber['value'] . ' [' . $branchName . ']';
                    Common::saveActionLogs(0, $act_id, $detail);

                    // SEND PUSH NOTIFICATION HERE
                } elseif($contactNumber['process'] == 'detail') {
                    // Save Temporary Client
                    $this->_saveTemporaryClient($contactNumber);
                }
            } else {
                if($contactNumber['action'] == 'proceed_anyway') {
                    if($contactNumber['process'] == 'complete') {
                        // SEND PUSH NOTIFICATION HERE
                    } elseif($contactNumber['process'] == 'detail') {
                        // Save Temporary Client
                        $this->_saveTemporaryClient($contactNumber);
                    }
                } elseif($contactNumber['action'] == 'use_existing_profile') {
                    // No Action
                }
            }
        }

        return json_encode([
            'success' => true,
            'message' => 'Temporary client/s successfully added.'
        ]);
    }

//     public function addTemporary_original(Request $request) {
//         // if contact number is already binded in the app
//         $contact =$request->contact_numbers;
//         $bindedUsers = [];
//         $ctr =0;
//         foreach($contact as $contactNumber){
//             $checkBind = User::whereHas('addresses', function($query) use($contactNumber) {
//                 $query->where('contact_number', $contactNumber['number']);
//             })
//             ->where(function($query) {
//                 $query->where('password', '<>', '')
//                     ->where('password', '<>', null);
//             })
//             ->where('last_name', '<>', 'N/A')
//             ->first();
//             if($checkBind){
//                 $ur = User::where('id',$checkBind->id)->select('id','first_name','last_name')->first();
//                 $bindedUsers[$ctr]['id'] = $ur->id; 
//                 $bindedUsers[$ctr]['full_name'] = $ur->first_name.' '.$ur->last_name; 
//             }
//             $ctr++;
//         }
// // \Log::info($bindedUsers);
//         if(count($bindedUsers) > 0) {
//             $success = false;
//             $message = 'Contact number/s already binded in the visa app.';
//             return json_encode([
//                 'success' => $success,
//                 'message' => $message,
//                 'bindedUsers' => $bindedUsers
//             ]);
//         }
//         else{
//         $ctr =0;
//         $temporaryClientIds = [];
//             foreach($contact as $contactNumber){
            
//                 // Save Client
//                 $user = new User;
//                 $user->password = bcrypt($contactNumber['number']);
//                 $user->first_name = $contactNumber['number'];
//                 $user->last_name = 'N/A';
//                 $user->save();

//                 $address = new Address;
//                 $address->addressable_id = $user->id;
//                 $address->addressable_type = 'App\Models\User';
//                 $address->contact_number = $contactNumber['number'];
//                 $address->save();

//                 $user->assignRole(['0' => 9]);
                    
//                 $success = true;
//                 $message = 'Temporary client successfully added.';

//                 $temporaryClientIds[] = $user->id;
//                 // Action logs
//                 $act_id = $user->id;
//                 $detail = "Created new client -> " . $contactNumber['number'];
//                 Common::saveActionLogs(0, $act_id, $detail);
//             }
//         }
//         return json_encode([
//             'success' => $success,
//             'message' => $message,
//             'bindedUsers' => $bindedUsers,
//             'temporaryClientIds' => $temporaryClientIds
//         ]);
//     }

}