<?php

namespace App\Http\Controllers\Api;

use App\Models\Schedule;

use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Controllers\ApiController;

class ScheduleController extends ApiController
{
    
	public function updateSchedule(Request $request, $id) {
		$this->validate($request, [
			'scheduleType' => 'required',
			'timeIn' => 'required_if:scheduleType, ==, 1',
			'timeOut' => 'required_if:scheduleType, ==, 1',
			'timeInFrom' => 'required_if:scheduleType, ==, 2',
			'timeInTo' => 'required_if:scheduleType, ==, 2',
		], [
			'timeIn.required_if' => 'The time in field is required when schedule type is Fixed.',
			'timeOut.required_if' => 'The time out field is required when schedule type is Fixed.',
			'timeInFrom.required_if' => 'The time in from field is required when schedule type is Flexi.',
			'timeInTo.required_if' => 'The time in to field is required when schedule type is Flexi.',
		]);

		if($request->scheduleType == 1) {
			$data = [
				'schedule_type_id' => 1,
				'time_in' => Carbon::createFromFormat('g:i A', $request->timeIn)->toDateTimeString(),
				'time_out' => Carbon::createFromFormat('g:i A', $request->timeOut)->toDateTimeString(),
				'time_in_from' => null,
				'time_in_to' => null
			];
		} elseif($request->scheduleType == 2) {
			$data = [
				'schedule_type_id' => 2,
				'time_in' => null,
				'time_out' => null,
				'time_in_from' => Carbon::createFromFormat('g:i A', $request->timeInFrom)->toDateTimeString(),
				'time_in_to' => Carbon::createFromFormat('g:i A', $request->timeInTo)->toDateTimeString()
			];
		} elseif($request->scheduleType == 3) {
			$data = [
				'schedule_type_id' => 3,
				'time_in' => null,
				'time_out' => null,
				'time_in_from' => null,
				'time_in_to' => null
			];
		}

		Schedule::updateOrCreate(
		    ['user_id' => $request->userId],
		    $data
		);

		return json_encode([
			'success' => true,
			'message' => 'Successfully updated employee\'s schedule.'
		]);
	}

}
