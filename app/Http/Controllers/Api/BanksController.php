<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\Shopping\Banks;

class BanksController extends ApiController
{

    public function __construct(Banks $model)
    {
        $this->model = $model;
    }

    public function index(){
    	$data = $this->model::all();

    	$jsonData = $this->createResponseData($data,'success');
    	
    	return $this->setStatusCode(200)
            ->respond($jsonData);
    }
}
