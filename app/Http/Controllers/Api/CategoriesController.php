<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Shopping\Category;
use App\Acme\Filters\CategoryFilters;
use App\Http\Controllers\ApiController;
use App\Http\Requests\CategoryValidation;

class CategoriesController extends ApiController
{
    protected $model;
    protected $filters;
    protected $validation;

    public function __construct(Category $model, CategoryFilters $filters)
    {
        $this->model = $model;
        $this->filters = $filters;
        $this->validation = new CategoryValidation;
    }
}
