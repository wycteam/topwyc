<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\Shopping\Favorites;
use App\Models\Shopping\Product;
use Illuminate\Support\Facades\DB;
use Auth;

class FavoritesController extends ApiController
{

    public function __construct(Favorites $model)
    {
        $this->model = $model;
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();   
            return $next($request);
        });
    }

    public function addtofavorites($id){

        $ret = [];

        $find = Favorites::where([
            ['user_id', '=', $this->user->id],
            ['product_id', '=', $id],
        ])->get();

        $product = Product::find($id);

        if($product->user_id == $this->user->id){
            $ret['error_msg'] = $product->name . ' ' . trans('cart.cannot-be-added');
            $ret['response'] = '2';
        } else {
            if(count($find) == 0){
                $ret['response'] = '1';
                $fav = new Favorites();
                $fav->user_id = $this->user->id;
                $fav->product_id = $id;
                $fav->save();
            }
            else {
                $ret['response'] = '0';
            }            
        }

        return $ret;
    }

    public function checkfavorites($id){

        $ret = [];

        $find = Favorites::where([
            ['user_id', '=', $this->user->id],
            ['product_id', '=', $id],
        ])->get();

        $product = Product::find($id);

        if(count($find) == 0){
            $ret['response'] = '1';
            $ret['fav_id'] = '0';
        }
        else{
            $ret['response'] = '0';
            $ret['fav_id'] = $find[0]->id;
        }
        return $ret;
    }

    public function favorites(){

        $data = $this->model::with('product')
            ->whereUserId($this->user->id)
            ->get();

        $favorites_list = $data->map(
            function($data) {
                return [
                    "id" => $data->id,
                    "product_id" => $data->product_id,
                    "slug" => $data->product->slug,
                    "name" => $data->product->name,
                    "stock" => $data->product->stock,
                    "price" => (is_null($data->product->discount) || $data->product->discount==0 ? $data->product->price : $data->product->sale_price),
                    "fee_included" => $data->product->fee_included,
                    "shipping_fee" => $data->product->shipping_fee, 
                    "charge" => $data->product->charge, 
                    "vat" => $data->product->vat,  
                    "image" => $data->product->main_image
                ];
            }
        );

    	$jsonData = $this->createResponseData($favorites_list,'success');
    	
    	return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function getfavoritestotal(){

        $data = Favorites::where('user_id', '=', $this->user->id)->get();

        return count($data);
    }

    public function deletefavorites($id){

        $data = Favorites::where('id', '=', $id)->delete();

        return $this->getfavoritestotal();
    }
}
