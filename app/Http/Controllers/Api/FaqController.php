<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\Faq;
use Auth;

class FaqController extends ApiController
{

    public function __construct(Faq $model)
    {
        $this->model = $model;
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();   
            return $next($request);
        });
    }

    public function faq(){

        $data = $this->model->all();
        
        $faq_list = $data->map(
            function($data) {
                return [
                    "id" => $data->id,
                    "type" => $data->type,
                    "question" => $data->question,
                    "answer" => $data->answer,
                    "upload_type" => $data->upload_type
                ];
            }
        );

    	$jsonData = $this->createResponseData($faq_list,'success');
    	
    	return $this->setStatusCode(200)
            ->respond($jsonData);
    }
}
