<?php

namespace App\Http\Controllers\Api;

use App\Models\City;
use Illuminate\Http\Request;
use App\Acme\Filters\CityFilters;
use App\Http\Requests\CityValidation;
use App\Http\Controllers\ApiController;

class CitiesController extends ApiController
{
    protected $model;
    protected $filters;
    protected $validation;

    public function __construct(City $model, CityFilters $filters)
    {
        $this->model = $model;
        $this->filters = $filters;
        $this->validation = new CityValidation;
    }
}
