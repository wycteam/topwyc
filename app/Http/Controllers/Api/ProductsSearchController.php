<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\Shopping\Product;
use App\Models\Shopping\Store;
use App\Models\Shopping\Category;
use App\Acme\Filters\UserFilters;

use DB;
use Auth;
use App\Support\Collection;
class ProductsSearchController extends ApiController
{
    public function __construct(Product $model)
    {
        $this->model = $model;
    }

    public function search(Request $request)
    {
        $queryString = '';

        // Get search keyword
        $keyword = strtolower(urldecode($request->get('keyword')));
        
        $category = Category::where('slug', $keyword)->first();

        $data = [];

        // Filter by category
        if($category) {
            if($request->has('type') && $request->get('type') == 'filterByCategory') {
                $treeParent = $category->tree_parent;

                if($treeParent != '') {
                    $treeParentArray = explode("-", $treeParent);

                    $grandParentCategoryId = $treeParentArray[0];
                } else {
                    $grandParentCategoryId = $category->id;
                }

                $relatedCategoriesArray = Category::where('id', $grandParentCategoryId)
                    ->orWhere('tree_parent', $grandParentCategoryId)
                    ->orWhere('tree_parent', 'LIKE', $grandParentCategoryId.'-%')->pluck('id');

                $data = Product::
                        whereHas('categories', function($query) use($relatedCategoriesArray) {
                            $query->whereIn('category_id', $relatedCategoriesArray);
                        });
            } else {
                $categoryId = $category->id;

                $data = Product::whereHas('categories', function($query) use($categoryId) {
                    $query->where('category_id', $categoryId);
                });
            }
            
            $data->where('is_draft','=','0')
                ->where('is_searchable', 1);

            // Sort product ascending and descending
            if($request->has('sort')) {
                if($request->get('sort') == "asc") {
                    $data = $this->filterBySort($data, 'asc');
                }
                if($request->get('sort') == "desc") {
                    $data = $this->filterBySort($data, 'desc');
                }
            }

            $data->orderBy('sold_count','desc')
                ->select(
                    'id'
                    ,'name'
                    ,'cn_name'
                    ,'slug'
                    ,'price'
                    ,'sale_price'
                    ,'shipping_fee'
                    ,'charge'
                    ,'vat'
                    ,'fee_included'
                    ,'discount'
                    ,'brand'
                    ,DB::raw('CONCAT("/shopping/images/product/",id,"/primary-thumbnail.jpg") as image')
                    ,DB::raw('(CASE WHEN sale_price IS NULL THEN price ELSE sale_price END) as orig_price')
                );

            // Filter by related categories
            if($data->count() == 0) {
                $treeParent = $category->tree_parent;

                if($treeParent != '') {
                    $treeParentArray = explode("-", $treeParent);

                    $grandParentCategoryId = $treeParentArray[0];
                } else {
                    $grandParentCategoryId = $category->id;
                }

                $relatedCategoriesArray = Category::where('id', $grandParentCategoryId)
                    ->orWhere('tree_parent', $grandParentCategoryId)
                    ->orWhere('tree_parent', 'LIKE', $grandParentCategoryId.'-%')->pluck('id');

                $data = Product::
                        whereHas('categories', function($query) use($relatedCategoriesArray) {
                            $query->whereIn('category_id', $relatedCategoriesArray);
                        })
                        ->where('is_searchable', 1)
                        ->where('is_draft','=','0');

                // Sort product ascending and descending
                if($request->has('sort')) {
                    if($request->get('sort') == "asc") {
                        $data = $this->filterBySort($data, 'asc');
                    }
                    if($request->get('sort') == "desc") {
                        $data = $this->filterBySort($data, 'desc');
                    }
                }

                $data->orderBy('sold_count','desc')
                        ->select(
                            'id'
                            ,'name'
                            ,'cn_name'
                            ,'slug'
                            ,'price'
                            ,'sale_price'
                            ,'shipping_fee'
                            ,'charge'
                            ,'vat'
                            ,'fee_included'
                            ,'discount'
                            ,'brand'
                            ,DB::raw('CONCAT("/shopping/images/product/",id,"/primary-thumbnail.jpg") as image')
                            ,DB::raw('(CASE WHEN sale_price IS NULL THEN price ELSE sale_price END) as orig_price')
                        );
            }

            $maxPrice = $data->max('price');

            // Filter by min/max price
            if($request->has('minPrice') && $request->has('maxPrice')) {
                $data = $this->filterByMinMaxPrice($data, $request->get('minPrice'), $request->get('maxPrice'));
            }

            // Filter by brand
            if($request->has('brands')) {
                $data = $this->filterByBrand($data, $request->get('brands'));
            }

            $data = $data->paginate(20);

        } 
        // Filter by search textbox
        else {

            // Get product by categories
            $data1 = DB::table('shopping.products as p')
                ->leftJoin('shopping.category_product as pc', 'pc.product_id', '=', 'p.id')
                ->leftJoin('shopping.categories as c', 'c.id', '=', 'pc.category_id')
                ->where(DB::raw('LOWER(c.slug)'),$keyword)
                ->where(DB::raw('p.is_searchable'), 1)
                ->select(
                    'p.id'
                    ,'p.name'
                    ,'p.cn_name'
                    ,'p.slug'
                    ,'p.price'
                    ,'p.sale_price'
                    ,'p.shipping_fee'
                    ,'p.charge'
                    ,'p.vat'
                    ,'p.fee_included'
                    ,'p.discount'
                    ,'p.brand'
                    ,DB::raw('CONCAT("/shopping/images/product/",p.id,"/primary-thumbnail.jpg") as image')
                    ,DB::raw('(CASE WHEN p.sale_price IS NULL THEN p.price ELSE p.sale_price END) as orig_price')
                )
                ->get();

            // Search by most relevance
            $data2  = Product::search($keyword, null, true,true)
                ->where('is_draft','=','0')
                ->where('is_searchable', 1)
                ->orderBy('sold_count','desc')
                ->select(
                    'id'
                    ,'name'
                    ,'cn_name'
                    ,'slug'
                    ,'price'
                    ,'sale_price'
                    ,'shipping_fee'
                    ,'charge'
                    ,'vat'
                    ,'fee_included'
                    ,'discount'
                    ,'brand'
                    ,DB::raw('CONCAT("/shopping/images/product/",id,"/primary-thumbnail.jpg") as image')
                    ,DB::raw('(CASE WHEN sale_price IS NULL THEN price ELSE sale_price END) as orig_price')
                )->get();

            // Search by 2nd most
            $data3 = Product::search($keyword, null, true)
                ->where('is_draft','=','0')
                ->where('is_searchable', 1)
                ->orderBy('sold_count','desc')
                ->select(
                    'id'
                    ,'name'
                    ,'cn_name'
                    ,'slug'
                    ,'price'
                    ,'sale_price'
                    ,'shipping_fee'
                    ,'charge'
                    ,'vat'
                    ,'fee_included'
                    ,'discount'
                    ,'brand'
                    ,DB::raw('CONCAT("/shopping/images/product/",id,"/primary-thumbnail.jpg") as image')
                    ,DB::raw('(CASE WHEN sale_price IS NULL THEN price ELSE sale_price END) as orig_price')
                )->get();

            $allProducts = new Collection();
            // Merge three collections
            $all = ($allProducts->merge($data1)->merge($data2)->merge($data3));

            $maxPrice = $all->max('price');

            // Filter by min/max price
            if($request->has('minPrice') && $request->has('maxPrice')) {
                $all = $this->filterByMinMaxPrice($all, $request->get('minPrice'), $request->get('maxPrice'));
            }

            // Filter by brand
            if($request->has('brands')) {
                $all = $this->filterByBrand($all, $request->get('brands'));
            }

            // Sort product ascending and descending
            if($request->has('sort')) {
                if($request->get('sort') == "asc") {
                    $all = $all->sortBy('price');
                }
                if($request->get('sort') == "desc") {
                    $all = $all->sortByDesc('price');
                }
            }

            // Remove duplicates by id
            $unique = $all->unique('id');
            $data = $unique->values()->paginate(20);
        }

        // When data is still empty, return all recently uploaded products
        if(!count($data)) {
            $data = Product::
                where('is_draft','=','0')
                ->where('is_searchable', 1);

            // Sort product ascending and descending
            if($request->has('sort')) {
                if($request->get('sort') == "asc") {
                    $data = $this->filterBySort($data, 'asc');
                }
                if($request->get('sort') == "desc") {
                    $data = $this->filterBySort($data, 'desc');
                }
            }

            $data->orderBy('sold_count','desc')
                ->select(
                    'id'
                    ,'name'
                    ,'cn_name'
                    ,'slug'
                    ,'price'
                    ,'sale_price'
                    ,'shipping_fee'
                    ,'charge'
                    ,'vat'
                    ,'fee_included'
                    ,'discount'
                    ,'brand'
                    ,DB::raw('CONCAT("/shopping/images/product/",id,"/primary-thumbnail.jpg") as image')
                    ,DB::raw('(CASE WHEN sale_price IS NULL THEN price ELSE sale_price END) as orig_price')
                );

            $maxPrice = $data->max('price');
            
            // Filter by min/max price
            if($request->has('minPrice') && $request->has('maxPrice')) {
                $data = $this->filterByMinMaxPrice($data, $request->get('minPrice'), $request->get('maxPrice'));
            }

            // Filter by brand
            if($request->has('brands')) {
                $data = $this->filterByBrand($data, $request->get('brands'));
            }

            $data = $data->paginate(20);

            $queryString = $request->get('keyword');
        }

        $result = [
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
                'to' => $data->lastItem()
            ],
            'data' => $data,
            'maxPrice' => $maxPrice,
            'queryString' => $queryString
        ];

        return $this->setStatusCode(200)->respond($result);

        /** END HERE **/

        //get product by categories
        /*
        $checkCats =DB::table('shopping.products as p')
            ->leftJoin('shopping.category_product as pc', 'pc.product_id', '=', 'p.id')
            ->leftJoin('shopping.categories as c', 'c.id', '=', 'pc.category_id')
            ->Where(DB::raw('LOWER(c.name)'),'like',$keyword.'%')
            ->select(
                'p.id'
                ,'p.name'
                ,'p.cn_name'
                ,'p.slug'
                ,'p.price'
                ,'p.sale_price'
                ,'p.discount'
                ,'p.brand'
                ,DB::raw('CONCAT("/shopping/images/product/",p.id,"/primary-thumbnail.jpg") as image')
                ,DB::raw('(CASE WHEN p.sale_price IS NULL THEN p.price ELSE p.sale_price END) as orig_price')
            )
            ->get();
        $allprods = new Collection();
        $cats = new Collection();
        foreach($checkCats as $c){
            $cats[] = Product::where('id','='.$c->id)->where('is_draft','=','0')->first();
        }

        //search by most relevance
        $data  = Product::search($keyword, null, true,true)
                    ->where('is_draft','=','0')
                    ->select(
                        'id'
                        ,'name'
                        ,'cn_name'
                        ,'slug'
                        ,'price'
                        ,'sale_price'
                        ,'discount'
                        ,'brand'
                        ,DB::raw('CONCAT("/shopping/images/product/",id,"/primary-thumbnail.jpg") as image')
                        ,DB::raw('(CASE WHEN sale_price IS NULL THEN price ELSE sale_price END) as orig_price')
                    )->get();

        //search by 2nd most
        $data2 = Product::search($keyword, null, true)
                    ->where('is_draft','=','0')
                    ->select(
                        'id'
                        ,'name'
                        ,'cn_name'
                        ,'slug'
                        ,'price'
                        ,'sale_price'
                        ,'discount'
                        ,'brand'
                        ,DB::raw('CONCAT("/shopping/images/product/",id,"/primary-thumbnail.jpg") as image')
                        ,DB::raw('(CASE WHEN sale_price IS NULL THEN price ELSE sale_price END) as orig_price')
                    )->get();

        //mrege three collections
        $all =($allprods->merge($data)->merge($data2)->merge($checkCats));

        if($request->has('minPrice') && $request->has('maxPrice')) {
            $maxPrice = $all->max('price');

            $all = $all
                ->where('price', '>=', $request->get('minPrice'))
                ->where('price', '<=', $request->get('maxPrice'));
        } else {
            $maxPrice = $all->max('price');
        }

        if($request->has('brands')) {
            $brandsArray = explode("--", $request->get('brands'));

            $all = $all->whereIn('brand', $brandsArray);
        }

        //remove duplicates by id
        $unique = $all->unique('id');
        $unique->values()->all();

        //sort product ascending and descending
        if($request->has('sort')){
            // if($checkCats)
            $unique = $this->objectToArray($unique);
            if($request->get('sort')=="asc")
                $unique = $this->order_array_num($unique,'orig_price','ASC');
            if($request->get('sort')=="desc")
                $unique = $this->order_array_num($unique,'orig_price','DESC');
        }

        $allprods = new Collection();

        $unique =($allprods->merge($unique));

        $data = $unique->paginate(20);

        if($data->count() < 1) {
            $category = Category::where('name', 'LIKE', $keyword.'%')->first();

            if($category) {
                $treeParent = $category->tree_parent;

                if($treeParent != '') {
                    $treeParentArray = explode("-", $treeParent);

                    $grandParentCategoryId = $treeParentArray[0];
                } else {
                    $grandParentCategoryId = $category->id;
                }

                $relatedCategoriesArray = Category::where('id', $grandParentCategoryId)
                    ->orWhere('tree_parent', $grandParentCategoryId)
                    ->orWhere('tree_parent', 'LIKE', $grandParentCategoryId.'-%')->pluck('id');

                $data = Product::
                        //search("", null, true)
                        wherehas('categories', function($query) use($relatedCategoriesArray) {
                            $query->whereIn('category_id', $relatedCategoriesArray);
                        })
                        ->where('is_draft','=','0')
                        ->orderBy('sold_count','desc')
                        ->select(
                            'id'
                            ,'name'
                            ,'cn_name'
                            ,'slug'
                            ,'price'
                            ,'sale_price'
                            ,'discount'
                            ,'brand'
                            ,DB::raw('CONCAT("/shopping/images/product/",id,"/primary-thumbnail.jpg") as image')
                            ,DB::raw('(CASE WHEN sale_price IS NULL THEN price ELSE sale_price END) as orig_price')
                        )->get();
            }

            if(!$category || !count($data)) {
                $data = Product::where('is_draft','=','0')
                        ->orderBy('sold_count','desc')
                        ->select(
                            'id'
                            ,'name'
                            ,'cn_name'
                            ,'slug'
                            ,'price'
                            ,'sale_price'
                            ,'discount'
                            ,'brand'
                            ,DB::raw('CONCAT("/shopping/images/product/",id,"/primary-thumbnail.jpg") as image')
                            ,DB::raw('(CASE WHEN sale_price IS NULL THEN price ELSE sale_price END) as orig_price')
                        )->get();
            }
                
            if($request->has('minPrice') && $request->has('maxPrice')) {
                $maxPrice = $data->max('price');
                $data = $data
                    ->where('price', '>=', $request->get('minPrice'))
                    ->where('price', '<=', $request->get('maxPrice'));
            } else {
                $maxPrice = $data->max('price');
            }

            if($request->has('brands')) {
                $brandsArray = explode("--", $request->get('brands'));

                $data = $data->whereIn('brand', $brandsArray);
            }

            //sort product ascending and descending
            if($request->has('sort')){
                if($request->get('sort')=="asc")
                    $data = $this->order_array_num($data,'orig_price','ASC');
                if($request->get('sort')=="desc")
                    $data = $this->order_array_num($data,'orig_price','DESC');
            }

            $allprods = new Collection();

            $all =($allprods->merge($data));

            //remove duplicates by id
            $unique = $all->unique('id');
            $unique->values()->all();

            $data = $unique->paginate(20);
        }

        $result = [
            'pagination' => [
                'total' => $data->total(),
                'per_page' => $data->perPage(),
                'current_page' => $data->currentPage(),
                'last_page' => $data->lastPage(),
                'from' => $data->firstItem(),
                'to' => $data->lastItem()
            ],
            'data' => $data,
            'maxPrice' => $maxPrice
        ];

        return $this->setStatusCode(200)
            ->respond($result);
        */
    }

    public function order_array_num($array, $key, $order = "ASC") 
    { 
        $tmp = array(); 
        foreach($array as $akey => $array2) 
        { 
            $tmp[$akey] = $array2[$key]; 
        } 
        
        if($order == "DESC") 
        {arsort($tmp , SORT_NUMERIC );} 
        else 
        {asort($tmp , SORT_NUMERIC );} 

        $tmp2 = array();        
        foreach($tmp as $key => $value) 
        { 
            $tmp2[$key] = $array[$key]; 
        }        
        
        return $tmp2; 
    }

    public function objectToArray($o)
    {
        $a = array();
        foreach ($o as $k => $v)
            $a[$k] = (is_array($v) || is_object($v)) ? $this->objectToArray($v): $v;

        return $a;
    }

    private function getMaxPrice($data) {
        return $data->max('price');
    }

    private function filterByMinMaxPrice($data, $min, $max) {
        return $data->where('price', '>=', $min)->where('price', '<=', $max);
    }

    private function filterByBrand($data, $brand) {
        $brandsArray = explode("--", $brand);

        return $data->whereIn('brand', $brandsArray);
    }

    private function filterBySort($data, $sort) {
        if($sort == "asc")
            return $data->orderBy('price');
        if($sort == "desc")
            return $data->orderBy('price', 'desc');
    }

}
