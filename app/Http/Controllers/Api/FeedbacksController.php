<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Shopping\Feedbacks;
use App\Notifications\NewFeedback;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Acme\Filters\FeedbackFilters;
use Intervention\Image\Facades\Image;
use App\Http\Controllers\ApiController;
use App\Notifications\FeedbackApproved;
use App\Http\Requests\FeedbackValidation;
use Illuminate\Support\Facades\Notification;

use App\Classes\Common;

class FeedbacksController extends ApiController
{
    public function __construct(Feedbacks $model,FeedbackFilters $filters)
    {
        $this->model = $model;
        $this->filters = $filters;
        $this->validation = new FeedbackValidation;
    }

    public function index(){
        $query = $this->filters?$this->model->filter($this->filters):$this->model;
        $data = $query
            ->whereParentId(0)
            ->get();

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function count(Request $request){
        if($request->has('approved'))
        {
            $approved = $request->get('approved');
            $data = $this->model
                ->whereParentId(0)
                ->whereApproved($approved)
                ->count();
            return $data;
        }
    }

    public function store(Request $request)
    {
        app(get_class($this->validation));
        $data = $this->model->create($request->all());
        if($request->has('imageTemp'))
        {
            $this->saveImage($request->get('imageTemp'), $data->image_path, $data, 'feedback');
        }

        if (! $data->parent_id) {
            $authUser = Auth::user();
            $adminUsers = User::whereHas('roles', function ($query) {
                $query->where('name', 'master')
                    ->orWhere('name', 'cpanel-admin');
            })->get();

            Notification::send($adminUsers, new NewFeedback($data, $authUser));
        }else
        {
            //send notification to the creator of the feedback
            
        }

        $data = $this->model->find($data->id);

        return $this->respondCreated($data);
    }

    public function update(Request $request, $id)
    {
        $data = $this->model->findOrFail($id);

        $data->fill($request->all());

        if($data->isDirty()){
            //getOriginal() -> get original values of model
            //getDirty -> get all fields updated with value
            $changes = $data->getDirty();
            $detail = 'Updated Feedback -> ';
            foreach ($changes as $key => $value) {
                $old = (is_null($data->getOriginal($key))) ? 'NULL' : $data->getOriginal($key);

                if($key == 'approved') {
                    if($old == 0) { $old = 'pending'; } 
                    elseif($old == 1) { $old = 'approved'; } 
                    elseif($old == 2) { $old = 'rejected'; }

                    if($value == 0) { $value = 'pending'; } 
                    elseif($value == 1) { $value = 'approved'; } 
                    elseif($value == 2) { $value = 'rejected'; }
                }

                if($key == 'processor_id') {
                    $key = 'processor';

                    $user = User::find($value);
                    $value = $user->first_name . ' ' . $user->last_name;
                }

                $detail .= "Change ".$key." from ".$old." to ".$value.". ";
            }
            Common::saveActionLogs('Feedback', $id, $detail);
        }

        $data->save();

        $values = $this->model->with('user')->findOrFail($id);
        return $this->respondUpdated($values);
    }

    private function saveImage($image, $path, $data, $type)
    {
        $fileName = $type . '.jpg';

        Image::make($image)
            ->encode('jpg', 100)
            ->save($path . $fileName);

        $data->images()->create([
            'name' => $fileName,
            'type' => $type,
        ]);
    }

    public function approve(Request $request, Feedbacks $feedback)
    {
        $data = DB::transaction(function () use ($request, $feedback) {
            $user = Auth::user();

            $feedback->processor_id = $user->id;
            $feedback->approved = true;
            $feedback->save();

            if ($notif = $user->unreadNotifications()->find($request->get('notification_id'))) {
                $notif->markAsRead();
            }

            $feedback->user->notify(new FeedbackApproved($feedback, $user));

            return $notif;
        });

        return $this->respondUpdated($data);
    }

    public function reject(Request $request, Feedbacks $feedback)
    {
        $user = Auth::user();

        if ($notif = $user->unreadNotifications()->find($request->get('notification_id'))) {
            $notif->markAsRead();
        }

        return $this->respondUpdated($notif);
    }
}
