<?php

namespace App\Http\Controllers\Api\Employee;

use DB;
use App\Models\User;
use App\Models\Address;
use App\Models\Employee\Attendance;
use Illuminate\Http\Request;
use App\Acme\Filters\UserFilters;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserValidation;
use App\Models\Shopping\Friends;
use App\Http\Controllers\ApiController;
use Carbon\Carbon;

class EmployeeController extends ApiController
{
    protected $model;
    protected $filters;
    protected $validation;

    public function __construct(User $model)
    {
        $this->model = $model;
        $this->validation = new UserValidation;
    }

    public function index()
    {
        $query = $this->model::with('roles', 'schedule.scheduleType');
            $data = $query->whereHas('roles', function ($query) {
                $query->where('name', 'employee');
            })->whereHas('schedule', function ($query) {
                $query->where('schedule_type_id', '!=','3');
            })->where('is_active',1)->get();

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function attCurrentMonth()
    {
        $now = Carbon::now('Asia/Manila');
        $cyear = $now->year;
        $cmonth = $now->month;
        $cweekyear = $now->weekOfYear;
        $totalDays = $now->daysInMonth;
        // $query = $this->model::with('roles', 'schedule.scheduleType');
        $employees = $this->model->whereHas('roles', function ($query) {
                    $query->where('name', 'employee');
                })->whereHas('schedule', function ($query) {
                    $query->where('schedule_type_id', '!=','3');
                })->where('is_active',1)->orderBy('last_name')->pluck('id');
        $data = [];

        $data['cyear'] = $cyear;
        $data['cmonth'] = $cmonth;
        $data['totalDays'] = $totalDays;
        foreach($employees as $d){
            $emp = User::where('id',$d)->select('id','last_name','first_name')->first();
            $emp = $emp->makeHidden(['permissions']);
            $data['sched'][]['name'] = $emp->last_name.", ".$emp->first_name;
            // $emp = $emp->makeHidden('address');
        }


        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function attendance(Request $request) {
        $users = User::whereHas('roles', function($query) {
                $query->where('name', 'employee');
            })
            ->whereHas('schedule', function($query) {
                $query->where('schedule_type_id', '!=', '3');
            })
            ->join('department_user', 'department_user.user_id', '=', 'users.id')
            ->select('users.*', 'department_user.*')
            ->orderBy('department_user.department_id', 'ASC')
            ->orderBy('last_name', 'ASC')
            ->get()->makeHidden('permissions');
            // \Log::info($users);

        $employees = [];
        for($i=0; $i<count($users); $i++) {
            $employees[$i]['id'] = $users[$i]->id;
            $employees[$i]['first_name'] = $users[$i]->first_name;
            $employees[$i]['last_name'] = $users[$i]->last_name;
            $employees[$i]['department'] = $users[$i]->department_id;

            $attendanceArray = [];
            for($j=0; $j<$request->numberOfDays; $j++) {
                $attendance = Attendance::where('user_id', $users[$i]->id)
                    ->where('month', str_pad($request->filterMonth, 2, '0', STR_PAD_LEFT))
                    ->where('day', $j+1)
                    ->where('year', $request->filterYear)
                    ->first();

                $attendanceArray[$j]['day'] = $j+1;

                if($attendance) {
                    $attendanceArray[$j]['has_data'] = true;
                    $attendanceArray[$j]['time_in'] = $attendance->time_in;
                    $attendanceArray[$j]['time_in_status'] = $attendance->timein_status;
                    $attendanceArray[$j]['time_out'] = $attendance->time_out;
                    $attendanceArray[$j]['time_out_status'] = $attendance->timeout_status;
                } else {
                    $attendanceArray[$j]['has_data'] = false;
                    $attendanceArray[$j]['time_in'] = null;
                    $attendanceArray[$j]['time_in_status'] = null;
                    $attendanceArray[$j]['time_out'] = null;
                    $attendanceArray[$j]['time_out_status'] = null;
                }
            }

            $employees[$i]['attendance'] = $attendanceArray;
        }

        return json_encode([
            'employees' => $employees
        ]);
    }

    public function employeeAttendance(Request $request, $id) {
        $attendance = [];
        $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        for($i=0; $i<12; $i++) {
            $data = [];

            for($j=0; $j<31; $j++) {

                $a = Attendance::where('user_id', $id)
                    ->where('month', $i+1) 
                    ->where('day', $j+1)
                    ->where('year', $request->filterYear)
                    ->first();

                if( date('n') == ($i+1) && date('j') == ($j+1) && date('Y') == $request->filterYear ) {
                    $data[$j]['current'] = true;
                } else {
                    $data[$j]['current'] = false;
                }

                if($a) {
                    $data[$j]['has_data'] = true;
                    $data[$j]['time_in'] = $a->time_in;
                    $data[$j]['time_in_status'] = $a->timein_status;
                    $data[$j]['time_out'] = $a->time_out;
                    $data[$j]['time_out_status'] = $a->timeout_status;
                } else {
                    $data[$j]['has_data'] = false;
                    $data[$j]['time_in'] = null;
                    $data[$j]['time_in_status'] = null;
                    $data[$j]['time_out'] = null;
                    $data[$j]['time_out_status'] = null;
                }
            }
            $attendance[$i]['month'] = $months[$i];
            $attendance[$i]['data'] = $data;
        }

        return json_encode([
            'attendance' => $attendance
        ]);
    }

    //////////////////
    //user functions//
    //////////////////  

    public function roles(Request $request, User $user)
    {
        return $this->setStatusCode(200)
            ->respond($user->roles);
    }

    public function permissions(Request $request, User $user)
    {
        return $this->setStatusCode(200)
            ->respond($user->permissions);
    }

}