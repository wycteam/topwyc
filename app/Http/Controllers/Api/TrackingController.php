<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\Visa\ClientService;
use App\Models\Visa\Deposit;
use App\Models\Visa\Payment;
use App\Models\Visa\Refund;
use App\Models\Visa\Discount;
use App\Models\Visa\Group;
use App\Models\Visa\Report;

use Auth, DB;

class TrackingController extends ApiController
{

    public function __construct(ClientService $model)
    {
        $this->model = $model;
    }

    public function service($id) {
        
        $data['tracking'] = $id;

        $services = $this->model::with('user','discount','report')->where('tracking', strtoupper($id))
            ->orderBy('id', 'desc')
            ->get();
        //\Log::info($services);

        $data['services'] = $services;

        if(count($services)!=0){

            $data['client'] = $services[0]->user->first_name . ' ' . $services[0]->user->last_name;

            $package_cost = $this->model::select(DB::raw('sum(cost+charge+tip) as total'))
                ->where('tracking', strtoupper($id))
                ->where('active', 1)
                ->get();

            $data['package_cost'] = $package_cost[0]->total;

            $package_deposit = Deposit::
                where('group_id', $services[0]->user->group_id)
                ->where('tracking', strtoupper($id))          
                ->sum('deposit_amount');

            $data['package_deposit'] = $package_deposit;

            $package_payment = Payment::
                where('group_id', $services[0]->user->group_id)
                ->where('tracking', strtoupper($id))       
                ->sum('payment_amount');

            $data['package_payment'] = $package_payment;

            $package_refund = Refund::
                where('group_id', $services[0]->user->group_id)
                ->where('tracking', strtoupper($id))       
                ->sum('refund_amount');

            $data['package_refund'] = $package_refund;

            $package_discount = Discount::
                where('group_id', $services[0]->user->group_id)
                ->where('tracking', strtoupper($id))       
                ->sum('discount_amount');

            $data['package_discount'] = $package_discount;

        }


        return $data;
    }

    public function group($id) {
        $t = strtoupper($id);
        $groups = Group::with('groupmember.client','groupmember.services.discount','groupmember.services.report')->where('tracking', $t)->get();

        $data['groups'] = $groups;

        if(count($groups)!=0){

            $totalcost = $this->model::
            select(DB::raw('sum(cost+charge+tip) as total'))
                ->where('group_id', $groups[0]->id)
                ->where('active', 1)
                ->get();
            
            $data['total_cost'] = $totalcost[0]->total;

            $package_deposit = Deposit::
                where('group_id', $groups[0]->id)         
                ->sum('deposit_amount');

            $data['package_deposit'] = $package_deposit;

            $package_payment = Payment::
                where('group_id', $groups[0]->id)      
                ->sum('payment_amount');

            $data['package_payment'] = $package_payment;

            $package_refund = Refund::
                where('group_id', $groups[0]->id)      
                ->sum('refund_amount');

            $data['package_refund'] = $package_refund;

            $package_discount = Discount::
                where('group_id', $groups[0]->id)      
                ->sum('discount_amount');

            $data['package_discount'] = $package_discount;

        }

        return $data;
    }

    public function report($id) {

        $data = Report::where('service_id', $id)->get();

        return $data;
    }

}
