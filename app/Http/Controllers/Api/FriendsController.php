<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Relationship;
use Illuminate\Http\Request;
use App\Models\Shopping\Friends;
use App\Models\DatabaseNotification;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use App\Http\Requests\FriendValidation;
use App\Notifications\NewFriendRequest;
use App\Notifications\FriendRequestAccepted;

class FriendsController extends ApiController
{
    public function __construct(Friends $model)
    {
        $this->model = $model;
        $this->validation = new FriendValidation;
    }

    public function index(){

    	$data = [
	    	 'friends' 		=> $this->model->getByStatus('Friend')
	    	,'requests' 	=> $this->model->getByStatus('Request')
	    	,'blocked' 		=> $this->model->getByStatus('Blocked')
	    	,'restricted' 	=> $this->model->getByStatus('Restricted')
    	];
        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function store(Request $request)
    {
        app(get_class($this->validation));

        $authUser = Auth::user();
        $toUser = User::findOrFail($request->get('user_id2'));


        if(!$this->hasRelationship($authUser->id, $toUser->id))
        {
            $data = $this->model->create([
                'user_id' => $authUser->id,
                'user_id2' => $toUser->id,
            ]);

            $toUser->notify(new NewFriendRequest($data, $authUser));

            return $this->respondCreated($data);
        }else
        {
            return $this->respondWithErrors("Invalid Request");
        }
    }

    private function hasRelationship($id,$id2){
        $rel1 = $this->model
            ->where('user_id',$id)
            ->where('user_id2',$id2)
            ->get()->count();

        $rel2 = $this->model
            ->where('user_id',$id2)
            ->where('user_id2',$id)
            ->get()->count();

        return $rel1+$rel2;

    }

    public function getFriendId(Request $requests){
    	$friend = $this->model->where(function($query) use ($requests){
    		$query
    			->where('user_id',$requests->get('user_id'))
    			->where('user_id2',$requests->get('user_id2'));
    	})->orWhere(function($query) use ($requests){
    		$query
    			->where('user_id2',$requests->get('user_id'))
    			->where('user_id',$requests->get('user_id2'));
    	})->first();

    	return $this->setStatusCode(200)
            ->respond($friend->id);
    }

    public function getFriends()
    {
        $user = Auth::user();

        // return $this->setStatusCode(200)
        //     ->respond($user->relationships());

        return $this->setStatusCode(200)
            ->respond($user->friends2());
    }

    public function getNotifications(Request $request)
    {
        $user = Auth::user();

        $data = [];

        $data['notifications'] = $user->friend_request_notifications()
            ->latest()
            ->get();

        $data['notseen_count'] = $user->friend_request_notifications()->where('seen_at', null)->count();

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function markAllAsSeen(Request $request)
    {
        $user = Auth::user();

        $notificationIds = $user->friend_request_notifications()
            ->where('seen_at', null)
            ->get()
                ->pluck('id')
                ->toArray();

        $data = DatabaseNotification::whereIn('id', $notificationIds)
            ->update(['seen_at' => Carbon::now()]);

        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function confirm(Request $request, $id)
    {
        $data = $this->model->findOrFail($id);
        $userId = $data->user_id;
        $userId2 = $data->user_id2;

        $authUser = Auth::user();
        $toUser = User::findOrFail(($userId == $authUser->id) ? $userId2 : $userId);

        $data->fill([
            'user_id' => $authUser->id,
            'user_id2' => ($userId2 == $authUser->id) ? $userId : $userId2,
            'status' => 'Friend',
        ])->save();

        if ($notifId = $request->get('notif_id')) {
            $this->deleteNotification($notifId);
        }

        $toUser->notify(new FriendRequestAccepted($authUser));

        return $this->respondUpdated($data);
    }

    public function deleteRequest(Request $request, $id)
    {
        $data = $this->model->findOrFail($id);
        $data->delete();

        if ($notifId = $request->get('notif_id')) {
            $this->deleteNotification($notifId);
        }

        return $this->respondDeleted($data);
    }

    public function block(Request $request)
    {
        $authUser = Auth::user();

        $data = Friends::where(function ($query) use ($request, $authUser) {
            $query->where(function ($query) use ($request, $authUser) {
                $query->where('user_id', $authUser->id)
                    ->where('user_id2', $request->get('user_id2'));
            })->orWhere(function ($query) use ($request, $authUser) {
                $query->where('user_id', $request->get('user_id2'))
                    ->where('user_id2', $authUser->id);
            });
        })->first();

        if ($data) {
            $userId = $data->user_id;
            $userId2 = $data->user_id2;

            $data->fill([
                'user_id' => $authUser->id,
                'user_id2' => ($userId2 == $authUser->id) ? $userId : $userId2,
                'status' => 'Blocked',
            ])->save();
        } else {
            $data = Friends::create([
                'user_id' => $authUser->id,
                'user_id2' => $request->get('user_id2'),
                'status' => 'Blocked',
            ]);
        }

        return $this->respondUpdated($data);
    }

    ///////////////////////
    // Private Functions //
    ///////////////////////

    private function deleteNotification($notifId)
    {
        $notif = DatabaseNotification::findOrFail($notifId);
        $notif->delete();

        return $notif;
    }
}
