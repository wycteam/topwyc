<?php

namespace App\Http\Controllers\Api;

use DB;
use App\Models\User;
use App\Models\Issue;
use Illuminate\Http\Request;
use App\Notifications\NewIssue;
use App\Acme\Filters\IssueFilters;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\IssueValidation;
use App\Http\Requests\IssueFeedbackValidation;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Notification;

class IssuesController extends ApiController
{
    protected $model;
    protected $filters;
    protected $validation;

    public function __construct(Issue $model, IssueFilters $filters)
    {
        $this->model = $model;
        $this->filters = $filters;
        $this->validation = new IssueValidation;
    }

    public function index()
    {
        $query = $this->model->filter($this->filters);
        $data = $query->paginate(request()->get('limit', 25));
        return $this->setStatusCode(200)
            ->respond($data);
    }

    public function store(Request $request)
    {
        app(get_class($this->validation));

        $data = $this->model->create(array_merge($request->all(), ['user_id' => Auth::user()->id]));

        // For reopening ticket
        if($request->chatRoomId) {
            // Get Chat Room ID
            $chatRoomId = $request->chatRoomId;
            // Get all current customer supports
            $attendees = DB::table('chat_room_user')->where('chat_room_id', $chatRoomId)->pluck('user_id');

            // Get all available customer supports
            $availableUsers = User::whereHas('roles', function($query) {
                    $query->where('role_id', 5); // Support role only
                })
                ->whereNotIn('id', $attendees)->pluck('id')->toArray();
            // Get random new customer support ID
            $newCustomerSupportId = $availableUsers[array_rand($availableUsers)];

            // Insert in chat_rooms and chat_room_user table
            $issue = Issue::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
            $issue->assignSupportToChatRoom($newCustomerSupportId);
        }

        // Auto assign of customer support when reporting a product
        if($request->formAction && $request->formAction == 'report') {
            // Get all available customer supports
            $availableUsers = User::whereHas('roles', function($query) {
                    $query->where('role_id', 5); // Support role only
                })
                ->pluck('id')->toArray();

            // Get random new customer support ID
            $newCustomerSupportId = $availableUsers[array_rand($availableUsers)];

            // Insert in chat_rooms and chat_room_user table
            $issue = Issue::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
            $issue->assignSupportToChatRoom($newCustomerSupportId);
        }

        $this->sendNotification($data);

        return $this->respondCreated($data);
    }

    public function update(Request $request, $issue_number)
    {
        app(get_class($this->validation));

        $issue = $this->model->where('issue_number',$request->get('case_number'))->first();
        $issue->body = $request->get('body');
        $issue->save();

        // $orderDetail->product->user->notify(new OrderReturn($orderDetail, Auth::user()));

        $this->sendNotification($issue);

        return $this->respondCreated($issue);
    }

    public function getIssueStatus($cn)
    {
        $issue =$this->model::where('issue_number', $cn)->get();
        return $issue;
    }

    public function getIssues($status)
    {
        if($status=='1')
            $issue =$this->model::with('chat_room')->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
        else 
            $issue =$this->model::with('chat_room')->where('user_id', Auth::user()->id)->where('status', $status)->orderBy('id', 'desc')->get();
        return $issue;
    }

    public function saveIssueFeedback(IssueFeedbackValidation $request, $cn)
    {
        $issue =$this->model::where('issue_number', $cn)->first();
        $issue->case_solved = $request->get('case_solved');
        $issue->rate_agent = $request->get('rate_agent');
        $issue->comments = $request->get('comments');
        $issue->save();       
    } 

    ///////////////////////
    // Private Functions //
    ///////////////////////

    private function sendNotification($data)
    {
        $cpanelUsers = User::whereHas('roles', function ($query) {
            $query->where('name', 'master')
                ->orWhere('name', 'vice-master');
                // ->orWhere('name', 'cpanel-admin')
                // ->orWhere('name', 'cpanel-user')
                // ->orWhere('name', 'support');
        })->get();

        Notification::send($cpanelUsers, new NewIssue($data, Auth::user()));
    }
}
