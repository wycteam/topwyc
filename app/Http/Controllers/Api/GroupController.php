<?php

namespace App\Http\Controllers\Api;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use App\Models\Visa\BudgetOutput as BudgetOutput;
use App\Classes\Common;
use App\Models\User;
use App\Models\Role;
use App\Models\UserOpen;
use App\Models\ShopRate;
use App\Models\Visa\Group as Group;
use App\Models\Visa\GroupMember;
use App\Models\Visa\Package;
use App\Models\Visa\Deposit;
use App\Models\Visa\Discount;
use App\Models\Visa\Payment;
use App\Models\Visa\Refund;
use App\Models\Visa\ClientService;
use App\Models\Visa\Service;
use App\Models\Visa\ServiceProfiles;
use App\Models\Visa\ServiceProfileCost;
use App\Models\Visa\ServiceBranchCost;
use App\Models\Visa\ServiceDocuments;
use App\Models\Visa\Report;
use App\Models\Visa\Task;
use App\Models\Visa\TransferBalance;
use App\Models\Visa\Branch;
use App\Models\Visa\ClientTransactions;
use App\Models\Visa\DocumentLogs;

use App\Models\Visa\ActionLogs;
use App\Models\Visa\TransactionLogs;
use App\Models\Visa\CommissionLogs;
use App\Http\Controllers\Cpanel\TasksController;

use App\Models\Address;
use App\Http\Requests\GroupValidation;
use App\Http\Requests\AddGroupMemberValidation;
use App\Http\Requests\AddGroupServiceValidation;
use App\Http\Requests\AddFundsValidation;
use App\Http\Requests\EditAddressValidation;
use App\Http\Requests\EditServiceValidation;
use App\Models\Visa\Finance as Finance;

use App\Http\Controllers\Cpanel\SynchronizationController;
use App\Http\Controllers\Cpanel\DocumentLogsController;

class GroupController extends ApiController
{

	protected $validation;

    public function __construct(Group $model)
    {
        $this->model = $model;
        $this->validation = new GroupValidation;
    }

    public function store(Request $request) {
    	app(get_class($this->validation));

        $group = Group::create([
            'name' => $request->groupName,
            'leader' => $request->leader,
            'address' => User::findOrFail($request->leader)->address,
            'tracking' => $this->generateGroupTracking(),
            'branch_id' => $request->branch
        ]);

        $group->groupmember()->create([
            'client_id' => $request->leader,
            'leader' => 1
        ]);

        // Action Log
        $branchName = Branch::findOrFail($request->branch)->name;
        $detail = "Created new group -> " . $request->groupName . ' [' . $branchName . ']';
        Common::saveActionLogs(0, 0, $detail, $group->id);

        // Address::where('addressable_id', $request->leader)
        //     ->where('addressable_type', 'App\Models\User')
        //     ->update([
        //         'address' => $request->address,
        //         'barangay' => ($request->barangay) ? $request->barangay : null,
        //         'city' => $request->city,
        //         'province' => $request->province,
        //         'zip_code' => ($request->zipCode) ? $request->zipCode : null
        //     ]);

        return json_encode([
            'success' => true,
            'message' => 'Group successfully added'
        ]);
    }

    private function generateGroupTracking() {
        Repack:
        $packId = 'GL' . $this->generateRandomString();
        $checkPackage = $this->checkPackage($packId);
        if($checkPackage > 0) :
            goto Repack;
        endif;
        return $packId;
    }

    private function checkPackage($packId){
        return Package::where('tracking', $packId)->count();
    }

    private function generateRandomString($length = 7) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i=0; $i<$length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function getGroupDeposit($group_id){
        return ClientTransactions::where('group_id',$group_id)->where('type', 'Deposit')->sum('amount');

	}

	private function getGroupDiscount($group_id){
        return ClientTransactions::where('group_id',$group_id)->where('type', 'Discount')->sum('amount');
	}

	private function getGroupPayment($group_id){
        return ClientTransactions::where('group_id',$group_id)->where('type', 'Payment')->sum('amount');
	}

	private function getGroupRefund($group_id){
		 return ClientTransactions::where('group_id',$group_id)->where('type', 'Refund')->sum('amount');
	}

	private function getGroupCost($group_id){
        $group_cost = ClientService::where('group_id', $group_id)
            ->where('active', 1)
            ->value(DB::raw("SUM(cost + charge + tip + com_agent + com_client)"));
        return $group_cost;
    }

    private function getCompleteGroupCost($group_id){
        $group_cost = ClientService::where('group_id', $group_id)
            ->where('active', 1)
            ->where('status', 'complete')
            ->value(DB::raw("SUM(cost + charge + tip + com_agent + com_client)"));
        if($group_cost == null){
             return 0;
        }
        return $group_cost;
    }

    private function groupCompleteBalance($group_id){
        $balance = ((
                        $this->getGroupDeposit($group_id)
                        + $this->getGroupPayment($group_id)
                        + $this->getGroupDiscount($group_id)
                    )-(
                        $this->getGroupRefund($group_id)
                        + $this->getCompleteGroupCost($group_id)
                    ));
        return $balance;
    }


    private function getGroupBalance($group_id){
		return (
            (
                $this->getGroupDeposit($group_id)
                + $this->getGroupPayment($group_id)
                +$this->getGroupDiscount($group_id)
            )
            -
            (
                $this->getGroupRefund($group_id)
                + $this->getGroupCost($group_id)
            )
        );
	}

    public function summary($groupId) {
        //$group = [];
        $clientCom = ClientService::select(DB::raw("SUM(com_client) as total"))->where('group_id',$groupId)->where('status','complete')->first()->total;
        $agentCom = ClientService::select(DB::raw("SUM(com_agent) as total"))->where('group_id',$groupId)->where('status','complete')->first()->total;
        //\Log::info($clientCom);
        $group = Group::findOrFail($groupId);
        $group['user'] = User::where('id',$group->leader)->select('id','first_name','last_name','contact_number','alternate_contact_number')->first()->makeHidden([ 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
    	return json_encode([
    		'group' => $group,
			'deposit' => $this->getGroupDeposit($groupId),
			'cost' => $this->getGroupCost($groupId),
            'completeCost' => $this->getCompleteGroupCost($groupId),
			'discount' => $this->getGroupDiscount($groupId),
			'refund' => $this->getGroupRefund($groupId),
            'balance' => $this->getGroupBalance($groupId),
            'payment' => $this->getGroupPayment($groupId),
            'collectables' => $this->groupCompleteBalance($groupId),
            'clientcom' => $clientCom,
            'agentcom' => $agentCom,
    	]);
    }

    public function serviceProfiles() {
        $data = ServiceProfiles::where('is_active',1)->get();
        return json_encode($data);
    }

    public function fetchBranch() {
        $data = Branch::where('id','!=',3)->get();
        return json_encode($data);
    }

    public function members($groupId) {
        // return GroupMember::with('client', 'services')
        //     ->where('group_id', $groupId)
        //     ->get()
        //     ->makeHidden(['client.full_name', 'client.avatar', 'client.permissions', 'client.access_control', 'client.binded', 'client.unread_notif', 'client.group_binded', 'client.document_receive', 'client.is_leader', 'client.total_deposit', 'client.total_discount', 'client.total_refund', 'client.total_payment', 'client.total_cost', 'client.total_balance', 'client.branch', 'client.three_days']);
        $arr = [];
        $group_members = GroupMember::where('group_id', $groupId)->get();
        //$arr[] = $group_members;
        $ctr=0;
        foreach($group_members as $gm){
            $arr[$ctr] = $gm;
            $arr[$ctr]['client'] = User::where('id',$gm->client_id)->select('id','first_name','last_name','date_register')->first()->makeHidden(['access_control','avatar','group_binded','document_receive','three_days','total_balance','total_cost','total_deposit','total_discount','total_payment','total_points','total_refund','unread_notif','permissions']);
            $arr[$ctr]['services'] = ClientService::where('client_id',$gm->client_id)->where('group_id',$gm->group_id)->where('active',1)->where('status','!=','pending')->orderBy('id', 'desc')->get();
            $ctr++;
        }
        return $arr;
    }
   
    public function setCommission(Request $request){
        switch($request->method_id){
            case 1 :
                DB::connection('visa')->table('groups')->where('id', $request->group_id)->update(['client_com_id'=>$request->client_id]);
            break;
            default:
                DB::connection('visa')->table('groups')->where('id', $request->group_id)->update(['agent_com_id'=>$request->client_id]);
            break;
        }
    }
    public function removeCommission(Request $request){
        switch($request->method_id){
            case 1 :
                DB::connection('visa')->table('groups')->where('id', $request->group_id)->update(['client_com_id'=>0]);
            break;
            default:
                DB::connection('visa')->table('groups')->where('id', $request->group_id)->update(['agent_com_id'=>0]);
            break;
        }
    }
    public function clientCommissionUser($client_id, $agent_id){
        $client = DB::table('users')->select(DB::raw("CONCAT_WS(' ',first_name,last_name) AS full_name, id, first_name, last_name"))->where('id',$client_id)->first();//User::where('id',$gm->client_id)->select('id','first_name','last_name')->first();
        $agent = DB::table('users')->select(DB::raw("CONCAT_WS(' ',first_name,last_name) AS full_name, id, first_name, last_name"))->where('id',$agent_id)->first();
        return array('client_com'=>$client,'agent_com'=>$agent);
    }
    public function clientCommissionList($client_id, $group_id) {
        $contains_letters = preg_match('/[A-Za-z]+/', $client_id);

        if($contains_letters==0){
            $agent = DB::connection('visa')->table('groups')->where('agent_com_id',$client_id)->where('id',$group_id)->count();
            if($agent==0){
                $group_members = DB::table('users')->select(DB::raw("CONCAT_WS(' ',first_name,last_name) AS full_name, id, first_name, last_name"))->where('id',$client_id)->get();
                // if(!$group_members){
                //     $group_members = DB::connection('visa')->table('groups')->where('agent_com_id',$client_id)->where('first_name','like','%'.$group_id.'%')->count();
                // }
            }else{
                
                $group_members=[];
            
            }
        }else{
            $agent_id = DB::connection('visa')->table('groups')->select('agent_com_id')->where('id',$group_id)->first()->agent_com_id;
            $agent_id = ($agent_id) ? $agent_id : 0;
           
            $val = explode(" ",$client_id);
            if(count($val)>1){
                $group_members = DB::table('users')->select(DB::raw("CONCAT_WS(' ',first_name,last_name) AS full_name, id, first_name, last_name"))
                        ->where('id','!=',$agent_id)
                        ->where('first_name','like','%'.$val[0].'%')
                        ->where('last_name','like','%'.$val[count($val)-1].'%')
                        ->orwhere('first_name','like','%'.$val[count($val)-1].'%')
                        ->where('id','!=',$agent_id)
                        ->where('last_name','like','%'.$val[0].'%')
                        ->get();
            }else{
                $group_members = DB::table('users')->select(DB::raw("CONCAT_WS(' ',first_name,last_name) AS full_name, id, first_name, last_name"))
                        ->where('id','!=',$agent_id)
                        ->where('first_name','like','%'.$client_id)
                        ->orwhere('last_name','like','%'.$client_id)
                        ->where('id','!=',$agent_id)
                        ->get();
            }
            
        }

         return array('user'=>$group_members);
    }
    public function agentCommissionList($client_id, $group_id) {   
        $contains_letters = preg_match('/[A-Za-z]+/', $client_id);
        
        if($contains_letters==0){
            $client = DB::connection('visa')->table('groups')->where('client_com_id',$client_id)->where('id',$group_id)->count();
        
            if($client==0){
                $group_members = DB::table('users')->select(DB::raw("CONCAT_WS(' ',first_name,last_name) AS full_name, id, first_name, last_name"))->where('id',$client_id)->get();
            }else{
                $group_members=[];
            }
        }else{
            $clients_id = DB::connection('visa')->table('groups')->select('client_com_id')->where('id',$group_id)->first()->client_com_id;
            $clients_id = ($clients_id) ? $clients_id : 0;
            
            $val = explode(" ",$client_id);
            if(count($val)>1){
                $group_members = DB::table('users')->select(DB::raw("CONCAT_WS(' ',first_name,last_name) AS full_name, id, first_name, last_name"))
                        ->where('id','!=',$clients_id)
                        ->where('first_name','like','%'.$val[0].'%')
                        ->where('last_name','like','%'.$val[count($val)-1].'%')
                        ->orwhere('first_name','like','%'.$val[count($val)-1].'%')
                        ->where('id','!=',$clients_id)
                        ->where('last_name','like','%'.$val[0].'%')
                        ->get();
            }else{
                $group_members = DB::table('users')->select(DB::raw("CONCAT_WS(' ',first_name,last_name) AS full_name, id, first_name, last_name"))
                        ->where('id','!=',$clients_id)
                        ->where('first_name','like','%'.$client_id)
                        ->orwhere('last_name','like','%'.$client_id)
                        ->where('id','!=',$clients_id)
                        ->get();
            }
            
        }
       

         return array('user'=>$group_members);
    }

    public function switchCostLevel($groupId, $level) {
        $group = Group::where('id',$groupId)->first();
        if($group){
            $group->cost_level = $level;
            $group->save();
        }
        $members = GroupMember::where('group_id',$groupId)->pluck('client_id');

        $member_services = ClientService::whereIn('client_id',$members)->where('group_id',$groupId)->where('status','!=','complete')->get();

        foreach($member_services as $ms){
            $getService = Service::where('id',$ms->service_id)->first();
            if($getService){
                $cost = 0;
                $charge = 0;
                $tip = 0;
                $client = 0 ;
                $agent = 0 ;

                if($group->branch_id > 1){
                    $amounts = ServiceBranchCost::where('branch_id',$group->branch_id)->where('service_id',$getService->id)->first();
                    $charge = $amounts->charge;
                    $cost = $amounts->cost;
                    $tip = $amounts->tip;
                }

                if($level > 0){
                    $pcost = ServiceProfileCost::where('profile_id',$level)->where('service_id',$getService->id)->where('branch_id',$group->branch_id)->first();
                    $charge = $pcost->charge;
                    $cost = $pcost->cost;
                    $tip = $pcost->tip;
                    $client = $pcost->com_client;
                    $agent = $pcost->com_agent;

                    $charge = ($charge > 0 ? $charge : $getService->charge);
                    $cost = ($cost > 0 ? $cost : $getService->cost);
                    $tip = ($tip > 0 ? $tip : $getService->tip);
                }
                // else{
                //     $charge = $getService->charge;
                //     $cost = $getService->cost;
                //     $tip = $getService->tip;
                // }


                if($level == 0 && $group->branch_id == 1){
                    $charge = $getService->charge;
                    $cost = $getService->cost;
                    $tip = $getService->tip;
                }


                $serv = ClientService::find($ms->id);
                if($charge > 0){
                    $serv->charge = $charge;
                }
                //$serv->cost = $cost;
                $serv->com_client = $client;
                $serv->com_agent = $agent;
                if($tip > 0){
                    $serv->tip = $tip;
                }
                $serv->save();
            }
        }

        return json_encode([
                'success' => true,
                'message' => 'Group Services successfully switched.'
            ]);
    }

    // public function switchCostLevel($groupId, $level) {
    //     $group = Group::where('id',$groupId)->first();
    //     if($group){
    //         $group->cost_level = $level;
    //         $group->save();
    //     }
    // 	$members = GroupMember::where('group_id',$groupId)->pluck('client_id');

    //     $member_services = ClientService::whereIn('client_id',$members)->where('group_id',$groupId)->where('status','!=','complete')->get();

    //     foreach($member_services as $ms){
    //         $getService = Service::where('id',$ms->service_id)->first();
    //         if($getService){
    //             $cost = 0;
    //             $charge = 0;
    //             $tip = 0;
    //             if($level > 0){
    //                 $pcost = ServiceProfileCost::where('profile_id',$level)->where('service_id',$getService->id)->first();
    //                 $charge = $pcost->cost;
    //             }
    //             if($group->branch_id > 1){
    //                 $amounts = ServiceBranchCost::where('branch_id',$group->branch_id)->where('service_id',$getService->id)->first();
    //             }
    //             else{
    //                 $amounts = $getService;
    //             }

    //             if($level == 0 || $charge == 0){
    //                 $charge = $amounts->charge;
    //             }
    //             $cost = $amounts->cost;
    //             $tip = $amounts->tip;

    //             if($charge == 0 && $cost == 0 && $tip == 0){
    //                 $charge = $getService->charge;
    //                 $cost = $getService->cost;
    //                 $tip = $getService->tip;
    //             }

    //             $serv = ClientService::find($ms->id);
    //             $serv->charge = $charge;
    //             //$serv->cost = $cost;
    //             $serv->tip = $tip;
    //             $serv->save();
    //         }
    //     }

    //     return json_encode([
    //             'success' => true,
    //             'message' => 'Group Services successfully switched.'
    //         ]);
    // }

    public function switchBranch($groupId, $branch_id) {
        $group = Group::where('id',$groupId)->first();
        if($group){
            $group->branch_id = $branch_id;
            $group->save();
        }
        $level = $group->cost_level;
        $members = GroupMember::where('group_id',$groupId)->pluck('client_id');

        User::whereIn('id', $members)->update(['branch_id' => $branch_id]);

        $member_services = ClientService::whereIn('client_id',$members)->where('group_id',$groupId)->get();
                //\Log::info($member_services);

        foreach($member_services as $ms){
            $getService = Service::where('id',$ms->service_id)->first();
            if($getService){
                $charge = 0;
                $cost = 0;
                $tip = 0;
                if($branch_id > 1){
                    $amounts = ServiceBranchCost::where('branch_id',$branch_id)->where('service_id',$getService->id)->first();
                    $charge = $amounts->charge;
                    $cost = $amounts->cost;
                    $tip = $amounts->tip;
                //\Log::info('BranchCost : '.$charge.' '.$cost.' '.$tip);

                }

                if($level > 0){
                    $pcost = ServiceProfileCost::where('profile_id',$level)->where('branch_id',$branch_id)->where('service_id',$getService->id)->first();
                    $charge = $pcost->charge;
                    $cost = $pcost->cost;
                    $tip = $pcost->tip;

                    $charge = ($charge > 0 ? $charge : $getService->charge);
                    $cost = ($cost > 0 ? $cost : $getService->cost);
                    $tip = ($tip > 0 ? $tip : $getService->tip);
                }

                if($level == 0 && $branch_id == 1){
                    $charge = $getService->charge;
                    $cost = $getService->cost;
                    $tip = $getService->tip;
                }

                //\Log::info($charge.' '.$cost.' '.$tip);

                $serv = ClientService::find($ms->id);
                $serv->charge = $charge;
                //$serv->cost = $cost;
                $serv->tip = $tip;
                $serv->save();
            }
        }

        return json_encode([
                'success' => true,
                'message' => 'Group successfully transferred.'
            ]);
    }

    // public function switchBranch($groupId, $branch_id) {
    //     $group = Group::where('id',$groupId)->first();
    //     if($group){
    //         $group->branch_id = $branch_id;
    //         $group->save();
    //     }
    //     $level = $group->cost_level;
    //     $members = GroupMember::where('group_id',$groupId)->pluck('client_id');

    //     User::whereIn('id', $members)->update(['branch_id' => $branch_id]);

    //     $member_services = ClientService::whereIn('client_id',$members)->where('group_id',$groupId)->get();

    //     foreach($member_services as $ms){
    //         $getService = Service::where('id',$ms->service_id)->first();
    //         if($getService){
    //             $charge = 0;
    //             $cost = 0;
    //             $tip = 0;
    //             if($branch_id > 1){
    //                 $amounts = ServiceBranchCost::where('branch_id',$branch_id)->where('service_id',$getService->id)->first();
    //             }
    //             else{
    //                 $amounts = $getService;
    //             }

    //             if($level > 0){
    //                 $pcost = ServiceProfileCost::where('profile_id',$level)->where('service_id',$getService->id)->first();
    //                 $charge = $pcost->cost;
    //             }

    //             if($level == 0 || $charge == 0){
    //                 $charge = $amounts->charge;
    //             }
    //             $cost = $amounts->cost;
    //             $tip = $amounts->tip;

    //             if($charge == 0 && $cost == 0 && $tip == 0){
    //                 $charge = $getService->charge;
    //                 $cost = $getService->cost;
    //                 $tip = $getService->tip;
    //             }

    //             //\Log::info($charge.' '.$cost.' '.$tip);

    //             $serv = ClientService::find($ms->id);
    //             $serv->charge = $charge;
    //             //$serv->cost = $cost;
    //             $serv->tip = $tip;
    //             $serv->save();
    //         }
    //     }

    //     return json_encode([
    //             'success' => true,
    //             'message' => 'Group successfully transferred.'
    //         ]);
    // }

    public function gServices($groupId) {
        $sId = ClientService::where('group_id',$groupId)->where('active',1)->with('discount')
                        //->groupBy('service_id')
                        ->orderBy('id',"DESC")
                        ->get()->unique('service_id');
        
        $ctr = 0;
        $services = [];
        foreach($sId as $s){
            $services[$ctr] = ClientService::where('id',$s->id)->select('detail','service_date','service_id')->orderBy('id',"DESC")->first();
            $services[$ctr]['totalcost'] = ClientService::where('service_id',$s->service_id)->where('group_id', $groupId)->where('active', 1)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
            //$getMem = ClientService::where('service_id',$s->service_id)->where('group_id',$groupId)->where('active',1)->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('client_id')->get();
            $ctr2 = 0;
            // \Log::info($getMem);

            // foreach($getMem as $m){
            //     $client[$ctr2] = User::where('id',$m->client_id)->select('first_name','last_name')->first();
            //     $client[$ctr2]['tcost'] = ClientService::where('service_id',$s->service_id)->where('group_id', $groupId)->where('active', 1)->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip)"));
            //     $client[$ctr2]['services'] = ClientService::where('service_id',$s->service_id)->where('group_id',$groupId)->where('active',1)->with('discount2')->orderBy('service_date','DESC')->where('client_id',$m->client_id)->get();
            //     $ctr2++;
            // }
            // $services[$ctr]['members'] = $client;
            $ctr++;
        }
        return $services;
    }

    public function serviceByDetail(Request $request,$groupId) {
        $id = $request->service_id;
        $query = ClientService::where('service_id',$id)->where('group_id',$groupId)->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('client_id')->get();
        $ctr2 = 0;
        $client = [];
        foreach($query as $m){
            $ss =  ClientService::where('service_id',$id)->where('group_id',$groupId)->with('discount2')->orderBy('service_date','DESC')->where('client_id',$m->client_id)->get();
            // return $ss;
            if(count($ss)){
                $client[$ctr2] = User::where('id',$m->client_id)->select('first_name','last_name')->first()->makeHidden(['permissions','access_control','document_receive','binded','unread_notif','group_binded']);
                $client[$ctr2]['tcost'] = ClientService::where('service_id',$id)->where('group_id', $groupId)->where('active', 1)->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
                $client[$ctr2]['services'] = $ss;
            }
            $ctr2++;
        }
        return $client;
    }

    public function serviceByDate(Request $request,$groupId) {
        $date = $request->date;
        $query = ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$date)->where('group_id', $groupId)->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('client_id')->get();
        $ctr2 = 0;
        $client = [];
        foreach($query as $m){
            $ss =  ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$date)->where('group_id', $groupId)->where('client_id',$m->client_id)->with('discount2')->get();
            // return $ss;
            if(count($ss)){
                $client[$ctr2] = User::where('id',$m->client_id)->select('first_name','last_name')->first()->makeHidden(['permissions','access_control','document_receive','binded','unread_notif','group_binded']);
                $client[$ctr2]['tcost'] = ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$date)->where('group_id', $groupId)->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip +com_client + com_agent)"));
                $client[$ctr2]['services'] = $ss;
            }
            $ctr2++;
        }
        return $client;
    }

    public function gBatch($groupId) {
        // $sId = ClientService::where('group_id',$groupId)->where('active',1)->with('discount')->groupBy('service_date')->orderBy('id',"DESC")->get();
        $sId = DB::connection('visa')
            ->table('client_services as a')
            ->select(DB::raw('id,detail,service_date, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y") as sdate'))
            ->where('active',1)->where('group_id',$groupId)
            ->orderBy('id','DESC')
            ->groupBy('sdate')
            ->get();
        $ctr = 0;
        $services = [];
        foreach($sId as $s){
            $services[$ctr]['detail'] = $s->detail;
            $services[$ctr]['service_date'] = $s->service_date;
            $services[$ctr]['sdate'] = $s->sdate;
            $services[$ctr]['group_id'] = $groupId;

            $query = ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$s->sdate)->where('group_id', $groupId)->where('active', 1);
            // $oldquery = $query;
            $services[$ctr]['totalcost'] = $query->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
            //$getMem = $query->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('client_id')->get();
            //$ctr2 = 0;
            //$client = [];
            // foreach($getMem as $m){
            //     $ss =  $oldquery->where('client_id',$m->client_id)->with('discount2')->get();
            //     // return $ss;
            //     if(count($ss)){
            //         $client[$ctr2] = User::where('id',$m->client_id)->select('first_name','last_name')->first()->makeHidden(['permissions','access_control','document_receive','binded','unread_notif','group_binded']);
            //         $client[$ctr2]['tcost'] = $query->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip)"));
            //         $client[$ctr2]['services'] = $ss;
            //     }
            //     $ctr2++;
            // }
            // $services[$ctr]['members'] = $client;

            $ctr++;
        }
        return $services;
    }

    public function getAllBalance() {
        $group = Group::select('id',DB::raw("CONCAT('Group : ',name) AS full_name"),'temp_balance','temp_collectable')
                    //->where('temp_collectable','<',0)
                    ->where('temp_balance','<',0)->where('branch_id','!=',2)->get()->makeHidden('branch','check_shop');

        $client = User::select('id','temp_balance','temp_collectable','first_name','last_name')->where('temp_balance','<',0)->where('branch_id','!=',2)->get()->makeHidden(['access_control','avatar','group_binded','document_receive','three_days','total_balance','total_cost','total_deposit','total_discount','total_payment','total_points','total_refund','unread_notif','permissions','total_complete_cost','collectable']);

        $grids = $group->pluck('id');
        $clids = $client->pluck('id');

        $merged = $group->merge($client);

        // $gPayment = ClientTransactions::select('id','group_id','amount')
        //                 ->where(function($q) {
        //                             $q->where('type', 'Payment');
        //                             $q->orwhere('type', 'Deposit');
        //                         })
        //                 ->whereIn('group_id',$grids)
        //                 ->where('id','>',21208)->orderBy('group_id')->get();

        $groupPayments =   DB::connection('visa')
                            ->table('groups as a')
                            ->select(DB::raw('CONCAT("Group : ",a.name) AS full_name,b.log_date,a.temp_balance,a.temp_balance as running,b.amount as total,a.id as cid, b.amount, b.log_date, b.id as trans_id'))
                            ->join('client_transactions as b','b.group_id','=','a.id')
                            ->whereIn('a.id', $grids)
                            ->where('b.id','>',21208)
                            ->where(function($q) {
                                    $q->where('b.type', 'Payment');
                                    $q->orwhere('b.type', 'Deposit');
                                })
                            ->orderBy('b.group_id')
                            ->get();

        $current = 0;
        $first = true;
        $amt = 0;
        $running = 0;
        foreach($groupPayments as $g){
            if($first){
                $g->total += $amt;
                $amt = $g->total;
                $first = false;
            }
            else{
                $g->total += $amt;
                $amt = $g->total;
            }

            if($current != $g->cid){
                $g->running = round($g->temp_balance + $g->amount , 2);
                $running = round($g->temp_balance + $g->amount , 2);
                $current = $g->cid;
            }
            else{
                $g->temp_balance = $running;
                $g->running = round($running + $g->amount, 2);
                $running = round($running + $g->amount, 2);
                $current = $g->cid;

            }
        }

        $clientPayments =   DB::connection('mysql')
                            ->table('users as a')
                            ->select(DB::raw('CONCAT(a.first_name," ",a.last_name) AS full_name,b.log_date,a.temp_balance,a.temp_balance as running,b.amount as total,a.id as cid, b.amount, b.log_date, b.id as trans_id'))
                            ->join('visa.client_transactions as b','b.client_id','=','a.id')
                            //->join('client_transactions as b','b.client_id','=','a.id' )
                            ->whereIn('a.id', $clids)
                            ->where('b.id','>',21208)
                            ->where('b.group_id','<',1)
                            ->where(function($q) {
                                    $q->where('b.type', 'Payment');
                                    $q->orwhere('b.type', 'Deposit');
                                })
                            ->orderBy('b.client_id')
                            ->get();

        //$current = 0;
        //$running = 0;
        foreach($clientPayments as $g){

                $g->total += $amt;
                $amt = $g->total;

            if($current != $g->cid){
                $g->running = round($g->temp_balance + $g->amount , 2);
                $running = round($g->temp_balance + $g->amount , 2);
                $current = $g->cid;
            }
            else{
                $g->temp_balance = $running;
                $g->running = round($running + $g->amount, 2);
                $running = round($running + $g->amount, 2);
                $current = $g->cid;
                
            }
        }

        $pays = $groupPayments->merge($clientPayments);

        $bal = $merged->sum('temp_balance');
        $col = $merged->sum('temp_collectable');
        $dis = $pays->sum('amount');

        //return $merged;
        return json_encode([
                'data' => $merged,
                'payments' => $pays,
                'total_balance' => $bal,
                'total_collectable' => $col,
                'total_distributed' => $dis
            ]);
    }

    //function to compute client 9838 commission in all ANVO group
    public function anvo() {
        $anvo = Group::where('name','LIKE','%Anvo%')->pluck('id');
        $count9A = ClientService::whereIn('group_id',$anvo)->where('detail','LIKE','%9A%')->where('active',1)->count();
        $total = 50 * $count9A;

        date_default_timezone_set("Asia/Manila");
        $date = date('m/d/Y h:i:s A');

            $depo = ClientTransactions::updateOrCreate(
                        ['client_id' => '9838'],
                        ['type' => 'Deposit'],
                        ['amount' => $total]
                    );
        return $depo;
    }

    // **** OLD ASSIGN LEADER **** //
    // public function assignLeader(Request $request, $groupId) {
    //     // Get old group leader
    //     $oldLeader = User::findOrFail(
    //         GroupMember::where('group_id', $groupId)
    //             ->where('leader', 1)
    //             ->first()
    //             ->client_id
    //     );
    //     $oldLeaderLabel = '[' . $oldLeader->id . '] ' . $oldLeader->full_name;

    //     // Get new group leader
    //     $newLeader = User::findOrFail($request->clientId);
    //     $newLeaderLabel = '[' . $newLeader->id . '] ' . $newLeader->full_name;

    //     GroupMember::where('group_id', $groupId)->update(['leader' => 0]);

    //     GroupMember::where('group_id', $groupId)
    //     	->where('client_id', $request->clientId)
    //     	->update(['leader' => 1]);

    //     Group::findOrFail($groupId)->update(['leader' => $request->clientId]);

    //     // Update all tables with leader id
    //         Deposit::where('group_id', $groupId)->update(['client_id' => $request->clientId]);
    //         Payment::where('group_id', $groupId)->update(['client_id' => $request->clientId]);
    //         Discount::where('group_id', $groupId)->update(['client_id' => $request->clientId]);
    //         Refund::where('group_id', $groupId)->update(['client_id' => $request->clientId]);

    //     // Action log
    //     $details = 'Change group leader from <strong>' . $oldLeaderLabel . '</strong> to <strong>' . $newLeaderLabel .'</strong>.';
    //     Common::saveActionLogs(0, 0, $details, $groupId);

    // 	return json_encode([
    // 		'success' => true,
    // 		'message' => 'Group leader successfully updated.'
    // 	]);
    // }

    public function assignLeader(Request $request, $groupId) {
        // Get old group leader
        $oldLeader = User::findOrFail(
            GroupMember::where('group_id', $groupId)
                ->where('leader', 1)
                ->first()
                ->client_id
        );
        $oldLeaderLabel = '[' . $oldLeader->id . '] ' . $oldLeader->full_name;

        // Get new group leader
        $newLeader = User::findOrFail($request->clientId)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);

        $newLeaderLabel = '[' . $newLeader->id . '] ' . $newLeader->full_name;

        if($type == 'leader'){
            //demote old leader to vice leader
            GroupMember::where('group_id', $groupId)
                ->where('client_id', $oldLeader->id)
                ->update(['leader' => 2]);

            //assign new main leader
            GroupMember::where('group_id', $groupId)
                ->where('client_id', $request->clientId)
                ->update(['leader' => 1]);

            Group::findOrFail($groupId)->update(['leader' => $request->clientId]);

            // Update all tables with new leader's id
            ClientTransactions::where('group_id', $groupId)->update(['client_id' => $request->clientId]);

            // Action log
            $details = 'Change new group main leader from <strong>' .$oldLeaderLabel. '</strong> to <strong>' . $newLeaderLabel .'</strong>.';
            Common::saveActionLogs(0, 0, $details, $groupId);

            return json_encode([
                'success' => true,
                'message' => 'Group main leader successfully changed.'
            ]);
        }else if ($type == 'member'){
            //change as member
            GroupMember::where('group_id', $groupId)
                ->where('client_id', $request->clientId)
                ->update(['leader' => 0]);

            // Action log
            $details = 'Changed member type to "member" -> <strong>' . $newLeaderLabel .'</strong>.';
            Common::saveActionLogs(0, 0, $details, $groupId);

            return json_encode([
                'success' => true,
                'message' => 'Member type successfully changed.'
            ]);
        }else if ($type == 'vice'){
            //make vice leader
            GroupMember::where('group_id', $groupId)
                ->where('client_id', $request->clientId)
                ->update(['leader' => 2]);

            // Action log
            $details = 'Assign new group vice leader -> <strong>' . $newLeaderLabel .'</strong>.';
            Common::saveActionLogs(0, 0, $details, $groupId);

            return json_encode([
                'success' => true,
                'message' => 'Group vice leader successfully assigned.'
            ]);
        }

    }


    public function addMembers(AddGroupMemberValidation $request, $id) {
        foreach($request->clientIds as $clientId) {
            GroupMember::create([
                'group_id' => $id,
                'client_id' => $clientId,
                'leader' => 0
            ]);

            $group = Group::findOrFail($id);
            $user = User::findOrFail($clientId)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
            if($user){
                $leaderAgent = UserOpen::where('referral_id',$group->leader)->first();
                if($leaderAgent || $group->is_shop > 0){
                    // if($user->contact_number == null || $user->contact_number == ''){
                    //     $user->contact_number = '09209824652';
                    // }
                    // if($user->birth_date == null || $user->birth_date == ''){
                    //     $user->birth_date = '1992-02-04';
                    // }
                    $checkIfReferred = UserOpen::where('client_id',$clientId)->first();
                    if(!$checkIfReferred){                    
                        UserOpen::create([
                            'client_id' => $clientId,
                            'cp_num' => $user->contact_number,
                            'first_name' => $user->first_name,
                            'last_name' => $user->last_name,
                            'bday' => $user->birth_date,
                            'gender' => $user->gender,
                            'referral_id' => $group->leader,
                            'is_verified' => 1,
                        ]);
                    }
                }
            }

            SynchronizationController::update('groups', Carbon::now()->toDateTimeString());
            // Action log
            $member = User::findOrFail($clientId)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
            $memberLabel = '[' . $member->id . '] ' . $member->full_name;
            $details = 'Added <strong>' . $memberLabel . '</strong> as new member of the group.';
            Common::saveActionLogs(0, 0, $details, $id);
        }

    	return json_encode([
    		'success' => true,
    		'message' => 'Clients successfully added to group.'
    	]);
    }

    private function generateServiceTracking() {
        Repack:
        $packId = 'G' . $this->generateRandomString();
        $checkPackage = $this->checkPackage($packId);
        if($checkPackage > 0) :
            goto Repack;
        endif;
        return $packId;
    }

    public function addService(AddGroupServiceValidation $request, $id) {
        //\Log::info($request->all());
        $clientServicesIdArray = [];

        $g = Group::findOrFail($id);
        $level = $g->cost_level;
        $trackingArray = [];
        $oldNewArray = [];
        $oldNewArrayChinese = [];
        for($i=0; $i<count($request->clients); $i++) {
            $clientId = $request->clients[$i];

            if($request->packages[$i] == 'Generate new package') { // Generate new package
                $tracking = $this->generateServiceTracking();

                Package::create([
                    'client_id' => $clientId,
                    'group_id' => $id,
                    'log_date' => Carbon::now()->format('F j, Y, g:i a'),
                    'tracking' => $tracking,
                    'status' => 2
                ]);
                $oldnew = "new";
                $oldnew_cn = "新的";
            } else {
                $tracking = $request->packages[$i];
                $oldnew = "old";
                $oldnew_cn = "旧的";
            }

            $trackingArray[] = $tracking;
            $oldNewArray[] = $oldnew;
            $oldNewArrayChinese[] = $oldnew_cn;
        }

        $ctr = 0;
        $msg = [];
        $collect_services = '';
        $collect_services_cn = '';
        $collect_clients = '';
        $collect_service_id = '';
        $service_status = 'on process';
        for($j=0; $j<count($request->services); $j++) {

            $translated = Service::where('id',$request->services[$j])->first();
            $cnserv =$translated->detail;
                if($translated){
                    $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                }
            $collect_services .= $translated->detail.', ';
            $collect_services_cn .= $cnserv.', ';
            $cls = '';
            for($i=0; $i<count($request->clients); $i++) {
            $clientId = $request->clients[$i];
            $client = User::findOrFail($clientId)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
            $oldnew = '';
            $oldpacks = '';
            $newpacks = '';

            // if($request->packages[$i] == 'Generate new package') { // Generate new package
            //     $tracking = $this->generateServiceTracking();

            //     Package::create([
            //         'client_id' => $clientId,
            //         'group_id' => $id,
            //         'log_date' => Carbon::now()->format('F j, Y, g:i a'),
            //         'tracking' => $tracking,
            //         'status' => 2
            //     ]);
            //     $oldnew = "new";
            // } else {
            //     $tracking = $request->packages[$i];
            //     $oldnew = "old";
            // }

              //  \Log::info('service : '.$translated->detail);
              //  \Log::info('client : '.$clientId);
              //  \Log::info('s1 : '.$translated->docs_needed);
                $req_docs = [];
                if($translated->docs_needed!= null || $translated->docs_needed!= ''){
                    $req_docs = explode(",", $translated->docs_needed); //33,16
                }
                // $req_docs = explode(",", $serv->docs_needed);
              //  \Log::info('s2 : '.$request->docs[$ctr]);
                $chosen_docs = [];
                if($request->docs[$ctr]!= null || $request->docs[$ctr]!= ''){
                    $chosen_docs =explode(",", $request->docs[$ctr]);
                }

                $compare_docs = array_diff($req_docs, $chosen_docs);
                //\Log::info($compare_docs);

                //status depending on submitted docs
                //\Log::info($compare_docs);
                //\Log::info(!empty($compare_docs));

                if(!empty($compare_docs)){
                    $service_status = 'pending';
                }else{
                    $service_status = 'on process';
                }

                $chosen = '';
                $chosen_cn = '';
                foreach($chosen_docs as $ch){
                    $sd = ServiceDocuments::findOrFail($ch);
                    $chosen .= $sd->title.",";
                    $chosen_cn .= $sd->title_cn.",";
                }
                $chosen = rtrim($chosen, ',');
                $chosen_cn = rtrim($chosen_cn, ',');

                $missing = '';
                $missing_cn = '';
                if(!empty($compare_docs)){
                    foreach($compare_docs as $cd){
                        $sd = ServiceDocuments::findOrFail($cd);
                        $missing .= $sd->title.",";
                        $missing_cn .= $sd->title_cn.",";
                    }
                }
                $missing = rtrim($missing, ',');
                $missing_cn = rtrim($missing_cn, ',');

                $serviceId =  $request->services[$j];
                $service = Service::findOrFail($serviceId);

                $dt = Carbon::now();
                $dt = $dt->toDateString();
                //$user = Auth::user();
                $author = $request->note.' - '. Auth::user()->first_name.' <small>('.$dt.')</small>';
                if($request->note==''){
                    $author = '';
                }
                $note = $service->remarks;
                if($note!=''){
                    if($request->get('note')!=''){
                        $note = $note.'</br>'.$author;
                    }
                }
                else{
                    $note = $author;
                }

                //service cost depends on cost level of group
                $scharge = $request->charge;
                $scost = $request->cost;
                $stip = $request->tip;
                $client_com_id = $request->client_com_id;
                $agent_com_id = $request->agent_com_id;

                $thisGroup = DB::connection('visa')->table('groups')->where('id',$id)->first();
               
                //  if($thisGroup->client_com_id==0 && $thisGroup->agent_com_id==0){
                //     DB::connection('visa')->table('groups')->where('id',$id)->update(['client_com_id'=>$client_com_id,'agent_com_id'=>$agent_com_id]);
                // }
                
               
                if($g->branch_id > 1){
                    $bcost = ServiceBranchCost::where('branch_id',$g->branch_id)->where('service_id',$serviceId)->first();
                    $scost = $bcost->cost;
                    $stip = $bcost->tip;
                    $scharge = $bcost->charge;
                    $com_client = $bcost->com_client;
                    $com_agent = $bcost->com_agent;
                }
                else{
                    $scost = $service->cost;
                    $stip = $service->tip;
                    $scharge = $service->charge;
                    $com_client = $service->com_client;
                    $com_agent = $service->com_agent;
                }

                //\Log::info('After Checking of Branch: Charge : '.$scharge.' --- Cost :'.$scost.' --- Tip :'.$stip);

                if($g->cost_level > 0 ){
                    $newcost = ServiceProfileCost::where('profile_id',$g->cost_level)
                                    ->where('branch_id',$g->branch_id)
                                    ->where('service_id',$serviceId)
                                    ->first();
                    if($newcost){
                        $scharge = $newcost->charge;
                        $scost = $newcost->cost;
                        $stip = $newcost->tip;
                        $com_client = $newcost->com_client;
                        $com_agent = $newcost->com_agent;
                    }
                }

                //\Log::info('After Checking of Profile: Charge : '.$scharge.' --- Cost :'.$scost.' --- Tip :'.$stip);

                $scharge = ($scharge > 0 ? $scharge : $service->charge);
                $scost = ($scost > 0 ? $scost : $service->cost);
                $stip = ($stip > 0 ? $stip : $service->tip);
                // if($scost == 0 && $scharge == 0 && $stip == 0){
                //     $scost = $service->cost;
                //     $scharge = $service->charge;
                //     $stip = $service->tip;
                // }

                //\Log::info('Value to be saved : '.$scharge.' --- Cost :'.$scost.' --- Tip :'.$stip);

                $service_status = DocumentLogsController::getClientServiceStatus($serviceId, $request->docs[$ctr]);

                $clientService = ClientService::create([
                    'client_id' => $clientId,
                    'service_id' => $serviceId,
                    'detail' => $service->detail,
                    'cost' => $scost,
                    'charge' => $scharge,
                    'tip' => $stip,
                    'status' => $service_status,
                    'com_client' => $com_client,
                    'com_agent' => $com_agent,
                    'agent_com_id' => $agent_com_id,
                    'agent_com_id' => $agent_com_id,
                    'service_date' => Carbon::now()->format('m/d/Y'),
                    'remarks' => $author,
                    'group_id' => $id,
                    'tracking' => $trackingArray[$i],
                    'temp_tracking' => null,
                    'active' => 1,
                    'extend' => null,
                    'rcv_docs' => (!is_null($request->docs[$ctr])) ? $request->docs[$ctr] : ''
                ]);

                //add new shop rate if not existing
                $referred = UserOpen::where('client_id',$clientId)->first();
                if($referred){                
                    $sd = explode("/",$clientService->service_date);
                    $rateExist = ShopRate::where('client_id',$referred->referral_id)->where('month',$sd[0])->where('year',$sd[2])->first();
                    if(!$rateExist){
                         $prevMaxRate = ShopRate::where('client_id',$referred->referral_id)->max('rate');
                         $rate = ShopRate::create([
                            'client_id' => $referred->referral_id,
                            'month' => $sd[0],
                            'year' => $sd[2],
                            'rate' => ($prevMaxRate > 0.4 ? $prevMaxRate : 0.4),
                        ]);
                    }
                }
                // end

                // Task Generator
                if($clientService->status == 'on process') {
                    TasksController::save([$clientService], [], [], [], [], [], [], $request->sameDayFilling);
                }

                // ******************** Document Tracker ******************** //
                    // for (63)9A VISA EXTENSION only // TEMPORARY
                    if( $request->docs[$ctr] && $translated->parent_id == 63 ) {
                        $originsArray = DocumentLogsController::getOrigins(
                            explode(',', $request->docs[$ctr]),
                            $clientId,
                            $clientService->id
                        );

                        DocumentLogsController::saveOrigins($originsArray, $clientService->id);
                    }
                // ******************** End of Document Tracker ******************** //

                if($missing!=''){
                    $missing = "<br><br>- Missing documents (".$missing."), ";
                    $missing_cn = "<br><br>- 文件缺失 (".$missing_cn."), ";
                 }

                if($chosen!=''){
                    $chosen = "<br><br>- Received documents (".$chosen."), ";
                    $chosen_cn = "<br><br>- 已验收文件 (".$chosen_cn."), ";
                }

                if($chosen!= '' || $missing !=''){
                    $reportDetail = $chosen.$missing;
                    Report::create([
                        'service_id' => $clientService->id,
                        'client_id' => $clientService->client_id,
                        'group_id' => $clientService->group_id,
                        'detail' => $reportDetail,
                        'user_id' => Auth::user()->id,
                        'tracking' => $clientService->tracking,
                        'log_date' => Carbon::now()->format('Y-m-d H:i:s a'),
                        'is_read' => 0
                    ]);
                }
                // \Log::info($chosen_cn);

                $clientServicesIdArray[] = $clientService;

                $this->check_package_updates($trackingArray[$i]);

                $disp = 1;
                // if(count($request->services) <2  && count($request->clients) < 2){
                //     $disp = 0;
                // }

                $total_charge = $scharge + $stip + $scost;
                $log_data['client_id'] = $clientId;
                $log_data['group_id'] = $id;
                $log_data['service_id'] = $clientService->id;
                $log_data['tracking'] = $trackingArray[$i];
                $log_data['user_id'] = Auth::user()->id;
                $log_data['type'] = 'service';
                $log_data['amount'] = 0;
                $log_data['display'] = $disp;
                $log_data['balance'] = $this->groupCompleteBalance($id);
                // $log_data['detail'] = 'Added a service <strong>' . $service->detail . '</strong> to Package Number <strong>' . $trackingArray[$i] . '</strong>  for Client <strong>[' . $clientId . '] ' . $client->full_name . '</strong>';
                $log_data['detail'] = 'Added a service. Service status is '.$clientService->status.'.';
                $detail_cn = ($service->detail_cn != null ? $service->detail_cn : $service->detail);
                $log_data['detail_cn'] = '已添加服务. 服务状态为 '.$clientService->status.'.';
                Common::saveTransactionLogs($log_data);
                $collect_clients .= $clientId .', ';
                $collect_service_id .= $clientService->id . ', ';

                $dc = '';
                $dcn = '';
                $totalperservice = $service->cost;
                // if($request->discount && count($request->services) == 1) {
                //     ClientTransactions::create([
                //         'client_id' => $clientId,
                //         'amount' => $request->discount,
                //         'type' => 'Discount',
                //         'group_id' => $id,
                //         'service_id' => $clientService->id,
                //         'reason' => $request->reason,
                //         'tracking' => $trackingArray[$i],
                //         'log_date' => Carbon::now()->format('m/d/Y g:i')
                //     ]);
                //     $dc = " and receive a discount worth Php".$request->discount;
                //     $dcn = " 予折扣Php".$request->discount;
                //     $totalperservice -= $request->discount;
                // }


                $title = "New Service";
                $title_cn = "新的服务";
                // $body = 'New Service '.$translated->detail.' worth Php'.$service->cost.$dc.' was added to your '.$oldNewArray[$i].' package #'.$trackingArray[$i].' with total service fee of Php'.$totalperservice;
                // $body_cn = "新的服务 ".$cnserv.$dc." 已添加到您的编号为#".$trackingArray[$i]."的服务包";
                // $main = $translated->detail;
                // $main_cn = $cnserv;
                $parent = $trackingArray[$i];
                $parent_type = 1;
                $group = Group::findOrFail($id);

                $msg[$ctr]['client'] = $client->first_name." ".$client->last_name;
                $msg[$ctr]['msg'] = $chosen.$missing;
                $msg[$ctr]['msg_cn'] = $chosen_cn.$missing_cn;
                $msg[$ctr]['status'] = $service_status;

                $checkIfVice = [];
                $checkIfVice[] = $clientId;
                $groupVice = GroupMember::where('group_id',$id)->where('leader',2)->whereIn('client_id',$checkIfVice)->count();

                // if(($clientId != $group->leader && $groupVice < 1) || count($request->clients) == 1){
                if(($clientId != $group->leader && $groupVice < 1)){
                    if($service_status == 'complete'){
                        $status = "<b><h4>".$service_status."</h4></b>";
                        $status_cn = "<b><h4> 已完成  </h4></b>";
                    }
                    if($service_status == 'on process'){
                        $status = "<b><h5>".$service_status."</h5></b>";
                        $status_cn = "<b><h5> 办理中 </h5></b>";
                    }
                    if($service_status == 'pending'){
                        $status = "<b><h6>".$service_status."</h6></b>";
                        $status_cn = "<b><h6> 待办 </h6></b>";
                    }
                    $body = 'New Service '.$translated->detail.' was added. '.$chosen.$missing.'<br><br>- Service status is '.$status;
                    $body_cn = '新的服务 '.$translated->detail.' 已添加. '.$chosen_cn.$missing_cn.'<br><br>- 服务状态为 '.$status_cn;
                    $main = $body;
                    $main_cn = $body_cn;
                    Common::savePushNotif($clientId, $title, $body,'Service',$clientService->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
                    //$cls = $cls.$client->first_name." ".$client->last_name." (".$oldNewArray[$i]." package), ";
                    $cls = $cls.$client->first_name." ".$client->last_name.", ";
                }
                else{
                    $cls = $cls.$client->first_name." ".$client->last_name.", ";
                    // $cls = $cls."your ".$oldNewArray[$i]." package, ";
                }
                $ctr++;
            }
            $result = array();
            $result_cn = array();

            foreach ($msg as $m) {
                $result[$m['msg']][] = $m;
                $result_cn[$m['msg_cn']][] = $m;
            }
            //\Log::info($result);
            $keys = array_keys($result);
            $keys_cn = array_keys($result_cn);
            $perclient = [];
            $perclient_cn = [];
            $count = 0;
            foreach ($keys as $k) {
                $perdocs = $result[$keys[$count]];
                $names = '';
                $status = '';
                $status_cn = '';
                foreach($perdocs as $p){
                    $names .=$p['client'].", ";
                    $status = $p['status'];
                }
                if($status == 'complete'){
                    $status = "<b><h4>".$status."</h4></b>";
                    $status_cn = "<b><h4> 已完成  </h4></b>";
                }
                if($status == 'on process'){
                    $status = "<b><h5>".$status."</h5></b>";
                    $status_cn = "<b><h5> 办理中 </h5></b>";
                }
                if($status == 'pending'){
                    $status = "<b><h6>".$status."</h6></b>";
                    $status_cn = "<b><h6> 待办 </h6></b>";
                }
                $perclient[] = substr($keys[$count], 0, -2)." for <b>".$names."</b> service status is now ".$status.".";
                $perclient_cn[] = substr($keys_cn[$count], 0, -2)." 为 <b>".$names."</b> 服务状态现在为 ".$status_cn.".";
                $count++;
            }

            //Combined Notif
            if($clientId == $group->leader || count($request->clients) >= 1){
                // $cls = " was added to ".$cls;
                $clientNames = substr($cls, 0, -2);
                $nms = explode(", ", $clientNames);
                sort($nms);
                // $nms = implode (", ", $nms);
                $clientNames = '';
                $crt = 1;
                foreach($nms as $n){
                    $clientNames .=$crt.".".$n." ";
                    $crt++;
                }
                $nms = $clientNames;
                //\Log::info($nms);
                $cls = "for ".substr($cls, 0, -2);
                if($dc!= ''){
                    $dc .= " each";
                }
                $totalperservice *=count($request->clients);
                // $main = 'New Service '.$translated->detail.' was added worth Php'.$service->cost." each".$dc.' with the total price of Php'.$totalperservice." as part of batch ".Carbon::now()->format('M d,Y').".";
                $ttlcost = $service->cost + $service->charge + $service->tip ;
                $main = 'New Service '.$translated->detail." was added as part of batch ".Carbon::now()->format('M d,Y').".";
                $main_cn = '新的服务 '.$cnserv.' 添加了总价Php'.$service->cost.$dcn.' 为'.Carbon::now()->format('M d,Y').'总体服务的一部分';
                foreach($perclient as $per){
                    $main = $main."<br><br>-".$per;
                }
                foreach($perclient_cn as $per_cn){
                    $main_cn = $main_cn."<br><br>-".$per_cn;
                }
                //$main = $translated->detail;
                $body = $main;
                $body_cn = $main_cn;
                $title = "New Service";
                $title_cn = "新的服务";

                $parent_type = 1;
                $sDate = Carbon::now()->format('m/d/Y');
                $parent = $nms;

                Common::savePushNotif($g->leader, $title, $body,'GService',$sDate."--".$g->tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$nms);

                //send push notif to vice leaders
                // if($leader != null){
                    $vices = GroupMember::where('group_id',$g->id)->where('leader',2)->get();
                    foreach($vices as $v){
                        $par = $nms;
                        $parent = $v->client_id."-".$par;
                        Common::savePushNotif($v->client_id, $title, $body,'GService',$sDate."--".$g->tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$nms);
                    }
                // }
            }

        }

        $collect_clients = rtrim($collect_clients, ', ');
        $collect_clients = implode(',', array_keys(array_flip(explode(',', str_replace(' ', '', $collect_clients)))));
        $cls = explode(',',$collect_clients);
        $unq_client = '';
        foreach($cls as $c){
            $client = User::findOrFail($c)->makeHidden([ 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
            $unq_client .= '<b>[' . $client->id . ']</b> ' . $client->full_name.',';
        }
        $unq_client = rtrim($unq_client, ',');

        $collect_services = rtrim($collect_services, ', ');
        $collect_services_cn = rtrim($collect_services_cn, ', ');
        $collect_service_id = rtrim($collect_service_id, ', ');
        $ifMulti = substr_count($collect_services, ",");
        $ifMulti2 = substr_count($unq_client, ",");
        // if($ifMulti > 0 || $ifMulti2 > 0){
        //     //$getcs = ClientService::limit(2)->pluck('id');
        //     $collect_service_id = str_replace(" ", "", $collect_service_id);
        //     //\Log::info($collect_service_id);   
        //     $csid = explode(',', $collect_service_id);
        //     $getcs = ClientService::whereIn('id',$csid)->select('id','detail','service_id','client_id','charge','cost','tip','status','active')->orderBy('service_id')->get();
        //     $serv_list = '<div class="row">';
        //     $current_id = 0;
        //     $old_detail = '';
        //     $old_detail_cn = '';
        //     foreach($getcs as $gcs){
        //         // $ttl = ' - Total Charge : 0';
        //         // if($gcs->status == 'complete'){
        //         //     $ttl = ' - Total Charge : '.($gcs->cost + $gcs->charge + $gcs->tip);
        //         // }

        //         if($current_id == 0 || $current_id != $gcs->service_id){
        //             if($current_id != 0 && $serv_list != '<div class="row">'){
        //                 $serv_list .= '</div>';
        //             }
        //             $current_id = $gcs->service_id;
        //             $serv_list .= '<div class="col-xs-6"><br><b>'.$gcs->detail.'</b>';
        //             //$serv_list .= '<br><b>Client #'.$gcs->client_id.'</b> - '.$gcs->status.' - Total Charge : '.($gcs->cost + $gcs->charge + $gcs->tip);
        //             $serv_list .= '<br><b>Client #'.$gcs->client_id.'</b> - '.$gcs->status.' - Total Charge : 0';
        //         }
        //         else{
        //             //$serv_list .= '<br><b>Client #'.$gcs->client_id.'</b> - '.$gcs->status.' - Total Charge : '.($gcs->cost + $gcs->charge + $gcs->tip);
        //             $serv_list .= '<br><b>Client #'.$gcs->client_id.'</b> - '.$gcs->status.' - Total Charge : 0';
        //         }

        //         $update_log = TransactionLogs::where('service_id',$gcs->id)->where('type','service')->first();
        //         if($update_log){
        //             $old_detail .= $update_log->detail.'<br><br>'.$update_log->old_detail;
        //             $old_detail_cn .= $update_log->detail_cn.'<br><br>'.$update_log->old_detail_cn;
        //         }
        //     }
        //     $serv_list .= '</div>';

        //     //\Log::info($getcs);

        //     $log_data['client_id'] = $id;
        //     $log_data['group_id'] = $id;
        //     $log_data['service_id'] = 0;
        //     $log_data['tracking'] = $collect_service_id;
        //     $log_data['user_id'] = Auth::user()->id;
        //     $log_data['type'] = 'service';
        //     $log_data['amount'] = 0;
        //     $log_data['display'] = 0;
        //     $log_data['balance'] = $this->groupCompleteBalance($id);
        //     $log_data['detail'] = 'Added a service <strong>' . $collect_services . '</strong> for client/s ' . $unq_client .'.<small></small><br>'.$serv_list;
        //     $log_data['old_detail'] = $old_detail;
        //     $log_data['old_detail_cn'] = $old_detail_cn;
        //     $log_data['detail_cn'] = 'Added a service <strong>' . $collect_services_cn . '</strong> for client/s ' . $unq_client .'.<small></small><br>'.$serv_list;
        //     Common::saveTransactionLogs($log_data);
        // }

        $collect_service_id = str_replace(" ", "", $collect_service_id);
        $csid = explode(',', $collect_service_id);
        $group_array = array_chunk($csid, $ifMulti+1);
        for($i = 0; $i < $ifMulti+1; $i++){
            $getcs = ClientService::whereIn('id',$group_array[$i])->select('id','detail','service_id','client_id','charge','cost','tip','status','active')->orderBy('service_id')->get();


            $serv_list = '<div>';
            $current_id = 0;
            $total = 0;
            $old_detail = '';
            $old_detail_cn = '';
            $collect_client_service_ids = '';
            $client_service_id = '';
            foreach($getcs as $gcs){
                $client = User::findOrFail($gcs->client_id)->makeHidden([ 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
                $unq_client = '<b>[' . $client->id . ']</b> ' . $client->full_name;

                $collect_client_service_ids .= $gcs->id.',';
                $client_service_id = $gcs->id;

                $total += ($gcs->cost + $gcs->charge + $gcs->tip + $gcs->com_agent + $gcs->com_client) - ($gcs->discount_amount);


                // $checkstat = ($gcs->status == 'complete' ? 'complete-status' : $gcs->status == 'on process' ? 'on-process-status' : $gcs->status == 'pending' ? 'pending-status' : '');

                $checkstat = '';

                if($gcs->status == 'complete'){
                    $checkstat = 'complete-status';
                }
                else if($gcs->status == 'on process'){
                    $checkstat = 'on-process-status';
                }
                else if($gcs->status == 'pending'){
                    $checkstat = 'pending-status';
                }

                $stat = '<small class="label '.$checkstat.'">'.strtoupper($gcs->status).'</small>';
                if($current_id == 0 || $current_id != $gcs->service_id){
                    if($current_id != 0 && $serv_list != '<div>'){
                        $serv_list .= '</div>';
                    }
                    $current_id = $gcs->service_id;
                    $serv_list .= '<div>';
                    $serv_list .= ''.$stat.' '.$unq_client;
                }
                else{
                    $serv_list .= '<br>'.$stat.' '.$unq_client;
                }

                $update_log = TransactionLogs::where('service_id',$gcs->id)->where('type','service')->first();
                if($update_log){
                    $serv_list .= '<br><br>&bull; '.$update_log->detail.'<br><br>'.$update_log->old_detail;
                    //$old_detail_cn .= $update_log->detail_cn.'<br><br>'.$update_log->old_detail_cn;
                }
            }
            $serv_list .= '</div>';

            //\Log::info($getcs);
            $collect_client_service_ids = rtrim($collect_client_service_ids, ',');
            $log_data['client_id'] = $id;
            $log_data['group_id'] = $id;
            $log_data['service_id'] = $client_service_id;
            $log_data['tracking'] = $collect_client_service_ids;
            $log_data['user_id'] = Auth::user()->id;
            $log_data['type'] = 'service';
            $log_data['amount'] = 0;
            $log_data['display'] = 0;
            $log_data['balance'] = $this->groupCompleteBalance($id);
            $log_data['detail'] = $serv_list;
            $log_data['detail_cn'] = $serv_list;
            $log_data['old_detail'] = $old_detail;
            $log_data['old_detail_cn'] = $old_detail_cn;
            $log_data['total'] = $total;
            Common::saveTransactionLogs($log_data);
        }

        // ******************** Document Tracker ******************** //
            DocumentLogsController::share($request->clients);
        // ******************** End of Document Tracker ******************** //

        return json_encode([
            'success' => true,
            'message' => 'Service/s successfully added to group',
            'clientsId' => $request->clients,
            'clientServicesId' => $clientServicesIdArray,
            'trackings' => $trackingArray
        ]);
    }

    private function checkPackageComplete($pack_id){
        $package_complete = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%complete%')
            ->count();

        return $package_complete;
    }

    private function checkPackagePending($pack_id){
        $package_pending = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%pending%')
            ->count();

        return $package_pending;
    }

    private function checkPackageOnProcess($pack_id){
        $package_onprogress = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%on process%')
            ->count();

        return $package_onprogress;
    }

    private function check_package_updates($pack_id){
        $stat = 0; // empty

        if($this->checkPackageComplete($pack_id)>0){
            $stat = 4; // complete
        }
        if($this->checkPackageOnProcess($pack_id)>0){
            $stat = 1; // process
        }
        if($this->checkPackagePending($pack_id)>0){
            $stat = 2; // pending
        }

        $data = array('status' => $stat);

        DB::connection('visa')
            ->table('packages')
            ->where('tracking', $pack_id)
            ->update($data);
    }

    public function addFunds(AddFundsValidation $request, $id) {
        $group = Group::findOrFail($id);
        $leaderId = $group->leader;
        $logDate = Carbon::now()->format('m/d/Y h:i:s A');
        $amount = $request->amount;
        $details = "";
        $log_data['client_id'] = $leaderId;
        $log_data['group_id'] = $id;
        $log_data['service_id'] = 0;
        $log_data['tracking'] = null;
        $log_data['user_id'] = Auth::user()->id;
        $log_data['amount'] = $amount;
        $log_data['detail'] = $details;
        if($request->storage=='alipay'){
            $amount = $amount - ($amount*0.0175);
        }

        $ali_ref = '';
        $bank_name = '';
        if($request->alipay_reference != ''){
            $ali_ref = '<br>Reference # : '.$request->alipay_reference;
        }
        if($request->storage=='bank'){
            $bank_name = ' - '.$request->bank_type;
        }

        $d = Carbon::now('Asia/Manila');
        $notifdate = $d->toDateString();
        $notiftime = $d->toTimeString();
        $dayname = $d->format('l');

        if($request->option == 'Deposit') {
            $depo = ClientTransactions::create([
                'client_id' => $leaderId,
                'amount'    => $amount,
                'type'      => 'Deposit',
                'group_id'  => $id,
                'tracking'  => null,
                'log_date'  => $logDate,
                'storage_type'=>($request->storage!='cash') ? $request->bank_type : null,
                'alipay_reference'=>($request->storage=='alipay') ? $request->alipay_reference : null
            ]);

            //add to financing
			$f = new Finance;
			$f->type = 'deposit';
            $f->record_id = $depo->id;
            $f->cat_type = 'process';
			$f->cat_storage = $request->storage;
            $f->branch_id = $group->branch_id;
            $f->trans_desc = Auth::user()->first_name.' received deposit from group '.$group->name;
            if($request->storage=='alipay'){
                $f->trans_desc = Auth::user()->first_name.' received deposit from group '.$group->name.' with alipay reference: '.$request->alipay_reference;
            }
			(($request->storage=='cash') ? $f->cash_client_depo_payment = $amount: $f->bank_client_depo_payment = $amount );
			$f->storage_type = ($request->storage!='cash') ? $request->bank_type : null;
			$f->save();

            //save transaction logs for group deposit
            $log_data['type'] = 'deposit';
            $detail = 'Deposited an amount of Php<strong>'.$amount.'</strong>.';
            $detail_cn = '预存了款项 Php<strong>'.$amount.'</strong>.';
            $log_data['detail'] = $detail;
            $log_data['detail_cn'] = $detail_cn;
            $groupBalance = $this->groupCompleteBalance($id);
            $log_data['balance'] = $groupBalance;
            Common::saveTransactionLogs($log_data);

            //push notif
            $title = "Deposit received";
            $title_cn = "";
            $body = 'Thank you, we received your deposit worth Php'.$amount.' for group '.$group->name.'. <br><br><b>'.ucfirst($request->storage).'</b>'.$bank_name.$ali_ref;
            $body_cn = "已折扣 Php".$amount;
            $main = $body;
            $main_cn = $body_cn;
            $parent = $notifdate."-".$leaderId;
            $parent_type = 1;
            Common::savePushNotif($leaderId, $title, $body,'Funds',$dayname."--".$leaderId,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

            return json_encode([
                'success' => true,
                'message' => 'Funds successfully added'
            ]);
        } elseif($request->option == 'Payment') {
            $payment = ClientTransactions::create([
                'client_id' => $leaderId,
                'amount'    => $amount,
                'type'      => 'Payment',
                'group_id'  => $id,
                'tracking'  => null,
                'log_date'  => $logDate,
                'storage_type'=>($request->storage!='cash') ? $request->bank_type : null,
                'alipay_reference'=>($request->storage=='alipay') ? $request->alipay_reference : null
            ]);

            //save to financing
			$f = new Finance;
            $f->type = 'payment';
            $f->record_id = $payment->id;
			$f->cat_type = 'process';
			$f->cat_storage = $request->storage;
            $f->branch_id = $group->branch_id;
            $f->trans_desc = Auth::user()->first_name.' received payment from group '.$group->name;
            if($request->storage=='alipay'){
                $f->trans_desc = Auth::user()->first_name.' received payment from group '.$group->name.' with alipay reference: '.$request->alipay_reference;
            }
			(($request->storage=='cash') ? $f->cash_client_depo_payment = $amount: $f->bank_client_depo_payment = $amount );
			$f->storage_type = ($request->storage!='cash') ? $request->bank_type : null;
			$f->save();

            // save transaction logs for group payment
            $log_data['type'] = 'payment';
            $detail = 'Paid an amount of Php<strong>'.$amount.'</strong> .';
            $detail_cn = '已支付 Php<strong>'.$amount.'</strong> .';
            $log_data['detail'] = $detail;
            $log_data['detail_cn'] = $detail_cn;
            $groupBalance = $this->groupCompleteBalance($id);
            $log_data['balance'] = $groupBalance;
            Common::saveTransactionLogs($log_data);

            //push notif
            $title = "Payment received";
            $title_cn = "";
            $body = 'Thank you, we received your payment worth Php'.$amount.' for group '.$group->name.'. <br><br><b>'.ucfirst($request->storage).'</b>'.$bank_name.$ali_ref;
            $body_cn = "已折扣 Php".$amount;
            $main = $body;
            $main_cn = $body_cn;
            $parent = $notifdate."-".$leaderId;
            $parent_type = 1;
            Common::savePushNotif($leaderId, $title, $body,'Funds',$dayname."--".$leaderId,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

            return json_encode([
                'success' => true,
                'message' => 'Funds successfully added'
            ]);
        } elseif($request->option == 'Discount') {
            ClientTransactions::create([
                'client_id'  => $leaderId,
                'amount'     => $amount,
                'type'       => 'Discount',
                'group_id'   => $id,
                'service_id' => null,
                'reason'     => $request->reason,
                'tracking'   => null,
                'log_date'   => $logDate,
                'storage_type'=>($request->storage!='cash') ? $request->bank_type : null,
                'alipay_reference'=>($request->storage=='alipay') ? $request->alipay_reference : null
            ]);

            if($request->clearbalance){
                $group = Group::findOrFail($id);
                if($group->temp_balance < 0){
                    $group->temp_balance += $amount;
                    $group->save();
                }
            }

            $log_data['type'] = 'discount';
            $groupBalance = $this->groupCompleteBalance($id);
            $log_data['balance'] = $groupBalance;
            $detail = 'Discounted an amount of Php<strong>'.$amount.'</strong> with the reason of <strong>"'.$request->reason.'"</strong>.';
            $detail_cn = '给于折扣 Php<strong>'.$amount.'</strong> 因为 <strong>"'.$request->reason.'"</strong>.';
            $log_data['detail'] = $detail;
            $log_data['detail_cn'] = $detail_cn;
            Common::saveTransactionLogs($log_data);

             // push notif
            $title = "Received Group Discount";
            $title_cn = "";
            $body = 'You received a discount worth Php'.$amount.' for group '.$group->name.'.';
            $body_cn = "已折扣 Php".$amount;
            $main = $body;
            $main_cn = $body_cn;
            $parent = $notifdate."-".$leaderId;
            $parent_type = 1;
            Common::savePushNotif($leaderId, $title, $body,'Funds',$dayname."--".$leaderId,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
            return json_encode([
                'success' => true,
                'message' => 'Funds successfully added'
            ]);
        } elseif($request->option == 'Refund') {
            $groupBalance = $this->getGroupBalance($id);
            if(($groupBalance > 0)||($groupBalance > $amount)){
                $refund = ClientTransactions::create([
                    'client_id'     => $leaderId,
                    'amount'        => $amount,
                    'type'          => 'Refund',
                    'group_id'      => $id,
                    'reason'        => $request->reason,
                    'tracking'      => null,
                    'log_date'      => $logDate,
                    'storage_type'=>($request->storage!='cash') ? $request->bank_type : null,
                    'alipay_reference'=>($request->storage=='alipay') ? $request->alipay_reference : null
                ]);

                //save to financing
                $f = new Finance;
                $f->type = 'refund';
                $f->record_id = $refund->id;
                $f->cat_type = 'process';
                $f->cat_storage = $request->storage;
                $f->cash_client_refund = $amount;
                $f->branch_id = $group->branch_id;
                $f->trans_desc = Auth::user()->first_name.' refunded from group '.$group->name.' for the reason of '.$request->reason;
                $f->storage_type = ($request->storage!='cash') ? $request->bank_type : null;
                $f->save();

                //save to transaction logs for group refund
                $log_data['type'] = 'refund';
                //$details = 'Got a refund of Php<strong>' . $amount . '</strong> with a reason of <strong>' . $request->reason . '</strong>';
                $detail = 'Refunded an amount of Php<strong>'.$amount.'</strong> with the reason of <strong>"'.$request->reason.'"</strong>.';
                $detail_cn = '退款了 Php'.$amount.' 因为 <strong>"'.$request->reason.'"</strong>.';
                $log_data['amount'] = '-'.$amount;
                $log_data['detail'] = $detail;
                $log_data['detail_cn'] = $detail_cn;
                $groupBalance = $this->groupCompleteBalance($id);
                $log_data['balance'] = $groupBalance;
                Common::saveTransactionLogs($log_data);

                // push notif
                $title = "Made a Refund";
                $title_cn = "";
                $body = 'You\'ve made a refund worth Php'.$amount.' for group '.$group->name.'. <br><br><b>'.ucfirst($request->storage).'</b>'.$bank_name.$ali_ref;
                $body_cn = "已折扣 Php".$amount;
                $main = $body;
                $main_cn = $body_cn;
                $parent = $notifdate."-".$leaderId;
                $parent_type = 1;
                Common::savePushNotif($leaderId, $title, $body,'Funds',$dayname."--".$leaderId,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

                return json_encode([
                    'success' => true,
                    'message' => 'Funds successfully added'
                ]);

            }elseif(($groupBalance <= 0)||($groupBalance < $amount)){
                 return json_encode([
                    'success' => false,
                    'message' => 'Insufficient Balance'
                ]);
            }
        }
        elseif($request->option == 'Transfer') {
            date_default_timezone_set("Asia/Manila");
            $date = date('m/d/Y h:i:s A');
            $transfer_type = $request->transfer_type;
            $selClient = $request->selected_client;
            $selGroup = $request->selected_group;

            $total_balance = $this->groupCompleteBalance($id);

            $trans = new TransferBalance;
            $trans->transfer_from = $id;
            $trans->from_type = 'group';
            $trans->transfer_to = ($transfer_type == 'client' ? $selClient : $selGroup);
            $trans->to_type = $transfer_type;
            $trans->amount = $amount;
            $trans->reason = $request->reason;
            $trans->log_date = $date;
            $trans->save();

            $transferred = $trans->transfer_to;
            if($transfer_type == 'group'){
                $transferred = Group::where('id',$selGroup)->first()->name;
            }
            if($transfer_type == 'client'){
                $cl_usr = User::where('id',$selClient)->select('id','first_name','last_name')->first();
                $transferred = $cl_usr->first_name.' '.$cl_usr->last_name;
            }
            // Refund amount to group
            $refund = new ClientTransactions;
            $refund->client_id = $group->leader;
            $refund->amount = $amount;
            $refund->type = 'Refund';
            $refund->group_id = $id;
            $refund->reason = $request->reason;
            $refund->tracking = null;
            $refund->log_date = $date;
            $refund->save();


            $notes = 'Refunded an amount of Php<strong>'.$amount.'</strong>, transferred to '.$transfer_type.' '.$transferred;
            $notes_cn = '退款 Php<strong>'.$amount.'</strong>, 转移到了客户 '.$transferred;
            $total_balance = $this->groupCompleteBalance($id);
            $log_data = array(
                'client_id' => $group->leader,
                'service_id' => 0,
                'group_id' => $id,
                'type' => 'refund',
                'amount' => '-'.$amount,
                'balance' => $total_balance,
                'detail'=> $notes,
                'detail_cn'=> $notes_cn,
            );
            Common::saveTransactionLogs($log_data);

            // push notif
            $title = "Made a Refund";
            $title_cn = "";
            $body = 'You\'ve made a refund worth Php'.$amount.', transferred to '.$transfer_type.' '.$transferred.'.';
            $body_cn = "已折扣 Php".$amount;
            $main = $body;
            $main_cn = $body_cn;
            $parent = $notifdate."-".$group->leader;
            $parent_type = 1;
            Common::savePushNotif($group->leader, $title, $body,'Funds',$dayname."--".$group->leader,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

            $transTo = $selClient;
            $grid = 0;
            if($transfer_type == 'group'){
                $transTo = Group::where('id',$selGroup)->first()->leader;
                $grid = $selGroup;
                $total_balance = $this->groupCompleteBalance($grid);
            }

            // Deposit amount to client or group selected
            $depo = new ClientTransactions;
            $depo->client_id = $transTo;
            $depo->amount = $amount;
            $depo->type = 'Deposit';
            $depo->group_id = $grid;
            $depo->tracking = null;
            $depo->log_date = $date;
            $depo->save();

           //for financing
            $finance = new Finance;
            $finance->user_sn = Auth::user()->id;
            $finance->type = 'transfer';
            $finance->record_id = $trans->id;
            $finance->cat_type = "process";
            $finance->cat_storage = $request->storage;
            $finance->branch_id = $group->branch_id;
            ((strcasecmp($request->storage,'cash')==0) ? $finance->cash_client_depo_payment = $amount : $finance->bank_client_depo_payment = $amount);
            ((strcasecmp($request->storage,'cash')==0) ? $finance->cash_client_refund = $amount : $finance->bank_cost = $amount);
            $finance->trans_desc = Auth::user()->first_name.' transferred funds from group '.$group->name.' to '.$transfer_type.' '.$transferred.'.';
            $finance->save();

            $total_balance = $this->clientBalance($trans->transfer_to);
            if($transfer_type == 'group'){
                $total_balance = $this->groupCompleteBalance($selGroup);
            }

            $notes = 'Deposited an amount of Php<strong>'.$amount.'</strong> from group '.$group->name.'.';
            $notes_cn = '预存了款项 Php<strong>'.$amount.'</strong> 从 团体'.$group->name.'.';
            $log_data = array(
                'client_id' => $transTo,
                'service_id' => 0,
                'group_id' => $grid,
                'type' => 'deposit',
                'amount' => $amount,
                'balance' => $total_balance,
                'detail'=> $notes,
                'detail_cn'=> $notes_cn,
            );
            Common::saveTransactionLogs($log_data);


            $transfrom = Group::where('id',$id)->first()->name;

            $title = "Deposit received";
            $title_cn = "";
            $body = 'Thank you, we received your deposit worth Php'.$amount.' from group '.$transfrom.'.';
            $body_cn = "已折扣 Php".$amount;
            $main = $body;
            $main_cn = $body_cn;
            $parent = $notifdate."-".$transTo;
            $parent_type = 1;
            Common::savePushNotif($transTo, $title, $body,'Funds',$dayname."--".$transTo,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
            return json_encode([
                    'success' => true,
                    'message' => 'Funds successfully transferred'
                ]);
        }
    }

    public function deleteMember(Request $request, $id) {
        $leader = GroupMember::where('group_id', $id)
            ->where('client_id', $request->memberId)
            ->where('leader', 1)
            ->count();

        if($leader != 0) {
            $success = false;
            $message = 'Cannot delete leader of the group';
        } else {
            $services = ClientService::where('group_id', $id)
                ->where('client_id', $request->memberId)
                ->where('active', 1)
                ->count();

            if($services != 0) {
                $success = false;
                $message = 'Cannot delete member/s that has registered service/s.';
            } else {
                GroupMember::where('group_id', $id)->where('client_id', $request->memberId)->delete();

                // Action log
                $member = User::findOrFail($request->memberId)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
                $memberLabel = '[' . $member->id . '] ' . $member->full_name;
                $details = 'Deleted <strong>' . $memberLabel . '</strong> as member of the group.';
                Common::saveActionLogs(0, 0, $details, $id);

                $success = true;
                $message = 'Member successfully deleted';
            }
        }

        return json_encode([
            'success' => $success,
            'message' => $message
        ]);
    }

    public function show($groupId) {
        return Group::where('id',$groupId)->with('user')->first();
    }

    public function editAddress(EditAddressValidation $request,$groupId) {
        $group = Group::findOrFail($groupId);
        $oldAddress = $group->address;
        $oldName = $group->name;
        $oldNumber = User::findOrFail($group->leader)->contact_number;

        $group->update(['address' => $request->address]);
        $group->update(['name' => $request->name]);
        User::findOrFail($group->leader)->update(['contact_number' => $request->number]);

        $details = '';
        // Action log
        if($oldName != $request->name) {
            $details .= 'Change group name from <strong>' . $oldName . '</strong> to <strong>' . $request->name .'</strong>.';
        }
        if($oldAddress != $request->address) {
            $details .= 'Change group address from <strong>' . $oldAddress . '</strong> to <strong>' . $request->address .'</strong>.';
        }
        if($oldNumber != $request->number) {
            $details .= 'Change group contact number from <strong>' . $oldNumber . '</strong> to <strong>' . $request->number .'</strong>.';
        }

        if($details!=''){
            Common::saveActionLogs(0, 0, $details, $groupId);
        }

        return json_encode([
            'success' => true,
            'message' => 'Address successfully updated'
        ]);
    }

    public function editServices(EditServiceValidation $request, $groupId) {
       // \Log::info($request->all());
        $collection = ClientService::whereIn('id', $request->clientServicesId);
        $oldCollect = $collection;
        $clientServices = $collection->get();

        $getGroup = Group::findOrFail($groupId);

        $userId = Auth::user()->id;
        $cdetail = '';
        $colClients = [];
        $cls = '';

        $cancelledClientServicesArr = [];

        foreach($clientServices as $clientService) {
            $updCollect = '';
            $forleadCollect ='';
            $forleadChinese ='';
            $log = '';
            $translog = '';
            $translog_cn = '';
            $translog1 = '';
            $translog1_cn = '';
            $translog2 = '';
            $translog2_cn = '';
            $dt = Carbon::now();
            $dt = $dt->toDateString();
            //$user = Auth::user();
            $author = $request->note.' - '. Auth::user()->first_name.' <small>('.$dt.')</small>';
            if($request->note==''){
                $author = '';
            }
            $note = $clientService->remarks;
            if($note!=''){
                if($request->note!=''){
                    $note = $note.'</br>'.$author;
                }
            }
            else{
                $note = $author;
            }

            $srv = ClientService::findOrFail($clientService->id);
            $oldStatus = $srv->status;
            $translated = Service::where('id',$srv->service_id)->first();
            $cnserv =$srv->detail;
            if($translated){
                $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
            }
            $cdetail = $cnserv;

            $updCollect = '';
            $updChinese = '';

			//insert budget output for financing2
			// if($srv->cost != $request->cost || $srv->tip != $request->additional_charge){
			// 	BudgetOutput::create([
			// 			'user_id' => Auth::user()->id,
			// 			'client_id' => $srv->client_id,
			// 			'service_id' => $srv->service_id,
			// 			'cost' => $request->cost,
			// 			'extra_cost' => $request->additional_charge,
			// 	]);
			// }

        $oldDiscount = 0;
        $newDiscount = 0;
        $dcflag = false;
        $amount = 0;   
        $discnotes = '';   
        $discnotes_cn = '';   
        if($request->discount > 0) {
            $__oldDiscount = null;
            
            $dc = ClientTransactions::where("service_id",$srv->id)->where('type','Discount')->withTrashed()->first();
           
            if($dc){
                $__oldDiscount = $dc->amount;
                $oldDiscount = $dc->amount;
                if($dc->amount != $request->discount){
                    
                    $newDiscount = $request->discount;
                    //$forleadCollect .= "Update Discount from Php".$dc->amount."to Php".$request->discount.".";
                    //$forleadChinese .= "更新折扣 从 Php".$dc->amount."到 Php".$request->discount.".";

                    $discnotes = ' Updated discount from Php'.$dc->amount.' to Php'.$request->discount.', ';
                    $discnotes_cn = ', 更新折扣 从 Php'.$dc->amount.' 到 Php'.$request->discount.', ';
                    $amount = $request->discount - $dc->amount;

                    $dc->amount =  $request->get('discount');
                    $dc->reason =  $request->get('reason');
                    $dc->deleted_at = null;
                    $dc->save();
                }else{
                    if($dc->reason != $request->reason){
                        $dc->reason =  $request->get('reason');
                        $dc->save();
                    }
                }
            }
            else{
                
                $newDiscount = $request->discount;
                ClientTransactions::create([
                    'client_id' => $getGroup->leader,
                    'type' => 'Discount',
                    'amount' => $request->get('discount'),
                    'group_id' => $getGroup->id,
                    'service_id' => $srv->id,
                    'reason' => $request->get('reason'),
                    'tracking' => $srv->tracking,
                    'log_date' => Carbon::now()->format('m/d/Y h:i:s A')
                ]);
                $discnotes = 'Discounted an amount of Php'.$request->get('discount').', ';
                $discnotes_cn = ', 已折扣额度 Php'.$request->get('discount').', ';
                $amount = $request->get('discount');
            }
            $dcflag = true;


            if($__oldDiscount == $request->get('discount')){
                $oldDiscount = $__oldDiscount;
                $newDiscount = $request->get('discount'); 
            }
       
        } else {
            $discountExist = ClientTransactions::where('service_id', $srv->id)->where('type','Discount')->first();
            if($discountExist && $request->active != 0){
                $oldDiscount = $discountExist->amount;
                $newDiscount = 0;
                $discnotes = ', remove discount of Php'.$discountExist->amount.', ';
                $discnotes_cn = ', 移除折扣 Php'.$discountExist->amount.', ';

                ClientTransactions::where('client_id', $srv->client_id)
                    ->where('group_id', $srv->group_id)
                    ->where('service_id', $srv->id)
                    ->where('tracking', $srv->tracking)
                    ->forceDelete();
            }
        }


            $old_total_charge = $srv->cost + $srv->tip + $srv->charge + $srv->com_client + $srv->com_agent;
            $new_total_charge = ($request->cost != null ? $request->cost : $srv->cost) + 
                                ($request->additional_charge != null ? $request->additional_charge : $srv->tip) + 
                                ($request->charge != null ? $request->charge : $srv->charge)
                                + $srv->com_client + $srv->com_agent;
                                //$request->additional_charge + $request->charge;

            if($newDiscount > 0 || $oldDiscount > 0 ){
                $old_total_charge -= $oldDiscount;
                $new_total_charge -= $newDiscount;
            }
            $service_status = $request->status;
            //\Log::info($old_total_charge . ' '. $new_total_charge);
            if ($old_total_charge != $new_total_charge || $service_status == 'complete') :
                if($request->get('active') == 1) { // Enabled
                    $toAmount = $new_total_charge;
                } elseif($request->get('active') == 0) { // Disabled
                    $toAmount = 0;
                }

                //if($clientService->status == 'complete'){            
                    $translog1 = 'Total service charge from Php' . ($old_total_charge) . ' to Php' . $toAmount;
                    $translog1_cn = '总服务费从 Php' . ($old_total_charge) . ' 到 Php' . $toAmount;
                //}
                if($service_status == 'complete'){
                    $translog1 = 'Total service charge is Php' . $toAmount;
                    $translog1_cn = '总服务费 Php' . $toAmount;
                }

                //$newVal +=$new_total_charge;
                //$oldVal +=$old_total_charge;
            endif;

            if ($old_total_charge == $new_total_charge && $service_status != 'complete') :
                $toAmount = 0;
               
                if($clientService->status == 'complete'){ 
                    $translog1 = 'Total service charge from Php' . ($old_total_charge) . ' to Php' . $toAmount;
                    $translog1_cn = '总服务费从 Php' . ($old_total_charge) . ' 到 Php' . $toAmount;
                }
                else{
                    $translog1 = '';
                    $translog1_cn = '';
                }

                //$newVal +=$new_total_charge;
                //$oldVal +=$old_total_charge;
            endif;

            if ($old_total_charge == $new_total_charge && $service_status == $clientService->status) :
                $translog1 = '';
                $translog1_cn = '';
            endif;

            $activer1 = (($request->get('active') == 1)? 'Active' : 'Inactive');
            $activer2 = (($clientService->active == 1)? 'Active' : 'Inactive');
            if ($clientService->active != $request->get('active')) :
                if ($log == ''):
                    $log = $log.'from '.$activer2.' to '.$activer1;
                else:
                    $log = ','.$log.'from '.$activer2.' to '.$activer1;
                endif;
                
                if($request->get('active') == 1) { // Enabled
                    $translog2 = 'Service was enabled.';
                    $translog2_cn = '服务被标记为已启用.';
                    $translog1 = 'Total service charge from Php0 to ' . 'Php'. ($new_total_charge);
                    $translog1_cn = '总服务费从 Php0 到 ' . 'Php'. ($new_total_charge);
                } elseif($request->get('active') == 0) { // Disabled
                    $translog2 = 'Service was disabled.';
                    $translog2_cn = '服务被标记为失效.';
                    $translog1 = 'Total service charge from Php'. ($new_total_charge).' to Php'.'0.';
                    $translog1_cn = '总服务费从 Php'. ($new_total_charge).' 到 Php'.'0.';
                }
            endif;

            //if theres changes in total charge ( cost + charge + tip)
            if($old_total_charge != $new_total_charge){
                $forleadCollect .= "Total Service Charge update from Php".$old_total_charge." to Php".$new_total_charge.". ";
                $forleadChinese .= "总计服务费  Php".$old_total_charge." 到 Php".$new_total_charge.". ";
            }

            //if theres changes in service status
            if($srv->status != $request->status){
                $status = $request->status;
                $status_cn = $request->status;
                if($status == 'complete'){
                    $status = "<b><h4>".$status."</h4></b>";
                    $status_cn = "<b><h4> 已完成  </h4></b>";
                }
                if($status == 'on process'){
                    $status = "<b><h5>".$status."</h5></b>";
                    $status_cn = "<b><h5> 办理中 </h5></b>";
                }
                if($status == 'pending'){
                    $status = "<b><h6>".$status."</h6></b>";
                    $status_cn = "<b><h6> 待办 </h6></b>";
                }

                $updCollect .= 'Service status is now '.$status.'. ';
                $updChinese .= '服务状态为 '.$status_cn.'. ';
            }

            //if theres changes in active/inactive
            if($srv->active != $request->active){
                 $activer1 = (($request->active == 1)? 'enabled' : 'disabled');
                 $active_cn = (($request->active == 1)? '已启用' : '为失效');
                 $updCollect .= 'Service is now '.$activer1.'.';
                 $updChinese .= '服务被标记为 '.$active_cn.'.';
                 if($activer1 == 'enabled'){
                    $forleadCollect = "Total Service Charge update from Php0 to Php".$new_total_charge.". ";
                    $forleadChinese = "总计服务费  Php0 到 Php".$new_total_charge.". ";
                 }
                 else{
                    $forleadCollect = "Total Service Charge update from Php".$new_total_charge." to Php0. ";
                    $forleadChinese = "总计服务费  Php".$new_total_charge." 到 Php0. ";
                 }
            }

            if($request->status!=null) {
                $srv->status = $request->status;
            }
            if($request->active!=null) {
                $srv->active = $request->active;
            }
            
            $srv->cost = ($request->cost != null ? $request->cost : $srv->cost);
            //$srv->charge = ($request->charge != null ? $request->charge : $srv->charge);
            $srv->tip = ($request->additional_charge != null ? $request->additional_charge : $srv->tip);
            $srv->remarks = $note;
            $srv->save();
            $oldCost = $srv->cost + $srv->charge + $srv->tip;
            $client = User::findOrFail($srv->client_id)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);

            // Task Generator
            if($oldStatus == 'pending' && $request->status == 'on process') {
                TasksController::save([$srv]);
            }

            // ******************** Document Tracker ******************** //
                if($request->get('active') == 0) {
                    $srv->update([
                        'rcv_docs' => null,
                        'rcv_docs_released' => null,
                        'report_docs' => null
                    ]);

                    $cancelledClientServicesArr[] = $srv;
                } elseif($request->get('active') == 1) {

                }
            // ******************** End of Document Tracker ******************** //

            // $msg[$ctr]['client'] = $client->first_name." ".$client->last_name;
            // $msg[$ctr]['msg'] = $chosen.$missing;
            // $msg[$ctr]['status'] = $service_status;

            if($updCollect!=''){
                if (!in_array($srv->client_id, $colClients)) {
                    $checkIfVice = [];
                    $checkIfVice[] = $srv->client_id;
                    $groupVice = GroupMember::where('group_id',$groupId)->where('leader',2)->whereIn('client_id',$checkIfVice)->count();
                    if($srv->client_id != $getGroup->leader || $groupVice < 1){
                        $title = "Update for Service ".$srv->detail;
                        $title_cn = "服务更新 ".$cnserv;
                        $body = "Update for Service ".$srv->detail.", ".$updCollect;
                        $body_cn = "服务更新 ".$cnserv.", ".$updChinese;
                        $main = "Update for Service ".$srv->detail.", ".$updCollect;
                        $main_cn = "服务更新 ".$cnserv.", ".$updChinese;
                        $parent = $srv->id;
                        $parent_type = 2;
                        Common::savePushNotif($srv->client_id, $title, $body,'Service',$srv->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
                        $cls = $cls.$client->first_name." ".$client->last_name.", ";
                    }
                    else{
                        $cls = $cls.$client->first_name." ".$client->last_name.", ";
                        //$cls = "your service, ".$cls." ";
                    }
                    $colClients[] = $srv->client_id;
                }
            }


            // Soft delete discount
                if($request->active == 0) {
                    ClientTransactions::where('group_id', $clientService->group_id)
                        ->where('client_id', $clientService->client_id)
                        ->where('service_id', $clientService->id)
                        ->where('tracking', $clientService->tracking)
                        ->delete();
                } else {
                    ClientTransactions::withTrashed()
                        ->where('group_id', $clientService->group_id)
                        ->where('client_id', $clientService->client_id)
                        ->where('service_id', $clientService->id)
                        ->where('tracking', $clientService->tracking)
                        ->restore();
                }

            // Check package updates
                ClientService::where('group_id', $clientService->group_id)
                    ->where('client_id', $clientService->client_id)
                    ->where('service_id', $clientService->service_id)
                    ->where('tracking', $clientService->tracking)
                    ->select('tracking')
                    ->get()
                    ->map(function($d) {
                        $this->check_package_updates($d->tracking);
                    });

            // Transaction Log
                // if($translog1 != '' || $translog2 != '' || $discnotes != ''){
                //     $user = Auth::user();
                //     $client = User::findOrFail($clientService->client_id);
                //     $amount = $request->cost + $request->charge + $request->additional_charge;

                //     if($request->active == 1){
                //         $amount *= -1;
                //     }

                //     $translated = Service::where('id',$clientService->service_id)->first();
                //     $cnserv =$clientService->detail;
                //     if($translated){
                //         $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                //     }
                //     $log_data['client_id'] = $clientService->client_id;
                //     $log_data['group_id'] = $clientService->group_id;
                //     $log_data['service_id'] = $clientService->service_id;
                //     $log_data['tracking'] = $clientService->tracking;
                //     $log_data['user_id'] = $user->id;
                //     $log_data['type'] = 'service';
                //     $log_data['amount'] = $amount;
                //     $log_data['balance'] = $this->groupCompleteBalance($clientService->group_id);
                //     $log_data['detail'] = 'Updated service <strong>' . $clientService->detail . '</strong> on Package Number <strong>' . $clientService->tracking . '</strong>, ' . $forleadCollect.$updCollect . ' for Client <strong>[' . $client->id . '] ' . $client->full_name . '</strong>';
                //     $log_data['detail_cn'] = '更新了服务 <strong>' . $cnserv . '</strong> 于服务包 <strong>' . $clientService->tracking . '</strong>, ' . $forleadChinese.$updChinese . '</strong> 为客户 <strong>[' . $client->id . '] ' . $client->full_name . '</strong>';
                //     Common::saveTransactionLogs($log_data);
                // }

                // \Log::info($translog1 . $translog2 . $discnotes);
                if($translog1 != '' || $translog2 != '' || $discnotes != ''){
                    $translated = Service::where('id',$clientService->service_id)->first();
                    $cnserv =$clientService->detail;
                    if($translated){
                        $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                    }
                    $client = User::findOrFail($clientService->client_id)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
                    // $notes =  $clientService->detail .': '.$discnotes .$translog1.' on Package #' . $clientService->tracking. ' for Client <strong>[' . $client->id . '] ' . $client->full_name . '</strong>' . '. ' . $translog2;
                    // $notes_cn =  $cnserv . ': '.$discnotes_cn . $translog1_cn . ' 于服务包 ' . $clientService->tracking.' 为客户 <strong>[' . $client->id . '] ' . $client->full_name . '</strong>' . '. ' . $translog2_cn;


                    $notes =  $clientService->detail .': '.$discnotes .$translog1. '. ' . $translog2;
                    $notes_cn =  $cnserv . ': '.$discnotes_cn . $translog1_cn . '. ' . $translog2_cn;

                    //$user = Auth::user();
                    $log_data = array(
                        'client_id' => $clientService->client_id,
                        'group_id' => $clientService->group_id,
                        'service_id' => $clientService->id,
                        'tracking' => $clientService->tracking,
                        'user_id' => Auth::user()->id,
                        'type' => 'service',
                        'amount' => 0,
                        'detail'=> $notes,
                        'detail_cn'=> $notes_cn,
                        'balance' => $this->groupCompleteBalance($clientService->group_id),
                    );

                    $checkTransIfGroup =  TransactionLogs::where('tracking', 'LIKE', '%'.$clientService->id.'%')
                                            ->where('display', 0)
                                            ->where('group_id', $clientService->group_id)
                                            ->first();

                    $log_data['display'] = 0; 
                    if($checkTransIfGroup){
                        $log_data['display'] = 1;
                        $notes =  ' : '.$discnotes .$translog1. '. ' . $translog2;
                        $notes_cn =  ' : '.$discnotes_cn . $translog1_cn . '. ' . $translog2_cn;
                    }

                    if($clientService->status != 'complete' && $service_status == 'complete'){
                        $log_data['id'] = '';
                        $log_data['detail'] = 'Completed Service '.$notes;
                        $log_data['detail_cn'] = '完成的服务 '.$notes_cn;   
                        $log_data['amount'] = '-'.$new_total_charge;
                        // if($request->get('active') == 0){
                        //     $log_data['amount'] = '-'.$newServiceCost;
                        // }   
                        $update_log = TransactionLogs::where('service_id',$clientService->id)->where('type','service')->first();
                        if($update_log){
                            $log_data['old_detail'] = $update_log->detail.'<small> by '.Auth::user()->first_name.' - '.$update_log->log_date.'</small><br>'.$update_log->old_detail;
                            $log_data['old_detail_cn'] = $update_log->detail_cn.'<small> by '.Auth::user()->first_name.' - '.$update_log->log_date.'</small><br>'.$update_log->old_detail_cn;
                            $update_log->update($log_data);
                            //$update_log->delete();
                        }
                        else{
                            //$origDate = $clientService->service_date;
                            //$t=time();
                            //$tym = date("g:i a",$t);
                            //$newDate = date("F j, Y", strtotime($origDate));
                            //$log_data['log_date'] = $newDate.', '.$tym;
                            Common::saveTransactionLogs($log_data);
                        }
                    }
                    else{
                        if(($clientService->status != 'complete' && $service_status != 'complete')){
                             $log_data['amount'] = 0;
                        }
                        if($clientService->status == 'complete' && $service_status != 'complete'){
                            $log_data['amount'] = '-'.$new_total_charge;
                        }
                        if($clientService->status == 'complete' && $service_status == 'complete'){
                            $log_data['amount'] = '-'.$new_total_charge;
                        }     
                        //$log_data['id'] = '';
                        $log_data['detail'] = 'Updated Service '.$notes;
                        $log_data['detail_cn'] = '服务更新 '.$notes;
                        $update_log = TransactionLogs::where('service_id',$clientService->id)->where('type','service')->first();

                        if($update_log){
                            $log_data['detail'] = 'Updated Service '.$notes;
                            $log_data['detail_cn'] = '服务更新 '.$notes_cn;
                            $log_data['old_detail'] = $update_log->detail.'<small> by '.Auth::user()->first_name.' - '.$update_log->log_date.'</small><br>'.$update_log->old_detail;
                            $log_data['old_detail_cn'] = $update_log->detail_cn.'<small> by '.Auth::user()->first_name.' - '.$update_log->log_date.'</small><br>'.$update_log->old_detail_cn;
                            $update_log->update($log_data);
                            //$update_log->delete();
                        }
                        else{
                            // $origDate = $clientService->service_date;
                            // $t=time();
                            // $tym = date("g:i a",$t);
                            // $newDate = date("F j, Y", strtotime($origDate));
                            // $log_data['log_date'] = $newDate.', '.$tym;

                            Common::saveTransactionLogs($log_data);
                        }
                    }
                }
        }

        // if($leaderIncluded){
            $cls = substr($cls, 0, -2);
            $nms = explode(", ", $cls);
            sort($nms);
            $clientNames = '';
            $crt = 1;
            foreach($nms as $n){
                $clientNames .=$crt.".".$n." ";
                $crt++;
            }
            if($updCollect!='' || $forleadCollect!=''){
                $title = "Update for Service ".$srv->detail;
                $title_cn = "服务更新 ".$cnserv;
                $body = "Update for ".$clientNames." Service ".$srv->detail;
                $body_cn = "更新了 ".$clientNames." 服务 ".$cnserv;
                $main = "Update for ".$clientNames." Service ".$srv->detail.", ".$updCollect.$forleadCollect;
                $main_cn = "更新了 ".$clientNames." 服务 ".$cnserv.", ".$updChinese.$forleadChinese;
                $parent_type = 1;
                $sDate = $srv->service_date;
                $parent = $clientNames;

                Common::savePushNotif($getGroup->leader, $title, $body,'GService',$sDate."--".$getGroup->tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$clientNames);

                $vices = GroupMember::where('group_id',$getGroup->id)->where('leader',2)->get();
                foreach($vices as $v){
                    $par = $clientNames;
                    $parent = $v->client_id."-".$par;
                    Common::savePushNotif($v->client_id, $title, $body,'GService',$sDate."--".$getGroup->tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$clientNames);
                }
            }
        // }

        // ******************** Document Tracker ******************** //
            if( count($cancelledClientServicesArr) > 0 ) {
                DocumentLogsController::cancelClientServices($cancelledClientServicesArr);
            }
        // ******************** End of Document Tracker ******************** //

        return json_encode([
            'success' => true,
            'message' => 'Service successfully updated'
        ]);
    }

    public function deposits($groupId, $limit = 0) {
        $ret =  ClientTransactions::where('group_Id',$groupId)->where('type', 'Deposit')->orderBy('id', 'desc')->get();
        if($limit > 0){
            $ret =  ClientTransactions::where('group_Id',$groupId)->where('type', 'Deposit')->orderBy('id', 'desc')->limit($limit)->get();
        }
        return $ret;
    }

    public function payments($groupId) {
        return ClientTransactions::where('group_Id',$groupId)->where('type', 'Payment')->orderBy('id', 'desc')->get();
    }

    public function refunds($groupId) {
        return ClientTransactions::where('group_Id',$groupId)->where('type', 'Refund')->orderBy('id', 'desc')->get();
    }

    public function discounts($groupId) {
        return ClientTransactions::where('group_Id',$groupId)->where('type', 'Discount')->orderBy('id', 'desc')->get();
    }

    public function combinedDiscountsPayments($groupId) {
        return ClientTransactions::where('group_Id',$groupId)->where('type', 'Deposit')->orWhere('type', 'Payment')->orderBy('id', 'desc')->get();
    }

    public function getGroupTransactions($group_id){
        $transactions = DB::connection('visa')
                    ->table('client_transactions as a')
                    ->select(DB::raw('
                        a.amount,a.type, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%M %d,%Y") as log_date'))
                        ->where('group_id', $group_id)
                        ->orderBy('tdate','DESC')    
                        ->get();

        return $transactions;              
    }


    public function actionLogs($groupId, $limit = 0) {
        $action =  ActionLogs::with('user')->where('group_id', $groupId)->orderBy('id', 'desc')->get();
        if($limit > 0){
            $action =  ActionLogs::with('user')->where('group_id', $groupId)->orderBy('id', 'desc')->limit($limit)->get();
        }
        $arraylogs = [];
        $month = null;
        $day = null;
        $year = null;

        foreach($action as $a){
            $cdate = Carbon::parse($a->log_date)->format('M d Y');
            $dt = explode(" ", $cdate);
            $m = $dt[0];
            $d = $dt[1];
            $y = $dt[2];
            if($y == $year){
                $y = null;
                if($m == $month && $d == $day){
                    $m = null;
                    $d = null;
                }
                else{
                    $month = $m;
                    $day = $d;
                }
            }
            else{
                $year = $y;
                $month = $m;
                $day = $d;
            }

            $arraylogs[] = array(
                'month' => $m,
                'day' => $d,
                'year' => $y,
                'data' => array ( 
                    'title' => $a->details,
                    'processor' => $a->user->full_name,
                    'date' => Carbon::parse($a->log_date)->format('F d,Y'),
                )
            );
        }
        return json_encode($arraylogs);
    }
    public function commissionLogs($groupId, $limit = 0){

        if($limit > 0){
            $transaction =  CommissionLogs::with('user')->where('group_id', $groupId)
                            ->where('display',0)
                            ->orderBy('id', 'desc')->limit($limit)->get();
        }else{
            $transaction =  CommissionLogs::with('user')->where('group_id', $groupId)
                            ->where('display',0)
                            ->orderBy('id', 'desc')->get();
        }
       
        $arraylogs = [];
        $month = null;
        $day = null;
        $year = null;
        //$currentBalance = $this->groupCompleteBalance($groupId);
        foreach($transaction as $a){
            
            //$a->balance = $currentBalance;
            //$currentBalance -= $a->amount;
           
                $totalClientCommission =  CommissionLogs::select(DB::raw("SUM(commission) as total"))
                        ->where('display',0)
                        ->where('group_id',$a->group_id)
                        ->where('client_id',$a->client_id)
                        ->where('type',$a->type)
                        ->where('id','<=',$a->id)
                        ->first()->total;

                $client_last_record  =  CommissionLogs::select('commission')->where('group_id',$a->group_id)
                        ->where('display',0)
                        ->where('client_id',$a->client_id)
                        ->where('type',$a->type)
                        ->where('id','<',$a->id)
                        ->orderby('id', 'desc')->first();

                        //\Log::info($totalClientCommission);
            if($client_last_record['commission']==null){
                $client_last_record['commission'] = $totalClientCommission;
               
            }
          //\Log::info($totalClientCommission.':'.$client_last_record['commission']);
            $cdate = Carbon::parse($a->log_date)->format('M d Y');
            $dt = explode(" ", $cdate);
            $m = $dt[0];
            $d = $dt[1];
            $y = $dt[2];
            if($y == $year){
                $y = null;
                if($m == $month && $d == $day){
                    $m = null;
                    $d = null;
                }
                else{
                    $month = $m;
                    $day = $d;
                }
            }
            else{
                $year = $y;
                $month = $m;
                $day = $d;
            }

            $arraylogs[] = array(
                'month' => $m,
                'day' => $d,
                'year' => $y,
                'data' => array ( 
                    'id' => $a->id,
                    'title' => $a->detail,
                    'commission' => $totalClientCommission,
                    'prevcommission' => floatval($totalClientCommission)-floatval($client_last_record['commission']),
                    'type' => $a->type,
                    'processor' => $a->user->full_name,
                    'date' => Carbon::parse($a->log_date)->format('F d,Y'),
                )
            );
        }
        //\Log::info($arraylogs);
        return json_encode($arraylogs);

    }
    public function transactionLogs($groupId, $limit = 0) {
        $transaction =  TransactionLogs::with('user')->where('group_id', $groupId)
                        ->where('display',0)
                        ->orderBy('id', 'desc')->get();
        if($limit > 0){
            $transaction =  TransactionLogs::with('user')->where('group_id', $groupId)
                            ->where('display',0)
                            ->orderBy('id', 'desc')->limit($limit)->get();
        }
        $arraylogs = [];
        $month = null;
        $day = null;
        $year = null;
        $currentBalance = $this->groupCompleteBalance($groupId);
        foreach($transaction as $a){

            $usr =  User::where('id',$a->user_id)->select('id','first_name','last_name')->limit(1)->get()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);

            $cs = ClientService::where('id',$a->service_id)->first();

            $a->balance = $currentBalance;
            $currentBalance -= $a->amount;
            $cdate = Carbon::parse($a->log_date)->format('M d Y');
            $dt = explode(" ", $cdate);
            $m = $dt[0];
            $d = $dt[1];
            $y = $dt[2];
            if($y == $year){
                $y = null;
                if($m == $month && $d == $day){
                    $m = null;
                    $d = null;
                }
                else{
                    $month = $m;
                    $day = $d;
                }
            }
            else{
                $year = $y;
                $month = $m;
                $day = $d;
            }

            if($cs){
                $csdetail = $cs->detail;
                $cstracking =  $cs->tracking;
                $csstatus =  $cs->status;
                $csactive =  $cs->active;
            }
            else{
                $csdetail = ucfirst($a->type);
                $cstracking = '';
                $csstatus = '';
                $csactive = 'none';
            }

            $arraylogs[] = array(
                'month' => $m,
                'day' => $d,
                'year' => $y,
                'data' => array ( 
                    'id' => $a->id,
                    'title' => $a->detail,
                    'balance' => $a->balance,
                    'prevbalance' => $currentBalance,
                    'amount' => $a->amount,
                    'type' => $a->type,
                    'processor' => $usr[0]->first_name,
                    'old_detail' => $a->old_detail,
                    'date' => Carbon::parse($a->log_date)->format('F d,Y'),
                    'service_name' => $csdetail,
                    'tracking' => $cstracking,
                    'status' => $csstatus,
                    'active' => $csactive,
                    'total' => $a->total,
                )
            );
        }
        //\Log::info(json_encode($arraylogs));
        return json_encode($arraylogs);
    }

    public function getClientService($groupId, $clientServiceId) {
        return DB::connection('visa')
                    ->table('client_services as a')
                    ->select(DB::raw('a.*, b.parent_id as parent_id, b.docs_needed as docs_needed, b.docs_optional as docs_optional, b.months_required as months_required'))
                    ->leftjoin(DB::raw('(select * from services) as b'), 'b.id', '=', 'a.service_id')
                    ->where('a.group_id', $groupId)
                    ->where('a.id', $clientServiceId)
                    ->orderBy('a.id', 'desc')
                    ->get();
    }

    public function getClientServices($groupId, Request $request) {
        return DB::connection('visa')
                    ->table('client_services as a')
                    ->select(DB::raw('a.*, b.parent_id as parent_id, b.docs_needed as docs_needed, b.docs_optional as docs_optional, b.months_required as months_required'))
                    ->leftjoin(DB::raw('(select * from services) as b'), 'b.id', '=', 'a.service_id')
                    ->where('a.group_id', $groupId)
                    ->whereIn('a.id', $request->clientServicesId)
                    ->orderBy('a.id', 'desc')
                    ->get();
    }

    public function clientDeposit($client_id){
        return ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',0)
                    ->where('type','Deposit')
                    ->sum('amount');
    }

    public function clientDiscount($client_id){
        return ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',0)
                    ->where('type','Discount')
                    ->sum('amount');
    }

    public function clientPayment($client_id){
        return ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',0)
                    ->where('type','Payment')
                    ->sum('amount');
    }

    public function clientRefund($client_id){
        return ClientTransactions::where('client_id',$client_id)
                    ->where('group_id',0)
                    ->where('type','Refund')
                    ->sum('amount');
    }

    public function clientServiceCost($client_id){
        $cost = ClientService::where('client_id',$client_id)
            ->where('group_id', 0)
            ->where('active', 1)
            ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
        if($cost){
            return $cost;
        }
        return 0;
    }

    public function clientBalance($client_id){
        $balance = ((
                        Common::toInt($this->clientDeposit($client_id))
                        + Common::toInt($this->clientPayment($client_id))
                        + Common::toInt($this->clientDiscount($client_id))
                    )-(
                        Common::toInt($this->clientRefund($client_id))
                        + Common::toInt($this->clientServiceCost($client_id))
                    ));

        return $balance;
    }
    public function saveCommission(Request $request, $group_id){
        switch($request->com_type){
            case 'agent':
                 DB::connection('visa')->table('groups')->where('id',$group_id)->update(['agent_com_id'=>$request->com]);
            break;
            default:
                 DB::connection('visa')->table('groups')->where('id',$group_id)->update(['client_com_id'=>$request->com]);
            break;
        }
        
    }
    public function getCommission($group_id){
        //\Log::info(DB::connection('visa')->table('groups')->where('id',$group_id)->get());
        return  DB::connection('visa')->table('groups')->where('id',$group_id)->get();
    }

}
