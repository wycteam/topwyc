<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Models\Shopping\FeedbackLikes;

use App\Http\Requests\FeedbackLikesValidation;

use App\Acme\Filters\FeedbackLikesFilters;

class FeedbackLikesController extends ApiController
{
    public function __construct(
    	 FeedbackLikes $model
    	,FeedbackLikesFilters $filters
    )
    {
        $this->model = $model;
        $this->filters = $filters;
        $this->validation = new FeedbackLikesValidation;
    }
}
