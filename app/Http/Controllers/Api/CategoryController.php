<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Shopping\Category;
use App\Models\Shopping\ProductCategory;
use Illuminate\Http\Request;
use App\Acme\Filters\UserFilters;
use App\Http\Requests\UserValidation;
use Auth;
use App\Http\Controllers\ApiController;

class CategoryController extends ApiController
{
	protected $featured;
    protected $latest;
	protected $user;

    public function __construct(Category $model)
    {
        $this->model = $model;
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();   
	        $this->featured = $this->model::where('featured', 1)->get();
            return $next($request);
        });
    }

    public function index(){
    	$data['cats'] = $this->model::all();

        // $cat_list = $data->map(
        //     function($data) {
        //         return [
        //             "value" => $data->id,
        //             "name" => $data->name
        //         ];
        //     }
        // );
        //$cat_list->toArray();
    	$jsonData = $this->createResponseData($data,'success');
    	return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function productcategories(Request $request, $product_id)
    {
        $cats = ProductCategory::where('product_id',$product_id)->get();
        // $getcats = array();
        // foreach($cats as $c){
        //     $getcats[] = $c->category_id;
        // }
        $cats = $cats->pluck('category_id');
        $data = $this->model::whereIn('id', $cats)->get();

        $jsonData = $this->createResponseData($data,'success');
        return $this->setStatusCode(200)
            ->respond($jsonData);
    }
}
