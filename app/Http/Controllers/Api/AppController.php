<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
// use App\Models\Address;
use App\Models\Visa\Group;
use App\Models\Visa\GroupMember;
use App\Models\Visa\Package;
use App\Models\Visa\ClientService;
use App\Models\Visa\ClientDocuments;
use App\Models\Visa\ClientDocumentType;
// use App\Models\Visa\Deposit;
// use App\Models\Visa\Discount;
// use App\Models\Visa\Payment;
// use App\Models\Visa\Refund;
use App\Models\Visa\Ads;
use App\Models\Visa\Report;
use App\Models\Visa\Remark;
use App\Models\Visa\Service;
use App\Models\Visa\ActionLogs;
use App\Models\Visa\TransactionLogs;
use App\Models\Visa\PushNotificationsModel;
use App\Models\Visa\ServiceDocuments;
use App\Models\Visa\Action;
use App\Models\Visa\Category;
use App\Models\Visa\ServiceProfiles;
use App\Models\Visa\ServiceProfileCost;
use App\Models\Visa\Finance;
use App\Models\Visa\TransferBalance;
use App\Models\Visa\Updates;
use App\Models\Visa\News;
use App\Models\Visa\ClientTransactions;
use App\Models\Visa\ServiceBranchCost;
use App\Models\Visa\ByBatch;

use App\Models\Schedule;
use App\Models\Employee\Attendance;
use App\Models\Employee\AttendanceLogs;

use App\Http\Controllers\Api\Helpers\WriteReportHelperController;
use App\Http\Controllers\Cpanel\SynchronizationController;
use App\Http\Controllers\Cpanel\ClientsController;

use Illuminate\Support\Facades\Auth;

use App\Models\Visa\Synchronization;

use Response;
use Validator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Hash, DB;
use App\Classes\Common;
use Carbon\Carbon;

use Excel;
use DateTime;
use File;
use Storage;

class AppController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('auth:api');
    // }

     public function verifyUsername(Request $request) {
   
        $validator = Validator::make($request->all(), [
            'username' => 'required',

        ]);
        $login = $request->username;
        $result = filter_var( $login, FILTER_VALIDATE_EMAIL );
       
        if(!$result){
            
            preg_match_all('!\d+!', $login, $matches);
            $login = implode("", $matches[0]);
            $login = ltrim($login,"0");
            $login = ltrim($login,'+');
            $login = ltrim($login,'63');
            
            if(is_numeric($login)){
            //$id = Address::where('contact_number','like','%'.$login)->where('addressable_type','App\Models\User')->pluck('addressable_id');
            $binded = User::where('password','!=','')
                ->where('contact_number', 'like', '%'.$login)
                ->get(); 
            }else{
                $binded = NULL;
            }  
        }
        else{
            $binded = User::where('password','!=','')->where('email', $login)->get();
        }
        
        $response = [];

        if( $validator->fails() ) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
        }
        else{
            $response['total_bind'] = count($binded);
        }
        return Response::json($response);

    }

    public function getNews2($height, $width){
        $ret = News::where('only_private',0)->orderBy('created_at', 'desc')->get();
        foreach($ret as &$row){
            $ncontent = base64_decode($row->content);
            $dom = new \domdocument();
            libxml_use_internal_errors(true);
            $dom->loadHtml( mb_convert_encoding($ncontent , 'HTML-ENTITIES', "UTF-8"), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $images = $dom->getelementsbytagname('img');
            foreach($images as $image){
                $img_width = $image->getAttribute('width');
                $img_height = $image->getAttribute('height');
                $img_width = $width-20;
                $gap = ($img_width>=$img_height) ? $img_width-$img_height : $img_height - $img_width;
                $img_height = ($img_width>=$img_height) ? $width - $gap : $width + $gap;
                //$img_width = $width;//$image->getAttribute('width');
                //$image->setAttribute('width', $img_width.'px');
                //$image->setAttribute('height', $img_height.'px');
                $image->setAttribute('style', 'width:100%;');
                $image->removeAttribute('width');
                $image->removeAttribute('height');
                
            }
            
           $row->content = $dom->savehtml();
        //    //$row->content = preg_replace('/\\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)\\>/i', '<$1$4$7>', $row->content);
        //$row->content = base64_encode(preg_replace('/(<[^>]+) style=".*?"/i', '$1',  $row->content));   
        //$row->content =preg_replace('/(<[^>]+) style=".*?"/i', '$1',   $row->content);
        
        //$row->content = htmlentities($row->content);
        $row->content =stripslashes(str_replace("|-|","\"",str_replace("mick_mick","' width='",str_replace("mick-mick","='",str_replace("mickmick","'",html_entity_decode(str_replace("&quot;","'",str_replace("family: &quot;","family: |-|",str_replace("&quot;;","|-|;",str_replace("&quot; style","' style",str_replace("=&quot;","mick-mick", str_replace("&quot; width=&quot;","mick_mick",str_replace("px&quot;","px'",htmlentities($row->content))))))))))))));
        }
        return $ret;

    }

    public function getNews(){
        return  News::select('news_id','header','image','author_id','featured','allow_comment','only_private','created_at','updated_at')->where('only_private',0)->orderBy('created_at', 'desc')->get();
    }

    public function showNews($id){
        $ret = News::where('news_id',$id)->select('news_id','header','mobile_content as content')->first();
        $ret->content = html_entity_decode(str_replace("&#039;","'",str_replace("&quot;","'",htmlentities($ret->content,ENT_QUOTES))));
        //$ret->content =stripslashes(str_replace("|-|","\"",str_replace("mick_mick","' width='",str_replace("mick-mick","='",str_replace("mickmick","'",html_entity_decode(str_replace("&quot;","'",str_replace("family: &quot;","family: '",str_replace("&quot;;","';",str_replace("&quot; style","' style",str_replace("=&quot;","mick-mick", str_replace("&quot; width=&quot;","mick_mick",str_replace("px&quot;","px'",htmlentities($ret->content)))))))))))))); 
        return $ret;
    }

    public function getAds(){
        $ret = Ads::select('serialized')->where('status',1)->orderBy('created_at', 'desc')->get();
        foreach($ret as &$row){
            $row->serialized = unserialize($row->serialized);
        }
        return $ret;
    }

    function check_base64_image($base64) {
        $img = imagecreatefromstring(base64_decode($base64));
        if (!$img) {
            return false;
        }
    
        imagepng($img, 'tmp.png');
        $info = getimagesize('tmp.png');
    
        unlink('tmp.png');
    
        if ($info[0] > 0 && $info[1] > 0 && $info['mime']) {
            return true;
        }
    
        return false;
    }

    public function mobileLogin(Request $request) {
        $validator = Validator::make($request->all(), [
            // 'access_token' => 'required',
            'username' => 'required',
            'password' => 'required',
            'token' => 'required',
            'device_id' => 'required',
            'device_type' => 'required',
            'language' => 'required',
        ]);
       
        $login = $request->username;
        $result = filter_var( $login, FILTER_VALIDATE_EMAIL);

        if(!$result){
            preg_match_all('!\d+!', $login, $matches);
            $login = implode("", $matches[0]);
            $login = ltrim($login,"0");
            $login = ltrim($login,'+');
            $login = ltrim($login,'63');
            //$id = Address::where('contact_number','like','%'.$login)->where('addressable_type','App\Models\User')->pluck('addressable_id');
            if(is_numeric($login)){
                
                $id = User::where('contact_number','like','%'.$login)->pluck('id');
                $user = User::whereIn('id', $id)->get();
            }else{
                $user = NULL; 
            }
            
        }
        else{
            $user = User::where('email', $login)->get();
        }

        $response = [];

        if( $validator->fails() ) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 200; // Request Error
        }
        else{
            if($user) {
                foreach($user as $u){
                    if (Hash::check($request->password, $u->password)) {
                        //update user details 
                        $user = User::findorFail($u->id)->makeVisible('access_token');
                        //\Log::info($user);
                        $user->device_id = $request->device_id;
                        $user->device_type = $request->device_type;
                        $user->token = $request->token;
                        $user->language = $request->language;
                        $user->access_token = $user->createToken('Personal')->accessToken;
                        $user->save();
                        //$tokenset = $user->createToken('Personal')->accessToken;
                        //Userwhere('id', $u->id)->update(array('access_token'=>$tokenset));
                        //User::where('id',$u->id)->update(['access_token' => $tokenset]);
                        // $clientAdd = Address::where('addressable_id',$user->id)
                        //         ->where('addressable_type','App\Models\User')->first();
                        $is_new_user = 0;

                        // if($clientAdd){
                        $cnum = $user->contact_number;
                        if($cnum!=NULL){
                            preg_match_all('!\d+!', $cnum, $matches);
                            $cnum = implode("", $matches[0]);
                            $cnum = ltrim($cnum,"0");
                            $cnum = ltrim($cnum,'+');
                            $cnum = ltrim($cnum,'63');
                            $cnum = "0".$cnum;
                            if (Hash::check($cnum,  $u->password)) {
                                $is_new_user = 1;
                            }
                        }

                        // }

                        //response
                        $response['id'] = $user->id;
                        $response['email'] = $user->email;
                        $response['contact_info'] = $user->contact_number;
                        $response['old_contact_info'] = $user->alternate_contact_number;
                        $response['token'] = $user->token;
                        $response['device_id'] = $user->device_id;
                        $response['device_type'] = $user->device_type;
                        $response['active'] = $user->is_active;
                        $response['language'] = $user->language;
                        $response['access_token'] = $user->access_token;
                        $response['is_new_user'] = $is_new_user;

                        $admin = 0;
                        if($user->hasRole('cpanel-admin') || $user->hasRole('master') || $user->hasRole('vice-master') || $user->hasRole('support') || $user->hasRole('employee')){
                            $admin = 1;
                        }
                        $vclient =0;
                        if($user->hasRole('visa-client')){
                            $vclient = 1;
                        }
                        $response['admin'] = $admin;
                        $response['client'] = $vclient;
                        $response['roles'] = $user->rolesname->pluck('name');

                        $httpStatusCode = 200; // Ok
                        return Response::json($response, $httpStatusCode);
                    } 
                }
                    $response['status'] = 'Failed';
                    $response['desc'] = 'Client authentication failed';

                    $httpStatusCode = 200; // Client Authentication failed
                

            } else {
                $response['status'] = 'Failed';
                $response['desc'] = 'Client authentication failed';

                $httpStatusCode = 200; // Client Authentication failed
            }
        }
        return Response::json($response, $httpStatusCode);

    }

    public function checkClient(Request $request) {
        $validator = Validator::make($request->all(), [
            'client_fname' => 'required',
            'client_lname' => 'required',
            'client_gender' => 'required',
            'client_dob' => 'required',
        ]);

        if( $validator->fails() ) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 400; // Request Error
            return Response::json($response, $httpStatusCode);
        }
        else{     
            $fname = $request['client_fname'];
            $lname = $request['client_lname'];
            $client_gender = $request['client_gender'];
            $client_dob = $request['client_dob'];
            $contact_number = $request['contact_number'];
            preg_match_all('!\d+!', $contact_number, $matches);
            $contact_number = implode("", $matches[0]);
            $contact_number = ltrim($contact_number,"0");
            $contact_number = ltrim($contact_number,'+');
            $contact_number = ltrim($contact_number,'63');
            $bd = explode("-", $client_dob);
            $date = Carbon::create($bd[0], $bd[1], $bd[2]);
            $client_dob2 = $date->format('d/m/Y');
            $client_dob4 = $date->format('m/d/Y');
            $client_dob3 = $date->format('d-m-Y');

            if($contact_number!=null){
                $ids = User::where('contact_number','like','%'.$contact_number)->pluck('id');
            }
            else{
                $ids = [0];
            }
            //dd($ids);
            //$user = User::whereIn('id', $id)->get();
            //foreach($user as $u){
                $client = User::where(function($q) use($fname,$lname){
                                    $q->where('first_name', $fname)->where('last_name', $lname);
                                    $q->orwhere('last_name', $fname)->where('first_name', $lname);
                                })
                            ->where(function($q) use($client_dob,$client_dob2,$client_dob3,$client_dob4){
                                $q->where('birth_date', $client_dob);
                                $q->orwhere('birth_date', $client_dob2);
                                $q->orwhere('birth_date', $client_dob3);
                                $q->orwhere('birth_date', $client_dob4);
                            })
                            ->where('gender',$client_gender)
                            ->select('id')
                            ->get();

                if(count($client)<1){
                    $client = User::where(function($q) use($fname,$lname){
                                    $q->where('first_name', $fname)->where('last_name', $lname);
                                    $q->orwhere('last_name', $fname)->where('first_name', $lname);
                                })
                            ->where(function($q) use($client_dob,$client_dob2,$client_dob3,$client_dob4){
                                $q->where('birth_date', $client_dob);
                                $q->orwhere('birth_date', $client_dob2);
                                $q->orwhere('birth_date', $client_dob3);
                                $q->orwhere('birth_date', $client_dob4);
                            })
                            ->select('id')
                            ->get();
                    if(count($client)<1){
                        $client = User::where(function($q) use($fname,$lname){
                                    $q->where('first_name', $fname)->where('last_name', $lname);
                                    $q->orwhere('last_name', $fname)->where('first_name', $lname);
                                })
                            ->where('gender',$client_gender)
                            ->select('id')
                            ->get();
                        if(count($client)<1){
                            return 0;
                        }
                    }

                }
                if(count($client)>1){
                    $data['warning'] = "Multiple users are using the details you entered.";
                    $data['users_found'] = array_pluck($client, 'id');
                    return Response::json($data);
                }
                else{
                    if($ids == [0]){
                        $ids= $client->pluck('id');
                    }
                    $client = $client->first();
                    $client = User::findorFail($client->id);
                    $data['client_id'] = $client->id;

                    // $checkIfLeader = GroupMember::where('client_id',$client->id)->where('leader','1')->first();
                    $checkIfLeader = GroupMember::where('client_id',$client->id)->where('leader','>',0)->first();
                    $leaderctr = ($checkIfLeader ? 1 : 0);
                    $data['is_leader'] = $leaderctr;

                    $checkIfBind = User::whereIn('id',$ids)->where('password','!=','')->first();
                    $bindctr = ($checkIfBind ? 1 : 0);
                    $data['is_bind'] = $bindctr;

                    $checkIfDetailsBind = User::where('id',$client->id)->whereIn('id',$ids)->where('password','!=','')->first();
                    $bindetctr = ($checkIfDetailsBind ? 1 : 0);
                    $data['details_bind'] = $bindetctr;

                    //getting of client info if bind
                    if($checkIfBind){
                        $userBind = User::where('id',$checkIfBind->id)->select('id','first_name','middle_name','last_name')->first();
                        $data['user_bind'] = $userBind;         
                    }
                    else{
                        $data['user_bind'] = '';         
                    }

                    //check empty details
                    $emp = null;
                    if($client->passport == null || $client->passport == '' || $client->passport == 'n/a' || $client->passport == 'N/A'){
                        $emp.="passport ";
                    }
                    if($client->height == null || $client->height == '' || $client->height == 'n/a' || $client->height == 'N/A'){
                        $emp.="height ";
                    }
                    if($client->weight == null || $client->weight == '' || $client->weight == 'n/a' || $client->weight == 'N/A'){
                        $emp.="weight ";
                    }
                    if($client->civil_status == null || $client->civil_status == '' || $client->civil_status == 'n/a' || $client->civil_status == 'N/A'){
                        $emp.="civil_status ";
                    }
                   
                    if($client->address == NULL || $client->address == ''){
                        $emp.="local_address ";
                    }
                    // if($checkGender->client_address_abroad == null || $checkGender->client_address_abroad == '' || $checkGender->client_address_abroad == 'n/a' || $checkGender->client_address_abroad == 'N/A'){
                    //     $emp.="foreign_address ";
                    // }
                    if($emp == null){
                        $data['empty'] = [];
                    }
                    else{
                        $emp = trim($emp);
                        $data['empty'] = explode(" ",$emp);
                    }
                    $sv = 0;
                    if($client->birth_date == null){
                        $client->birth_date = $client_dob;
                        $sv++;
                    }
                    if($client->gender == null){
                        $client->gender = $client_gender;
                        $sv++;
                    }
                    if($sv>0){
                        $client->save();
                    }
                }
        }

        return Response::json($data);
    }

    public function checkPassport(Request $request) {
        $validator = Validator::make($request->all(), [
            'users_found' => 'required',
            'client_passport' => 'required',
        ]);

        if( $validator->fails() ) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 400; // Request Error
            return Response::json($response, $httpStatusCode);
        }
        else{     
            $passport = $request['client_passport'];
            $clients = str_replace(array( '[', ']' ), '', $request['users_found']);
            $users_found = explode(',', $clients);
            // return $users_found;
            $passUser = User::whereIn('id',$users_found)
                        ->where('passport',$passport)->first();

        if($passUser){
            //$checkIfLeader = GroupMember::where('client_id',$passUser->id)->where('leader','1')->first();
            $checkIfLeader = GroupMember::where('client_id',$passUser->id)->where('leader','>',0)->first();
            $leaderctr = ($checkIfLeader ? 1 : 0);
            $data['client_id'] = $passUser->id;
            $data['is_leader'] = $leaderctr;
            return Response::json($data);
        }

        return 0;
        }

    }

    public function getApkVersion(){
        $data['version'] = '1.0.1.2.6';
        $data['apkUrl'] = "http://topwyc.com/apk/wycgrouptracker.apk";
        $data['forceUpdate'] = true;
        return json_encode($data, JSON_UNESCAPED_SLASHES);
    }

    public function updateLanguage(Request $request){
        $id = $request['client_id'];
        $language = $request['language'];
        $updlang = User::where('id',$id)->update(['language' => $language]);

        if($updlang){
            $data['status'] = 'success';
        }
        else{
            $data['status'] = 'failed';
        }
        return Response::json($data);
    }

    public function saveEmptyDetails(Request $request){
        $client_id = $request['client_id'];
        $passport = $request['passport'];
        $height = $request['height'];
        $weight = $request['weight'];
        $civil_status = $request['civil_status'];
        $address_local = $request['local_address'];
        // $address_foreign = $request['foreign_address'];

        $client = User::where('id',$client_id)->first();

        if(count($client)>0){
            // $client = ClientContactModel::find($client->id);

            if($passport != null){
                $client->passport = $passport;
            }
            if($height != null){
                $client->height = $height;
            }
            if($weight != null){
                $client->weight = $weight;
            }
            if($civil_status != null){
                $client->civil_status = $civil_status;
            }
            if($address_local != null){
                $client->address = $address_local;
            }
            // if($address_foreign != null){
            //     $client->client_address_abroad = $address_foreign;
            // }
            $client->save();
            return $client;
        }
    }

    public function personalInfo($id) {
        $user = User::where('id',$id)->first()->makeHidden('permissions');
        return Response::json($user);
    }

    public function checkIfMembers(Request $request){
        $fname1 = $request['fname1'];
        $lname1 = $request['lname1'];
        $fname2 = $request['fname2'];
        $lname2 = $request['lname2'];
        $client_id = $request['client_id'];

        $member1 = User::where(function($q) use($fname1,$lname1){
                            $q->where('first_name', $fname1);
                            $q->where('last_name', $lname1);
                            })
                        ->orwhere(function($q) use($fname1,$lname1){
                            $q->where('last_name', $fname1);
                            $q->where('first_name', $lname1);
                            })
                        ->get();
        $cids1 = array_pluck($member1, 'id');

        $member2 = User::where(function($q) use($fname2,$lname2){
                            $q->where('first_name', $fname2);
                            $q->where('last_name', $lname2);
                            })
                        ->orwhere(function($q) use($fname2,$lname2){
                            $q->where('last_name', $fname2);
                            $q->where('first_name', $lname2);
                            })
                        ->get();
        $cids2 = array_pluck($member2, 'id');

        //combine two arrays
        $cids = array_merge($cids1, $cids2);
        //check if user enter his own name
        if (in_array($client_id, $cids)) {
            $data['error'] = 'You enter your own name.';
            return response()->json($data);
        }

        //remove id of leader in case he/she enter his/her own name
        $cids = array_diff( $cids, [$client_id]);
        // $getGroups = GroupMember::where('client_id',$client_id)->where('leader','1')->get();
        $getGroups = GroupMember::where('client_id',$client_id)->where('leader','>',0)->get();
        $a = 0;
        $ctr=[];
        $verifyleader = false;
        if($getGroups){
            foreach($getGroups as $g){
                if(!$verifyleader){
                    $totalMembers =  GroupMember::where('group_id',$g->group_id)->where('leader','0')->get();
                    $ctr[$a]['members_total'] =$totalMembers->count();            
                    $searchMembers =  GroupMember::whereIn('client_id',$cids)->where('group_id',$g->group_id)->get();
                    $ctr[$a]['members_found'] =$searchMembers->count();                    
                    if($totalMembers->count() > 1 && $searchMembers->count() <2){
                        $verifyleader = false;
                    }
                    if($totalMembers->count() == 1 && $searchMembers->count() <1){
                        $verifyleader = false;
                    }

                    if($totalMembers->count() == 1 && $searchMembers->count() >=1){
                        $verifyleader = true;
                    }
                    if($totalMembers->count() > 1 && $searchMembers->count() >=2){
                        $verifyleader = true;
                    }
                    $ctr[$a]['verify_leader'] =($verifyleader ? 1 : 0);
                    $a++;
                }
                // $verifyleader = false;
            }
        }
        return response()->json($ctr);

    }

    public function savePassword(Request $request){
        $client_id = $request['client_id'];
        $password = $request['password'];
        $password_confirmation = $request['password_confirmation'];
        $email = $request['email'];
        $token = $request['token'];
        $device_id = $request['device_id'];
        $device_type = $request['device_type'];
        $number = $request['number'];
        $new_number = $request['new_number'];
        $client_dob = $request['client_dob'];
        $language = $request['language'];

            // $messages = [
            //     'password.confirmed' => 'Password does not match the confirm password.',
            //     'password.required'  => 'Password is required.',
            //     'password.min'       => 'Password is too short.',
            //     'email.unique'       => 'Email already in used.',
            // ];
            // $validator = Validator::make($request->all(), [
            //     'password' => 'required|min:8|confirmed',
            //     'email' => 'unique:users',
            // ],$messages); 
        $user = User::where('id','!=',$client_id)->where('email',$email)->first();             

        // if ($validator->fails()) {
        //     $data['error'] = $validator->errors()->all();
        //     return response()->json($data,422);
        // };
        if ($user) {
            $data['error'] = "Email not unique.";
            $data['error_cn'] = "电邮不是唯一的.";
            return response()->json($data,422);
        };
        // preg_match_all('!\d+!', $number, $matches);
        // $number = implode("", $matches[0]);
        // $number = ltrim($number,"0");
        // $number = ltrim($number,'+');
        // $number = ltrim($number,'63');

        if($number!=''){
            $add = User::where(DB::raw("REPLACE(REPLACE(REPLACE(contact_number, '-',''),')',''),'-','(')"),$number)->where('id',$client_id)->first();
        }
        else{
            $add = User::where('id',$client_id)->first();
        }

        $client = User::where('id',$client_id)->first();

        if(count($client)>0){
            // $client = ClientContactModel::find($client->id);

            if($new_number != ''){
                $newnum = $new_number;
                $numlen = strlen((string) $newnum);
                // preg_match_all('!\d+!', $newnum, $matches);
                // $newnum = implode("", $matches[0]);
                // $newnum = ltrim($newnum,"0");
                // $newnum = ltrim($newnum,'+');
                // $newnum = ltrim($newnum,'63');
                if($numlen < 11){
                    //if wrong format of mobile number
                    $data['error'] = "Please enter a correct cellphone number format in the Philippines. Ex.(0917xxxxxxx). Total of 11 digits.";
                    $data['error_cn'] = "请输入正确格式的菲律宾电话号码，比如0917xxxxxxx，总共11位数电话号码";
                    return $data;
                }

                $numIfBind = User::where(DB::raw("REPLACE(REPLACE(REPLACE(contact_number, '-',''),')',''),'-','(')"),$newnum)->pluck('id');
                $userFound = User::whereIn('id',$numIfBind)->where('password','!=','')->get();
                if(count($userFound)>0){
                    //if mobile number already in used
                    $data['error'] = "Mobile number is already in use. If you are sure the mobile number you have entered is yours, Please contact our customer support.";
                    $data['error_cn'] = "您输入的电话号码已存在，如果您确认这个号码是您的，请联系我们的客服";
                    return $data;
                }

                //saving of contact number & alternate contact_number
                if($add){
                    $add->alternate_contact_number = $add->contact_number;
                    $add->contact_number = $new_number;
                    $add->save();                  
                }
                else{
                    $add = User::where('id',$client_id)->first();
                    if($add){
                        $add->alternate_contact_number = $add->contact_number;
                        $add->contact_number = $new_number;
                        $add->save();                  
                    }
                }
            }
            $client->password = bcrypt($password);
            $client->email = $email;
            $client->token = $token;
            $client->device_id = $device_id;
            $client->device_type = $device_type;
            $client->is_active = 1;
            $client->language = $language;
            $client->save();
            return $client;
        }
    }

    public function saveNewPassword(Request $request){
        $client_id = $request['client_id'];
        $password = $request['password'];
        $old_password = $request['old_password'];
        $user = User::where('id',$client_id)->first();             

        if($user){
            $cnum = $user->contact_number;
            if(Hash::check($cnum, $password)) {
                $response['status'] = 'Failed';
                $response['desc'] = 'Please don\'t use your mobile number as your password.';
                $response['desc_cn'] = 'Please don\'t use your mobile number as your password.';
                $httpStatusCode = 200; // Client Authentication failed
                return Response::json($response, $httpStatusCode);
            }

            if(Hash::check($password, $user->password)) {
                $response['status'] = 'Failed';
                $response['desc'] = 'Please enter different password.';
                $response['desc_cn'] = 'Please enter different password.';
                $httpStatusCode = 200; // Client Authentication failed
                return Response::json($response, $httpStatusCode);
            }

            if(Hash::check($old_password, $user->password)) {
                $user->password = bcrypt($password);
                $user->save();
                return $user;
            } else {
                $response['status'] = 'Failed';
                $response['desc'] = 'Incorrect old password.';
                $response['desc_cn'] = 'Incorrect old password.';
                $httpStatusCode = 200; // Client Authentication failed
                return Response::json($response, $httpStatusCode);
            }
        }
    }

    public function actionLogs($id,$group_id=0) {
        if($group_id == 0){
            $tlogs['logs'] = ActionLogs::where('client_id',$id)->where('group_id',$group_id)->orderBy('id','Desc')->get();
        }
        else{
            $tlogs['logs'] = ActionLogs::where('group_id',$group_id)->orderBy('id','Desc')->get();
        }
        $ctr =0;
        foreach($tlogs['logs'] as $tl){
            $user = User::where('id',$tl->user_id)->select('first_name','last_name')->get();
            $tlogs['logs'][$ctr]['processor'] = $user;
            $ctr++;
        }
        return Response::json($tlogs);
    }

    public function transactionLogs($id,$group_id=0) {
        if($group_id == 0){
            $tlogs['logs'] = TransactionLogs::where('client_id',$id)->where('group_id',$group_id)->where('display',0)->orderBy('id','desc')->get();
        }
        else{
            $tlogs['logs'] = TransactionLogs::where('group_id',$group_id)->orderBy('id','desc')->where('display',0)->get();
        }
        // $ctr =0;
        // foreach($tlogs['logs'] as $tl){
        //     $user = User::where('id',$tl->user_id)->select('first_name','last_name')->get()->makeHidden(['access_control','avatar','group_binded','document_receive','three_days','total_balance','total_cost','total_deposit','total_discount','total_payment','total_points','total_refund','unread_notif','permissions','total_complete_cost','collectable']);
        //     $tlogs['logs'][$ctr]['processor'] = $user;
        //     $ctr++;
        // }
        //return Response::json($tlogs);

        $arraylogs = [];
        $month = null;
        $day = null;
        $year = null;
        $ctr = 0;
        $currentBalance = $this->groupCompleteBalance($group_id);
        // return $currentBalance;
        foreach($tlogs['logs'] as $a){
            $user = User::where('id',$a->user_id)->select('first_name','last_name')->get()->makeHidden(['access_control','avatar','group_binded','document_receive','three_days','total_balance','total_cost','total_deposit','total_discount','total_payment','total_points','total_refund','unread_notif','permissions','total_complete_cost','collectable']);
            $tlogs['logs'][$ctr]['processor'] = $user;

            $string = $a->old_detail; 
            $find = 'Total service charge from';
            $lastPos = 0;
            $lastPos2 = 0;

            while (($lastPos = strpos($string, $find, $lastPos))!== false) {
                while (($lastPos2 = strpos($string, '. <br>', $lastPos)) > $lastPos){
                        $string = substr_replace($string, '', $lastPos, $lastPos2-$lastPos);
                        $lastPos2 = $lastPos + strlen('. <br>');
                }
                $lastPos = $lastPos + strlen($find);
            }

            $string = str_replace(": .", ".", $string);
            $string = str_replace(", .", ".", $string);

            $tlogs['logs'][$ctr]['old_detail'] = $string;
            $tlogs['logs'][$ctr]['balance'] = $currentBalance;
            $currentBalance -= $a->amount;
            $ctr++;
        }
        return Response::json($tlogs);

    }

    public function transactionLogsV2($groupId, $limit = 0) {
        $transaction =  TransactionLogs::with('user')->where('group_id', $groupId)
                        ->where('display',0)
                        ->orderBy('id', 'desc')->get();
        if($limit > 0){
            $transaction =  TransactionLogs::with('user')->where('group_id', $groupId)
                            ->where('display',0)
                            ->orderBy('id', 'desc')->limit($limit)->get();
        }
        $arraylogs = [];
        $month = null;
        $day = null;
        $year = null;
        $currentBalance = $this->groupCompleteBalance($groupId);
        foreach($transaction as $a){
            $a->balance = $currentBalance;
            $currentBalance -= $a->amount;
            $cdate = Carbon::parse($a->log_date)->format('M d Y');
            $dt = explode(" ", $cdate);
            $m = $dt[0];
            $d = $dt[1];
            $y = $dt[2];
            if($y == $year){
                $y = null;
                if($m == $month && $d == $day){
                    $m = null;
                    $d = null;
                }
                else{
                    $month = $m;
                    $day = $d;
                }
            }
            else{
                $year = $y;
                $month = $m;
                $day = $d;
            }

            $arraylogs[] = array(
                'month' => $m,
                'day' => $d,
                'year' => $y,
                'data' => array ( 
                    'id' => $a->id,
                    'title' => $a->detail,
                    'balance' => $a->balance,
                    'prevbalance' => $currentBalance,
                    'amount' => $a->amount,
                    'type' => $a->type,
                    'processor' => $a->user->full_name,
                    'old_detail' => $a->old_detail,
                    'date' => Carbon::parse($a->log_date)->format('F d,Y'),
                )
            );
        }
        return json_encode($arraylogs);
    }

    public function allPackages($id,$group_id = 0) {
        if($group_id == 0){
            $packs['packages'] = Package::where('client_id',$id)->where('group_id',$group_id)
                                ->with(['service' => function ($q) {
                                    $q->where('active', 1)->with('report');
                                }])->get();
            $ctr = 0;
            foreach($packs['packages'] as $p){
                $pt = ClientService::
                        select(DB::raw('sum(cost+charge+tip+com_client+com_agent) as total_cost'))
                        ->where('client_id',$id)
                        ->where('tracking',$p->tracking)
                        ->where('active',1)
                        ->get();
                $packs['packages'][$ctr]['total_service_cost'] = $pt[0]->total_cost;
                $ctr++;
            }
        }
        else if($group_id < 0){
            $packs['packages'] = Package::where('client_id',$id)
                                ->with(['service' => function ($q) {
                                    $q->where('active', '>', '-1')->with('report');
                                }])->get();
            $ctr = 0;
            foreach($packs['packages'] as $p){
                $pt = ClientService::
                        select(DB::raw('sum(cost+charge+tip + com_client + com_agent) as total_cost'))
                        ->where('client_id',$id)
                        ->where('tracking',$p->tracking)
                        ->where('active',1)
                        ->get();
                $packs['packages'][$ctr]['total_service_cost'] = $pt[0]->total_cost;
                $ctr++;
            }
            $group_id = 0; // change to zero for summary computation
        }
        else{
            $packs['packages'] = Package::where('group_id',$group_id)->where('group_id',$group_id)
                                ->with(['service' => function ($q) {
                                    $q->where('active', 1)->with('report');
                                }])->get();
            $ctr = 0;
            foreach($packs['packages'] as $p){
                $pt = ClientService::
                        select(DB::raw('sum(cost+charge+tip + com_client + com_agent) as total_cost'))
                        ->where('group_id',$group_id)
                        ->where('tracking',$p->tracking)
                        ->where('active',1)
                        ->get();
                $packs['packages'][$ctr]['total_service_cost'] = $pt[0]->total_cost;
                $ctr++;
            }
        }


        $initialdeposit = (
            (
                floatval($this->clientDeposit($id,$group_id))
                + floatval($this->clientPayment($id,$group_id))
                + floatval($this->clientDiscount($id,$group_id))
            )
            - 
            (
                floatval($this->clientRefund($id,$group_id))
                + floatval($this->clientServiceCost($id,$group_id))
            )
        );

        $packs['summary']['deposit'] = $this->clientDeposit($id,$group_id);
        $packs['summary']['refund'] = $this->clientRefund($id,$group_id);
        $packs['summary']['discount'] = $this->clientDiscount($id,$group_id);
        $packs['summary']['payment'] = $this->clientPayment($id,$group_id);
        $packs['summary']['cost'] = $this->clientServiceCost($id,$group_id);
        $packs['summary']['balance'] = Common::formatMoney($initialdeposit, true);
        return Response::json($packs);
    }

    public function byService(Request $request) {
        $group_id = $request['group_id'];
        $name = $request['service_name'];

        $packs['Service'] = $name;
        $clients = ClientService::where('group_id',$group_id)->where('detail',$name)->groupBy('client_id')->get();
        $ctr=0;
        foreach($clients as $c){
            // $serviceClient = ClientService::where('group_id',$group_id)->where('detail',$name)->where('client_id',$c->client_id)->where('active',1)->with('discount')->get();
            $serviceClient = ClientService::select('id','client_id','service_id','group_id','detail','cost','tip','charge','com_client','com_agent','status','service_date','remarks','active',DB::raw('(charge+com_client+com_agent) as charge'))
                    ->where('group_id',$group_id)
                    ->where('client_id',$c->client_id)->where('detail',$name)
                    ->where('active',1)->with('discount')
                    ->get();
            $usr = User::where('id',$c->client_id)->select('first_name','last_name')->first();
            $packs['members'][$ctr]= $usr;
            $packs['members'][$ctr]['services']= $serviceClient;
            $ctr++;
        }

        return Response::json($packs);
    }

    public function byBatch(Request $request) {
        $group_id = $request['group_id'];
        $date = $request['service_date'];

        $packs['Date'] = $date;
        $sdates = ClientService::where('group_id',$group_id)
                                // ->where('service_date',$date)
                                ->where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$date)
                                ->where('active',1)->orderBy('service_date','DESC')->orderBy('client_id')
                                ->groupBy('client_id')
                                ->get();
        $packs['Total_Cost'] = 0;
        $ctr=0;
        foreach($sdates as $c){
            // $serviceClient = ClientService::where('group_id',$group_id)
            //                 ->where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$date)
            //                 ->where('active',1)->where('client_id',$c->client_id)
            //                 ->with('discount')->get();
            $serviceClient = ClientService::select('id','client_id','service_id','group_id','detail','cost','tip','charge','com_client','com_agent','status','service_date','remarks','active',DB::raw('(charge+com_client+com_agent) as charge'))
                    ->where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$date)
                    ->where('group_id',$group_id)
                    ->where('client_id',$c->client_id)
                    ->where('active',1)->with('discount')
                    ->get();
            $usr = User::where('id',$c->client_id)->select('first_name','last_name')->first()
                    ->makeHidden(['permissions','access_control','document_receive','binded','unread_notif','group_binded']);
            $packs['Total_Cost'] += ClientService::where('group_id',$group_id)
                            ->where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$date)
                            ->where('active',1)->where('client_id',$c->client_id)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
            $packs['members'][$ctr]= $usr;
            $packs['members'][$ctr]['services']= $serviceClient;
            $ctr++;
        }
        return Response::json($packs);
    }

    public function groupByDate($group_id) {
        $dates = DB::connection('visa')
            ->table('client_services as a')
            ->select(DB::raw('a.*, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y") as sdate'))
            ->where('active',1)->where('group_id',$group_id)
            ->orderBy('id','DESC')
            ->groupBy('sdate')
            ->get();
        //$dates = ClientService::where('group_id',$group_id)->groupBy('service_date')->orderBy('service_date','DESC')->get();
        $ctr=0;
        $packs = null;
        foreach($dates as $d){
            $a = $d->service_date;
            $b = $d->sdate;
            $serv = DB::connection('visa')
            ->table('client_services as a')
            ->select(DB::raw('a.*,date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y") as sdate'))
            ->where('id',$d->id)->first();
            //$serv =ClientService::where('id',$d->id)->select('service_date')->first();
            $serviceCount = ClientService::where('group_id',$group_id)->where('active',1)
            ->where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$b)
            ->count();
            //$usr = User::where('id',$c->client_id)->select('first_name','last_name')->first();
            $packs['dates'][$ctr]['service_date']= $serv->sdate;
            $packs['dates'][$ctr]['service_count']= $serviceCount;
            $ctr++;
        }
        if($packs == null){
            return json_encode($packs);
        }
        return Response::json($packs);
    }

    public function groupByService($groupId) {
        $sId = ClientService::where('group_id',$groupId)->where('active',1)->with('discount')->groupBy('service_id')->orderBy('id',"DESC")->get();
        $ctr = 0;
        $services = [];
        foreach($sId as $s){
            $services[$ctr] = ClientService::where('id',$s->id)->select('detail','service_date','service_id')->orderBy('id',"DESC")->first();
            $services[$ctr]['totalcost'] = ClientService::where('service_id',$s->service_id)->where('group_id', $groupId)->where('active', 1)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
            $ctr2 = 0;
            $ctr++;
        }
        return Response::json($services);
    }

    public function filterBatch($start, $end) {
        $group = Group::where('id','>=', $start)->where('id','<=',$end)->get();
        foreach($group as $g){
                $id = $g->id;
                $groupId = $g->id;
                $byBatch = DB::connection('visa')
                        ->table('client_services as a')
                        ->select(DB::raw('id,detail,service_date, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y") as sdate, date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%Y%m%d") as tdate'))
                        ->where('active',1)->where('group_id',$id)
                        //->where('service_date',$service_date)
                        ->orderBy('id','DESC')
                        ->groupBy('sdate')
                        ->get();
                $byBatchDate = $byBatch->pluck('tdate');

                $transactions = DB::connection('visa')
                    ->table('client_transactions as a')
                    ->select(DB::raw('
                        a.amount,a.type,a.service_id, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%Y%m%d") as tdate, date_format(STR_TO_DATE(a.log_date, "%m/%d/%Y"),"%M %d,%Y") as log_date'))
                        ->where('group_id', $id)
                        //->where('service_id', null)
                        ->where(function($query) {
                            $query->where('service_id', null)
                                ->orWhere('service_id', 0);
                        })
                        //->where('log_date','%LIKE%', $service_date)
                        ->orderBy('tdate','DESC')    
                        ->get();
                //\Log::info($transactions);

                $transDate = $transactions->pluck('tdate');

                $merged = $byBatchDate->merge($transDate);

                $result = $merged->all();
                $tdates = array_unique($result);
                rsort($tdates);
                //\Log::info($tdates);
                $groupcost = ClientsController::groupCollectable2($groupId);
                $lastcost = 0;
                $tlist = [];
                $datectr = 0;
                $dt = null;
                $dt2 = null;
                $arr = [];
                $arr2 = [];
                foreach($tdates as $d){
                    $y =  substr($d,0,4);
                    $m =  substr($d,4,2);
                    $day =  substr($d,6,2);
                    $nDate = $m.'/'.$day.'/'.$y;

                    $dt =   $d;
                    $dt2 =   Carbon::parse($nDate)->format('F d,Y');
                    $checkBatch = ByBatch::where('date', $dt)->where('group_id',$groupId)->first();
                    if(!$checkBatch){
                    $query = DB::connection('visa')
                        ->table('client_services')->where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$nDate)->where('group_id', $id)->where('active', 1)->orderBy('service_date','DESC')->orderBy('client_id')->groupBy('client_id')->get();
                        $ctr2 = 0;
                        //$servTotal = 0;
                        $client = [];
                        $arr = [];
                        $flag = true;
                        $client_ctr = 0;
                        foreach($query as $m){
                            $ss =  ClientService::where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$nDate)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->with('discount2')->get();
                            if(count($ss) > 0){
                                $client = User::where('id',$m->client_id)->select('first_name','last_name')->first()->full_name;
                                $totalcost = ClientService::where('service_id',$id)->where('group_id', $id)->where('active', 1)->where('client_id',$m->client_id)->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
                                $services = $ss;

                                $arr[$client_ctr]['name'] = $client;

                                $servctr = 0;
                                foreach($services as $serv){
                                    $tcost =  $serv->cost + $serv->tip + $serv->charge + $serv->com_client + $serv->com_agent;
                                    if($serv->status != 'complete'){
                                        $tcost = 0;
                                    }

                                    if(strpos($serv->detail, "9A") === false && $serv->status != 'complete'){
                                        $tcost = $serv->cost + $serv->tip + $serv->charge + $serv->com_client + $serv->com_agent;
                                    }

                                    $arr[$client_ctr]['services'][$servctr]['tracking'] = $serv->tracking;
                                    $arr[$client_ctr]['services'][$servctr]['detail'] = $serv->detail;
                                    $arr[$client_ctr]['services'][$servctr]['total_cost'] = $tcost;
                                    $arr[$client_ctr]['services'][$servctr]['service_id'] = $serv->id;

                                    $sp = $serv->status;
                                    if($serv->active==0){
                                        $sp = "CANCELLED";
                                    }
                                    $arr[$client_ctr]['services'][$servctr]['status'] = $sp;

                                    // if($flag= false){
                                    //     $tempTotal = $groupcost - $lastcost;
                                    // }
                                    // else{
                                    //     $tempTotal = $groupcost + $lastcost;
                                    // }

                                    $translated = Service::where('id',$serv->service_id)->first();
                                    $cnserv =$serv->detail;
                                    if($translated){
                                        $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                                    }
                                    $arr[$client_ctr]['services'][$servctr]['detail_cn'] = $cnserv;
                                    //$arr[$client_ctr]['services'][$servctr]['tempTotal'] = $tempTotal;


                                    //$lastcost = $tcost;
                                    //$groupcost = $tempTotal;

                                    if($serv->discount_amount>0){
                                        $servctr++;
                                        //$tempTotal = $groupcost + $lastcost;

                                        $arr[$client_ctr]['services'][$servctr]['tracking'] = '';
                                        $arr[$client_ctr]['services'][$servctr]['detail'] = 'Discount';
                                        $arr[$client_ctr]['services'][$servctr]['detail_cn'] = '折扣';
                                        $arr[$client_ctr]['services'][$servctr]['total_cost'] = $serv->discount_amount;
                                        $arr[$client_ctr]['services'][$servctr]['service_id'] = '';
                                        $arr[$client_ctr]['services'][$servctr]['status'] = '';
                                        //$arr[$client_ctr]['services'][$servctr]['tempTotal'] = $tempTotal;


                                        //$lastcost = -1 *$serv->discount_amount;
                                        //$groupcost = $tempTotal;

                                    }

                                    //$row++;
                                    $flag = false;
                                    $servctr++;
                                }
                            }
                            $client_ctr++;

                            //$row++;
                            $ctr2++;
                        }

                        $flag = false;
                        $trans_ctr = 0;
                        $arr2 = [];
                        foreach($transactions as $t){
                            if($t->tdate == $d){
                                if($t->type != 'Discount' || ($t->type == 'Discount' && ($t->service_id == null || $t->service_id == 0))){
                                    $dtype = '';
                                    // $sign = ' +';
                                    // if($flag= false){
                                    //     $tempTotal = $groupcost - $lastcost;
                                    // }
                                    // else{
                                    //     $tempTotal = $groupcost + $lastcost;
                                    // }


                                    if($t->type == 'Deposit'){
                                        $dtype = '预存款';

                                        //$lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }
                                    if($t->type == 'Discount'){
                                        $dtype = '折扣';
                                        //$lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }                            
                                    if($t->type == 'Payment'){
                                        $dtype = '已付款';
                                        //$lastcost = -1 *$t->amount;
                                        //$tempTotal = $groupcost - $lastcost;
                                    }                            
                                    if($t->type == 'Refund'){
                                        $dtype = '退款';
                                        //$lastcost = $t->amount;
                                        //$sign = '-';
                                        //$tempTotal = $groupcost + (-1*$lastcost);
                                    }

                                    $arr2[$trans_ctr]['detail'] = $t->type;
                                    $arr2[$trans_ctr]['detail_cn'] = $dtype;
                                    $arr2[$trans_ctr]['total_cost'] = $t->amount;
                                    //$arr2[$trans_ctr]['tempTotal'] = $tempTotal;
                                    }
                                    //$groupcost = $tempTotal;

                                    //$flag = true;
                                    $trans_ctr++;

                            }
                        }
                        ByBatch::updateOrCreate(
                           ['group_id' => $groupId, 'date' => $dt],
                           ['services' => json_encode($arr) ,'transactions' => json_encode($arr2), 'date2' => $dt2]
                        );
                    }
                    $datectr++;
                    //if(!empty($tlist) == ''){
                        //ByBatch::where('group_id', $groupId)->where('date',$dt)->updateOrCreate([$tlist]);
                    //}

                }
            //\Log::info($arr);
            //\Log::info($arr2);
            //\Log::info(!empty($tlist));
            // if(!empty($tlist) == ''){
            //     ByBatch::updateOrCreate(
            //        ['group_id' => $groupId, 'date' => $dt],
            //        ['services' => json_encode($arr) ,'transactions' => json_encode($arr2), 'date2' => $dt2]
            //     );
            //     //ByBatch::where('group_id', $groupId)->where('date',$dt)->updateOrCreate([$tlist]);
            // }

        }
        $data['status'] = 'success';
        return response()->json($data);
    }

    public function createPackagesServices($groupId) { // jeff
        $group_members = GroupMember::where('group_id', $groupId)->where('client',null)->get();
        $arr = [];

        foreach($group_members as $gm){
            $usr =  User::where('id',$gm->client_id)->select('id','first_name','last_name')->limit(1)->get()->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_points', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_complete_cost', 'total_balance', 'collectable', 'branch', 'three_days']);
            if($usr){
                $arr = $usr[0];
                $arr['packages'] = Package::where('client_id',$gm->client_id)->where('group_id',$gm->group_id)->get();
                $arr['uniqueDocumentsOnHand'] = $this->uniqueDocumentsOnHand($gm->client_id);
                //$ctr++;
            }
            //if($gm->client == null){
                GroupMember::updateOrCreate(
                               ['group_id' => $groupId, 'client_id' => $gm->client_id],
                               ['client' => json_encode($arr)]
                            );
            //}
        }
        $data['status'] = 'success';
        return response()->json($data);

    }

    private function uniqueDocumentsOnHand($clientId) {
        $clientServices = ClientService::has('documentLogs')
            ->with(['documentLogs' => function($query) {
                $query->orderBy('id', 'desc');
            }])
            ->where('active', 1)
            ->where('client_id', $clientId)
            ->get()
            ->makeHidden([
                'detail_cn','docs_needed','docs_optional', 'discount_amount', 'group_binded', 'parent_id',
                'documentLogs.documents_string', 'documentLogs.documents_on_hand_string'
            ]);

        $documentsOnHand = [];

        foreach($clientServices as $clientService) {
            if( $clientService->documentLogs[0]->documents_on_hand  ) {
                $documentsOnHand[] = $clientService->documentLogs[0]->documents_on_hand;
            }
        }

        $docsIdArr = explode(',', implode(',', $documentsOnHand));

        return DB::connection('visa')->table('service_documents')->where('is_unique', 1)->whereIn('id', $docsIdArr)->pluck('id');
    }

    public function getGroupMembers($id) {
        $group=[];
        $group['groupmember'] = GroupMember::where('group_id',$id)->get();
        $mem =0;
        foreach($group['groupmember'] as $gm){
            $user = User::where('id',$gm->client_id)->select('id','first_name','last_name')->first()->makeHidden([
            "total_points",
            "total_deposit",
            "total_discount",
            "total_refund",
            "total_payment",
            "total_cost",
            "total_balance",
            "document_receive",
            "unread_notif",
            "group_binded",
            "access_control",
            "permissions",
            "three_days",
            ]);
            $group['groupmember'][$mem]['user_details'] = $user; 
            $mem++;
        }
        return Response::json($group);
    }

    public function groupInfo($id) {
        $grid = GroupMember::where('client_id',$id)->pluck('group_id');
        $groups = Group::whereIn('id',$grid)->get();
        $ctr = 0;
        $group=[];
        if($groups){
            foreach($groups as $g){
                $ifLead = GroupMember::where('client_id',$id)->where('group_id',$g->id)->where('leader',2)->count();
                if($g->leader == $id || $ifLead > 0){
                    $group[$ctr] = Group::where('id',$g->id)
                                    ->with('groupmember')
                                    ->first();
                    $group[$ctr]['leader'] = $id;
                    //$group[$ctr]['member_count'] = GroupMember::where('group_id',$g->id)->count();
                }
                else{
                    $group[$ctr] = Group::where('id',$g->id)->first();
                    //$group[$ctr]['member_count'] = GroupMember::where('group_id',$g->id)->count();
                    $group[$ctr]['groupmember'] = GroupMember::where('client_id',$id)->where('group_id',$g->id)->get();
                }
                $group_id = $g->id;
                $initialdeposit = (
                    (
                        floatval($this->clientDeposit($id,$group_id))
                        + floatval($this->clientPayment($id,$group_id))
                        + floatval($this->clientDiscount($id,$group_id))
                    )
                    - 
                    (
                        floatval($this->clientRefund($id,$group_id))
                        + floatval($this->clientServiceCost($id,$group_id))
                    )
                );

                $lead = User::findorFail($g->leader);
                $group[$ctr]['leader_name'] = $lead->first_name." ".$lead->last_name;
                $group[$ctr]['deposit'] = $this->clientDeposit($id,$group_id);
                $group[$ctr]['refund'] = $this->clientRefund($id,$group_id);
                $group[$ctr]['discount'] = $this->clientDiscount($id,$group_id);
                $group[$ctr]['payment'] = $this->clientPayment($id,$group_id);
                $group[$ctr]['cost'] = $this->clientServiceCost($id,$group_id);
                $group[$ctr]['balance'] = Common::formatMoney($initialdeposit, true);

                $mem =0;
                foreach($group[$ctr]->groupmember as $gm){
                    $user = User::where('id',$gm->client_id)->select('first_name','last_name')->first()->makeHidden([
                    "total_points",
                    "total_deposit",
                    "total_discount",
                    "total_refund",
                    "total_payment",
                    "total_cost",
                    "total_balance",
                    "document_receive",
                    "unread_notif",
                    "group_binded",
                    "access_control",
                    "permissions",
                    "three_days",
                    ]);
                    $group[$ctr]['groupmember'][$mem]['user_details'] = $user; 
                    $packs = Package::where('client_id',$gm->client_id)->where('group_id',$g->id)->with('service.report')->get();
                    $group[$ctr]['groupmember'][$mem]['packages'] = $packs;
                    $ctr2 = 0;
                    foreach($group[$ctr]['groupmember'][$mem]['packages'] as $p){
                        $pt = ClientService::
                                select(DB::raw('sum(cost+charge+tip+com_client+com_agent) as total_cost'))
                                ->where('group_id',$p->group_id)
                                ->where('tracking',$p->tracking)
                                ->where('active',1)
                                ->get();
                        $group[$ctr]['groupmember'][$mem]['packages'][$ctr2]['total_service_cost'] = $pt[0]->total_cost;
                        $ctr2++;
                    }
                    $mem++;
                }
                $ctr++;

            }
        }
        return Response::json($group);
    }

    public function groupInfoV2($id) {
        $grid = GroupMember::where('client_id',$id)->pluck('group_id');
        $groups = Group::whereIn('id',$grid)->get();
        $ctr = 0;
        $group=[];
        if($groups){
            foreach($groups as $g){
                $ifLead = GroupMember::where('client_id',$id)->where('group_id',$g->id)->where('leader',2)->count();
                if($g->leader == $id || $ifLead > 0){
                    $group[$ctr] = Group::where('id',$g->id)
                                    //->with('groupmember')
                                    ->first();
                    $group[$ctr]['leader'] = $id;
                    $group[$ctr]['member_count'] = GroupMember::where('group_id',$g->id)->count();
                }
                else{
                    $group[$ctr] = Group::where('id',$g->id)->first();
                    $group[$ctr]['member_count'] = GroupMember::where('group_id',$g->id)->count();
                    //$group[$ctr]['groupmember'] = GroupMember::where('client_id',$id)->where('group_id',$g->id)->get();
                }
                $group_id = $g->id;
                $initialdeposit = (
                    (
                        floatval($this->clientDeposit($id,$group_id))
                        + floatval($this->clientPayment($id,$group_id))
                        + floatval($this->clientDiscount($id,$group_id))
                    )
                    - 
                    (
                        floatval($this->clientRefund($id,$group_id))
                        + floatval($this->clientServiceCost($id,$group_id))
                    )
                );

                $lead = User::findorFail($g->leader);
                $group[$ctr]['leader_name'] = $lead->first_name." ".$lead->last_name;
                $group[$ctr]['deposit'] = $this->clientDeposit($id,$group_id);
                $group[$ctr]['refund'] = $this->clientRefund($id,$group_id);
                $group[$ctr]['discount'] = $this->clientDiscount($id,$group_id);
                $group[$ctr]['payment'] = $this->clientPayment($id,$group_id);
                $group[$ctr]['cost'] = $this->clientServiceCost($id,$group_id);
                $group[$ctr]['balance'] = Common::formatMoney($initialdeposit, true);

                // $mem =0;
                // foreach($group[$ctr]->groupmember as $gm){
                //     $user = User::where('id',$gm->client_id)->select('first_name','last_name')->first()->makeHidden([
                //     "total_points",
                //     "total_deposit",
                //     "total_discount",
                //     "total_refund",
                //     "total_payment",
                //     "total_cost",
                //     "total_balance",
                //     "document_receive",
                //     "unread_notif",
                //     "group_binded",
                //     "access_control",
                //     "permissions",
                //     "three_days",
                //     ]);
                //     $group[$ctr]['groupmember'][$mem]['user_details'] = $user; 
                //     $packs = Package::where('client_id',$gm->client_id)->where('group_id',$g->id)->with('service.report')->get();
                //     $group[$ctr]['groupmember'][$mem]['packages'] = $packs;
                //     $ctr2 = 0;
                //     foreach($group[$ctr]['groupmember'][$mem]['packages'] as $p){
                //         $pt = ClientService::
                //                 select(DB::raw('sum(cost+charge+tip) as total_cost'))
                //                 ->where('group_id',$p->group_id)
                //                 ->where('tracking',$p->tracking)
                //                 ->where('active',1)
                //                 ->get();
                //         $group[$ctr]['groupmember'][$mem]['packages'][$ctr2]['total_service_cost'] = $pt[0]->total_cost;
                //         $ctr2++;
                //     }
                //     $mem++;
                // }
                $ctr++;

            }
        }
        return Response::json($group);
    }

    public function memberPackageServices($group_id, $client_id){
        //$packs = Package::where('client_id',$client_id)->where('group_id',$group_id)->where('status','!=',0)->with('service.report')->get();
        $packs = Package::where('client_id',$client_id)->where('group_id',$group_id)->where('status','!=',0)->get();
        $group['packages'] = $packs;
        $ctr2 = 0;
        $total_overall = 0;
        foreach($group['packages'] as $p){
            $pt = ClientService::
                    select(DB::raw('sum(cost+charge+tip+com_client+com_agent) as total_cost'))
                    ->where('group_id',$p->group_id)
                    ->where('tracking',$p->tracking)
                    ->where('active',1)
                    ->get();
            $servs = ClientService::
                    select('id','client_id','service_id','group_id','detail','cost','tip','charge','com_client','com_agent','status','service_date','remarks','active',DB::raw('(charge+com_client+com_agent) as charge'))
                    ->where('group_id',$p->group_id)
                    ->where('tracking',$p->tracking)
                    ->where('active',1)->with('report')
                    ->get();
            $group['packages'][$ctr2]['service'] = $servs;
            $group['packages'][$ctr2]['total_service_cost'] = $pt[0]->total_cost;
            $total_overall += $pt[0]->total_cost;
            $ctr2++;
        }
        $group['total_overall'] = $total_overall;

        return Response::json($group);
    }


    public function getAllUserNotif($id){
        // $notifs = PushNotificationsModel::select(DB::raw('max(id) as id'),'client_id','type_id')
        //              ->where('client_id',$id)
        //              ->orderBy('id','desc')
        //              ->groupBy('type_id')
        //              ->get();
        // $ctr = 0;
        // $data = array(
        //     'notifications' => $notifs,
        // );
        // foreach($notifs as $n){
        //     $parent = PushNotificationsModel::find($n->id);
        //     $data['notifications'][$ctr]['title'] = $parent->title; 
        //     $data['notifications'][$ctr]['title_cn'] = $parent->title_cn; 
        //     $data['notifications'][$ctr]['body'] = $parent->body; 
        //     $data['notifications'][$ctr]['body_cn'] = $parent->body_cn; 
        //     $data['notifications'][$ctr]['extra'] = $parent->extra; 
        //     $data['notifications'][$ctr]['type'] = $parent->type; 
        //     $data['notifications'][$ctr]['main'] = $parent->main; 
        //     $data['notifications'][$ctr]['main_cn'] = $parent->main_cn; 
        //     $data['notifications'][$ctr]['seen_at'] = $parent->seen_at; 
        //     $data['notifications'][$ctr]['read_at'] = $parent->read_at; 
        //     $data['notifications'][$ctr]['created_at'] = $parent->created_at; 
        //     $data['notifications'][$ctr]['updated_at'] = $parent->updated_at; 
        //     $child = PushNotificationsModel::where('client_id',$id)
        //                 ->where('type_id',$n->type_id)
        //                 ->where('id','!=',$n->id)
        //                 ->orderBy('id','desc')
        //                 ->get();
        //     $data['notifications'][$ctr]['child'] = $child; 
        //     $ctr++;
        // }
        $page = 5;
        $is_leader = GroupMember::where('client_id',$id)->where('leader','>',0)->first();
        if($is_leader){
            $page = 2;
        }

        $notifs = PushNotificationsModel::where('client_id',$id)
                     ->where('parent_type',0)
                     ->orderBy('updated_at','desc')
                     ->paginate($page);
        $ctr = 0;
        $data = array(
            'notifications' => $notifs,
        );
        foreach($notifs as $n){
            $child = PushNotificationsModel::where('client_id',$id)
                        ->where('parent',$n->type_id)
                        ->where('parent_type',1)
                        ->orderBy('updated_at','desc')
                        ->get();
            $push = PushNotificationsModel::where('client_id',$id)
                        ->where('id',$n->id)
                        ->orderBy('updated_at','desc')
                        ->get();
            $childrens = $child->merge($push);
            $data['notifications'][$ctr]['nparent'] = $childrens; 
            $chi = 0;
            foreach($child as $c){
               $child2 = PushNotificationsModel::where('client_id',$id)
                        ->where('parent',$c->type_id)
                        ->where('parent_type',2)
                        ->orderBy('updated_at','desc')
                        ->get();
                $push2 = PushNotificationsModel::where('client_id',$id)
                        ->where('id',$c->id)
                        ->where('type','Service')
                        ->orderBy('updated_at','desc')
                        ->get();
                $grandchildren = $child2->merge($push2);
                $data['notifications'][$ctr]['nparent'][$chi]['nchild'] = $grandchildren;              
                $chi++;
            }
            $ctr++;
        }
        return Response::json($data);
    }

    public function seenNotif(Request $request){
        $client_id = $request['client_id'];
        $d = Carbon::now('Asia/Manila');
        $notifs = PushNotificationsModel::where('client_id',$client_id)->where('seen_at','0000-00-00 00:00:00')->orwhere('seen_at',null)->update(['seen_at' => $d]);
        if($notifs > 0){
            $data['status'] = 'success';
        }
        else{
            $data['status'] = 'No record updated';
        }
        return response()->json($data);
    }

    public function readNotif(Request $request){
        $id = $request['id'];
        $notifs = PushNotificationsModel::find($id);
        $countNotifs =  count($notifs);

        $d = Carbon::now('Asia/Manila');
        if($countNotifs> 0){
            $notifs->read_at = $d;
            $notifs->save();
            $data['status'] = 'success';
        }
        else{
            $data['status'] = 'No record found';
        }
        return response()->json($data);
    }

    public function readAllNotif(Request $request){
        $id = $request['client_id'];
        $d = Carbon::now('Asia/Manila');
        $notifs = PushNotificationsModel::where('client_id', $id)->where('read_at',null)->update(['read_at' => $d]);

        if($notifs){
            $data['status'] = 'success';
        }
        else{
            $data['status'] = 'No record found';
        }
        return response()->json($data);
    }

    public function getServiceCategory(){
        $parent = Service::where('parent_id',0)->where('is_active',1)->get();
        $data = [];
        $ctr = 0;
        foreach($parent as $p){
            $data['services'][$ctr] = Service::where('id',$p->id)->where('is_active',1)->first();
            $data['services'][$ctr]['child']  = Service::where('parent_id',$p->id)->where('is_active',1)->get();
            $ctr++;
        }
        return response()->json($data);
    }

    public function transferRemarks(){
        $cs = ClientService::where('remarks','!=','')->get();
        foreach($cs as $c){
        $rem = explode("</br>", $c->remarks);
        $atr = 1;
        foreach($rem as $r){
            $cx = strip_tags($r);
            $parts = explode('(', trim($cx, ' )')); //get strings inside parentheses
            $date = end($parts); // get last string only
            $date = $date." 09:00:00";
            if((date('Y-m-d H:i:s', strtotime($date)) == $date)){
                $date = $date;
                $cx = substr($cx, 0, -13);
                $last = strrpos($cx,"-");
                $author = trim(substr($cx,$last+1));
                $cx = trim(substr($cx, 0, $last));

                $users = User::where('first_name',$author)->get();
                foreach($users as $u){
                    if($u->hasRole('cpanel-admin') || $u->hasRole('master')){
                        $atr = $u->id;
                    }
                }
            }
            else{
                // $date = Carbon::now()->format('Y-m-d H:i:s a');
                $dt = $c->service_date." 09:00:00";
                $date = Carbon::createFromFormat('m/d/Y H:i:s', $dt);
                $author = 1;
            }
                if(trim($cx)!=''){

                    Remark::create([
                        'service_id' => $c->id,
                        'detail' => $cx,
                        'client_id' => $c->client_id,
                        'group_id' => $c->group_id,
                        'user_id' => $atr,
                        'tracking' => $c->tracking,
                        'log_date' => $date,
                        'is_read' => 0
                    ]);
                }
        }
        }
        return response()->json($c);
    }


    public function trackNumber(Request $request){
        if(!is_null($request->isAdmin))
            $isAdmin = $request->isAdmin;
        else
            $isAdmin = null;

        $messages = [
            'package.required' => 'Package Number is required.',
        ];

        $validator = Validator::make($request->all(), [
            'package' => 'required',
        ],$messages);

        if ($validator->fails()) {
            $data['package'] = $validator->errors()->all();

            return response()->json($data,422);
        };

        $tracking = strtoupper($request['package']);
        $data = [];
            $pos = null;
        if($tracking){
            $validator->after(function ($validator) use ($tracking) {
                $pos = strpos($tracking, "GL");
                if($pos !== false){
                    $data = Group::where('tracking', $tracking)->count();
                }else{
                    $data = Package::where('tracking', $tracking)->count();
                }
                if(empty($data) || is_null($data)){
                    $validator->errors()->add('package', 'No records found. Please try again.');
                }

            });
        }


        if ($validator->fails()) {
            $data['package'] = $validator->errors()->all();
            return response()->json($data,422);
        };

        $pos = strpos($tracking, "GL");
        //return $pos;
        if($pos !== false){
            $data =  $this->wholeGroupTracking($tracking, $isAdmin);
        }else
        {
            $data =  $this->getTrackClientPackage($tracking, $isAdmin);
        }

        return response()->json($data);
    }

    /* PRIVATE */
    private function wholeGroupTracking($package, $isAdmin)
    {
        $tracking = $package;
        $group = Group::where('tracking', $tracking)->get();
        if ($group) :
            foreach ($group as $v) :
                $whole_grp = ClientService::
                    where('group_id', $v->id)
                    ->get();
                $group_id = $v->id;
                $grp_name = $v->name;
            endforeach;
        endif;

        $group_deposit = ClientTransactions::where('type', 'Deposit')->where('group_id', strtoupper($group_id))->sum('amount');

        $group_payment = ClientTransactions::where('type', 'Payment')->where('group_id', strtoupper($group_id))->sum('amount');

        $group_discount = ClientTransactions::where('type', 'Discount')->where('group_id', strtoupper($group_id))->sum('amount');

        $group_refund = ClientTransactions::where('type', 'Refund')->where('group_id', strtoupper($group_id))->sum('amount');

        $initialdeposit = (
            (
                Common::toInt(Common::formatMoney($group_deposit))
                + Common::toInt(Common::formatMoney($group_payment))
                + Common::toInt(Common::formatMoney($group_discount))
            )
            - Common::toInt(Common::formatMoney($group_refund))
        );


        $totalcost = ClientService::
            select(DB::raw('sum(cost+charge+tip+com_client+com_agent) as total_amount_sum'))
            ->where('group_id', $group_id)
            ->where('active', 1)
            ->orderBy('id', 'desc')
            ->get();

        $whole_total_cost = $totalcost[0]->total_amount_sum;

        $mem = GroupMember::where('group_id',$group_id)->select('client_id')->get();

        foreach($mem as $m){
            $client = User::where('id',$m->client_id)->select('first_name','last_name','id')->first();
            $services = ClientService::where('client_id',$client->id)->where('group_id',$group_id)
                ->where(function($query) use($isAdmin) {
                    if(!is_null($isAdmin))
                        $query->where('active', 1)->orWhere('active', 0);
                    else
                        $query->where('active', 1);
                })
                ->select('id','detail','status','service_date','remarks','tracking','active','cost','charge','tip','group_id', 'extend', 'remarks')
                ->get();
            $total_cost = 0;  

            $ctr = 0;
            foreach($services as $l){
                if($l->group_id > 0){
                    $gr = Group::find($l->group_id);
                    if($gr){
                        $list[$ctr]['group_name'] = $gr->name; 
                    }
                    else{
                        $list[$ctr]['group_name'] = ''; 
                    }
                }
                else{
                    $list[$ctr]['group_name'] = ''; 
                }
                $services[$ctr]['reports'] = Report::where('service_id',$l->id)->get();
                $ctr++;
            }
            
            $members[] = [
                'client_id' => $client->id,
                'fname' => $client->first_name,
                'lname' => $client->last_name,
                'total_cost' => $total_cost,
                'services' => $services,
            ];
        }            

        $totalcost = ClientService::
        select(DB::raw('sum(cost+charge+tip+com_client+com_agent) as total_amount_sum'))
            ->where('tracking', strtoupper($tracking))
            ->where('active', 1)
            ->where('group_id', '!=', 0)
            ->orderBy('id', 'desc')
            ->get();
        $total_amount_sum = $totalcost[0]->total_amount_sum;

        $data['deposit'] =  $group_deposit;
        $data['refund'] =  $group_refund;
        $data['discount'] =  $group_discount;
        $data['payment'] =  $group_payment;
        $data['cost'] = $whole_total_cost;
        $data['balance'] = $initialdeposit - $whole_total_cost;
        $data['group'] = $grp_name;
        $data['group_id'] = $group_id;
        $data['slug'] = "wyc-tracking";
        $data['tracking'] = $package;
        $data['members'] = $members;
        $data['total_amount_sum'] = $total_amount_sum;

        return $data;
    }

    private function getTrackClientPackage($package, $isAdmin)
    {
         if(strpos(strtoupper($package),'G')!==false){
            $package = strtoupper($package);
            $client_id = Package::where('tracking', '=', $package)->select('client_id')->firstOrFail();
            $client = User::where('id','=',$client_id->client_id)->firstOrFail();
            //$client = PackageModel::find($package)->client->where('client_id','=',PackageModel::find($package)->client_id)->first();
            $group = Package::where("tracking",'=',$package)->select('group_id')->firstOrFail();
            $group_id = $group->group_id;
            $group_deposit = ClientTransactions::where('group_id', $group_id)->where('type','Deposit')->sum('amount');
            $group_payment = ClientTransactions::where('group_id', $group_id)->where('type','Payment')->sum('amount');
            $group_discount = ClientTransactions::where('group_id', $group_id)->where('type','Discount')->sum('amount');
            $group_refund = ClientTransactions::where('group_id', $group_id)->where('type','Refund')->sum('amount');

        $totalcost = ClientService::
            select(DB::raw('sum(IFNULL(cost,0)+IFNULL(charge,0)+IFNULL(tip,0)+IFNULL(com_client,0)+IFNULL(com_agent,0)) as total_amount_sum'))
            ->where('tracking', strtoupper($package))
            ->where('active', 1)
            ->where('group_id', '!=', 0)
            ->orderBy('id', 'desc')
            ->get();
        $total_cost = $totalcost[0]->total_amount_sum;

        $gMoney = ($group_payment + $group_deposit + $group_discount)-$group_refund;
        //$GpackageCost = $this->GetGroupCost($groupID->group_id);
        $CostGroupPackage = ClientService::
            select(DB::raw('sum(IFNULL(cost,0)+IFNULL(charge,0)+IFNULL(tip,0)+IFNULL(com_client,0)+IFNULL(com_agent,0)) as total, tracking'))
            ->where('group_id', $group_id)
            ->where('active', 1)
            ->groupBy('tracking')
            ->orderBy('id', 'asc')
            ->get();
            //dd($CostGroupPackage);

                foreach($CostGroupPackage as $g){
                  if($gMoney<0){
                      $gMoney = 0;
                  }
                  $gMoney -= $g->total;
                  if($g->tracking == $package){
                      if($gMoney>0){
                        $bal = 0;
                        $dep = $total_cost;
                      }
                      else{
                        $bal = $gMoney;
                        $dep = $bal + $total_cost;
                      }
                  }
                }//end if

                $package_deposit = ClientTransactions::
                    where('group_id', strtoupper($group_id))
                    ->where('tracking', $package)
                    ->where('type','Deposit')
                    ->sum('amount');

                $package_discount = ClientTransactions::
                    where('group_id', $group_id)
                    ->where('tracking', $package)
                    ->where('type','Discount')
                    ->sum('amount');

                $package_refund = ClientTransactions::
                    where('group_id', $group_id)
                    ->where('tracking', $package)
                    ->where('type','Refund')
                    ->sum('amount');

                //passing values
                $cost = $total_cost;
                $payment = $dep;
                $deposit = $package_deposit;
                $refund = $package_refund;
                $discount = $package_discount;
                $balance = $bal;

        }else {
            $client_id = Package::where('tracking', '=', $package)->select('client_id')->first();
            $client = User::where('id','=',$client_id->client_id)->first();
            $group = null;
            $totalcost = ClientService::
            select(DB::raw('sum(IFNULL(cost,0)+IFNULL(charge,0)+IFNULL(tip,0)+IFNULL(com_client,0)+IFNULL(com_agent,0)) as total_amount_sum'))
            ->where('tracking', strtoupper($package))
            ->where('active', 1)
            ->where('group_id', 0)
            ->orderBy('id', 'desc')
            ->get();
            $cost = $totalcost[0]->total_amount_sum;
            $deposit = ClientTransactions::where('tracking', $package)->where('type','Deposit')->sum('amount');
            $payment = ClientTransactions::where('tracking', $package)->where('type','Payment')->sum('amount');
            $discount = ClientTransactions::where('tracking', $package)->where('type','Discount')->sum('amount');
            $refund = ClientTransactions::where('tracking', $package)->where('type','Refund')->sum('amount');
            //$cost = PackageModel::find($package)->services->sum('amount') + PackageModel::find($package)->services->sum('charge') + PackageModel::find($package)->services->sum('charge2');
            // $deposit = PackageModel::find($package)->deposit->sum('deposit_amount');
            // $discount = PackageModel::find($package)->discount->sum('discount_amount');
            // $payment = PackageModel::find($package)->payment->sum('payment_amount');
            // $refund = PackageModel::find($package)->refund->sum('refund_amount');
            $initialdeposit = (
                (
                    Common::formatMoney($deposit)
                    + Common::formatMoney($payment)
                    + Common::formatMoney($discount)
                )
                - Common::formatMoney($refund)
            );
            $trackcost = ClientService::
                select(DB::raw('round(sum(cost))+round(sum(charge))+round(sum(tip))+round(sum(com_client))+round(sum(com_agent)) as total'))
                ->where('tracking', $package)
                ->where('group_id', 0)
                ->where('active', 1)
                ->first();

            $totalcost = Common::formatMoney($trackcost->total);

            $balance = $initialdeposit - $totalcost;
          }

        // $reports = ReportModel::where('tracking','=',$package)->get();
        $reports = Report::where('tracking',$package)->get();
        // $reports = DB::connection('mysql')
        //     ->table('tbl_reports as rep')
        //     ->where('tracking','=',$package)
        //     ->get();
        //$list = PackageModel::find($package)->services;
        $list = ClientService::where('tracking',$package)
            ->with('discount')
            ->where(function($query) use($isAdmin) {
                if(!is_null($isAdmin))
                    $query->where('active', 1)->orWhere('active', 0);
                else
                    $query->where('active', 1);
            })
            ->get();
        $list = ClientService::
                    select('id','client_id','service_id','group_id','detail','cost','tip','charge','com_client','com_agent','status','service_date','remarks','active',DB::raw('(charge+com_client+com_agent) as charge'))
                    ->where('tracking',$package)->with('discount')
                    ->where(function($query) use($isAdmin) {
                        if(!is_null($isAdmin))
                            $query->where('active', 1)->orWhere('active', 0);
                        else
                            $query->where('active', 1);
                    })
                    ->get();
        $ctr = 0;
        foreach($list as $l){
            if($l->group_id > 0){
                $gr = Group::find($l->group_id);
                if($gr){
                    $list[$ctr]['group_name'] = $gr->group_name; 
                }
                else{
                    $list[$ctr]['group_name'] = ''; 
                }
            }
            else{
                $list[$ctr]['group_name'] = ''; 
            }
            $list[$ctr]['reports'] = Report::where('service_id',$l->id)->get();
            $ctr++;
        }

        $data =  array('slug' => 'wyc-tracking', 'list' => $list, 'package' => $package, 'cost' => $cost, 'client' => $client, 'deposit' => $deposit,'refund'=>$refund,'discount'=>$discount,'payment'=>$payment,'group'=>$group,'balance'=>$balance, 'user'=>null);
        return $data;
    }


    /* ADMIN  */
    public function latestService($id){

        $record = DB::connection('mysql')
            ->table('users as a')
            ->select(DB::raw('
                a.id,
                srv.sdates, srv.sdates2'))
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%m/%d/%Y") as sdates,cs.client_id, date_format(max(cs.servdates),"%M %e, %Y") as sdates2
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM visa.client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.client_id) as srv'),
                    'srv.client_id', '=', 'a.id')
                ->where("id",$id)
                ->first();
        $latest = [];
        if($record){
            $latest['client_id'] = $id;
            $latest['latest_service_date'] = $record->sdates2;
            $getServices = ClientService::where('client_id',$id)->where(DB::raw('date_format(STR_TO_DATE(service_date, "%m/%e/%Y"),"%m/%d/%Y")'),$record->sdates)->select('detail','status','group_id')->get();
            $ctr = 0;
            foreach($getServices as $g){
                $latest['services'][$ctr]['detail'] = $g->detail;
                $latest['services'][$ctr]['status'] = $g->status;
                $latest['services'][$ctr]['group'] = 'N/A Group';
                if($g->group_id > 0){
                    $getGroup = Group::where('id',$g->group_id)->first();
                    if($getGroup){
                        $latest['services'][$ctr]['group'] = $getGroup->name;
                    }
                }
                $ctr++;
            }
        }
        return Response::json($latest);
    }

    public function documentTypes(){
        $documents = ClientDocumentType::orderBy('name')->get();
        return $documents;
    }

    public function combineDuplicates(){
        $duplicates = DB::table('users')
                        ->select('id', 'first_name','last_name','gender','birth_date')
                        ->groupBy('first_name','last_name','gender','birth_date')
                        ->havingRaw('COUNT(*) > 1')
                        ->orderBy('id')
                        // ->limit(1)
                        ->get();
        //$duplicates = User::where('id',5385)->limit(1)->get();
        foreach($duplicates as $d){
            $old = $d->id;
            $getLatest =  DB::table('users')->where('last_name',$d->last_name)->where('first_name',$d->first_name)
                            ->where('gender',$d->gender)->where('birth_date',$d->birth_date)
                            ->where('id','!=',$old)->select('id', 'first_name','last_name','gender','birth_date')->first();
            if($getLatest){
                $new = $getLatest->id;
                ClientTransactions::where('client_id', $old)
                    ->update(['client_id' => $new]);
                ClientService::where('client_id', $old)
                    ->update(['client_id' => $new]);
                Package::where('client_id', $old)
                    ->update(['client_id' => $new]);
                Group::where('leader', $old)
                    ->update(['leader' => $new]);
                GroupMember::where('client_id', $old)
                    ->update(['client_id' => $new]);
                Remark::where('client_id', $old)
                    ->update(['client_id' => $new]);
                Report::where('client_id', $old)
                    ->update(['client_id' => $new]);
                ActionLogs::where('client_id', $old)
                    ->update(['client_id' => $new]);
                TransactionLogs::where('client_id', $old)
                    ->update(['client_id' => $new]);

                $row_count = GroupMember::where('client_id', $new)->groupBy('group_id')->havingRaw('COUNT(*) > 1')->get();
                if($row_count->count() > 0){
                    foreach($row_count as $r){
                        GroupMember::where("client_id", $new)->where('group_id',$r->group_id)
                            ->orderBy("id", "ASC")
                            ->take(1)
                            ->delete();
                    }
                }
            }
        }


        return Response::json($duplicates->count());
    }

    public function uploadDocuments(Request $request) {
        $clientDoc = ClientDocumentType::findOrFail($request->docTypeID);
        $path = '';
        $img64 = $request->imgBase64;
        $issueDate = $request->issueDate;
        $expiryDate = $request->expiryDate;
        $client_id = $request->client_id;

        $filename = $client_id.'&&'.$issueDate.'.jpeg';
        if($expiryDate != null){
            $filename = $client_id.'&&'.$issueDate.'&&'.$expiryDate.'.jpeg';
        }
            //$f = explode('&&',$filename);

               @list($type, $img64) = explode(';', $img64);
               @list(, $img64) = explode(',', $img64); 
               if($img64!=""){ // storing image in storage/app/public Folder 
                      \Storage::disk('public')->put('client-documents/'.$clientDoc->name.'/'.$filename,base64_decode($img64)); 
                } 
            $path = 'client-documents/'.$clientDoc->name.'/'.$filename;

            $imgdata = base64_decode($img64);
            //$data = getimagesizefromstring($imgdata);

            $checkDuplicate = $clientDoc->clientdocuments()->where('type',$clientDoc->name)
                                ->where('user_id',$client_id)->where('issue_at',$issueDate)->where('expires_at',$expiryDate)
                                ->first();
            if($checkDuplicate){
                $clientDoc->clientdocuments()->where('type',$clientDoc->name)
                                ->where('user_id',$client_id)->where('issue_at',$issueDate)->where('expires_at',$expiryDate)->delete();
            }

            $clientDoc->clientdocuments()->create([
                'type' => $clientDoc->name,
                'file_path' => $path,
                'user_id' => $client_id,
                'file_size' => strlen($imgdata),
                'file_mime' => 'image/jpeg',
                'issue_at' => $issueDate,
                'expires_at' => $expiryDate,
            ]);


        return json_encode([
            'success' => true,
            'message' => 'Successfully saved.',
            'path' => $path
        ]);
    }

    public function uploadDocumentsV2(Request $request) {
        $clientDoc = ClientDocumentType::findOrFail($request->docTypeID);
        $path = '';
        $img64 = $request->imgBase64;
        $issueDate = $request->issueDate;
        $expiryDate = $request->expiryDate;
        $client_id = $request->client_id;
        $id = $request->id;
        $multiple = $request->multiple;

        $multi = '';
        $checkDoc = false;
        // if($id == null || $id == ''){        
        //     $checkMultiple = ClientDocuments::where('multiple',1)->where('issue_at',$issueDate)->where('expires_at',$expiryDate)
        //                         ->where('type',$clientDoc->name)->where('user_id',$client_id)->count();

        //     if($checkMultiple > 0){
        //         $multi = ' ('.$checkMultiple.')';
        //     }
        // }
        // else{
            if($id != null && $id > 0){ 
                $checkDoc = ClientDocuments::where('id',$id)->first();
            }
            if($checkDoc){
                $getRelated = ClientDocuments::where('type',$checkDoc->type)
                                ->where('user_id',$client_id)->where('issue_at',$checkDoc->issue_at)
                                ->where('expires_at',$checkDoc->expires_at)
                                ->get();
                foreach($getRelated as $gr){
                    $del = ClientDocuments::findOrFail($gr->id);
                    $file_delete = Storage::delete('public/'.$del['file_path']);
                    $file_del = $del->delete();
                }
            }
            else{            
                $checkMultiple = ClientDocuments::where('multiple',1)->where('issue_at',$issueDate)->where('expires_at',$expiryDate)
                                    ->where('type',$clientDoc->name)->where('user_id',$client_id)->count();

                if($checkMultiple >= 0 && $multiple == 1){
                    $multi = ' ('.$checkMultiple.')';
                }

                // $checkSingle = ClientDocuments::where('multiple',0)->where('issue_at',$issueDate)->where('expires_at',$expiryDate)
                //                     ->where('type',$clientDoc->name)->where('user_id',$client_id)->count();
                // if($checkSingle > 0){
                //     $multi = ' ('.$checkMultiple.')';
                // }
            }
        // }

        $filename = $client_id.'&&'.$issueDate.$multi.'.jpeg';

        if($expiryDate != null){
            $filename = $client_id.'&&'.$issueDate.'&&'.$expiryDate.$multi.'.jpeg';
        }
            //$f = explode('&&',$filename);

               @list($type, $img64) = explode(';', $img64);
               @list(, $img64) = explode(',', $img64); 
               if($img64!=""){ // storing image in storage/app/public Folder 
                      \Storage::disk('public')->put('client-documents/'.$clientDoc->name.'/'.$filename,base64_decode($img64)); 
                } 
            $path = 'client-documents/'.$clientDoc->name.'/'.$filename;

            $imgdata = base64_decode($img64);
            //$data = getimagesizefromstring($imgdata);

            // $checkDuplicate = $clientDoc->clientdocuments()->where('type',$clientDoc->name)
            //                     ->where('user_id',$client_id)->where('issue_at',$issueDate)->where('expires_at',$expiryDate)
            //                     ->first();
            // if($checkDuplicate){
            //     $clientDoc->clientdocuments()->where('type',$clientDoc->name)
            //                     ->where('user_id',$client_id)->where('issue_at',$issueDate)->where('expires_at',$expiryDate)->delete();
            // }

            $clientDoc->clientdocuments()->create([
                'type' => $clientDoc->name,
                'file_path' => $path,
                'user_id' => $client_id,
                'file_size' => strlen($imgdata),
                'file_mime' => 'image/jpeg',
                'issue_at' => $issueDate,
                'expires_at' => $expiryDate,
                'multiple' => $multiple,
            ]);


        return json_encode([
            'success' => true,
            'message' => 'Successfully saved.',
            'path' => $path
        ]);
    }

    public function createImageFromBase64(Request $request){ 
       $file_data = $request->input('company_logo'); 
       $file_name = 'image_'.time().'.png'; //generating unique file name; 
       @list($type, $file_data) = explode(';', $file_data);
       @list(, $file_data) = explode(',', $file_data); 
       if($file_data!=""){ // storing image in storage/app/public Folder 
              \Storage::disk('public')->put($file_name,base64_decode($file_data)); 
        } 
    }

    public function getALLClients(){

        $records = DB::connection('mysql')
            ->table('users as a')
            ->select(DB::raw('
                a.id, a.first_name, a.last_name, a.balance,
                total6.dates, role.role_id,
                srv.sdates
                '))
            // ->select(DB::raw('
            //     a.*,
            //     total.total_amount as total_costs,
            //     total.service_date,
            //     total2.total_deposit,
            //     total4.total_refund,
            //     total5.total_discount,
            //     total6.dates, role.role_id,
            //     srv.sdates,
            //     (
            //         (case when total.total_amount > 0 then total.total_amount else 0 end)
            //         - (
            //             (
            //                 case when total2.total_deposit > 0 then total2.total_deposit else 0 end
            //                 + case when total3.total_payment > 0 then total3.total_payment else 0 end
            //                 + case when total5.total_discount > 0 then total5.total_discount else 0 end
            //             )
            //             - case when total4.total_refund > 0 then total4.total_refund else 0 end
            //         )
            //     ) as total_balance,
            //     total3.total_payment'))
                // ->leftjoin(DB::raw('
                //     (
                //         Select  IFNULL(sum(b.cost)+sum(b.charge)+sum(b.tip),0) as total_amount,
                //         COALESCE(sum(b.tip),0) as total_charge,
                //         b.client_id, b.service_date
                //         from visa.client_services as b
                //         where b.active = 1
                //         and b.group_id = 0
                //         group by b.client_id
                //         order by b.service_date desc
                //     ) as total'),
                //     'total.client_id', '=', 'a.id')
                // ->leftjoin(DB::raw('
                //     (
                //         Select  IFNULL(sum(c.deposit_amount),0) as total_deposit,
                //         c.client_id
                //         from visa.client_deposits as c
                //         where c.group_id = 0
                //         group by c.client_id
                //     ) as total2'),
                //     'total2.client_id', '=', 'a.id')
                // ->leftjoin(DB::raw('
                //     (
                //         Select  IFNULL(sum(d.payment_amount),0) as total_payment,
                //         d.client_id
                //         from visa.client_payments as d
                //         where d.group_id = 0
                //         group by d.client_id
                //     ) as total3'),
                //     'total3.client_id', '=', 'a.id')
                // ->leftjoin(DB::raw('
                //     (
                //         Select  IFNULL(sum(e.refund_amount),0) as total_refund,
                //         e.client_id
                //         from visa.client_refunds as e
                //         where e.group_id = 0
                //         group by e.client_id
                //     ) as total4'),
                //     'total4.client_id', '=', 'a.id')
                // ->leftjoin(DB::raw('
                //     (
                //         Select  IFNULL(sum(f.discount_amount),0) as total_discount,
                //         f.client_id
                //         from visa.client_discounts as f
                //         where f.group_id = 0
                //         group by f.client_id
                //     ) as total5'),
                //     'total5.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select *
                        from role_user as r
                        where r.role_id = 9
                    ) as role'),
                    'role.user_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as dates,
                        x.client_id
                        from( SELECT STR_TO_DATE(log_date, "%M %e, %Y, %l:%i %p") as dates,
                            client_id, status
                            FROM visa.packages
                            ORDER BY dates desc
                        ) as x
                        group by x.client_id) as total6'),
                    'total6.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as sdates,cs.client_id
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM visa.client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.client_id) as srv'),
                    'srv.client_id', '=', 'a.id')
                ->orderBy('a.first_name','asc')
                ->where("role.role_id","9")
                ->get();

        $clients = [];
        $ctr = 0;
        foreach ($records as $value) {
            $clients[$ctr]['name'] = ucwords($value->first_name).' '.ucwords($value->last_name);
            $clients[$ctr]['client_id'] = $value->id;
            // $clients[$ctr]['latest_package'] = $value->dates;
            $clients[$ctr]['latest_service'] = $value->sdates;
            $clients[$ctr]['balance'] = floatval($value->balance*-1);
            $ctr++;
        }
        return $clients;
    }


    public function getAllGroups(){
        $records = DB::connection('visa')
            ->table('groups as a')
            ->select(DB::raw('
                a.*,a.id as gr_id,a.balance as gr_bal, a.collectable as gr_col, b.*, 
                total6.dates,
                srv.sdates
                '))
                ->leftjoin(DB::raw('(select * from wycgroup.users) as b'), 'b.id', '=', 'a.leader')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as dates,
                        x.group_id
                        from( SELECT STR_TO_DATE(log_date, "%M %e, %Y, %l:%i %p") as dates,
                            group_id, status
                            FROM packages
                            ORDER BY dates desc
                        ) as x
                        group by x.group_id) as total6'),
                    'total6.group_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as sdates,cs.client_id,cs.group_id
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.group_id) as srv'),
                    'srv.group_id', '=', 'a.id')
                ->orderBy('a.name','asc')
                ->get();

        $count = 0;
        $bal = 0;
        $collect = 0;
        $groups = [];
        $ctr = 0;
        foreach ($records as $value) {

            $id = $value->gr_id;
            $deposit =  $this->getGroupDeposit($id);
            $payment =  $this->getGroupPayment($id);
            $discount =  $this->getGroupDiscount($id);
            $refund =  $this->getGroupRefund($id);
            $cost =  $this->getGroupCost($id);
                // $total_balance = (
                //     (
                //         $deposit
                //         + $payment
                //         + $discount
                //     )
                //     - (
                //         $refund
                //         + $cost
                //     )
                // );
                $col_balance = 0;
                $total_balance = $value->gr_bal;

                if($total_balance<0){
                // $col_balance = (
                //     (
                //         $this->getGroupDeposit($id)
                //         + $this->getGroupPayment($id)
                //         +$this->getGroupDiscount($id)
                //     )
                //     - (
                //         $this->getGroupRefund($id)
                //         + ($this->getCompleteCost($id))
                //     )
                // );
                $col_balance = $value->gr_col;
                if($col_balance > 0){
                    $col_balance = 0;
                }
                // else{
                //     $collect += $col_balance;
                // }

                }

                if($total_balance<0){
                    $bal += $total_balance;
                }
            $groups[$ctr]['group_id'] = $value->gr_id;
            $groups[$ctr]['tracking'] = $value->tracking;
            $groups[$ctr]['name'] = ucwords($value->name);
            $groups[$ctr]['leader'] = ucwords($value->first_name).' '.ucwords($value->last_name);
            $groups[$ctr]['client_id'] = $value->leader;
            $groups[$ctr]['latest_package'] = $value->dates;
            $groups[$ctr]['balance'] = $total_balance*1;
            $groups[$ctr]['collectibles'] = $col_balance*1;
            $groups[$ctr]['deposit'] = $deposit*1;
            $groups[$ctr]['payment'] = $payment*1;
            $groups[$ctr]['discount'] = $discount*1;
            $groups[$ctr]['refund'] = $refund*1;
            $groups[$ctr]['cost'] = $cost*1;
            $count++;
            $ctr++;
         }   

         return $groups;
    }

public function exportGroupSummary($id) {
        
        $getGroupMembers = DB::connection('visa')
            ->table('group_members as a')
            ->select('a.*')
            ->where('a.group_id', $id)
            ->get();

        $getGroupName = DB::connection('visa')
            ->table('groups as a')
            ->select('a.*')
            ->where('a.id', $id)
            ->first();

        $filename = $getGroupName->name .'--'. Carbon::today()->format('n/j/Y');
        Excel::create($filename, function($excel) use ($filename, $getGroupMembers, $id) {

            // Set the title
            $excel->setTitle($filename);

            // Chain the setters
            $excel->setCreator('WYC')
                  ->setCompany('WYC');

            // Call them separately
            $excel->setDescription('Group Summary');

            //First Sheet -> Group Member's Cash Flow
            $excel->sheet('Group Summary', function($sheet1) use ($getGroupMembers, $id) {
                $row = 1;
                $tempTotal = 0;
                $sheet1->row($row, array(
                    'Date', 'Service', 'Note' , 'Status' , 'Cost', 'Service Charge', 'Subtotal', 'Member Balance', 'Group Total Balance' ,' '
                ));
                $cellNum = 'A'.$row.':J'.$row;
                $sheet1->cells($cellNum, function($cells) {
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#6fc4dc');
                });
                $row++;

                foreach ($getGroupMembers as $value) {
                    $getName = DB::table('users as a')
                    ->select('a.*')
                    ->where('a.id', $value->client_id)
                    ->first();

                    $row++;
                    $sheet1->row($row, array(
                        ucfirst($getName->first_name.' '.$getName->last_name)
                    ));
                    $cellNum = 'A'.$row.':J'.$row;
                    $sheet1->mergeCells($cellNum);
                    $sheet1->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setBackground('#a6ced9');
                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                        $cells->setFontWeight('bold');
                    });

                    $getServices = DB::connection('visa')
                    ->table('client_services as a')
                    ->select('a.*')
                    ->where('a.client_id', $value->client_id)
                    ->where('a.group_id', $id)
                    ->get();
                    $bal = 0;
                    foreach ($getServices as $s) {
                        $datetime = new DateTime($s->service_date);
                        $getdate = $datetime->format('M d,Y');
                        $gettime = $datetime->format('h:i A');

                        //note
                        $the_string = $s->remarks;
                        $the_word  = "<small>";
                        $the_string = str_replace('</small>', '', $the_string);
                        $count_occur = substr_count($the_string,$the_word);
                        for($i = 0; $i<$count_occur; $i++){
                            if (strpos($the_string, $the_word) !== false) {
                                $the_string = str_replace('</br>', ' --> ', $the_string);
                                $postr = strpos($the_string, $the_word);
                                $str1 = substr($the_string,0,$postr-8);
                                $str2 = substr($the_string,$postr+7);
                                $the_string = $str1.$str2;
                            }
                        }

                        //Cost
                        $amt = $s->cost;
                        if($s->active == 0){
                            $amt = 0;
                        }


                        //Service Charge
                        $chrg = $s->charge;
                        if($s->active == 0){
                            $chrg = 0;
                        }

                        //Subtotal
                        $sub = $chrg + $amt;

                        //Per Person Balance
                        if($s->active == 0){
                            $sub = 0;
                        }
                        $bal += $sub;

                        //I
                        $tempTotal +=$sub;

                        //Cancelled
                        $sp = "";
                        if($s->active==0){
                            $sp = "CANCELLED";
                        }
                        $row++;
                         $sheet1->row($row, array(
                             $getdate, $s->detail, ucwords(strtolower($the_string)) , ucfirst($s->status) , $amt, $chrg, $sub, $bal , $tempTotal  ,$sp
                         ));

                    }

                    $row++;
                    $row++;
                }

                $row++;
                $summaryrow = $row;
                $cost = DB::connection('visa')
                    ->table('client_services')
                    ->select(DB::raw('sum(cost+charge+tip+com_client+com_agent) as total_cost'))
                    ->where('group_id', '=', $id)
                    ->where('active', '=', 1)
                    ->first();

                $total_deposit = (
                    (
                        $this->GetGroupDeposit($id)
                        + $this->GetGroupPayment($id)   
                    )
                ) + 0;

                $total_discount = $this->GetGroupDiscount($id);
                $total_refund = $this->GetGroupRefund($id);

                $total_balance = (
                    (
                        $this->GetGroupDeposit($id)
                        + $this->GetGroupPayment($id)
                        +$this->GetGroupDiscount($id)
                    )
                    - (
                        $this->GetGroupRefund($id)
                        +$this->GetGroupCost($id)
                    )
                );
                $sheet1->cell('I'.$row, function($cell) use($total_deposit) {
                    $cell->setValue('Total Deposit : '.number_format($total_deposit, 2, '.', ','));
                });

                $cellNum = 'I'.$row.':J'.$row;
                $sheet1->mergeCells($cellNum);
                $sheet1->cells($cellNum, function($cells)  {
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                });

                $row++;
                $sheet1->cell('I'.$row, function($cell) use($cost) {
                    $cell->setValue('Total Cost : '.number_format($cost->total_cost, 2, '.', ','));
                });
                $cellNum = 'I'.$row.':J'.$row;
                $sheet1->mergeCells($cellNum);
                $sheet1->cells($cellNum, function($cells) {
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                });

                $row++;
                $sheet1->cell('I'.$row, function($cell) use($total_discount) {
                    $cell->setValue('Total Discount : '.number_format($total_discount, 2, '.', ','));
                });
                $cellNum = 'I'.$row.':J'.$row;
                $sheet1->mergeCells($cellNum);
                $sheet1->cells($cellNum, function($cells) {
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                });

                $row++;
                $sheet1->cell('I'.$row, function($cell) use($total_refund) {
                    $cell->setValue('Total Refund : '.number_format($total_refund, 2, '.', ','));
                });
                $cellNum = 'I'.$row.':J'.$row;
                $sheet1->mergeCells($cellNum);
                $sheet1->cells($cellNum, function($cells) {
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                });

                $row++;
                $sheet1->cell('I'.$row, function($cell) use($total_balance) {
                    $cell->setValue('Total Balance : '.number_format($total_balance, 2, '.', ','));
                });
                $cellNum = 'I'.$row.':J'.$row;
                $sheet1->mergeCells($cellNum);
                $sheet1->cells($cellNum, function($cells) {
                    $cells->setValignment('center');
                    $cells->setBackground('#a6ced9');
                    $cells->setFontWeight('bold');
                });

                $row++;

            }); // First Sheet

            // 2nd sheet
            $excel->sheet('Deposit History', function($sheet2) use($id) {
                $gdepo = ClientTransactions::where('type', 'Deposit')->where('group_id', $id)->get();

                $row = 1;
                $sheet2->row($row, array(
                    'Amount', 'Date'
                ));
                $cellNum = 'A'.$row.':B'.$row;
                $sheet2->cells($cellNum, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#6fc4dc');
                });
                $row++; 
                $ttd = 0;
                foreach($gdepo as $d){
                    $datetime = new DateTime($d->log_date);
                    $getdate = $datetime->format('M d,Y');
                    $gettime = $datetime->format('h:i A');
                    $sheet2->row($row, array(
                        number_format($d->amount, 2, '.', ','), $getdate.' '.$gettime
                    ));
                    $cellNum = 'A'.$row.':B'.$row;
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });
                    $ttd+=$d->amount;
                    $row++; 
                }
                $row++;
                $sheet2->row($row, array(
                    'Total Deposit : '.number_format($ttd, 2, '.', ',')
                ));
                $cellNum = 'A'.$row.':B'.$row;
                $sheet2->cells($cellNum, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                });     
            }); // 2nd sheet

            // 3rd Sheet
            $excel->sheet('Payments History', function($sheet2) use($id) {
                $gdepo = ClientTransactions::where('type', 'Payment')->where('group_id', $id)->get();

                $row = 1;
                $sheet2->row($row, array(
                    'Amount', 'Date'
                ));
                $cellNum = 'A'.$row.':B'.$row;
                $sheet2->cells($cellNum, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#6fc4dc');
                });
                $row++; 
                $ttd = 0;
                foreach($gdepo as $d){
                    $datetime = new DateTime($d->log_date);
                    $getdate = $datetime->format('M d,Y');
                    $gettime = $datetime->format('h:i A');
                    $sheet2->row($row, array(
                        number_format($d->amount, 2, '.', ','), $getdate.' '.$gettime
                    ));
                    $cellNum = 'A'.$row.':B'.$row;
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });
                    $ttd+=$d->amount;
                    $row++; 
                }
                $row++;
                $sheet2->row($row, array(
                    'Total Payment : '.number_format($ttd, 2, '.', ',')
                ));
                $cellNum = 'A'.$row.':B'.$row;
                $sheet2->cells($cellNum, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                });       
            }); // 3rd Sheet

            // 4th Sheet
            $excel->sheet('Refunds History', function($sheet2) use($id) {
                $gdepo = ClientTransactions::where('type', 'Refund')->where('group_id', $id)->get();

                $row = 1;
                $sheet2->row($row, array(
                    'Amount', 'Date'
                ));
                $cellNum = 'A'.$row.':B'.$row;
                $sheet2->cells($cellNum, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#6fc4dc');
                });
                $row++; 
                $ttd = 0;
                foreach($gdepo as $d){
                    $datetime = new DateTime($d->log_date);
                    $getdate = $datetime->format('M d,Y');
                    $gettime = $datetime->format('h:i A');
                    $sheet2->row($row, array(
                        number_format($d->amount, 2, '.', ','), $getdate.' '.$gettime
                    ));
                    $cellNum = 'A'.$row.':B'.$row;
                    $sheet2->cells($cellNum, function($cells) {
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });
                    $ttd+=$d->amount;
                    $row++; 
                }
                $row++;
                $sheet2->row($row, array(
                    'Total Refunds : '.number_format($ttd, 2, '.', ',')
                ));
                $cellNum = 'A'.$row.':B'.$row;
                $sheet2->cells($cellNum, function($cells) {
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    $cells->setFontWeight('bold');
                });       
            }); // 4th Sheet

        })->export('xls'); // Excel

    }


    /* Employee */
    public function empTimeLog(Request $request){
        $user_id = $request['client_id'];
        $flag = $request['flag']; //0,1
        $d = Carbon::now('Asia/Manila');
        $date = $d->toDateString();
        $time = $d->toTimeString();

        $dt = explode('-',$date);
        $day = $dt[2];
        $month = $dt[1];
        $year = $dt[0];

        $tin = null;
        $tout = null;
        $sched = Schedule::where('user_id',$user_id)->first();
        $timeStatus = null;
        $color = "#2a2c2e";
        if($sched){
            if($sched->schedule_type_id == "1"){
                $tin = $sched->time_in;
                $tout = $sched->time_out;
            }
            if($sched->schedule_type_id == "2"){
                $tin = $sched->time_in_to;
                $tout = '';
            }
            $dt = $date.' '.$tin;
        }
        $dayname = $d->format('l');
        $checkIn = Attendance::where('user_id',$user_id)->where('day',$day)->where('month',$month)->where('year',$year)->first();
        $record = "new";
        if($checkIn){
            if($tout==''){
                $dt2 = $date.' '.$checkIn->time_in;
                $dt = Carbon::parse($dt2);
                $toutflexi = $dt->addHours(9);
                $toutflexi = $toutflexi->toTimeString();
                if (strtotime($toutflexi) < strtotime('16:00:00')) {
                    $tout = '16:00:00';
                }
                elseif(strtotime($toutflexi) > strtotime('18:00:00')){
                    $tout = '18:00:00';
                }
                else{
                    $tout = $toutflexi;
                }
                //return $tout;
            }
            
            //if early out
            if (strtotime($time) < strtotime($tout)) {
                if($flag == 0){
                    $earlyout['earlyout'] = true;
                    return response()->json($earlyout);
                }
                $color = "#ece401";
                $timeStatus = 'EARLYOUT';
            }

            $checkIn->timeout_status = $timeStatus;
            $checkIn->time_out = $time;
            $checkIn->save();
            $data['status'] = 'Time out recorded';
            $record = "Time out: ";
        }
        else{
            if($sched){                    
                if (strtotime($time) > strtotime($tin)) {
                    $lateInMinutes = $d->diffInMinutes($dt); 
                    if($lateInMinutes>30){
                        $color = "#f3a010";
                        $timeStatus = 'SLATE';
                    }
                    if($lateInMinutes> 0 && $lateInMinutes <= 30){
                        $color = "#ece401";
                        $timeStatus = 'LATE';
                    }
                }
                else{
                    $timein_status = null;
                }
            }
            $timein = new Attendance();
            $timein->user_id = $user_id;
            $timein->day = $day;
            $timein->month = $month;
            $timein->year = $year;
            $timein->time_in = $time;
            $timein->timein_status = $timeStatus;
            $timein->save();
            $data['status'] = 'Time in recorded';
            $record = "Time in: ";
        }

        //send push notif of attendance
        $title = $time;
        $title_cn = $time;
        $body = $record."<b>".$time."</b>";
        $body_cn = $record.$time;
        $main = $record."<b>".$time."</b>";
        $main_cn = $record.$time;
        $parent_type = 1;
        $parent = $date."-".$user_id;
        $extra = $color;

        Common::savePushNotif($user_id, $title, $body,'Timelog',$dayname."--".$user_id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type, null , $extra);
        return response()->json($data);
    }

    public function getTimeLog(Request $request){
        $user_id = $request['client_id'];
        $month = $request['month'];
        $year = $request['year'];
        $mode = $request['mode'];

        $user = User::findorFail($user_id);
        if($month > 0){
            $att = Attendance::where('month',$month)->where('year',$year);
        }
        else{
            $att = Attendance::where('year',$year);
        }

        if($mode == 'all'){
            $att = $att->with('userName')->get()->makeHidden('permissions');
        }
        else{
            $att = $att->where('user_id',$user_id)->get();
        }
        return $att;
    }

    /* Employee */


    /* GLOBAL */

    public function clientDeposit($client_id,$group_id=0){
        if($group_id == 0){
            return ClientTransactions::where('type', 'Deposit')
                ->where('client_id', $client_id)
                ->where('group_id', $group_id)
                ->sum('amount');
        }

        return ClientTransactions::where('type', 'Deposit')
            ->where('group_id', $group_id)
            ->sum('amount');
    }

    public function clientDiscount($client_id,$group_id=0){
        if($group_id == 0){
            return ClientTransactions::where('type', 'Discount')
                ->where('client_id', $client_id)
                ->where('group_id', $group_id)
                ->sum('amount');
        }

        return ClientTransactions::where('type', 'Discount')
                ->where('group_id', $group_id)
                ->sum('amount');
    }
    
    public function clientPayment($client_id,$group_id=0){
        if($group_id == 0){
            return ClientTransactions::where('type', 'Payment')
                ->where('client_id', $client_id)
                ->where('group_id', $group_id)
                ->sum('amount');
        }

        return ClientTransactions::where('type', 'Payment')
                ->where('group_id', $group_id)
                ->sum('amount');
    }

    public function clientRefund($client_id,$group_id=0){
        if($group_id == 0){
            return ClientTransactions::where('type', 'Refund')
                ->where('client_id', $client_id)
                ->where('group_id', $group_id)
                ->sum('amount');
        }

        return ClientTransactions::where('type', 'Refund')
                ->where('group_id', $group_id)
                ->sum('amount');
    }

    public function clientServiceCost($client_id,$group_id=0){
        if($group_id == 0){
            return ClientService::where('client_id',$client_id)
                    ->where('group_id', $group_id)
                    ->where('active', 1)
                    ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
        }
        return ClientService::where('group_id', $group_id)
                    ->where('active', 1)
                    ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));
    }

    public function getGroupDeposit($group_id){
        return ClientTransactions::where('type', 'Deposit')->where('group_id',$group_id)->sum('amount');
    }

    public function getGroupDiscount($group_id){
        return ClientTransactions::where('type', 'Discount')->where('group_id',$group_id)->sum('amount');
    }
    
    public function getGroupPayment($group_id){
        return ClientTransactions::where('type', 'Payment')->where('group_id',$group_id)->sum('amount');
    }

    public function getGroupRefund($group_id){
        return ClientTransactions::where('type', 'Refund')->where('group_id',$group_id)->sum('amount');
    }

    public function getCompleteCost($group_id){
        $group_cost = ClientService::select(DB::raw('sum(cost+charge+tip+com_client+com_agent) as total_cost'))
            ->where('group_id', '=', $group_id)
            ->where('active', '=', 1)
            ->where('status', '=', 'complete')
            ->first();

        return $group_cost->total_cost;
    }

    public function getClientCompleteCost($client_id){
        $client_cost = ClientService::select(DB::raw('sum(cost + charge + tip + com_client + com_agent) as total_cost'))
            ->where('client_id', '=', $client_id)
            ->where('group_id', '=', 0)
            ->where('active', '=', 1)
            ->where('status', '=', 'complete')
            ->first();

        return $client_cost->total_cost;
    }

    public function getClientCompleteCostDec1($client_id){
        $client_cost = ClientService::select(DB::raw('sum(cost + charge + tip + com_client + com_agent) as total_cost'))
            ->where('client_id', '=', $client_id)
            ->where('group_id', '=', 0)
            ->where('active', '=', 1)
            ->where('status', '=', 'complete')
            ->where('id','<=',67386)
            ->first();

        return $client_cost->total_cost;
    }

    public function getCompleteCostDec1($group_id){
        $group_cost = ClientService::select(DB::raw('sum(cost+charge+tip+com_client+com_agent) as total_cost'))
            ->where('group_id', '=', $group_id)
            ->where('active', '=', 1)
            ->where('status', '=', 'complete')
            ->where('id','<=',67386)
            ->first();

        return $group_cost->total_cost;
    }

    public function getGroupCost($group_id){
        $group_cost = ClientService::where('group_id', $group_id)
            ->where('active', 1)
            ->value(DB::raw("SUM(cost + charge + tip + com_client + com_agent)"));

        return $group_cost;
    }

    private function getGroupBalance($group_id){
        return (
            (
                $this->getGroupDeposit($group_id)
                + $this->getGroupPayment($group_id)
                +$this->getGroupDiscount($group_id)
            )
            - 
            (
                $this->getGroupRefund($group_id)
                + $this->getGroupCost($group_id)
            )
        );
    }

    private function groupCompleteBalance($group_id){
        $balance = ((
                        $this->getGroupDeposit($group_id)
                        + $this->getGroupPayment($group_id)
                        + $this->getGroupDiscount($group_id)
                    )-(
                        $this->getGroupRefund($group_id)
                        + $this->getCompleteCost($group_id)
                    ));
        // if($balance > 0){
        //     return 0;
        // }
        return $balance;
    }

    private function clientBalance($client_id){
        $balance = ((
                        Common::toInt($this->clientDeposit($client_id))
                        + Common::toInt($this->clientPayment($client_id))
                        + Common::toInt($this->clientDiscount($client_id))
                    )-(
                        Common::toInt($this->clientRefund($client_id))
                        + Common::toInt($this->clientServiceCost($client_id))
                    ));

        return $balance;
    }

    private function checkPackageComplete($pack_id){
        $package_complete = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%complete%')
            ->count();

        return $package_complete;
    }

    private function checkPackagePending($pack_id){
        $package_pending = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%pending%')
            ->count();

        return $package_pending;
    }

    private function checkPackageOnProcess($pack_id){
        $package_onprogress = DB::connection('visa')
            ->table('client_services')
            ->select('*')
            ->where('tracking', $pack_id)
            ->where('active', 1)
            ->where('status', 'like', '%on process%')
            ->count();

        return $package_onprogress;
    }

    private function check_package_updates($pack_id){
        $stat = 0; // empty

        if($this->checkPackageComplete($pack_id)>0){
            $stat = 4; // complete
        }
        if($this->checkPackageOnProcess($pack_id)>0){
            $stat = 1; // process
        }
        if($this->checkPackagePending($pack_id)>0){
            $stat = 2; // pending
        }

        $data = array('status' => $stat);

        DB::connection('visa')
            ->table('packages')
            ->where('tracking', $pack_id)
            ->update($data);
    }

    // Add Temporary Client
    public function temporaryClientValidation(Request $request) {
        $validator = Validator::make($request->all(), [
            'contact_numbers' => 'required|array',
            'contact_numbers.*.country_code' => 'required',
            'contact_numbers.*.value' => 'required|string|min:10|max:10'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 400;

            return Response::json($response, $httpStatusCode);
        } else {
            $data = [];

            foreach($request->contact_numbers as $contactNumber) {
                $temp = [];

                $initQuery = User::where(function($query) use($contactNumber) {
                    $query->where('contact_number', $contactNumber['country_code'] . $contactNumber['value'])
                        ->orWhere('contact_number', '0' . $contactNumber['value']);
                });

                $ifExist = $initQuery->select('id', 'first_name', 'last_name', 'gender', 'birth_date', 'passport')->first();

                $ifBinded = $initQuery->where(function($query) {
                    $query->where('password', '<>', '')->where('password', '<>', null);
                })->select('id', 'first_name', 'last_name', 'gender', 'birth_date', 'passport')->first();

                if($ifBinded) {
                    $temp['user'] = $ifBinded;
                } elseif($ifExist) {
                    $temp['user'] = $ifExist;
                } else {
                    $temp['user'] = null;
                }

                $temp['ifExist'] = ($ifExist) ? true : false;
                $temp['ifBinded'] = ($ifBinded) ? true : false;
                $temp['ifGroupLeader'] = ($ifExist && Group::where('leader', $ifExist->id)->first()) 
                    ? true : false;

                $data[] = $temp;
            }

            $response['status'] = 'Success';
            $response['data'] = $data;

            return Response::json($response);
        } 
    }

    private function _saveTemporaryClient($contactNumber) {
        $user = new User;
        $user->password = bcrypt('0'.$contactNumber['value']);
        $user->first_name = $contactNumber['first_name'];
        $user->last_name = $contactNumber['last_name'];
        $user->passport = $contactNumber['passport'];
        $user->birth_date = $contactNumber['birthdate'];
        $user->gender = $contactNumber['gender'];
        $user->contact_number = $contactNumber['country_code'].$contactNumber['value'];
        $user->save();

        $user->assignRole(['0' => 9]);

        // Action logs
        $act_id = $user->id;
        $detail = "Created new client -> " . $contactNumber['first_name'] . ' ' . $contactNumber['last_name'];
        Common::saveActionLogs(0, $act_id, $detail);

        return User::findOrFail($user->id)->makeHidden(['full_name', 'avatar', 'permissions', 'access_control', 'binded', 'unread_notif', 'group_binded', 'document_receive', 'is_leader', 'total_deposit', 'total_discount', 'total_refund', 'total_payment', 'total_cost', 'total_balance', 'branch']);
    }

    public function temporaryClientStore(Request $request) {
        $validator = Validator::make($request->all(), [
            'contact_numbers' => 'required|array',
            'contact_numbers.*.country_code' => 'required',
            'contact_numbers.*.value' => 'required|string|min:10|max:10',
            'contact_numbers.*.action' => 'required_if:contact_numbers.*.ifBinded,true|required_if:contact_numbers.*.ifGroupLeader,true',
            'contact_numbers.*.process' => 'required_if:contact_numbers.*.action,proceed_anyway',
            'contact_numbers.*.first_name' => 'required_if:contact_numbers.*.process,detail',
            'contact_numbers.*.last_name' => 'required_if:contact_numbers.*.process,detail',
            'contact_numbers.*.passport' => 'required_if:contact_numbers.*.process,detail',
            'contact_numbers.*.birthdate' => 'required_if:contact_numbers.*.process,detail',
            'contact_numbers.*.gender' => 'required_if:contact_numbers.*.process,detail'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 400;

            return Response::json($response, $httpStatusCode);
        } else {
            $users = [];

            foreach($request->contact_numbers as $contactNumber) {
                if($contactNumber['ifExist'] == 'false') {
                    if($contactNumber['process'] == 'complete') {
                        // SEND PUSH NOTIFICATION HERE
                    } elseif($contactNumber['process'] == 'detail') {
                        // Save Temporary Client
                        $users[] = $this->_saveTemporaryClient($contactNumber);
                    }
                } else {
                    if($contactNumber['action'] == 'proceed_anyway') {
                        if($contactNumber['process'] == 'complete') {
                            // SEND PUSH NOTIFICATION HERE
                        } elseif($contactNumber['process'] == 'detail') {
                            // Save Temporary Client
                            $users[] = $this->_saveTemporaryClient($contactNumber);
                        }
                    } elseif($contactNumber['action'] == 'use_existing_profile') {
                        // No Action
                    }
                }
            }

            $response['status'] = 'Success';
            $response['users'] = $users;

            return Response::json($response);
        }
    }

    // Add Service to Client
    public function servicesIndex() {
        $response['status'] = 'Success';
        $response['services'] = Service::where('parent_id', '<>', 0)->get();

        return Response::json($response);
    }

    public function servicesGetDocs(Request $request) {
        $validator = Validator::make($request->all(), [
            'services_id' => 'required|array'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 400;

            return Response::json($response, $httpStatusCode);
        } else {
            $services = Service::whereIn('id', $request->services_id)
                ->select('detail', 'docs_needed', 'docs_optional')->get();

            $services_array = [];
            foreach($services as $service) {
                $docs_needed = explode(',', $service->docs_needed);
                $docs_optional = explode(',', $service->docs_optional);

                $temp = [];
                $temp['detail_array'] = $service->detail;
                $temp['docs_needed_array'] = ServiceDocuments::whereIn('id', $docs_needed)->get();
                $temp['docs_optional_array'] = ServiceDocuments::whereIn('id', $docs_optional)->get();
                $services_array[] = $temp;
            }

            $response['status'] = 'Success';
            $response['services_array'] = $services_array;

            return Response::json($response);
        }
    }

    public function servicesStore(Request $request) {
        $validator = Validator::make($request->all(), [
            'services' => 'required|array',
            'discount' => 'numeric',
            'reason' => 'required_unless:discount,0',
            'client_id' => 'required',
            'track' => 'required',
            //'docs' => 'required|array'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 200;

            return Response::json($response, $httpStatusCode);
        } else {
            $services = $request->get('services');
            $client_id = $request->get('client_id');
            $tracking = $request->get('track');
            $sid = $request->get('serviceid');
            $docs_needed = $request->get('req_docs');

            if(is_array($services)) :
                $ctr = 0;
                foreach($services as $service) :
                    $serv = Service::findOrFail($service);

                    $dt = Carbon::now();
                    $dt = $dt->toDateString();
                    $user = \Auth::user();
                    if($request->get('note')) {
                        $author = $request->get('note').' - '. $user->first_name.' <small>('.$dt.')</small>';
                    } else {
                        $author = '';
                    }
                    if($service == $sid[0]) {
                        $docs = $request->get('docs');
                    } else {
                        $docs = '';
                    }

                    $req_docs = [];
                    if($serv->docs_needed!= null || $serv->docs_needed!= ''){
                        $req_docs = explode(",", $serv->docs_needed); //33,16
                    }

                    $chosen_docs = [];
                    if($request->docs[$ctr]!= null || $request->docs[$ctr]!= ''){
                        $chosen_docs =explode(",", $request->docs[$ctr]);
                    }

                    $compare_docs = array_diff($req_docs, $chosen_docs);

                    if(!empty($compare_docs)){
                        $service_status = 'pending'; 
                    }else{
                        $service_status = 'on process';
                    }

                    $chosen = '';
                    foreach($chosen_docs as $ch){
                        $sd = ServiceDocuments::findOrFail($ch);
                        $chosen .= $sd->title.",";
                    }
                    $chosen = rtrim($chosen, ',');

                    $missing = '';
                    if(!empty($compare_docs)){
                        foreach($compare_docs as $cd){
                            $sd = ServiceDocuments::findOrFail($cd);
                            $missing .= $sd->title.",";
                        }
                    }
                    $missing = rtrim($missing, ',');

                    $cs = new ClientService;
                    $cs->client_id = $client_id;
                    $cs->service_id = $service;
                    $cs->detail = $serv->detail;
                    $cs->cost = $serv->cost;
                    $cs->charge = $serv->charge;
                    $cs->tip = $serv->tip;
                    $cs->status = $service_status;
                    $cs->service_date = Carbon::now()->format('m/d/Y');
                    $cs->remarks = $author;
                    $cs->group_id = 0;
                    $cs->tracking = $tracking;
                    $cs->temp_tracking = null;
                    $cs->active = 1;
                    $cs->extend = null;
                    $cs->rcv_docs = (!is_null($request->docs[$ctr])) ? $request->docs[$ctr] : '';
                    $cs->save();

                    $this->check_package_updates($tracking);

                    $translated = Service::where('id',$service)->first();
                    $cnserv =$serv->detail;
                    if($translated){
                        $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                    }

                    $log ='with <b>Service Charge</b> of &nbsp; Php'.($serv->cost + '0');
                    $log_cn = '服务费为 Php'.($serv->cost + '0');
                    $notes = 'Added a service <b>'.$serv->detail.'</b> to package number <b>'.$tracking.'</b></br>'.$log.'.';
                    $notes_cn = '添加了一个服务 '.$cnserv.' 到服务包'.$tracking.'</b></br>'.$log_cn.'。';
                    $total_balance = $this->clientBalance($client_id);
                    $log_data = array(
                        'client_id' => $client_id,
                        'service_id' => 0,
                        'group_id' => 0,
                        'type' => 'service',
                        'amount' => '-'.$serv->cost,
                        'balance' => $total_balance,
                        'detail'=> $notes,
                        'detail_cn'=> $notes_cn,
                    );
                    Common::saveTransactionLogs($log_data);

                    if($missing!=''){
                        $missing = " Missing documents (".$missing.").";
                    }

                    if($chosen!=''){
                        $chosen = "Received documents (".$chosen.").";
                    }
                    if($chosen!= '' || $missing !=''){   
                        $reportDetail = $chosen.$missing;
                        Report::create([
                            'service_id' => $cs->id,
                            'client_id' => $cs->client_id,
                            'group_id' => $cs->group_id,
                            'detail' => $reportDetail,
                            'user_id' => \Auth::user()->id,
                            'tracking' => $cs->tracking,
                            'log_date' => Carbon::now()->format('Y-m-d H:i:s a'),
                            'is_read' => 0
                        ]);
                    }

                    //send notif to client
                    $title = "New Service";
                    $body = 'New Service '.$serv->detail.' was added '.$chosen.$missing.', service status is '.$service_status.'.';
                    $main = $serv->detail;
                    $title_cn = "新的服务";
                    $body_cn = "新的服务 ".$cnserv." 已添加到您的";
                    $main_cn = $cnserv;
                    $parent = $tracking;
                    $parent_type = 1;
                    Common::savePushNotif($client_id, $title, $body,'Service',$cs->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

                    if($request->get('discount')) {
                        ClientTransactions::create([
                            'client_id' => $client_id,
                            'amount' => $request->get('discount'),
                            'type' => 'Discount',
                            'group_id' => 0,
                            'service_id' => $cs->id,
                            'reason' => $request->get('reason'),
                            'tracking' => $tracking,
                            'log_date' => Carbon::now()->format('m/d/Y h:i:s A')
                        ]);
                        $personal_balance = $this->clientBalance($client_id);
                        $notes = 'Discounted an amount of Php'.$request->get('discount').'&nbsp; with the reason of <i>"'.$request->get('reason').'"</i> on Package '.$tracking.'.';
                        $notes_cn = "已折扣额度 Php".$request->get('discount')."  在服务包 #".$tracking.'.';
                        $log_data2 = array(
                            'client_id' => $client_id,
                            'service_id' => 0,
                            'tracking' => $tracking,
                            'type' => 'discount',
                            'amount' => $request->get('discount'),
                            'detail'=> $notes,
                            'detail_cn'=> $notes_cn,
                            'balance'=> $personal_balance,
                        );
                        Common::saveTransactionLogs($log_data2);
                        $title = "Received Discount";
                        $body = 'Discounted an amount of Php'.$request->get('discount').'" on Service '.$cs->detail.'.';
                        $main = 'Package #'.$tracking;
                        $title_cn = "收到折扣";
                        $body_cn = "已折扣 Php".$request->get('discount');
                        $main_cn ="服务包编号 #".$tracking;
                        $parent = $cs->id;
                        $parent_type = 2;
                        Common::savePushNotif($client_id, $title, $body,'Discount',$tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
                    }

                    $ctr++;
                endforeach;
            endif;

            $response['status'] = 'Success';

            return Response::json($response);
        }
    }

    //add service to group
    public function groupServicesStore(Request $request) {
        $validator = Validator::make($request->all(), [
            'services' => 'required|array',
            'discount' => 'numeric|nullable',
            'reason' => 'required_unless:discount,0',
            'clients' => 'required|array',
            'packages' => 'required|array',
            'docs' => 'required|array',
            'group_id' => 'required'
        ]);
        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 200;

            return Response::json($response, $httpStatusCode);
        } else {
        $clientServicesIdArray = [];
        $id =$request->group_id;
        $g = Group::findOrFail($id);

        $trackingArray = [];
        $oldNewArray = [];
        $oldNewArrayChinese = [];
        for($i=0; $i<count($request->clients); $i++) {
            $clientId = $request->clients[$i];
            
            if($request->packages[$i] == 'New package') { // Generate new package
                $tracking = $this->generateServiceTracking();

                Package::create([
                    'client_id' => $clientId,
                    'group_id' => $id,
                    'log_date' => Carbon::now()->format('F j, Y, g:i a'),
                    'tracking' => $tracking,
                    'status' => 2
                ]);
                $oldnew = "new";
                $oldnew_cn = "新的";
            } else {
                $tracking = $request->packages[$i];
                $oldnew = "old";
                $oldnew_cn = "旧的";
            }

            $trackingArray[] = $tracking;
            $oldNewArray[] = $oldnew;
            $oldNewArrayChinese[] = $oldnew_cn;
        }

        $ctr = 0;
        $msg = [];
            for($j=0; $j<count($request->services); $j++) {
                $translated = Service::where('id',$request->services[$j])->first();
                $cnserv =$translated->detail;
                    if($translated){
                        $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
                    }

                $cls = '';
                for($i=0; $i<count($request->clients); $i++) {
                $clientId = $request->clients[$i];
                $client = User::findOrFail($clientId);
                $oldnew = '';
                $oldpacks = '';
                $newpacks = '';

                    $req_docs = [];
                    if($translated->docs_needed!= null || $translated->docs_needed!= ''){
                        $req_docs = explode(",", $translated->docs_needed); 
                    }
                    // $req_docs = explode(",", $serv->docs_needed);
                    $chosen_docs = [];
                    if($request->docs[$ctr]!= null || $request->docs[$ctr]!= ''){
                        $chosen_docs =explode(",", $request->docs[$ctr]);
                    }

                    $compare_docs = array_diff($req_docs, $chosen_docs);
                    
                    //status depending on submitted docs

                    if(!empty($compare_docs)){
                        $service_status = 'pending'; 
                    }else{
                        $service_status = 'on process';
                    }
                    
                    $chosen = ''; 
                    $chosen_cn = ''; 
                    foreach($chosen_docs as $ch){
                        $sd = ServiceDocuments::findOrFail($ch);
                        $chosen .= $sd->title.",";
                        $chosen_cn .= $sd->title_cn.",";
                    }
                    $chosen = rtrim($chosen, ',');
                    $chosen_cn = rtrim($chosen_cn, ',');

                    $missing = '';
                    $missing_cn = '';
                    if(!empty($compare_docs)){
                        foreach($compare_docs as $cd){
                            $sd = ServiceDocuments::findOrFail($cd);
                            $missing .= $sd->title.",";
                            $missing_cn .= $sd->title_cn.",";
                        }
                    }
                    $missing = rtrim($missing, ',');
                    $missing_cn = rtrim($missing_cn, ',');

                    $serviceId =  $request->services[$j];
                    $service = Service::findOrFail($serviceId);

                    $dt = Carbon::now();
                    $dt = $dt->toDateString();
                    $user = Auth::user();
                    $author = $request->note.' - '. $user->first_name.' <small>('.$dt.')</small>';
                    if($request->note==''){
                        $author = '';
                    }
                    $note = $service->remarks;
                    if($note!=''){
                        if($request->get('note')!=''){
                            $note = $note.'</br>'.$author;
                        }
                    }
                    else{
                        $note = $author;
                    }

                    
                    $scharge = 0;
                    $scost = 0;
                    $stip = 0;

                    //service cost depends on branch of group
                    if($g->branch_id > 1){
                        $bcost = ServiceBranchCost::where('branch_id',$g->branch_id)->where('service_id',$serviceId)->first();
                        $scost = ($scost < 1 ? $bcost->cost : $scost);
                        $stip = ($stip < 1 ? $bcost->tip : $stip);
                        $scharge = ($scharge < 1 ? $bcost->charge : $scharge);
                    }
                    else{
                        $scost = ($scost < 1 ? $service->cost : $scost);
                        $stip = ($stip < 1 ? $service->tip : $stip);
                        $scharge = ($scharge < 1 ? $service->charge : $scharge);
                    }

                    //service cost depends on cost level of group
                    if($g->cost_level > 0 ){
                        $newcost = ServiceProfileCost::where('profile_id',$g->cost_level)->where('service_id',$serviceId)->first();
                        if($newcost){
                            $scharge = ($newcost->cost<1 ? $scharge : $newcost->cost);
                        }
                    }

                    if($scost == 0 && $scharge == 0 && $stip == 0){
                        $scost = $service->cost;
                        $scharge = $service->charge;
                        $stip = $service->tip;
                    }

                    $clientService = ClientService::create([
                        'client_id' => $clientId,
                        'service_id' => $serviceId,
                        'detail' => $service->detail,
                        'cost' => $scost,
                        'charge' => $scharge,
                        'tip' => $stip,
                        'status' => $service_status,
                        'service_date' => Carbon::now()->format('m/d/Y'),
                        'remarks' => $author,
                        'group_id' => $id,
                        'tracking' => $trackingArray[$i],
                        'temp_tracking' => null,
                        'active' => 1,
                        'extend' => null,
                        'rcv_docs' => (!is_null($request->docs[$ctr])) ? $request->docs[$ctr] : ''
                    ]);

                    if($missing!=''){
                        $missing = " Missing documents (".$missing."), ";
                        $missing_cn = " 文件缺失 (".$missing_cn."), ";
                     }

                    if($chosen!=''){
                        $chosen = "Received documents (".$chosen."), ";
                        $chosen_cn = "已验收文件 (".$chosen_cn."), ";
                    }

                    if($chosen!= '' || $missing !=''){   
                        $reportDetail = $chosen.$missing;
                        Report::create([
                            'service_id' => $clientService->id,
                            'client_id' => $clientService->client_id,
                            'group_id' => $clientService->group_id,
                            'detail' => $reportDetail,
                            'user_id' => Auth::user()->id,
                            'tracking' => $clientService->tracking,
                            'log_date' => Carbon::now()->format('Y-m-d H:i:s a'),
                            'is_read' => 0
                        ]);
                    }

                    $clientServicesIdArray[] = $clientService;

                    $this->check_package_updates($trackingArray[$i]);

                    $log_data['client_id'] = $clientId;
                    $log_data['group_id'] = $id;
                    $log_data['service_id'] = $serviceId;
                    $log_data['tracking'] = $trackingArray[$i];
                    $log_data['user_id'] = Auth::user()->id;
                    $log_data['type'] = 'service';
                    $log_data['amount'] = $service->cost;
                    $log_data['balance'] = $this->getGroupBalance($id);
                    $log_data['detail'] = 'Added a service <strong>' . $service->detail . '</strong> to Package Number <strong>' . $trackingArray[$i] . '</strong> with Service Charge of Php<strong>' . $service->cost . '</strong> for Client <strong>[' . $clientId . '] ' . $client->full_name . '</strong>';
                    Common::saveTransactionLogs($log_data);

                    $dc = '';
                    $dcn = '';
                    $totalperservice = $service->cost; 
                    if($request->discount && count($request->services) == 1) {
                        ClientTransactions::create([
                            'client_id' => $clientId,
                            'type' => 'Discount',
                            'amount' => $request->discount,
                            'group_id' => $id,
                            'service_id' => $clientService->id,
                            'reason' => $request->reason,
                            'tracking' => $trackingArray[$i],
                            'log_date' => Carbon::now()->format('m/d/Y g:i')
                        ]);
                        $dc = " and receive a discount worth Php".$request->discount;
                        $dcn = " 予折扣Php".$request->discount;
                        $totalperservice -= $request->discount;
                    }


                    $title = "New Service";
                    $title_cn = "新的服务";
                    $parent = $trackingArray[$i];
                    $parent_type = 1;
                    $group = Group::findOrFail($id);

                    $msg[$ctr]['client'] = $client->first_name." ".$client->last_name;
                    $msg[$ctr]['msg'] = $chosen.$missing;
                    $msg[$ctr]['msg_cn'] = $chosen_cn.$missing_cn;
                    $msg[$ctr]['status'] = $service_status;

                    $checkIfVice = [];
                    $checkIfVice[] = $clientId;
                    $groupVice = GroupMember::where('group_id',$id)->where('leader',2)->whereIn('client_id',$checkIfVice)->count();

                    // if(($clientId != $group->leader && $groupVice < 1) || count($request->clients) == 1){
                    if(($clientId != $group->leader && $groupVice < 1)){
                        if($service_status == 'complete'){
                            $status = "<b><h4>".$service_status."</h4></b>";
                            $status_cn = "<b><h4> 已完成  </h4></b>";
                        }
                        if($service_status == 'on process'){
                            $status = "<b><h5>".$service_status."</h5></b>";
                            $status_cn = "<b><h5> 办理中 </h5></b>";
                        }
                        if($service_status == 'pending'){
                            $status = "<b><h6>".$service_status."</h6></b>";
                            $status_cn = "<b><h6> 待办 </h6></b>";
                        }
                        $body = 'New Service '.$translated->detail.' was added to your '.$oldNewArray[$i].' package, '.$chosen.$missing.' service status is '.$status;
                        $body_cn = '新的服务 '.$translated->detail.' 以添加到你的 '.$oldNewArrayChinese[$i].' 务包, '.$chosen_cn.$missing_cn.' 服务状态为 '.$status_cn;
                        $main = $body;
                        $main_cn = $body_cn;
                        Common::savePushNotif($clientId, $title, $body,'Service',$clientService->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
                        //$cls = $cls.$client->first_name." ".$client->last_name." (".$oldNewArray[$i]." package), ";
                        $cls = $cls.$client->first_name." ".$client->last_name.", ";
                    }
                    else{
                        $cls = $cls.$client->first_name." ".$client->last_name.", ";
                        // $cls = $cls."your ".$oldNewArray[$i]." package, ";
                    }    
                    $ctr++;            
                }
                $result = array();
                $result_cn = array();

                foreach ($msg as $m) {
                    $result[$m['msg']][] = $m;
                    $result_cn[$m['msg_cn']][] = $m;
                }
                //\Log::info($result);
                $keys = array_keys($result);
                $keys_cn = array_keys($result_cn);
                $perclient = [];
                $perclient_cn = [];
                $count = 0;
                foreach ($keys as $k) {
                    $perdocs = $result[$keys[$count]];
                    $names = '';
                    $status = '';
                    $status_cn = '';
                    foreach($perdocs as $p){
                        $names .=$p['client'].", ";
                        $status = $p['status'];
                    }
                    if($status == 'complete'){
                        $status = "<b><h4>".$status."</h4></b>";
                        $status_cn = "<b><h4> 已完成  </h4></b>";
                    }
                    if($status == 'on process'){
                        $status = "<b><h5>".$status."</h5></b>";
                        $status_cn = "<b><h5> 办理中 </h5></b>";
                    }
                    if($status == 'pending'){
                        $status = "<b><h6>".$status."</h6></b>";
                        $status_cn = "<b><h6> 待办 </h6></b>";
                    }
                    $perclient[] = substr($keys[$count], 0, -2)." for <b>".$names."</b> service status is now ".$status.".";
                    $perclient_cn[] = substr($keys_cn[$count], 0, -2)." 为 <b>".$names."</b> 服务状态现在为 ".$status_cn.".";
                    $count++;
                }

                //Combined Notif               
                if($clientId == $group->leader || count($request->clients) >= 1){                
                    // $cls = " was added to ".$cls;
                    $clientNames = substr($cls, 0, -2);
                    $nms = explode(", ", $clientNames);
                    sort($nms);
                    // $nms = implode (", ", $nms);
                    $clientNames = '';
                    $crt = 1;
                    foreach($nms as $n){
                        $clientNames .=$crt.".".$n." ";
                        $crt++;
                    }
                    $nms = $clientNames;
                    $cls = "for ".substr($cls, 0, -2);
                    if($dc!= ''){
                        $dc .= " each";
                    }
                    $totalperservice *=count($request->clients);
                    // $main = 'New Service '.$translated->detail.' was added worth Php'.$service->cost." each".$dc.' with the total price of Php'.$totalperservice." as part of batch ".Carbon::now()->format('M d,Y')."."; 
                    $main = 'New Service '.$translated->detail.' was added worth Php'.$service->cost." each".$dc." as part of batch ".Carbon::now()->format('M d,Y')."."; 
                    $main_cn = '新的服务 '.$cnserv.' 添加了总价Php'.$service->cost.$dcn.' 为'.Carbon::now()->format('M d,Y').'总体服务的一部分';
                    foreach($perclient as $per){
                        $main = $main."<br><br>-".$per;
                    }
                    foreach($perclient_cn as $per_cn){
                        $main_cn = $main_cn."<br><br>-".$per_cn;
                    }
                    //$main = $translated->detail;
                    $body = $main;
                    $body_cn = $main_cn;
                    $title = "New Service";
                    $title_cn = "新的服务";
                   
                    $parent_type = 1;
                    $sDate = Carbon::now()->format('m/d/Y');
                    $parent = $nms;

                    Common::savePushNotif($g->leader, $title, $body,'GService',$sDate."--".$g->tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$nms);

                    //send push notif to vice leaders
                    // if($leader != null){
                        $vices = GroupMember::where('group_id',$g->id)->where('leader',2)->get();
                        foreach($vices as $v){
                            $par = $nms;
                            $parent = $v->client_id."-".$par;
                            Common::savePushNotif($v->client_id, $title, $body,'GService',$sDate."--".$g->tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$nms);
                        }
                    // }
                }

            }
        }

        return json_encode([
            'success' => true,
            'message' => 'Service/s successfully added to group',
            'clientsId' => $request->clients,
            'clientServicesId' => $clientServicesIdArray,
            'trackings' => $trackingArray
        ]);
    }
    //end add service to group

    //edit service for both individual and group
    public function clientEditService(Request $request){
        $validator = Validator::make($request->all(), [
            'discount' => 'numeric|nullable',
            'cost' => 'numeric|nullable',
            'tip' => 'numeric|nullable',
            'reason' => 'required_unless:discount,0',
            'rcv_docs' => 'required',
            'id' => 'required' // client service id
        ]);
        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 200;

            return Response::json($response, $httpStatusCode);
        } 

        $id = $request->id;

        $service = ClientService::findOrFail($id);
        $activer1 = (($request->get('active') == 1)? 'Active' : 'Inactive');
        $activer2 = (($service->active == 1)? 'Active' : 'Inactive');

        $log = ''; $translog = '';
        $newVal = 0; $oldVal = 0;
        $colClients = [];
        $cls = '';

        $translated = Service::where('id',$service->service_id)->first();
        $cnserv =$service->detail;
        if($translated){
            $cnserv = ($translated->detail_cn!='' ? $translated->detail_cn : $translated->detail);
        }

        //SERVICE DOCUMENTS
        $allreq_docs = [];
        if($translated->docs_needed!= null){
            $allreq_docs = explode(",", $translated->docs_needed);
        }

        $req_docs = [];
        if($service->rcv_docs!= null){
            $req_docs = explode(",", $service->rcv_docs); 
        }

        $chosen_docs = [];
        if($request->get('rcv_docs')!= null){
            $chosen_docs =explode(",", $request->rcv_docs); 
        }

        $compare_docs = array_diff($allreq_docs, $chosen_docs); 

        $same_docs = array_intersect($req_docs, $chosen_docs); 

        $remove = array_diff($req_docs, $same_docs); 

        $additional = array_diff($chosen_docs, $same_docs);
        
        //status depending on submitted docs
        if(!empty($req_docs)){
            if($request->get('status') != 'complete'){
                if(!empty($compare_docs)){
                    $service_status = 'pending'; 
                }else{
                    $service_status = 'on process';
                }
            }
            else{
                $service_status = 'complete';
            }
        }
        else{
            $service_status = $request->get('status');
        }
        
        $add = '';
        $add_cn = '';
        foreach($additional as $ch){
            $sd = ServiceDocuments::findOrFail($ch);
            $add .= $sd->title.",";
            $add_cn .= $sd->title_cn.",";
        }
        $add = rtrim($add, ',');
        $add_cn = rtrim($add_cn, ',');

        $rem = '';
        $rem_cn = '';
        if(!empty($remove)){
            foreach($remove as $cd){
                $sd = ServiceDocuments::findOrFail($cd);
                $rem .= $sd->title.",";
                $rem_cn .= $sd->title_cn.",";
            }
        }
        $rem = rtrim($rem, ',');
        $rem_cn = rtrim($rem_cn, ',');
        //END SERVICE DOCUMENTS

        $updCollect = ''; $updChinese = ''; $forleadCollect =''; $forleadChinese ='';

        //if theres changes in active/inactive
        if($service->active != $request->active){
             $activer1 = (($request->active == 1)? 'Active' : 'Inactive');
             $active_cn = (($request->active == 1)? '已激活' : '未激活');
             $updCollect .= 'Service is now '.$activer1.'. ';
             $updChinese .= '服务现在 '.$active_cn.'.';
            //for action logs
             $log .= $updCollect;
             $newVal +=0;
             $oldVal +=($service->cost + $service->charge + $service->tip);
        }

        //if theres changes in service status
        if($service->status != $service_status){
            $status = $service_status;
            $status_cn = $service_status;
            if($status == 'complete'){
                $status = "<b><h4>".$status."</h4></b>";
                $status_cn = "<b><h4> 已完成  </h4></b>";
            }
            if($status == 'on process'){
                $status = "<b><h5>".$status."</h5></b>";
                $status_cn = "<b><h5> 办理中 </h5></b>";
            }
            if($status == 'pending'){
                $status = "<b><h6>".$status."</h6></b>";
                $status_cn = "<b><h6> 待办 </h6></b>";
            }
               $updCollect .= 'Service status is now '.$status;
               $updChinese .= '服务状态为 '.$status_cn;
               $log .= 'Service status is now '.$service_status;
        }

        //if theres changes in service cost
        if($service->cost != $request->cost){
            $forleadCollect .= "Cost update from Php".$service->cost." to Php".$request->cost.".";
            $forleadChinese .= "花费从 Php".$service->cost." 到 Php".$request->cost.".";
            //for transaction logs
            $translog .= '&nbsp; <b>Service Cost</b> from Php'.$service->cost.' to Php'.$request->get('cost');
            $newVal +=$request->cost;
            $oldVal +=$service->cost;
        }

        //if theres changes in service tip
        if($service->tip != $request->tip){
            $forleadCollect .= "Additional payment(CwoOR) update from Php".$service->tip." to Php".$request->tip.".";
            $forleadChinese .= "附加款从 Php".$service->tip." 到 Php".$request->tip.".";
            //for transaction logs
            $translog .= '<b>Additional Payment(CwoOR)</b> from '.$service->tip.' to '.$request->tip;
            $newVal +=$request->tip;
            $oldVal +=$service->tip;
        }

        //if theres changes in service discount
        if($request->discount > 0) {
            $dc = ClientTransactions::where("service_id",$id)->where('type','Discount')->withTrashed()->first();
            if($dc){
                if($dc->amount != $request->discount){
                    $forleadCollect .= "Receive Discount of Php".$request->discount.".";
                    $forleadChinese .= "收到折扣 Php".$request->discount.".";
                    $translog .= 'Update Discount from '.$dc->amount.' to '.$request->discount;

                    $newVal +=$request->discount;
                    $oldVal +=$dc->amount;

                    $dc->amount =  $request->get('discount');
                    $dc->reason =  $request->get('reason');
                    $dc->deleted_at = null;
                    $dc->save();
                }
            }
            else{
                $forleadCollect .= "Receive Discount of Php".$request->discount.".";
                $forleadChinese .= "收到折扣 Php".$request->discount.".";

                $newVal += ($request->discount)*-1;
                $oldVal +=0;

                ClientTransactions::create([
                    'client_id' => $service->client_id,
                    'amount' => $request->get('discount'),
                    'type' => 'Discount',
                    'group_id' => $service->group_id,
                    'service_id' => $id,
                    'reason' => $request->get('reason'),
                    'tracking' => $service->tracking,
                    'log_date' => Carbon::now()->format('m/d/Y h:i:s A')
                ]);
            }
        } else {
            $discountExist = ClientTransactions::where('service_id', $request->id)->first();
            if($discountExist){
                $discountExist->delete();
            }
        }


            $addition = '';
            $addition_cn = '';
            if($add != ''){
                $addition = 'Received Additional Document/s ('.$add.'), ';
                $addition_cn = '已验收文件 ('.$add_cn.'), ';
            }

            $rmv = '';
            $rmv_cn = '';
            if($rem != ''){
                $rmv = 'Remove Invalid Document/s ('.$rem.'), ';
                $rmv_cn = '去掉文件 ('.$rem_cn.'), ';
            }

            if($add!= '' || $rem !=''){        
               Report::create([
                    'service_id' => $service->id,
                    'client_id' => $service->client_id,
                    'group_id' => $service->group_id,
                    'detail' => $addition.$rmv.'.',
                    'user_id' => Auth::user()->id,
                    'tracking' => $service->tracking,
                    'log_date' => Carbon::now()->format('Y-m-d H:i:s a'),
                    'is_read' => 0
                ]);
            }
        $getGroup = false;
        if($service->group_id > 0){
            $getGroup = Group::findOrFail($service->group_id);
        }
        $client = User::findOrFail($service->client_id);

        if($updCollect!=''){
            $solo = true;
            $checkIfVice = [];
            $checkIfVice[] = $service->client_id;
            $groupVice = GroupMember::where('group_id',$service->group_id)->where('leader',2)->whereIn('client_id',$checkIfVice)->count();
            if($getGroup){
                if($service->client_id != $getGroup->leader && $groupVice < 1){
                    $solo = true;
                }
                else{
                    $solo = false;
                }
            }

                
                if($solo){
                    $title = "Update for Service ".$service->detail;
                    $title_cn = "服务更新 ".$cnserv;
                    $body = "Update for Service ".$service->detail.", ".$updCollect;
                    $body_cn = "服务更新 ".$cnserv.", ".$updChinese;
                    $main = $body;
                    $main_cn = $body_cn;
                    $parent = $service->id;
                    $parent_type = 2;

                    if($request->get('status') =='complete' && $service->group_id == 0){
                        $checkDisc = ClientTransactions::where("service_id",$service->id)->where('type','Discount')->first();
                        $gTotal = $service->cost + $service->charge + $service->tip;
                        if($checkDisc){
                        $gTotal -= $checkDisc->amount;
                        }
                        $body = $body.", with total cost of Php".$gTotal;
                        $body_cn = $body_cn.", 以及总服务费 Php".$gTotal;
                    }
                    Common::savePushNotif($service->client_id, $title, $body,'Service',$service->id,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
                    //$cls = $cls.$client->first_name." ".$client->last_name.", ";
                }

        }
       //return $updCollect." --- ".$forleadCollect;
                if($getGroup){
                    $cls = $client->first_name." ".$client->last_name.", ";
                    $cls = substr($cls, 0, -2);
                    $nms = explode(", ", $cls);
                    sort($nms);
                    $clientNames = '';
                    $crt = 1;
                    foreach($nms as $n){
                        $clientNames .=$crt.".".$n." ";
                        $crt++;
                    }
                    if($updCollect!='' || $forleadCollect!=''){
                        $title = "Update for Service ".$service->detail;
                        $title_cn = "服务更新 ".$cnserv;
                        $body = "Update for ".$clientNames." Service ".$service->detail;
                        $body_cn = "更新了 ".$clientNames." 服务 ".$cnserv;
                        $main = "Update for ".$clientNames." Service ".$service->detail.", ".$updCollect.$forleadCollect;
                        $main_cn = "更新了 ".$clientNames." 服务 ".$cnserv.", ".$updChinese.$forleadChinese;
                        $parent_type = 1;
                        $sDate = $service->service_date;
                        $parent = $clientNames;

                        Common::savePushNotif($getGroup->leader, $title, $body,'GService',$sDate."--".$getGroup->tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$clientNames);

                        $vices = GroupMember::where('group_id',$getGroup->id)->where('leader',2)->get();
                        foreach($vices as $v){
                            $par = $clientNames;
                            $parent = $v->client_id."-".$par;
                            Common::savePushNotif($v->client_id, $title, $body,'GService',$sDate."--".$getGroup->tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type,$clientNames);
                        }
                    }
                }

        //for new notes 
        if(stripslashes($service->remarks) != $request->get('note') && $request->get('note') != '') :
            if ($log == ''):
                $log = $log.' Added new comment <i>"'.$request->get('note').'"</i>';
            else:
                $log = ','.$log.' Added new comment <i>"'.$request->get('note').'"</i>';
            endif;
        endif;

        // for remarks
        $dt = Carbon::now();
        $dt = $dt->toDateString();
        $user = Auth::user();
        $author = $request->get('note').' - '. $user->first_name.' <small>('.$dt.')</small>';
        if($request->get('note')==''){
            $author = '';
        }
        $note = $service->remarks;
        if($note!=''){
            if($request->get('note')!=''){
                $note = $note.'</br>'.$author;
            }
        }
        else{
            $note = $author;
        }

        //for service extension
        $extend = $request->get('extend');
        $extend_dt = null;
        if($extend!=''){
            $extend_dt = date("Y-m-d", strtotime($extend));
        }

        //save the updates
        $data = array(
            'cost'=> $request->get('cost'),
            'tip' => $request->get('tip'),
            'remarks' => $note,
            'status' => $service_status,
            'active' => $request->get('active'),
            'extend' => $extend_dt,
            'rcv_docs' => $request->get('rcv_docs')
        );
        $update_service = ClientService::where('id', $id)->update($data);

        if ($update_service) :
            $this->check_package_updates($service->tracking);
            if($activer1 == "Inactive"){
                $d = ClientTransactions::where('type', 'Discount')->where("service_id", $id)->first();
                if($d){
                    $d->delete();
                }
            }
            else{
                $d = ClientTransactions::withTrashed()->where('type', 'Discount')->where("service_id", $id)->first();
                if($d){
                    $d->restore();
                }
            }

            $group = Package::where('tracking', $service->tracking)->first();
            if($log != '') :
                if(substr($service->tracking,0,1)!="G"){
                    $new_log = 'Updated Service &nbsp;'.$service->detail.'&nbsp;'.$log.'&nbsp; on Package '.$service->tracking.'.';
                    Common::saveActionLogs($service->id,$service->client_id,$new_log);
                }
                else{
                    $new_log = 'Log: Updated Client Service '.$id.' - '.$service->detail.'</br>'.$log;
                    Common::saveActionLogs($service->id,$service->client_id,$new_log,$group->group_id);
                }
            endif;

            //additional codes
            $client_id = $service->client_id;
            $personal_payment = $this->clientPayment($client_id);
            $personal_cost = $this->clientServiceCost($client_id);
            $personal_discount = $this->clientDiscount($client_id);
            $personal_refund = $this->clientRefund($client_id);
            $personal_deposit = $this->clientDeposit($client_id);
            $personal_balance = $this->clientBalance($client_id);

            $notes = 'Updated Service &nbsp;'.$service->detail.'&nbsp;. '.$translog.'&nbsp; on Package '.$service->tracking.'.';
            $notes_cn = '服务更新 '.$service->detail.' 到服务包 '.$service->tracking.'.';
            $total_balance = $this->getGroupBalance($group->group_id);

                if($translog != ''){
                    $newVal = $oldVal - $newVal;
                    $log_data = array(
                        'client_id' => $client_id,
                        'service_id' => 0,
                        'tracking' => $service->tracking,
                        'user_id' => "", 
                        'type' => 'service',
                        'amount' => $newVal,
                        'detail'=> $notes,
                        'detail_cn'=> $notes_cn,
                    );
                    if(substr($service->tracking,0,1)=="G"){
                        $log_data['group_id'] = $group->group_id;
                        $log_data['balance'] = $total_balance;
                    }
                    else{
                        $log_data['balance'] = $personal_balance;
                    }

                    Common::saveTransactionLogs($log_data);
                }
            $result = array('success' => true);
        else :
            $result = array('success' => true, 'data' => ClientService::findOrFail($id));
        endif;

        return json_encode($result);
    }

    //genereate group member tracking
    private function generateServiceTracking() {
        Repack:
        $packId = Common::generateGroupTrackingNumber();
        $checkPackage = $this->checkPackage($packId);
        if($checkPackage > 0) :
            goto Repack;
        endif;
        return $packId;
    }

    //function to check if package already exists or not
    private function checkPackage($packId){
        return Package::where('tracking', $packId)->count();
    }

    //ADD FUNDS to client or groups
    public function addFunds(Request $request) {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required',
            'amount' => 'numeric',
            'type' => 'required',
            'selected_client' => 'required_if:type,==,transfer',
            'storage' => 'required',
            'reason' => 'nullable|required_if:type,==,refund|required_if:type,==,discount',
            'group_id' => 'required', //if group package
            'tracking' => 'required_if:group_id,==,0',
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 400;

            return Response::json($response, $httpStatusCode);
        } else {
            $client_id = $request->get('client_id');
            $amount = $request->get('amount');
            $type = $request->get('type');
            $reason = $request->get('reason');
            $group_id = $request->get('group_id');
            $tracking = $request->get('tracking');
            // $authorizer = $request->get('authorizer');
            // $auth_name = $request->get('auth_name');
            date_default_timezone_set("Asia/Manila");
            $date = date('m/d/Y h:i:s A');
            $notes = '';
            $notes_cn = '';
            $result = '';
            if ($type == "deposit") :
                $depo = new ClientTransactions;
                $depo->type = 'Deposit';
                $depo->client_id = $client_id;
                $depo->group_id = $group_id;
                $depo->amount = $amount;
                $depo->log_date = $date;
                $depo->tracking = $tracking;
                $depo->save();

                $notes = 'Deposited an amount of Php'.$amount.' on Package '.$tracking.'.';
                $notes_cn = '预存了款项 Php'.$amount.' 在服务包 '.$tracking.'.';
                $result = array('status' => 'success', 'log' => 'Deposit added successfully.');

                // For financing
                $finance = new Finance;
                $finance->cat_type = "process";
                $finance->cat_storage = $request->storage;
                if($request->storage == 'cash') {
                    $finance->cash_client_depo_payment = $amount;
                } elseif($request->storage == 'bank') {
                    $finance->bank_client_depo_payment = $amount;
                }
                $finance->trans_desc = Auth::user()->first_name.' received deposit from client #'.$client_id.' on Package #'.$tracking;
                $finance->save();

            elseif($type == "payment") :
                $payment = new ClientTransactions;
                $payment->type = 'Payment';
                $payment->client_id = $client_id;
                $payment->group_id = $group_id;
                $payment->amount = $amount;
                $payment->log_date = $date;
                $payment->tracking = $tracking;
                $payment->save();

                $notes = 'Payed an amount of Php'.$amount.' on Package '.$tracking.'.';
                $notes_cn = '已支付 Php'.$amount.' 到服务包 '.$tracking.'.';
                $result = array('status' => 'success', 'log' => 'Payment added successfully.');

                // For financing
                $finance = new Finance;
                $finance->cat_type = "process";
                $finance->cat_storage = $request->storage;
                if($request->storage == 'cash') {
                    $finance->cash_client_depo_payment = $amount;
                } elseif($request->storage == 'bank') {
                    $finance->bank_client_depo_payment = $amount;
                }
                $finance->trans_desc = Auth::user()->first_name.' received payment from client #'.$client_id.' on Package #'.$tracking;
                $finance->save();

            elseif($type == "refund") :
                $refund = new ClientTransactions;
                $refund->type = 'Refund';
                $refund->client_id = $client_id;
                $refund->group_id = $group_id;
                $refund->amount = $amount;
                $refund->log_date = $date;
                $refund->reason = $reason;
                $refund->tracking = $tracking;
                $refund->save();

                $notes = 'Refunded an amount of Php'.$amount.'&nbsp; with the reason of <i>"'.$reason.'"</i> on Package '.$tracking.'.';
                $notes_cn = '退款了 Php'.$amount.' 因为"'.$reason.'"于服务包 '.$tracking.'.';
                $result = array('status' => 'success', 'log' => 'Refund added successfully.');

                // For financing
                $finance = new Finance;
                $finance->cat_type = "process";
                $finance->cat_storage = $request->storage;
                $finance->cash_client_refund = $amount;
                $finance->trans_desc = Auth::user()->first_name.' refund to client #'.$client_id.' on Package #'.$tracking.' for the reason of '.$reason;
                $finance->save();

            elseif($type == "discount") :
                $discount = new ClientTransactions;
                $discount->type = 'Discount';
                $discount->client_id = $client_id;
                $discount->group_id = $group_id;
                $discount->amount = $amount;
                $discount->log_date = $date;
                $discount->reason = $reason;
                $discount->tracking = $tracking;
                $discount->save();

                // $discount->authorizer = $authorizer;
                $discount->save();
                $notes = 'Discounted an amount of Php'.$amount.'&nbsp; with the reason of <i>"'.$reason.'"</i> on Package '.$tracking.'.';
                $notes_cn = '给于折扣 Php'.$amount.' 因为"'.$reason.'"于服务包 '.$tracking.'.';

                $title = "Received Discount";
                $title_cn = "收到折扣";
                $body = 'Discounted an amount of Php'.$amount.'.';
                $body_cn = "已折扣 Php".$amount;
                $main = $body;
                $main_cn = $body_cn;
                $parent = $tracking;
                $parent_type = 1;
                Common::savePushNotif($client_id, $title, $body,'Discount',$tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);
                $result = array('status' => 'success', 'log' => 'Discount added successfully.');

            elseif($type == "transfer") :
                $total_balance = $this->clientBalance($client_id);
                $trans = new TransferBalance;
                $trans->transfer_from = ($total_balance < 0 ? $request->selected_client : $client_id);
                $trans->transfer_to = ($total_balance >= 0 ? $request->selected_client : $client_id);
                $trans->amount = $amount;
                $trans->reason = $reason;
                $trans->log_date = $date;
                $trans->save();

                $refund = new ClientTransactions;
                $refund->type = 'Refund';
                $refund->client_id = $trans->transfer_from;
                $refund->group_id = $group_id;
                $refund->amount = $amount;
                $refund->log_date = $date;
                $refund->reason = $reason;
                $refund->tracking = $tracking;
                $refund->save();

                $depo = new ClientTransactions;
                $depo->type = 'Deposit';
                $depo->client_id = $trans->transfer_to;
                $depo->group_id = $group_id;
                $depo->amount = $amount;
                $depo->log_date = $date;
                $depo->tracking = $tracking;
                $depo->save();

                $result = array('status' => 'success', 'log' => 'Transfer Balance successfully.');

            else :
                $result = array('status' => 'failed', 'log' => '.');
            endif;

            if($result['status'] == 'success') {
                if($type == "transfer") { // BALANCE TRANSFER

                    $notes = 'Deposited an amount of Php'.$amount.' from client#'.$trans->transfer_from.'.';
                    $notes_cn = $notes;
                    $total_balance = $this->clientBalance($trans->transfer_to);
                    $log_data = array(
                        'client_id' => $trans->transfer_to,
                        'service_id' => 0,
                        'group_id' => $group_id,
                        'type' => 'deposit',
                        'amount' => $amount,
                        'balance' => $total_balance,
                        'detail'=> $notes,
                        'detail_cn'=> $notes_cn,
                    );
                    Common::saveTransactionLogs($log_data);

                    $notes = 'Refunded an amount of Php'.$amount.', transferred to client#'.$trans->transfer_to;
                    $notes_cn = '退款 Php'.$amount.', 转移到了客户 #'.$trans->transfer_to;
                    $total_balance = $this->clientBalance($trans->transfer_from);
                    $log_data = array(
                        'client_id' => $trans->transfer_from,
                        'service_id' => 0,
                        'group_id' => $group_id,
                        'type' => 'refund',
                        'amount' => '-'.$amount,
                        'balance' => $total_balance,
                        'detail'=> $notes,
                        'detail_cn'=> $notes_cn,
                    );
                    Common::saveTransactionLogs($log_data);

                } else {// DEPOSIT, PAYMENT, REFUND, DISCOUNT

                    $total_balance = ($group_id>0? $this->getGroupBalance($group_id) : $this->clientBalance($client_id));
                    $log_data = array(
                        'client_id' => $client_id,
                        'service_id' => 0,
                        'group_id' => $group_id,
                        'type' => $type,
                        'amount' => $amount,
                        'balance' => $total_balance,
                        'detail'=> $notes,
                        'detail_cn'=> $notes_cn,
                    );

                    Common::saveTransactionLogs($log_data);

                }
            }

            return json_encode($result);
        }
    }

    // old
    public function clientDocuments($id){
        $docs = ClientDocuments::where('user_id',$id)
                     ->orderBy('id','desc')
                     ->get();
        $cdoc = [];
        $cdoc['client_id'] = $id;
        $ctr = 0;
        foreach($docs as $n){
            $cdoc['scanned'][$ctr]['type'] = $n->type; 
            $cdoc['scanned'][$ctr]['issue_at'] = $n->issue_at; 
            $cdoc['scanned'][$ctr]['expires_at'] = $n->expires_at; 
            $cdoc['scanned'][$ctr]['file_path'] = '/storage/'.$n->file_path; 
            $ctr++;
        }
        return Response::json($cdoc);
    }

    public function clientDocumentsV2($id){
        $docs = ClientDocuments::where('user_id',$id)
                     ->get();
        $cdoc = [];
        $cdoc['client_id'] = $id;
        $cdoc['scanned'] = [];
        $ctr = 0;
        $multi = 0;
        $pagectr = 0;
        $cur_type = 0;
        $cur_issue = 0;
        $cur_exp = 0;
        foreach($docs as $n){
            if($n->multiple == 0){
                $multi = 0;
                $pagectr = 0;
                $cur_type = 0;
                $cur_issue = 0;
                $cur_exp = 0;
                $ctr++;
            }
            else{
                if($cur_type != $n->type || $cur_issue != $n->issue_at || $cur_exp != $n->expires_at){
                    $multi = 0;
                    $pagectr = 0;
                    $cur_type = 0;
                    $cur_issue = 0;
                    $cur_exp = 0;
                    $ctr++;
                }
            }

            if($multi == 0){            
                $cdoc['scanned'][$ctr]['id'] = $n->id; 
                $cdoc['scanned'][$ctr]['type'] = $n->type; 
                $cdoc['scanned'][$ctr]['issue_at'] = $n->issue_at; 
                $cdoc['scanned'][$ctr]['expires_at'] = $n->expires_at; 
                $cdoc['scanned'][$ctr]['file_path'] = '/storage/'.$n->file_path; 
                $cdoc['scanned'][$ctr]['pages'] = null; 
                if($n->multiple == 1 ){
                    $multi = 1;
                    $cur_type = $n->type;
                    $cur_issue = $n->issue_at;
                    $cur_exp = $n->expires_at;
                }else{
                    $ctr++;
                }
            }
            else{
                // $cdoc['scanned'][$ctr]['pages'][$pagectr] = $n; 
                $cdoc['scanned'][$ctr]['pages'][$pagectr]['id'] = $n->id; 
                $cdoc['scanned'][$ctr]['pages'][$pagectr]['type'] = $n->type; 
                $cdoc['scanned'][$ctr]['pages'][$pagectr]['issue_at'] = $n->issue_at; 
                $cdoc['scanned'][$ctr]['pages'][$pagectr]['expires_at'] = $n->expires_at; 
                $cdoc['scanned'][$ctr]['pages'][$pagectr]['file_path'] = '/storage/'.$n->file_path; 
                $cur_type = $n->type;
                $cur_issue = $n->issue_at;
                $cur_exp = $n->expires_at;
                $pagectr++;
            }
        }

        if(isset($cdoc['scanned'])){
            $cdoc['scanned'] = array_reverse($cdoc['scanned']);
        }

        return Response::json($cdoc);
    }

    public function clientAllFunds($id){
        $deposit = ClientTransactions::where('type', 'Deposit')
            ->where('client_id', $id)
            ->where('group_id', 0)
            ->orderBy('id', 'desc')
            ->get();

        $payments = ClientTransactions::where('type', 'Payment')
            ->where('client_id', $id)
            ->where('group_id', 0)
            ->orderBy('id', 'desc')
            ->get();

        $discounts = ClientTransactions::where('type', 'Discount')
            ->where('client_id', $id)
            ->where('group_id', 0)
            ->orderBy('id', 'desc')
            ->get();

        $refunds = ClientTransactions::where('type', 'Refund')
            ->where('client_id', $id)
            ->where('group_id', 0)
            ->orderBy('id', 'desc')
            ->get();

        $funds = [];
        $funds['deposis'] = $deposit;
        $funds['payments'] = $payments;
        $funds['discounts'] = $discounts;
        $funds['refunds'] = $refunds;
        return Response::json($funds);
    }

    // Add Package
    public function packagesStore(Request $request) {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 422;

            return Response::json($response, $httpStatusCode);
        } else {
            Repack:
            $tracking = Common::generateIndividualTrackingNumber(7);
            $check_package = Package::where('tracking', $tracking)->count();
            if($check_package > 0) :
                goto Repack;
            endif;

            date_default_timezone_set("Asia/Manila");
            $dates = date("F j, Y, g:i a");

            $new_package_data = array(
                'client_id' => $request->client_id,
                'tracking' => $tracking,
                'log_date' => $dates
            );

            $new_package = Package::insert($new_package_data);

            if($new_package) :
                $new_log = 'Created new package';
                Common::saveActionLogs(0, $request->client_id, $new_log);
                $title = "New Package Created";
                $body = 'New package created';
                $main = 'Package #'.$tracking;
                $title_cn = "新的服务包创建";
                $body_cn = "新的服务包创建";
                $main_cn = "服务包编号 #".$tracking;
                $parent  = 0;
                $parent_type  = 0;

                Common::savePushNotif($request->client_id, $title, $body,'Package',$tracking,$main,$main_cn,$title_cn,$body_cn,$parent,$parent_type);

                $response['status'] = 'Success';
                $response['data'] = $new_package_data;

                return Response::json($response);
            else :
                $response['status'] = 'Failed';
                $httpStatusCode = 400;

                return Response::json($response, $httpStatusCode);
            endif;
        }
    }


    // Add Package Batch
    public function packagesBatchStore(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required|array',
            'data.*.client_id' => 'required',
            'data.*.group_id' => 'required',
            'data.*.local_id' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 422;

            return Response::json($response, $httpStatusCode);
        } else {
            $data = [];

            foreach($request->data as $d) {
                $clientId = $d['client_id'];
                $groupId = $d['group_id'];
                $localId = $d['local_id'];

                do {
                    if($groupId == 0) { // Individual
                        $tracking = Common::generateIndividualTrackingNumber(7);
                    } else { // Group
                        $tracking = Common::generateGroupTrackingNumber();
                    }

                    $count = Package::where('tracking', $tracking)->count();
                } while($count > 0);

                date_default_timezone_set("Asia/Manila");
                $dates = date("F j, Y, g:i a");

                $new_package_data = array(
                    'client_id' => $clientId,
                    'group_id' => $groupId,
                    'tracking' => $tracking,
                    'log_date' => $dates
                );

                // Save to packages table
                Package::insert($new_package_data);

                // Action logs
                $new_log = 'Created new package';
                Common::saveActionLogs(0, $clientId, $new_log, $groupId);

                // Return
                $new_package_data['local_id'] = $localId;
                $data[] = $new_package_data;
            }

            $response['status'] = 'Success';
            $response['data'] = $data;

            return Response::json($response);
        }
    }

    // Delete Package
    public function deletePackage(Request $request) {
        $validator = Validator::make($request->all(), [
            'tracking' => 'required',
            'reason' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 422;

            return Response::json($response, $httpStatusCode);
        } else {
            $tracking = $request->tracking;

            $check_registered_service = ClientService::where('tracking',  $tracking)->count();
            $package = Package::where('tracking',  $tracking)->first();

            if($check_registered_service == 0) {
                $reason = $request->reason;

                Package::where('tracking', $tracking)->delete();

                $new_log = 'Deleted the package number <b>'.$tracking.'</b> with the reason of &nbsp; <i>"'.$reason.'"</i>.';
                Common::saveActionLogs(0, $package->client_id, $new_log);

                $title = "Package Deleted";
                $title_cn = "服务包已删除";
                $body = 'One of your package with reference number '.$tracking.' was deleted';
                $body_cn = "你的服务包及编号为 ".$tracking." 被删除。";
                $main = $body;
                $main_cn = $body_cn;
                $parent = $tracking;
                $parent_type = 1;
                Common::savePushNotif(
                    $package->client_id, 
                    $title, 
                    $body,
                    'Package',
                    $tracking,
                    $main,
                    $main_cn,
                    $title_cn,
                    $body_cn,
                    $parent,
                    $parent_type
                );

                $response['status'] = 'Success';

                return Response::json($response);
            } else {
                $response['status'] = 'Failed. Unable to delete because it has services.';
                $httpStatusCode = 400;

                return Response::json($response, $httpStatusCode);
            }
        }
    }

    // Write Report
    public function indexActions() {
        $response['status'] = 'Success';
        $response['actions'] = Action::orderBy('name')->get();

        return Response::json($response);
    }

    public function getCategoriesByAction($action_id = 0) {
        $response['status'] = 'Success';
        $response['categories'] = Category::orderBy('name')->whereHas('actions', function($query) use($action_id) {
            $query->where('action_id', $action_id);
        })->get();

        return Response::json($response);
    }

    public function indexCompanyCouriers() {
        $response['status'] = 'Success';
        $response['company_couriers'] = User::whereHas('roles', function($query) {
            $query->where('name', 'company-courier');
        })->get();

        return Response::json($response);
    }

    public function getClientsWithServices() {
        $response['status'] = 'Success';
        $response['clients'] = User::with(['services' => function($query) {
                $query->where('active', 1)
                    ->where(function($query) {
                        $query->where('status', 'pending')->orWhere('status', 'on process');
                    });
            }])
            ->whereHas('roles', function($query) {
                $query->where('name', 'visa-client');
            })->where('is_active', 1)->paginate(200);

        return Response::json($response);
    }

    //all clients
    public function getClientsWithPackagesServices() {
        $response['status'] = 'Success';
        $response['clients'] = User::with('packages.service')
            ->whereHas('roles', function($query) {
                $query->where('name', 'visa-client');
            })->where('is_active', 1)->paginate(100);

        return Response::json($response);
    }

    //post : only updated clients
    public function getUpdatedClientsWithPackagesServices(Request $request) {
        $last = $request->last_update;

        $getUpdates = Updates::where('date_modified','>=',$last)->groupBy('client_id')->orderBy('id','DESC');
        $clients = $getUpdates->pluck('client_id');
        $new_last_update = $getUpdates->first();
        if($new_last_update){        
            $response['new_last_update'] = $new_last_update->date_modified;
            $response['status'] = 'Success';
            $response['clients'] = User::with('packages.service')
                ->whereHas('roles', function($query) {
                    $query->where('name', 'visa-client');
                })->where('is_active', 1)->whereIn('id',$clients)->paginate(100);
            return Response::json($response);
        }
        return [];

    }

    public function getClientServices(Request $request) {
        $response['status'] = 'Success';
        $response['data'] = [];

        foreach($request->clientIds as $clientId) {
            
            $response['data'][] = [
                'id' => $clientId,
                'services' => ClientService::where('client_id', $clientId)
                    ->where('active', 1)
                    ->where(function($query) {
                        $query->where('status', 'pending')->orWhere('status', 'on process');
                    })
                    ->get()
            ];     
        }

        return Response::json($response);
    }

    public function getServicesDocuments(Request $request) {
        $validator = Validator::make($request->all(), [
            'documents_id' => 'required|array'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 400;

            return Response::json($response, $httpStatusCode);
        } else {
            $response['status'] = 'Success';
            $response['documents'] = ServiceDocuments::orderBy('title')->whereIn('id', $request->documents_id)->get();

            return Response::json($response);
        }
    }

    public function batchWriteReport(Request $request) {
        foreach($request->write_report as $__writeReport__) {

            $__clientServices__ = $__writeReport__['client_services'];
            $data = WriteReportHelperController::getData($__clientServices__);

                $distinctClientServices = $data['distinctClientServices'];

                $clientsId = $data['clientsId'];

                $clientServicesId = $__clientServices__;

                $trackings = $data['trackings'];

            $__data__ = $__writeReport__['data'];
            $data2 = WriteReportHelperController::getData2($__data__);

                $actionIdArray = $data2['actionIdArray'];

                $categoryIdArray = $data2['categoryIdArray'];

                $categoryNameArray = $data2['categoryNameArray'];

                $categoryNameChinese = $data2['categoryNameChinese'];

                $date1Array = $data2['date1Array'];

                $date2Array = $data2['date2Array'];

                $date3Array = $data2['date3Array'];

                $dateTimeArray = $data2['dateTimeArray'];

                $extraDetailsArray = $data2['extraDetailsArray'];

                $extraDetailsArrayChinese = $data2['extraDetailsArrayChinese'];

            // For Group Members
            for($i=0; $i<count($clientsId); $i++) {
                for($j=0; $j<count($clientServicesId); $j++) {

                    for($k=0; $k<count($distinctClientServices); $k++) {
                        if($distinctClientServices[$k] == $clientServicesId[$j]['detail']) {
                            $getReport = WriteReportHelperController::getReportDetail(
                                null,
                                $actionIdArray[$k],
                                $categoryIdArray[$k],
                                $categoryNameArray[$k],
                                $date1Array[$k],
                                $date2Array[$k],
                                $date3Array[$k],
                                $dateTimeArray[$k],
                                $extraDetailsArray[$k],
                                $categoryNameChinese[$k],
                                $extraDetailsArrayChinese[$k]
                            );
                            $detail = $getReport[0];
                            $detail_cn = $getReport[1];
                        }
                    }

                    if($clientServicesId[$j]['client_id'] == $clientsId[$i]) {
                        Report::create([
                            'client_id' => $clientsId[$i],
                            'group_id' => ($clientServicesId[$j]['group_id'] != 0) ? $clientServicesId[$j]['group_id'] : null,
                            'service_id' => $clientServicesId[$j]['id'],
                            'detail' => $detail,
                            'user_id' => \Auth::user()->id,
                            'tracking' => $clientServicesId[$j]['tracking'],
                            'log_date' => Carbon::now()->format('Y-m-d H:i:s a'),
                            'is_read' => 0
                        ]);
                        
                        if( !WriteReportHelperController::checkIfGroupLeader($clientServicesId[$j]['group_id'], $clientServicesId[$j]['client_id']) ) {
                            WriteReportHelperController::sendPushNotif($clientServicesId[$j]['id'], $detail, null, null, $detail_cn);
                        }
                    }

                }
            } // endfor

            // For Group Leader
            $groupIdArrayDone = [];
            $serviceIdArrayDone = [];
            for ( $i=0; $i<count($clientServicesId); $i++ ) {

                if ( $clientServicesId[$i]['group_id'] != 0 ) {
                    $done = false;
                    for ( $k=0; $k<count($groupIdArrayDone); $k++ ) {
                        if( $groupIdArrayDone[$k] == $clientServicesId[$i]['group_id'] && $serviceIdArrayDone[$k] == $clientServicesId[$i]['service_id'] ) {
                            $done = true;
                        }
                    }

                    if(!$done) {
                        $groupIdArrayDone[] = $clientServicesId[$i]['group_id'];
                        $serviceIdArrayDone[] = $clientServicesId[$i]['service_id'];

                        $clientsIdArray = [];
                        for ( $j=0; $j<count($clientServicesId); $j++ ) {
                            if ( $clientServicesId[$i]['group_id'] == $clientServicesId[$j]['group_id'] && $clientServicesId[$i]['service_id'] == $clientServicesId[$j]['service_id'] ) {
                                $clientsIdArray[] = $clientServicesId[$j]['client_id'];
                            }
                        }

                        $groupLeaderId = Group::find($clientServicesId[$i]['group_id'])->leader;

                        for( $k=0; $k<count($request->distinctClientServices); $k++ ) {
                            if($request->distinctClientServices[$k] == $clientServicesId[$i]['detail']) {
                                $getReport = WriteReportHelperController::getReportDetail(
                                    WriteReportHelperController::getAllClientNames($clientsIdArray, $groupLeaderId),
                                    $actionIdArray[$k],
                                    $categoryIdArray[$k],
                                    $categoryNameArray[$k],
                                    $date1Array[$k],
                                    $date2Array[$k],
                                    $date3Array[$k],
                                    $dateTimeArray[$k],
                                    $extraDetailsArray[$k],
                                    $categoryNameChinese[$k],
                                    $extraDetailsArrayChinese[$k]
                                );
                                $detail = $getReport[0];
                                $detail_cn = $getReport[1];
                            }
                        }

                        WriteReportHelperController::sendPushNotif($clientServicesId[$i]['id'], $detail, $groupLeaderId, WriteReportHelperController::getAllClientNames($clientsIdArray, $groupLeaderId), $detail_cn);
                    }
                }
            } // endfor

        } // endforeach

        $response['status'] = 'Success';

        return Response::json($response);
    }

    public function writeReport(Request $request) {
        $validator = Validator::make($request->all(), [
            'client_services' => 'required|array',
            'data' => 'required|array',
            'data.*.action_id' => 'required',

            'data.*.action_1_category_id' => 'required_if:data.*.action_id, == , 1',
            'data.*.action_1_current_date' => 'required_if:data.*.action_id, == , 1',
            'data.*.action_1_estimated_releasing_date' => 'required_if:data.*.action_id, == , 1',
            // 'data.*.action_1_scheduled_hearing_date_and_time' => 'array|required_if:data.*.action_1_category_id, ==, 1',
            'data.*.action_1_scheduled_appointment_date_and_time' => '
                required_if:data.*.action_1_category_id, ==, 6 |
                required_if:data.*.action_1_category_id, ==, 7 |
                required_if:data.*.action_1_category_id, ==, 10 |
                required_if:data.*.action_1_category_id, ==, 14 |
                required_if:data.*.action_1_category_id, ==, 15
            ',

            'data.*.action_2_category_id' => 'required_if:data.*.action_id, == , 2',

            'data.*.action_3_category_id' => 'required_if:data.*.action_id, == , 3',
            'data.*.action_3_current_date' => 'required_if:data.*.action_id, == , 3',

            'data.*.action_4_current_date' => 'required_if:data.*.action_id, == , 4',
            'data.*.action_4_documents' => 'nullable|array|required_if:data.*.action_id, == , 4',

            'data.*.action_6_submission_date' => 'required_if:data.*.action_id, == , 6',
            'data.*.action_6_consequence_of_non_submission' => 'nullable|array',

            'data.*.action_7_current_date' => 'required_if:data.*.action_id, == , 7',
            'data.*.action_7_estimated_time_of_finishing' => 'required_if:data.*.action_id, == , 7',

            'data.*.action_8_category_id' => 'required_if:data.*.action_id, == , 8',
            'data.*.action_8_affected_date_from' => 'required_if:data.*.action_id, == , 8',
            'data.*.action_8_affected_date_to' => 'required_if:data.*.action_id, == , 8',

            'data.*.action_9_category_id' => 'required_if:data.*.action_id, == , 9',
            'data.*.action_9_category_name' => 'required_if:data.*.action_9_category_id, == , 0',
            'data.*.action_9_date_range_from' => 'required_if:data.*.action_id, == , 9',
            'data.*.action_9_date_range_to' => 'required_if:data.*.action_id, == , 9',

            'data.*.action_10_category_id' => 'required_if:data.*.action_id, == , 10',
            'data.*.action_10_extended_period_for_processing' => 'required_if:data.*.action_id, == , 10',

            'data.*.action_11_category_id' => 'required_if:data.*.action_id, == , 11',
            'data.*.action_11_current_date' => 'required_if:data.*.action_id, == , 11',
            'data.*.action_11_estimated_releasing_date' => 'required_if:data.*.action_id, == , 11',

            'data.*.action_12_date_and_time_pickup' => 'required_if:data.*.action_id, == , 12',

            'data.*.action_13_date_and_time_deliver' => 'required_if:data.*.action_id, == , 13',

            'data.*.action_14_category_id' => 'required_if:data.*.action_id, == , 14',
            'data.*.action_14_company_courier' => 'nullable|array|required_if:data.*.action_14_category_id, == , 128',
            'data.*.action_14_tracking_number' => 'nullable|array|required_if:data.*.action_14_category_id, != , 128',
            'data.*.action_14_date_and_time_delivered' => 'required_if:data.*.action_id, == , 14',

            'data.*.action_15_category_id' => 'required_if:data.*.action_id, == , 15',
            'data.*.action_15_current_date' => 'required_if:data.*.action_id, == , 15',
            'data.*.action_15_estimated_releasing_date' => '
                required_if:data.*.action_15_category_id, == , 76 |
                required_if:data.*.action_15_category_id, == , 77 |
                required_if:data.*.action_15_category_id, == , 78 |
                required_if:data.*.action_15_category_id, == , 79 |
                required_if:data.*.action_15_category_id, == , 80 |
                required_if:data.*.action_15_category_id, == , 81 |
                required_if:data.*.action_15_category_id, == , 82 |
                required_if:data.*.action_15_category_id, == , 83
            ',

            'data.*.action_16_category_id' => 'required_if:data.*.action_id, == , 16',
        ], [
            'data.required' => 'The write report form data is required.',

            'data.*.action_1_category_id.required_if' => 'The category field is required when action is Filed.',
            'data.*.action_1_current_date.required_if' => 'The current date field is required when action is Filed.',
            'data.*.action_1_estimated_releasing_date.required_if' => 'The estimated releasing date field is required when action is Filed.',
            // 'data.*.action_1_scheduled_hearing_date_and_time.required_if' => 'The scheduled hearing date and time field is required when category is Immigration.',
            'data.*.action_1_scheduled_appointment_date_and_time.required_if' => 'The scheduled appointment date and time field is required when category is PRA, Chinese Embassy, BOQ, LTO or NBI.',

            'data.*.action_2_category_id.required_if' => 'The category field is required when action is Released.',

            'data.*.action_3_category_id.required_if' => 'The category field is required when action is Paid OPS.',
            'data.*.action_3_current_date.required_if' => 'The current date field is required when action is Paid OPS.',

            'data.*.action_4_current_date.required_if' => 'The current date field is required when action is Unable to file need additional requirements.',
            'data.*.action_4_documents.required_if' => 'The documents field is required when action is Unable to file need additional requirements.',

            'data.*.action_6_submission_date.required_if' => 'The submission date field is required when action is Deadline of submission.',
            'data.*.action_6_consequence_of_non_submission.array' => 'The consequence of non submission field must be an array.',

            'data.*.action_7_current_date.required_if' => 'The current date field is required when action is For implementation.',
            'data.*.action_7_estimated_time_of_finishing' => 'The estimated time of finishing field is required when action is For implementation.',

            'data.*.action_8_category_id.required_if' => 'The category field is required when action is Unable to file due to bad weather.',
            'data.*.action_8_affected_date_from.required_if' => 'The affected date from field is required when action is Unable to file due to bad weather.',
            'data.*.action_8_affected_date_to.required_if' => 'The affected date to field is required when action is Unable to file due to bad weather.',

            'data.*.action_9_category_id.required_if' => 'The category field is required when action is Unable to finish due to suspension of work.',
            'data.*.action_9_category_name.required_if' => 'The category name field is requried when category is custom.',
            'data.*.action_9_date_range_from.required_if' => 'The date range from field is required when action is Unable to finish due to suspension of work.',
            'data.*.action_9_date_range_to.required_if' => 'The date range to field is required when action is Unable to finish due to suspension of work.',

            'data.*.action_10_category_id.required_if' => 'The category field is required when action is Extended processing period.',
            'data.*.action_10_extended_period_for_processing.required_if' => 'The extended period for processing field is required when action is Extended processing period.',

            'data.*.action_11_category_id.required_if' => 'The category field is required when action is Paid at.',
            'data.*.action_11_current_date.required_if' => 'The current date field is required when action is Paid at.',
            'data.*.action_11_estimated_releasing_date' => 'The estimated releasing date field is required when action is Paid at.',

            'data.*.action_12_date_and_time_pickup.required_if' => 'The date and time pickup field is required when action is Picked up documents from client.',

            'data.*.action_13_date_and_time_deliver.required_if' => 'The date and time deliver field is required when For delivery.',

            'data.*.action_14_category_id.required_if' => 'The category field is required when action is Delivered.',
            'data.*.action_14_company_courier.required_if' => 'The company courier field is required when category is Company Courier.',
            'data.*.action_14_tracking_number.required_if' => 'The tracking number field is required when category is not Company Courier.',
            'data.*.action_14_date_and_time_delivered.required_if' => 'The date and time delivered is required when action is Delivered.',

            'data.*.action_15_category_id.required_if' => 'The category field is required when action is For processing of.',
            'data.*.action_15_current_date.required_if' => 'The current date field is required when action is For processing of.',
            'data.*.action_15_estimated_releasing_date.required_if' => 'The estimated releasing date is required when category is Barangay clearance, Business Permit/Mayors Permit, Locational Clearance, Sanitary Permit, BIR Registration, Lessor Permit, Occupational Permit or Architectural Plans.',

            'data.*.action_16_category_id.required_if' => 'The category field is required when action is Pending.',
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 400;

            return Response::json($response, $httpStatusCode);
        } else {
            $data = WriteReportHelperController::getData($request->client_services);

                $distinctClientServices = $data['distinctClientServices'];

                $clientsId = $data['clientsId'];

                $clientServicesId = $request->client_services;

                $trackings = $data['trackings'];

            $data2 = WriteReportHelperController::getData2($request->data);

                $actionIdArray = $data2['actionIdArray'];

                $categoryIdArray = $data2['categoryIdArray'];

                $categoryNameArray = $data2['categoryNameArray'];

                $categoryNameChinese = $data2['categoryNameChinese'];

                $date1Array = $data2['date1Array'];

                $date2Array = $data2['date2Array'];

                $date3Array = $data2['date3Array'];

                $dateTimeArray = $data2['dateTimeArray'];

                $extraDetailsArray = $data2['extraDetailsArray'];

                $extraDetailsArrayChinese = $data2['extraDetailsArrayChinese'];

            // For Group Members
            for($i=0; $i<count($clientsId); $i++) {
                for($j=0; $j<count($clientServicesId); $j++) {

                    for($k=0; $k<count($distinctClientServices); $k++) {
                        if($distinctClientServices[$k] == $clientServicesId[$j]['detail']) {
                            $getReport = WriteReportHelperController::getReportDetail(
                                null,
                                $actionIdArray[$k],
                                $categoryIdArray[$k],
                                $categoryNameArray[$k],
                                $date1Array[$k],
                                $date2Array[$k],
                                $date3Array[$k],
                                $dateTimeArray[$k],
                                $extraDetailsArray[$k],
                                $categoryNameChinese[$k],
                                $extraDetailsArrayChinese[$k]
                            );
                            $detail = $getReport[0];
                            $detail_cn = $getReport[1];
                        }
                    }

                    if($clientServicesId[$j]['client_id'] == $clientsId[$i]) {
                        Report::create([
                            'client_id' => $clientsId[$i],
                            'group_id' => ($clientServicesId[$j]['group_id'] != 0) ? $clientServicesId[$j]['group_id'] : null,
                            'service_id' => $clientServicesId[$j]['id'],
                            'detail' => $detail,
                            'user_id' => \Auth::user()->id,
                            'tracking' => $clientServicesId[$j]['tracking'],
                            'log_date' => Carbon::now()->format('Y-m-d H:i:s a'),
                            'is_read' => 0
                        ]);
                        
                        if( !WriteReportHelperController::checkIfGroupLeader($clientServicesId[$j]['group_id'], $clientServicesId[$j]['client_id']) ) {
                            WriteReportHelperController::sendPushNotif($clientServicesId[$j]['id'], $detail, null, null, $detail_cn);
                        }
                    }

                }
            }

            // For Group Leader
            $groupIdArrayDone = [];
            $serviceIdArrayDone = [];
            for ( $i=0; $i<count($clientServicesId); $i++ ) {

                if ( $clientServicesId[$i]['group_id'] != 0 ) {
                    $done = false;
                    for ( $k=0; $k<count($groupIdArrayDone); $k++ ) {
                        if( $groupIdArrayDone[$k] == $clientServicesId[$i]['group_id'] && $serviceIdArrayDone[$k] == $clientServicesId[$i]['service_id'] ) {
                            $done = true;
                        }
                    }

                    if(!$done) {
                        $groupIdArrayDone[] = $clientServicesId[$i]['group_id'];
                        $serviceIdArrayDone[] = $clientServicesId[$i]['service_id'];

                        $clientsIdArray = [];
                        for ( $j=0; $j<count($clientServicesId); $j++ ) {
                            if ( $clientServicesId[$i]['group_id'] == $clientServicesId[$j]['group_id'] && $clientServicesId[$i]['service_id'] == $clientServicesId[$j]['service_id'] ) {
                                $clientsIdArray[] = $clientServicesId[$j]['client_id'];
                            }
                        }

                        $groupLeaderId = Group::find($clientServicesId[$i]['group_id'])->leader;

                        for( $k=0; $k<count($request->distinctClientServices); $k++ ) {
                            if($request->distinctClientServices[$k] == $clientServicesId[$i]['detail']) {
                                $getReport = WriteReportHelperController::getReportDetail(
                                    WriteReportHelperController::getAllClientNames($clientsIdArray, $groupLeaderId),
                                    $actionIdArray[$k],
                                    $categoryIdArray[$k],
                                    $categoryNameArray[$k],
                                    $date1Array[$k],
                                    $date2Array[$k],
                                    $date3Array[$k],
                                    $dateTimeArray[$k],
                                    $extraDetailsArray[$k],
                                    $categoryNameChinese[$k],
                                    $extraDetailsArrayChinese[$k]
                                );
                                $detail = $getReport[0];
                                $detail_cn = $getReport[1];
                            }
                        }

                        WriteReportHelperController::sendPushNotif($clientServicesId[$i]['id'], $detail, $groupLeaderId, WriteReportHelperController::getAllClientNames($clientsIdArray, $groupLeaderId), $detail_cn);
                    }
                }

            }

            $response['status'] = 'Success';

            return Response::json($response);
        }
    }

    // Group::Add members
    public function getAvailableClients(Request $request) {
        $validator = Validator::make($request->all(), [
            'groupId' => 'required',
            'q' => 'required'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 400;

            return Response::json($response, $httpStatusCode);
        } else {
            $q = $request->q;

            $groupMembers = GroupMember::where('group_id', $request->groupId)->pluck('client_id');

            $clients = User::select(DB::raw("
                    users.id AS id,
                    CONCAT('[', users.id, ']', ' ', users.first_name, ' ', users.last_name) AS name
                "))
                ->where(function($query) use($q) {
                    $query->where('users.first_name', 'LIKE', '%'.$q.'%')
                        ->orWhere('users.last_name', 'LIKE', '%'.$q.'%')
                        ->orWhere('users.id', 'LIKE', '%'.$q.'%'); 
                })
                ->whereNotIn('id', $groupMembers)
                ->whereHas('roles', function($query) {
                    $query->where('role_id', 9);
                })
                ->get();

            $response['status'] = 'Success';
            $response['clients'] = $clients;

            return Response::json($response);
        }
    }

    public function addMembers(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'clientId' => 'required|array'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 400;

            return Response::json($response, $httpStatusCode);
        } else {
            foreach($request->clientId as $clientId) {
                GroupMember::create([
                    'group_id' => $id,
                    'client_id' => $clientId,
                    'leader' => 0
                ]);

                SynchronizationController::update('groups', Carbon::now()->toDateTimeString());

                $member = User::findOrFail($clientId);
                $memberLabel = '[' . $member->id . '] ' . $member->full_name;
                $details = 'Added <strong>' . $memberLabel . '</strong> as new member of the group.';
                Common::saveActionLogs(0, 0, $details, $id);
            }

            $response['status'] = 'Success';

            return Response::json($response);
        }
    }

    public function serviceDocuments() {
        return ServiceDocuments::orderBy('title')->get();
    }

    public function actionCategories() {
        return Action::with('categories')->orderBy('name')->get();
    }
    
    public function getServicesByClient($id) {
        return ClientService::where('client_id', $id)
            ->where('active', 1)
            ->where(function($query) {
                $query->where('status', 'pending')->orWhere('status', 'on process');
            })
            ->get();
    }

    // Synchronization
    public function synchronizationIndex() {
        return Synchronization::all();
    }

        // Module::services
        public function synchronizationServices() {
            return Service::all();
        }

        public function synchronizationServiceProfiles() {
            return ServiceProfiles::all();
        }

        public function synchronizationServicesWithProfiles() {
            return Service::with('servicecost')->get();
        }

        // Module::actions
        public function synchronizationActions() {
            return Action::all();
        }

        public function synchronizationCategories() {
            return Category::all();
        }

        public function synchronizationActionCategories() {
            return Action::with('categories')->get();
        }

        // Module::documents
        public function synchronizationDocuments() {
            return ServiceDocuments::all();
        }

        public function updateTrackingStatus($client_id) {
            $packs = Package::where('client_id',$client_id)->get();
            foreach($packs as $p){
                $this->check_package_updates($p->tracking);
            }
            //return Response::json($packs);
            $response['status'] = 'Success';

            return Response::json($response);
        }

    // Add group members
    public function addGroupMembers(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required|array',
            'data.*.client_id' => 'required',
            'data.*.group_id' => 'required',
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 422;

            return Response::json($response, $httpStatusCode);
        } else {
            foreach($request->data as $d) {
                $clientId = $d['client_id'];
                $groupId = $d['group_id'];

                GroupMember::create([
                    'client_id' => $clientId,
                    'group_id' => $groupId,
                    'leader' => 0
                ]);

                // Action log
                $member = User::findOrFail($clientId);
                $memberLabel = '[' . $member->id . '] ' . $member->full_name;
                $details = 'Added <strong>' . $memberLabel . '</strong> as new member of the group.';
                Common::saveActionLogs(0, $clientId, $details, $groupId);
            }

            $response['status'] = 'Success';

            return Response::json($response);
        }
    }

    public function load_client_list($start,$end) {
        
        $paths = storage_path('/app/public/data');
        if (!File::exists($paths)){
          Storage::put($paths.'/Clients.txt', '');
        }
        $myfile = fopen($paths.'/Clients.txt', 'w') or die('Unable to open file!');
        $txt = "{\n";
        fwrite($myfile, $txt);
        $txt = '"data":[';
        fwrite($myfile, $txt);
        $txt = "\n";
        fwrite($myfile, $txt);
        $count = 0;

        $records = DB::connection('mysql')
            ->table('users as a')
            ->select(DB::raw('
                a.*,
                total.total_amount as total_costs,
                total.service_date,
                total6.dates,total6.dateshid, role.role_id,
                srv.sdates,
                srv.sdateshid,
                total2.total_deposit,
                total2.total_refund,
                total2.total_discount,
                total2.total_payment'))
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(sum(b.cost)+sum(b.charge)+sum(b.tip)+sum(b.com_client)+sum(b.com_agent),0) as total_amount,
                        COALESCE(sum(b.tip),0) as total_charge,
                        b.client_id, b.service_date
                        from visa.client_services as b
                        where b.active = 1
                        and b.group_id = 0
                        group by b.client_id
                        order by b.service_date desc
                    ) as total'),
                    'total.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  SUM(IF(c.type = "Deposit", c.amount, 0)) as total_deposit,
                        SUM(IF(c.type = "Refund", c.amount, 0)) as total_refund,
                        SUM(IF(c.type = "Payment", c.amount, 0)) as total_payment,
                        SUM(IF(c.type = "Discount", IF(c.deleted_at = null, c.amount,0), 0)) as total_discount,
                        c.client_id
                        from visa.client_transactions as c
                        where c.group_id = 0
                        group by c.client_id
                    ) as total2'),
                    'total2.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select *
                        from role_user as r
                        where r.role_id = 9
                    ) as role'),
                    'role.user_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as dates,
                        x.client_id,date_format(max(x.dates),"%Y-%m-%d %H:%i:%s") as dateshid
                        from( SELECT STR_TO_DATE(log_date, "%M %e, %Y, %l:%i %p") as dates,
                            client_id, status
                            FROM visa.packages
                            ORDER BY dates desc
                        ) as x
                        group by x.client_id) as total6'),
                    'total6.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as sdates,
                        date_format(max(cs.servdates),"%Y:%m:%d") as sdateshid,cs.client_id
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM visa.client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.client_id) as srv'),
                    'srv.client_id', '=', 'a.id')
                ->where("role.role_id","9")->get();


        foreach ($records as $value) {

                $col_balance = 0;
                $total_balance = 0;

                $id = $value->id;
                if($id >= $start && $id <= $end){
                    $disc = $this->clientDiscount($id);
                    $total_balance =  (
                            (
                                $value->total_deposit
                                + $value->total_payment
                                + $disc
                            )
                            - (
                                $value->total_refund
                                + $value->total_costs
                            )
                        );
                }
                else{
                    $total_balance = floatval($value->balance);
                }

                if($total_balance<0){
                    if($id >= $start && $id <= $end){
                        $col_balance = (
                            (
                                $value->total_deposit
                                + $value->total_payment
                                + $disc
                            )
                            - (
                                $value->total_refund
                                + ($this->getClientCompleteCost($id))
                            )
                        );
                    }
                    else{
                        $col_balance = floatval($value->collectable);
                    }

                    if($col_balance > 0){
                        $col_balance = 0;
                    }
                    // else{
                    //     $collect += $col_balance;
                    // }
                }

                $count++;
                $txt = "[";
                fwrite($myfile, $txt);
                $txt = '"'.$value->id.'",';
                fwrite($myfile, $txt);
                // $txt = '"'.ucwords($value->first_name).' '.ucwords($value->last_name).'",';
                if($value->risk == 'High-Risk'){
                    $txt = '"<span style=\"color:red;\" id=\"cl_'.$value->id.'\"><b>'.ucwords($value->first_name).' '.ucwords($value->last_name).'</b></span>",';
                }
                else{
                    $txt = '"<span id=\"cl_'.$value->id.'\"><b>'.ucwords($value->first_name).' '.ucwords($value->last_name).'</b></span>",';
                }
                fwrite($myfile, $txt);

                if($total_balance==0){
                $txt = '"'.(Common::formatMoney($total_balance)).'",';
                }
                else{
                $txt = '"'.(Common::formatMoney($total_balance*1)).'",';
                }

                fwrite($myfile, $txt);
                if($col_balance==0){
                $txt = '"'.(Common::formatMoney($col_balance)).'",';
                }
                else{
                $txt = '"'.(Common::formatMoney($col_balance*1)).'",';
                }

                // updating of user balance and collectables
                // if($disc > 0){

                User::where('id', $value->id)
                 ->update([
                    'balance' => ($total_balance), 
                    'collectable' => $col_balance
                ]);
                // }


                fwrite($myfile, $txt);
                $txt = '"<span style=\"display:none;\">'.$value->dateshid.'--</span>'.$value->dates.'",';

                fwrite($myfile, $txt);
                $txt = '"<span style=\"display:none;\">'.$value->sdateshid.'--</span>'.$value->sdates.'",';

                fwrite($myfile, $txt);
                $txt = '"<center><a href=\"#\" id=\"cs_'.$value->id.'\" client-id=\"'.$value->id.'\" client-status=\"'.$value->risk.'\" class=\"changeStatus\" data-toggle=\"tooltip\" title=\"Change Status\" style=\"margin-right:8px;\"><i class=\"fa fa-bell fa-1x\"></i></a><a href=\"/visa/client/'.$value->id.'\" target=\"_blank\" data-toggle=\"tooltip\" title=\"View Client\"><i class=\"fa fa-arrow-right fa-1x\"></i></a></center>"';
                fwrite($myfile, $txt);
                if(count($records)==$count){
                    $txt = "]\n";
                }else{
                    $txt = "],\n";
                }
                fwrite($myfile, $txt);
            }
            $txt = "\n]\n}";
            fwrite($myfile, $txt);
            fclose($myfile);
            return array(true);
    }


    public function load_client_list_dec1($start,$end) {

        $records = DB::connection('mysql')
            ->table('users as a')
            ->select(DB::raw('
                a.*,
                total.total_amount as total_costs,
                total.service_date,
                total6.dates,total6.dateshid, role.role_id,
                srv.sdates,
                srv.sdateshid,
                total2.total_deposit,
                total2.total_refund,
                total2.total_discount,
                total2.total_payment'))
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(sum(b.cost)+sum(b.charge)+sum(b.tip)+sum(b.com_client)+sum(b.com_agent),0) as total_amount,
                        COALESCE(sum(b.tip),0) as total_charge,
                        b.client_id, b.service_date
                        from visa.client_services as b
                        where b.active = 1
                        and b.group_id = 0 and b.id <=67386
                        group by b.client_id
                        order by b.service_date desc
                    ) as total'),
                    'total.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  SUM(IF(c.type = "Deposit", c.amount, 0)) as total_deposit,
                        SUM(IF(c.type = "Refund", c.amount, 0)) as total_refund,
                        SUM(IF(c.type = "Payment", c.amount, 0)) as total_payment,
                        SUM(IF(c.type = "Discount", IF(c.deleted_at = null, c.amount,0), 0)) as total_discount,
                        c.client_id
                        from visa.client_transactions as c
                        where c.group_id = 0
                        and c.id <=21208
                        group by c.client_id
                    ) as total2'),
                    'total2.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select *
                        from role_user as r
                        where r.role_id = 9
                    ) as role'),
                    'role.user_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as dates,
                        x.client_id,date_format(max(x.dates),"%Y-%m-%d %H:%i:%s") as dateshid
                        from( SELECT STR_TO_DATE(log_date, "%M %e, %Y, %l:%i %p") as dates,
                            client_id, status
                            FROM visa.packages
                            ORDER BY dates desc
                        ) as x
                        group by x.client_id) as total6'),
                    'total6.client_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as sdates,
                        date_format(max(cs.servdates),"%Y:%m:%d") as sdateshid,cs.client_id
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM visa.client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.client_id) as srv'),
                    'srv.client_id', '=', 'a.id')
                ->where("role.role_id","9")
                ->where("a.id",">=", $start)
                ->where("a.id","<=", $end)->get();


        foreach ($records as $value) {

                $col_balance = 0;
                $total_balance = 0;

                $id = $value->id;
                if($id >= $start && $id <= $end){
                    $disc = $this->clientDiscount($id);
                    $total_balance =  (
                            (
                                $value->total_deposit
                                + $value->total_payment
                                + $disc
                            )
                            - (
                                $value->total_refund
                                + $value->total_costs
                            )
                        );
                }
                else{
                    $total_balance = floatval($value->balance);
                }

                if($total_balance<0){
                    if($id >= $start && $id <= $end){
                        $col_balance = (
                            (
                                $value->total_deposit
                                + $value->total_payment
                                + $disc
                            )
                            - (
                                $value->total_refund
                                + ($this->getClientCompleteCostDec1($id))
                            )
                        );
                    }
                    else{
                        $col_balance = floatval($value->collectable);
                    }

                    if($col_balance > 0){
                        $col_balance = 0;
                    }
                    // else{
                    //     $collect += $col_balance;
                    // }
                }


                User::where('id', $value->id)
                 ->update([
                    'temp_balance' => ($total_balance), 
                    'temp_collectable' => $col_balance
                ]);

            }
            
            return array(true);
    }


    public function load_group_list_dec1($start,$end) {

        $records = DB::connection('visa')
            ->table('groups as a')
            ->select(DB::raw('
                a.*,
                total.total_amount as total_costs,
                total.service_date,
                total6.dates,total6.dateshid,
                srv.sdates,
                srv.sdateshid,
                total2.total_deposit,
                total2.total_refund,
                total2.total_discount,
                total2.total_payment'))
                ->leftjoin(DB::raw('
                    (
                        Select  IFNULL(sum(b.cost)+sum(b.charge)+sum(b.tip)+sum(b.com_client)+sum(b.com_agent),0) as total_amount,
                        COALESCE(sum(b.tip),0) as total_charge,
                        b.client_id,b.group_id, b.service_date
                        from visa.client_services as b
                        where b.active = 1
                        and b.id <=67386
                        group by b.group_id
                        order by b.service_date desc
                    ) as total'),
                    'total.group_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select  SUM(IF(c.type = "Deposit", c.amount, 0)) as total_deposit,
                        SUM(IF(c.type = "Refund", c.amount, 0)) as total_refund,
                        SUM(IF(c.type = "Payment", c.amount, 0)) as total_payment,
                        SUM(IF(c.type = "Discount", IF(c.deleted_at = null, c.amount,0), 0)) as total_discount,
                        c.group_id
                        from visa.client_transactions as c
                        where c.id <=21208
                        group by c.group_id
                    ) as total2'),
                    'total2.group_id', '=', 'a.id')
                // ->leftjoin(DB::raw('
                //     (
                //         Select *
                //         from role_user as r
                //         where r.role_id = 9
                //     ) as role'),
                //     'role.user_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as dates,
                        x.client_id,x.group_id, date_format(max(x.dates),"%Y-%m-%d %H:%i:%s") as dateshid
                        from( SELECT STR_TO_DATE(log_date, "%M %e, %Y, %l:%i %p") as dates,
                            group_id, status, client_id
                            FROM visa.packages
                            ORDER BY dates desc
                        ) as x
                        group by x.group_id) as total6'),
                    'total6.group_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as sdates,
                        date_format(max(cs.servdates),"%Y:%m:%d") as sdateshid,cs.client_id,cs.group_id
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM visa.client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.group_id) as srv'),
                    'srv.group_id', '=', 'a.id')
                //->where("role.role_id","9")
                ->get();

                //\Log::info($records->count());
        foreach ($records as $value) {

                $col_balance = 0;
                $total_balance = 0;

                $id = $value->id;

                if($id >= $start && $id <= $end){
                //\Log::info($value->total_costs);
                //\Log::info($value->total_deposit);
                    $disc = $this->getGroupDiscount($id);
                    $total_balance =  (
                            (
                                $value->total_deposit
                                + $value->total_payment
                                + $disc
                            )
                            - (
                                $value->total_refund
                                + $value->total_costs
                            )
                        );
                }
                else{
                    $total_balance = floatval($value->balance);
                }

                if($total_balance<0){
                    if($id >= $start && $id <= $end){
                        $col_balance = (
                            (
                                $value->total_deposit
                                + $value->total_payment
                                + $disc
                            )
                            - (
                                $value->total_refund
                                + ($this->getCompleteCostDec1($id))
                            )
                        );
                    }
                    else{
                        $col_balance = floatval($value->collectable);
                    }

                    if($col_balance > 0){
                        $col_balance = 0;
                    }
                    // else{
                    //     $collect += $col_balance;
                    // }
                }


                Group::where('id', $value->id)
                 ->update([
                    'temp_balance' => ($total_balance), 
                    'temp_collectable' => $col_balance
                ]);

            }
            
            return array(true);
    }


    public function updateGroupBalance(){
        $myfile = fopen(storage_path('/app/public/data/Group.txt'), 'w') or die('Unable to open file!');
        $txt = "{\n";
        fwrite($myfile, $txt);
        $txt = '"data":[';
        fwrite($myfile, $txt);
        $txt = "\n";
        fwrite($myfile, $txt);
        $count = 0;

        // $records = DB::connection('visa')
        //      ->table('groups as a')
        //      ->select(DB::raw('a.*, b.*'))
        //      ->leftjoin(DB::raw('(select * from wycgroup.users) as b'), 'b.id', '=', 'a.leader')
        //      ->get();

        $records = DB::connection('visa')
            ->table('groups as a')
            ->select(DB::raw('
                a.*,a.id as gr_id,a.balance as gr_bal, a.collectable as gr_col, b.*,
                total6.dates,total6.dateshid,
                srv.sdates,srv.sdateshid
                '))
                ->leftjoin(DB::raw('(select * from wycgroup.users) as b'), 'b.id', '=', 'a.leader')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(x.dates),"%M %e, %Y, %l:%i %p") as dates,
                        x.group_id,date_format(max(x.dates),"%Y-%m-%d %H:%i:%s") as dateshid
                        from( SELECT STR_TO_DATE(log_date, "%M %e, %Y, %l:%i %p") as dates,
                            group_id, status
                            FROM packages
                            ORDER BY dates desc
                        ) as x
                        group by x.group_id) as total6'),
                    'total6.group_id', '=', 'a.id')
                ->leftjoin(DB::raw('
                    (
                        Select date_format(max(cs.servdates),"%M %e, %Y") as sdates,cs.client_id,
                        date_format(max(cs.servdates),"%Y:%m:%d") as sdateshid,cs.group_id
                        from( SELECT STR_TO_DATE(service_date, "%m/%d/%Y") as servdates,
                            group_id, active,client_id
                            FROM client_services
                            ORDER BY servdates desc
                        ) as cs
                        where cs.active = 1
                        group by cs.group_id) as srv'),
                    'srv.group_id', '=', 'a.id');

        // if($request && $request->branch != 0) {
        //     $records = $records->where('a.branch_id', $request->branch)->get();
        // } else {
            $records = $records->get();
        //}

        $bal = 0;
        $collect = 0;
        foreach ($records as $value) {

            $id = $value->gr_id;
                //if($id == 450){

                $tempbalance = (
                    (
                        $this->getGroupDeposit($id)
                        + $this->getGroupPayment($id)
                        +$this->getGroupDiscount($id)
                    )
                    - (
                        $this->getGroupRefund($id)
                    )
                );

                $total_balance = (
                    (
                        $tempbalance
                    )
                    - (
                        $this->getGroupCost($id)
                    )
                );

                $col_balance = (
                    (
                        $tempbalance
                    )
                    - (
                        $this->getCompleteCost($id)
                    )
                );
                //$total_balance = $value->gr_bal;
                //\Log::info($total_balance);
                //\Log::info($col_balance);
                //$col_balance = 0;
                //if($total_balance<0){
                //$col_balance = $total_balance;
                //$col_balance = $value->gr_col;
                if($col_balance > 0){
                    $col_balance = 0;
                }
                else{
                    $collect += $col_balance;
                }

                //}

                if($total_balance<0){
                    $bal += $total_balance;
                }
            $count++;
            $txt = "[";
            fwrite($myfile, $txt);
            if($value->risk == 'High-Risk'){
                $txt = '"<span style=\"color:red;\" id=\"cl_'.$value->gr_id.'\"><b>'.ucwords($value->name).'</b></span>",';
            }
            else{
                $txt = '"<span id=\"cl_'.$value->gr_id.'\"><b>'.ucwords($value->name).'</b></span>",';
            }

            fwrite($myfile, $txt);
            $txt = '"'.Common::formatMoney($total_balance*1).'",';
            fwrite($myfile, $txt);
            $txt = '"'.(Common::formatMoney($col_balance*1)).'",';

            Group::where('id', $value->gr_id)
                ->update(['balance' => $total_balance, 'collectable' => $col_balance]);

            fwrite($myfile, $txt);
            $txt = '"'.ucwords($value->first_name).' '.ucwords($value->last_name).'",';
            fwrite($myfile, $txt);
            // $txt = '"'.ucwords($value->dates).'",';
            $txt = '"<span style=\"display:none;\">'.$value->dateshid.'--</span>'.ucwords($value->dates).'",';
            fwrite($myfile, $txt);
            // $txt = '"'.ucwords($value->sdates).'",';
            $txt = '"<span style=\"display:none;\">'.$value->sdateshid.'--</span>'.ucwords($value->sdates).'",';
            fwrite($myfile, $txt);
            $txt = '"<center><a href=\"#\" id=\"cs_'.$value->gr_id.'\" client-id=\"'.$value->gr_id.'\" client-status=\"'.$value->risk.'\" class=\"changeStatus\" data-toggle=\"tooltip\" title=\"Change Status\" style=\"margin-right:8px;\"><i class=\"fa fa-bell fa-1x\"></i></a><a href=\"#\"  data-toggle=\"tooltip\" title=\"Export to Excel\" class=\"exportExcel\" group-id=\"'.$value->gr_id.'\" style=\"margin-right:8px;\"><i class=\"fa fa-file-text fa-1x\" ></i></a><a href=\"/visa/group/'.$value->gr_id.'\" target=\"_blank\" data-toggle=\"tooltip\" title=\"View Group\"><i class=\"fa fa-arrow-right fa-1x\"></i></a></center>"';

            //<a href=\"/visa/report/get-group-summary/'.$value->gr_id.'\" class=\"groupToExcel\" group-id=\"'.$value->gr_id.'\" style=\"margin-right:8px;\" data-toggle=\"tooltip\" title=\"Export to Excel\"><i class=\"fa fa-file-text fa-1x\"></i></a>

            fwrite($myfile, $txt);
            if(count($records)==$count){
                $txt = "]\n";
            }else{
                $txt = "],\n";
            }

            fwrite($myfile, $txt);
        //}
        }
        $txt = "\n]\n}";
        fwrite($myfile, $txt);
        fclose($myfile);
        return array(true);
    }

    // Add service multiple // individual/group
    public function servicesBatchStore(Request $request) {
        $validator = Validator::make($request->all(), [
            'data' => 'required|array',
            'data.*.client_id' => 'required',
            'data.*.group_id' => 'required',
            'data.*.services' => 'required|array',
            'data.*.discount' => 'numeric|nullable',
            'data.*.reason' => 'required_unless:data.*.discount,0',
            'data.*.tracking' => 'required',
            'data.*.docs' => 'required|array'
        ]);

        if($validator->fails()) {
            $response['status'] = 'Failed';
            $response['desc'] = $validator->errors();
            $httpStatusCode = 200;

            return Response::json($response, $httpStatusCode);
        } else {
            foreach($request->data as $d) {
                $clientId = $d['client_id'];
                $groupId = $d['group_id'];
                $services = $d['services'];
                $note = ($d['note']) ? $d['note'] : null;
                $discount = ($d['discount']) ? $d['discount'] : null;
                $reason = ($d['reason']) ? $d['reason'] : null;
                $tracking = $d['tracking'];
                $docs = $d['docs'];

                $client = User::findOrFail($clientId);

                if($tracking == 'New package') {
                    $tracking = $this->generateServiceTracking();

                    Package::create([
                        'client_id' => $clientId,
                        'group_id' => $groupId,
                        'log_date' => Carbon::now()->format('F j, Y, g:i a'),
                        'tracking' => $tracking,
                        'status' => 2
                    ]);
                }

                foreach($services as $index => $serviceId) {
                    $service = Service::findOrFail($serviceId);

                    // Service status
                        $req_docs = [];
                        if($service->docs_needed != null && $service->docs_needed != ''){
                            $req_docs = explode(',', $service->docs_needed); 
                        }

                        $chosen_docs = [];
                        if($docs[$index] != null && $docs[$index] != ''){
                            $chosen_docs = explode(',', $docs[$index]);
                        }

                        $compare_docs = array_diff($req_docs, $chosen_docs);
                        if(!empty($compare_docs)){
                            $service_status = 'pending';

                        }else{
                            $service_status = 'on process';
                        }

                    // Author
                        $dt = Carbon::now()->toDateString();
                        $user = Auth::user();
                        $author = $note . ' - ' . $user->first_name . ' <small>(' . $dt . ')</small>';
                        if(is_null($note)) {
                            $author = '';
                        }

                    $clientService = ClientService::create([
                        'client_id' => $clientId,
                        'service_id' => $serviceId,
                        'detail' => $service->detail,
                        'cost' => 0,
                        'charge' => $service->cost,
                        'tip' => 0,
                        'status' => $service_status,
                        'service_date' => Carbon::now()->format('m/d/Y'),
                        'remarks' => $author,
                        'group_id' => $groupId,
                        'tracking' => $tracking,
                        'temp_tracking' => null,
                        'active' => 1,
                        'extend' => null,
                        'rcv_docs' => $docs[$index]
                    ]);

                    $chosen = ''; 
                    $chosen_cn = ''; 
                    foreach($chosen_docs as $ch) {
                        $sd = ServiceDocuments::findOrFail($ch);
                        $chosen .= $sd->title.",";
                        $chosen_cn .= $sd->title_cn.",";
                    }
                    $chosen = rtrim($chosen, ',');
                    $chosen_cn = rtrim($chosen_cn, ',');

                    $missing = '';
                    $missing_cn = '';
                    if(!empty($compare_docs)) {
                        foreach($compare_docs as $cd) {
                            $sd = ServiceDocuments::findOrFail($cd);
                            $missing .= $sd->title.",";
                            $missing_cn .= $sd->title_cn.",";
                        }
                    }
                    $missing = rtrim($missing, ',');
                    $missing_cn = rtrim($missing_cn, ',');

                    if($missing != '') {
                        $missing = " Missing documents (".$missing."), ";
                        $missing_cn = " 文件缺失 (".$missing_cn."), ";
                    }

                    if($chosen != '') {
                        $chosen = "Received documents (".$chosen."), ";
                        $chosen_cn = "已验收文件 (".$chosen_cn."), ";
                    }

                    if($chosen != '' || $missing != '') {   
                        $reportDetail = $chosen . $missing;

                        Report::create([
                            'service_id' => $clientService->id,
                            'client_id' => $clientService->client_id,
                            'group_id' => $clientService->group_id,
                            'detail' => $reportDetail,
                            'user_id' => Auth::user()->id,
                            'tracking' => $clientService->tracking,
                            'log_date' => Carbon::now()->format('Y-m-d H:i:s a'),
                            'is_read' => 0
                        ]);
                    }

                    $this->check_package_updates($tracking);

                    // Transaction log
                    $log_data['client_id'] = $clientId;
                    $log_data['group_id'] = $groupId;
                    $log_data['service_id'] = $serviceId;
                    $log_data['tracking'] = $tracking;
                    $log_data['user_id'] = Auth::user()->id;
                    $log_data['type'] = 'service';
                    $log_data['amount'] = $service->cost;
                    $log_data['balance'] = ($groupId != 0) ? $this->getGroupBalance($groupId) : $this->clientBalance($clientId);
                    $log_data['detail'] = 'Added a service <strong>' . $service->detail . '</strong> to Package Number <strong>' . $tracking . '</strong> with Service Charge of Php<strong>' . $service->cost . '</strong> for Client <strong>[' . $clientId . '] ' . $client->full_name . '</strong>';
                    Common::saveTransactionLogs($log_data);

                    if($discount && count($services) == 1) {
                        ClientTransactions::create([
                            'type' => 'Discount',
                            'client_id' => $clientId,
                            'group_id' => $groupId,
                            'amount' => $discount,
                            'log_date' => Carbon::now()->format('m/d/Y g:i:s A'),
                            'service_id' => $clientService->id,
                            'reason' => $reason,
                            'tracking' => $tracking
                        ]);

                        $log_data = array(
                            'client_id' => $clientId,
                            'group_id' => $groupId,
                            'service_id' => $serviceId,
                            'tracking' => $tracking,
                            'user_id' => Auth::user()->id,
                            'type' => 'discount',
                            'amount' => $discount,
                            'balance' => ($groupId != 0) ? $this->getGroupBalance($groupId) : $this->clientBalance($clientId),
                            'detail'=> 'Discounted an amount of Php'.$discount.'&nbsp; with the reason of <i>"'.$reason.'"</i> on Package '.$tracking.'.',
                            'detail_cn'=> '已折扣额度 Php'.$discount.'&nbsp; 因为<i>"'.$reason.'"</i> 在服务包 # '.$tracking.'.'
                        );
                        Common::saveTransactionLogs($log_data);
                    }

                } // endforeach

            } // endforeach

            $response['status'] = 'Success';

            return Response::json($response);
        }
    }

}