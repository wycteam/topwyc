<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\DocumentLog;

use App\Acme\Filters\EwalletTransactionFilters;
use App\Http\Requests\DocLogValidation;

class DocumentLogsController extends ApiController
{
    public function __construct(DocumentLog $model, EwalletTransactionFilters $filter)
    {
        $this->model = $model;
        $this->filters = $filter;
        $this->validation = new DocLogValidation;
    }

    public function store(Request $request)
    {
        app(get_class($this->validation));

        $data = $this->model
        	->create($request->all());

        $data = $this->model
	        ->with(['processor','user','document'])
	        ->findOrFail($data->id);


        return $this->respondCreated($data);
    }
}
