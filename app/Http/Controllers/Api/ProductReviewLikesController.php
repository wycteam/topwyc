<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Models\Shopping\ProductReviewLikes;

use App\Http\Requests\ProductReviewLikesValidation;

use App\Acme\Filters\FeedbackLikesFilters;

class ProductReviewLikesController extends ApiController
{

    public function __construct(
    	 ProductReviewLikes $model
    	,FeedbackLikesFilters $filters
    )
    {
        $this->model = $model;
        $this->filters = $filters;
        $this->validation = new ProductReviewLikesValidation;
    }
}
