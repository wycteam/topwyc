<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Models\Shopping\Compare;
use App\Models\Shopping\Product;
use Illuminate\Support\Facades\DB;
use Auth;

class CompareController extends ApiController
{

    public function __construct(Compare $model)
    {
        $this->model = $model;
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();   
            return $next($request);
        });
    }

    public function addtocompare($id){

        $ret = [];

        $find = Compare::where([
            ['user_id', '=', $this->user->id],
            ['product_id', '=', $id],
        ])->get();

        if($this->getcomparetotal() >= 3){
            $ret['response'] = '3';
        } else {
            if(count($find) == 0){
                $ret['response'] = '1';
                $fav = new Compare();
                $fav->user_id = $this->user->id;
                $fav->product_id = $id;
                $fav->save();
            }
            else {
                $ret['response'] = '0';           
            }           
        }

        return $ret;
    }

    public function compare(){

        $data = $this->model::with('product')
            ->whereUserId($this->user->id)
            ->get();

        $compare_list = $data->map(
            function($data) {
                return [
                    "id" => $data->id,
                    "prod_id" => $data->product_id,
                    "slug" => $data->product->slug,
                    "name" => $data->product->name,
                    "price" => (is_null($data->product->discount) || $data->product->discount==0  ? $data->product->price : $data->product->sale_price),
                    "fee_included" => $data->product->fee_included,
                    "shipping_fee" => $data->product->shipping_fee, 
                    "charge" => $data->product->charge, 
                    "vat" => $data->product->vat,
                    "stock" => $data->product->stock,
                    "rating" => 'width:' . $data->product->product_rating . '%',
                    "desc" => $data->product->description,
                    "image" => $data->product->main_image
                ];
            }
        );

    	$jsonData = $this->createResponseData($compare_list,'success');
    	
    	return $this->setStatusCode(200)
            ->respond($jsonData);
    }

    public function getcomparetotal(){

        $data = Compare::where('user_id', '=', $this->user->id)->get();

        return count($data);
    }

    public function deletecompare($id){

        $data = Compare::where('id', '=', $id)->delete();

        return $this->getcomparetotal();
    }
}
