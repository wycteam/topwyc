<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->cookie('locale') ?: 'en';
        app()->setLocale($locale);
        return $next($request);
    }

    // public function handle($request, Closure $next)
    // {
    //     //dd(\Session::has('applocale'));
    //     if (\Session::has('applocale') AND array_key_exists(\Session::get('applocale'), Config::get('languages'))) {
    //         App::setLocale(\Session::get('applocale'));
    //     }
    //     else { // This is optional as Laravel will automatically set the fallback language if there is none specified
    //         App::setLocale(Config::get('app.fallback_locale'));
    //     }
    //     //dd(App::getLocale());
    //     return $next($request);
    // }
}
