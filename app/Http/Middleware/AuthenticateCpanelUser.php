<?php

namespace App\Http\Middleware;

use Closure;

class AuthenticateCpanelUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        if ($user->hasRole('master') || $user->hasRole('vice-master') || $user->hasRole('cpanel-admin') || $user->hasRole('human-resources') || $user->hasRole('support')) {
            return $next($request);
        }

        abort(403, 'Unauthorized');
    }
}
