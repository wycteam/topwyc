<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class Clients2Validation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();

        return $user->hasRole('master') || $user->hasRole('vice-master') || $user->hasRole('cpanel-admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // $user = User::find($this->user);
        return [
            'first_name' => 'bail|required|string|max:255',
            'last_name' => 'bail|required|string|max:255',
            'birth_date' => 'bail|required|date',
            'gender' => 'bail|required|string',
            'civil_status' => 'bail|required|string',

            // 'address.address' => 'bail|required|string',
            // 'address.city' => 'bail|required|string',
            // 'address.province' => 'bail|required|string',

            //'address.contact_number' => 'required_if:group_id,==,0',

            'email' => 'bail|required|string|email|max:255|unique:users,email,'.$this->id,
            'nationality'=>'required',
            'birth_country'=>'required'
            // 'roles' => 'bail|required|array',
            // 'permissions' => 'bail|required|array',
        ];
    }

    public function messages()
    {
        return [
            // 'address.address.required' => 'Address is required.',
            // 'address.city.required' => 'City is required.',
            // 'address.province.required' => 'Province is required.',
            'address.contact_number.required_if' => 'Contact Number is required.',
            'nationality.required' => 'Nationality is required',
            'birth_country.required' => 'Birth country is required',
        ];
    }
}
