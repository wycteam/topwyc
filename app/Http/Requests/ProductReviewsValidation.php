<?php

namespace App\Http\Requests;

use App\Models\Shopping\Product;
use Illuminate\Foundation\Http\FormRequest;

use Auth;
class ProductReviewsValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'required|max:450',
            'rating_id'  => 'required',
            'review' => 'required'

        ];
    }

    public function messages()
    {
        return [
            'content' => 'Review Content is required.',
            'rating_id' => 'Please rate the product from 1 to 5.',
            'review' => 'Please answer if it is good or bad.'
        ];
    }
}
