<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PasswordValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'old_password'              => 'required|min:6|match_old_password'
            ,'new_password'              => 'required|min:6|confirmed'
            ,'new_password_confirmation' => 'required|min:6'
        ];
    }

    public function messages()
    {
        return [
            'old_password.match_old_password' => 'Old password does not match.'
        ];
    }
}
