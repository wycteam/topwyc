<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Shopping\BankAccounts;
use Auth;

class NewBankAccountRequest extends FormRequest
{
    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            // Call the after method of the FormRequest (see below)
            $this->after($validator);
        });
    }


    public function after($validator)
    {
        $user = Auth::user();
        //if the user is a shopper because the admins can add bank account??
        if($user->hasRole('shop-user'))
        {
            if( 
                strtolower(trim(request()->account_name," ")) !=  
                strtolower(trim($user->first_name," ") . ' ' . trim($user->last_name," ")) 
            ) // withdraw or transfer
            {
                $validator->errors()->add('account_name', 'Account Name and your name should be the same. ('.$user->first_name . ' ' . $user->last_name.')');

            }

            //duplicate acct number check for current user
            $hasDuplicateBankAccount = BankAccounts::whereUserId($user->id)
                ->whereAccountNumber(request()->account_number)
                ->count();

            if($hasDuplicateBankAccount)
            {
                $validator->errors()->add('account_number', 'Account Number already used.');
            }
        }
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_id'           => 'required',
            'account_name'      => 'required|',
            'account_number'    => 'required|numeric',
            'reaccount_number'  => 'required|same:account_number',
        ];
    }

    public function attributes()
    {
        return [
            'bank_id'           => 'Bank',
            'reaccount_number'  => 'Re-typed Account Number',
            'account_name'      => 'Account Name',
            'account_number'    => 'Account Number',
        ];
    }

}
