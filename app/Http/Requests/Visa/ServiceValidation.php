<?php

namespace App\Http\Requests\Visa;

use Illuminate\Foundation\Http\FormRequest;

class ServiceValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'detail'       => 'required|unique:visa.services,detail'
            ,'type'         => 'required'
            // ,'cost'         => 'required_if:type,==,Child'
            ,'parent_id'    => 'required_if:type,==,Child'
            // ,'description'  => 'required'
            // ,'docs_needed'  => 'required_if:type,==,Child'
        ];
    }

    public function messages()
    {
        return [
             'detail.required'          => 'Name of Service is required.'
             ,'detail.unique'          => 'Name of Service is already exist'
            // ,'description.required'     => 'Please describe the service.'
            ,'type.required'            => 'Please choose if this service is a Parent or a Child.'
            ,'parent_id.required_if'    => 'Please choose a parent.'
            // ,'cost.required_if'         => 'Service Charge is required.'
            // ,'docs_needed.required_if'  => 'Please select required documents for this service.'
        ];  
    }
}
