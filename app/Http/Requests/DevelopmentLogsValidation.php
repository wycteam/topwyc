<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DevelopmentLogsValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'title'    => 'required'
            ,'date'     => 'required | date'
            ,'version'  => 'required'
        ];
    }

    public function messages()
    {
        return [
             'title.required'   => 'Please indicate the title of your update.'
            ,'date.required'    => 'Please indicate update date.'
            ,'date.date'        => 'Please use valid date.'
            ,'version.required' => 'Please indicate the version of your update.'
            
        ];
    }
}
