<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email2' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'email2.required' => 'Email is required.',
            'email2.email' => 'Invalid email format.'
        ];
    }
}
