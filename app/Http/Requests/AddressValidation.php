<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddressValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address'   =>  'required' 
           //,'barangay'  =>  'required'
           ,'city'      =>  'required'
           ,'province'  =>  'required'
           ,'mobile'    =>  'required'
        ];
    }

    public function messages(){
        return [
            'address.required'   =>  'Please indicate your complete address.'          
           ,'area_code.required'  =>  'Area code required.'          
        ];
    }
}
