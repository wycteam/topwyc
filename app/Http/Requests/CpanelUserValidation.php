<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class CpanelUserValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();

        return $user->hasRole('master') || $user->hasRole('vice-master') || $user->hasRole('cpanel-admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'bail|required|string|max:255',
            'last_name' => 'bail|required|string|max:255',
            'email' => 'bail|required|string|email|max:255|unique:users',
            'password' => 'bail|required|string|min:6|max:255|confirmed',
            'birth_date' => 'bail|required|date',
            'gender' => 'bail|required|string',
            'civil_status' => 'bail|required|string',

            'address' => 'bail|required|string',
            'contact_number' => 'bail|required|string',

            'roles' => 'bail|required|array',
            'permissions' => 'bail|required|array',
        ];
    }

    public function messages()
    {
        return [
            'address.required' => 'Address is required.',
            'contact_number.required' => 'Contact Number is required.'
        ];
    }
}
