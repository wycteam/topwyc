<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CartValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'name' => 'required',
            'number' => 'required',
            'address' => 'required',
            //'barangay' => 'required',
            'city' => 'required',
            'province' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'Type is required.',
            'name.required' => 'Name of Receiver is required.',
            'number.required' => 'Contact Number is required.',
            'address.required' => 'Complete Address is required.',
            //'barangay.required' => 'Barangay is required.',
            'city.required' => 'City is required.',
            'province.required' => 'Province is required.'
        ];
    }
}
