<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddFundsValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'option' => 'required',
            'amount' => 'required|numeric',
            'reason' => 'required_if:option,==,Discount|required_if:option,==,Refund'
        ];
    }
}
