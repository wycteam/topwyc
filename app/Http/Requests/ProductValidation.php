<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class ProductValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required',
            'store_list'        => 'required',
            'primaryImage'      => 'required',
            'category_id'       => 'required',
            'price'             => 'required|numeric',
            'stock'             => 'required|numeric',
            'boxContent'        => 'required',
            'description'       => 'required',
            //'sku'               => 'required',
            'package_type'      => 'required',
            'weight'            => 'required',
            'length'            => 'required',
            'width'             => 'required',
            'height'            => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'name'              => 'Product Name',
            'store_id'          => 'Store ID',
            'image'             => 'Product Primary Image',
            'price'             => 'Product Price',
            'stock'             => 'Product Stock',
            'boxContent'        => 'Product Box Content',
            'description'       => 'Product Description',
            //'sku'               => 'Product SKU',
            'package_type'      => 'Package Type',
            'weight'            => 'Weight',
            'length'            => 'Length',
            'width'             => 'Width',
            'height'            => 'Height',
        ];
    }


}
