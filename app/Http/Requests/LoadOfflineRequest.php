<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory ;

use App\Models\Shopping\Ewallet;
use App\Models\Shopping\EwalletTransactions;

use Auth;

class LoadOfflineRequest extends FormRequest
{

    protected function getValidatorInstance()
    {
        return parent::getValidatorInstance()->after(function ($validator) {
            // Call the after method of the FormRequest (see below)
            $this->after($validator);
        });
    }


    public function after($validator)
    {
        $user = Auth::user();
        $wallet =  $user->ewallet()->first();
        
        $current_balance = $wallet->amount;

        $usable_ewallet = $user->ewallet->usable_ewallet;
        //temp fix
        // $usable_ewallet = $wallet->usable_ewallet;

        if(request()->type != '') // withdraw or transfer
        {
            if(request()->type == 'Withdraw'){
                if(request()->amount < 2000){
                    $validator->errors()->add('amount', 'Minimum 2000 to request withdraw.');
                }
            }
            if(request()->amount > $usable_ewallet){ //if amount is greater than current balance stop1
                $validator->errors()->add('amount', 'You only have P'. number_format($usable_ewallet,2).' on your account.');
            }else if ( request()->amount > ($usable_ewallet) ) { //if amount is greater than on hold stop
                $canUse = $usable_ewallet<0?0:$usable_ewallet;
                $validator->errors()->add('amount', 'You can only use P'. number_format($canUse,2).'. Please wait for other transactions to finish.');
            }
        }else
        {
            $docs = $user->documents()->latest()->get()->unique('type')->values();
            $is_enterprise = 0;
            foreach ($docs as  $doc) {
                //individual
                switch ($doc->type) {
                    case 'bir':
                    case 'permit':
                    case 'sec':
                        if($doc->status == 'Verified')
                        {
                            $is_enterprise++;
                        }
                        break;
                }
                
            }
            //enterprise user  == 7m max ewallet else 1m only
            $max_amt_deposit = $is_enterprise==3?7000000:700000;
            $max_deposit_request = $max_amt_deposit - $current_balance;
            if(request()->amount > $max_deposit_request)
            {
                $validator->errors()->add('amount', 'You can only request for P'. number_format($max_deposit_request,2).' .');

            }
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank_id'   => 'required',
            'amount'    => 'required|numeric|min:100',
        ];
    }

    public function attributes()
    {
        $return_name = null;
        if(request()->type == 'Transfer')
            $return_name = 'Receiver';
        return [
            'bank_id'           => $return_name?$return_name:'Bank',
        ];
    }
}
