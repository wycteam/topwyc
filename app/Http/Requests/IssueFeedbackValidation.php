<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IssueFeedbackValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'case_solved' => 'required',
            'rate_agent' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'case_solved.required' => 'Please answer Was your case solved?',
            'rate_agent.required' => 'Please answer How would you rate the agent?'
        ];
    }
}
