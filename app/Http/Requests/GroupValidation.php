<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'groupName' => 'required',
            'branch' => 'required',
            'leader' => 'required'
            
            // 'contact' => 'required',
            // 'address' => 'required',
            // 'city' => 'required',
            // 'province' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'groupName' => 'Group name',
            'branch' => 'Branch',
            'leader'  => 'Leader'
        ];
    }
}
