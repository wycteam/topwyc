<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SecurityQuestionsValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'question'  => 'required'
            ,'question2' => 'required'
            ,'question1' => 'required'
            ,'answer'    => 'required'
            ,'answer2'   => 'required'
            ,'answer1'   => 'required'
        ];
    }

    public function messages()
    {
        return [
             'question.required'  => 'Please select a question.'
            ,'question2.required' => 'Please select a question.'
            ,'question1.required' => 'Please select a question.'
            ,'answer.required'    => 'Please indicate your answer.'
            ,'answer2.required'   => 'Please indicate your answer.'
            ,'answer1.required'   => 'Please indicate your answer.'
        ];
    }
}
