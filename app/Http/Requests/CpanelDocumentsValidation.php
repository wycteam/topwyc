<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CpanelDocumentsValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'expires_at' => 'required|after_or_equal:today'
        ];
    }


    public function messages()
    {
        return [
             'expires_at.required'       => 'Please indicate expiration date.'
            ,'expires_at.after_or_equal' => 'Please use valid expiration date.'
            
        ];
    }
}
