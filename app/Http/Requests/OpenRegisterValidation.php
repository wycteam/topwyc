<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OpenRegisterValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'nullable|email',
            'cp_num' => 'required',
            'bday'  => 'required'
           
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'First name is required.',
            'last_name.required' => 'Last name is required.',
            'email.unique' => 'Email need to be unique.',
            'email.email' => 'Invalid email format.',
            'cp_num.unique' => 'Mobile Number need to be unique.',
            'cp_num.required' => 'Mobile number is required.',
            'bday.required' => 'Birthday is required'
           
        ];
    }
}
