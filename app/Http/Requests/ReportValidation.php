<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class ReportValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();

        return $user->hasRole('master') || $user->hasRole('vice-master') || $user->hasRole('cpanel-admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'bail|required|string|max:255'
        ];
    }

    public function messages()
    {
        return [
            'message.required' => 'The report field is required.'
        ];
    }

}
