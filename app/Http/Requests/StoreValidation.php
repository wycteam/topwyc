<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'address.address' => 'required',
            'address.city' => 'required',
            'address.province' => 'required',
            'address.landmark' => 'required',
            'number.number' => 'required',
            'company.facebook' => 'nullable|regex:/http(s)?:\/\/(www\.)?facebook\.com\/.+/i',
            'company.twitter' => 'nullable|regex:/http(s)?:\/\/(www\.)?twitter\.com\/.+/i',
            'company.instagram' => 'nullable|regex:/http(s)?:\/\/(www\.)?instagram\.com\/.+/i',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Store name is required.',
            'email.required' => 'Email is required.',
            'email.email' => 'Invalid email format.',
            'address.address.required' => 'Complete address is required.',
            'address.city.required' => 'City is required.',
            'address.province.required' => 'Province is required.',
            'address.landmark.required' => 'Landmark is required.',
            'number.number.required' => 'Mobile number is required.',
        ];
    }
}
